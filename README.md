![](doc/samarbeid-logo-and-name.png)

# samarbeid community edition

Samarbeid is a powerful but intuitive and easy to use **collaboration plattform for small to medium-sized organisations**. Samarbeid is three-in-one 🛠️: it is for flexible task and process management, it is for task-oriented team communication and it is for integrated data and knowledge management. Samarbeid empowers teams with little to zero IT skills 🧓👩🏼👨 to manage all their processes and data in one place. Samarbeid is designed as a learning system for learning organisations.     




## Try samarbeid
You can try samarbeid in [our trial system](https://try.samarbeid.org). Login credentials are lutz@samarbeid.org | samarbeid or marie@samarbeid.org | samarbeid. 

## Features
- **Manage** all your tasks ✅ in light-weight but structured and **flexible processes**.
- **Communicate with the team** 💬 directly in the context of your tasks and processes.
- Add flexibly fields for **structured data into all tasks**.
- Use **process templates** to re-use best practices in your teams.
- Retain full flexibility by changing and **enhance each process individually** when necessary.
- Get **360° views** on the core concepts of your organisation (like each contract, customer or supplier) with **dossiers**.   
- Simply 📎 **add PDFs and other documents** to your tasks and processes - to store and find them in their relevant context.
- All data added to samarbeid is indexed by the **full text search** 🔍.
- **Manage the visibility** of processes and data at the level of groups.
- **Export all data** from your tasks and process for analyses and re-use in spreadsheet software. 
- Integrate your samarbeid **due calendar** 📅📆 into your personal calendar apps. 

## Licence

The source code of the samarbeid community edition is published under the MIT Open Source License deposited in [LICENSE](https://gitlab.com/samarbeid-org/samarbeid/-/blob/main/LICENSE).

The logo of samarbeid, all other components of the coprorate identity of samarbeid as well as all contents of the website (https://samarbeid.org as well as all subpages) are explicitly NOT part of the project licensed here. Any use of these contents and the logo of samarbeid - outside of the use of the software - requires the written consent of the owner. Requests for this by e-mail to: kontakt@samarbeid.org


## Getting started

- Requirements: Ruby (see `.ruby-version` for exact version), Nodejs (see `.tools-version`), PostgreSQL, Docker & Docker Compose
- `bin/setup`

Open http://localhost:3000 and you should see your local samarbeid running. A initial admin user account is automatically generated (see output of above command for details).


## Get in touch
If you want to learn more about samarbeid and the team behind the project please check the [samarbeid website](https://samarbeid.org). 

**Subscribe our newsletter** (4 issues per year) 📧 to regularly read a well curated overview about the latest developments in samarbeid. You find the subscription form at the [samarbeid website](https://samarbeid.org).

For **bug reports** please [create an issue](https://gitlab.com/samarbeid-org/samarbeid/-/issues/new).  

For **feature requests** and questions regarding our roadmap please write us an e-mail 💌 to contact@samarbeid.org

**Follow us** on [Twitter](https://twitter.com/samarbeid_org), [LinkedIn](https://www.linkedin.com/company/82678036/) or [Instagram](https://www.instagram.com/samarbeid_org/) to stay up to data about the samarbeid project.

## Software stack
Samarbeid is a Ruby on Rails application with Vue.js frontend. Our stack is:
- Ruby (MRI)
- Vue.js
- MaterialDesign via vuetify
- PostgreSQL
- GoodJob
