class UpdateMentionTypesForEventData < ActiveRecord::Migration[6.0]
  def up
    Events::ChangedSummaryEvent.all.each do |event|
      if event.old_summary || event.new_summary
        event.data[:old_summary] = Services::Mentioning.rename_mention_type(event.old_summary, "process", "workflow") if event.old_summary
        event.data[:new_summary] = Services::Mentioning.rename_mention_type(event.new_summary, "process", "workflow") if event.new_summary
        event.save!(touch: false) if event.changed?
      end
    end
  end

  def down
    Events::ChangedSummaryEvent.all.each do |event|
      if event.old_summary || event.new_summary
        event.data[:old_summary] = Services::Mentioning.rename_mention_type(event.old_summary, "workflow", "process") if event.old_summary
        event.data[:new_summary] = Services::Mentioning.rename_mention_type(event.new_summary, "workflow", "process") if event.new_summary
        event.save!(touch: false) if event.changed?
      end
    end
  end
end

class Services::Mentioning
  def self.rename_mention_type(html, from, to)
    iterate_mention_content(html) { |id, type, element|
      element.set_attribute("m-type", to) if type == from
    }
  end
end
