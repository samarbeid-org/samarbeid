class RemoveNameFromWorkflow < ActiveRecord::Migration[6.1]
  def change
    remove_column :workflows, :name
  end
end
