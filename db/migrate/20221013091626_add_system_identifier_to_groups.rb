class AddSystemIdentifierToGroups < ActiveRecord::Migration[6.1]
  def change
    add_column :groups, :system_identifier, :string
    add_index :groups, :system_identifier

    Group.find_by(name: I18n.t("system_group_all.name"))
      &.update!(system_identifier: :system_group_all, description: I18n.t("system_group_all.description"))
    group_all = Group.system_group_all
    group_all.users = User.all
  end
end
