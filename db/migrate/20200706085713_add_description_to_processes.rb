class AddDescriptionToProcesses < ActiveRecord::Migration[6.0]
  def change
    add_column :workflow_processes, :description, :text
  end
end
