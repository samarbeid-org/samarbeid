class AllowEmptyDossierFieldDefinitionNames < ActiveRecord::Migration[6.1]
  def change
    change_column_null :dossier_field_definitions, :name, true
  end
end
