class CreateShareLinks < ActiveRecord::Migration[6.1]
  def change
    create_table :share_links do |t|
      t.string :name
      t.string :token, null: false, index: {unique: true}
      t.references :shareable, polymorphic: true, null: false
      t.string :aasm_state, null: false

      t.timestamps
    end
  end
end
