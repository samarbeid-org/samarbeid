class ReindexForProjectSearch < ActiveRecord::Migration[5.2]
  def change
    Project.reindex
  end
end
