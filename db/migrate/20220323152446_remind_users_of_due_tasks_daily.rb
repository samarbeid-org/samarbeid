class RemindUsersOfDueTasksDaily < ActiveRecord::Migration[6.1]
  def change
    add_column :tasks, :due_processed_last, :date
    Task.where(due_processed: true).update_all(due_processed_last: Date.current)
    remove_column :tasks, :due_processed
  end
end
