class AddLabelToProcess < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_process_definitions, :label_prefix, :string
    add_index :workflow_process_definitions, :label_prefix, unique: true

    Workflow::ProcessDefinition.all.each do |pd|
      lp = pd.key[0..1].upcase
      pd.update_columns(label_prefix: lp)
    end
  end
end
