class AddTimeTrackingBudgetToTasks < ActiveRecord::Migration[6.1]
  def change
    add_column :tasks, :time_tracking_budget_in_minutes, :integer, null: false, default: 0
  end
end
