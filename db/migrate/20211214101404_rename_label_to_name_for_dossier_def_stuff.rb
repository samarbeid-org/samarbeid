class RenameLabelToNameForDossierDefStuff < ActiveRecord::Migration[6.1]
  def change
    rename_column :dossier_definitions, :label, :name
    rename_column :dossier_field_definitions, :label, :name

    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end
