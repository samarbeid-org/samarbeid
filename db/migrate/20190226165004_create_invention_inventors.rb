class CreateInventionInventors < ActiveRecord::Migration[5.2]
  def change
    create_table :invention_inventors do |t|
      t.references :person, foreign_key: true
      t.references :invention, foreign_key: true

      t.timestamps
    end

    OldInventorInventionJoin.all.each do |fmip|
      InventionInventor.create(person_id: fmip.person_id, invention_id: fmip.invention_id)
    end

    drop_table :inventors_inventions_persons
  end
end

class OldInventorInventionJoin < ApplicationRecord
  self.table_name = "inventors_inventions_persons"
end
