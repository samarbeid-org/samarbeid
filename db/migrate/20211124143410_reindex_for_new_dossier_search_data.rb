class ReindexForNewDossierSearchData < ActiveRecord::Migration[6.1]
  def change
    Dossier.reindex
  end
end
