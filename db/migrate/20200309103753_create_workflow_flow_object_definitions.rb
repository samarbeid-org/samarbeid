class CreateWorkflowFlowObjectDefinitions < ActiveRecord::Migration[6.0]
  def change
    rename_table :workflow_task_definitions, :workflow_flow_object_definitions
    add_column :workflow_flow_object_definitions, :type, :string
    change_column_null :workflow_flow_object_definitions, :order, true
    Workflow::FlowObjectDefinition.all.update_all(type: "Workflow::TaskDefinition")
    change_column_null :workflow_flow_object_definitions, :type, false
  end
end
