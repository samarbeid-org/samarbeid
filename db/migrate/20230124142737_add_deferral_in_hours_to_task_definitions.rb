class AddDeferralInHoursToTaskDefinitions < ActiveRecord::Migration[6.1]
  def change
    add_column :task_definitions, :deferral_in_hours, :integer
  end
end
