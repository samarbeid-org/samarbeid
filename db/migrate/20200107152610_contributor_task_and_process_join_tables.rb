class ContributorTaskAndProcessJoinTables < ActiveRecord::Migration[5.2]
  def change
    create_join_table :users, :workflow_processes do |t|
      t.index :user_id
      t.index :workflow_process_id
      t.index [:user_id, :workflow_process_id], unique: true, name: "unique_index_users_process_join"
    end

    create_join_table :users, :workflow_tasks do |t|
      t.index :user_id
      t.index :workflow_task_id
      t.index [:user_id, :workflow_task_id], unique: true, name: "unique_index_users_tasks_join"
    end
  end
end
