class CreateExternalRequirements < ActiveRecord::Migration[5.2]
  def change
    create_table :external_requirements do |t|
      t.datetime :first_meeting_at
      t.text :formalities
      t.text :detailed_needs

      t.timestamps
    end

    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end

class Workflow::ProcessDefinition < ApplicationRecord
  def self.data_entity_classes
    [Invention, PatentApplication, EventPlanning, ExternalRequirement]
  end
end
