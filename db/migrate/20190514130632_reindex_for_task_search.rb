class ReindexForTaskSearch < ActiveRecord::Migration[5.2]
  def change
    Workflow::Task.reindex
  end
end
