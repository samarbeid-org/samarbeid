class AddDossierAssociationsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :dossier_associations do |t|
      t.references :dossier, null: false, foreign_key: true, index: true
      t.references :data_entity, polymorphic: true, index: {name: :index_dossier_associations_on_data_entity_type_and_id}, null: false
      t.string :data_entity_field, null: false, index: true
      t.timestamps
    end
  end
end
