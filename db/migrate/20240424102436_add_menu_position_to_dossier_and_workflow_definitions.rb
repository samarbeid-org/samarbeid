class AddMenuPositionToDossierAndWorkflowDefinitions < ActiveRecord::Migration[6.1]
  def change
    add_column :workflow_definitions, :menu_position, :integer
    add_column :dossier_definitions, :menu_position, :integer
  end
end
