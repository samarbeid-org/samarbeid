class AddWorkflowDefinitionIdToTaskItemDefinitions < ActiveRecord::Migration[6.0]
  def change
    add_reference :task_item_definitions, :workflow_definition, foreign_key: true
    TaskItemDefinition.find_each do |task_item_definition|
      workflow_definition = task_item_definition&.content_item_definition&.workflow_definition
      task_item_definition.delete! unless workflow_definition
      task_item_definition.update!(workflow_definition: workflow_definition)
    end
    change_column_null :task_item_definitions, :workflow_definition_id, false
  end
end
