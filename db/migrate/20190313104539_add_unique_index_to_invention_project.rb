class AddUniqueIndexToInventionProject < ActiveRecord::Migration[5.2]
  def change
    remove_index :inventions, :project_id
    add_index :inventions, :project_id, unique: true
  end
end
