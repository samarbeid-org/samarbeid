class RemoveExitCodeStuff < ActiveRecord::Migration[6.1]
  def change
    drop_join_table :exit_codes, :workflow_definitions
    remove_reference :workflows, :exit_code
    drop_table :exit_codes

    Events::CompletedEvent.all.update_all(data: nil)
  end
end
