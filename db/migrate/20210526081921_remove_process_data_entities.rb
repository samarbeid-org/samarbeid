class RemoveProcessDataEntities < ActiveRecord::Migration[6.0]
  def change
    remove_column :workflow_definitions, :model, :jsonb
    remove_reference :workflows, :data_entity, polymorphic: true
    remove_reference :documents, :data_entity, polymorphic: true
    remove_column :documents, :data_entity_field, :string

    drop_table :process_associations
    drop_table :dossier_associations
    drop_table :process_data_entities

    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end
