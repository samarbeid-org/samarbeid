class LinkProcessToDataEntities < ActiveRecord::Migration[5.2]
  def change
    add_reference :workflow_processes, :data_entity, polymorphic: true, index: true
    Workflow::Process.all.find_each do |process|
      process.update(data_entity: process.old_data_entity)
    end
    remove_column :inventions, :workflow_process_id
    remove_column :patent_applications, :workflow_process_id
  end
end

class Workflow::Process < ApplicationRecord
  has_many :tasks, foreign_key: "workflow_process_id", dependent: :destroy
  has_one :invention, foreign_key: "workflow_process_id"
  has_one :patent_application, foreign_key: "workflow_process_id"

  belongs_to :data_entity, polymorphic: true

  def old_data_entity
    invention || patent_application
  end
end
