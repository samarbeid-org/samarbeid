class AddRoleToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :role, :integer, default: 0

    User.where(admin: true).update_all(role: :super_admin)

    remove_column :users, :admin
  end
end
