class AddWorkingTitleToEventPlannigns < ActiveRecord::Migration[5.2]
  def change
    add_column :event_plannings, :working_title, :string
  end
end
