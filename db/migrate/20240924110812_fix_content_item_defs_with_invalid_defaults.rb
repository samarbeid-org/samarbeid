class FixContentItemDefsWithInvalidDefaults < ActiveRecord::Migration[7.0]
  def change
    possible_invalids = ContentItemDefinition.where.not(default_value: nil).where(content_type: "file")

    puts "Checking #{possible_invalids.count} ContentItemDefinitions for invalid default values..."
    possible_invalids.each do |content_item_definition|
      content_item_definition.default_value
      puts "OK: #{content_item_definition.id}"
    rescue => error
      puts "ContentItemDefinition #{content_item_definition.id} has invalid default value: #{error}"
      content_item_definition.update_column(:default_value, nil)
      puts "Fixed: #{content_item_definition.id} by setting default_value to nil"
    end
  end
end
