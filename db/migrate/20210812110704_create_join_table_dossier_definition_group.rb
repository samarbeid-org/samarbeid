class CreateJoinTableDossierDefinitionGroup < ActiveRecord::Migration[6.0]
  def change
    create_join_table :dossier_definitions, :groups do |t|
      t.index [:dossier_definition_id, :group_id], unique: true, name: "dossier_def_on_group_unique"
    end

    # Everybody may see all Dossiers - as has been up to now
    Group.all.find_each do |group|
      group.update!(dossier_definitions: DossierDefinition.all)
    end

    RunFullReindexJob.perform_later
  end
end

class Group < ApplicationRecord
  def full_reindex(_params)
    # Do nothing in this migration
  end
end
