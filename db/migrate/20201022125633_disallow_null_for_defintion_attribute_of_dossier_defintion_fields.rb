class DisallowNullForDefintionAttributeOfDossierDefintionFields < ActiveRecord::Migration[6.0]
  def change
    change_column_null :dossier_definition_fields, :definition_id, false
  end
end
