class CreateJoinTableExitCodeWorkflow < ActiveRecord::Migration[6.0]
  def change
    create_join_table :exit_codes, :workflow_definitions
    add_reference :workflows, :exit_code
  end
end
