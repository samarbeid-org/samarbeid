class AddVersionToWorkflowAndDefinitions < ActiveRecord::Migration[6.1]
  def change
    add_column :workflow_definitions, :version, :integer, default: 0
    add_column :workflows, :definition_version, :integer

    Events::ChangedStructureEventByInstance.all.find_each do |event|
      event.update!(data: {workflow_used_for_update: event.data&.[](:workflow_instance),
                           new_workflow_definition_version: 0})
    end
  end
end
