class RefactorDocumentContentTypeToFileContentType < ActiveRecord::Migration[6.0]
  def change
    ContentItem.where(content_type: "document").update_all(content_type: "file")
    # Clear all previously associated documents
    ContentItem.where(content_type: "file").update_all(value: "[]")

    ContentItemDefinition.where(content_type: "document").update_all(content_type: "file")

    Document.destroy_all
    drop_table :documents
  end
end

class Document < ApplicationRecord
  self.inheritance_column = "sti_type"

  has_one_attached :file
end
