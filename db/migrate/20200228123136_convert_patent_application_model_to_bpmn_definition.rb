class ConvertPatentApplicationModelToBpmnDefinition < ActiveRecord::Migration[6.0]
  def change
    PersonAssociation.where(data_entity_type: "PatentApplication").delete_all
    UploadedFile.where(data_entity_type: "PatentApplication").delete_all
    Workflow::Process.where(data_entity_type: "PatentApplication").destroy_all
    PaperTrail::Version.where(item_type: "PatentApplication").delete_all
    PaperTrail::Version.where(item_type: "ExternalRequirement").delete_all
    drop_table "patent_applications"
  end
end
