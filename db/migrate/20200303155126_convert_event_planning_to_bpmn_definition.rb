class ConvertEventPlanningToBpmnDefinition < ActiveRecord::Migration[6.0]
  def change
    PersonAssociation.where(data_entity_type: "EventPlanning").delete_all
    UploadedFile.where(data_entity_type: "EventPlanning").delete_all
    Workflow::Process.where(data_entity_type: "EventPlanning").destroy_all
    Workflow::Process.where(data_entity_type: "EventPlanning").destroy_all
    PaperTrail::Version.where(item_type: "EventPlanning").delete_all # because we want to remove model completely
    Event.all.select { |event| event.object.blank? }.map(&:destroy)
    drop_table "event_plannings"
  end
end
