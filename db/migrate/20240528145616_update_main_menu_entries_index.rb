class UpdateMainMenuEntriesIndex < ActiveRecord::Migration[6.1]
  def change
    remove_index :main_menu_entries, column: [:parent_id, :position]
    add_index :main_menu_entries, [:parent_id, :position]
  end
end
