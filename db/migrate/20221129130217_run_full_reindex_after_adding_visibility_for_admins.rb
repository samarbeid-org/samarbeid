class RunFullReindexAfterAddingVisibilityForAdmins < ActiveRecord::Migration[6.1]
  def change
    RunFullReindexJob.perform_later
  end
end
