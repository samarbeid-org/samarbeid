class CreatePersonAssociations < ActiveRecord::Migration[5.2]
  def change
    create_table :person_associations do |t|
      t.references :person, null: false, foreign_key: true, index: true
      t.references :data_entity, polymorphic: true, index: {name: :index_person_associations_on_data_entity_type_and_id}, null: false
      t.string :data_entity_field, null: false, index: true
      t.timestamps
    end

    Person.all.each do |person|
      if person.invention_inventors.any?
        person.invention_inventors.each do |invention_inventor|
          PersonAssociation.create(person: person, data_entity: invention_inventor.invention, data_entity_field: :inventors)
        end
      end

      if person.invention_first_meeting_participations.any?
        person.invention_first_meeting_participations.each do |invention_first_meeting_participation|
          PersonAssociation.create(person: person, data_entity: invention_first_meeting_participation.invention, data_entity_field: :first_meeting_participants)
        end
      end
    end

    drop_table :invention_first_meeting_participations
    drop_table :invention_inventors
  end
end

class InventionFirstMeetingParticipation < ApplicationRecord
  belongs_to :invention
  belongs_to :person

  has_paper_trail
end

class InventionInventor < ApplicationRecord
  belongs_to :person
  belongs_to :invention

  has_paper_trail
end

class Person < ApplicationRecord
  has_many :invention_inventors
  has_many :invention_first_meeting_participations
end
