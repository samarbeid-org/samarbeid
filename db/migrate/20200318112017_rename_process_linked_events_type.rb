class RenameProcessLinkedEventsType < ActiveRecord::Migration[6.0]
  def change
    Event.where(type: "Events::ProcessLinkedEvent").update_all(type: "Events::ProcessAddedSuccessorProcessEvent")
  end
end
