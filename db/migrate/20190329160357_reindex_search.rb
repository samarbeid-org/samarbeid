class ReindexSearch < ActiveRecord::Migration[5.2]
  def change
    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end

class Workflow::ProcessDefinition < ApplicationRecord
  def self.data_entity_classes
    [Invention, PatentApplication]
  end
end
