class ReindexWorkflowsToUpdateSearchData < ActiveRecord::Migration[6.1]
  def change
    Workflow.reindex
  end
end
