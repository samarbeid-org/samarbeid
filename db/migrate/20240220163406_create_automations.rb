class CreateAutomations < ActiveRecord::Migration[6.1]
  def change
    create_table :automations do |t|
      t.string :title, null: false
      t.boolean :active, null: false
      t.jsonb :schedule, null: false
      t.date :next_execution, null: false
      t.belongs_to :workflow_definition, foreign_key: {to_table: :workflow_definitions}, null: false, index: true
      t.belongs_to :candidate_assignee, foreign_key: {to_table: :users}

      t.timestamps
    end
    add_index :automations, :title, unique: true
  end
end
