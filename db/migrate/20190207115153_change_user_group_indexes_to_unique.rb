class ChangeUserGroupIndexesToUnique < ActiveRecord::Migration[5.2]
  def change
    remove_index :user_groups, [:user_id, :group_id]
    remove_index :user_groups, [:group_id, :user_id]

    add_index :user_groups, [:user_id, :group_id], unique: true
    add_index :user_groups, [:group_id, :user_id], unique: true
  end
end
