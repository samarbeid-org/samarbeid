class AddDossierDefinitionsTables < ActiveRecord::Migration[6.0]
  def change
    create_table :dossier_definitions do |t|
      t.string :identifier, null: false, index: {unique: true}
      t.string :label, null: false
      t.text :description

      t.timestamps
    end

    create_table :dossier_definition_fields do |t|
      t.belongs_to :definition, foreign_key: {to_table: :dossier_definitions}
      t.string :identifier, null: false
      t.string :label, null: false
      t.integer :position, null: false
      t.integer :content_type, null: false

      t.timestamps
    end

    add_index :dossier_definition_fields, [:definition_id, :identifier], unique: true
    add_index :dossier_definition_fields, [:definition_id, :position], unique: true
  end
end
