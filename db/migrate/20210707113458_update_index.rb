class UpdateIndex < ActiveRecord::Migration[6.0]
  def change
    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end
