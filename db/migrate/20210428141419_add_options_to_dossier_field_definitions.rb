class AddOptionsToDossierFieldDefinitions < ActiveRecord::Migration[6.0]
  def change
    add_column :dossier_field_definitions, :options, :jsonb, null: false, default: {}
  end
end
