class AllowNullForDossierData < ActiveRecord::Migration[6.0]
  def change
    change_column_null :dossiers, :data, true
  end
end
