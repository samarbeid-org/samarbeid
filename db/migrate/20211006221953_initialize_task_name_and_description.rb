class InitializeTaskNameAndDescription < ActiveRecord::Migration[6.1]
  def change
    Task.where(name: nil).each { |t| t.update(name: t.task_definition.name) }
    Task.where(description: nil).each { |t| t.update(description: t.task_definition&.description) if t.task_definition&.description.present? }
  end
end
