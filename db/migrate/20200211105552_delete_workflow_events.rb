class DeleteWorkflowEvents < ActiveRecord::Migration[5.2]
  def change
    drop_table :workflow_events
  end
end
