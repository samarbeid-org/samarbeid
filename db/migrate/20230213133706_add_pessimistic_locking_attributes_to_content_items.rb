class AddPessimisticLockingAttributesToContentItems < ActiveRecord::Migration[6.1]
  def change
    add_column :content_items, :locked_at, :datetime
    add_column :content_items, :locked_by, :bigint
  end
end
