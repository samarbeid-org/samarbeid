class RenameProjectToAmbition < ActiveRecord::Migration[5.2]
  def change
    rename_table :projects, :ambitions
    rename_column :workflow_processes, :project_id, :ambition_id
    begin
      CustomElasticSearchConfig.initalize_searchkick_indexes
    rescue
      nil
    end
  end
end
