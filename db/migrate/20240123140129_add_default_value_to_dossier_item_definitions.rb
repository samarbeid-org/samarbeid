class AddDefaultValueToDossierItemDefinitions < ActiveRecord::Migration[6.1]
  def change
    add_column :dossier_item_definitions, :default_value, :jsonb

    DossierItemDefinition.where(content_type: ContentTypes::Note.to_s).find_each do |did|
      did.default_value = did.options&.[]("text")
      did.options = {}
      did.save!
    end
  end
end
