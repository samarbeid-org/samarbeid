class RemoveReopenWorkflowTasks < ActiveRecord::Migration[5.2]
  def change
    Workflow::Task.where(state: "reopened").update_all(state: Workflow::Task::COMPLETED)
    Workflow::Event.where(event_type: "reopened").destroy_all
  end
end
