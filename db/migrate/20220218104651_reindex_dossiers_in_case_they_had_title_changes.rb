class ReindexDossiersInCaseTheyHadTitleChanges < ActiveRecord::Migration[6.1]
  def change
    ReindexSearchableJob.perform_later(Dossier)
  end
end
