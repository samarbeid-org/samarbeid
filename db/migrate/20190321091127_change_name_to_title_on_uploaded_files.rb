class ChangeNameToTitleOnUploadedFiles < ActiveRecord::Migration[5.2]
  def change
    rename_column :uploaded_files, :name, :title
  end
end
