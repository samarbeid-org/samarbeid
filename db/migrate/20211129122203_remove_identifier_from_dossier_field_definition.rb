class RemoveIdentifierFromDossierFieldDefinition < ActiveRecord::Migration[6.1]
  def change
    remove_column :dossier_field_definitions, :identifier
    add_index :dossier_field_definitions, [:definition_id, :label], unique: true

    DossierFieldDefinition.reset_column_information
    CustomElasticSearchConfig.reindex_and_refresh(Dossier)
  end
end
