class ReindexAfterChangingTaskTabCategory < ActiveRecord::Migration[6.1]
  def change
    Task.reindex
  end
end
