class MoveProjectAssociationToWorkflowProcess < ActiveRecord::Migration[5.2]
  def change
    add_reference :workflow_processes, :project, foreign_key: true

    Workflow::Process.all.each do |process|
      process.update!(project: process.data_entity.project)
    end
  end
end

class Invention < ApplicationRecord
  belongs_to :project
end

class PatentApplication < ApplicationRecord
  belongs_to :project
end
