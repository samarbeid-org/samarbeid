class RemoveReadyState < ActiveRecord::Migration[6.1]
  def change
    # Currently snoozed tasks, snoozed event already exists
    Task.where(aasm_state: "ready").where.not(start_at: nil).update_all(aasm_state: :snoozed)

    # Currently real waiting tasks, ready event should be deleted
    Task.where(aasm_state: "ready").update_all(aasm_state: :active)
    ready_events = Event.where(type: "Events::ReadiedEvent")
    Notification.where(event: ready_events).destroy_all
    ready_events.delete_all

    ReindexSearchableJob.perform_later(Task)
    ReindexSearchableJob.perform_later(Workflow)
  end
end
