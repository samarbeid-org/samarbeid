class AddPageToElasticSearch < ActiveRecord::Migration[6.1]
  def change
    CustomElasticSearchConfig.reindex_model(Page)
  end
end
