class CreateComments < ActiveRecord::Migration[5.2]
  def change
    create_table :comments do |t|
      t.text :message
      t.references :author, foreign_key: {to_table: :users}, null: false
      t.references :task, foreign_key: {to_table: :workflow_tasks}, null: false

      t.timestamps
    end

    Workflow::Event.where(event_type: Workflow::Event::COMMENTED).find_each do |event_comment|
      Comment.create!(message: event_comment.payload[:message], task: event_comment.task, author: event_comment.user)
      event_comment.destroy!
    rescue => error
      puts "EXCEPTION for #{event_comment}"
      puts error
    end
  end
end
