class ChangeBooleansToEnumsForInventions < ActiveRecord::Migration[5.2]
  def change
    remove_column :inventions, :patentable
    remove_column :inventions, :service_invention
    add_column :inventions, :patentable, :integer, limit: 1
    add_column :inventions, :service_invention, :integer, limit: 1
  end
end
