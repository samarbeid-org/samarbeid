class AddStateToWorkflowProcesses < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_processes, :state, :string
    add_column :workflow_processes, :cancel_info, :text

    Workflow::Process.all.each do |process|
      process.update!(state: process.tasks.started.any? ? Workflow::Process::RUNNING : Workflow::Process::COMPLETED)
    end

    change_column_null :workflow_processes, :state, false
  end
end
