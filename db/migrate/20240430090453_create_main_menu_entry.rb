class CreateMainMenuEntry < ActiveRecord::Migration[6.1]
  def change
    create_table :main_menu_entries do |t|
      t.belongs_to :parent
      t.integer :position, null: false
      t.string :title
      t.references :linked_object, polymorphic: true
      t.timestamps
    end

    add_index :main_menu_entries, [:parent_id, :position], unique: true

    remove_column :workflow_definitions, :menu_position, :integer
    remove_column :dossier_definitions, :menu_position, :integer
  end
end
