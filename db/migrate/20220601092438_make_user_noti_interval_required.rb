class MakeUserNotiIntervalRequired < ActiveRecord::Migration[6.1]
  def change
    User.where(noti_interval: nil).update_all(noti_interval: 1)
    change_column_null :users, :noti_interval, false
  end
end
