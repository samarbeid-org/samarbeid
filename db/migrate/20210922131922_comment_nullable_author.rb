class CommentNullableAuthor < ActiveRecord::Migration[6.1]
  def change
    change_column_null(:comments, :author_id, true)
    change_column_default(:comments, :author_id, nil)
  end
end
