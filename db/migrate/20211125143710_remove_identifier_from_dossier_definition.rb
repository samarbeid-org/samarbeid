class RemoveIdentifierFromDossierDefinition < ActiveRecord::Migration[6.1]
  def change
    remove_column :dossier_definitions, :identifier
    add_index :dossier_definitions, :label, unique: true

    CustomElasticSearchConfig.reindex_and_refresh(Dossier)
  end
end
