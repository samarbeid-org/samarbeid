class DropBpmnStructure < ActiveRecord::Migration[6.0]
  def change
    Workflow::FlowObjectDefinition.where.not(type: "Workflow::TaskDefinition").delete_all

    rename_table :workflow_flow_object_definitions, :definition_tasks

    remove_column :definition_tasks, :type, :string

    drop_table :workflow_sequence_flows
  end
end

module Workflow
  def self.table_name_prefix
    "workflow_"
  end

  class FlowObjectDefinition < ApplicationRecord
  end
end
