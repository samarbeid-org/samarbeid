class AllowDossierCreatedByToBeNull < ActiveRecord::Migration[6.0]
  def change
    change_column_null :dossiers, :created_by_id, true
  end
end
