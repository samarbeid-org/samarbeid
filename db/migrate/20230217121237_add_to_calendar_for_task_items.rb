class AddToCalendarForTaskItems < ActiveRecord::Migration[6.1]
  def change
    add_column :task_items, :add_to_calendar, :boolean, default: nil
  end
end
