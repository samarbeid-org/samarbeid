class AddNewFieldsOfUpdatedProcessToInvetion < ActiveRecord::Migration[5.2]
  def change
    add_column :inventions, :served_by_university, :boolean
  end
end
