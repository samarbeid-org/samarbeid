class ChangeContentItemValueTypeToJsonb < ActiveRecord::Migration[6.0]
  def up
    # Migrate Dossier field data in dossiers
    DossierFieldDefinition.where(content_type: "dossier").each do |field_def|
      field_def.definition.dossiers.each do |dossier|
        old_value = dossier.data[field_def.identifier]
        unless old_value.nil?
          new_value = field_def.content_type.deserialize(JSON.parse(old_value)).map(&:id)
          dossier.set_field_value(field_def.identifier, new_value)
          dossier.update_columns(data: dossier.data)
        end
      end
    end

    # Migrate ContentItems
    add_column :content_items, :value_tmp, :string
    ContentItem.all.each do |item|
      item.update_columns(value_tmp: item.read_attribute(:value), value: nil)
    end
    change_column :content_items, :value, :jsonb, using: "value::jsonb"
    ContentItem.reset_column_information
    ContentItem.update_all(value: nil)
    ContentItem.where.not(value_tmp: nil).each do |item|
      item.value_helper = case item.content_type.name
      when ContentTypes::Dossier.name, ContentTypes::Document.name
        item.content_type.deserialize(JSON.parse(item.value_tmp)).map(&:id)
      when ContentTypes::Selection.name
        item.content_type.deserialize(JSON.parse(item.value_tmp))
      else
        item.content_type.deserialize(item.value_tmp)
      end

      item.save!
    end
    remove_column :content_items, :value_tmp, :string

    # Migrate default_value of content_item_definitions
    add_column :content_item_definitions, :default_value_tmp, :string
    ContentItemDefinition.all.each do |item|
      item.update_columns(default_value_tmp: item.read_attribute(:default_value), default_value: nil)
    end
    change_column :content_item_definitions, :default_value, :jsonb, using: "default_value::jsonb"
    ContentItemDefinition.reset_column_information
    ContentItemDefinition.where.not(default_value_tmp: nil).each do |item|
      item.default_value = case item.content_type.name
      when ContentTypes::Dossier.name, ContentTypes::Document.name
        item.content_type.deserialize(JSON.parse(item.default_value_tmp)).map(&:id)
      when ContentTypes::Selection.name
        item.content_type.deserialize(JSON.parse(item.default_value_tmp))
      else
        item.content_type.deserialize(item.default_value_tmp)
      end

      item.save!
    end
    remove_column :content_item_definitions, :default_value_tmp, :string
  end

  def down
    change_column :content_items, :value, :string
  end
end

class ContentItem < ApplicationRecord
  def value_helper=(new_value)
    casted_new_value = content_type.serialize(content_type.cast(new_value))
    return if casted_new_value == value
    write_attribute(:value, casted_new_value)
  end

  def reindex_workflow
  end
end
