class RefactorNoteTexts < ActiveRecord::Migration[6.1]
  def change
    ContentItemDefinition.where(content_type: ContentTypes::Note.to_s).find_each do |cid|
      cid.default_value = cid.options&.[]("text")
      cid.options = {}
      cid.save!
    end

    ContentItem.where(content_type: ContentTypes::Note.to_s).find_each do |ci|
      ci.value = ci.options&.[]("text")
      ci.options = {}
      ci.save!
    end
  end
end
