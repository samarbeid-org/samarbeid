class AddCalendarAccessTokenToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :calendar_token, :string, unique: true, index: true
  end
end
