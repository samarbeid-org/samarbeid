class AddDeactivatedAtToWorkflowDefinitions < ActiveRecord::Migration[6.1]
  def change
    add_column :workflow_definitions, :deactivated_at, :datetime
  end
end
