class DropAllDataRelatedToInventionModel < ActiveRecord::Migration[6.0]
  def change
    PersonAssociation.where(data_entity_type: "Invention").delete_all
    UploadedFile.where(data_entity_type: "Invention").delete_all
    Workflow::Process.where(data_entity_type: "Invention").destroy_all
    PaperTrail::Version.where(item_type: "Invention").delete_all
    Event.all.select { |event| event.object.blank? }.map(&:destroy)
    drop_table "inventions"
  end
end

class Invention < ApplicationRecord
  include WorkflowSupport
  include AssociationSupport
  include DataEntitySearch

  has_many_persons :first_meeting_participants
  has_many_persons :inventors

  has_many_files :protocol_files
  has_many_files :rough_description_files
  has_many_files :srs_inventor_contract_files

  def get_task_result(task_identifier)
    case task_identifier.to_sym
    when :specify_service_invention
      {is_service_invention: service_invention}
    when :specify_if_served_by_university
      {is_served_by_university: served_by_university}
    end
  end

  def self.camunda_process_definition_key
    "initial_meeting_invention"
  end

  def self.define_task_attributes
    {
      arrange_first_meeting: [{first_meeting_at: :datetime}, {first_meeting_participants: :has_many_persons}],
      add_protocol: [{protocol_files: :has_many_files}],
      specifiy_inventors: [{inventors: :has_many_persons}],
      initial_rating_patentability: [{patentable: :boolean}],
      specify_service_invention: [{service_invention: :boolean}],
      specifiy_rough_description: [{rough_description_files: :has_many_files}],
      specify_if_served_by_university: [{served_by_university: :boolean}],
      add_contract_between_srs_and_inventor: [{srs_inventor_contract_files: :has_many_files}],
      set_reference_from_genese: [{reference: :string}]
    }
  end
end

class Workflow::Process < ApplicationRecord
  alias_method :data_entity, :data_entity_without_class_loading
end
