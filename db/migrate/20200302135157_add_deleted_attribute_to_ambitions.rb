class AddDeletedAttributeToAmbitions < ActiveRecord::Migration[6.0]
  def change
    add_column :ambitions, :deleted, :boolean, null: false, default: false
  end
end
