class CreateWorkflowSequenceFlows < ActiveRecord::Migration[6.0]
  def change
    create_table :workflow_sequence_flows do |t|
      t.references :from_object, references: :workflow_object_definitions, null: false, polymorphic: true, index: false
      t.references :to_object, references: :workflow_object_definitions, null: false, polymorphic: true, index: false
      t.references :workflow_process_definition, references: :workflow_process_definitions, null: false, index: false
      t.index [:workflow_process_definition_id, :from_object_id, :to_object_id], unique: true, name: "workflow_sequence_flows_only_connect_once_per_direction"
    end
  end
end
