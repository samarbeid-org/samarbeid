class MigrateEventDataValuesToUseGlobalIdSerializer < ActiveRecord::Migration[6.0]
  def change
    add_column :events, :data_new, :text

    Event.all.each do |event|
      event.update!(data_new: GlobalIdSerializer.dump(event.data))
    end

    remove_column :events, :data
    rename_column :events, :data_new, :data
  end
end

class Event < ApplicationRecord
  serialize :data
end
