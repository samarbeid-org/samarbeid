class RenameProcessEvents < ActiveRecord::Migration[6.0]
  def change
    Event.where(type: "Events::ProcessAddedContributorEvent").update_all(type: "Events::WorkflowAddedContributorEvent")
    Event.where(type: "Events::ProcessAddedSuccessorProcessEvent").update_all(type: "Events::WorkflowAddedSuccessorWorkflowEvent")
    Event.where(type: "Events::ProcessAssignedEvent").update_all(type: "Events::WorkflowAssignedEvent")
    Event.where(type: "Events::ProcessCanceledEvent").update_all(type: "Events::WorkflowCanceledEvent")
    Event.where(type: "Events::ProcessChangedSummaryEvent").update_all(type: "Events::WorkflowChangedSummaryEvent")
    Event.where(type: "Events::ProcessChangedTitleEvent").update_all(type: "Events::WorkflowChangedTitleEvent")
    Event.where(type: "Events::ProcessChangedVisibilityEvent").update_all(type: "Events::WorkflowChangedVisibilityEvent")
    Event.where(type: "Events::ProcessCompletedEvent").update_all(type: "Events::WorkflowCompletedEvent")
    Event.where(type: "Events::ProcessRemovedContributorEvent").update_all(type: "Events::WorkflowRemovedContributorEvent")
    Event.where(type: "Events::ProcessStartedEvent").update_all(type: "Events::WorkflowStartedEvent")
    Event.where(type: "Events::ProcessUnassignedEvent").update_all(type: "Events::WorkflowUnassignedEvent")

    Event.where(type: "Events::AmbitionAddedProcessEvent").update_all(type: "Events::AmbitionAddedWorkflowEvent")
    Event.where(type: "Events::AmbitionCompletedProcessEvent").update_all(type: "Events::AmbitionCompletedWorkflowEvent")
    Event.where(type: "Events::AmbitionRemovedProcessEvent").update_all(type: "Events::AmbitionRemovedWorkflowEvent")

    Events::WorkflowAddedSuccessorWorkflowEvent.all.each do |e|
      e.update!(data: {successor_workflow: e.data&.[](:successor_process)})
    end

    Events::AmbitionAddedWorkflowEvent.all.each do |e|
      e.update!(data: {added_workflow: e.data&.[](:added_process)})
    end

    Events::AmbitionCompletedWorkflowEvent.all.each do |e|
      e.update!(data: {completed_workflow: e.data&.[](:completed_process)})
    end

    Events::AmbitionRemovedWorkflowEvent.all.each do |e|
      e.update!(data: {removed_workflow: e.data&.[](:removed_process)})
    end
  end
end
