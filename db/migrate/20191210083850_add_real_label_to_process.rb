class AddRealLabelToProcess < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_processes, :label, :string, default: nil

    Workflow::Process.find_each do |process|
      process.send(:set_default_label)
      process.save!
    end

    add_index :workflow_processes, [:label, :data_entity_type], unique: true,
      name: "idx_workflow_processes_labels_uniq_per_data_entity"

    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end
