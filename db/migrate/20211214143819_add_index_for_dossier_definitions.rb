class AddIndexForDossierDefinitions < ActiveRecord::Migration[6.1]
  def change
    DossierDefinition.reindex
  end
end
