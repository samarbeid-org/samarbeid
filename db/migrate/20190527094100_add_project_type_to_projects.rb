class AddProjectTypeToProjects < ActiveRecord::Migration[5.2]
  def change
    add_reference :projects, :project_type, foreign_key: true

    if Project.any?
      pt = ProjectType.all.first
      if pt.nil?
        project_types_workflow_process_definitions = []
        Workflow::ProcessDefinition.all.each do |process_definition|
          project_types_workflow_process_definitions << ProjectTypesWorkflowProcessDefinition.new(workflow_process_definition: process_definition, count: (process_definition.key == "patent_application") ? nil : 1)
        end
        pt = ProjectType.create(name: "Standard Project Type", project_types_workflow_process_definitions: project_types_workflow_process_definitions)
      end

      Project.all.each do |project|
        project.update(project_type: pt)
      end
    end

    change_column_null :projects, :project_type_id, false
  end
end
