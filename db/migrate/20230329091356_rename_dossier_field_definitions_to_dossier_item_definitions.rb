class RenameDossierFieldDefinitionsToDossierItemDefinitions < ActiveRecord::Migration[6.1]
  def change
    rename_table :dossier_field_definitions, :dossier_item_definitions
  end
end
