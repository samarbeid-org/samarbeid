class RenameProcessLabelToTitle < ActiveRecord::Migration[6.0]
  def change
    rename_column :workflow_processes, :label, :title

    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end
