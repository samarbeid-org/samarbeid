class RefactorDossiersToUseJsonbForData < ActiveRecord::Migration[6.0]
  def up
    remove_column :dossiers, :data
    add_column :dossiers, :data, :jsonb, null: false, default: {}
    add_index :dossiers, :data, using: :gin
  end

  def down
    remove_column :dossiers, :data
    add_column :dossiers, :data, :text
  end
end
