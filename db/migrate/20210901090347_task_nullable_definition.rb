class TaskNullableDefinition < ActiveRecord::Migration[6.1]
  def change
    change_column_null(:tasks, :task_definition_id, true)
    change_column_default(:tasks, :task_definition_id, nil)
  end
end
