class AddEndedAtDateAndCancelUserToProcesses < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_processes, :ended_at, :datetime
    add_reference :workflow_processes, :cancel_user, foreign_key: {to_table: :users}
  end
end
