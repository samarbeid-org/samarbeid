class ReindexWorkflows < ActiveRecord::Migration[6.1]
  def change
    CustomElasticSearchConfig.reindex_model(Workflow)
  end
end
