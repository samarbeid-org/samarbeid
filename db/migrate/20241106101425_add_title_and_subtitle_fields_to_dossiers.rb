class AddTitleAndSubtitleFieldsToDossiers < ActiveRecord::Migration[7.0]
  def change
    add_column :dossiers, :title, :string
    add_column :dossiers, :subtitle, :string

    Dossier.find_each do |dossier|
      dossier.title = dossier.title_from_fields
      dossier.subtitle = dossier.subtitle_from_fields
      dossier.save!(touch: false)
    end
  end
end
