class RemoveUnusedNotiStuffFromUser < ActiveRecord::Migration[6.1]
  def up
    remove_column :users, :noti_last_sent_at
    remove_column :users, :noti_next_send_at
  end

  def down
    add_column :users, :noti_last_sent_at, :datetime, default: -> { "CURRENT_TIMESTAMP" }
    add_column :users, :noti_next_send_at, :datetime, default: -> { "CURRENT_TIMESTAMP" }
  end
end
