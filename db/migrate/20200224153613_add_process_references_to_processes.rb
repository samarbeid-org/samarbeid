class AddProcessReferencesToProcesses < ActiveRecord::Migration[6.0]
  def change
    add_reference :workflow_processes, :started_by_process, index: true, foreign_key: {to_table: :workflow_processes}
  end
end
