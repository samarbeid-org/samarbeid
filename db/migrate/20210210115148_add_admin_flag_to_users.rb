class AddAdminFlagToUsers < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :admin, :boolean, null: false, default: false

    admin_group = Group.find_by(name: "admin")
    admin_group&.users&.update_all(admin: true)
  end
end
