class RemoveProjectTypes < ActiveRecord::Migration[5.2]
  def change
    remove_column :ambitions, :project_type_id
    drop_table :project_types_workflow_process_definitions
    drop_table :project_types
  end
end
