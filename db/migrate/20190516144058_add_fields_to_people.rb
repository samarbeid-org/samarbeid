class AddFieldsToPeople < ActiveRecord::Migration[5.2]
  def change
    add_column :people, :email, :string
    add_column :people, :phone, :string
    add_column :people, :address, :text
    add_column :people, :organization, :string

    remove_index :people, column: :name, unique: true
    add_index :people, :name
    add_index :people, :email, unique: true

    Faker::Config.locale = :de

    Person.all.each do |person|
      person.update!(
        email: Faker::Internet.unique.email(person.name),
        phone: Faker::PhoneNumber.unique.phone_number_with_country_code,
        address: Faker::Address.full_address,
        organization: Faker::University.name
      )
    end

    Person.reindex
  end
end
