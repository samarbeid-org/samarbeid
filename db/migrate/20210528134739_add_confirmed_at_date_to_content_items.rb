class AddConfirmedAtDateToContentItems < ActiveRecord::Migration[6.0]
  def change
    add_column :content_items, :confirmed_at, :datetime
    ContentItem.confirmed.find_each do |content_item|
      completed_task = content_item.task_items.map(&:task).find(&:completed?)
      next unless completed_task
      content_item.update!(confirmed_at: completed_task.completed_at)
    end
  end
end
