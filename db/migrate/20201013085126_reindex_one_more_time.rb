class ReindexOneMoreTime < ActiveRecord::Migration[6.0]
  def up
    CustomElasticSearchConfig.initalize_searchkick_indexes
  end

  def down
  end
end
