class RefactorDossierDefinitionTitleAndSubtitleFieldsToUseIds < ActiveRecord::Migration[6.1]
  def change
    DossierDefinition.all.each do |dd|
      dd.title_fields.map! { |field_identifier| dd.fields.find_by!(identifier: field_identifier).id }
      dd.subtitle_fields.map! { |field_identifier| dd.fields.find_by!(identifier: field_identifier).id }
      dd.save!
    end
  end
end
