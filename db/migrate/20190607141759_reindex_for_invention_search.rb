class ReindexForInventionSearch < ActiveRecord::Migration[5.2]
  def change
    Invention.reindex
  end
end
