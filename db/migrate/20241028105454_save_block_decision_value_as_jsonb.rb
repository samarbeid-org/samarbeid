class SaveBlockDecisionValueAsJsonb < ActiveRecord::Migration[7.0]
  def change
    rename_column :block_definitions, :decision, :decision_old
    rename_column :blocks, :decision, :decision_old
    add_column :block_definitions, :decision, :jsonb
    add_column :blocks, :decision, :jsonb

    BlockDefinition.where.not(content_item_definition_id: nil).find_each do |block_definition|
      block_definition.decision = block_definition.decision_old.to_i.nonzero? || block_definition.decision_old
      block_definition.save!(touch: false)
    end

    Block.where.not(content_item_id: nil).find_each do |block|
      block.decision = block.decision_old.to_i.nonzero? || block.decision_old
      block.save!(touch: false)
    end

    remove_column :block_definitions, :decision_old
    remove_column :blocks, :decision_old
  end
end
