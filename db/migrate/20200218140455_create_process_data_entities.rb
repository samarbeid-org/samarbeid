class CreateProcessDataEntities < ActiveRecord::Migration[6.0]
  def change
    rename_table :abuse_reports, :process_data_entities
    add_column :process_data_entities, :type, :string
    add_column :process_data_entities, :data, :jsonb, default: {}

    ProcessDataEntity.all.each do |report|
      report.type = "AbuseReport"
      report.data = {
        email_sender: report.email_sender,
        email_content: report.email_content,
        ad_url: report.ad_url,
        ad_content: report.ad_content,
        should_be_deleted: report.should_be_deleted,
        reason_for_decision: report.reason_for_decision
      }
      report.save(touch: false)
    end

    remove_columns :process_data_entities, :email_sender, :email_content, :ad_url, :ad_content, :should_be_deleted, :reason_for_decision

    Workflow::Process.where(data_entity_type: "AbuseReport").update_all(data_entity_type: "ProcessDataEntity")

    # The following are not really relevant for AbuseReport as it is not using those associations.
    # They're here to remind us that other classes might have these migrated.
    # However, those might not be updated in-place but moved to another table and thus also need their IDs updated.
    PersonAssociation.where(data_entity_type: "AbuseReport").update_all(data_entity_type: "ProcessDataEntity")
    UploadedFile.where(data_entity_type: "AbuseReport").update_all(data_entity_type: "ProcessDataEntity")
  end
end
