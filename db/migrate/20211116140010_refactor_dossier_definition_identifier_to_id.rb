class RefactorDossierDefinitionIdentifierToId < ActiveRecord::Migration[6.1]
  def change
    DossierFieldDefinition.where(content_type: ContentTypes::Dossier.type).each do |item|
      item.options["type"] = DossierDefinition.find_by!(identifier: item.options["type"]).id
      item.save!
    end

    ContentItemDefinition.where(content_type: ContentTypes::Dossier.type).each do |item|
      item.options["type"] = DossierDefinition.find_by!(identifier: item.options["type"]).id
      item.save!
    end

    ContentItem.where(content_type: ContentTypes::Dossier.type).each do |item|
      item.options["type"] = DossierDefinition.find_by!(identifier: item.options["type"]).id
      item.save!
    end
  end
end
