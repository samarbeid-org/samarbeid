class AddReferenceFieldToUploadedFiles < ActiveRecord::Migration[5.2]
  def change
    add_column :uploaded_files, :data_entity_field, :string
    add_index :uploaded_files, :data_entity_field
  end
end
