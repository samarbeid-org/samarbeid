class CreateProjectTypesWorkflowProcessDefinitions < ActiveRecord::Migration[5.2]
  def change
    create_table :project_types_workflow_process_definitions do |t|
      t.bigint :project_type_id, null: false
      t.bigint :workflow_process_definition_id, null: false
      t.integer :count

      t.timestamps
    end

    add_index :project_types_workflow_process_definitions, [:project_type_id, :workflow_process_definition_id], unique: true, name: "index_project_types_workflow_process_definitions_on_pt_and_wpd"
  end
end
