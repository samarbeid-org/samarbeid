class AddDeletedFlagToMostDefinitions < ActiveRecord::Migration[6.1]
  def change
    [:workflow_definitions, :task_definitions, :content_item_definitions, :task_item_definitions, :block_definitions].each do |definition_table|
      add_column definition_table, :deleted, :boolean, default: false, null: false
    end
  end
end
