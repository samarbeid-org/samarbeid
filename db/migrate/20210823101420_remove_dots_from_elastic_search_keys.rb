class RemoveDotsFromElasticSearchKeys < ActiveRecord::Migration[6.0]
  def change
    [ContentItem.where("label like ?", "%.%"), ContentItemDefinition.where("label like ?", "%.%"), DossierFieldDefinition.where("label like ?", "%.%")].each do |labels_with_dot|
      labels_with_dot.find_each do |label_with_dot|
        label_with_dot.update(label: label_with_dot.label.delete("."))
        label_with_dot.update(label: "punkte sind nicht erlaubt #{SecureRandom.hex}") unless label_with_dot.valid? # Just in case we have something with only dots
      end
    end

    RunFullReindexJob.perform_later
  end
end
