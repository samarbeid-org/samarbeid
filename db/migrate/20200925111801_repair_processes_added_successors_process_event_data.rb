class RepairProcessesAddedSuccessorsProcessEventData < ActiveRecord::Migration[6.0]
  def change
    Events::ProcessAddedSuccessorProcessEvent.all.each do |event|
      tmp = event.object
      event.object = event.predecessor_process
      event.data = {successor_process: tmp}
      event.save!
    end
  end
end

class Events::ProcessAddedSuccessorProcessEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def process
    object
  end

  def predecessor_process
    data&.[](:predecessor_process)
  end

  private

  def validate_event_type_values
    errors.add(:object, "should be a process") unless process.is_a?(Workflow::Process)
  end
end
