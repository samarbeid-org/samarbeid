class ChangeNewsUrlForNewWebsite < ActiveRecord::Migration[6.1]
  def change
    News.all.each do |item|
      item.url = item.url.sub("https://www.samarbeid.org/blog/", "https://www.samarbeid.org/")
      item.url = item.url + "/" unless item.url.last === "/"
      item.updated_at = DateTime.new(2022, 1, 1)
      item.save!(touch: false)
    end
  end
end
