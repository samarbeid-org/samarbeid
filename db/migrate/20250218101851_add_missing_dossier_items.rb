class AddMissingDossierItems < ActiveRecord::Migration[7.0]
  def change
    Dossier.all.find_each do |dossier|
      dossier.definition.items.find_each do |dossier_item_definition|
        unless dossier.dossier_items.where(dossier_item_definition: dossier_item_definition).exists?
          DossierItem.create!(dossier: dossier, dossier_item_definition: dossier_item_definition, value: dossier_item_definition.default_value)
        end
      end
    end
  end
end
