class AddDescriptionToWorkflowTasks < ActiveRecord::Migration[6.0]
  def change
    add_column :workflow_tasks, :description, :text
  end
end
