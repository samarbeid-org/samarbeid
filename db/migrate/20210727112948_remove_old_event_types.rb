class RemoveOldEventTypes < ActiveRecord::Migration[6.0]
  def change
    Notification.joins(:event).where("events.type": "Events::ChangedVisibilityEvent").delete_all
    Event.where(type: "Events::ChangedVisibilityEvent").delete_all

    Notification.joins(:event).where("events.type": "Events::AddedContributorEvent").delete_all
    Event.where(type: "Events::AddedContributorEvent").delete_all

    Notification.joins(:event).where("events.type": "Events::RemovedContributorEvent").delete_all
    Event.where(type: "Events::RemovedContributorEvent").delete_all
  end
end
