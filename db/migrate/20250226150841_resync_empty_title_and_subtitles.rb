class ResyncEmptyTitleAndSubtitles < ActiveRecord::Migration[7.0]
  def change
    Dossier.where(title: nil).or(Dossier.where(title: ""))
      .or(Dossier.where(subtitle: nil)).or(Dossier.where(subtitle: "")).find_each do |dossier|
      dossier.sync_title_from_fields
    end

    WorkflowDefinition.where(auto_generate_title: true).find_each do |workflow_definition|
      workflow_definition.workflows.where(title: nil).or(workflow_definition.workflows.where(title: "")).find_each do |workflow|
        workflow.sync_title_from_fields
      end
    end
  end
end
