class AddNotifyToWebhook < ActiveRecord::Migration[7.0]
  def change
    add_column :webhooks, :notify_after_execution, :boolean, default: true
    add_column :webhook_definitions, :notify_after_execution, :boolean, default: true
  end
end
