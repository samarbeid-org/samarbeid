class CreateWebhooks < ActiveRecord::Migration[7.0]
  def change
    create_table :webhooks do |t|
      t.string :url
      t.string :auth_token
      t.boolean :wait_for_response, default: true
      t.references :task, null: false, foreign_key: true
      t.references :webhook_definition, foreign_key: true
      t.integer :response_code
      t.text :response_body

      t.timestamps
    end
  end
end
