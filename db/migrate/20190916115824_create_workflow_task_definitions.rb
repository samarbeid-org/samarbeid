class CreateWorkflowTaskDefinitions < ActiveRecord::Migration[5.2]
  def change
    create_table :workflow_task_definitions do |t|
      t.string :key, null: false
      t.string :name, null: false
      t.text :description

      t.references :workflow_process_definition, index: {name: :idx_workflow_task_definitions_on_workflow_process_definition_id}, foreign_key: true, null: false

      t.timestamps
    end

    add_index :workflow_task_definitions, [:key, :workflow_process_definition_id], unique: true, name: :idx_wf_task_defs_on_camunda_id_and_wf_process_definition_id
  end
end
