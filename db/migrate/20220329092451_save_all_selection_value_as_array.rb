class SaveAllSelectionValueAsArray < ActiveRecord::Migration[6.1]
  def change
    ContentItem.where(content_type: ContentTypes::Selection.type).each do |content_item|
      old_value = content_item.value
      new_value = Array(old_value)
      if old_value != new_value
        content_item.value = new_value
        content_item.save!
      end
    end

    DossierFieldDefinition.where(content_type: ContentTypes::Selection.type).each do |dossier_field_definition|
      dossier_field_definition.definition.dossiers.each do |dossier|
        old_value = dossier.get_field_value(dossier_field_definition.id)
        new_value = Array(old_value)
        if old_value != new_value
          dossier.set_field_value(dossier_field_definition.id, new_value)
          dossier.save!
        end
      end
    end
  end
end
