class AddOptimisticLockingToDossierAndContentItem < ActiveRecord::Migration[6.1]
  def change
    add_column :dossiers, :lock_version, :bigint, default: 0, null: false
    add_column :content_items, :lock_version, :bigint, default: 0, null: false
    add_column :tasks, :lock_version, :bigint, default: 0, null: false
  end
end
