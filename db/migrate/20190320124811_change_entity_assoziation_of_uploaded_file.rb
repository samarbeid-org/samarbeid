class ChangeEntityAssoziationOfUploadedFile < ActiveRecord::Migration[5.2]
  def change
    add_reference :uploaded_files, :data_entity, polymorphic: true, index: true
    UploadedFile.all.find_each do |upload|
      upload.update(data_entity: upload.invention)
    end
    remove_column :uploaded_files, :invention_id
  end
end

class UploadedFile < ApplicationRecord
  belongs_to :invention
  belongs_to :data_entity, polymorphic: true
end
