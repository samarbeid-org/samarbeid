class CreateJoinTableInventorsInventionsPersons < ActiveRecord::Migration[5.2]
  def change
    create_join_table :inventions, :persons, table_name: :inventors_inventions_persons do |t|
      t.index :invention_id
      t.index :person_id
      t.index [:invention_id, :person_id], unique: true, name: :index_inventors_inventions_persons_on_i_id_and_p_id
    end
  end
end
