class AddKeyToTask < ActiveRecord::Migration[5.2]
  def change
    Workflow::Task.destroy_all
    add_column :workflow_tasks, :key, :string, null: false
  end
end
