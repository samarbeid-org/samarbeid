class AddGeneseReferenceToPatentApplication < ActiveRecord::Migration[5.2]
  def change
    add_column :patent_applications, :reference, :string
  end
end
