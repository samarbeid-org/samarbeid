class CombineEventsWithSameAction < ActiveRecord::Migration[6.0]
  def change
    Events::TaskCanceledEvent.destroy_all

    Event.where(type: "Events::AmbitionAddedContributorEvent").update_all(type: "Events::AddedContributorEvent")
    Event.where(type: "Events::TaskAddedContributorEvent").update_all(type: "Events::AddedContributorEvent")
    Event.where(type: "Events::WorkflowAddedContributorEvent").update_all(type: "Events::AddedContributorEvent")

    Event.where(type: "Events::AmbitionRemovedContributorEvent").update_all(type: "Events::RemovedContributorEvent")
    Event.where(type: "Events::TaskRemovedContributorEvent").update_all(type: "Events::RemovedContributorEvent")
    Event.where(type: "Events::WorkflowRemovedContributorEvent").update_all(type: "Events::RemovedContributorEvent")

    Event.where(type: "Events::AmbitionAssignedEvent").update_all(type: "Events::AssignedEvent")
    Event.where(type: "Events::TaskAssignedEvent").update_all(type: "Events::AssignedEvent")
    Event.where(type: "Events::WorkflowAssignedEvent").update_all(type: "Events::AssignedEvent")

    Event.where(type: "Events::AmbitionUnassignedEvent").update_all(type: "Events::UnassignedEvent")
    Event.where(type: "Events::TaskUnassignedEvent").update_all(type: "Events::UnassignedEvent")
    Event.where(type: "Events::WorkflowUnassignedEvent").update_all(type: "Events::UnassignedEvent")

    Event.where(type: "Events::AmbitionChangedSummaryEvent").update_all(type: "Events::ChangedSummaryEvent")
    Event.where(type: "Events::TaskChangedSummaryEvent").update_all(type: "Events::ChangedSummaryEvent")
    Event.where(type: "Events::WorkflowChangedSummaryEvent").update_all(type: "Events::ChangedSummaryEvent")

    Event.where(type: "Events::AmbitionChangedTitleEvent").update_all(type: "Events::ChangedTitleEvent")
    Event.where(type: "Events::WorkflowChangedTitleEvent").update_all(type: "Events::ChangedTitleEvent")

    Event.where(type: "Events::AmbitionChangedVisibilityEvent").update_all(type: "Events::ChangedVisibilityEvent")
    Event.where(type: "Events::WorkflowChangedVisibilityEvent").update_all(type: "Events::ChangedVisibilityEvent")

    Event.where(type: "Events::AmbitionAddedWorkflowEvent").update_all(type: "Events::AddedWorkflowEvent")

    Event.where(type: "Events::AmbitionRemovedWorkflowEvent").update_all(type: "Events::RemovedWorkflowEvent")

    Event.where(type: "Events::AmbitionCompletedWorkflowEvent").update_all(type: "Events::CompletedWorkflowEvent")

    Event.where(type: "Events::TaskChangedDueDateEvent").update_all(type: "Events::ChangedDueDateEvent")

    Event.where(type: "Events::WorkflowAddedSuccessorWorkflowEvent").update_all(type: "Events::AddedSuccessorWorkflowEvent")

    Event.where(type: "Events::AmbitionCompletedEvent").update_all(type: "Events::CompletedEvent")
    Event.where(type: "Events::TaskCompletedEvent").update_all(type: "Events::CompletedEvent")
    Event.where(type: "Events::WorkflowCompletedEvent").update_all(type: "Events::CompletedEvent")

    Event.where(type: "Events::AmbitionCreatedEvent").update_all(type: "Events::CreatedEvent")
    Event.where(type: "Events::TaskCreatedEvent").update_all(type: "Events::CreatedEvent")

    Event.where(type: "Events::TaskStartedEvent").update_all(type: "Events::StartedEvent")
    Event.where(type: "Events::WorkflowStartedEvent").update_all(type: "Events::StartedEvent")

    Event.where(type: "Events::AmbitionDeletedEvent").update_all(type: "Events::DeletedEvent")

    Event.where(type: "Events::AmbitionReopenedEvent").update_all(type: "Events::ReopenedEvent")

    Event.where(type: "Events::CommentCreatedEvent").update_all(type: "Events::CommentedEvent")
  end
end

class Events::TaskCanceledEvent < Event; end
