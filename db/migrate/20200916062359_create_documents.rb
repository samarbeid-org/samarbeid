class CreateDocuments < ActiveRecord::Migration[6.0]
  def change
    create_table :documents do |t|
      t.string :title, null: false
      t.string :type, null: false
      t.references :data_entity, null: false, index: false, foreign_key: {to_table: :process_data_entities}
      t.string :data_entity_field, null: false
      t.string :link_url
      t.text :file_as_text

      t.timestamps
    end

    add_index :documents, [:data_entity_id, :data_entity_field], name: :idx_documents_on_data_entity_id_and_field
  end
end
