class RenameStatusUpdatedAtOfAmbtions < ActiveRecord::Migration[6.0]
  def change
    rename_column :ambitions, :status_updated_at, :state_updated_at
  end
end
