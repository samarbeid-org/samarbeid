class MakeBlockParallelRequired < ActiveRecord::Migration[6.1]
  def change
    change_column_null :blocks, :parallel, false, false
    change_column_default :blocks, :parallel, false
    change_column_null :block_definitions, :parallel, false, false
    change_column_default :block_definitions, :parallel, false
  end
end
