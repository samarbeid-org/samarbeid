class LinkContentItemToBlock < ActiveRecord::Migration[6.0]
  def change
    add_reference :block_definitions, :content_item_definition
    add_reference :blocks, :content_item

    remove_column :block_definitions, :decision_option, :string
    remove_column :blocks, :decision_option, :string
  end
end
