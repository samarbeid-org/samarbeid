class CreateTaskItemDefinitions < ActiveRecord::Migration[6.0]
  def change
    create_table :task_item_definitions do |t|
      t.references :task_definition, null: false, foreign_key: true
      t.references :content_item_definition, null: false, foreign_key: true
      t.integer :position, null: false
      t.boolean :required
      t.boolean :info_box

      t.timestamps
    end

    add_reference :task_items, :task_item_definition
  end
end
