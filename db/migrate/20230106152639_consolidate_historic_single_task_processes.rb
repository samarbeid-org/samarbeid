class ConsolidateHistoricSingleTaskProcesses < ActiveRecord::Migration[6.1]
  def change
    historic_single_task_def = WorkflowDefinition.find_by(name: "Einzelne Aufgabe")
    if historic_single_task_def&.workflows&.any?
      workflow_single_task_def = WorkflowDefinition.system_process_definition_single_task
      workflow_single_task_def.update!(workflows: historic_single_task_def.workflows + workflow_single_task_def.workflows)

      historic_single_task_def.reload.destroy!
    end
  end
end
