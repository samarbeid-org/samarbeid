class ChangeDefaultMailInterval < ActiveRecord::Migration[6.1]
  def change
    change_column_default(:users, :noti_interval, from: 86400, to: 1)
  end
end
