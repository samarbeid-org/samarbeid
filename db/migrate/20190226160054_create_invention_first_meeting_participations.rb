class CreateInventionFirstMeetingParticipations < ActiveRecord::Migration[5.2]
  def change
    create_table :invention_first_meeting_participations do |t|
      t.references :person, foreign_key: true
      t.references :invention, foreign_key: true

      t.timestamps
    end
    FirstMeetingInventionsPerson.all.each do |fmip|
      InventionFirstMeetingParticipation.create(person_id: fmip.person_id, invention_id: fmip.invention_id)
    end

    drop_table :first_meeting_inventions_persons
  end
end

class FirstMeetingInventionsPerson < ApplicationRecord
  self.table_name = "first_meeting_inventions_persons"
end
