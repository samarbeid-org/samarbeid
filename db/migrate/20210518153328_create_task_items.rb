class CreateTaskItems < ActiveRecord::Migration[6.0]
  def change
    create_table :task_items do |t|
      t.references :task, null: false, foreign_key: true
      t.references :content_item, null: false, foreign_key: true
      t.integer :position, null: false
      t.boolean :required
      t.boolean :info_box

      t.timestamps
    end
  end
end
