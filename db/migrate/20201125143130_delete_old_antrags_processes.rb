class DeleteOldAntragsProcesses < ActiveRecord::Migration[6.0]
  def change
    ex_def = Workflow::ProcessDefinition.find_by_key!("fundraising")
    ex_def.destroy!

    CamundaInitializer.new.start_and_init_camunda_engine
  end
end
