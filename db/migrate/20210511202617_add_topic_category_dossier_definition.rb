class AddTopicCategoryDossierDefinition < ActiveRecord::Migration[6.0]
  def change
    dossier_definition = DossierDefinition.create(identifier: "topic_category", label: "Politische Themenfelder")
    DossierFieldDefinition.create(definition: dossier_definition, identifier: "name", label: "Name",
      position: 10, content_type: "string",
      required: true, recommended: false, unique: true)
    dossier_definition.update(title_fields: ["name"])
  end
end
