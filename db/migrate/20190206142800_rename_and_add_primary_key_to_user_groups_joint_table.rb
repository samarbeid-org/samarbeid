class RenameAndAddPrimaryKeyToUserGroupsJointTable < ActiveRecord::Migration[5.2]
  def change
    rename_table :groups_users, :user_groups
    add_column :user_groups, :id, :primary_key
  end
end
