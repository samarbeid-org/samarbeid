class AddHighResSrcToRichtextImages < ActiveRecord::Migration[7.0]
  def change
    ContentItem.where(content_type: ContentTypes::Richtext.to_s).where.not(value: nil).find_each do |item|
      new_value = Services::RichtextImageStorage.update_high_res_img_data(item.value)
      item.update_column(:value, new_value) if item.value != new_value
    end

    DossierItem.joins(:dossier_item_definition).where(dossier_item_definition: {content_type: ContentTypes::Richtext.to_s}).where.not(value: nil).find_each do |item|
      new_value = Services::RichtextImageStorage.update_high_res_img_data(item.value)
      item.update_column(:value, new_value) if item.value != new_value
    end

    Comment.where.not(message: nil).find_each do |comment|
      new_message = Services::RichtextImageStorage.update_high_res_img_data(comment.message)
      comment.update_column(:message, new_message) if comment.message != new_message
    end
  end
end

class Services::RichtextImageStorage
  def self.update_high_res_img_data(value)
    iterate_active_storage_img_elements(value) do |element|
      unless element["data-high-res-src"].present?
        signed_blob_id = signed_blob_id_from_url(element["src"])
        next unless signed_blob_id

        image_blob = ActiveStorage::Blob.find_signed(signed_blob_id)
        element["data-high-res-src"] = Rails.application.routes.url_helpers.rails_blob_path(image_blob, only_path: true) unless image_blob.nil?
      end
    end
  end
end
