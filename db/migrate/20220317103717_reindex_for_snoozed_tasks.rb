class ReindexForSnoozedTasks < ActiveRecord::Migration[6.1]
  def change
    ReindexSearchableJob.perform_later(Task)
  end
end
