class RemoveCamundaLinkingIDs < ActiveRecord::Migration[6.0]
  def change
    remove_column :groups, :camunda_identifier
    remove_column :users, :camunda_identifier
    remove_column :users, :camunda_token
    remove_column :workflow_processes, :camunda_id
    remove_column :workflow_tasks, :camunda_id
  end
end
