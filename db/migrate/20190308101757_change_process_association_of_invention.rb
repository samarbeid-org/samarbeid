class ChangeProcessAssociationOfInvention < ActiveRecord::Migration[5.2]
  def change
    remove_column :inventions, :process_instance_id
    add_reference :inventions, :workflow_process, foreign_key: true
  end
end
