class ChangeContentTypeSelectionValueToInteger < ActiveRecord::Migration[6.1]
  def change
    DossierFieldDefinition.where(content_type: ContentTypes::Selection.to_s).each do |dfd|
      translation_table = set_auto_increment_values(dfd.options["items"])
      dfd.save!

      Dossier.where(definition: dfd.definition).each do |dossier|
        if dossier.data[dfd.identifier]
          dossier.data[dfd.identifier] = Array(dossier.data[dfd.identifier]).map { |value| translation_table[value] }
          dossier.save!
        end
      end
    end

    ContentItemDefinition.where(content_type: ContentTypes::Selection.to_s).each do |cid|
      set_auto_increment_values(cid.options["items"])
      cid.save!
    end

    ContentItem.where(content_type: ContentTypes::Selection.to_s).each do |ci|
      translation_table = set_auto_increment_values(ci.options["items"])

      if ci.value
        ci.value = Array(ci.value).map { |value| translation_table[value] }
      end

      ci.save!
    end
  end

  private

  def set_auto_increment_values(items)
    translation_table = {}
    index = 1

    items.each do |item|
      translation_table[item["value"]] = index
      item["value"] = index
      index += 1
    end

    translation_table
  end
end
