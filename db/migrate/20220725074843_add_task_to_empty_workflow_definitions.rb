class AddTaskToEmptyWorkflowDefinitions < ActiveRecord::Migration[6.1]
  def change
    WorkflowDefinition.all.select { |wd| wd.all_task_definitions.count == 0 }.each do |wd_without_task|
      Interactors::WorkflowDefinitionInteractor.add_default_task_definition(wd_without_task)
    end
  end
end
