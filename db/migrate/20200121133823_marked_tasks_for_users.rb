class MarkedTasksForUsers < ActiveRecord::Migration[5.2]
  def change
    remove_column :workflow_tasks, :marked

    create_join_table :workflow_tasks, :users, table_name: "users_workflow_tasks_marked" do |t|
      t.index :user_id
      t.index :workflow_task_id
      t.index [:user_id, :workflow_task_id], unique: true, name: "idx_users_workflow_tasks_marked_on_user_id_and_workflow_task_id"
    end
  end
end
