class RemoveCreatedByFromDossiers < ActiveRecord::Migration[6.1]
  def change
    remove_column :dossiers, :created_by_id
  end
end
