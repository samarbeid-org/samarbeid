class AddSupersederToEvent < ActiveRecord::Migration[6.1]
  def change
    rename_column :notifications, :marked_at, :done_at

    add_reference :events, :superseder, index: true

    # find_each is using order by PK asc by default which is exactly what we want.
    # If you want to do it manually with .all.each be sure to order by :id.
    Event.find_each(&:apply_supersedees)

    # Thanks to the callback mark_notifications_as_done this should be 0
    Notification.undone.unbookmarked.joins(:event).where("events.superseder_id is not null").update_all(done_at: Time.now)
  end
end
