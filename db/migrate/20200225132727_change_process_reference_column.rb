class ChangeProcessReferenceColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :workflow_processes, :started_by_process_id, :parent_process_id
  end
end
