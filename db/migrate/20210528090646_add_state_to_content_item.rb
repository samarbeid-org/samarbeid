class AddStateToContentItem < ActiveRecord::Migration[6.0]
  def change
    add_column :content_items, :aasm_state, :string
    ContentItem.update_all(aasm_state: "undefined")
    ContentItem.all.find_each do |content_item|
      next if content_item.confirmed?
      content_item.confirm! if content_item.task_items.map(&:task).any? { |task| task.completed? }
    end
    change_column_null :content_items, :aasm_state, false
  end
end
