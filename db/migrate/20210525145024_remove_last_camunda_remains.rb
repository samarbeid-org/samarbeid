class RemoveLastCamundaRemains < ActiveRecord::Migration[6.0]
  def change
    remove_column :workflow_definitions, :key, :string
    remove_column :task_definitions, :key, :string
  end
end
