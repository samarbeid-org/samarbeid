class FixSomeWorkflowStates < ActiveRecord::Migration[6.0]
  def change
    remove_column :workflows, :cancel_info
    remove_column :workflows, :cancel_user_id

    Notification.where(event_id: Event.where(type: "Events::WorkflowCanceledEvent")).delete_all
    Event.where(type: "Events::WorkflowCanceledEvent").delete_all

    Workflow.where.not(assignee: nil).where(aasm_state: "ready").update_all(aasm_state: "active") # workflows with assignees are active
    exit_code = ExitCode.find_or_create_by!(label: "nicht umsetzbar")
    WorkflowDefinition.find_each { |w_def| w_def.exit_codes << exit_code }
    Workflow.reindex
    Task.reindex # we changed tab_categories
  end
end
