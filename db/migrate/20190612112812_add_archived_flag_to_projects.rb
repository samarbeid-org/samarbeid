class AddArchivedFlagToProjects < ActiveRecord::Migration[5.2]
  def change
    add_column :projects, :archived, :boolean, null: false, default: false
  end
end
