class RenameSingleTaskAttachmentField < ActiveRecord::Migration[6.1]
  def change
    content_item_definition_attachment = WorkflowDefinition.system_process_definition_single_task.content_item_definitions.find_by!(content_type: ContentTypes::File.to_s)
    content_item_definition_attachment.update!(label: I18n.t("system_process_definition_single_task.task.items.attachments"))
  end
end
