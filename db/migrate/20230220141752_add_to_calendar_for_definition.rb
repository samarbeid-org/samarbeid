class AddToCalendarForDefinition < ActiveRecord::Migration[6.1]
  def change
    add_column :task_item_definitions, :add_to_calendar, :boolean, default: nil
  end
end
