class AddDescriptionToTask < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_tasks, :description, :text
  end
end
