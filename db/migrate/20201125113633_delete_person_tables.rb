class DeletePersonTables < ActiveRecord::Migration[6.0]
  def change
    drop_table :person_associations
    drop_table :people
  end
end
