class SkipAllOpenTasksInCompletedWorkflows < ActiveRecord::Migration[6.1]
  def change
    add_column :tasks, :old_aasm_state, :string

    Workflow.where(aasm_state: :completed).find_each do |completed_workflow|
      next if completed_workflow.completable? # new "completed" state
      completed_workflow.all_tasks.select(&:open?).each do |open_task|
        open_task.update!(old_aasm_state: open_task.aasm_state, aasm_state: :skipped)
      end
    end
    Workflow.all.find_each { |workflow| workflow.align_state }

    RunFullReindexJob.perform_later # just to be sure
  end
end
