class RemoveUnusedSeedData < ActiveRecord::Migration[6.1]
  def change
    default_admin = User.find_by(email: "admin@example.org")
    seed_group = Group.find_by(name: "Hallo-Welt-Gruppe")

    # Destroy seed group is not changed
    if seed_group && (seed_group.users.none? || seed_group.users == [default_admin])
      puts "Destroying seed group"
      seed_group.destroy!
    end

    # Destroy default dossiers if not used by anybody
    default_dossier_definition = DossierDefinition.find_by(name: "Planet")
    if default_dossier_definition&.groups&.none?
      puts "Destroying #{default_dossier_definition} instances"
      default_dossier_definition.dossiers.find_each { |dossier| dossier.destroy! }
    end

    # Destroy default processes if not used by anybody
    default_workflow_definition = WorkflowDefinition.find_by(name: "Hallo Welt")
    if default_workflow_definition&.groups&.none?
      puts "Destroying #{default_workflow_definition} and instances"
      default_workflow_definition.workflows.find_each { |workflow| workflow.destroy! }
      default_workflow_definition.destroy!
    end

    # Destroy default dossier definition now as before it may have been used in above workflow
    if default_dossier_definition&.groups&.none?
      puts "Destroying #{default_dossier_definition}"
      default_dossier_definition.destroy!
    end

    # Destroy default ambition in any case (as not used anymore)
    default_ambition = Ambition.find_by(title: "Jedem Planeten Hallo sagen")
    if default_ambition
      puts "Destroying #{default_ambition}"
      default_ambition.destroy!
    end
  end
end
