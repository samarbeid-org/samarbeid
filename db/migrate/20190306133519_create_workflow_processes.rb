class CreateWorkflowProcesses < ActiveRecord::Migration[5.2]
  def change
    create_table :workflow_processes do |t|
      t.string :camunda_id, null: false
      t.string :name, null: false

      t.timestamps
    end
  end
end
