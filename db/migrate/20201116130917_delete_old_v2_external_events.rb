class DeleteOldV2ExternalEvents < ActiveRecord::Migration[6.0]
  def change
    Workflow::ProcessDefinition.find_by(key: "external_event").processes.each(&:destroy)
  end
end
