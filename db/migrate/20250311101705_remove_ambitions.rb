class RemoveAmbitions < ActiveRecord::Migration[7.0]
  def change
    Event.where(object_type: "Ambition")
      .or(Event.where(type: "Events::AddedWorkflowEvent"))
      .or(Event.where(type: "Events::CompletedWorkflowEvent"))
      .or(Event.where(type: "Events::RemovedWorkflowEvent"))
      .find_each do |event|
      event.destroy!
    end

    Comment.where(object_type: "Ambition").find_each do |comment|
      comment.destroy!
    end

    Contribution.where(contributable_type: "Ambition").find_each do |contribution|
      contribution.destroy!
    end

    drop_table :ambitions_workflows
    drop_table :ambitions
  end
end

class Events::AddedWorkflowEvent < Event
end

class Events::CompletedWorkflowEvent < Event
end

class Events::RemovedWorkflowEvent < Event
end
