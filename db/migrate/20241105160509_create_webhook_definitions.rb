class CreateWebhookDefinitions < ActiveRecord::Migration[7.0]
  def change
    create_table :webhook_definitions do |t|
      t.string :url
      t.string :auth_token
      t.boolean :wait_for_response, default: true
      t.references :task_definition, null: false, foreign_key: true

      t.timestamps
    end
  end
end
