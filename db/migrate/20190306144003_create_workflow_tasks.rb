class CreateWorkflowTasks < ActiveRecord::Migration[5.2]
  def change
    create_table :workflow_tasks do |t|
      t.string :camunda_id, null: false
      t.string :name, null: false
      t.string :state, null: false
      t.date :due_at
      t.references :workflow_process, foreign_key: true, null: false
      t.references :assignee, foreign_key: {to_table: :users}

      t.timestamps
    end
  end
end
