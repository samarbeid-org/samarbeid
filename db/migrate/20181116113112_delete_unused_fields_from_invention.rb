class DeleteUnusedFieldsFromInvention < ActiveRecord::Migration[5.2]
  def change
    remove_column :inventions, :report
    remove_column :inventions, :patent_application
  end
end
