class RenameSystemProcessDefinitionSingleTask < ActiveRecord::Migration[6.1]
  def change
    WorkflowDefinition.system_process_definition_single_task.update!(name: I18n.t("system_process_definition_single_task.name"))
  end
end
