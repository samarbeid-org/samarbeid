class SetMainMenuAutoAddDeactivatedSetting < ActiveRecord::Migration[6.1]
  def change
    Setting.main_menu_auto_add_deactivated = true
  end
end
