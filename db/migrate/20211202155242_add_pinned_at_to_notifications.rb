class AddPinnedAtToNotifications < ActiveRecord::Migration[6.1]
  def change
    add_column :notifications, :bookmarked_at, :datetime
  end
end
