class RemainingBitsAndPieces < ActiveRecord::Migration[6.0]
  def change
    rename_column :workflows, :predecessor_process_id, :predecessor_workflow_id
    rename_column :blocks, :process_id, :workflow_id
    rename_column :tasks, :workflow_task_definition_id, :task_definition_id
  end
end
