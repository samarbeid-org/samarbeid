class RemoveLabelPrefixFromWorkflowDefinitions < ActiveRecord::Migration[6.1]
  def change
    remove_column :workflow_definitions, :label_prefix
  end
end
