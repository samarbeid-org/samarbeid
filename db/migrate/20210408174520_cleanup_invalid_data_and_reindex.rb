class CleanupInvalidDataAndReindex < ActiveRecord::Migration[6.0]
  def change
    Event.destroy_invalid
    Document.destroy_invalid

    CustomElasticSearchConfig.initalize_searchkick_indexes
  end
end
