class MigrateDataEntitiesToContentItems < ActiveRecord::Migration[6.0]
  def up
    return unless defined?(ProcessDataEntity)
    admin_user = User.where(admin: true).first!

    begin
      wd = WorkflowDefinition.find_by!(key: "external_requirements")
      wd.model["tasks"]["arrange_meeting_nuc"]["fields"].find { |f| f["id"] == "firs_meeting_notes_documents" }["label"] = "Gesprächsnotizen (Dateien)"
      wd.save!
      td = wd.task_definitions.find { |t| t.key == "arrange_meeting_nuc" }
      td.tasks.destroy_all
    rescue => error
      puts "changing definitions did not work: #{error.inspect}"
    end

    WorkflowDefinition.all.find_each do |workflow_definition|
      new_workflow = Workflow.new(workflow_definition: workflow_definition, data_entity: workflow_definition.data_entity_class.new)
      workflow_definition.definition_tasks.each { |def_task| new_workflow.direct_tasks << def_task.build_task(new_workflow) }
      new_workflow.save!
      new_workflow.direct_tasks.each do |task|
        data_attributes = task.data_attributes(admin_user)
        data_attributes.each_with_index do |data_attribute, index|
          label = data_attribute[:label]
          label = "Vorher kein Label vorhanden #{index}" if label.blank?
          content_item_definition = ContentItemDefinition.find_or_initialize_by(label: label, workflow_definition: workflow_definition)
          type = data_attribute[:type]
          case type
          when "has_many_dossiers"
            content_item_definition.content_type = "dossier"
            content_item_definition.options = {dossier_type: data_attribute[:parameter]["type"], multiple: true}
          when "has_one_dossier"
            content_item_definition.content_type = "dossier"
            content_item_definition.options = {dossier_type: data_attribute[:parameter]["type"], multiple: false}
          when "has_many_files"
            content_item_definition.content_type = "document"
          when "info"
            content_type = data_attribute[:parameter][:valueType]
            content_type = "document" if content_type.match?("_file")
            content_type = "dossier" if content_type.match?("_dossier")
            content_item_definition.content_type = content_type
            TaskItemDefinition.create!(task_definition: task.task_definition, content_item_definition: content_item_definition, position: index, info_box: true)
          when "has_many_workflows"
            next # Do nothing for now
          else # "date", "text","string", "datetime", "boolean"
            content_item_definition.content_type = type
          end
          content_item_definition.default_value ||= data_attribute[:value]
          content_item_definition.save!
          TaskItemDefinition.find_or_create_by!(task_definition: task.task_definition, content_item_definition: content_item_definition, position: index)
        end
      end
      new_workflow.direct_tasks.each(&:delete)
      new_workflow.data_entity.delete
      new_workflow.delete
    end

    Task.all.find_each do |task|
      data_attributes = task.data_attributes(admin_user)
      data_attributes.each_with_index do |data_attribute, index|
        label = data_attribute[:label]
        label = "Vorher kein Label vorhanden #{index}" if label.blank?
        content_item = ContentItem.find_or_initialize_by(label: label, workflow: task.workflow)
        content_item_definition = ContentItemDefinition.find_by(label: data_attribute[:label], workflow_definition: task.workflow.workflow_definition)
        content_item.content_item_definition = content_item_definition

        type = data_attribute[:type]
        case type
        when "has_many_dossiers"
          content_item.content_type = "dossier"
          content_item.options = {dossier_type: data_attribute[:parameter]["type"], multiple: true}
        when "has_one_dossier"
          content_item.content_type = "dossier"
          content_item.options = {dossier_type: data_attribute[:parameter]["type"], multiple: false}
          content_item.value = [data_attribute[:value]] unless data_attribute[:value].nil?
        when "has_many_files"
          content_item.content_type = "document"
        when "info"
          content_type = data_attribute[:parameter][:valueType]
          content_type = "document" if content_type.match?("_file")
          content_type = "dossier" if content_type.match?("dossier")
          content_item.content_type = content_type
          TaskItem.create!(task: task, content_item: content_item, position: index, info_box: true)
        when "has_many_workflows"
          next # Do nothing for now
        else # "date", "text","string", "datetime", "boolean"
          content_item.content_type = type
        end
        content_item.value ||= data_attribute[:value]
        content_item.save!
        task_item = TaskItem.find_or_create_by!(task: task, content_item: content_item, position: index)
        task_item_definition = TaskItemDefinition.find_by(task_definition: task.task_definition, content_item_definition: content_item, position: index)
        task_item.update(task_item_definition: task_item_definition)
      end
    end
  end

  def down
    TaskItem.delete_all
    ContentItem.delete_all
    TaskItemDefinition.delete_all
    ContentItemDefinition.delete_all
  end
end
