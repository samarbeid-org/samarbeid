class RefactorEngineNamespace < ActiveRecord::Migration[6.0]
  def change
    rename_table :definition_blocks, :block_definitions
    rename_column :block_definitions, :definition_process_id, :process_definition_id

    rename_table :workflow_processes, :workflows
    rename_table :ambitions_workflow_processes, :ambitions_workflows
    rename_column :ambitions_workflows, :workflow_process_id, :workflow_id
    rename_column :tasks, :workflow_process_id, :workflow_id
    rename_column :tasks, :process_or_block_type, :workflow_or_block_type
    rename_column :tasks, :process_or_block_id, :workflow_or_block_id

    rename_table :definition_tasks, :task_definitions

    rename_table :workflow_process_definitions, :workflow_definitions
    rename_table :groups_workflow_process_definitions, :groups_workflow_definitions
    rename_column :groups_workflow_definitions, :workflow_process_definition_id, :workflow_definition_id
    rename_column :workflows, :workflow_process_definition_id, :workflow_definition_id
    rename_column :block_definitions, :process_definition_id, :workflow_definition_id
    rename_column :task_definitions, :workflow_process_definition_id, :workflow_definition_id

    TaskDefinition.where(definition_process_or_block_type: "Definition::Block").update_all(definition_process_or_block_type: "BlockDefinition")
    TaskDefinition.where(definition_process_or_block_type: "Workflow::ProcessDefinition").update_all(definition_process_or_block_type: "WorkflowDefinition")

    Comment.where(object_type: "Workflow::Process").update_all(object_type: "Workflow")
    Contribution.where(contributable_type: "Workflow::Process").update_all(contributable_type: "Workflow")
    Event.where(object_type: "Workflow::Process").update_all(object_type: "Workflow")
    Task.where(workflow_or_block_type: "Workflow::Process").update_all(workflow_or_block_type: "Workflow")

    Comment.where(object_type: "Workflow::Task").update_all(object_type: "Task")
    Contribution.where(contributable_type: "Workflow::Task").update_all(contributable_type: "Task")
    Event.where(object_type: "Workflow::Task").update_all(object_type: "Task")
  end
end
