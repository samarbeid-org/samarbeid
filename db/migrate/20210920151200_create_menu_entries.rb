class CreateMenuEntries < ActiveRecord::Migration[6.1]
  def change
    create_table :menu_entries do |t|
      t.string :location, null: false
      t.integer :position, null: false
      t.string :title, null: false
      t.references :page, null: false, foreign_key: true

      t.timestamps
    end
    add_index :menu_entries, [:location, :position], unique: true
  end
end
