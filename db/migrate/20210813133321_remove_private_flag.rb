class RemovePrivateFlag < ActiveRecord::Migration[6.0]
  def change
    remove_column :workflows, :private
    remove_column :ambitions, :private
  end
end
