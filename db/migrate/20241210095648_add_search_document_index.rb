class AddSearchDocumentIndex < ActiveRecord::Migration[7.0]
  def change
    execute <<-SQL
        CREATE INDEX index_search_documents_on_full_text 
        ON search_documents 
        USING gin((
          setweight(to_tsvector('german', coalesce(identifier::text, '')), 'A') ||
          setweight(to_tsvector('german', coalesce(title::text, '')), 'A') ||
          setweight(to_tsvector('german', coalesce(subtitle::text, '')), 'B') ||
          setweight(to_tsvector('german', coalesce(content::text, '')), 'C')
        ));
    SQL
  end
end
