class ReExtractAndIndexUploadedFiles < ActiveRecord::Migration[6.1]
  def up
    UploadedFile.find_each do |uploaded_file|
      pp uploaded_file.id
      ExtractTextFromFileJob.perform_later(uploaded_file)
    end
    RunFullReindexJob.perform_later
  end

  def down
  end
end
