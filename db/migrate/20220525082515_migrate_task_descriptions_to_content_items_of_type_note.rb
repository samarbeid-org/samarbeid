class MigrateTaskDescriptionsToContentItemsOfTypeNote < ActiveRecord::Migration[6.1]
  def up
    Task.where.not(description: nil).each do |task|
      if ActionView::Base.full_sanitizer.sanitize(task.description).present?
        content_item = task.workflow.content_items.create!(content_type: ContentTypes::Note.to_s, options: {text: task.description})

        position = 0
        task.task_items.where("position >= ?", position).update_all("position = position + 1")

        task.task_items.create!(content_item: content_item, position: position)
      end
    end

    TaskDefinition.where.not(description: nil).each do |task_definition|
      if ActionView::Base.full_sanitizer.sanitize(task_definition.description).present?
        content_item_definition = task_definition.workflow_definition.content_item_definitions.create!(
          content_type: ContentTypes::Note.to_s,
          options: {text: task_definition.description}
        )

        position = 0
        task_definition.task_item_definitions.where("position >= ?", position).update_all("position = position + 1")

        task_definition.task_item_definitions.create!(
          content_item_definition: content_item_definition,
          position: position,
          workflow_definition: task_definition.workflow_definition
        )
      end
    end

    remove_column(:tasks, :description)
    remove_column(:task_definitions, :description)
  end

  def down
    add_column(:tasks, :description, :text)
    add_column(:task_definitions, :description, :text)
  end
end
