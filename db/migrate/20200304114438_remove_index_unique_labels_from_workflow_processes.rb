class RemoveIndexUniqueLabelsFromWorkflowProcesses < ActiveRecord::Migration[6.0]
  def change
    remove_index :workflow_processes, name: "idx_workflow_processes_labels_uniq_per_data_entity"
  end
end
