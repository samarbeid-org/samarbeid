class RemoveReadyStateFromWorkflows < ActiveRecord::Migration[6.0]
  def change
    Workflow.where(aasm_state: "ready").update_all(aasm_state: "active")
  end
end
