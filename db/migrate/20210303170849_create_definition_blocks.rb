class CreateDefinitionBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :definition_blocks do |t|
      t.references :definition_process, null: false
      t.integer :position, null: false
      t.boolean :parallel
      t.string :decision
      t.string :decision_option
      t.string :title
      t.timestamps
    end
    # link def::tasks to def::blocks XOR def::process
    add_reference :definition_tasks, :definition_process_or_block, polymorphic: true, null: true, index: {name: "index_definition_tasks_belong_to_process_or_block"}
    rename_column :definition_tasks, :order, :position

    Workflow::TaskDefinition.all.find_each do |def_task|
      def_task.definition_process_or_block = def_task.process_definition
      def_task.save!
    end
  end
end

class Workflow::TaskDefinition < ApplicationRecord
  self.table_name = "definition_tasks"
  belongs_to :process_definition, class_name: "Workflow::ProcessDefinition", foreign_key: "workflow_process_definition_id"
  belongs_to :definition_process_or_block, polymorphic: true, inverse_of: :definition_tasks
end
