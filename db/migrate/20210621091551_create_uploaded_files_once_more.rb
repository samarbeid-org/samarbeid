class CreateUploadedFilesOnceMore < ActiveRecord::Migration[6.0]
  def change
    create_table :uploaded_files do |t|
      t.string :title
      t.text :file_as_text

      t.timestamps
    end
  end
end
