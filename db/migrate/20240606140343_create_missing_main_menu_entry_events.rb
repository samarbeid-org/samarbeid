class CreateMissingMainMenuEntryEvents < ActiveRecord::Migration[6.1]
  def change
    MainMenuEntry.all.find_each do |entry|
      Events::CreatedEvent.create!(subject: nil, object: entry)
    end
  end
end
