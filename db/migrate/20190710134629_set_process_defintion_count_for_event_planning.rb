class SetProcessDefintionCountForEventPlanning < ActiveRecord::Migration[5.2]
  def change
    process_definition = Workflow::ProcessDefinition.find_by(key: "event_planning")
    process_definition.project_types_workflow_process_definitions.update_all(count: nil)
  end
end

class Workflow::ProcessDefinition < ApplicationRecord
  has_many :processes, foreign_key: "workflow_process_definition_id", dependent: :destroy

  has_many :project_types_workflow_process_definitions, foreign_key: "workflow_process_definition_id", dependent: :destroy
  has_many :project_types, through: :project_types_workflow_process_definitions

  validates :key, presence: true, uniqueness: true
  validates :name, presence: true, uniqueness: true

  def self.options_for_select
    order(:name).map { |p| [p.name, p.id.to_s] }
  end

  def self.data_entity_classes
    [Invention, PatentApplication, EventPlanning, ExternalRequirement]
  end

  def data_entity_class
    self.class.data_entity_classes.find { |data_entity_class| data_entity_class.camunda_process_definition_key == key }
  end
end
