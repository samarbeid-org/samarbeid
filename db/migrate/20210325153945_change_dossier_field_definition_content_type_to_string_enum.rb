class ChangeDossierFieldDefinitionContentTypeToStringEnum < ActiveRecord::Migration[6.0]
  def change
    rename_column :dossier_definition_fields, :content_type, :content_type_old
    add_column :dossier_definition_fields, :content_type, :string

    DossierDefinitionField.all.each do |field|
      field.content_type = field.content_type_old
      field.save!
    end

    change_column_null :dossier_definition_fields, :content_type, false
    remove_column :dossier_definition_fields, :content_type_old
  end
end

class DossierDefinitionField < ApplicationRecord
  enum content_type_old: [:string, :text, :integer, :boolean, :date, :richtext], _suffix: true
end
