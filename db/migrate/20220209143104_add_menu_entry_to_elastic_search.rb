class AddMenuEntryToElasticSearch < ActiveRecord::Migration[6.1]
  def change
    CustomElasticSearchConfig.reindex_model(MenuEntry)
  end
end
