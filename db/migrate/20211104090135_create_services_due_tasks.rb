class CreateServicesDueTasks < ActiveRecord::Migration[6.1]
  def change
    add_column :tasks, :due_processed, :boolean, default: false, index: true
  end
end
