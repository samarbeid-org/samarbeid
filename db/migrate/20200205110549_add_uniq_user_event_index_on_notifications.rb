class AddUniqUserEventIndexOnNotifications < ActiveRecord::Migration[5.2]
  def change
    add_index :notifications, [:user_id, :event_id], unique: true
  end
end
