class ChangeProcessNameFieldToAssociationToProcessDefintion < ActiveRecord::Migration[5.2]
  def change
    add_reference :workflow_processes, :workflow_process_definition, foreign_key: true

    Workflow::Process.all.each do |process|
      pd = Workflow::ProcessDefinition.find_by(name: process.read_attribute(:name))
      process.update(definition: pd)
    end

    change_column_null :workflow_processes, :workflow_process_definition_id, false
    remove_column :workflow_processes, :name
  end
end
