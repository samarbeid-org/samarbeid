class CreateContentItems < ActiveRecord::Migration[6.0]
  def change
    create_table :content_items do |t|
      t.string :label
      t.string :content_type
      t.jsonb :options, default: {}, null: false
      t.string :value
      t.references :workflow, null: false, foreign_key: true

      t.timestamps
    end
  end
end
