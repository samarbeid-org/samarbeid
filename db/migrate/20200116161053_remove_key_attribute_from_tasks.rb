class RemoveKeyAttributeFromTasks < ActiveRecord::Migration[5.2]
  def change
    remove_column :workflow_tasks, :key
  end
end
