class LinkBlocksToTheirDefinition < ActiveRecord::Migration[6.1]
  def change
    add_reference :blocks, :block_definition, index: true
  end
end
