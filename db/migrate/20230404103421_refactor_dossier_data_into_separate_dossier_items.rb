class RefactorDossierDataIntoSeparateDossierItems < ActiveRecord::Migration[6.1]
  def change
    create_table :dossier_items do |t|
      t.belongs_to :dossier, null: false, index: true
      t.belongs_to :dossier_item_definition, null: false, index: true
      t.jsonb :value
      t.bigint :lock_version, default: 0, null: false

      t.timestamps
    end

    uniq_dossier_item_definition_ids = DossierItemDefinition.where(unique: true).pluck(:id)
    DossierItemDefinition.where(id: uniq_dossier_item_definition_ids).each do |did|
      did.update!(unique: false)
    end

    Dossier.find_each do |dossier|
      dossier.data_fields.each do |field|
        dossier_item_definition = field[:definition]
        value = field[:value]

        unless dossier_item_definition.content_type.data_empty?(value)
          DossierItem.create!(dossier: dossier, dossier_item_definition: dossier_item_definition, value: value)
        end
      end
    end

    DossierItemDefinition.where(id: uniq_dossier_item_definition_ids).each do |did|
      did.update!(unique: true)
    end
  end
end

class Dossier < ApplicationRecord
  belongs_to :definition, class_name: "DossierDefinition", inverse_of: :dossiers

  def get_item_value(id)
    item_definition = item_definitions[id.to_s]
    return nil if item_definition.nil?

    item_definition.content_type.deserialize(data[id.to_s])
  end

  def data_fields(required_and_recommended_only = false)
    definition_item = definition.items
    definition_item = definition_item.required_or_recommended if required_and_recommended_only
    definition_item.map { |item| {definition: item, value: get_item_value(item.id), lock_version: lock_version} }
  end

  private

  def item_definitions
    return @item_definitions unless @item_definitions.nil?

    @item_definitions = {}
    definition.items.each do |item_definition|
      @item_definitions[item_definition.id.to_s] = item_definition
    end

    @item_definitions
  end
end
