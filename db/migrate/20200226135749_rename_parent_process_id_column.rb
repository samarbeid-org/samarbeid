class RenameParentProcessIdColumn < ActiveRecord::Migration[6.0]
  def change
    rename_column :workflow_processes, :parent_process_id, :predecessor_process_id
  end
end
