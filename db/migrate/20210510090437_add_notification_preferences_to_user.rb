class AddNotificationPreferencesToUser < ActiveRecord::Migration[6.0]
  def change
    add_column :users, :noti_last_sent_at, :datetime, default: -> { "CURRENT_TIMESTAMP" }
    add_column :users, :noti_next_send_at, :datetime, default: -> { "CURRENT_TIMESTAMP" }
    add_column :users, :noti_interval, :bigint, default: 1.day
  end
end
