class AddAssigneeToProcess < ActiveRecord::Migration[5.2]
  def change
    add_reference :workflow_processes, :assignee, index: true, foreign_key: {to_table: :users}
  end
end
