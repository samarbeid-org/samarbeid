class AllowSettingProcessAssigneesAsCandidatesForTask < ActiveRecord::Migration[6.1]
  def change
    add_column :task_definitions, :candidate_assignee_from_workflow, :boolean, default: false
  end
end
