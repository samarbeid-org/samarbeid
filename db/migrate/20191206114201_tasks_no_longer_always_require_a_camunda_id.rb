class TasksNoLongerAlwaysRequireACamundaId < ActiveRecord::Migration[5.2]
  def change
    change_column_null :workflow_tasks, :camunda_id, true

    # create Tasks for existing processes
    Workflow::Process.find_each do |process|
      process.definition.task_definitions.map do |task_definition|
        Workflow::Task.create(name: task_definition.name,
          description: task_definition.description,
          process: process,
          state: Workflow::Task::CREATED,
          key: task_definition.key)
      end
    end
  end
end
