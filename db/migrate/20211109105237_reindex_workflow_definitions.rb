class ReindexWorkflowDefinitions < ActiveRecord::Migration[6.1]
  def change
    CustomElasticSearchConfig.reindex_and_refresh(WorkflowDefinition)
  end
end
