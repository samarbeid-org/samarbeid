class DropOldUnusedTables < ActiveRecord::Migration[6.1]
  def change
    drop_table :invention_first_meeting_participations, if_exists: true
    drop_table :invention_inventors, if_exists: true
    drop_table :inventions, if_exists: true
    drop_table :people, if_exists: true
    drop_table :workflow_events, if_exists: true
    drop_table :workflow_processes, if_exists: true
    drop_table :workflow_tasks, if_exists: true
    drop_table :version_associations, if_exists: true
    drop_table :versions, if_exists: true
  end
end
