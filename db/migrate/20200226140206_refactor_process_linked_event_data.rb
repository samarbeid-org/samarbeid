class RefactorProcessLinkedEventData < ActiveRecord::Migration[6.0]
  def change
    Events::ProcessLinkedEvent.all.each do |event|
      event.update(data: {predecessor_process: event.parent_process})
    end
  end
end

class Events::ProcessLinkedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def process
    object
  end

  def parent_process
    data&.[](:parent_process)
  end

  def predecessor_process
    data&.[](:predecessor_process)
  end

  private

  def validate_event_type_values
    errors.add(:object, "should be a process") unless process.is_a?(Workflow::Process)
  end
end
