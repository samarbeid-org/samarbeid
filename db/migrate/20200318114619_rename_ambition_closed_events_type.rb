class RenameAmbitionClosedEventsType < ActiveRecord::Migration[6.0]
  def change
    Event.where(type: "Events::AmbitionClosedEvent").update_all(type: "Events::AmbitionCompletedEvent")
  end
end
