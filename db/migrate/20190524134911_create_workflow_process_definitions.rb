class CreateWorkflowProcessDefinitions < ActiveRecord::Migration[5.2]
  def change
    create_table :workflow_process_definitions do |t|
      t.string :key, null: false, index: {unique: true}
      t.string :name, null: false, index: {unique: true}

      t.timestamps
    end

    Camunda::ProcessDefinition.where(latestVersion: true).each do |process_definition|
      Workflow::ProcessDefinition.create(key: process_definition.key, name: process_definition.name)
    end
  end
end
