class AddStatusUpdatedAtToAmbitions < ActiveRecord::Migration[6.0]
  def change
    add_column :ambitions, :status_updated_at, :datetime, null: false, default: -> { "NOW()" }
  end
end
