class ImportContactCsvFile < ActiveRecord::Migration[6.1]
  def change
    csv_file_blob = ActiveStorage::Blob.find_by(key: "8hr7pqduje33dank2togyxhrf02n")

    MigrationJob::ImportContactCsvJob.new.perform(csv_file_blob) if csv_file_blob
  end
end
