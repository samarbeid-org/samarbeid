class JoinAmbitionProcess < ActiveRecord::Migration[5.2]
  def change
    create_join_table :ambitions, :workflow_processes do |t|
      t.index :ambition_id
      t.index :workflow_process_id
      t.index [:ambition_id, :workflow_process_id], unique: true, name: :unique_index_ambitions_processes_join
    end

    Workflow::Process.all.each do |process|
      ambition = Ambition.find(process.ambition_id)
      process.ambitions << ambition
    end

    remove_column :workflow_processes, :ambition_id
  end
end
