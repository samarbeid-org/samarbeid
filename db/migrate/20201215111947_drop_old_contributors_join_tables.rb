class DropOldContributorsJoinTables < ActiveRecord::Migration[6.0]
  def change
    drop_join_table :users, :workflow_processes
    drop_join_table :users, :workflow_tasks
    drop_join_table :users, :ambitions
  end
end
