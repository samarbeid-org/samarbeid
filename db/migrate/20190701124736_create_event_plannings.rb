class CreateEventPlannings < ActiveRecord::Migration[5.2]
  def change
    create_table :event_plannings do |t|
      t.string :event_type
      t.text :marketing_concept
      t.text :scheduling
      t.text :agenda
      t.text :content
      t.text :financial_planning
      t.text :spatial_planning
      t.text :other_points
      t.text :responsibilities

      t.timestamps
    end
  end
end
