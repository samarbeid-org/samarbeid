class RemoveTextfieldsFromPatentApplication < ActiveRecord::Migration[5.2]
  def change
    remove_column :patent_applications, :description
    remove_column :patent_applications, :prior_art
    remove_column :patent_applications, :object_of_invention
    remove_column :patent_applications, :patent_claim
  end
end
