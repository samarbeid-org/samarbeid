class RemoveSnoozeForeverPossibility < ActiveRecord::Migration[6.1]
  def change
    Events::SnoozedEvent.find_each do |snooze|
      snooze.destroy if snooze.snoozed_until.blank?
    end
  end
end
