class RefactorTaskAssociationToPolymorphicObjectAssociationInComments < ActiveRecord::Migration[6.0]
  def change
    add_reference :comments, :object, polymorphic: true, index: true

    Comment.all.each do |comment|
      comment.object = comment.task
      comment.save!
    end

    change_column_null :comments, :object_id, false
    change_column_null :comments, :object_type, false

    remove_column :comments, :task_id
  end
end

class Comment < ApplicationRecord
  belongs_to :task, class_name: "Workflow::Task"
end
