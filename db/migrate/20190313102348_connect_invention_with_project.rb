class ConnectInventionWithProject < ActiveRecord::Migration[5.2]
  def change
    add_reference :inventions, :project, foreign_key: true

    Invention.all.each do |invention|
      project = Project.create!(title: invention.name)
      invention.update!(project_id: project.id)
    end

    remove_column :inventions, :name
  end
end
