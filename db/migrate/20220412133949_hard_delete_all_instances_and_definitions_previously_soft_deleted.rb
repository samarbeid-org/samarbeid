class HardDeleteAllInstancesAndDefinitionsPreviouslySoftDeleted < ActiveRecord::Migration[6.1]
  def change
    Workflow.where(aasm_state: ["trashed", "deleted"]).destroy_all
    Task.where(aasm_state: "deleted").destroy_all
    Block.where(aasm_state: "deleted").destroy_all
    Dossier.where(deleted: true).destroy_all
    Ambition.where(deleted: true).destroy_all
    BlockDefinition.where(deleted: true).destroy_all
    ContentItemDefinition.where(deleted: true).destroy_all
    TaskDefinition.where(deleted: true).destroy_all
    TaskItemDefinition.where(deleted: true).destroy_all
    WorkflowDefinition.where(deleted: true).destroy_all
    # ContentItems previously were not destroyed properly due to a bug
    Workflow.find_each do |workflow|
      workflow.content_items.select { |ci| ci.task_items.none? }.each(&:destroy!)
    end

    sleep 5 # due to ActiveRecord::PreparedStatementCacheExpired: ERROR:  cached plan must not change result type EXCEPTION

    remove_column :workflows, :deleted_at
    remove_column :tasks, :deleted_at
    remove_column :dossiers, :deleted
    remove_column :ambitions, :deleted
    remove_column :block_definitions, :deleted
    remove_column :content_item_definitions, :deleted
    remove_column :task_definitions, :deleted
    remove_column :task_item_definitions, :deleted
    remove_column :workflow_definitions, :deleted
  end
end
