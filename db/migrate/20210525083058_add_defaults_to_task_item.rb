class AddDefaultsToTaskItem < ActiveRecord::Migration[6.0]
  def change
    change_column_default :task_items, :required, from: nil, to: false
    change_column_default :task_items, :info_box, from: nil, to: false
    TaskItem.where(required: nil).update_all(required: false)
    TaskItem.where(info_box: nil).update_all(info_box: false)
  end
end
