class AllowLockingByShareLink < ActiveRecord::Migration[6.1]
  def change
    add_reference :content_items, :locked_by, polymorphic: true

    # Migrate existing data
    execute "UPDATE content_items SET locked_by_id = locked_by"
    ContentItem.where.not(locked_by_id: nil).update_all(locked_by_type: "User")

    remove_column :content_items, :locked_by
  end
end
