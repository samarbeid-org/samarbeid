class RefactorMenuEntries < ActiveRecord::Migration[6.1]
  def change
    change_column_null :menu_entries, :page_id, true
    remove_index :menu_entries, :page_id

    add_column :menu_entries, :url, :string
    add_column :menu_entries, :new_window, :boolean, default: false
  end
end
