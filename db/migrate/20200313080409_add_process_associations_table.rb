class AddProcessAssociationsTable < ActiveRecord::Migration[6.0]
  def change
    create_table :process_associations do |t|
      t.references :process, references: :workflow_processes, null: false, foreign_key: {to_table: :workflow_processes}, index: true
      t.references :data_entity, polymorphic: true, index: {name: :index_process_associations_on_data_entity_type_and_id}, null: false
      t.string :data_entity_field, null: false, index: true
      t.timestamps
    end
  end
end
