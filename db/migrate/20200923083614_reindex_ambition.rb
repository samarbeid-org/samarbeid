class ReindexAmbition < ActiveRecord::Migration[6.0]
  def change
    Ambition.reindex
  end
end
