class AddEndedDateToWorkflowTask < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_tasks, :ended_at, :datetime
  end
end
