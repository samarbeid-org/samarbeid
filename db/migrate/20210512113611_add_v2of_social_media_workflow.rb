class AddV2ofSocialMediaWorkflow < ActiveRecord::Migration[6.0]
  def change
    camunda_process_xml = File.read(Rails.root.join("workflow", "social_media_v3.bpmn"))
    xml_doc = Nokogiri::XML(camunda_process_xml)
    Services::CamundaParser.create_or_update_process_from_camunda(xml_doc)

    dossier_definition = DossierDefinition.find_by(identifier: "topic_category")
    DossierFieldDefinition.create_with(label: "Notizen", position: 1, content_type: "richtext").find_or_create_by(definition: dossier_definition, identifier: "notes")
  end
end
