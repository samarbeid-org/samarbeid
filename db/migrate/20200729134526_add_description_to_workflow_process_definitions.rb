class AddDescriptionToWorkflowProcessDefinitions < ActiveRecord::Migration[6.0]
  def change
    add_column :workflow_process_definitions, :description, :text
  end
end
