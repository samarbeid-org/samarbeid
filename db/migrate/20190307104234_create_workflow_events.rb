class CreateWorkflowEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :workflow_events do |t|
      t.string :event_type, null: false
      t.text :payload

      t.references :workflow_task, foreign_key: true, null: false
      t.references :user, foreign_key: true

      t.timestamps
    end
  end
end
