class AddCandidateAssigneesToTasks < ActiveRecord::Migration[6.1]
  def change
    add_reference :task_definitions, :candidate_assignee, foreign_key: {to_table: :users}
  end
end
