class AdddDeletedFlagToDossiers < ActiveRecord::Migration[6.0]
  def change
    add_column :dossiers, :deleted, :boolean, default: false, null: false
  end
end
