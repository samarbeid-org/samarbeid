class AddDescriptionToAmbitions < ActiveRecord::Migration[5.2]
  def change
    add_column :ambitions, :description, :text
  end
end
