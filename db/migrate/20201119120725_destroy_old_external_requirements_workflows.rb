class DestroyOldExternalRequirementsWorkflows < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key "documents", "process_data_entities", column: "data_entity_id"
    Workflow::ProcessDefinition.find_by_key("external_event")&.destroy

    CamundaInitializer.new.start_and_init_camunda_engine # clean setup of new external_event workflow
  end
end
