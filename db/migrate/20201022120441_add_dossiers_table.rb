class AddDossiersTable < ActiveRecord::Migration[6.0]
  def change
    create_table :dossiers do |t|
      t.belongs_to :definition, foreign_key: {to_table: :dossier_definitions}, null: false, index: true
      t.text :data
      t.belongs_to :created_by, foreign_key: {to_table: :users}, null: false

      t.timestamps
    end
  end
end
