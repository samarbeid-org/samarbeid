class ApplySupersedeRules < ActiveRecord::Migration[6.1]
  def change
    Event.find_each(&:apply_supersedees)
  end
end
