class SetCorrectOptionsForDossierContentItems < ActiveRecord::Migration[6.0]
  def change
    ContentItemDefinition.where(content_type: ContentTypes::Dossier.type).each do |item|
      if item.options.has_key?("dossier_type")
        item.options["type"] = item.options.delete "dossier_type"
        item.save!
      end
    end

    ContentItem.where(content_type: ContentTypes::Dossier.type).each do |item|
      if item.options.has_key?("dossier_type")
        item.options["type"] = item.options.delete "dossier_type"
        item.save!
      end
    end
  end
end
