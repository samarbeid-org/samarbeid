class ReindexDataEntities < ActiveRecord::Migration[5.2]
  def change
    Invention.reindex
    PatentApplication.reindex
  end
end
