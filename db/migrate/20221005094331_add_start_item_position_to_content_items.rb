class AddStartItemPositionToContentItems < ActiveRecord::Migration[6.1]
  def change
    add_column :content_item_definitions, :start_item_position, :integer
  end
end
