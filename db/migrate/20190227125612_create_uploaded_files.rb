class CreateUploadedFiles < ActiveRecord::Migration[5.2]
  def change
    create_table :uploaded_files do |t|
      t.string :name
      t.references :invention, index: true
      t.timestamps
    end
  end
end
