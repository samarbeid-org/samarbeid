class AddGeneralStructureChangeEvent < ActiveRecord::Migration[6.1]
  def change
    Event.where(type: "Events::ChangedStructureEvent").update_all(type: "Events::ChangedStructureEventByInstance")
  end
end
