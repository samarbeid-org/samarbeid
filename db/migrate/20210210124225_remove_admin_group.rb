class RemoveAdminGroup < ActiveRecord::Migration[6.0]
  def change
    Group.find_by(name: "admin")&.destroy
  end
end
