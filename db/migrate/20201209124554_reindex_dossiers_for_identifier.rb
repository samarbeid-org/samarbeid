class ReindexDossiersForIdentifier < ActiveRecord::Migration[6.0]
  def change
    Dossier.searchkick_index.delete if Dossier.searchkick_index.exists?
    Dossier.reindex
  end
end
