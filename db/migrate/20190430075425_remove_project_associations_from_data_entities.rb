class RemoveProjectAssociationsFromDataEntities < ActiveRecord::Migration[5.2]
  def change
    remove_reference :inventions, :project, foreign_key: true
    remove_reference :patent_applications, :project, foreign_key: true
  end
end
