class StopReferencingLastAttributesFromDefinitions < ActiveRecord::Migration[6.0]
  def change
    add_column :workflows, :name, :string
    Workflow.all.find_each do |workflow|
      next unless workflow.workflow_definition
      workflow.update_columns(name: workflow.workflow_definition.name)
      workflow.update_columns(description: workflow.workflow_definition.description) unless workflow.read_attribute(:description).present?
    end
  end
end
