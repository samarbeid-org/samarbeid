class ImportContactUpdateCsv < ActiveRecord::Migration[6.1]
  def change
    csv_file_blob = ActiveStorage::Blob.find_by(key: "apb4fc6so9fx9vdgndzcpkokwt9d")

    MigrationJob::ImportContactUpdateCsvJob.new.perform(csv_file_blob) if csv_file_blob
  end
end
