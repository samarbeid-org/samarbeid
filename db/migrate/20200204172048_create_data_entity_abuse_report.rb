class CreateDataEntityAbuseReport < ActiveRecord::Migration[5.2]
  def change
    create_table :abuse_reports do |t|
      t.string :email_sender
      t.text :email_content
      t.string :ad_url
      t.text :ad_content
      t.boolean :should_be_deleted
      t.text :reason_for_decision

      t.timestamps
    end
  end
end
