class UpdateElasticSearchCluster < ActiveRecord::Migration[6.0]
  def up
    CustomElasticSearchConfig.initalize_searchkick_indexes
  end

  def down
    # Nothing to do
  end
end
