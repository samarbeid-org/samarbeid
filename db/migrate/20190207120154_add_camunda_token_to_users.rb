class AddCamundaTokenToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :camunda_token, :string
  end
end
