class RemoveDataChangedEvents < ActiveRecord::Migration[6.0]
  def change
    Event.where(type: "Events::DataChangedEvent").delete_all
  end
end
