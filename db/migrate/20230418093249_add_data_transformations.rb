class AddDataTransformations < ActiveRecord::Migration[6.1]
  def change
    create_table :data_transformations do |t|
      t.belongs_to :dossier_item_definition, null: false
      t.string :content_type, null: false
      t.jsonb :options, default: {}, null: false
      t.boolean :required
      t.boolean :unique

      t.timestamps
    end

    create_table :data_transformation_items do |t|
      t.belongs_to :data_transformation, null: false
      t.belongs_to :dossier, null: false
      t.jsonb :value
      t.string :error_messages
      t.boolean :confirmed, null: false

      t.timestamps
    end
  end
end
