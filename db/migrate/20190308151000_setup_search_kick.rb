class SetupSearchKick < ActiveRecord::Migration[5.2]
  def change
    # CustomElasticSearchConfig.initalize_searchkick_indexes can no longer run on heroku and is not needed as later migration does the same thing.
  end
end
