class DropUniquenessOfTaskOrder < ActiveRecord::Migration[6.0]
  def up
    remove_index :workflow_flow_object_definitions, column: [:workflow_process_definition_id, :order]
  end

  def down
  end
end
