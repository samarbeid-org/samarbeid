class ReindexElasticSearchOnceMore < ActiveRecord::Migration[5.2]
  def change
    # Ignore errors as this is only for convenience and no strictly necessary data migration

    CustomElasticSearchConfig.initalize_searchkick_indexes
  rescue
    nil
  end
end
