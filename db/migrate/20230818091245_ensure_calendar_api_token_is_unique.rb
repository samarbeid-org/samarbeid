class EnsureCalendarApiTokenIsUnique < ActiveRecord::Migration[6.1]
  def change
    add_index :users, :calendar_token, unique: true
  end
end
