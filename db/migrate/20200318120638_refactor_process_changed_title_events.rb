class RefactorProcessChangedTitleEvents < ActiveRecord::Migration[6.0]
  def change
    Event.where(type: "Events::ProcessChangedLabelEvent").update_all(type: "Events::ProcessChangedTitleEvent")

    Events::ProcessChangedTitleEvent.all.each do |event|
      event.update!(data: {new_title: event.new_label})
    end
  end
end

class Events::ProcessChangedTitleEvent < Event
  validate :validate_event_type_values

  def new_label
    data&.[](:new_label)
  end

  private

  def validate_event_type_values
  end
end
