class EnsureAllAssigneesAreContributors < ActiveRecord::Migration[6.1]
  def change
    [Task, Workflow, Ambition].each do |contrib|
      contrib.where.not(assignee: nil).find_each do |thingy|
        unless thingy.contributors.include?(thingy.assignee)
          thingy.contributors << thingy.assignee
        end
      end
    end
  end
end
