class UpdateMenitionTypesForWorkflows < ActiveRecord::Migration[6.0]
  def up
    Comment.all.each do |comment|
      if comment.message
        comment.message = Services::Mentioning.rename_mention_type(comment.message, "process", "workflow")
        comment.save!(touch: false) if comment.changed?
      end
    end

    Ambition.all.each do |ambition|
      if ambition.description
        ambition.description = Services::Mentioning.rename_mention_type(ambition.description, "process", "workflow")
        ambition.save!(touch: false) if ambition.changed?
      end
    end

    Workflow.all.each do |workflow|
      if workflow.description
        workflow.description = Services::Mentioning.rename_mention_type(workflow.description, "process", "workflow")
        workflow.save!(touch: false) if workflow.changed?
      end
    end

    Task.all.each do |task|
      if task.description
        task.description = Services::Mentioning.rename_mention_type(task.description, "process", "workflow")
        task.save!(touch: false) if task.changed?
      end
    end
  end

  def down
    Comment.all.each do |comment|
      if comment.message
        comment.message = Services::Mentioning.rename_mention_type(comment.message, "workflow", "process")
        comment.save!(touch: false) if comment.changed?
      end
    end

    Ambition.all.each do |ambition|
      if ambition.description
        ambition.description = Services::Mentioning.rename_mention_type(ambition.description, "workflow", "process")
        ambition.save!(touch: false) if ambition.changed?
      end
    end

    Workflow.all.each do |workflow|
      if workflow.description
        workflow.description = Services::Mentioning.rename_mention_type(workflow.description, "workflow", "process")
        workflow.save!(touch: false) if workflow.changed?
      end
    end

    Task.all.each do |task|
      if task.description
        task.description = Services::Mentioning.rename_mention_type(task.description, "workflow", "process")
        task.save!(touch: false) if task.changed?
      end
    end
  end
end

class Services::Mentioning
  def self.rename_mention_type(html, from, to)
    iterate_mention_content(html) { |id, type, element|
      element.set_attribute("m-type", to) if type == from
    }
  end
end
