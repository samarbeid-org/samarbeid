class CreateSearchDocuments < ActiveRecord::Migration[7.0]
  def change
    create_table :search_documents do |t|
      t.string :identifier, index: {unique: true}
      t.string :title
      t.string :subtitle
      t.text :content
      t.references :searchable, polymorphic: true, null: false, index: {unique: true}

      t.timestamps
    end

    ReindexAllJob.perform_later
  end
end
