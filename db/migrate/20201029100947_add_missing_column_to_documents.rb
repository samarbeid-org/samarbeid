class AddMissingColumnToDocuments < ActiveRecord::Migration[6.0]
  def change
    add_column :documents, "data_entity_type", :string, default: "ProcessDataEntity"

    CustomElasticSearchConfig.initalize_searchkick_indexes # Reindex to get those files into search
  end
end
