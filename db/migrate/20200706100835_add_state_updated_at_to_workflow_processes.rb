class AddStateUpdatedAtToWorkflowProcesses < ActiveRecord::Migration[6.0]
  def change
    add_column :workflow_processes, :state_updated_at, :datetime, null: false, default: -> { "NOW()" }
  end
end
