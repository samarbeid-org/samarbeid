class ReindexWorkflowDefinitionsForActivatedData < ActiveRecord::Migration[6.1]
  def change
    WorkflowDefinition.reindex
  end
end
