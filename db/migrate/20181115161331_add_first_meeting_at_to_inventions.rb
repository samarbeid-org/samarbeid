class AddFirstMeetingAtToInventions < ActiveRecord::Migration[5.2]
  def change
    add_column :inventions, :first_meeting_at, :datetime
  end
end
