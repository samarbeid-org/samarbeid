class RenameTaskKey < ActiveRecord::Migration[6.0]
  def change
    Workflow::TaskDefinition.where(key: "decide_if_absuse").update_all(key: "decide_if_abuse")
  end
end
