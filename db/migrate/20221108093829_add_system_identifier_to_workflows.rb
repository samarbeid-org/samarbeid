class AddSystemIdentifierToWorkflows < ActiveRecord::Migration[6.1]
  def change
    add_column :workflows, :system_identifier, :string

    # Set system_process_single_task on all workflow instances of single task workflow def with a single task
    wd_single_task = WorkflowDefinition.system_process_definition_single_task
    wd_single_task.workflows.each do |workflow|
      if workflow.items.size === 1 && workflow.items.first.is_a?(Task)
        workflow.update!(system_identifier: Workflow.system_identifiers[:system_process_single_task])
      end
    end
  end
end
