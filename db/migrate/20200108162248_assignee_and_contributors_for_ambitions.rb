class AssigneeAndContributorsForAmbitions < ActiveRecord::Migration[5.2]
  def change
    add_reference :ambitions, :assignee, index: true, foreign_key: {to_table: :users}

    create_join_table :ambitions, :users do |t|
      t.index :user_id
      t.index :ambition_id
      t.index [:user_id, :ambition_id], unique: true
    end
  end
end
