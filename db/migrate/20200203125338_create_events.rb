class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :type, null: false
      t.references :object, polymorphic: true, null: false
      t.references :subject, foreign_key: {to_table: :users}
      t.text :data

      t.timestamps
    end
  end
end
