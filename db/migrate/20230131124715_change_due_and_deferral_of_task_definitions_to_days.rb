class ChangeDueAndDeferralOfTaskDefinitionsToDays < ActiveRecord::Migration[6.1]
  def change
    rename_column :task_definitions, :due_in_hours, :due_in_days
    rename_column :task_definitions, :deferral_in_hours, :deferral_in_days

    TaskDefinition.reset_column_information
    TaskDefinition.where.not(due_in_days: nil).or(TaskDefinition.where.not(deferral_in_days: nil)).each do |task_definition|
      due_in_days = nil
      deferral_in_days = nil

      due_in_days = task_definition.due_in_days / 24 unless task_definition.due_in_days.nil?
      unless task_definition.deferral_in_days.nil?
        deferral_in_days = task_definition.deferral_in_days / 24
        deferral_in_days = 1 if deferral_in_days < 1
      end

      task_definition.update!(due_in_days: due_in_days, deferral_in_days: deferral_in_days)
    end
  end
end
