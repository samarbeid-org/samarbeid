class RemoveUniqueFromAmbitionsTitleIndex < ActiveRecord::Migration[5.2]
  def change
    remove_index :ambitions, :title
    add_index :ambitions, :title
  end
end
