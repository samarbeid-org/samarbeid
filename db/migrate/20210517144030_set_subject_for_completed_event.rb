class SetSubjectForCompletedEvent < ActiveRecord::Migration[6.0]
  def change
    subject = User.first
    Events::CompletedEvent.where(subject: nil).update_all(subject_id: subject.id)
  end
end
