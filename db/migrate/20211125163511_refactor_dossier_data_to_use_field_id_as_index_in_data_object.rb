class RefactorDossierDataToUseFieldIdAsIndexInDataObject < ActiveRecord::Migration[6.1]
  def change
    Dossier.all.each do |dossier|
      dossier.data = dossier.data.map { |k, v| [dossier.field_definitions_from_identifier[k].id, v] }.to_h
      dossier.save!
    end
  end
end

class Dossier < ApplicationRecord
  def field_definitions_from_identifier
    return @field_definitions_from_identifier unless @field_definitions_from_identifier.nil?

    @field_definitions_from_identifier = {}
    definition.fields.each do |field_definition|
      @field_definitions_from_identifier[field_definition.identifier] = field_definition
    end

    @field_definitions_from_identifier
  end
end
