class AddNewEngineStatesToWorkflow < ActiveRecord::Migration[6.0]
  def change
    add_column :workflows, :aasm_state, :string
    rename_column :workflows, :ended_at, :completed_at
    add_column :workflows, :deleted_at, :datetime

    Workflow.where(state: "running").update_all(aasm_state: "ready")
    Workflow.where(state: "completed").update_all(aasm_state: "completed")
    Workflow.where(state: "canceled").update_all(aasm_state: "ready")

    remove_column :workflows, :state

    CustomElasticSearchConfig.reindex_and_refresh(Workflow)
  end
end
