class CreateContributions < ActiveRecord::Migration[6.0]
  def change
    create_table :contributions do |t|
      t.references :contributable, polymorphic: true, null: false, index: true
      t.references :user, null: false, foreign_key: true

      t.timestamps
    end

    Workflow::Process.all.each do |process|
      process.contributors = process.contributors_old
    end

    Workflow::Task.all.each do |task|
      task.contributors = task.contributors_old
    end

    Ambition.all.each do |ambition|
      ambition.contributors = ambition.contributors_old
    end
  end
end

class Workflow::Process < ApplicationRecord
  has_and_belongs_to_many :contributors_old, class_name: "User", foreign_key: :workflow_process_id, after_add: :reindex_tasks_and_self, after_remove: :reindex_tasks_and_self
end

class Workflow::Task < ApplicationRecord
  has_and_belongs_to_many :contributors_old, class_name: "User", foreign_key: :workflow_task_id, after_add: :reindex_self_and_process, after_remove: :reindex_self_and_process
end

class Ambition < ApplicationRecord
  has_and_belongs_to_many :contributors_old, class_name: "User", after_add: :reindex_later, after_remove: :reindex_later
end
