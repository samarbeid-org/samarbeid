class StoreFileTextsInDb < ActiveRecord::Migration[5.2]
  def change
    add_column :uploaded_files, :file_as_text, :text
  end
end
