class SetCancelUserAndEndedAtForExistingCanceledProcesses < ActiveRecord::Migration[5.2]
  def change
    user = User.first
    Workflow::Process.all.each do |process|
      if process.canceled?
        process.ended_at = process.updated_at if process.ended_at.nil?
        process.cancel_user = user if process.canceled? && process.cancel_user.nil?
        process.save!
      end
    end
  end
end
