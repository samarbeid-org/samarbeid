class FixEmailContentItems < ActiveRecord::Migration[6.0]
  def change
    ContentItemDefinition.all.select { |ci| ci.label.match("E-Mail-Adresse") }.each { |ci| ci.update(content_type: "email") }
    ContentItem.all.select { |ci| ci.label.match("E-Mail-Adresse") }.each { |ci| ci.update(content_type: "email") }
  end
end
