class MakeAdminAtExampleOrgSuperadminAgain < ActiveRecord::Migration[6.1]
  def change
    User.where(email: "admin@example.org").find_each { |admin| admin.update(role: :super_admin) }
  end
end
