class ChangeTimeTrackingSpentAtToDate < ActiveRecord::Migration[6.1]
  def change
    Events::CommentedEvent.all.filter { |event| (event.time_spent_in_minutes || 0) > 0 }.each do |event|
      event.data[:time_spent_at] = event.time_spent_at.to_date
      event.save!
    end
  end
end
