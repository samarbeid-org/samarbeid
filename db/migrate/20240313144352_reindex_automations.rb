class ReindexAutomations < ActiveRecord::Migration[6.1]
  def change
    Automation.reindex
  end
end
