class AddNewEngineStatesToTasks < ActiveRecord::Migration[6.0]
  def change
    rename_column :tasks, :state, :aasm_state

    rename_column :tasks, :ended_at, :completed_at
    add_column :tasks, :deleted_at, :datetime

    add_column :task_definitions, :due_in_hours, :integer

    Task.where(aasm_state: "started").update_all(aasm_state: "ready")

    CustomElasticSearchConfig.reindex_and_refresh(Workflow)
  end
end
