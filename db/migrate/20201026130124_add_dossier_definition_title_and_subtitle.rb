class AddDossierDefinitionTitleAndSubtitle < ActiveRecord::Migration[6.0]
  def change
    add_column :dossier_definitions, :title_fields, :jsonb, default: []
    add_column :dossier_definitions, :subtitle_fields, :jsonb, default: []
  end
end
