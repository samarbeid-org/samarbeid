class CreateNews < ActiveRecord::Migration[6.1]
  def change
    create_table :news do |t|
      t.string :url, null: false
      t.string :title, null: false

      t.timestamps
    end
    add_index :news, :url, unique: true
  end
end
