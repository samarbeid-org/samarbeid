class ::ExternalRequirement < ApplicationRecord; end

class RemoveExternalRequirement < ActiveRecord::Migration[6.0]
  def change
    PersonAssociation.where(data_entity_type: "ExternalRequirement").delete_all
    UploadedFile.where(data_entity_type: "ExternalRequirement").delete_all
    Workflow::Process.where(data_entity_type: "ExternalRequirement").destroy_all
    drop_table :external_requirements
  end
end
