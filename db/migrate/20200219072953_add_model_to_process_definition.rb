class AddModelToProcessDefinition < ActiveRecord::Migration[6.0]
  def change
    add_column :workflow_process_definitions, :model, :jsonb, default: {}
  end
end
