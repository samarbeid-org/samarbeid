class AddDeliveredAtAndReadAtToNotification < ActiveRecord::Migration[6.1]
  def change
    add_column :notifications, :delivered_at, :datetime
    add_column :notifications, :read_at, :datetime

    # initialize delivered_at:
    # done, bookmarked and old notifications: assume delivered instantly
    Notification.undelivered.done.update_all("delivered_at = created_at")
    Notification.undelivered.bookmarked.update_all("delivered_at = created_at")
    Notification.undelivered.before(2.weeks.ago).update_all("delivered_at = created_at")

    # neither of the above, but older than last sent: assume when mails were last sent
    # Doesn't work: Notification.undelivered.joins(:user).where("users.noti_last_sent_at IS NOT NULL AND users.noti_last_sent_at > notifications.created_at").update_all("delivered_at = users.noti_last_sent_at")
    users_with_undelivered_notifications = Notification.undelivered.pluck(:user_id).uniq
    User.find(users_with_undelivered_notifications).each do |user|
      puts "User #{user.email} has #{user.notifications.undelivered.count} undelivered notifications. Assuming last sent at #{user.noti_last_sent_at}"

      # if a mail was sent, assume delivered at that time
      last_sent = user.noti_last_sent_at
      if last_sent
        res = user.notifications.undelivered.before(last_sent).update_all(delivered_at: last_sent)
        puts "Updated #{res} notifications with last_sent"
      end

      # if no mail was sent, or notifications created later, assume that any notification interacted with (which was marked as delivered above) implicates all earlier notifications delivered as well
      youngest_known_delivered_id = user.notifications.delivered.maximum(:id)
      if youngest_known_delivered_id
        res = user.notifications.undelivered.where("id < ?", youngest_known_delivered_id).update_all("delivered_at = created_at")
        puts "Updated #{res} notifications with youngest_known_delivered_id"
      end
    end
    # younger than last sent or user-never-sent: undelivered

    # initialize read_at:
    # done, bookmarked at interaction time
    Notification.unread.done.update_all("read_at = done_at")
    Notification.unread.bookmarked.update_all("delivered_at = bookmarked_at")
    # without interaction, they're not marked as read
  end
end
