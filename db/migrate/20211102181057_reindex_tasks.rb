class ReindexTasks < ActiveRecord::Migration[6.1]
  def change
    Task.reindex
  end
end
