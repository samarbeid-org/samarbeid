class FixContentItemFileTypeValues < ActiveRecord::Migration[6.0]
  def change
    ContentItem.where(content_type: "file").update_all(value: [])
  end
end
