class AddNewEngineStatesToBlock < ActiveRecord::Migration[6.0]
  def change
    add_column :blocks, :aasm_state, :string

    Block.update_all(aasm_state: "created")
    Block.all.find_each do |block|
      block.enable!
      block.complete! if block.may_complete?
    rescue => error
      puts error
    end
  end
end
