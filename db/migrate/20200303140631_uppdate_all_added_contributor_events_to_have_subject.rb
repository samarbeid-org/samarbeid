class UppdateAllAddedContributorEventsToHaveSubject < ActiveRecord::Migration[6.0]
  def change
    contributor = User.first

    Events::ProcessAddedContributorEvent.where(subject: nil).each do |event|
      event.update!(subject: contributor)
    end

    Events::TaskAddedContributorEvent.where(subject: nil).each do |event|
      event.update!(subject: contributor)
    end
  end
end

class Events::ProcessAddedContributorEvent < Event
  validate :validate_event_type_values

  def process
    object
  end

  def new_contributor
    data&.[](:new_contributor)
  end

  def added_contributor
    data&.[](:added_contributor)
  end

  alias_method :involved_user, :added_contributor

  def notification_receivers
  end

  private

  def validate_event_type_values
    errors.add(:object, "should be a process") unless process.is_a?(Workflow::Process)
  end
end

class Events::TaskAddedContributorEvent < Event
  validate :validate_event_type_values

  def task
    object
  end

  def new_contributor
    data&.[](:new_contributor)
  end

  def added_contributor
    data&.[](:added_contributor)
  end

  alias_method :involved_user, :added_contributor

  def title
    "Teilnehmer hinzugefügt"
  end

  def notification_receivers
  end

  private

  def validate_event_type_values
    errors.add(:object, "should be a task") unless task.is_a?(Workflow::Task)
  end
end
