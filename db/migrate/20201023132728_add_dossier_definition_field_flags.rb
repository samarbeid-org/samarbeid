class AddDossierDefinitionFieldFlags < ActiveRecord::Migration[6.0]
  def change
    add_column :dossier_definition_fields, :required, :boolean, null: false, default: false
    add_column :dossier_definition_fields, :recommended, :boolean, null: false, default: false
    add_column :dossier_definition_fields, :unique, :boolean, null: false, default: false
  end
end
