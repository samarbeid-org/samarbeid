class AddMarkedAttributeToWorkflowTasks < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_tasks, :marked, :boolean, default: false
  end
end
