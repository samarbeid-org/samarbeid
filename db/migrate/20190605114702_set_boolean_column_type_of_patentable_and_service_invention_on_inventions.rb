class SetBooleanColumnTypeOfPatentableAndServiceInventionOnInventions < ActiveRecord::Migration[5.2]
  def change
    rename_column :inventions, :patentable, :patentable_tmp
    rename_column :inventions, :service_invention, :service_invention_tmp
    add_column :inventions, :patentable, :boolean
    add_column :inventions, :service_invention, :boolean

    Invention.all.each do |invention|
      patentable_new = nil
      unless invention.patentable_tmp.nil?
        patentable_new = invention.patentable_tmp == 2
      end

      service_invention_new = nil
      unless invention.service_invention_tmp.nil?
        service_invention_new = invention.service_invention_tmp == 2
      end

      invention.update!(patentable: patentable_new, service_invention: service_invention_new)
    end

    remove_column :inventions, :patentable_tmp
    remove_column :inventions, :service_invention_tmp
  end
end
