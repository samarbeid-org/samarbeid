class ReindexWorkflowsForDueAt < ActiveRecord::Migration[6.1]
  def change
    ReindexSearchableJob.perform_later(Workflow)
  end
end
