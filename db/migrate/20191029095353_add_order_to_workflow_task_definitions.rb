class AddOrderToWorkflowTaskDefinitions < ActiveRecord::Migration[5.2]
  def change
    Workflow::TaskDefinition.all.destroy_all

    add_column :workflow_task_definitions, :order, :integer, null: false
    add_index :workflow_task_definitions, [:workflow_process_definition_id, :order], unique: true, name: :idx_wf_task_defs_on_wf_process_definition_id_and_order
  end
end
