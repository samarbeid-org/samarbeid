class ChangeThirdPartyFundsColumnOfInventions < ActiveRecord::Migration[5.2]
  def change
    rename_column :inventions, :third_party_funds, :service_invention
  end
end
