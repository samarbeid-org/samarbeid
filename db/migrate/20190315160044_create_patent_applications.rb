class CreatePatentApplications < ActiveRecord::Migration[5.2]
  def change
    create_table :patent_applications do |t|
      t.references :project, foreign_key: true
      t.references :workflow_process, foreign_key: true
      t.text :description
      t.text :prior_art
      t.text :object_of_invention
      t.text :patent_claim

      t.timestamps
    end
  end
end
