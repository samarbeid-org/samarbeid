class DropUploadedFilesTable < ActiveRecord::Migration[6.0]
  def change
    drop_table :uploaded_files
  end
end
