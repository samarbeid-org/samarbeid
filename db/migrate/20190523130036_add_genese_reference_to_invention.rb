class AddGeneseReferenceToInvention < ActiveRecord::Migration[5.2]
  def change
    add_column :inventions, :reference, :string
  end
end
