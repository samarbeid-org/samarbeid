class AddSystemIdentifierToWorkflowDefinitions < ActiveRecord::Migration[6.1]
  def up
    add_column :workflow_definitions, :system_identifier, :string
    add_index :workflow_definitions, :system_identifier

    wd = WorkflowDefinition.find_by(name: "Ohne Vorlage") || WorkflowDefinition.find_by(name: "Individuelle Maßnahme")
    if wd && wd.item_definitions.count == 1 && wd.item_definitions.first.is_a?(TaskDefinition)
      # A suitable workflow definition exists

      # Set correct values of workflow definition
      wd.update!(
        system_identifier: :system_process_definition_single_task,
        name: I18n.t("system_process_definition_single_task.name"),
        description: I18n.t("system_process_definition_single_task.description"),
        group_ids: Group.system_group_all.id
      )

      # Set correct values of task definition
      td = wd.item_definitions.first
      td.update!(name: I18n.t("system_process_definition_single_task.task.name"))

      # Find or create content item definition and task item definition for richtext field
      cid_description = wd.content_item_definitions.find_by(content_type: ContentTypes::Richtext.to_s)
      tid_description = nil
      if cid_description
        cid_description.update!(label: I18n.t("system_process_definition_single_task.task.items.description"))
        tid_description = cid_description.task_item_definitions.first
      else
        cid_description = wd.content_item_definitions.create!(
          workflow_definition_id: wd.id,
          label: I18n.t("system_process_definition_single_task.task.items.description"),
          content_type: ContentTypes::Richtext.to_s
        )

        last_position = wd.task_item_definitions.maximum(:position) || 0
        tid_description = cid_description.task_item_definitions.create!(
          position: last_position + 1,
          task_definition_id: td.id,
          workflow_definition_id: wd.id
        )
      end

      # Find or create content item definition and task item definition for file field
      cid_attachments = wd.content_item_definitions.find_by(content_type: ContentTypes::File.to_s)
      tid_attachments = nil
      if cid_attachments
        cid_attachments.update!(label: I18n.t("system_process_definition_single_task.task.items.attachments"), options: {multiple: true})
        tid_attachments = cid_attachments.task_item_definitions.first
      else
        cid_attachments = wd.content_item_definitions.create!(
          workflow_definition_id: wd.id,
          label: I18n.t("system_process_definition_single_task.task.items.attachments"),
          content_type: ContentTypes::File.to_s,
          options: {multiple: true}
        )

        last_position = wd.task_item_definitions.maximum(:position) || 0
        tid_attachments = cid_attachments.task_item_definitions.create!(
          position: last_position + 1,
          task_definition_id: td.id,
          workflow_definition_id: wd.id
        )
      end

      # Destroy all other content item definitions and task item definitions
      wd.content_item_definitions.where.not(id: [cid_description.id, cid_attachments.id]).each do |cid|
        cid.destroy!
      end

      # Reorder if necessary
      if tid_description.position > tid_attachments.position
        tid_attachments.update!(position: tid_description.position + 1)
      end
    else
      # No suitable workflow definition exists
      WorkflowDefinition.system_process_definition_single_task
    end
  end

  def down
    remove_column :workflow_definitions, :system_identifier, :string
  end
end
