class CreateContentItemDefinitions < ActiveRecord::Migration[6.0]
  def change
    create_table :content_item_definitions do |t|
      t.string :label
      t.string :content_type
      t.jsonb :options, default: {}, null: false
      t.string :default_value
      t.references :workflow_definition, null: false, foreign_key: true

      t.timestamps
    end

    add_reference :content_items, :content_item_definition
  end
end
