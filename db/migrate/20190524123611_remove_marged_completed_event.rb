class RemoveMargedCompletedEvent < ActiveRecord::Migration[5.2]
  def change
    Workflow::Event.where(event_type: "marked_completed").update_all(event_type: Workflow::Event::COMPLETED)
  end
end
