class CreateBlocks < ActiveRecord::Migration[6.0]
  def change
    create_table :blocks do |t|
      t.references :process, null: false
      t.integer :position, null: false
      t.boolean :parallel
      t.string :decision
      t.string :decision_option
      t.string :title
      t.timestamps
    end

    # link tasks to blocks XOR process AND add stuff now needed for tasks
    rename_table :workflow_tasks, :tasks
    add_reference :tasks, :process_or_block, polymorphic: true, null: true, index: {name: "index_tasks_belong_to_process_or_block"}
    add_column :tasks, :position, :integer
    add_column :tasks, :name, :string

    Workflow::Task.all.find_each do |task|
      task.process_or_block = task.process
      task.position = task.task_definition.position
      task.name = task.task_definition.name
      task.save!
    end
  end
end

class Workflow::Task < ApplicationRecord
  self.table_name = "tasks"

  belongs_to :process, class_name: "Workflow::Process", foreign_key: "workflow_process_id"
  belongs_to :task_definition, class_name: "Workflow::TaskDefinition", foreign_key: "workflow_task_definition_id" # , inverse_of: :tasks
  belongs_to :process_or_block, polymorphic: true, inverse_of: :tasks
end
