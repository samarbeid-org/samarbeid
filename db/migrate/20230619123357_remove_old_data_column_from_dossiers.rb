class RemoveOldDataColumnFromDossiers < ActiveRecord::Migration[6.1]
  def change
    remove_column :dossiers, :data
  end
end
