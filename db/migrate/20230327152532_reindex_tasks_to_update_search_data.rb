class ReindexTasksToUpdateSearchData < ActiveRecord::Migration[6.1]
  def change
    CustomElasticSearchConfig.reindex_model(Task)
  end
end
