class AddDefaultPageAndMenuEntries < ActiveRecord::Migration[6.1]
  def change
    if MenuEntry.none?
      MenuEntry.create!(location: :footer_menu, position: 30, title: "samarbeid.org", url: "https://samarbeid.org", new_window: true)
    end
    if Page.none?
      Page.create!(slug: "impressum", title: "Impressum", content: "Hier können Sie ihr Impressum angeben. Bearbeiten als Admin unter 'Zahnrad' > 'Statische Seiten'")
      MenuEntry.create!(location: :footer_menu, position: MenuEntry.pluck(:position).max.to_i + 10, title: "Impressum", page: Page.find_by!(slug: "impressum"))
    end
  end
end
