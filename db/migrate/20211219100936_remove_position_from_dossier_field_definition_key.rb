class RemovePositionFromDossierFieldDefinitionKey < ActiveRecord::Migration[6.1]
  def change
    remove_index :dossier_field_definitions, [:definition_id, :position]
  end
end
