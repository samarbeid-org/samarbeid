class RenameArchivedToClosedOnAmbtions < ActiveRecord::Migration[6.0]
  def change
    rename_column :ambitions, :archived, :closed
  end
end
