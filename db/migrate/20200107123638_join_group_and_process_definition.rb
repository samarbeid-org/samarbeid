class JoinGroupAndProcessDefinition < ActiveRecord::Migration[5.2]
  def change
    create_join_table :groups, :workflow_process_definitions do |t|
      t.index :group_id
      t.index :workflow_process_definition_id, name: "index_groups_workflow_pd_on_workflow_process_definition_id"
      t.index [:group_id, :workflow_process_definition_id], unique: true, name: "unique_index_groups_process_definitions_join"
    end
  end
end
