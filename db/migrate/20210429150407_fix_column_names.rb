class FixColumnNames < ActiveRecord::Migration[6.0]
  def change
    rename_column :task_definitions, :definition_process_or_block_type, :definition_workflow_or_block_type
    rename_column :task_definitions, :definition_process_or_block_id, :definition_workflow_or_block_id
  end
end
