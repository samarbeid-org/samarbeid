class RenameDossierDefinitionFields < ActiveRecord::Migration[6.0]
  def change
    rename_table :dossier_definition_fields, :dossier_field_definitions
  end
end
