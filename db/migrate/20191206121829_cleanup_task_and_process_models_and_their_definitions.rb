class CleanupTaskAndProcessModelsAndTheirDefinitions < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_tasks, :workflow_task_definition_id, :bigint, foreign_key: true, index: true

    Workflow::Task.find_each do |task|
      task.task_definition = task.process.definition.task_definitions.find { |td| td.key == task.key }
      task.save!
    end
    change_column_null :workflow_tasks, :workflow_task_definition_id, false
    remove_column :workflow_tasks, :name, :string
    remove_column :workflow_tasks, :description, :text
  end
end
