class RemovePaperTrailBasedDataChanges < ActiveRecord::Migration[6.0]
  def change
    drop_table :versions
    drop_table :version_associations
  end
end
