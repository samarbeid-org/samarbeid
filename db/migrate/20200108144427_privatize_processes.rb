class PrivatizeProcesses < ActiveRecord::Migration[5.2]
  def change
    add_column :workflow_processes, :private, :boolean, default: false
    add_column :ambitions, :private, :boolean, default: false
  end
end
