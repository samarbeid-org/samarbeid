class AddProcessInstanceIdToInventions < ActiveRecord::Migration[5.2]
  def change
    add_column :inventions, :process_instance_id, :string
  end
end
