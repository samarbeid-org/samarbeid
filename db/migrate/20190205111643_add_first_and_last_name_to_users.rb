class AddFirstAndLastNameToUsers < ActiveRecord::Migration[5.2]
  def change
    add_column :users, :firstname, :string, null: false, default: ""
    add_column :users, :lastname, :string, null: false, default: ""

    change_column_default :users, :firstname, nil
    change_column_default :users, :lastname, nil
  end
end
