# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2025_03_11_101705) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", precision: nil, null: false
    t.string "service_name", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", force: :cascade do |t|
    t.bigint "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "automations", force: :cascade do |t|
    t.string "title", null: false
    t.boolean "active", null: false
    t.jsonb "schedule", null: false
    t.date "next_execution", null: false
    t.bigint "workflow_definition_id", null: false
    t.bigint "candidate_assignee_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["candidate_assignee_id"], name: "index_automations_on_candidate_assignee_id"
    t.index ["title"], name: "index_automations_on_title", unique: true
    t.index ["workflow_definition_id"], name: "index_automations_on_workflow_definition_id"
  end

  create_table "block_definitions", force: :cascade do |t|
    t.bigint "workflow_definition_id", null: false
    t.integer "position", null: false
    t.boolean "parallel", default: false, null: false
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "content_item_definition_id"
    t.jsonb "decision"
    t.index ["content_item_definition_id"], name: "index_block_definitions_on_content_item_definition_id"
    t.index ["workflow_definition_id"], name: "index_block_definitions_on_workflow_definition_id"
  end

  create_table "blocks", force: :cascade do |t|
    t.bigint "workflow_id", null: false
    t.integer "position", null: false
    t.boolean "parallel", default: false, null: false
    t.string "title"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "aasm_state"
    t.bigint "content_item_id"
    t.bigint "block_definition_id"
    t.jsonb "decision"
    t.index ["block_definition_id"], name: "index_blocks_on_block_definition_id"
    t.index ["content_item_id"], name: "index_blocks_on_content_item_id"
    t.index ["workflow_id"], name: "index_blocks_on_workflow_id"
  end

  create_table "comments", force: :cascade do |t|
    t.text "message"
    t.bigint "author_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "object_type", null: false
    t.bigint "object_id", null: false
    t.datetime "deleted_at"
    t.index ["author_id"], name: "index_comments_on_author_id"
    t.index ["object_type", "object_id"], name: "index_comments_on_object_type_and_object_id"
  end

  create_table "content_item_definitions", force: :cascade do |t|
    t.string "label"
    t.string "content_type"
    t.jsonb "options", default: {}, null: false
    t.jsonb "default_value"
    t.bigint "workflow_definition_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer "start_item_position"
    t.integer "title_position"
    t.index ["workflow_definition_id"], name: "index_content_item_definitions_on_workflow_definition_id"
  end

  create_table "content_items", force: :cascade do |t|
    t.string "label"
    t.string "content_type"
    t.jsonb "options", default: {}, null: false
    t.jsonb "value"
    t.bigint "workflow_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "content_item_definition_id"
    t.string "aasm_state", null: false
    t.datetime "confirmed_at", precision: nil
    t.bigint "lock_version", default: 0, null: false
    t.datetime "locked_at", precision: nil
    t.string "locked_by_type"
    t.bigint "locked_by_id"
    t.index ["content_item_definition_id"], name: "index_content_items_on_content_item_definition_id"
    t.index ["locked_by_type", "locked_by_id"], name: "index_content_items_on_locked_by"
    t.index ["workflow_id"], name: "index_content_items_on_workflow_id"
  end

  create_table "contributions", force: :cascade do |t|
    t.string "contributable_type", null: false
    t.bigint "contributable_id", null: false
    t.bigint "user_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["contributable_type", "contributable_id"], name: "index_contributions_on_contributable_type_and_contributable_id"
    t.index ["user_id"], name: "index_contributions_on_user_id"
  end

  create_table "data_transformation_items", force: :cascade do |t|
    t.bigint "data_transformation_id", null: false
    t.bigint "dossier_id", null: false
    t.jsonb "value"
    t.string "error_messages"
    t.boolean "confirmed", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["data_transformation_id"], name: "index_data_transformation_items_on_data_transformation_id"
    t.index ["dossier_id"], name: "index_data_transformation_items_on_dossier_id"
  end

  create_table "data_transformations", force: :cascade do |t|
    t.bigint "dossier_item_definition_id", null: false
    t.string "content_type", null: false
    t.jsonb "options", default: {}, null: false
    t.boolean "required"
    t.boolean "unique"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dossier_item_definition_id"], name: "index_data_transformations_on_dossier_item_definition_id"
  end

  create_table "dossier_definitions", force: :cascade do |t|
    t.string "name", null: false
    t.text "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "title_fields", default: []
    t.jsonb "subtitle_fields", default: []
    t.index ["name"], name: "index_dossier_definitions_on_name", unique: true
  end

  create_table "dossier_definitions_groups", id: false, force: :cascade do |t|
    t.bigint "dossier_definition_id", null: false
    t.bigint "group_id", null: false
    t.index ["dossier_definition_id", "group_id"], name: "dossier_def_on_group_unique", unique: true
  end

  create_table "dossier_item_definitions", force: :cascade do |t|
    t.bigint "definition_id", null: false
    t.string "name"
    t.integer "position", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "required", default: false, null: false
    t.boolean "recommended", default: false, null: false
    t.boolean "unique", default: false, null: false
    t.string "content_type", null: false
    t.jsonb "options", default: {}, null: false
    t.jsonb "default_value"
    t.index ["definition_id", "name"], name: "index_dossier_item_definitions_on_definition_id_and_name", unique: true
    t.index ["definition_id"], name: "index_dossier_item_definitions_on_definition_id"
  end

  create_table "dossier_items", force: :cascade do |t|
    t.bigint "dossier_id", null: false
    t.bigint "dossier_item_definition_id", null: false
    t.jsonb "value"
    t.bigint "lock_version", default: 0, null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["dossier_id"], name: "index_dossier_items_on_dossier_id"
    t.index ["dossier_item_definition_id"], name: "index_dossier_items_on_dossier_item_definition_id"
  end

  create_table "dossiers", force: :cascade do |t|
    t.bigint "definition_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "lock_version", default: 0, null: false
    t.string "title"
    t.string "subtitle"
    t.index ["definition_id"], name: "index_dossiers_on_definition_id"
  end

  create_table "events", force: :cascade do |t|
    t.string "type", null: false
    t.string "object_type", null: false
    t.bigint "object_id", null: false
    t.bigint "subject_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "data"
    t.bigint "superseder_id"
    t.index ["object_type", "object_id"], name: "index_events_on_object_type_and_object_id"
    t.index ["subject_id"], name: "index_events_on_subject_id"
    t.index ["superseder_id"], name: "index_events_on_superseder_id"
  end

  create_table "good_job_batches", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.jsonb "serialized_properties"
    t.text "on_finish"
    t.text "on_success"
    t.text "on_discard"
    t.text "callback_queue_name"
    t.integer "callback_priority"
    t.datetime "enqueued_at", precision: nil
    t.datetime "discarded_at", precision: nil
    t.datetime "finished_at", precision: nil
  end

  create_table "good_job_executions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "active_job_id", null: false
    t.text "job_class"
    t.text "queue_name"
    t.jsonb "serialized_params"
    t.datetime "scheduled_at", precision: nil
    t.datetime "finished_at", precision: nil
    t.text "error"
    t.integer "error_event", limit: 2
    t.text "error_backtrace", array: true
    t.uuid "process_id"
    t.interval "duration"
    t.index ["active_job_id", "created_at"], name: "index_good_job_executions_on_active_job_id_and_created_at"
    t.index ["process_id", "created_at"], name: "index_good_job_executions_on_process_id_and_created_at"
  end

  create_table "good_job_processes", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.jsonb "state"
    t.integer "lock_type", limit: 2
  end

  create_table "good_job_settings", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "key"
    t.jsonb "value"
    t.index ["key"], name: "index_good_job_settings_on_key", unique: true
  end

  create_table "good_jobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.text "queue_name"
    t.integer "priority"
    t.jsonb "serialized_params"
    t.datetime "scheduled_at", precision: nil
    t.datetime "performed_at", precision: nil
    t.datetime "finished_at", precision: nil
    t.text "error"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.uuid "active_job_id"
    t.text "concurrency_key"
    t.text "cron_key"
    t.uuid "retried_good_job_id"
    t.datetime "cron_at", precision: nil
    t.uuid "batch_id"
    t.uuid "batch_callback_id"
    t.boolean "is_discrete"
    t.integer "executions_count"
    t.text "job_class"
    t.integer "error_event", limit: 2
    t.text "labels", array: true
    t.uuid "locked_by_id"
    t.datetime "locked_at", precision: nil
    t.index ["active_job_id", "created_at"], name: "index_good_jobs_on_active_job_id_and_created_at"
    t.index ["batch_callback_id"], name: "index_good_jobs_on_batch_callback_id", where: "(batch_callback_id IS NOT NULL)"
    t.index ["batch_id"], name: "index_good_jobs_on_batch_id", where: "(batch_id IS NOT NULL)"
    t.index ["concurrency_key"], name: "index_good_jobs_on_concurrency_key_when_unfinished", where: "(finished_at IS NULL)"
    t.index ["cron_key", "created_at"], name: "index_good_jobs_on_cron_key_and_created_at_cond", where: "(cron_key IS NOT NULL)"
    t.index ["cron_key", "cron_at"], name: "index_good_jobs_on_cron_key_and_cron_at_cond", unique: true, where: "(cron_key IS NOT NULL)"
    t.index ["finished_at"], name: "index_good_jobs_jobs_on_finished_at", where: "((retried_good_job_id IS NULL) AND (finished_at IS NOT NULL))"
    t.index ["labels"], name: "index_good_jobs_on_labels", where: "(labels IS NOT NULL)", using: :gin
    t.index ["locked_by_id"], name: "index_good_jobs_on_locked_by_id", where: "(locked_by_id IS NOT NULL)"
    t.index ["priority", "created_at"], name: "index_good_job_jobs_for_candidate_lookup", where: "(finished_at IS NULL)"
    t.index ["priority", "created_at"], name: "index_good_jobs_jobs_on_priority_created_at_when_unfinished", order: { priority: "DESC NULLS LAST" }, where: "(finished_at IS NULL)"
    t.index ["priority", "scheduled_at"], name: "index_good_jobs_on_priority_scheduled_at_unfinished_unlocked", where: "((finished_at IS NULL) AND (locked_by_id IS NULL))"
    t.index ["queue_name", "scheduled_at"], name: "index_good_jobs_on_queue_name_and_scheduled_at", where: "(finished_at IS NULL)"
    t.index ["scheduled_at"], name: "index_good_jobs_on_scheduled_at", where: "(finished_at IS NULL)"
  end

  create_table "groups", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "description"
    t.string "system_identifier"
    t.index ["system_identifier"], name: "index_groups_on_system_identifier"
  end

  create_table "groups_workflow_definitions", id: false, force: :cascade do |t|
    t.bigint "group_id", null: false
    t.bigint "workflow_definition_id", null: false
    t.index ["group_id", "workflow_definition_id"], name: "unique_index_groups_process_definitions_join", unique: true
    t.index ["group_id"], name: "index_groups_workflow_definitions_on_group_id"
    t.index ["workflow_definition_id"], name: "index_groups_workflow_pd_on_workflow_process_definition_id"
  end

  create_table "main_menu_entries", force: :cascade do |t|
    t.bigint "parent_id"
    t.integer "position", null: false
    t.string "title"
    t.string "linked_object_type"
    t.bigint "linked_object_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["linked_object_type", "linked_object_id"], name: "index_main_menu_entries_on_linked_object"
    t.index ["parent_id", "position"], name: "index_main_menu_entries_on_parent_id_and_position"
    t.index ["parent_id"], name: "index_main_menu_entries_on_parent_id"
  end

  create_table "menu_entries", force: :cascade do |t|
    t.string "location", null: false
    t.integer "position", null: false
    t.string "title", null: false
    t.bigint "page_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "url"
    t.boolean "new_window", default: false
    t.index ["location", "position"], name: "index_menu_entries_on_location_and_position", unique: true
  end

  create_table "news", force: :cascade do |t|
    t.string "url", null: false
    t.string "title", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.text "description"
    t.index ["url"], name: "index_news_on_url", unique: true
  end

  create_table "notifications", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "event_id", null: false
    t.datetime "done_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.datetime "bookmarked_at", precision: nil
    t.datetime "delivered_at", precision: nil
    t.datetime "read_at", precision: nil
    t.index ["event_id"], name: "index_notifications_on_event_id"
    t.index ["user_id", "event_id"], name: "index_notifications_on_user_id_and_event_id", unique: true
    t.index ["user_id"], name: "index_notifications_on_user_id"
  end

  create_table "pages", force: :cascade do |t|
    t.string "slug", null: false
    t.string "title", null: false
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["slug"], name: "index_pages_on_slug", unique: true
  end

  create_table "search_documents", force: :cascade do |t|
    t.string "identifier"
    t.string "title"
    t.string "subtitle"
    t.text "content"
    t.string "searchable_type", null: false
    t.bigint "searchable_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index "((((setweight(to_tsvector('german'::regconfig, COALESCE((identifier)::text, ''::text)), 'A'::\"char\") || setweight(to_tsvector('german'::regconfig, COALESCE((title)::text, ''::text)), 'A'::\"char\")) || setweight(to_tsvector('german'::regconfig, COALESCE((subtitle)::text, ''::text)), 'B'::\"char\")) || setweight(to_tsvector('german'::regconfig, COALESCE(content, ''::text)), 'C'::\"char\")))", name: "index_search_documents_on_full_text", using: :gin
    t.index ["identifier"], name: "index_search_documents_on_identifier", unique: true
    t.index ["searchable_type", "searchable_id"], name: "index_search_documents_on_searchable", unique: true
  end

  create_table "settings", force: :cascade do |t|
    t.string "key"
    t.text "value"
    t.index ["key"], name: "index_settings_on_key", unique: true
  end

  create_table "share_links", force: :cascade do |t|
    t.string "name"
    t.string "token", null: false
    t.string "shareable_type", null: false
    t.bigint "shareable_id", null: false
    t.string "aasm_state", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["shareable_type", "shareable_id"], name: "index_share_links_on_shareable"
    t.index ["token"], name: "index_share_links_on_token", unique: true
  end

  create_table "task_definitions", force: :cascade do |t|
    t.string "name", null: false
    t.bigint "workflow_definition_id", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.integer "position"
    t.string "definition_workflow_or_block_type"
    t.bigint "definition_workflow_or_block_id"
    t.integer "due_in_days"
    t.bigint "candidate_assignee_id"
    t.integer "deferral_in_days"
    t.boolean "candidate_assignee_from_workflow", default: false
    t.index ["candidate_assignee_id"], name: "index_task_definitions_on_candidate_assignee_id"
    t.index ["definition_workflow_or_block_type", "definition_workflow_or_block_id"], name: "index_definition_tasks_belong_to_process_or_block"
    t.index ["workflow_definition_id"], name: "idx_workflow_task_definitions_on_workflow_process_definition_id"
  end

  create_table "task_item_definitions", force: :cascade do |t|
    t.bigint "task_definition_id", null: false
    t.bigint "content_item_definition_id", null: false
    t.integer "position", null: false
    t.boolean "required"
    t.boolean "info_box"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "workflow_definition_id", null: false
    t.boolean "add_to_calendar"
    t.index ["content_item_definition_id"], name: "index_task_item_definitions_on_content_item_definition_id"
    t.index ["task_definition_id"], name: "index_task_item_definitions_on_task_definition_id"
    t.index ["workflow_definition_id"], name: "index_task_item_definitions_on_workflow_definition_id"
  end

  create_table "task_items", force: :cascade do |t|
    t.bigint "task_id", null: false
    t.bigint "content_item_id", null: false
    t.integer "position", null: false
    t.boolean "required", default: false
    t.boolean "info_box", default: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.bigint "task_item_definition_id"
    t.boolean "add_to_calendar"
    t.index ["content_item_id"], name: "index_task_items_on_content_item_id"
    t.index ["task_id"], name: "index_task_items_on_task_id"
    t.index ["task_item_definition_id"], name: "index_task_items_on_task_item_definition_id"
  end

  create_table "tasks", force: :cascade do |t|
    t.string "aasm_state", null: false
    t.date "due_at"
    t.bigint "workflow_id", null: false
    t.bigint "assignee_id"
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.datetime "completed_at", precision: nil
    t.bigint "task_definition_id"
    t.datetime "state_updated_at", precision: nil, default: -> { "now()" }, null: false
    t.string "workflow_or_block_type"
    t.bigint "workflow_or_block_id"
    t.integer "position"
    t.string "name"
    t.datetime "start_at", precision: nil
    t.bigint "lock_version", default: 0, null: false
    t.date "due_processed_last"
    t.string "old_aasm_state"
    t.integer "time_tracking_budget_in_minutes", default: 0, null: false
    t.index ["assignee_id"], name: "index_tasks_on_assignee_id"
    t.index ["workflow_id"], name: "index_tasks_on_workflow_id"
    t.index ["workflow_or_block_type", "workflow_or_block_id"], name: "index_tasks_belong_to_process_or_block"
  end

  create_table "uploaded_files", force: :cascade do |t|
    t.string "title"
    t.text "file_as_text"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_groups", force: :cascade do |t|
    t.bigint "user_id", null: false
    t.bigint "group_id", null: false
    t.index ["group_id", "user_id"], name: "index_user_groups_on_group_id_and_user_id", unique: true
    t.index ["user_id", "group_id"], name: "index_user_groups_on_user_id_and_group_id", unique: true
  end

  create_table "users", force: :cascade do |t|
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.datetime "remember_created_at", precision: nil
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.string "firstname", null: false
    t.string "lastname", null: false
    t.bigint "noti_interval", default: 1, null: false
    t.integer "role", default: 0
    t.datetime "deactivated_at", precision: nil
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at", precision: nil
    t.string "confirmation_token"
    t.datetime "confirmed_at", precision: nil
    t.datetime "confirmation_sent_at", precision: nil
    t.string "unconfirmed_email"
    t.string "calendar_token"
    t.string "api_token"
    t.index ["api_token"], name: "index_users_on_api_token", unique: true
    t.index ["calendar_token"], name: "index_users_on_calendar_token", unique: true
    t.index ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_workflow_tasks_marked", id: false, force: :cascade do |t|
    t.bigint "workflow_task_id", null: false
    t.bigint "user_id", null: false
    t.index ["user_id", "workflow_task_id"], name: "idx_users_workflow_tasks_marked_on_user_id_and_workflow_task_id", unique: true
    t.index ["user_id"], name: "index_users_workflow_tasks_marked_on_user_id"
    t.index ["workflow_task_id"], name: "index_users_workflow_tasks_marked_on_workflow_task_id"
  end

  create_table "webhook_definitions", force: :cascade do |t|
    t.string "url"
    t.string "auth_token"
    t.boolean "wait_for_response", default: true
    t.bigint "task_definition_id", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "notify_after_execution", default: true
    t.index ["task_definition_id"], name: "index_webhook_definitions_on_task_definition_id"
  end

  create_table "webhooks", force: :cascade do |t|
    t.string "url"
    t.string "auth_token"
    t.boolean "wait_for_response", default: true
    t.bigint "task_id", null: false
    t.bigint "webhook_definition_id"
    t.integer "response_code"
    t.text "response_body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.boolean "notify_after_execution", default: true
    t.index ["task_id"], name: "index_webhooks_on_task_id"
    t.index ["webhook_definition_id"], name: "index_webhooks_on_webhook_definition_id"
  end

  create_table "workflow_definitions", force: :cascade do |t|
    t.string "name", null: false
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.text "description"
    t.integer "version", default: 0
    t.boolean "auto_generate_title", default: false
    t.string "system_identifier"
    t.datetime "deactivated_at", precision: nil
    t.index ["name"], name: "index_workflow_definitions_on_name", unique: true
    t.index ["system_identifier"], name: "index_workflow_definitions_on_system_identifier"
  end

  create_table "workflows", force: :cascade do |t|
    t.datetime "created_at", precision: nil, null: false
    t.datetime "updated_at", precision: nil, null: false
    t.bigint "workflow_definition_id", null: false
    t.datetime "completed_at", precision: nil
    t.string "title"
    t.bigint "assignee_id"
    t.bigint "predecessor_workflow_id"
    t.text "description"
    t.datetime "state_updated_at", precision: nil, default: -> { "now()" }, null: false
    t.string "aasm_state"
    t.integer "definition_version"
    t.string "system_identifier"
    t.index ["assignee_id"], name: "index_workflows_on_assignee_id"
    t.index ["predecessor_workflow_id"], name: "index_workflows_on_predecessor_workflow_id"
    t.index ["workflow_definition_id"], name: "index_workflows_on_workflow_definition_id"
  end

  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "automations", "users", column: "candidate_assignee_id"
  add_foreign_key "automations", "workflow_definitions"
  add_foreign_key "comments", "users", column: "author_id"
  add_foreign_key "content_item_definitions", "workflow_definitions"
  add_foreign_key "content_items", "workflows"
  add_foreign_key "contributions", "users"
  add_foreign_key "dossier_item_definitions", "dossier_definitions", column: "definition_id"
  add_foreign_key "dossiers", "dossier_definitions", column: "definition_id"
  add_foreign_key "events", "users", column: "subject_id"
  add_foreign_key "menu_entries", "pages"
  add_foreign_key "notifications", "events"
  add_foreign_key "notifications", "users"
  add_foreign_key "task_definitions", "users", column: "candidate_assignee_id"
  add_foreign_key "task_definitions", "workflow_definitions"
  add_foreign_key "task_item_definitions", "content_item_definitions"
  add_foreign_key "task_item_definitions", "task_definitions"
  add_foreign_key "task_item_definitions", "workflow_definitions"
  add_foreign_key "task_items", "content_items"
  add_foreign_key "task_items", "tasks"
  add_foreign_key "tasks", "users", column: "assignee_id"
  add_foreign_key "tasks", "workflows"
  add_foreign_key "webhook_definitions", "task_definitions"
  add_foreign_key "webhooks", "tasks"
  add_foreign_key "webhooks", "webhook_definitions"
  add_foreign_key "workflows", "users", column: "assignee_id"
  add_foreign_key "workflows", "workflow_definitions"
  add_foreign_key "workflows", "workflows", column: "predecessor_workflow_id"
end
