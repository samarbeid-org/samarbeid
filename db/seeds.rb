# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).

puts "SEEDing Database with initial Samarbeid Team super-admin user admin@example.org and default imprint page"

# Create system group 'all' and system process definition 'without definition'
Group.system_group_all
WorkflowDefinition.system_process_definition_single_task

Page.create!(slug: "impressum", title: "Impressum", content: "Hier können Sie ihr Impressum angeben. Bearbeiten als Admin unter 'Zahnrad' > 'Statische Seiten'")
MenuEntry.create!(location: :footer_menu, position: 1, title: "Impressum", page: Page.find_by!(slug: "impressum"))
MenuEntry.create!(location: :footer_menu, position: 2, title: "samarbeid.org", url: "https://samarbeid.org", new_window: true)

Rake::Task["db:create_super_admin"].invoke
