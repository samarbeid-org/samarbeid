require "test_helper"
require "minitest/retry"
require "system_test_browser_console_helper"

Minitest::Retry.use! if ENV["CI"]
Minitest::Retry.on_retry do |klass, test_name, retry_count, result|
  Sentry.with_scope do |scope|
    scope.set_extras(test_class: klass.to_s, test_name: test_name, result: result, retry_count: retry_count, gitlab_job_url: ENV["CI_JOB_URL"])
    Sentry.capture_message "Minitest::Retry triggered by flaky test #{klass} #{test_name}"
  end
end

Capybara::Lockstep.debug = true if ENV["DEBUG_LOCKSTEP"]

# https://github.com/bblimke/webmock/blob/master/README.md#connecting-on-nethttpstart
WebMock.allow_net_connect!(net_http_connect_on_start: true)

class ApplicationSystemTestCase < ActionDispatch::SystemTestCase
  puts "For a live view use: 'SYSTEM_TEST_BROWSER=chrome PARALLEL_WORKERS=1 rails test:system'"

  browser = ENV["SYSTEM_TEST_BROWSER"]&.to_sym || :headless_chrome

  url = ENV.fetch("SELENIUM_URL", nil)
  options = if url
    {browser: :remote, url: url}
  else
    {browser: :chrome}
  end

  driven_by :selenium, using: browser,
    # Selenium defaults - we can chose whatever
    screen_size: [1360, 1020],
    # If running on CI we use a remote instance
    options: options do |driver_options|
    driver_options.add_argument("--headless=new") if browser == :headless_chrome
    driver_options.add_argument("--disable-search-engine-choice-screen")
    # driver_options.add_argument("--disable-dev-shm-usage") if ENV["CI"]

    driver_options.add_extension(Rails.root.join("test", "browser_extensions", "vue_js_devtools_5.3.4_0.crx")) if browser == :chrome

    # uncomment to get all levels of browser logs (but only for debugging purposes)
    # driver_options.add_option("goog:loggingPrefs", {browser: 'ALL'})
  end

  Capybara.default_max_wait_time = 2.seconds.to_i
  Capybara.default_max_wait_time = 10.seconds.to_i if ENV["CI"]

  include Warden::Test::Helpers # allows login_as(@user)

  Warden.test_mode!

  setup do
    # We must also tell SystemTest where to find rails server
    Capybara.app_host = "http://#{Capybara.server_host}:#{Capybara.server_port}"

    setup_browser_console
  end

  teardown do
    Warden.test_reset!
    Capybara.reset_sessions!
    Capybara.use_default_driver
  end

  def before_teardown
    assert_empty_browser_console_logs
  end

  # Retries block if exceptions occurs
  def retry_flaky_actions(retries: 5, sleep: 0.5, show_puts: true, raise: true, &block)
    count ||= 0
    caller_path = caller_locations(1..1).first.path

    yield block

    count
  rescue Minitest::Assertion, StandardError => exception
    count += 1
    messages = []

    backtrace_line = exception.backtrace.find { |l| l.match(caller_path) } ||
      exception.backtrace.find { |l| l.match(Rails.root.to_s) }

    if backtrace_line
      backtrace_line.slice!("#{Rails.root}/")
      test_file_and_line = backtrace_line.split(":").take(2).join(":")

      messages << "Retrying flaky actions in [#{test_file_and_line}]"
    end

    messages << if exception.is_a?(Minitest::Assertion)
      "Assertion failed '#{exception.message.tr("\n", " ")}'"
    else
      "Exception occurred '#{exception.message.tr("\n", " ")}'"
    end
    messages << "Waiting #{sleep} sec before trying again (#{count})"
    puts "\n" + messages.join(". ") if show_puts

    Sentry.with_scope do |scope|
      scope.set_extras(test_class: test_file_and_line, result: messages, retry_count: retries, gitlab_job_url: ENV["CI_JOB_URL"])
      Sentry.capture_message "retry_flaky_actions triggered by: #{test_file_and_line}" if raise # otherwise we are just testing this method
    end

    sleep sleep
    retry if count < retries

    raise exception if raise

    count
  end
end
