require "test_helper"

class MentioningsHelperTest < ActionView::TestCase
  test "add_mention_labels should work for present and non present users" do
    admin_user = User.find_by!(email: "admin@example.org")

    content_without_labels = 'Referencing a existing user <mention m-id="1" m-type="user"></mention> and non existing <mention m-id="23423434" m-type="user" :deleted="true"></mention>'
    converted_content = add_mention_labels(content_without_labels, admin_user)

    assert_equal 'Referencing a existing user <mention m-id="1" m-type="user" :noaccess="false" :deleted="false">@Admin User</mention> and non existing <mention m-id="23423434" m-type="user" :deleted="true" :noaccess="false"></mention>',
      converted_content
  end
end
