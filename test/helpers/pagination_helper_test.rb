require "test_helper"

class PaginationHelperTest < ActionView::TestCase
  test "#total_page_count_for should calculate right results" do
    expectations = [
      [100, 25, 4],
      [99, 25, 4],
      [101, 25, 5]
    ]
    expectations.each do |results, per_page, total_pages|
      result = OpenStruct.new(total_count: results, per_page: per_page)
      assert_equal total_pages, total_page_count_for(result)
    end
  end
end
