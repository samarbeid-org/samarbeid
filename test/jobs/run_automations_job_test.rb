require "test_helper"

class RunAutomationsJobTest < ActiveJob::TestCase
  test "job should work" do
    assert_nothing_raised { RunAutomationsJob.new.perform }
  end
end
