require "test_helper"

class FindAndStartSnoozedTasksJobTest < ActiveJob::TestCase
  test "job should work" do
    assert_nothing_raised { FindAndStartSnoozedTasksJob.new.perform }
  end
end
