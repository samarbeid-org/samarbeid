require "test_helper"

class ImportRssFeedJobTest < ActiveJob::TestCase
  test "importing and updating news from rss feed should work" do
    VCR.use_cassette("rss_feed") do
      old_news = News.first
      assert_changes -> { old_news.title } do
        assert_changes -> { old_news.updated_at } do
          assert_difference -> { News.count } => 18,
            -> { Events::FeatureReleasedEvent.count } => 18,
            -> { Notification.count } => 18 * User.count do
            ImportRssFeedJob.perform_now
            old_news.reload
          end
        end
      end
    end
  end

  test "a second import should not change anything" do
    VCR.use_cassette("rss_feed") do
      ImportRssFeedJob.perform_now

      assert_no_difference %w[News.count Events::FeatureReleasedEvent.count Notification.count] do
        ImportRssFeedJob.perform_now
      end
    end
  end
end
