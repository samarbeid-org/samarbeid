require "test_helper"

class GoodJobSettingsTest < ActiveJob::TestCase
  test "running failing job in production or staging env should not raise error but report error to sentry" do
    mock_environment(env: "production") do
      load "application_job.rb"

      perform_enqueued_jobs do
        assert_nothing_raised do
          JobWhichAlwaysFailsJob.perform_later
        end
      end
    end

    mock_environment(env: "staging") do
      load "application_job.rb"

      perform_enqueued_jobs do
        assert_nothing_raised do
          JobWhichAlwaysFailsJob.perform_later
        end
      end
    end
  end

  test "running failing job in test or development env should raise error" do
    skip "This test was flaky and has a low priority"

    mock_environment(env: "test") do
      load "application_job.rb"

      error_raised = false
      perform_enqueued_jobs do
        JobWhichAlwaysFailsJob.perform_later
      rescue ActiveRecord::RecordNotFound
        error_raised = true
      end
      assert error_raised, "In test environment the job should have raised an ActiveRecord::RecordNotFound error"
    end

    mock_environment(env: "development") do
      load "application_job.rb"

      error_raised = false
      perform_enqueued_jobs do
        JobWhichAlwaysFailsJob.perform_later
      rescue ActiveRecord::RecordNotFound
        error_raised = true
      end
      assert error_raised, "In development environment the job should have raised an ActiveRecord::RecordNotFound error"
    end
  end

  test "running job not inheriting from ApplicationJob should work" do
    perform_enqueued_jobs do
      assert_nothing_raised do
        MailerAlwaysFailing.perform_later
      end
    end
  end
end

class MailerAlwaysFailing < ActionMailer::MailDeliveryJob
  def perform
    raise ActiveRecord::RecordNotFound, "We are using a exception which is not reported to Sentry"
  end
end

class JobWhichAlwaysFailsJob < ApplicationJob
  def perform
    raise ActiveRecord::RecordNotFound, "We are using a exception which is not reported to Sentry"
  end
end
