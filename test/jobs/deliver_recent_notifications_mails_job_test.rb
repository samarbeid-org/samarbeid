require "test_helper"

class DeliverRecentNotificationsMailsJobTest < ActiveJob::TestCase
  class EmailDeliveryObserver
    attr_accessor :delivered_emails
    def initialize
      self.delivered_emails = []
    end

    def count
      delivered_emails.length
    end

    def delivered_email(message)
      delivered_emails << message
    end
  end

  test "running job should work" do
    current_user = User.second
    user = User.first

    Notification.destroy_all
    assert_equal 0, User.for_sending_notifications_every(user.noti_interval).length

    # trigger a notification
    workflow = Workflow.first
    Interactors::WorkflowInteractor.update_assignee(workflow, user, current_user)

    assert_equal 1, User.for_sending_notifications_every(user.noti_interval).length

    job_email_delivery_observer = EmailDeliveryObserver.new
    ActionMailer::Base.register_observer(job_email_delivery_observer)
    perform_enqueued_jobs do
      assert_nothing_raised { DeliverRecentNotificationsMailsJob.perform_now(user.noti_interval) }
    end
    ActionMailer::Base.unregister_observer(job_email_delivery_observer)

    user.reload

    assert_equal 0, User.for_sending_notifications_every(user.noti_interval).length
    assert_equal 1, job_email_delivery_observer.count

    mail = job_email_delivery_observer.delivered_emails.first
    assert_equal "admin@example.org", mail.to.first
    assert_equal "no-reply@example.org", mail.from.first
    assert_match(/\Asamarbeid \| .*hat Nutzer:in Admin User als Verantwortliche:n von Prozess .*/, mail.subject)
    assert_equal 0, mail.errors.length

    html = Capybara.string mail.html_part.decoded
    html.assert_text "Hallo Admin User"
    html.assert_text "Du hast 1 neue Benachrichtigung in samarbeid"

    html.assert_selector "span", text: "Admin User"
    html.assert_selector "span", text: "%7 (Bestimmung Externer Bedarfe) • EB-7"

    event = workflow.events.last
    html.assert_selector "a[href=\"https://samarbeid-tests.example.com/workflows/7?event=#{event.id}#timeline-item-assigned-#{event.id}\"]", text: "Öffnen"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/my/settings/notifications"]', text: "Einstellungen"
  end

  test " deactivated users shouldn't receive notification emails" do
    current_user = User.find_by!(email: "team-admin@example.org")
    user = User.find_by!(email: "srs_user@example.org")

    Notification.destroy_all
    assert_equal 0, User.for_sending_notifications_every(user.noti_interval).length

    # trigger a notification
    workflow = Workflow.find_by!(aasm_state: :active, assignee: user)
    Interactors::WorkflowInteractor.update_assignee(workflow, nil, current_user)
    assert_equal 1, User.for_sending_notifications_every(user.noti_interval).length

    user.deactivate
    assert_equal 0, User.for_sending_notifications_every(user.noti_interval).length
  end

  test "unconfirmed users shouldn't receive notification emails" do
    current_user = User.find_by!(email: "team-admin@example.org")
    unconfirmed_user = User.create!(email: "test@test.org", firstname: "test", lastname: "test")
    unconfirmed_user.groups << Group.system_group_all unless unconfirmed_user.groups.include?(Group.system_group_all)

    Notification.destroy_all
    assert_equal 0, User.for_sending_notifications_every(unconfirmed_user.noti_interval).length

    # trigger a notification
    workflow = Workflow.find_by!(aasm_state: :active)
    workflow.workflow_definition.groups << Group.system_group_all unless workflow.workflow_definition.groups.include?(Group.system_group_all)

    message = "<p>Hallo <mention m-id=\"#{unconfirmed_user.id}\" m-type=\"user\" :noaccess=\"false\" :deleted=\"false\">@Test Test</mention> </p>"
    Interactors::CommentInteractor.create(message, nil, nil, workflow, current_user)
    refute_includes User.for_sending_notifications_every(unconfirmed_user.noti_interval), unconfirmed_user

    unconfirmed_user.confirm
    assert_includes User.for_sending_notifications_every(unconfirmed_user.noti_interval), unconfirmed_user
  end
end
