require "test_helper"

class ReindexJobTest < ActiveJob::TestCase
  test "reindex job should work" do
    SearchDocument.delete_all
    [Task.first, Workflow.first, Dossier.first].each do |searchable|
      assert_nothing_raised do
        ReindexJob.perform_now(searchable)
      end
      assert searchable.reload.search_document.persisted?, "search document should be persisted for #{searchable.class} #{searchable.id}"
    end
  end
end
