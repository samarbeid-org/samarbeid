require "test_helper"

class ReindexSearchablesJobTest < ActiveJob::TestCase
  test "running job for a dossier with references should work" do
    dossier_with_references = Dossier.all.find { |d| d.referenced_in_workflows.count > 0 }
    assert_nothing_raised { ReindexSearchablesJob.perform_now(dossier_with_references, :referenced_in_workflows) }
  end

  test "running job for a dossier_definition with dossiers should work" do
    dossier_definition_with_dossiers = DossierDefinition.all.find { |dd| dd.dossiers.count > 0 }
    assert_nothing_raised { ReindexSearchablesJob.perform_now(dossier_definition_with_dossiers, :dossiers) }
  end

  test "running job for a workflow_definition with workflows should work" do
    workflow_definition_with_workflows = WorkflowDefinition.all.find { |wd| wd.workflows.count > 0 }
    assert_nothing_raised { ReindexSearchablesJob.perform_now(workflow_definition_with_workflows, :workflows) }
  end
end
