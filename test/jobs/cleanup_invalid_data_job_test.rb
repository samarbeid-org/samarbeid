require "test_helper"

class CleanupInvalidDataJobTest < ActiveJob::TestCase
  test "running job should work" do
    assert_nothing_raised { CleanupInvalidDataJob.perform_now }
    assert_nothing_raised { CleanupInvalidDataJob.perform_now(cleanup_all: true) }
  end

  test "stats should be returned" do
    assert_kind_of Hash, CleanupInvalidDataJob.invalid_data_stats
  end
end
