require "test_helper"
require "resolv-replace"

class SendStatsHeartbeatJobTest < ActiveJob::TestCase
  test "should do nothing if not activated" do
    refute Services::StatsHeartbeat.new.enabled?
    refute SendStatsHeartbeatJob.new.perform
  end

  test "should send stats heartbeat" do
    mock_environment(partial_env_hash: {"SEND_STATS_HEARTBEAT_TO" => "http://samarbeid-stats.example.com/metrics"}) do
      # To see what is sent review below YAML file (located under test/cassettes)
      VCR.use_cassette("stats-heartbeat") do |cassette|
        Timecop.freeze(cassette.originally_recorded_at) { SendStatsHeartbeatJob.new.perform }
      end
    end

    mock_environment(partial_env_hash: {"SEND_STATS_HEARTBEAT_TO" => "http://user:invalid-password@samarbeid-stats.example.com/metrics"}) do
      VCR.use_cassette("stats-heartbeat_invalid-request") do |cassette|
        assert_raises UnexpectedResponseException do
          Timecop.freeze(cassette.originally_recorded_at) { SendStatsHeartbeatJob.new.perform }
        end
      end
    end
  end
end
