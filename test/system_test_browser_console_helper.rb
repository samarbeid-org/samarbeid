def setup_browser_console
  @other_browser_console_logs = []
end

def assert_empty_browser_console_logs
  assert_browser_logs_empty(from: OpenStruct.new({path: self.class.name, lineno: method_name}))
end

# be careful calling this method directly: It clears the console logs, thus may hide errors from final check.
def get_browser_logs
  page.driver.browser.logs.get(:browser)
end

# only for debugging: this will clears console logs and only works with added option "goog:loggingPrefs" in application_system_test_case.rb
def print_browser_logs(level = "ALL")
  get_browser_logs.select { |e| level.nil? || level == "ALL" || e.level == level }.each { |e| puts "#{e.level}: #{e.message}" }
end

def ignore_console_log_error(message:)
  expect_console_log_error(message: message, ignore_missing: true)
end

def expect_console_log_error(message:, level: "SEVERE", count: nil, logs: get_browser_logs, from: caller_locations(1..1).first, ignore_missing: false)
  expected, other = logs.partition { |e| e.message.include?(message) && (level.nil? || e.level == level) }
  @other_browser_console_logs += other

  return if ignore_missing

  additional_info = "Full log: [\n#{logs.map { "#{_1.level}: #{_1.message}" }.join('\n')}\n] [#{from.path}:#{from.lineno}]"
  if count
    assert_equal count, expected.size,
      "Expected #{level}: '#{message}' to be logged #{count} times, but found #{expected.size}.\n#{additional_info}"
  else
    assert expected.size > 0, "Expected #{level}: '#{message}' to be logged.\n#{additional_info}"
  end
end

def assert_browser_logs_empty(logs: get_browser_logs + @other_browser_console_logs, from: caller_locations(1..1).first)
  return if logs.empty?

  if logs.present?
    aggregate_assertions("browser javascript console errors [#{from.path}:#{from.lineno}]") do
      logs.each do |error|
        next if ignore_browser_log?(error, from)

        assert error.level != "SEVERE", error.message
        next unless error.level == "WARNING"

        warn "WARN: javascript warning"
        warn error.message
      end
    end
    @other_browser_console_logs = []
  end
end

def ignore_browser_log?(error, from)
  ignore_list = [
    # add any browser console errors that should be ignored here:
    ## short name of the ignore entry
    ## message to match against
    ## a level to match against (nil matches all levels)
    ## a longer explanation / investigation hint (optional)
    {
      short: "tiptap",
      message: "Uncaught TypeError: Cannot read property 'addEventListener' of null",
      level: nil,
      explanation: %{... n = document.querySelector(".tiptap-vuetify-editor .ProseMirror"); n.addEventListener("mouseup", e), ...}
    },
    {
      short: "read addEventListener of null",
      message: "Uncaught TypeError: Cannot read properties of null (reading 'addEventListener')",
      level: nil,
      explanation: "See explaination in https://gitlab.com/samarbeid-org/samarbeid/-/issues/778#note_710447461"
    },
    {
      short: "read addEventListener of undefined",
      message: "Uncaught TypeError: Cannot read properties of undefined (reading 'addEventListener')",
      level: nil,
      explanation: "sieht fast so aus wie oben bei tiptap..."
    }
  ]

  reason = ignore_list.find { |ignore| error.message.include?(ignore[:message]) && (ignore[:level].nil? || error.level == ignore[:level]) }
  # warn "Ignoring browser log #{error.level}: '#{error.message}' because of ignore-list entry #{reason[:short]} ('#{reason[:message]}' caused by #{reason[:explanation] || "unknown"})" if reason
  Rails.logger.debug "Ignoring browser log '#{reason[:short]}' [#{from.path}:#{from.lineno}]" if reason
  reason
end
