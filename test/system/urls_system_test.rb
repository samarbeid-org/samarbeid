require "application_system_test_case"
require "system_test_helper"
require "minitest/mock"

class UrlsSystemTest < ApplicationSystemTestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
    login_as @user
  end

  class MockedEventsController < Api::EventsController
    def journal
      super
      @events = test_events
    end

    def test_events
      # one event for each event type
      event_table = Event.arel_table
      Event.find_by_sql(
        event_table.project(event_table[Arel.star])
                   .distinct_on(event_table[:type])
                   .order(event_table[:type], event_table[:id].desc)
      )
        .reject { _1.type == "Events::FeatureReleasedEvent" }
        .reject { _1.object_type == "MainMenuEntry" }
        .reject { _1.type == "Events::DeletedEvent" } # TODO: this we should really create notifications at some point...
    end
  end
  PseudoNotification = Struct.new(:event)

  test "event URL generation in Frontend (Vue) and Backend (mails) should work same" do
    # The intention of this test is to render an event for every event type, both in vue frontend and in mails (backend).
    # As there is no frontend action which returns this specific artificial list of events, it's mocked.
    # The mocked controller is above this test due to code style guidelines.
    # The list of events is generated inside the controller due to challenges with lexical scoping and instance variables.
    @events = MockedEventsController.new.test_events

    frontend_urls = []
    Api::EventsController.stub :new, MockedEventsController.method(:new) do
      visit "/journal"
      frontend_urls = page.find_all("a[title='Öffnen']").to_a.map { _1["href"] }
    end

    assert_not_empty frontend_urls

    assert_equal @events.length, frontend_urls.length

    frontend_root = page.find_all("a").first["href"].chomp("/")
    frontend_urls = frontend_urls.map { |url| url.delete_prefix(frontend_root) }

    # TODO: It'd be probably a good idea to have NotificationsMailer use events instead of notifications, but here we are...
    notifications = @events.map { PseudoNotification.new(_1) }

    mail = NotificationsMailer.with(user: @user, notifications: notifications).recent

    assert_equal 0, mail.errors.length
    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    html = Capybara.string mail.html_part.decoded

    mail_urls = html.find_all("a").filter { _1.text == "Öffnen" }.map { _1["href"].delete_prefix("https://samarbeid-tests.example.com") }

    assert_equal @events.length, mail_urls.length

    assert_equal frontend_urls.length, mail_urls.length
    frontend_urls.zip(mail_urls).each do |frontend_url, mail_url|
      assert_equal frontend_url, mail_url
    end
  end
end
