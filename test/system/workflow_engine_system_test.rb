require "application_system_test_case"
require "system_test_helper"

class WorkflowEngineSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "starting tasks prematurely should work" do
    login_as(User.find_by!(email: "dsb@example.org"))
    visit "/tasks"

    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel des Prozesses", with: "Test-Prozess")
      click_card_action_button("PROZESS STARTEN")
    end

    # First Task is show and ready
    assert_css ".task-edit-page"
    within middle_area do
      assert has_text?("Missbrauchshinweis aufnehmen") # we are redirected to next task
      assert_css ".v-chip__content", text: "Aktiv"
    end

    # Want to start second task early
    click_on "Entscheidung Löschung"
    assert_css ".v-chip__content", text: "Erstellt"
    click_on "Aufgabe starten"
    assert_css ".v-chip__content", text: "Aktiv"

    # Want to start task in inaktive block
    find(".v-list-item__title", text: "Löschen: Ja").click
    click_on "Versendung Standard-Dankes-E-Mail"
    assert_css ".v-chip__content", text: "Erstellt"
    within(middle_area) { assert_text "Versendung Standard-Dankes-E-Mail" }
    click_on "Aufgabe starten"
    assert_css ".v-chip__content", text: "Aktiv"
  end

  test "aborting a unfinished workflow should work" do
    login_as(User.find_by!(email: "dsb@example.org"))

    visit "/tasks"

    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel des Prozesses", with: "Test-Prozess")
      click_card_action_button("PROZESS STARTEN")
    end
    assert_css ".task-edit-page"

    within left_sidebar do
      click_on "Test-Prozess"
    end

    open_vertical_dots_context_menu
    click_menu_list_item "Prozess abbrechen"
    click_card_action_button "PROZESS ABBRECHEN"

    assert_browser_logs_empty
    # TODO: There should be something telling us the process is no longer active
  end

  test "set a decision should open the block" do
    login_as(User.find_by!(email: "dsb@example.org"))

    visit "/tasks"

    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel des Prozess", with: "Test-Prozess")
      click_card_action_button("PROZESS STARTEN")
    end

    within left_sidebar do
      find(".v-list-item", text: "Entscheidung Löschung").click
    end
    within middle_area do
      assert_text "Entscheidung Löschung" # wait for page load
    end
    within left_sidebar do
      refute_css ".v-list-group.v-list-group--active", text: "Löschen: Ja", wait: 0.1
    end

    within(middle_area) do
      assert_no_button "Aufgabe abschließen"
      within(find(".v-input", text: "Soll die Anzeige gelöscht werden?")) do
        click_on "Ja"
        has_text? "Ja" # wait until spinner disappears
      end
    end

    within left_sidebar do
      assert_css ".v-list-group.v-list-group--active", text: "Löschen: Ja"
    end
  end
end
