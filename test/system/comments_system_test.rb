require "application_system_test_case"
require "system_test_helper"

class CommentsSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  def setup
    login_as(User.find_by!(email: "srs_user@example.org"))
    @comment_text = "This is a test comment with random stuff so we are sure our comment is found #{Random.hex(16)} !!!"
  end

  test "creating a comment on a task should work" do
    visit "/tasks"
    within(middle_area) do
      fill_in "Titel", with: "srs-workflow"
      click_on "Patentakte in Genese anlegen"
    end

    comment_area = find_prosemirror_editor(page.find(".activity-hub"))
    prosemirror_fill_in(comment_area, @comment_text)
    click_button("Kommentieren")

    assert page.has_css?(".activity-hub-item-comment", text: @comment_text)
  end

  test "creating a comment on a dossier should work" do
    visit "/dossiers"
    within(middle_area) do
      fill_in "Titel / Subtitel", with: "Die ist ein Teststring"
      click_on "Die ist ein Teststring"
    end

    comment_area = find_prosemirror_editor(page.find(".activity-hub"))
    prosemirror_fill_in(comment_area, @comment_text)
    click_button("Kommentieren")

    assert page.has_css?(".activity-hub-item-comment", text: @comment_text)
  end

  test "A not submitted comment should be saved locally and loaded when entering the page with the comment" do
    visit "/tasks"
    within(middle_area) do
      fill_in "Titel", with: "srs-workflow"
      click_on "Patentakte in Genese anlegen"
    end

    within(middle_area) do
      comment_area = find_prosemirror_editor(page.find(".activity-hub"))
      assert comment_area.has_css?("p.tiptap-vuetify-editor__paragraph--is-empty", count: 1)

      prosemirror_fill_in(comment_area, @comment_text)
      assert comment_area.has_css?("p", text: @comment_text)
    end

    within(left_sidebar) { click_on("Beschreibung erstellen") }

    within(middle_area) do
      comment_area = find_prosemirror_editor(page.find(".activity-hub"))
      assert comment_area.has_css?("p.tiptap-vuetify-editor__paragraph--is-empty", count: 1)
    end

    within(left_sidebar) { click_on("Patentakte in Genese anlegen") }

    within(middle_area) do
      comment_area = find_prosemirror_editor(page.find(".activity-hub"))
      assert comment_area.has_css?("p", text: @comment_text)
    end
  end
end
