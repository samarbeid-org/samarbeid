require "application_system_test_case"
require "system_test_helper"

class FooterTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "Footer should only be visible on authentication pages (sign-in, lost-password, change-password) and on static pages" do
    footer_menu_entry_count = MenuEntry.where(location: "footer_menu").count

    visit new_user_session_path
    assert_current_path(new_user_session_path)
    assert_css ".v-bottom-navigation a", count: footer_menu_entry_count

    visit new_user_password_path
    assert_current_path(new_user_password_path)
    assert_css ".v-bottom-navigation a", count: footer_menu_entry_count

    visit edit_user_password_path(reset_password_token: "test")
    assert_current_path(edit_user_password_path(reset_password_token: "test"))
    assert_css ".v-bottom-navigation a", count: footer_menu_entry_count

    click_on "Impressum"
    assert_current_path("/p/impressum")
    assert_css ".v-bottom-navigation a", count: footer_menu_entry_count

    external_link_window = window_opened_by do
      click_on "Externer Link"
    end
    within_window external_link_window do
      assert_current_path("https://example.org/")
    end
    external_link_window.close

    login_as(User.find_by!(email: "srs_user@example.org"))
    visit "/"
    assert_no_css ".v-bottom-navigation"
  end
end
