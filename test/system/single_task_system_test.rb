require "application_system_test_case"
require "system_test_helper"

# At some point we will have a real implementation for single / lonely tasks
# Up to then the workaround is a Workflow with a single Task in it and some features which make working
# with it more feasible
class SingleTaskSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper
  setup do
    @user = User.find_by!(email: "dsb@example.org")
    login_as(@user)
  end

  test "working with a single task and making it a process" do
    visit "/"
    click_link_in_app_menu("Alle Aufgaben")

    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("Einzelne Aufgabe")
      fill_in("Titel des Prozesses", with: "Wäsche aufhängen")
      within(control_for("Verantwortlich")) do
        click_on "Ändern"
        fill_in "Suche", with: "Boss"
        find(".v-list-item", text: "dsb-boss@example.org").click
      end
      click_card_action_button("PROZESS STARTEN")
    end
    assert middle_area.has_text?("ToDo")
    assert middle_area.has_text?("Beschreibung des ToDo")
    within(right_sidebar) { assert control_for("Verantwortlich").has_text?("dsb-boss@example.org") }

    within(middle_area) do
      fill_richtext_control_and_assert_response_message("Beschreibung des ToDo",
        "Wichtig ist vor allem die Frage ob Klammern genutzt werden oder nicht.")
    end
  end
end
