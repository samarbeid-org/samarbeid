require "application_system_test_case"
require "system_test_helper"

class UserSettingsTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper
  setup do
    @user = User.find_by!(email: "srs_user@example.org")
    login_as(@user)
  end

  test "Updating notification settings" do
    visit root_url

    find_button("user-menu").click
    assert has_text?("Nutzerprofil")
    find("a", text: "Nutzerprofil").click
    assert_current_path("/users/#{@user.id}/settings")

    menu_item = find(".v-navigation-drawer__content a .v-list-item__title", text: "Benachrichtigungen")
    menu_item.click
    assert_current_path("/users/#{@user.id}/settings/notifications")

    selection_control = find_control("Neue Benachrichtigungen per E-Mail")

    assert_equal "immediately", @user.noti_interval
    within(selection_control) { assert_selector "div.v-select__selection", text: "Zeitnah (alle 2 Minuten)" }
    retry_flaky_actions do
      within(selection_control) { find("div.v-select__selection", text: "Zeitnah (alle 2 Minuten)").click }
      find(".menuable__content__active .v-list-item__title", text: "Nie").click
    end
    within(selection_control) { assert_selector "div.v-select__selection", text: "Nie" }

    assert control_has_message?(selection_control, :success)
    assert_equal "never", @user.reload.noti_interval
  end
end
