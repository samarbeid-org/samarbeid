require "application_system_test_case"
require "system_test_helper"

class SearchSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper
  setup do
    @user = User.find_by!(email: "srs_user@example.org")
    login_as(@user)
  end

  test "after uploading a file it may be found in the search" do
    visit "/tasks/54" # Task: Beschreibung erstellen

    file_control = find_control("Beschreibung (Dateianhang)")

    perform_enqueued_jobs do # to immediately execute reindex jobs from document
      page.attach_file("test/fixtures/files/cairo-multiline.pdf") do
        file_control.click_on "Hinzufügen"
      end

      assert control_has_message?(file_control, :success, wait: [5, Capybara.default_max_wait_time].max)
      assert file_control.has_css?(".v-input__slot .v-list .v-list-item .v-list-item__content .v-list-item__title", text: "cairo-multiline.pdf")
    end

    # Edit title of uploaded file
    file_list_item = file_control.find(".v-input__slot .v-list .v-list-item", text: "cairo-multiline.pdf")
    file_list_item.find("button .v-icon.mdi-dots-vertical").click
    click_menu_list_item "Bearbeiten"
    fill_in "Titel", with: "Eine Test Datei"
    click_button("Speichern")
    assert file_control.has_css?(".v-input__slot .v-list .v-list-item .v-list-item__content .v-list-item__title", text: "Eine Test Datei")

    use_app_search_with("From James")
    assert page.has_css?("#search-results .v-list .v-list-item__subtitle", text: "Hello World From James")
  end

  test "adding a person dossier in a task shows the workflow as result in search" do
    visit "/tasks/102" # Erstgespräch Erfindung für eine Test-Erfindung

    within(control_for("Erfinder")) { click_button("Ändern") }
    page.find(".v-menu__content .v-list .v-list-item--link", text: "Anastasia Raubuch").click
    perform_enqueued_jobs do # to immediately execute reindex jobs from document
      within(control_for("Erfinder")) { click_button("Ändern") } # trigger saving
    end
    assert control_for("Erfinder").has_text?("Erfolgreich gespeichert")

    use_app_search_with("Anastasia Raubuch")
    search_results = page.all("#search-results .v-list .v-list-item")

    assert_not_empty search_results
    assert search_results.find(text: "Erstgespräch Erfindung für eine Test-Erfindung")
  end
end
