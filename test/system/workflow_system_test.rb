require "application_system_test_case"
require "system_test_helper"

class WorkflowSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "creating a workflow and closing a task should work" do
    login_as(User.find_by!(email: "srs_user@example.org"))
    visit "/tasks"

    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("Veranstaltungsplanung")
      fill_in("Titel des Prozess", with: "Test-Prozess")
      click_card_action_button("PROZESS STARTEN")
    end
    assert_css ".task-edit-page"

    within left_sidebar do
      click_on "Test-Prozess"
    end
    assert_css ".workflow-edit-page"

    # make sure we can only select assignees with access rights for workflows
    within right_sidebar do
      within(control_for("Verantwortlich")) do
        click_on "Ändern"
        fill_in "Suche", with: "no_access_user@example.org"
        assert_css ".v-list-item", text: "Es gibt keine Ergebnisse"
      end
    end

    # complete task
    within left_sidebar do
      click_on "Art der Veranstaltung angeben"
    end

    # make oneself assignee
    within right_sidebar do
      within(control_for("Verantwortlich")) do
        click_on "Ändern"

        # make sure we can only select assignees with access rights for tasks
        fill_in "Suche", with: "no_access_user@example.org"
        assert_css ".v-list-item", text: "Es gibt keine Ergebnisse"

        fill_in "Suche", with: "srs"
        find(".v-list-item", text: "srs_user@example.org").click
      end

      assert control_for("Verantwortlich").has_text?("srs_user@example.org")
    end

    assert_css ".v-chip__content", text: "Aktiv"

    # complete task
    within middle_area do
      fill_text_control_and_assert_response_message "Working title", "Das wird eine tolle Party!"
    end
    click_on "Aufgabe abschließen"
    assert middle_area.has_text?("Veranstalter angeben") # we are redirected to next task
    click_on "Art der Veranstaltung angeben" # go back to original task
    assert_css ".v-chip__content", text: "Abgeschlossen"

    # Controls von abgeschlossene Aufgaben sollen readonly sein
    assert has_control?(with_label: "Working title", with_value: "Das wird eine tolle Party!", is_readonly: true)
  end

  test "let's show a workflow with all possible fields" do
    login_as(User.find_by!(email: "team-admin@example.org"))
    visit "/tasks"

    fill_in "Titel", with: "Prozess mit Beispieldaten"
    click_on "Aufgabe mit allen Infoboxen"

    left_sidebar.click_on "Prozess mit Beispieldaten"
    assert middle_area.has_text? "Prozess mit Beispieldaten"

    left_sidebar.click_on "Aufgabe mit allen Feldern"
    assert_text "Alle Feldtypen in einer Aufgabe"

    left_sidebar.click_on "Aufgabe mit allen Infoboxen"
    assert_text "Alle Feldtypen als Infoboxen anzeigen"

    visit "/tasks"
    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("Prozessvorlage für automatisierte Tests")
      fill_in("Titel des Prozesses", with: "Test-Prozess")
      click_card_action_button("PROZESS STARTEN")
    end
    assert_text "Aufgabe mit allen Feldern"
  end

  test "navigation between tasks of different processes should update left sidebar" do
    login_as(User.find_by!(email: "team-admin@example.org"))
    visit "/tasks"

    fill_in "Titel", with: "Prozess mit Beispieldaten"
    click_on "Aufgabe mit allen Infoboxen"

    within(left_sidebar) do
      assert_css(".v-list-item.sticky-border--workflow .v-list-item__title", text: "Prozess mit Beispieldaten")
      assert_css(".sticky-border--task .v-list-item__title", text: "Aufgabe mit allen Feldern")
      assert_css(".sticky-border--task .v-list-item__title", text: "Aufgabe mit allen Infoboxen")
    end

    mention_task = Task.find(154)
    mention_link = page.find(".activity-hub .v-timeline-item a.object-link", text: mention_task.mention_label)
    mention_link.click

    within(left_sidebar) do
      assert_css(".v-list-item.sticky-border--workflow .v-list-item__title", text: "Diese Kleinigkeit mal erledigen")
      assert_css(".sticky-border--task .v-list-item__title", text: "ToDo")
    end
  end

  test "adding and deleting images in richtext fields should work" do
    login_as(User.find_by!(email: "team-admin@example.org"))
    visit "/tasks"

    fill_in "Titel", with: "Task outside of Block"
    click_on "Task outside of Block"

    richtext_control = within(middle_area) { find_control("Beschreibung") }
    editor = find_prosemirror_editor(richtext_control.find(:xpath, ".."))

    richtext_control.click

    assert_difference -> { ActiveStorage::Blob.count }, 2 do
      page.attach_file("test/fixtures/files/avatar.png") do
        richtext_control.find(".tiptap-vuetify-editor__toolbar button.tiptap-vuetify-editor__action-render-btn .mdi-image").ancestor("button").click
      end

      prosemirror_fill_in(editor, :tab)
      assert control_has_message?(richtext_control, :success, wait: [5, Capybara.default_max_wait_time].max)

      editor_content = richtext_control.find(".tiptap-vuetify-editor__content")
      editor_content.assert_selector('img[src^="/rails/active_storage/representations/redirect/"]')
    end

    assert control_has_message?(richtext_control, :updated, wait: [5, Capybara.default_max_wait_time].max)

    within(middle_area) do
      assert_difference -> { ActiveStorage::Blob.count }, -2 do
        perform_enqueued_jobs do
          prosemirror_fill_in(editor, [:backspace])
          prosemirror_fill_in(editor, "A text without an image should trigger deleting the uploaded image in the backend")
          prosemirror_fill_in(editor, :tab)
          assert control_has_message?(richtext_control, :success, wait: [5, Capybara.default_max_wait_time].max)

          editor_content = richtext_control.find(".tiptap-vuetify-editor__content")
          editor_content.assert_no_selector('img[src^="/rails/active_storage/representations/redirect/"]')
        end
      end
    end
  end
end
