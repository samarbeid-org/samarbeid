require "application_system_test_case"
require "system_test_helper"

class MainMenuSystemTest < ApplicationSystemTestCase
  include ActionMailer::TestHelper

  setup do
    @window_size = page.driver.browser.manage.window.size
    page.driver.browser.manage.window.resize_to(@window_size.width, 600)
  end

  teardown do
    page.driver.browser.manage.window.resize_to(@window_size.width, @window_size.height)
  end

  test "manage users and groups and process definitions" do
    login_as(User.find_by!(email: "admin@example.org"))
    visit "/main-menu-entries"

    add_main_menu_entry_dialog = open_add_main_menu_entry_dialog
    within(add_main_menu_entry_dialog) do
      assert_selector(".infinite-scroll-list .v-list-item", count: 10)
      page.execute_script "const isl = document.querySelector('.infinite-scroll-list'); isl.scrollTo(0, 100000);"
      assert_selector(".infinite-scroll-list .v-list-item", count: 11)
    end
  end
end
