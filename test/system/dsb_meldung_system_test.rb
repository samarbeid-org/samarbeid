require "application_system_test_case"
require "system_test_helper"

# Im Rahmen dieses Prozesses verarbeitet DSB-Nutzer eine Missbrauchsmeldung,
# die sich auf eine dortige Anzeige bezieht. Jedoch trifft er nicht selbst die Entscheidung,
# sondern bittet Nutzer1 als Experten darum, dies zu tun.
class DsbMeldungSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  setup do
    # Run all jobs during this Smoke Test to update visibilies etc.
    ActiveJob::Base.queue_adapter.perform_enqueued_jobs = true
    ActiveJob::Base.queue_adapter.perform_enqueued_at_jobs = true
  end

  teardown do
    ActiveJob::Base.queue_adapter.perform_enqueued_jobs = false
    ActiveJob::Base.queue_adapter.perform_enqueued_at_jobs = false
  end

  test "DSB-Missbrauchsmeldung Smoke-Test" do
    # Schritt 1 (fehlerhafte Anmeldung)
    #
    # Der DSB-Nutzer versucht, sich in das System einzuloggen, gibt hierbei jedoch das falsche Passwort ein,
    # was zu einer Fehlermeldung führt.
    visit root_url
    login_using_form_with("dsb@example.org", "das falsche passwort")
    assert_text "E-Mail oder Passwort ist ungültig."

    # Schritt 2 (erfolgreiche Anmeldung)
    #
    # Nachdem dem DSB-Nutzer sein Fehler dank der Fehlermeldung aufgefallen ist, loggt er sich mit dem korrekten
    # Passwort ein und wird zu seiner Startseite weitergeleitet.
    login_using_form_with("dsb@example.org", "password")
    assert_button("user-menu") # We are logged in

    # Schritt 3 (Starten eines Prozesses zur Verarbeitung der Missbrauchsmeldung)
    #
    # DSB-Nutzer hatte sich in TOMSY eingeloggt, da er per E-Mail eine Missbrauchsmeldung erhalten hatte. Diese will er
    # jetzt in TOMSY weiter verarbeiten. Dazu startet er einen Prozess der Vorlage “DSB Meldung Missbrauch”.  Durch das
    # Starten gelangt er automatisch auf die Seite des neuen Prozesses und wird als Verantwortlicher zugewiesen.
    click_link_in_app_menu("Alle Aufgaben")
    create_workflow_dialog = open_create_workflow_dialog
    within(create_workflow_dialog) do
      click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
      fill_in("Titel des Prozess", with: "Test-Prozess")
      click_card_action_button("PROZESS STARTEN")
    end
    assert_current_path(/tasks\/\d+/)

    within left_sidebar do
      click_on "Test-Prozess"
    end
    assert within(right_sidebar) { control_for("Verantwortlich").has_text?("dsb@example.org") }

    assert_browser_logs_empty

    # Schritt 4 (Löschen aller Aufgaben und Blöcke aus der Prozess-Definition (Instanzen sollen unabhängig von Definitionen existieren))
    click_link_in_settings_menu("Prozessvorlagen")
    click_list_item_link("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")

    task_elements = []
    task_elements << left_sidebar.all("a.block-definition-list-item a.task-definition-list-item")
    task_elements << left_sidebar.all("a.block-definition-list-item a.task-definition-list-item")

    %w[block_task block task].each do |element_type|
      elements = case element_type
      when "block_task"
        left_sidebar.all("a.block-definition-list-item a.task-definition-list-item")
      when "block"
        left_sidebar.all("a.block-definition-list-item")
      when "task"
        left_sidebar.all("a.task-definition-list-item")
      end

      elements.each do |element|
        element.click
        middle_area.find(".page-detail-header").find("button .v-icon.mdi-dots-vertical").click
        if element_type == "block"
          find(".v-list-item__title", text: "Block löschen").click
          within find(".v-dialog--active") do
            assert_text "Block löschen?"
            click_button "Löschen"
          end
        else
          find(".v-list-item__title", text: "Aufgabe löschen").click
          within find(".v-dialog--active") do
            assert_text "Aufgabe löschen?"
            click_button "Löschen"
          end
        end
      end
    end

    # Go back to new process instance
    click_link_in_app_menu("Alle Aufgaben")
    within(middle_area) do
      fill_in "Titel", with: "Test-Prozess"
      click_on "Missbrauchshinweis aufnehmen"
    end

    # Schritt 5 (Ziel zuweisen)
    #
    # Ziele sind nicht länger Teil der Anwendung und werden zu labels migriert.

    # Schritt 6 (Titel und Beschreibung)
    #
    # Der Prozess wird mithilfe von Titel und Beschreibung kurz genauer beschrieben.
    within(left_sidebar) { click_on("Prozess") }

    open_vertical_dots_context_menu
    click_menu_list_item "Titel bearbeiten"

    new_title = "Oper Leipzig"
    fill_in_title new_title
    click_dialog_button "SPEICHERN"

    assert_text new_title

    assert_browser_logs_empty

    # Schritt 7 (Beginn der Eingabe der Daten für die erste Aufgabefehlerhafte Mailadresse eingeben)
    #
    # Um die Missbrauchsmeldung zu erfassen, wird zunächst die Mailadresse des Absenders hinterlegt.
    # Diese wird aus Unachtsamkeit im falschen Format eingegeben.
    within(left_sidebar) { click_on("Missbrauchshinweis aufnehmen") }
    assert middle_area.has_text?("Missbrauchshinweis aufnehmen")
    assert_css ".v-chip__content", text: "Aktiv"

    within(middle_area) do
      fill_text_control_and_assert_response_message("Link zur betroffenen Anzeige", "https://www.dsble.de/784406", :success, has_message_options: {wait: [5, Capybara.default_max_wait_time].max})
    end
    within right_sidebar do
      find(".v-input", text: "Meine Merkliste").click # to get above field to save
      # TODO: check for #946
      # assert control_for("Verantwortlich").has_text?("dsb@example.org")
    end

    within(middle_area) do
      # Controls von aktiven Aufgaben sollen nicht readonly sein
      assert has_control?(with_label: "E-Mail-Adresse Absender", is_readonly: false)

      assert_browser_logs_empty
      fill_text_control_and_assert_response_message("E-Mail-Adresse Absender", "somebody@emailprovider", :error_email)

      current_task = Task.find(current_path.split("/").last.to_i)
      current_task_item = current_task.workflow.content_items.find_by!(label: "E-Mail-Adresse Absender").task_items.find_by!(task_id: current_task.id)
      expect_console_log_error(message: "data_items/#{current_task_item.id} - Failed to load resource: the server responded with a status of 422 (Unprocessable Entity)")
    end

    assert_browser_logs_empty

    # Schritt 8 (Vollständige Eingabe der Daten für die Aufgabe Missbrauchshinweis aufnehmen)
    #
    # Anschließend werden alle Datenfelder in der Aufgabe “Missbrauchshinweis aufnehmen” korrekt eingegeben.
    within(middle_area) do
      fill_text_control_and_assert_response_message "E-Mail-Adresse Absender", "somebody@emailprovider.co.uk"

      fill_text_control_and_assert_response_message "Text der E-Mail", <<~EOF
        Es handelt sich hierbei um eine Veranstaltung bei der ehrenamtlich Tätige eingeladen werden, um deren Einsatz und Engagement zu würdigen. 
        Die Einladungen sind personengebunden. Eine Weitergabe der Einladung und Eintrittskarten an Dritte ist nicht vorgesehen.
        Ich appelliere daher an Sie, das o.g. Inserat zu löschen.
        
        Mit freundlichen Grüßen
        Im Auftrag
        Dr. Prof. Sombody
      EOF

      fill_text_control_and_assert_response_message "Text der betroffenen Anzeige", "Hallo\nich biete 2 Eintrittskarten für die das Event am 04.12.2019 um 18.30 Uhr an."
    end

    assert_browser_logs_empty

    # Schritt 9 (Aufgabe abschließen und Weiterleiten an Nutzer1)
    #
    # Da das Erfassen der Missbrauchsmeldung damit abgeschlossen, beendet der DSB-Nutzer die Aufgabe, wodurch
    # automatisch die Aufgabe “Entscheidung Löschen” bereit wird. Diese Entscheidung soll “Nutzer1” treffen.
    within middle_area do
      assert has_text?("Missbrauchshinweis aufnehmen")
      click_on "Aufgabe abschließen"
    end

    within left_sidebar do
      next_task_in_sidebar = find(".v-list-item", text: "Entscheidung Löschung")
      assert next_task_in_sidebar.has_text?("Aktiv", wait: [5, Capybara.default_max_wait_time].max)
    end
    assert middle_area.has_text?("Entscheidung Löschung") # Automatisch zur nächsten Aufgabe weitergeleitet

    within right_sidebar do
      within(control_for("Verantwortlich")) do
        click_on "Ändern"
        fill_in "Suche", with: "Boss"
        find(".v-list-item", text: "dsb-boss@example.org").click
      end

      assert control_for("Verantwortlich").has_text?("dsb-boss@example.org")
    end

    comment_area = find_prosemirror_editor(page.find(".activity-hub"))
    prosemirror_fill_in(comment_area, "@dsb-bo")
    page.find(".mention-suggestions-menu .v-list-item", text: "dsb-boss@example.org").click
    prosemirror_fill_in(comment_area, "kannst Du bitte noch heute diese Entscheidung vornehmen. Danke.")
    click_button("Kommentieren")

    assert_browser_logs_empty
    logout_using_menu

    # Schritt 10 (Nutzer1 trifft die Entscheidung, dass diese Anzeige gelöscht werden soll)
    #
    # Der Nutzer1 loggt sich wie jeden Tag einmal in das System ein. Dann wechselt er zu “my Tasks” und sieht,
    # dass die Aufgabe “Entscheidung Löschung” ihm zugewiese ist. Er geht zu dieser Aufgabe und trifft die Entscheidung,
    # dass die Anzeige gelöscht werden soll.
    login_using_form_with("dsb-boss@example.org", "password")
    click_on("user-tasks-button")
    click_on("Entscheidung Löschung")

    tasks_and_workflow_visible = left_sidebar.all("a.task-list-item")
    assert_equal 2, tasks_and_workflow_visible.count # We see all task as we are part of the group (we used to allow users to only see single tasks)

    within(middle_area) do
      click_on "Aufgabe abschließen"
      assert has_text? 'Aufgabe kann erst abgeschlossen werden, wenn alle erforderlichen Datenfelder ("Soll die Anzeige gelöscht werden?") ausgefüllt sind.'
      expect_console_log_error(message: "Failed to load resource: the server responded with a status of 422 (Unprocessable Entity)")
      within(find(".v-input", text: "Deine kurze Begründung für die Entscheidung")) do
        fill_in "Deine kurze Begründung für die Entscheidung", with: "Offensichtlich ist es nicht gewünscht, dass diese Karten verkauft werden. Daher löschen wir die Anzeige."
      end
      within(find(".custom-input-control", text: "Soll die Anzeige gelöscht werden?")) do
        click_on "Ja"
        has_text? "Ja" # wait until spinner disappears
      end
      click_on "Aufgabe abschließen"
    end
    # we are redirected to next task which is now ready
    assert middle_area.has_text?("Anzeige löschen")
    assert middle_area.has_text?("Aktiv")

    assert_browser_logs_empty

    logout_using_menu

    # Schritt 15 (Benachrichtigung sehen und zu nächster Aufgabe gehen)
    #
    # Später am Tag loggt sich DSB-Nutzer wieder ein. Er sieht in den Benachrichtigungen, dass die Aufgabe, die er
    # Nutzer1 zugewiesen hatte abgeschlossen wurde. Er wechselt zum Prozess um daran weiterzuarbeiten.
    login_using_form_with("dsb@example.org", "password")
    click_on("user-notifications-button")
    notification_on_finished_task = find(".v-main .v-timeline-item", text: /DSB Boss hat Aufgabe #\d+ • Entscheidung Löschung in Prozess/)
    within(notification_on_finished_task) { click_on "Entscheidung Löschung" }

    within(left_sidebar) { click_on("Anzeige löschen") }
    assert middle_area.has_text?("Anzeige löschen")

    assert_browser_logs_empty

    # Schritt 16 (Anzeige Löschen und Dankes-Email-Schreiben)
    #
    # Er schließt den Prozess ab.

    within middle_area do
      assert has_text?("Anzeige löschen")
      assert has_infobox?(with_label: "Link zur betroffenen Anzeige", with_value: "https://www.dsble.de/784406")
      click_on "Aufgabe abschließen"
    end

    assert middle_area.has_text?("Versendung Standard-Dankes-E-Mail")

    within right_sidebar do
      within(control_for("Verantwortlich")) do
        click_on "Ändern"
        find(".v-list-item", text: "dsb@example.org").click
      end
      assert control_for("Verantwortlich").has_text?("dsb@example.org")
    end

    within middle_area do
      assert has_text?("Versendung Standard-Dankes-E-Mail")
      assert has_infobox?(with_label: "E-Mail-Adresse Absender", with_value: "somebody@emailprovider.co.uk")
      assert has_infobox?(with_label: "Versendung Standard-Dankes-E-Mail", with_value: "Lieber Nutzer, danke für den Hinweis. Wir haben die Anzeige gelöscht. Viele Grüße")
      click_on "Aufgabe abschließen"
    end

    assert middle_area.has_text?("NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
  end
end
