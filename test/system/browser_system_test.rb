require "application_system_test_case"
require "system_test_helper"

class BrowserSystemTest < ApplicationSystemTestCase
  test "timezone is set to Europe/Berlin" do
    assert_equal "Europe/Berlin", page.evaluate_script("Intl.DateTimeFormat().resolvedOptions().timeZone;")
  end
end
