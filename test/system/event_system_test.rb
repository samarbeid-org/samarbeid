require "application_system_test_case"
require "system_test_helper"

class EventSystemTest < ApplicationSystemTestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
    login_as @user
  end

  test "ActivityHub should show events/history for all possible object types" do
    workflow = Workflow.find_by!(title: "event-test-workflow")
    visit "/workflows/#{workflow.id}"
    left_sidebar.click_on "event-test-workflow"

    page.find(".activity-hub .v-tabs .v-tab", text: "ALLE").click
    assert page.has_css?(".activity-hub-item-comment", count: Event.for_object(workflow).count { |e| e.is_a?(Events::CommentedEvent) })
    assert page.has_css?(".activity-hub-item-event", count: Event.for_object(workflow).count { |e| !e.is_a?(Events::CommentedEvent) })

    task = Workflow.find_by!(title: "event-test-workflow").tasks_ordered_in_structure.first
    visit "/tasks/#{task.id}"

    page.find(".activity-hub .v-tabs .v-tab", text: "ALLE").click
    assert page.has_css?(".activity-hub-item-comment", count: Event.for_object(task).count { |e| e.is_a?(Events::CommentedEvent) })
    assert page.has_css?(".activity-hub-item-event", count: Event.for_object(task).count { |e| !e.is_a?(Events::CommentedEvent) })
  end

  test "Notifications rendering (comments, feature-release news)" do
    login_as(User.find_by!(email: "srs_user@example.org"))
    visit "/tasks"

    fill_in "Titel", with: "Bestimmung Externer Bedarfe"
    click_on "Folgeprojekte wählen aus P-Matching, Teammatching und anderen Bereichen"
    assert middle_area.has_text?("Folgeprojekte wählen aus P-Matching, Teammatching und anderen Bereichen")

    # Comment and mention a user.
    mention_user = User.find_by!(email: "admin@example.org")
    new_comment = "Ein kleiner Kommentar. Benachrichtigen möchte ich "

    activity_hub_element = page.find(".activity-hub")
    editor = find_prosemirror_editor(activity_hub_element)

    # Can only mention users with access to workflow/task
    prosemirror_fill_in(editor, new_comment + "@no_access_user")
    assert_css(".mention-suggestions-menu .v-list-item", text: "Es gibt keine Ergebnisse")

    prosemirror_fill_in(editor, new_comment + "@admin")
    page.find(".mention-suggestions-menu .v-list-item", text: mention_user.email).click
    activity_hub_element.click_button("Kommentieren")
    activity_hub_element.find(".v-timeline-item").assert_text new_comment + mention_user.mention_label

    # Mentioned user should have received his notification
    logout_using_menu
    login_as(mention_user)
    visit root_url
    page.find("#user-notifications-button").click
    item = page.find(".v-timeline-item.activity-hub-item-comment", text: new_comment + mention_user.mention_label)
    item.assert_text "SRS User hat Dich in einem Kommentar zu Aufgabe #26 • Folgeprojekte"

    # News
    page.find("#user-notifications-button").click
    page.find(".v-tab", text: "MERKLISTE").click
    page.assert_text "Immer Up to Date mit Benachrichtigungen zu Feature-Releases (old)"
    page.assert_text "Die einzige Konstante ist die Veränderung - Was der griechische Philosoph Heraklit schon vor 2500 Jahren erkannte, ist bei samarbeid gelebte Kultur. Durch das Feedback unserer Nutzer:innen verändert sich samarbeid stetig. Ab jetzt informieren wir Dich in samarbeid immer über unsere neuesten Features."
  end
end
