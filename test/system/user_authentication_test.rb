require "application_system_test_case"
require "system_test_helper"
require "action_mailer/test_helper"

class UserAuthenticationTest < ApplicationSystemTestCase
  include ActionMailer::TestHelper

  setup do
    logout
  end

  # Simulating timout of session is a bit harder than expected. We tried several other options before settling on
  # warden highjacking (is fastest and reliable)
  # Tried: Devise/Warden sign_out helpers, deleting cookies, mokey-patching timedout? method on user object,
  # changing devise config timout for this test and sleeping @user.timout_in
  test "a user who's session expires should be shown sign in form" do
    visit root_path
    assert_equal new_user_session_path, current_path

    login_using_form_with("srs_user@example.org")

    assert_button("user-menu") # We are logged in

    retry_flaky_actions do
      simulate_session_timeout
      click_link_in_app_menu("Alle Aufgaben", false) # loading a new page should trigger app to realize our session has timed out
      assert has_text?("Anmelden") # we are shown login form
    end
    assert_equal new_user_session_path, current_path

    ignore_console_log_error(message: "Failed to load resource: the server responded with a status of 401 (Unauthorized)")
  end

  test "only user within admin group can visit admin interfaces" do
    visit root_url
    ["srs_user@example.org", "team-admin@example.org"].each do |not_super_admin_email|
      login_using_form_with(not_super_admin_email)

      visit rails_admin.dashboard_path
      assert has_text? "Sie dürfen nicht auf diese Seite zugreifen."
      expect_console_log_error(message: "Failed to load resource: the server responded with a status of 403 (Forbidden)")

      visit "/good_job"
      assert has_title? "404 Fehler"

      visit root_path
      logout_using_menu
    end

    login_using_form_with("admin@example.org")

    rails_admin_tab = window_opened_by { click_link_in_admin_menu("Rails Admin") }
    within_window rails_admin_tab do
      assert has_text?("Administration")
      assert_equal rails_admin.dashboard_path, current_path

      # Visit some pages to see if customized rails_admin is working
      visit "/admin/workflow_definition"
      assert has_text?("Liste von")
      visit "/admin/workflow_definition/5"
      assert has_text?("Details für")
      visit "/admin/workflow_definition/5/edit"
      assert has_text?("bearbeiten")
    end

    visit root_path

    rails_admin_tab = window_opened_by { click_link_in_admin_menu("Good Job") }
    within_window rails_admin_tab do
      assert has_text?("GoodJob")
      assert_equal "/good_job/jobs", current_path
    end
  end

  test "user should be able to recover his password" do
    user = User.find_by!(email: "srs_user@example.org")
    visit root_path
    assert_current_path new_user_session_path

    click_on "Passwort vergessen?"
    assert_current_path new_user_password_path

    assert_emails 1 do
      within("div.lost-password-page form") do
        fill_in("user[email]", with: user.email)
        click_button("Passwort zurücksetzen")
      end
    end

    assert_current_path new_user_session_path
    assert_text "Sie erhalten in Kürze eine E-Mail mit Anweisungen zum Zurücksetzen Ihres Passworts."

    last_email = ActionMailer::Base.deliveries.last
    assert_equal [user.email], last_email.to
    assert_equal "samarbeid | Anweisungen zum Zurücksetzen Ihres Passworts", last_email.subject

    mail_html = Capybara.string last_email.html_part.body.to_s
    change_password_url = mail_html.find_link(text: "https://samarbeid-tests.example.com/users/change-password?reset_password_token=")["href"]

    visit URI(change_password_url).request_uri
    assert_text "Mein Passwort festlegen"

    within("div.change-password-page form") do
      fill_in("user[password]", with: "testtest")
      fill_in("user[password_confirmation]", with: "testtest")
      click_button("Passwort speichern")
    end

    assert_text "Ihr Passwort wurde geändert. Sie sind jetzt angemeldet."

    logout_using_menu
    login_using_form_with(user.email, "testtest")
    assert_button("user-menu") # We are logged in
  end

  test "recover password of unknown email should fail" do
    no_user_email = "no_user@example.org"
    refute User.exists?(email: no_user_email)

    visit root_path
    assert_current_path new_user_session_path

    click_on "Passwort vergessen?"
    assert_current_path new_user_password_path

    assert_no_emails do
      within("div.lost-password-page form") do
        fill_in("user[email]", with: no_user_email)
        click_button("Passwort zurücksetzen")
      end
    end

    assert_current_path new_user_password_path
    assert_text "E-Mail nicht gefunden"
  end

  test "attempt to change password without reset_password_token should fail" do
    visit edit_user_password_path

    assert_current_path new_user_session_path
    assert_text "Sie können diese Seite nicht nutzen, es sei denn, Sie stammen von einer Passwort-Zurücksetzen-E-Mail. Wenn dies der Fall ist, stellen Sie sicher, dass Sie die vollständige URL verwendet haben."
  end

  test "recovering password of deactivated user should fail" do
    user = User.find_by!(email: "srs_user@example.org")
    user.deactivate
    assert user.deactivated?

    visit root_path
    assert_current_path new_user_session_path

    click_on "Passwort vergessen?"
    assert_current_path new_user_password_path

    assert_no_emails do
      within("div.lost-password-page form") do
        fill_in("user[email]", with: user.email)
        click_button("Passwort zurücksetzen")
      end
    end

    assert_current_path new_user_password_path
    assert_text "Ihr Konto wurde deaktiviert"
  end

  test "attempt to change password with wrong reset_password_token should fail" do
    visit edit_user_password_path(reset_password_token: "wrong-token")
    assert_text "Mein Passwort festlegen"

    within("div.change-password-page form") do
      fill_in("user[password]", with: "testtest")
      fill_in("user[password_confirmation]", with: "testtest")
      click_button("Passwort speichern")
    end

    assert_current_path edit_user_password_path
    assert_text "Token zum Zurücksetzen des Passworts ist nicht gültig"
  end

  private

  def simulate_session_timeout
    Warden.on_next_request do |proxy|
      session = proxy.env["rack.session"]["warden.user.user.session"]
      session["last_request_at"] = (Time.at(session["last_request_at"]).utc - (User.timeout_in + 10)).to_i
    end
  end
end
