require "application_system_test_case"
require "system_test_helper"

class VisibilityAfterDeletingSystemTest < ApplicationSystemTestCase
  include ActiveJob::TestHelper

  test "deleting a workflow should remove it and it's tasks from all lists etc." do
    login_as(User.find_by!(email: "admin@example.org"))
    workflow = Workflow.find_by!(title: "Prozess der überall vorkommt")

    # Prozess der gelöscht werden soll (mit Beschreibung vieler Referenzen die wir im einzelnen unten nochmal prüfen)
    visit "workflows/#{workflow.id}"
    assert_text workflow.title

    # Referenzen des Dossiers in Aufgaben
    visit "dossiers/1/task-references"
    assert_text workflow.title

    # Notification
    visit "notifications"
    assert_text "@Admin User was soll Ich hier tun?"
    assert_text workflow.title

    # Liste der Aufgaben
    visit "tasks"
    assert_text workflow.title

    #########################
    # Prozess jetzt löschen
    #########################
    visit "workflows/#{workflow.id}"
    left_sidebar.click_on workflow.title
    open_vertical_dots_context_menu
    perform_enqueued_jobs do
      click_menu_list_item "Prozess löschen"
      click_dialog_button "PROZESS LÖSCHEN"
    end

    # Prozess der gelöscht wurde
    assert_browser_logs_empty
    visit "workflows/#{workflow.id}"
    assert_text "Daten existieren nicht"
    refute_text workflow.title, wait: 0.1
    expect_console_log_error(message: "Failed to load resource: the server responded with a status of 404 (Not Found)")

    # Referenzen des Dossiers in Aufgaben
    visit "dossiers/1/task-references"
    assert_text "Admin User" # wait for page load
    refute_text workflow.title, wait: 0.1

    # Notification
    visit "notifications"
    # Die Benachrichtigung wurde gelöscht
    assert_text "Benachrichtigungen" # wait for page load
    refute_text workflow.title, wait: 0.1

    # Liste der Aufgaben
    visit "tasks"
    assert_text "Mein Geburtstag" # wait for page load
    refute_text workflow.title, wait: 0.1
    click_tab "ALLE"
    assert_text "event-test-automation-workflow" # wait for page load
    refute_text workflow.title, wait: 0.1
  end
end
