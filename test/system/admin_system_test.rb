require "application_system_test_case"
require "system_test_helper"

class AdminSystemTest < ApplicationSystemTestCase
  include ActionMailer::TestHelper

  test "admin user should be able to create group and user which needs to confirm his account via email and set password" do
    visit root_path
    login_using_form_with("team-admin@example.org")

    click_link_in_settings_menu("Gruppen")
    middle_area.click_on("Gruppe erstellen")

    create_group_dialog = page.find(".create-group-dialog", match: :first)
    new_group_name = "Gruppe-für-Performance-Test-1"
    within(create_group_dialog) do
      fill_in("Name", with: new_group_name)
      click_card_action_button("SPEICHERN")
    end

    click_link_in_settings_menu("Nutzer:innen")
    click_on "Nutzer:in erstellen"

    create_user_dialog = page.find(".create-user-dialog", match: :first)
    new_user_email = "test-nutzer@example.org"

    assert_emails 1 do
      within(create_user_dialog) do
        fill_in("E-Mail", with: new_user_email)
        fill_in("Vorname", with: "Test")
        fill_in("Nachname", with: "User")

        click_button("Ändern", match: :first)
        fill_in("Suche", with: new_group_name)
        find(".v-list-item", text: new_group_name).click
        click_button("Ändern", match: :first)

        click_card_action_button("SPEICHERN")
      end

      assert_text "Unbestätigt", wait: [5, Capybara.default_max_wait_time].max # is shown on user page
    end

    # Chick validation if group was assigned to user
    user = User.find_by!(email: new_user_email)
    assert_includes user.groups, Group.find_by!(name: new_group_name)

    assert_account_confirmation_email(new_user_email)

    logout_using_menu

    # Confirm account and set password
    password = "password"
    html = Capybara.string ActionMailer::Base.deliveries.last.html_part.decoded
    link_node = html.find_link text: "https://samarbeid-tests.example.com/users/confirmation?confirmation_token="

    assert_difference -> { Events::ConfirmedEvent.count } do
      visit URI(link_node["href"]).request_uri
    end

    assert_text "Ihre E-Mail-Adresse wurde erfolgreich bestätigt. Bitte legen Sie Ihr Passwort fest."
    assert_text "Mein Passwort festlegen"

    change_password_form = page.find("form[action$=\"#{users_change_password_path}\"]", match: :first)
    within(change_password_form) do
      fill_in("Neues Passwort", with: password)
      fill_in("Neues Passwort wiederholen", with: password)
      click_button("Passwort speichern")
    end
    assert_text "Ihr Passwort wurde geändert. Sie sind jetzt angemeldet."
  end
end
