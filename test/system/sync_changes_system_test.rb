require "application_system_test_case"
require "system_test_helper"

class SynChangesSystemTest < ApplicationSystemTestCase
  setup do
    @todo_task = Task.find(154)

    # Admin is default Session
    login_as(User.find_by_email("admin@example.org"))
    visit "/tasks/#{@todo_task.id}"
    assert has_text?("Diese Kleinigkeit mal erledigen")

    # Second session for DSB User (utilizing Capybara session management)
    using_session("dsb_user") do
      login_as(User.find_by_email("dsb@example.org"))
      visit "/tasks/#{@todo_task.id}"
      assert has_text?("Diese Kleinigkeit mal erledigen")
    end
  end

  test "Update assignee in ui should get synced to all clients have been subscribed" do
    within right_sidebar do
      within(control_for("Verantwortlich")) { assert has_item?("dsb-boss@example.org") }
    end

    using_session("dsb_user") do
      within right_sidebar do
        within(control_for("Verantwortlich")) do
          assert has_item?("dsb-boss@example.org")
          select_objects_control_item("admin@example.org", search_text: "admin user")
          assert has_item?("admin@example.org")
        end
      end
    end

    within right_sidebar do
      within(control_for("Verantwortlich")) { assert has_item?("admin@example.org") }
    end
  end

  test "Update contributors in ui should get synced to all clients have been subscribed" do
    within right_sidebar do
      within(control_for("Teilnehmend")) { refute has_item?("TA", ".v-btn"), wait: 0.1 }
    end

    using_session("dsb_user") do
      within right_sidebar do
        within(control_for("Teilnehmend")) do
          refute has_item?("TA", ".v-btn"), wait: 0.1
          select_objects_control_item("team-admin@example.org", search_text: "team admin", close: true)
          assert has_item?("TA", ".v-btn")
        end
      end
    end

    within right_sidebar do
      within(control_for("Teilnehmend")) { assert has_item?("TA", ".v-btn") }
    end
  end

  test "Update due_date in ui should get synced to all clients have been subscribed" do
    within right_sidebar do
      within(control_for("Fälligkeitsdatum")) { assert has_item?("", ".custom-input-control__content") }
    end

    using_session("dsb_user") do
      within right_sidebar do
        within(control_for("Fälligkeitsdatum")) do
          freeze_backend_and_frontend_date(2023, 12, 21) do
            assert has_item?("", ".custom-input-control__content")
            select_date_control_day("22")
            assert has_item?("22.12.2023", ".custom-input-control__content")
          end
        end
      end
    end

    within right_sidebar do
      within(control_for("Fälligkeitsdatum")) { assert has_item?("22.12.2023", ".custom-input-control__content") }
    end
  end

  test "Update snooze_date in ui should get synced to all clients have been subscribed" do
    within right_sidebar do
      within(control_for("Zurückstellen bis")) { assert has_item?("", ".custom-input-control__content") }
    end

    using_session("dsb_user") do
      within right_sidebar do
        within(control_for("Zurückstellen bis")) do
          freeze_backend_and_frontend_date(2023, 12, 21) do
            assert has_item?("", ".custom-input-control__content")
            select_date_control_day("23")
            assert has_item?("23.12.2023", ".custom-input-control__content")
          end
        end
      end
    end

    within right_sidebar do
      within(control_for("Zurückstellen bis")) { assert has_item?("23.12.2023", ".custom-input-control__content") }
    end
  end

  test "Update time budget in ui should get synced to all clients have been subscribed" do
    within right_sidebar do
      within(control_for("Zeiterfassung")) { refute has_item?("Budget:", "div"), wait: 0.1 }
    end

    using_session("dsb_user") do
      within right_sidebar do
        within(control_for("Zeiterfassung")) do
          refute has_item?("Budget:", "div"), wait: 0.1
          set_time_budget("2", "30")
          assert has_item?("Budget: 2h 30min", "div")
        end
      end
    end

    within right_sidebar do
      within(control_for("Zeiterfassung")) { assert has_item?("Budget: 2h 30min", "div") }
    end
  end
end
