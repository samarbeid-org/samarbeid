require "test_helper"

class ProcessApiTest < ActionDispatch::IntegrationTest
  setup do
    @api_user = User.find_by_email!("dsb@example.org")
    @api_user.create_token(:api_token)
    @default_params = {process_definition_id: WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch").id,
                       api_token: @api_user.api_token}
  end

  test "creating a process with a custom name is possible" do
    new_process_name = "my first API process"
    assert_difference -> { Workflow.count } do
      post "/api/v0/process", params: {
        name: new_process_name
      }.merge(@default_params)
    end
    assert_response :success

    response_process = response.parsed_body

    assert_equal Workflow.order(:created_at).last.id, response_process["id"]
    assert_equal new_process_name, response_process["name"]
    assert response_process.key?("fields")
  end

  test "creating a process via API creates a new event to inform user who provided API-token" do
    assert_difference -> { Events::SimpleActionEvent.where(subject: @api_user).count } do
      post "/api/v0/process", params: @default_params
    end
    assert_response :success
  end

  test "creating a process via API automatically assigns the API user as process assignee" do
    post "/api/v0/process", params: @default_params
    assert_response :success

    assert @api_user, Workflow.find(response.parsed_body["id"]).assignee
  end

  test "a user can only create processes for which he has access to" do
    assert_no_difference -> { Events::SimpleActionEvent.where(subject: @api_user).count } do
      post "/api/v0/process", params: @default_params.merge({process_definition_id: 1})
    end
    assert_response :bad_request
  end

  test "creating a process with initial values for fields is possible" do
    post "/api/v0/process", params: {
      fields:
        [{
          definition_id: 51,
          value: "some-email@example.org"
        },
          {
            definition_id: 52,
            value: "what a message"
          }]
    }.merge(@default_params)

    assert_response :success

    response_process = response.parsed_body

    assert_equal "what a message", response_process["fields"].find { |field_p| field_p["label"].eql?("Text der E-Mail") }["value"]
    assert_equal "some-email@example.org", response_process["fields"].find { |field_p| field_p["label"].eql?("E-Mail-Adresse Absender") }["value"]
  end

  test "searching for proccesses of a kind is possible" do
    get "/api/v0/process", params: @default_params
    assert_response :success

    expected_processes = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch").workflows
    assert_equal(expected_processes.map(&:title).sort, response.parsed_body.map { |e| e["name"] }.sort)
  end

  test "searching for proccesses with certain name is possible" do
    get "/api/v0/process", params: {name: "event-test-workflow"}.merge(@default_params)
    assert_response :success

    assert_equal("event-test-workflow", response.parsed_body.first["name"])
  end

  test "searching for proccesses of a kind with certain field values is possible" do
    get "/api/v0/process", params: {
      fields:
        [{
          definition_id: 51,
          value: "test@tester.test"
        }]
    }.merge(@default_params)

    assert_response :success

    assert_equal("event-test-workflow", response.parsed_body.first["name"])
  end

  test "searching for proccesses with invalid field parameters is not possible" do
    get "/api/v0/process", params: {
      fields:
        [{
          definition_id: 10_000, # invalid
          value: "test@tester.test"
        }]
    }.merge(@default_params)

    assert_response 400
    assert_equal("400 Bad Request - invalid field definition_id given: 10000", response.parsed_body["error"])
  end

  test "searching for proccesses with field values not present returns empty result" do
    get "/api/v0/process", params: {
      fields:
        [{
          definition_id: 51,
          value: "some-email@example.org"
        }]
    }.merge(@default_params)

    assert_response :success
    assert_equal([], response.parsed_body)
  end
end
