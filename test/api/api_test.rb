require "test_helper"

class ApiTest < ActionDispatch::IntegrationTest
  setup do
    @api_user = User.find_by_email!("dsb@example.org")
    @api_user.create_token(:api_token)
  end

  test "Access API Documentation (swagger)" do
    login_as(@api_user)
    get "/samarbeid-api-documentation"
    assert_response :success
  end

  test "Deny API Documentation (swagger) as unauthorized user" do
    get "/samarbeid-api-documentation"
    assert_redirected_to new_user_session_path
  end

  test "API can not be accessed as non api_user or without valid token" do
    get "/api/v0/process_definitions", params: {api_token: @api_user.api_token}
    assert_response :success

    get "/api/v0/process_definitions", params: {api_token: ""}
    assert_response :bad_request

    get "/api/v0/process_definitions", params: {api_token: "something"}
    assert_response :unauthorized

    @api_user.delete_token(:api_token)
    get "/api/v0/process_definitions", params: {api_token: @api_user.api_token}
    assert_response :bad_request
  end
end
