require "test_helper"

class DossierApiTest < ActionDispatch::IntegrationTest
  setup do
    @api_user = User.find_by_email!("dsb@example.org")
    @api_user.create_token(:api_token)

    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    # @dossier = @dossier_definition.dossiers.find(59)

    @default_params = {api_token: @api_user.api_token,
                       dossier_definition_id: @dossier_definition.id}
  end

  test "list of dossier definitions can be retrieved and only those visible to user are shown" do
    DossierDefinition.find(1).update!(groups: []) # hide one dossier definition from user to check basic acl

    get "/api/v0/dossier_definitions", params: {api_token: @api_user.api_token}
    assert_response :success

    definitions_visible = [
      "DOSSIER FÜR AUTOMATISIERTE TESTS",
      "Kleinanzeige DSB",
      # "Person",
      "Politisches Themenfeld"
    ]

    assert_equal(definitions_visible.sort, response.parsed_body.map { |e| e["name"] }.sort)
  end

  test "creating a dossier via API creates a new event to inform user who provided API-token" do
    assert_difference -> { Events::SimpleActionEvent.where(subject: @api_user).count } do
      @default_params[:fields] = [{definition_id: 10, value: "the required string"}]
      post "/api/v0/dossier", params: @default_params
    end
    assert_response :success
  end

  test "creating a dossier with initial values for fields is possible" do
    @default_params[:fields] = []
    @default_params[:fields] << {definition_id: 10, value: "the required string"}
    @default_params[:fields] << {definition_id: 12, value: "some-email@example.org"}
    @default_params[:fields] << {definition_id: 13, value: "what a message"}

    post "/api/v0/dossier", params: @default_params

    assert_response :success

    response_dossier = response.parsed_body

    assert_equal "what a message", response_dossier["fields"].find { |field_p| field_p["label"].eql?("Text") }["value"]
    assert_equal "some-email@example.org", response_dossier["fields"].find { |field_p| field_p["label"].eql?("Email") }["value"]
  end

  test "searching for dossiers by name is possible" do
    get "/api/v0/dossier", params: {name: "Die ist ein Teststring"}.merge(@default_params)
    assert_response :success

    assert_equal("Die ist ein Teststring", response.parsed_body.first["name"])
  end

  test "searching for dossiers by field values is possible" do
    get "/api/v0/dossier", params: {
      fields:
        [{
          definition_id: 12,
          value: "admin@example.org"
        }]
    }.merge(@default_params)

    assert_response :success

    assert_equal("Die ist ein Teststring", response.parsed_body.first["name"])
  end

  test "searching for dossiers with invalid field parameters is not possible" do
    get "/api/v0/dossier", params: {
      fields:
        [{
          definition_id: 10_000, # invalid
          value: "admin@example.org"
        }]
    }.merge(@default_params)

    assert_response 400
    assert_equal("400 Bad Request - invalid field definition_id given: 10000", response.parsed_body["error"])
  end

  test "searching for dossiers with field values not present returns empty result" do
    get "/api/v0/dossier", params: {
      fields:
        [{
          definition_id: 12,
          value: "something-guranteed-not-present@example.org"
        }]
    }.merge(@default_params)

    assert_response :success
    assert_equal([], response.parsed_body)
  end

  test "updating a dossier field is possible" do
    dossier = Dossier.find(59) # Dossier for automated tests

    assert_difference -> { Events::ChangedDataEvent.where(subject: @api_user).count } do
      put "/api/v0/dossier", params: {
        id: dossier.id,
        field_definition_id: 10,
        value: "Updated String value"
      }.merge(@default_params)

      assert_response :success
    end

    assert_equal("Updated String value", response.parsed_body["fields"].find { |f| f["id"] == 349 }.dig("value"))

    selection_field_id = 21

    assert_difference -> { Events::ChangedDataEvent.where(subject: @api_user).count } do
      put "/api/v0/dossier", params: {
        id: dossier.id,
        field_definition_id: selection_field_id,
        value: [2]
      }.merge(@default_params)

      assert_response :success
    end

    assert_equal([2], response.parsed_body["fields"].find { |f| f["id"] == 360 }.dig("value"))
  end
end
