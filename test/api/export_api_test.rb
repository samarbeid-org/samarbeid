require "test_helper"

class ExportApiTest < ActionDispatch::IntegrationTest
  setup do
    @api_user = User.find_by_email!("dsb@example.org")
    @api_user.create_token(:api_token)

    @default_params = {api_token: @api_user.api_token}
  end

  test "exporting dossiers" do
    get "/api/v0/export/dossiers", params: @default_params.merge({dossier_definition_id: DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS").id})
    assert_response :success

    assert_equal "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", response.content_type
    assert_equal "attachment; filename=dossiers.xlsx", response.headers["Content-Disposition"]
  end

  test "exporting dossiers only for what user has access to" do
    definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    definition.update!(groups: [])
    get "/api/v0/export/dossiers", params: @default_params.merge({dossier_definition_id: definition.id})
    assert_response :bad_request
  end

  test "exporting processes" do
    get "/api/v0/export/processes", params: @default_params.merge({process_definition_id: WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch").id})
    assert_response :success

    assert_equal "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", response.content_type
    assert_equal "attachment; filename=processes.xlsx", response.headers["Content-Disposition"]
  end

  test "exporting processes only for what user has access to" do
    get "/api/v0/export/processes", params: @default_params.merge({process_definition_id: 1})
    assert_response :bad_request
  end
end
