require "test_helper"

class ProcessDefinitionApiTest < ActionDispatch::IntegrationTest
  setup do
    @api_user = User.find_by_email!("dsb@example.org")
    @api_user.create_token(:api_token)
  end

  test "list of process definitions can be retrieved and only those visible to user are shown" do
    get "/api/v0/process_definitions", params: {api_token: @api_user.api_token}
    assert_response :success

    process_definitions_visible_to_user = [
      "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch",
      "Einzelaufgaben",
      "Einzelne Aufgabe",
      "DSB Meldung Missbrauch",
      "DSB Premiumanzeige"
    ]

    assert_equal(process_definitions_visible_to_user.sort, response.parsed_body.map { |e| e["name"] }.sort)
  end
end
