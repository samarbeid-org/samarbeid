require "test_helper"

class Api::UsersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "non admin should get index of users" do
    login_as @user
    get api_users_path(format: :json)
    assert_response :success
  end

  test "admin should get index of users" do
    login_as @admin
    get api_users_path(format: :json)
    assert_response :success
  end

  test "#index should have a working filter for activation_state" do
    login_as @admin

    count_exp = User.activated.count
    get api_users_path(format: :json)
    assert_response :success
    assert_equal count_exp, @response.parsed_body["result"].count

    assert_difference -> { User.deactivated.count } do
      perform_enqueued_jobs do
        @user.deactivate
      end
    end
    count_exp = User.deactivated.count
    get api_users_path(format: :json, params: {activation_state: "deactivated"})
    assert_response :success
    assert_equal count_exp, @response.parsed_body["result"].count

    count_exp = User.count
    get api_users_path(format: :json, params: {activation_state: "all"})
    assert_response :success
    assert_equal count_exp, @response.parsed_body["result"].count
  end

  test "should get filtered list of users" do
    login_as @user
    get list_api_users_path(except: [@user.id], group_ids: [Group.first], query: "admin", format: :json)
    assert_response :success
  end

  test "list of users shouldn't include deactivated users" do
    @user.deactivate
    login_as @admin
    get list_api_users_path(format: :json)
    assert_response :success

    assert_equal User.activated.count, @response.parsed_body.length
    refute @response.parsed_body.any? { |user| user["id"] == @user.id }
  end

  test "list of users should return current_user first" do
    login_as @user
    get list_api_users_path(except: [], group_ids: Group.all.map(&:id), query: "", format: :json)
    assert_response :success
    assert_equal @user.id, @response.parsed_body.first["id"]
  end

  test "#list should support paging of results" do
    login_as @admin

    (1..10).each { |i| User.create!(email: "test-user-#{i}@example.com", lastname: "user-#{i}", firstname: "test", password: "password", confirmed_at: Time.current) }

    result_count = User.count
    assert_operator result_count, :>, 10
    assert_operator result_count, :<, 20

    get list_api_users_path(format: :json, params: {page: 1, per_page: 10})
    assert_response :success
    assert_equal 10, @response.parsed_body.length

    get list_api_users_path(format: :json, params: {page: 2, per_page: 10})
    assert_response :success
    assert_equal result_count - 10, @response.parsed_body.length
  end

  test "should get other user" do
    login_as @user
    get api_user_path(@admin, format: :json)
    assert_response :success
  end

  test "unauthorized user shouldn't get current user info" do
    get info_api_users_path(format: :json)
    assert_response :unauthorized
  end

  test "should get current user info" do
    login_as @user
    get info_api_users_path(format: :json)
    assert_response :success
  end

  test "admin should create an user" do
    login_as @admin

    new_user_email = "test@test.org"
    refute User.find_by(email: new_user_email)
    assert_difference -> { Events::CreatedEvent.count } do
      assert_emails 1 do
        post api_users_path, as: :json, params: {
          email: new_user_email,
          firstname: "Test",
          lastname: "User",
          group_ids: Group.all.map(&:id)
        }
      end
    end
    assert_response :success
    assert_account_confirmation_email(new_user_email)

    new_user = User.find_by(email: new_user_email)
    assert new_user
    assert_equal "Test", new_user.firstname
    assert_equal "User", new_user.lastname
    assert_equal Group.all.map(&:id), new_user.group_ids
  end

  test "non admin shouldn't create an user" do
    login_as @user

    assert_no_emails do
      post api_users_path, as: :json, params: {
        email: "test@test.org",
        firstname: "Test",
        lastname: "User"
      }
    end
    assert_response :forbidden
  end

  test "admin may not make himself non-admin" do
    login_as @admin
    patch update_admin_status_api_user_setting_path(@admin), as: :json, params: {admin: false}
    assert_response :forbidden
  end

  test "admin may make other user admin" do
    login_as @admin
    patch update_admin_status_api_user_setting_path(@user), as: :json, params: {admin: true}
    assert_response :success
  end

  test "non admin may not make other user admin" do
    login_as @user
    patch update_admin_status_api_user_setting_path(@user), as: :json, params: {admin: false}
    assert_response :forbidden
  end

  test "deactivated user shouldn't be able to login" do
    @user.deactivate

    post user_session_path, params: {
      user: {
        email: @user.email,
        password: "password"
      }
    }
    assert_redirected_to new_user_session_path

    refute @request.env["warden"].authenticated?
  end

  test "unconfirmed user shouldn't be able to use API (user-info as example)" do
    login_as @user
    @user.update!(confirmed_at: nil)
    refute @user.confirmed?

    get info_api_users_path(format: :json)
    assert_redirected_to new_user_session_path
  end

  test "deactivated user shouldn't be able to use API (user-info as example)" do
    login_as @user
    @user.deactivate
    assert @user.deactivated?

    get info_api_users_path(format: :json)
    assert_redirected_to new_user_session_path
  end

  test "signed-in user should be able to ping" do
    login_as @user
    get ping_api_users_path
    assert_response :success
  end

  test "not signed-in user shouldn't be able to ping" do
    logout
    get ping_api_users_path
    assert_response :unauthorized
  end

  test "not signed-in user with a share-link should be able to ping" do
    logout
    share_link = ShareLink.create!(shareable: Task.first)
    get ping_api_users_path(share_token: share_link.token)
    assert_response :success
  end
end
