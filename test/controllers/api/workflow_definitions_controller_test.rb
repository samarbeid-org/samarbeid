require "test_helper"

class Api::WorkflowDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "admin@example.org")

    @workflow_definitions_with_system_ones_visible_for_user = WorkflowDefinition.includes(:groups).where(groups: {id: @user.groups})
    @workflow_definitions_visible_for_user = @workflow_definitions_with_system_ones_visible_for_user.not_system_process_definition_single_task
    @workflow_definition = @workflow_definitions_visible_for_user.first
  end

  test "#index should return all visible definitions for non-admin user" do
    login_as(@user)

    visible_workflow_definition_count = @workflow_definitions_with_system_ones_visible_for_user.count
    assert_operator WorkflowDefinition.count, :>=, visible_workflow_definition_count
    assert_operator visible_workflow_definition_count, :<=, 99

    get api_workflow_definitions_url(format: :json, params: {per_page: 99})
    assert_response :success

    assert_equal 1, @response.parsed_body["total_pages"]
    assert_equal visible_workflow_definition_count, @response.parsed_body["result"].count
  end

  test "#index should return all definitions for admin user" do
    login_as(@admin)

    visible_workflow_definition_count = WorkflowDefinition.count
    assert_operator visible_workflow_definition_count, :<=, 99

    get api_workflow_definitions_url(format: :json, params: {per_page: 99})
    assert_response :success

    assert_equal 1, @response.parsed_body["total_pages"]
    assert_equal visible_workflow_definition_count, @response.parsed_body["result"].count
  end

  test "#index should have a working filter for state" do
    login_as(@user)

    count_exp = @workflow_definitions_with_system_ones_visible_for_user.activated.count
    get api_workflow_definitions_url(format: :json)
    assert_response :success
    assert_equal count_exp, @response.parsed_body["result"].count

    assert_difference -> { @workflow_definitions_with_system_ones_visible_for_user.deactivated.count } do
      perform_enqueued_jobs do
        @workflow_definitions_visible_for_user.activated.first.deactivate # prevent to deactivate system definitions
      end
    end
    count_exp = @workflow_definitions_with_system_ones_visible_for_user.deactivated.count
    get api_workflow_definitions_url(format: :json, params: {state: "deactivated"})
    assert_response :success
    assert_equal count_exp, @response.parsed_body["result"].count

    count_exp = @workflow_definitions_with_system_ones_visible_for_user.count
    get api_workflow_definitions_url(format: :json, params: {state: "all"})
    assert_response :success
    assert_equal count_exp, @response.parsed_body["result"].count
  end

  test "#index should order prepend active definitions without any groups and return correct count" do
    login_as(@admin)

    wd = WorkflowDefinition.activated.not_system_process_definition_single_task.order(name: :asc).find(17)

    get api_workflow_definitions_url(format: :json, params: {per_page: 99, order: "name_asc"})
    assert_response :success

    assert_equal wd.id, @response.parsed_body["result"].first["id"]
    assert_equal 1, @response.parsed_body["hiddenElementsCount"]

    perform_enqueued_jobs do
      wd.deactivate
    end

    get api_workflow_definitions_url(format: :json, params: {per_page: 99, order: "name_asc"})
    assert_response :success

    refute_equal wd.id, @response.parsed_body["result"].first["id"]
    assert_equal 0, @response.parsed_body["hiddenElementsCount"]
  end

  test "#list should return definitions but no deactivated ones" do
    login_as(@user)

    assert_operator @workflow_definitions_with_system_ones_visible_for_user.count, :<=, 99
    perform_enqueued_jobs do
      @workflow_definition.deactivate
    end
    assert_operator @workflow_definitions_with_system_ones_visible_for_user.deactivated.count, :>=, 0

    get list_api_workflow_definitions_path(format: :json, params: {page: 1, per_page: 99})
    assert_response :success

    assert_equal @workflow_definitions_with_system_ones_visible_for_user.activated.count, @response.parsed_body.count
  end

  test "#list should also return definitions without groups for admins depending on filter_by_groups_only parameter" do
    login_as(@admin)

    definitions = WorkflowDefinition.activated.order(name: :asc)
    definition = definitions.first

    perform_enqueued_jobs do
      definition.groups = []
    end

    get list_api_workflow_definitions_path(format: :json, params: {page: 1, per_page: 99})
    assert_response :success
    assert_equal definitions.count, @response.parsed_body.count
    assert_equal definition.id, @response.parsed_body.first["id"]

    visible_definitions_for_user = WorkflowDefinition.activated.includes(:groups).where(groups: {id: @admin.groups})
    get list_api_workflow_definitions_path(format: :json, params: {page: 1, per_page: 99, filter_by_groups_only: true})
    assert_response :success
    assert_equal visible_definitions_for_user.count, @response.parsed_body.count
  end

  test "#list should support paging of results" do
    login_as @user

    (1..10).each { |i| WorkflowDefinition.create!(name: "Test-Workflow-Definition #{i}", groups: Group.all) }

    result_count = @workflow_definitions_with_system_ones_visible_for_user.activated.count
    assert_operator result_count, :>, 10
    assert_operator result_count, :<, 20

    get list_api_workflow_definitions_path(format: :json, params: {page: 1, per_page: 10})
    assert_response :success
    assert_equal 10, @response.parsed_body.length

    get list_api_workflow_definitions_path(format: :json, params: {page: 2, per_page: 10})
    assert_response :success
    assert_equal result_count - 10, @response.parsed_body.length
  end

  test "should get show" do
    login_as(@user)

    get api_workflow_definition_path(@workflow_definition, format: :json)
    assert_response :success
  end

  test "shouldn't get show for definition which is not assigned to a group of the user" do
    login_as(@user)

    unaccessible_workflow_definition = WorkflowDefinition.where.not(id: @workflow_definitions_visible_for_user).first
    get api_workflow_definition_path(unaccessible_workflow_definition, format: :json)
    assert_response :forbidden
  end

  test "#show_system_process_definition_single_task should return show_system_process_definition_single_task" do
    login_as(@user)

    get show_system_process_definition_single_task_api_workflow_definitions_path(format: :json)
    assert_response :success

    single_task_process = WorkflowDefinition.system_process_definition_single_task

    assert_equal single_task_process.id, @response.parsed_body["id"]
    assert_equal single_task_process.name, @response.parsed_body["name"]
    assert_equal single_task_process.description, @response.parsed_body["description"]
    assert_equal "system_process_definition_single_task", @response.parsed_body["systemIdentifier"]
  end

  test "should create new workflow_definitions with groups of user" do
    login_as(@user)

    assert_difference -> { WorkflowDefinition.count } do
      post api_workflow_definitions_path(format: :json), params: {workflow_definition: {name: "A new name"}}
    end
    assert_response :success

    assert_equal @user.groups.map(&:id).sort, @response.parsed_body["groups"].map { |g| g["id"] }.sort
  end

  test "#create with workflow_id param should create workflow definition with structure of workflow instance" do
    login_as(@user)

    workflow = @workflow_definition.workflows.first
    refute_nil workflow

    assert_difference -> { WorkflowDefinition.count } do
      post api_workflow_definitions_path(format: :json), params: {workflow_id: workflow.id, workflow_definition: {name: "A new name"}}
    end
    assert_response :success

    new_workflow_definition = WorkflowDefinition.find(@response.parsed_body["id"])
    refute_equal @workflow_definition, new_workflow_definition
    assert_equal new_workflow_definition, workflow.reload.workflow_definition
  end

  test "should update" do
    login_as(@user)

    assert_changes -> { @workflow_definition.reload.name } do
      assert_changes -> { @workflow_definition.reload.description } do
        patch api_workflow_definition_path(@workflow_definition, format: :json),
          params: {name: "A new name", description: "Something different"}
      end
    end
    assert_response :success
  end

  test "only admin should update groups of definition" do
    login_as(@user)

    assert_no_changes -> { @workflow_definition.reload.group_ids } do
      assert_changes -> { @workflow_definition.reload.name } do
        assert_changes -> { @workflow_definition.reload.description } do
          patch api_workflow_definition_path(@workflow_definition, format: :json),
            params: {name: "A new name", description: "Something different", group_ids: Group.pluck(:id)}
        end
      end
    end
    assert_response :success
  end

  test "shouldn't update definition which is not assigned to a group of the user" do
    login_as(@user)

    unaccessible_workflow_definition = WorkflowDefinition.where.not(id: @workflow_definitions_visible_for_user).first

    assert_no_changes -> { unaccessible_workflow_definition.reload.name }, -> { unaccessible_workflow_definition.reload.description } do
      patch api_workflow_definition_path(unaccessible_workflow_definition, format: :json),
        params: {name: "A new name", description: "Something different"}
    end
    assert_response :forbidden
  end

  test "#update with active should activate/deactivate definition and have correct deactivatedAt value" do
    login_as(@user)

    assert_changes -> { @workflow_definition.reload.deactivated? }, from: false, to: true do
      patch api_workflow_definition_path(@workflow_definition, format: :json), params: {active: false}
    end
    assert_response :success

    refute_nil @response.parsed_body["deactivatedAt"]
    assert_equal "[deaktiviert] #{@workflow_definition.name}", @response.parsed_body["name"]
  end

  test "should destroy workflow definition without instances" do
    login_as(@user)

    workflow_definition_without_instances = @workflow_definitions_visible_for_user.find { |wd| wd.workflows.count == 0 }
    refute_nil workflow_definition_without_instances
    assert_difference -> { WorkflowDefinition.count }, -1 do
      delete api_workflow_definition_path(workflow_definition_without_instances, format: :json)
    end
    assert_response :success
  end

  test "shouldn't destroy workflow definition with instances" do
    login_as(@user)

    workflow_definition_with_instances = @workflow_definitions_visible_for_user.find { |wd| wd.workflows.count > 0 }
    refute_nil workflow_definition_with_instances
    assert_no_difference -> { WorkflowDefinition.count } do
      delete api_workflow_definition_path(workflow_definition_with_instances, format: :json)
    end
    assert_response :unprocessable_entity
  end

  test "shouldn't destroy workflow definition which is not assigned to a group of the user" do
    login_as(@user)

    workflow_definition_without_instances = WorkflowDefinition.where.not(id: @workflow_definitions_visible_for_user).find { |wd| wd.workflows.count == 0 }
    refute_nil workflow_definition_without_instances
    assert_no_difference -> { WorkflowDefinition.count } do
      delete api_workflow_definition_path(workflow_definition_without_instances, format: :json)
    end
    assert_response :forbidden
  end

  test "should send error while updating or deleting system_process_definition_single_task" do
    login_as(@user)

    patch api_workflow_definition_path(WorkflowDefinition.system_process_definition_single_task, format: :json),
      params: {name: "Changed process definition name"}
    assert_response :unprocessable_entity

    delete api_workflow_definition_path(WorkflowDefinition.system_process_definition_single_task), as: :json
    assert_response :unprocessable_entity
  end
end
