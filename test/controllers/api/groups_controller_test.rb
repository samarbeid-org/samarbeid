require "test_helper"

class Api::GroupsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "#list should support paging of results" do
    login_as @admin

    (1..10).each { |i| Group.create!(name: "Testgroup #{i}") }

    result_count = Group.count
    assert_operator result_count, :>, 10
    assert_operator result_count, :<, 20

    get list_api_groups_path, as: :json, params: {page: 1, per_page: 10}
    assert_response :success
    assert_equal 10, @response.parsed_body.length

    get list_api_groups_path, as: :json, params: {page: 2, per_page: 10}
    assert_response :success
    assert_equal result_count - 10, @response.parsed_body.length
  end

  test "any user should get index of groups" do
    login_as @user
    get api_groups_path, as: :json
    assert_response :success
  end

  test "any user should get any group" do
    login_as @user
    get api_group_path(@user.groups.first), as: :json
    assert_response :success

    other_group = Group.where.not(id: @user.groups.map(&:id)).first
    refute_nil other_group

    get api_group_path(other_group), as: :json
    assert_response :success
  end

  test "non admin shouldn't create a group" do
    login_as @user
    post api_groups_path, as: :json, params: {
      name: "Test group"
    }
    assert_response :forbidden
  end

  test "admin should create a group" do
    login_as @admin
    post api_groups_path, as: :json, params: {
      name: "Test group",
      description: "Test Description",
      user_ids: User.all.limit(5).pluck(:id),
      workflow_definition_ids: WorkflowDefinition.all.limit(5).pluck(:id)
    }
    assert_response :success
  end

  test "should send an error while creating a group with non valid attributes" do
    login_as @admin
    post api_groups_path, as: :json, params: {
      name: ""
    }
    assert_response :unprocessable_entity
  end

  test "non admin shouldn't update a group" do
    login_as @user
    patch api_group_path(Group.find_by!(name: "tomsy")), as: :json, params: {
      name: "Test group"
    }
    assert_response :forbidden
  end

  test "admin should update a group" do
    login_as @admin
    patch api_group_path(Group.find_by!(name: "tomsy")), as: :json, params: {
      name: "Test group",
      description: "Test Description",
      user_ids: User.all.limit(5).pluck(:id),
      workflow_definition_ids: WorkflowDefinition.all.limit(5).pluck(:id)
    }
    assert_response :success
  end

  test "should send an error while updating a group with non valid attributes" do
    login_as @admin
    patch api_group_path(Group.find_by!(name: "tomsy")), as: :json, params: {
      name: ""
    }
    assert_response :unprocessable_entity
  end

  test "non admin shouldn't destroy a group" do
    login_as @user
    delete api_group_path(Group.find_by!(name: "tomsy")), as: :json
    assert_response :forbidden
  end

  test "admin should destroy a group" do
    login_as @admin
    delete api_group_path(Group.find_by!(name: "tomsy")), as: :json
    assert_response :success
  end

  test "should send an error while deleting a not existing group" do
    login_as @admin
    delete api_group_path(id: Group.maximum(:id).next), as: :json
    assert_response :not_found
  end

  test "should send error while updating or deleting system_group_all" do
    login_as @admin
    patch api_group_path(Group.system_group_all), as: :json, params: {
      name: "Changed group name"
    }
    assert_response :unprocessable_entity

    delete api_group_path(Group.system_group_all), as: :json
    assert_response :unprocessable_entity
  end
end
