require "test_helper"

class Api::ExternalApiAccessControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
  end

  test "should get create" do
    login_as(@current_user)
    refute @current_user.api_token

    post api_external_api_access_url

    assert_response :success
    assert @current_user.reload.api_token
  end

  test "should get destroy" do
    @current_user.create_token(:api_token)
    login_as(@current_user)

    delete api_external_api_access_url

    assert_response :success
    refute @current_user.reload.api_token
  end

  test "if not logged in should reject get create" do
    post api_external_api_access_url
    assert_response :unauthorized
  end

  test "if not logged in should reject get destroy" do
    delete api_external_api_access_url
    assert_response :unauthorized
  end
end
