require "test_helper"

class Api::UsersAndGroupsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "list of users and groups shouldn't include deactivated users" do
    @user.deactivate
    login_as @admin
    get list_api_users_and_groups_path(format: :json)
    assert_response :success

    assert @response.parsed_body.any?
    refute @response.parsed_body.any? { |user_or_group| user_or_group["type"] == "user" && user_or_group["id"] == @user.id }
  end

  test "list of users and groups should support paging of results" do
    login_as @admin

    result_count = User.count + Group.count
    assert_operator result_count, :>, 10
    assert_operator result_count, :<, 20

    get list_api_users_and_groups_path(format: :json, params: {page: 1, per_page: 10})
    assert_response :success
    assert_equal 10, @response.parsed_body.length

    get list_api_users_and_groups_path(format: :json, params: {page: 2, per_page: 10})
    assert_response :success
    assert_equal result_count - 10, @response.parsed_body.length
  end

  test "list of users and groups should return users and groups in right order" do
    login_as @user
    get list_api_users_and_groups_path(format: :json)
    assert_response :success

    assert_equal [["user", 133235], ["user", 1], ["group", 9], ["group", 1], ["group", 8], ["user", 133238], ["user", 133237], ["user", 133236], ["user", 133239], ["group", 5]], @response.parsed_body.pluck("type", "id")
  end
end
