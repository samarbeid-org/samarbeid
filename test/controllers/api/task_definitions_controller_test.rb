require "test_helper"

class Api::TaskDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "dsb@example.org")
    @workflow_definitions = WorkflowDefinition.includes(:groups).where(groups: {id: @user.groups})
    @workflow_definition = @workflow_definitions.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @task_definition = @workflow_definition.direct_task_definitions.first
    login_as(@user)
  end

  test "should create task definition and add it to the end of an existing workflow definition" do
    task_count_before = @workflow_definition.direct_task_definitions.count

    assert_difference -> { @workflow_definition.direct_task_definitions.reload.count } do
      post api_task_definitions_path(format: :json), params: {task_definition: {workflow_definition_id: @workflow_definition.id}}
    end
    assert_response :success

    assert_equal task_count_before + 1, @response.parsed_body["structure"].count { |elem| elem["type"] == "task_definition" }
    last_item = @response.parsed_body["structure"].last
    assert_equal "task_definition", last_item["type"]
    assert_equal "Neue Aufgabe", last_item["name"]
  end

  test "should be updatable" do
    refute_equal "Updated name", @task_definition.name
    refute_equal @user, @task_definition.candidate_assignee
    refute_equal 2, @task_definition.due_in_days
    refute_equal 1, @task_definition.deferral_in_days

    patch api_task_definition_path(@task_definition, format: :json), params: {
      task_definition: {
        name: "Updated name",
        candidate_assignee_id: @user.id,
        due_in_days: 2,
        deferral_in_days: 1
      }
    }
    assert_response :success

    @task_definition.reload
    assert_equal "Updated name", @task_definition.name
    assert_equal @user, @task_definition.candidate_assignee
    assert_equal 2, @task_definition.due_in_days
    assert_equal 1, @task_definition.deferral_in_days
  end

  test "should destroy a task definition" do
    assert_difference -> { @workflow_definition.direct_task_definitions.count }, -1 do
      delete api_task_definition_path(@task_definition)
    end
    assert_response :success
  end

  test "should be possible to clone a task definition" do
    assert_difference -> { TaskDefinition.count } do
      patch clone_api_task_definition_url(@task_definition, format: :json)
    end
    assert_response :success
  end

  test "#update_candidate_contributors should work with users and groups and only add activated users" do
    group_all = Group.find_by!(name: "Alle")
    User.find_by!(email: "srs_user@example.org").deactivate

    assert_equal 0, @task_definition.candidate_contributors.count

    assert_difference -> { @task_definition.candidate_contributors.count }, group_all.users.activated.count do
      patch update_candidate_contributors_api_task_definition_url(@task_definition, format: :json), params: {
        candidate_contributors: [{type: "group", id: group_all.id}]
      }
    end
    assert_response :success

    patch update_candidate_contributors_api_task_definition_url(@task_definition, format: :json), params: {
      candidate_contributors: [{type: "user", id: @user.id}]
    }
    assert_response :success

    assert_equal 1, @task_definition.candidate_contributors.count
  end

  # Moving tests

  test "task_definition should be movable to beginning" do
    movee = @workflow_definition.direct_task_definitions.last
    assert_operator @workflow_definition.item_definitions.index(movee), :>, 0
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: 0}
    assert_response :success
    assert_equal movee.id, @workflow_definition.reload.item_definitions.first.id
  end

  test "task_definition should be movable to middle" do
    movee = @workflow_definition.direct_task_definitions.first
    assert_operator @workflow_definition.item_definitions.index(movee), :==, 0
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: 1}
    assert_response :success
    assert_equal movee.id, @workflow_definition.reload.item_definitions.second.id
  end

  test "task_definition from beginning should be movable" do
    movee = @workflow_definition.direct_task_definitions.first
    assert_operator @workflow_definition.item_definitions.index(movee), :!=, 2
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: 2}
    assert_response :success
    assert_equal movee.id, @workflow_definition.reload.item_definitions.third.id
  end

  test "task_definitions should be movable to end with higher index" do
    movee = @workflow_definition.direct_task_definitions.first
    new_index = @workflow_definition.item_definitions.length + 99
    assert_operator @workflow_definition.item_definitions.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert_equal movee, @workflow_definition.reload.item_definitions.last
  end

  test "task_definition should be movable from workflow_definition into block_definition (beginning)" do
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @block_definition = @workflow_definition.block_definitions.first
    assert_operator @block_definition.item_definitions.length, :>=, 2

    movee = @workflow_definition.direct_task_definitions.first
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: 0, target_id: @block_definition.id, target_type: @block_definition.class.name.underscore}
    assert_response :success
    refute_includes @workflow_definition.reload.direct_task_definitions, movee
    assert_equal movee, @block_definition.reload.direct_task_definitions.first
  end

  test "task_definition should be movable from workflow_definition into block_definition (end)" do
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @block_definition = @workflow_definition.block_definitions.first
    assert_operator @block_definition.item_definitions.length, :>=, 2

    movee = @workflow_definition.direct_task_definitions.first
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: @block_definition.item_definitions.length, target_id: @block_definition.id, target_type: @block_definition.class.name.underscore}
    assert_response :success
    refute_includes @workflow_definition.reload.direct_task_definitions, movee
    assert_equal movee, @block_definition.reload.direct_task_definitions.last
  end

  test "task_definition should be movable from workflow_definition into block_definition (middle)" do
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @block_definition = @workflow_definition.block_definitions.first
    assert_operator @block_definition.item_definitions.length, :>=, 2

    movee = @workflow_definition.direct_task_definitions.first
    assert_not_nil movee
    patch move_api_task_definition_path(movee, format: :json), params: {index: 1, target_id: @block_definition.id, target_type: @block_definition.class.name.underscore}
    assert_response :success
    refute_includes @workflow_definition.reload.direct_task_definitions, movee
    assert_equal movee, @block_definition.reload.direct_task_definitions.second
  end

  test "task_definition should be movable from block_definition into workflow_definition (beginning)" do
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    block_definition = @workflow_definition.block_definitions.first
    movee = block_definition.direct_task_definitions.first
    assert_not_nil movee

    patch move_api_task_definition_path(movee, format: :json), params: {index: 0, target_id: @workflow_definition.id, target_type: "process_definition"}
    assert_response :success
    refute_includes block_definition.reload.direct_task_definitions, movee
    assert_equal movee, @workflow_definition.reload.direct_task_definitions.first
  end

  test "task_definition should be movable from block_definition to same block_definition" do
    workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    block_definition = workflow_definition.block_definitions.first

    movee = block_definition.direct_task_definitions.first
    refute_includes workflow_definition.direct_task_definitions, movee
    assert_includes block_definition.direct_task_definitions, movee
    assert block_definition.direct_task_definitions.count > 1
    patch move_api_task_definition_path(movee, format: :json), params: {index: 999, target_id: block_definition.id, target_type: block_definition.class.name.underscore}
    assert_response :success

    refute_includes workflow_definition.reload.direct_task_definitions, movee
    assert_equal movee, block_definition.reload.direct_task_definitions.last
    assert_equal movee, block_definition.reload.item_definitions.last
  end

  test "task_definition should be movable from block_definition to same block_definition (without giving target)" do
    workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    block_definition = workflow_definition.block_definitions.first

    movee = block_definition.direct_task_definitions.first
    refute_includes workflow_definition.direct_task_definitions, movee
    assert_includes block_definition.direct_task_definitions, movee
    assert block_definition.direct_task_definitions.count > 1
    patch move_api_task_definition_path(movee, format: :json), params: {index: 999}
    assert_response :success

    refute_includes workflow_definition.reload.direct_task_definitions, movee
    assert_equal movee, block_definition.reload.direct_task_definitions.last
  end

  test "task_definition should be movable from block_definition to other block_definition" do
    workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    block_definition_1 = workflow_definition.block_definitions.first
    block_definition_2 = workflow_definition.block_definitions.last
    refute_equal block_definition_1, block_definition_2

    movee = block_definition_1.direct_task_definitions.first
    refute_includes workflow_definition.direct_task_definitions, movee
    assert_includes block_definition_1.direct_task_definitions, movee
    refute_includes block_definition_2.direct_task_definitions, movee

    patch move_api_task_definition_path(movee, format: :json), params: {index: 1, target_id: block_definition_2.id, target_type: block_definition_2.class.name.underscore}
    assert_response :success

    assert_includes block_definition_2.direct_task_definitions, movee
    refute_includes block_definition_1.direct_task_definitions, movee
    refute_includes workflow_definition.reload.direct_task_definitions, movee
    assert_equal movee, block_definition_2.reload.direct_task_definitions.second
  end

  test "should create, update, destroy or move task definition of system_process_definition_single_task" do
    post api_task_definitions_path(format: :json), params: {task_definition: {workflow_definition_id: WorkflowDefinition.system_process_definition_single_task.id}}
    assert_response :unprocessable_entity

    single_task_definition = WorkflowDefinition.system_process_definition_single_task.all_task_definitions.first

    patch api_task_definition_path(single_task_definition, format: :json), params: {
      task_definition: {name: "Updated name"}
    }
    assert_response :unprocessable_entity

    delete api_task_definition_path(single_task_definition)
    assert_response :unprocessable_entity

    patch move_api_task_definition_path(single_task_definition, format: :json), params: {index: 99}
    assert_response :unprocessable_entity
  end
end
