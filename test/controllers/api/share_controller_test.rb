require "test_helper"

class Api::ShareControllerTest < ActionDispatch::IntegrationTest
  def setup
    @share_link = ShareLink.create!(shareable: Task.first)
    logout
  end

  test "should get show" do
    get api_share_url(share_token: @share_link.token)
    assert_response :success
    assert_share_active @share_link.shareable.id

    completed_share_link = ShareLink.completed.first
    assert completed_share_link
    get api_share_url(share_token: completed_share_link.token)
    assert_response :success
    assert_share_completed
  end

  test "should complete" do
    patch complete_api_share_url(share_token: @share_link.token)
    assert_response :success
    assert_share_completed
  end

  # TODO: Implement tests after implementing action properly
  # test "should allow cancel as proper user" do
  #   login_as User.first
  #   patch api_share_cancel_url(share_token: @share_link.token)
  #   assert_response :redirect
  #   assert_redirected_to api_share_show_path(share_link: @share_link.token)
  # end
  #
  # test "should not allow cancel for share-link only user" do
  #   patch api_share_cancel_url(share_token: @share_link.token)
  #   assert_response :redirect
  #   assert_redirected_to new_user_session_path
  # end

  private

  def assert_share_active(id)
    refute @response.parsed_body["completed"]
    refute @response.parsed_body["canceled"]
    assert @response.parsed_body.key?("task")
    assert_equal id, @response.parsed_body["task"]["id"]
  end

  def assert_share_completed
    assert @response.parsed_body["completed"]
    refute @response.parsed_body["canceled"]
    refute @response.parsed_body.key?("task")
  end
end
