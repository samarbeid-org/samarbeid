require "test_helper"

class Api::DataItemsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")
    login_as(@current_user)

    @workflow = Workflow.find_by!(title: "MM-20")
    @content_item1 = @workflow.content_items.find_by!(label: "E-Mail-Adresse Absender")
    @task_item1 = @content_item1.task_items.where(info_box: false).first
    @task = @task_item1.task
    @task_item2 = @task.task_items.second
  end

  # index
  test "#index should get business data of a task" do
    get api_data_items_path(format: :json), params: {task_id: @task.id}
    assert_response :success
  end

  test "#index should be forbidden for users without access for the task" do
    login_as(@no_access_user)

    get api_data_items_path(format: :json), params: {task_id: @task.id}
    assert_response :forbidden
  end

  # show
  test "#show should return single data item of a task" do
    get api_data_item_path(@task_item1, format: :json)
    assert_response :success
  end

  test "#show should be forbidden for users without access for the task" do
    login_as(@no_access_user)

    get api_data_item_path(@task_item1, format: :json)
    assert_response :forbidden
  end

  test "#show should also work for external users authorized via share link" do
    logout
    share_link = ShareLink.create!(shareable: @task_item1.task)
    assert share_link.can?(:show, @task_item1)
    get api_data_item_path(@task_item1, format: :json), params: {share_token: share_link.token}
    assert_response :success
  end

  test "#show should also work for data items which are locked by external users" do
    share_link = ShareLink.create!(shareable: @task_item1.task)
    @task_item1.content_item.lock_with!(@task_item1.content_item.lock_version, share_link)

    get api_data_item_path(@task_item1, format: :json)

    assert_response :success
  end

  # create
  test "#create should add data field to task" do
    task = @workflow.direct_tasks.last
    task.task_items.destroy_all
    assert task.task_items.empty?

    post api_data_items_path(format: :json), params: {
      task_id: task.id,
      content_item: {label: "Text field", content_type: "text"},
      task_item: {required: false}
    }
    assert_response :success

    task.reload
    assert task.task_items.last.content_item.label == "Text field"
    assert task.task_items.last.content_item.content_type == ContentTypes::Text

    post api_data_items_path(format: :json), params: {
      task_id: task.id,
      content_item: {label: "Email field", content_type: "email"},
      task_item: {required: true}
    }
    assert_response :success

    task.reload
    assert task.task_items.last.content_item.label == "Email field"
    assert task.task_items.last.content_item.content_type == ContentTypes::Email
  end

  # update value
  test "#update should update business data of a task with getting assignee and contributor of this task" do
    assert_nil @task.assignee
    refute @task.contributors.include?(@current_user)

    patch api_data_item_path(@task_item1, format: :json), params: {value: "test@example.org", lock_version: @content_item1.lock_version}
    assert_response :success

    task_response = @response.parsed_body["workflow"]["structure"].find { |elem| elem["type"] == "task" && elem["id"] == @task.id }
    refute_nil task_response["assignee"]
    assert_equal @current_user.id, task_response["assignee"]["id"]
    refute_nil task_response["contributors"].find { |c| c["id"] == @current_user.id }

    assert @response.parsed_body["dataField"].key?("lastUpdated")
    assert @response.parsed_body["dataField"].key?("confirmedAt")
  end

  test "#update should work for share links and NOT return workflow information" do
    logout
    share_link = ShareLink.create!(shareable: @task_item1.task)

    patch api_data_item_path(@task_item1, format: :json), params: {value: "test@example.org", lock_version: @content_item1.lock_version, share_token: share_link.token}
    assert_response :success

    assert_nil @response.parsed_body["workflow"]
    assert @response.parsed_body["dataField"].present?
    refute @response.parsed_body["dataField"].key?("lastUpdated")
    refute @response.parsed_body["dataField"].key?("confirmedAt")
  end

  test "#update should be forbidden for users without access for the task" do
    login_as(@no_access_user)

    patch api_data_item_path(@task_item1, format: :json), params: {value: "test@example.org", lock_version: @content_item1.lock_version}
    assert_response :forbidden
  end

  test "#update should refused if data has been changed in between" do
    old_lock_version = @content_item1.lock_version
    @content_item1.touch

    patch api_data_item_path(@task_item1, format: :json), params: {value: "test@example.org", lock_version: old_lock_version}
    assert_response :unprocessable_entity
  end

  # update definition
  test "label should be updatable (only)" do
    refute_equal "new label", @task_item1.content_item.label
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: "new label"}}
    assert_response :success
    assert_equal "new label", @task_item1.reload.content_item.label
  end

  test "update definition (such as label) should be forbidden for share link users" do
    logout
    share_link = ShareLink.create!(shareable: @task_item1.task)
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: "new label"}, share_token: share_link.token}
    assert_response :forbidden

    patch api_data_item_url(@task_item1, format: :json), params: {task_item: {info_box: true, required: true}, share_token: share_link.token}
    assert_response :forbidden
  end

  test "required and infobox status should be updatable (only)" do
    refute_equal true, @task_item1.info_box
    refute_equal true, @task_item1.required
    patch api_data_item_url(@task_item1, format: :json), params: {task_item: {info_box: true, required: true}}
    assert_response :success
    @task_item1.reload
    assert_equal true, @task_item1.info_box
    assert_equal true, @task_item1.required
  end

  test "both label and required+infobox status should be updatable" do
    refute_equal "new label", @task_item1.content_item.label
    refute_equal true, @task_item1.info_box
    refute_equal true, @task_item1.required
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: "new label"}, task_item: {info_box: true, required: true}}
    assert_response :success
    assert_equal "new label", @task_item1.reload.content_item.label
    assert_equal true, @task_item1.info_box
  end

  test "add_to_calendar should be updatable" do
    user = User.find_by(email: "admin@example.org")
    login_as(user)

    calendar_task_item = TaskItem.find(147)

    assert calendar_task_item.add_to_calendar
    patch api_data_item_url(calendar_task_item, format: :json), params: {task_item: {add_to_calendar: false}}
    assert_response :success
    calendar_task_item.reload
    refute calendar_task_item.add_to_calendar

    patch api_data_item_url(calendar_task_item, format: :json), params: {task_item: {add_to_calendar: true}}
    assert_response :success
    calendar_task_item.reload
    assert calendar_task_item.add_to_calendar
  end

  test "add_to_calendar should not be updatable on non date/datetime datafields" do
    patch api_data_item_url(@task_item1, format: :json), params: {task_item: {add_to_calendar: true}}
    assert_response :unprocessable_entity

    assert @response.parsed_body.to_s.match("Add to calendar Für diesen Datentyp nicht möglich!")
  end

  test "label (and other definition stuff) should not be updateable by share links" do
    refute_equal "new label", @task_item1.content_item.label
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: "new label"}}
    assert_response :success
    assert_equal "new label", @task_item1.reload.content_item.label
  end

  test "merging of error messages for task_item and content_item" do
    refute_equal "new label", @task_item1.content_item.label
    refute_equal true, @task_item1.info_box
    patch api_data_item_url(@task_item1, format: :json), params: {content_item: {label: nil}, task_item: {info_box: "hi"}}
    assert_response :unprocessable_entity
    assert_not_nil @task_item1.reload.content_item.label
    assert_not_nil @task_item1.info_box
    assert @response.parsed_body.key?("label")

    # task_item doesn't validate anything. The "hi" in the api call is interpreted as true
    assert_equal true, @task_item1.info_box
    assert_equal 1, @response.parsed_body.size
  end

  # destroy
  test "deleting task_item which is not alone in its content_item" do
    get api_data_items_path(format: :json), params: {task_id: @task_item1.task.id}
    data_field_infos = @response.parsed_body["dataFields"].find { _1["definition"]["task_item_id"] == @task_item1.id }
    assert_operator data_field_infos["definition"]["task_item_count"], :>, 1

    assert_difference -> { @content_item1.task_items.count }, -1 do
      delete api_data_item_url(@task_item1)
      assert_response :success
      assert_raises ActiveRecord::RecordNotFound do
        @task_item1.reload
      end
    end
    assert_nothing_raised do
      @content_item1.reload
    end
  end

  test "deleting task_item which is alone in its content_item" do
    @content_item1.task_items.where.not(id: @task_item1.id).destroy_all
    assert_equal 1, @content_item1.task_items.count

    get api_data_items_path(format: :json), params: {task_id: @task_item1.task.id}
    data_field_infos = @response.parsed_body["dataFields"].find { _1["definition"]["task_item_id"] == @task_item1.id }
    assert_equal 1, data_field_infos["definition"]["task_item_count"]

    delete api_data_item_url(@task_item1)
    assert_response :success

    assert_raises ActiveRecord::RecordNotFound do
      @task_item1.reload
    end
    assert_raises ActiveRecord::RecordNotFound do
      @content_item1.reload
    end
  end

  test "deleting task_item which precondition for block is not allowed" do
    precondition_content_item = @workflow.blocks.first.content_item
    assert_not_nil precondition_content_item
    assert_equal 1, precondition_content_item.task_items.count
    precondition_task_item = precondition_content_item.task_items.first

    delete api_data_item_url(precondition_task_item)
    assert_response :unprocessable_entity
    assert_equal ["Dieses Feld wird in einem Block als Bedingung verwendet."], @response.parsed_body["base"]["full"]

    assert_nothing_raised do
      precondition_task_item.reload
    end
    assert_nothing_raised do
      precondition_content_item.reload
    end
  end

  # move
  test "task item should be movable down (index_new > index_old)" do
    movee = @task_item1
    assert_not_nil movee

    other = @task_item2
    assert_not_nil other

    assert_equal 0, movee.position
    assert_equal 0, @task.task_items.index(movee)
    assert_not_nil other
    assert_equal 1, other.position
    assert_equal 1, @task.task_items.index(other)
    patch move_api_data_item_url(movee, format: :json), params: {index: 1}
    assert_response :success
    @task.reload
    movee.reload
    other.reload
    assert_equal 0, @task.task_items.index(other)
    assert_equal 1, @task.task_items.index(movee)

    # those positions are an implementation detail, could be any position as long as the index is correct
    assert_equal 1, other.position
    assert_equal 2, movee.position
  end

  test "task item should be movable up (index_new < index_old)" do
    movee = @task_item2
    assert_not_nil movee

    other = @task_item1
    assert_not_nil other

    assert_equal 1, movee.position
    assert_equal 1, @task.task_items.index(movee)
    assert_not_nil other
    assert_equal 0, other.position
    assert_equal 0, @task.task_items.index(other)
    patch move_api_data_item_url(movee, format: :json), params: {index: 0}
    assert_response :success

    @task.reload
    movee.reload
    other.reload
    assert_equal 1, @task.task_items.index(other)
    assert_equal 0, @task.task_items.index(movee)

    # those positions are an implementation detail, could be any position as long as the index is correct
    assert_equal 1, other.position
    assert_equal 0, movee.position
  end

  # lock and unlock
  test "#lock should lock content item and return new lock_version" do
    refute @task_item1.content_item.locked?

    patch lock_api_data_item_path(@task_item1, format: :json), params: {lock_version: @task_item1.content_item.lock_version}
    assert_response :success

    @task_item1.content_item.reload
    assert @task_item1.content_item.locked?

    assert_equal @task_item1.content_item.lock_version, @response.parsed_body
  end

  test "#lock should raise error if already locked by other user" do
    @task_item1.content_item.update!(locked_at: Time.now, locked_by: User.find_by!(email: "admin@example.org"))
    assert @task_item1.content_item.locked?

    assert_raise LockSupport::LockException do
      patch lock_api_data_item_path(@task_item1, format: :json), params: {lock_version: @task_item1.content_item.lock_version}
    end

    @task_item1.content_item.reload
    assert @task_item1.content_item.locked?
  end

  test "#lock should raise error if lock_version is obsolete" do
    @task_item1.content_item.update!(value: "test@example.org")
    assert_operator 0, :<, @task_item1.content_item.lock_version
    assert_raise ActiveRecord::StaleObjectError do
      patch lock_api_data_item_path(@task_item1, format: :json), params: {lock_version: 0}
    end

    @task_item1.content_item.reload
    refute @task_item1.content_item.locked?
  end

  test "#lock should be able to access via share link" do
    logout
    share_link = ShareLink.create!(shareable: @task_item1.task)
    assert share_link.can?(:lock, @task_item1)
    patch lock_api_data_item_path(@task_item1, format: :json), params: {lock_version: @task_item1.content_item.lock_version, share_token: share_link.token}
    assert_response :success

    @task_item1.content_item.reload
    assert @task_item1.content_item.locked?

    assert_equal share_link, @task_item1.content_item.locked_by
  end

  test "#unlock should unlock content item and return new lock_version" do
    @task_item1.content_item.update!(locked_at: Time.now, locked_by: @current_user)
    assert @task_item1.content_item.locked?

    patch unlock_api_data_item_path(@task_item1, format: :json), params: {lock_version: @task_item1.content_item.lock_version}
    assert_response :success

    @task_item1.content_item.reload
    refute @task_item1.content_item.locked?

    assert_equal @task_item1.content_item.lock_version, @response.parsed_body
  end

  test "#unlock should raise error if content item is locked by other user" do
    @task_item1.content_item.update!(locked_at: Time.now, locked_by: User.find_by!(email: "admin@example.org"))
    assert @task_item1.content_item.locked?

    assert_raise LockSupport::LockException do
      patch unlock_api_data_item_path(@task_item1, format: :json), params: {lock_version: @task_item1.content_item.lock_version}
    end

    @task_item1.content_item.reload
    assert @task_item1.content_item.locked?
  end

  test "#unlock should work even if lock_version is obsolete" do
    @task_item1.content_item.update!(locked_at: Time.now, locked_by: @current_user)
    assert_operator 0, :<, @task_item1.content_item.lock_version

    patch unlock_api_data_item_path(@task_item1, format: :json), params: {lock_version: 0}
    assert_response :success

    refute @task_item1.content_item.reload.locked?
  end

  # Access via share_link

  test "#lock allow access via share_link" do
    logout
    share_link = ShareLink.create!(shareable: @task_item1.task)
    assert share_link.can?(:lock, @task_item1)
    patch lock_api_data_item_path(@task_item1, format: :json), params: {lock_version: @task_item1.content_item.lock_version, share_token: share_link.token}
    assert_response :success

    @task_item1.content_item.reload
    assert @task_item1.content_item.locked?

    assert_equal @task_item1.content_item.lock_version, @response.parsed_body
  end
end
