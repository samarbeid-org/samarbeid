require "test_helper"

class Api::WorkflowsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")

    @srs_workflow = Workflow.find_by!(title: "srs-workflow")

    login_as @user
  end

  test "index should work" do
    get api_workflows_url(format: :json)
    assert_response :success
  end

  test "index should work when filtering" do
    params = {
      page: 1,
      query: "james",
      assignee_ids: [@user.id],
      contributor_ids: User.all.limit(3).pluck(:id),
      workflow_definition_ids: [WorkflowDefinition.first.id],
      filter_category: "WITH_DUE_TASKS",
      order: "created_at_desc"
    }

    get api_workflows_url(params.merge(format: :json))
    assert_response :success
  end

  test "index should export to excel" do
    user = User.find_by!(email: "admin@example.org")
    login_as(user)

    params = {
      filter_category: "ALL"
    }

    get api_workflows_url(params.merge(format: :xlsx))
    assert_response :success

    assert_equal "application/octet-stream", response.header["Content-Type"] # File download
    assert_equal Workflow.accessible_by(user.build_ability(true)).count, @controller.instance_variable_get(:@result).query.count # No pagination
  end

  test "should get list" do
    get list_api_workflows_path(format: :json)
    assert_response :success
  end

  test "should get list with filtering" do
    params = {
      query: "james",
      except: Workflow.all.limit(3).pluck(:id),
      page: 1, per_page: 99
    }
    get list_api_workflows_path(params.merge(format: :json))
    assert_response :success
  end

  test "#list should support paging of results" do
    result_count = Workflow.not_system_process_single_task.accessible_by(@user.ability, :read).count
    assert_operator result_count, :>, 10

    last_page_count = result_count % 10
    per_page = result_count - last_page_count

    get list_api_workflows_path, as: :json, params: {page: 1, per_page: per_page}
    assert_response :success
    assert_equal per_page, @response.parsed_body.length

    get list_api_workflows_path, as: :json, params: {page: 2, per_page: per_page}
    assert_response :success
    assert_equal last_page_count, @response.parsed_body.length
  end

  test "workflow#show should work" do
    get api_workflow_url(@srs_workflow, format: :json)
    assert_response :success
  end

  test "should get not_found response for deleted workflow show" do
    @srs_workflow.destroy!
    get api_workflow_url(@srs_workflow, format: :json)
    assert_response :not_found
  end

  test "should update workflow" do
    patch api_workflow_path(@srs_workflow, format: :json), params: {workflow: {title: "Ein neuer Titel"}}
    assert_response :success
  end

  test "should update_assignee" do
    patch update_assignee_api_workflow_path(@srs_workflow, format: :json), params: {assignee: @user.id}
    assert_response :success
  end

  test "should allow removing assignee" do
    patch update_assignee_api_workflow_path(@srs_workflow, format: :json), params: {assignee: nil}
    assert_response :success
  end

  test "should update_contributors" do
    patch update_contributors_api_workflow_path(@srs_workflow, format: :json), params: {contributors: User.all.limit(3).pluck(:id)}
    assert_response :success
  end

  test "should allow removing all contributors" do
    patch update_contributors_api_workflow_path(@srs_workflow, format: :json), params: {contributors: [nil]}
    assert_response :success
  end

  test "should get new workflow with default values" do
    get new_api_workflow_path(format: :json), params: {workflow_definition_id: @srs_workflow.workflow_definition.id}
    assert_response :success
  end

  test "should create new workflows" do
    assert_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {workflow_definition_id: @srs_workflow.workflow_definition.id, workflow: {title: "Test-Workflow"}}
    end
    assert_response :success
  end

  test "should create new workflow without title or assignee" do
    assert_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {workflow_definition_id: @srs_workflow.workflow_definition.id}
    end
    assert_response :success
  end

  test "should create new workflows with predecessor" do
    assert_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {
          workflow_definition_id: @srs_workflow.workflow_definition.id,
          predecessor_workflow_id: @srs_workflow.id,
          workflow: {title: "Test-Workflow"}
        }
    end
    assert_response :success
  end

  test "should create new single task workflows" do
    assert_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {
          workflow_definition_id: WorkflowDefinition.system_process_definition_single_task.id,
          workflow: {title: "Test-Task"}
        }
    end
    assert_response :success
  end

  test "shouldn't create workflow from deactivated definition" do
    @srs_workflow.workflow_definition.deactivate

    assert_no_difference -> { Workflow.count } do
      post api_workflows_path(format: :json),
        params: {
          workflow_definition_id: @srs_workflow.workflow_definition.id,
          workflow: {title: "Test-Workflow"}
        }
    end
    assert_response :unprocessable_entity
  end

  # Access Forbidden Tests - should be compacted and/or moved to different test

  test "no_access_user shouldn't be able to show a workflow" do
    login_as(@no_access_user)
    get api_workflow_url(@srs_workflow, format: :json)
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update (title and description) of a workflow" do
    login_as(@no_access_user)
    patch api_workflow_url(@srs_workflow, format: :json), params: {title: "updated title", description: "updated description"}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to set or unset the assignee of a workflow" do
    login_as(@no_access_user)
    patch update_assignee_api_workflow_url(@srs_workflow, format: :json), params: {assignee: @no_access_user.id}
    assert_response :forbidden

    patch update_assignee_api_workflow_url(@srs_workflow, format: :json), params: {assignee: nil}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update the contributors of a workflow" do
    login_as(@no_access_user)
    patch update_contributors_api_workflow_url(@srs_workflow, format: :json), params: {contributors: [@no_access_user.id]}
    assert_response :forbidden
  end

  test "new invalid task should have error messages" do
    assert_difference -> { @srs_workflow.direct_tasks.count }, 0 do
      post add_task_api_workflow_url(@srs_workflow, format: :json), params: {task: {name: nil, description: nil}}
    end
    assert_response :unprocessable_entity
    assert @response.parsed_body.key?("name")
  end

  test "#add_task should create  an new task at the end which should be started and should have a richtext and a file field" do
    assert_difference -> { @srs_workflow.direct_tasks.count }, 1 do
      post add_task_api_workflow_url(@srs_workflow, format: :json), params: {task: {name: "Ein neuer Task"}}
    end
    assert_response :success

    new_task = @srs_workflow.reload.direct_tasks.last

    assert_equal "Ein neuer Task", new_task.name
    assert_equal "active", new_task.aasm_state
    assert_equal @user, new_task.assignee
    assert_not_nil new_task.state_updated_at

    assert_equal 2, new_task.task_items.count

    content_item_description = new_task.task_items.first.content_item
    assert_equal ContentTypes::Richtext.to_s, content_item_description.content_type.to_s
    assert_equal I18n.t("system_process_definition_single_task.task.items.description"), content_item_description.label

    content_item_attachments = new_task.task_items.second.content_item
    assert_equal ContentTypes::File.to_s, content_item_attachments.content_type.to_s
    assert_equal I18n.t("system_process_definition_single_task.task.items.attachments"), content_item_attachments.label
  end

  test "adding a task to a single task process should convert single task to normal process" do
    single_task_process_definition = WorkflowDefinition.system_process_definition_single_task
    single_task_process = Interactors::WorkflowInteractor.create(
      single_task_process_definition, {title: "Testaufgabe"}, {}, nil, @user
    )
    assert single_task_process.system_process_single_task?

    assert_difference -> { single_task_process.direct_tasks.count }, 1 do
      post add_task_api_workflow_url(single_task_process, format: :json), params: {task: {name: "Ein neuer Task", description: "Beschreibung des neuen Tasks."}}
    end
    assert_response :success

    refute single_task_process.reload.system_process_single_task?
  end

  test "new block should be created at the end" do
    assert_difference -> { @srs_workflow.blocks.count }, 1 do
      post add_block_api_workflow_url(@srs_workflow, format: :json), params: {block: {title: "Ein neuer Block"}}
      assert_response :success
    end

    new_block = @srs_workflow.items.last

    assert_equal "Ein neuer Block", new_block.title
    assert_equal false, new_block.parallel
    assert_equal "created", new_block.aasm_state
  end

  test "new block should be created with params" do
    workflow = Workflow.find_by!(title: "Erstgespräch Erfindung für eine Test-Erfindung")
    assert workflow.content_items.where(content_type: ContentTypes::Boolean.to_s).any?
    content_item = workflow.content_items.where(content_type: ContentTypes::Boolean.to_s).first

    assert_difference -> { workflow.blocks.count }, 1 do
      post add_block_api_workflow_url(workflow, format: :json), params: {block: {title: "Ein ganz neuer Block", parallel: true, decision: "true", content_item_id: content_item.id}}
      assert_response :success
    end

    new_block = workflow.items.last

    assert_equal "Ein ganz neuer Block", new_block.title
    assert_equal true, new_block.parallel
    assert_equal true, new_block.decision
    assert_equal content_item.id, new_block.content_item_id
    assert_equal "created", new_block.aasm_state
  end
end
