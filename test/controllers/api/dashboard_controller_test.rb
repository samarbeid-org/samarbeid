require "test_helper"

class Api::DashboardControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "admin@example.org")
  end

  test "only authenticated users should have access" do
    get api_dashboard_processes_path(format: :json)
    assert_response :unauthorized

    get api_dashboard_dossier_definitions_path(format: :json)
    assert_response :unauthorized

    get api_dashboard_everything_in_view_path(format: :json)
    assert_response :unauthorized

    get api_dashboard_my_last_changes_path(format: :json)
    assert_response :unauthorized
  end

  test "should get processes" do
    login_as @user

    get api_dashboard_processes_path(format: :json), params: {limit: 99}
    assert_response :success

    assert_equal 7, @response.parsed_body["total_count"]
    assert_equal [16, 1, 4, 2, 3, 12, 13], @response.parsed_body["result"].pluck("id")

    # system_process_definition_single_task is always first even if it has not active tasks assigned to current user
    assert_equal WorkflowDefinition.system_process_definition_single_task.id, @response.parsed_body["result"][0]["id"]
    assert_equal 1, @response.parsed_body["result"][0]["assignees_with_count"].size

    assert_equal 1, @response.parsed_body["result"][1]["assignees_with_count"].size
    assert_equal @user.id, @response.parsed_body["result"][1]["assignees_with_count"].first["user"]["id"]
    assert_equal 1, @response.parsed_body["result"][1]["assignees_with_count"].first["count"]

    WorkflowDefinition.find(1).deactivate
    get api_dashboard_processes_path(format: :json), params: {limit: 99}
    assert_response :success

    assert_equal 6, @response.parsed_body["total_count"]
    assert_equal [16, 4, 2, 3, 12, 13], @response.parsed_body["result"].pluck("id")
  end

  test "should get processes with offset and limit" do
    login_as @user

    get api_dashboard_processes_path(format: :json), params: {offset: 2, limit: 3}
    assert_response :success

    assert_equal 7, @response.parsed_body["total_count"]
    assert_equal [4, 2, 3], @response.parsed_body["result"].pluck("id")
  end

  test "#processes should only return elements visible by groups for admins" do
    login_as @admin

    definitions = WorkflowDefinition.activated.includes(:groups).where(groups: {id: @admin.groups})
    assert_operator definitions.count, :<=, 99
    perform_enqueued_jobs do
      definitions.first.groups = []
    end

    get api_dashboard_processes_path(format: :json), params: {limit: 99}
    assert_response :success

    assert_equal definitions.count, @response.parsed_body["total_count"]
  end

  test "should get dossier_definitions" do
    login_as @user

    get api_dashboard_dossier_definitions_path(format: :json), params: {limit: 99}
    assert_response :success

    assert_equal 4, @response.parsed_body["total_count"]
    assert_equal [1, 2, 4, 3], @response.parsed_body["result"].pluck("id")
    assert_equal 57, @response.parsed_body["result"][0]["instance_count"]
  end

  test "#dossier_definitions should only return elements visible by groups for admins" do
    login_as @admin

    definitions = DossierDefinition.includes(:groups).where(groups: {id: @admin.groups})
    assert_operator definitions.count, :<=, 99
    perform_enqueued_jobs do
      definitions.first.groups = []
    end

    get api_dashboard_dossier_definitions_path(format: :json), params: {limit: 99}
    assert_response :success

    assert_equal definitions.count, @response.parsed_body["total_count"]
  end

  test "everything_in_view for normal user should work" do
    login_as @user
    Setting.main_menu_auto_add_deactivated = nil

    get api_dashboard_everything_in_view_path(format: :json)
    assert_response :success

    assert_equal 7, @response.parsed_body["unassigned_tasks_count_of_my_active_workflows"]
    assert_equal 2, @response.parsed_body["unassigned_active_processes_count"]
    # assert_equal 2, @response.parsed_body["unassigned_active_workflows_count"]

    refute @response.parsed_body.key?("hidden_process_definition_count")
    refute @response.parsed_body.key?("hidden_dossier_definition_count")

    refute @response.parsed_body.key?("unconfigured_main_menu")
  end

  test "#everything_in_view for admins should show hidden definitions count" do
    login_as @admin
    Setting.main_menu_auto_add_deactivated = nil

    get api_dashboard_everything_in_view_path(format: :json)
    assert_response :success

    assert_equal 1, @response.parsed_body["hidden_process_definition_count"]
    assert_equal 0, @response.parsed_body["hidden_dossier_definition_count"]

    perform_enqueued_jobs do
      WorkflowDefinition.where.not(id: WorkflowDefinition.hidden).limit(2).each { |wd| wd.groups = [] }
      DossierDefinition.where.not(id: DossierDefinition.hidden).limit(2).each { |dd| dd.groups = [] }
    end

    get api_dashboard_everything_in_view_path(format: :json)
    assert_response :success

    assert_equal WorkflowDefinition.hidden.count, @response.parsed_body["hidden_process_definition_count"]
    assert_equal DossierDefinition.hidden.count, @response.parsed_body["hidden_dossier_definition_count"]

    assert_equal true, @response.parsed_body["unconfigured_main_menu"]
  end

  test "should get my_last_changes" do
    login_as @user

    objects = []

    workflow = Workflow.first
    Interactors::WorkflowInteractor.update(workflow, {title: "Updated title"}, @user)
    objects.prepend(workflow)

    task = Task.first
    Interactors::TaskInteractor.update(task, {name: "Updated name"}, task.lock_version, @user)
    objects.prepend(task)

    dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    assert_equal 1, dossier_definition.items.required_or_recommended.count
    item = dossier_definition.items.required_or_recommended.first
    assert_equal ContentTypes::String, item.content_type
    dossier = Interactors::DossierInteractor.create(dossier_definition, {item.id => "Test data"}, @user)
    objects.prepend(dossier)

    dossier_definition = DossierDefinition.first
    Interactors::DossierDefinitionInteractor.update(dossier_definition, {name: "Updated name"}, @user)
    objects.prepend(dossier_definition)

    workflow_definition = WorkflowDefinition.first
    Interactors::WorkflowDefinitionInteractor.update(workflow_definition, {name: "Updated name"}, @user)
    objects.prepend(workflow_definition)

    get api_dashboard_my_last_changes_path(format: :json), params: {limit: objects.count}
    assert_response :success

    objects.each_with_index do |object, index|
      assert_equal object.id, @response.parsed_body[index]["object"]["id"]
      assert_equal object.object_type, @response.parsed_body[index]["object"]["type"]
    end
  end
end
