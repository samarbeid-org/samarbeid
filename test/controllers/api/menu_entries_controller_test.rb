require "test_helper"

class Api::MenuEntriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @menu_entry = MenuEntry.find_by!(title: "Impressum")

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "non admin users shouldn't access menu entries" do
    logout

    get api_menu_entries_path(format: :json)
    assert_response :unauthorized

    get api_menu_entry_path(@menu_entry, format: :json)
    assert_response :unauthorized

    assert_no_difference -> { MenuEntry.count } do
      post api_menu_entries_path, as: :json, params: {}
    end
    assert_response :unauthorized

    patch api_menu_entry_path(@menu_entry), as: :json, params: {}
    assert_response :unauthorized

    assert_no_difference -> { MenuEntry.count } do
      delete api_menu_entry_path(@menu_entry, format: :json)
    end
    assert_response :unauthorized

    get locations_api_menu_entries_path(format: :json)
    assert_response :unauthorized
  end

  test "should get index" do
    get api_menu_entries_path(format: :json)
    assert_response :success

    assert_equal MenuEntry.count, @response.parsed_body["result"].size
  end

  test "should get show" do
    get api_menu_entry_path(@menu_entry, format: :json)
    assert_response :success

    assert_equal @menu_entry.id, @response.parsed_body["id"]
    assert_equal @menu_entry.title, @response.parsed_body["title"]
    assert_equal @menu_entry.location, @response.parsed_body["location"]["value"]
    assert_equal MenuEntry.human_enum_name(:location, @menu_entry.location), @response.parsed_body["location"]["text"]
    assert_equal @menu_entry.position, @response.parsed_body["position"]
    assert_equal @menu_entry.page.id, @response.parsed_body["page"]["id"]
    assert_equal @menu_entry.new_window, @response.parsed_body["new_window"]
    assert_equal @menu_entry.updated_at.as_json, @response.parsed_body["updatedAt"]
  end

  test "should create" do
    assert_difference -> { MenuEntry.count } do
      post api_menu_entries_path, as: :json, params: {
        title: "A new menu entry",
        location: "footer_menu",
        position: 99,
        url: "http://example.org",
        new_window: true
      }
    end
    assert_response :success

    assert_equal "A new menu entry", @response.parsed_body["title"]
    assert_equal "footer_menu", @response.parsed_body["location"]["value"]
    assert_equal MenuEntry.human_enum_name(:location, "footer_menu"), @response.parsed_body["location"]["text"]
    assert_equal 99, @response.parsed_body["position"]
    assert_nil @response.parsed_body["page"]
    assert_equal "http://example.org", @response.parsed_body["url"]
    assert_equal true, @response.parsed_body["new_window"]
  end

  test "should update" do
    patch api_menu_entry_path(@menu_entry), as: :json, params: {
      title: "A new title",
      page_id: nil,
      url: "http://example.org"
    }
    assert_response :success

    assert_equal "A new title", @response.parsed_body["title"]
    assert_nil @response.parsed_body["page_id"]
    assert_equal "http://example.org", @response.parsed_body["url"]
  end

  test "should destroy" do
    assert_difference -> { MenuEntry.count }, -1 do
      delete api_menu_entry_path(@menu_entry, format: :json)
    end
    assert_response :success
  end

  test "should get locations" do
    get locations_api_menu_entries_path(format: :json)
    assert_response :success

    assert_equal MenuEntry.locations.count, @response.parsed_body.size
  end
end
