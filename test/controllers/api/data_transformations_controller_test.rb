require "test_helper"

class Api::DataTransformationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_item_definition = @dossier_definition.items.find_by!(name: "String")
    @data_transformation = @dossier_item_definition.data_transformations.first
    refute_nil @data_transformation

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "#index should work" do
    get api_data_transformations_path, as: :json, params: {
      object_gid: @dossier_definition.to_global_id.to_s
    }
    assert_response :success

    assert_equal 1, @response.parsed_body.size
  end

  test "#show should work and create missing data transformation items on the fly" do
    @data_transformation.data_transformation_items.each { |dti| dti.destroy! }
    @data_transformation.data_transformation_items.reload
    refute @data_transformation.data_transformation_items.any?

    get api_data_transformation_path(@data_transformation), as: :json, params: {type: "INVALID"}
    assert_response :success

    assert_equal @data_transformation.dossier_item_definition.dossier_items.count, @data_transformation.data_transformation_items.count

    assert_equal @dossier_item_definition.id, @response.parsed_body["field"]["id"]
    assert_equal ContentTypes::String.to_s, @response.parsed_body["source_definition"]["type"]["name"]
    assert_equal ContentTypes::PhoneNumber.to_s, @response.parsed_body["target_definition"]["type"]["name"]
    assert_equal({"INVALID" => 1, "UNCONFIRMED" => 0, "READY" => 0}, @response.parsed_body["type_counts"])
    assert_kind_of Array, @response.parsed_body["items"]
    assert_equal 1, @response.parsed_body["items"].size
  end

  test "#destroy should work" do
    delete api_data_transformation_path(@data_transformation), as: :json
    assert_response :success
    refute @dossier_item_definition.data_transformations.any?
  end

  test "#create should work for content_type" do
    @data_transformation.destroy!
    refute @dossier_item_definition.data_transformations.any?

    post api_data_transformations_path, as: :json, params: {
      dossier_item_definition_id: @dossier_item_definition.id,
      content_type: ContentTypes::PhoneNumber.to_s
    }
    assert_response :success
    assert_equal 1, @dossier_item_definition.data_transformations.size
  end

  test "#create should merge options to only update changed attributes" do
    dossier_item_definition_dossiers = @dossier_definition.items.find_by!(name: "Dossiers")
    refute dossier_item_definition_dossiers.data_transformations.any?

    initial_options = {type: 1, multiple: true, inverseReference: {title: "Test Dossiers", showEmpty: true}}
    assert_equal initial_options.with_indifferent_access, dossier_item_definition_dossiers.options.with_indifferent_access

    post api_data_transformations_path, as: :json, params: {
      dossier_item_definition_id: dossier_item_definition_dossiers.id,
      options: {multiple: true}
    }
    assert_response :success

    assert_equal 1, dossier_item_definition_dossiers.data_transformations.size
    data_transformation = dossier_item_definition_dossiers.data_transformations.first
    assert_equal({type: 1, multiple: true, inverseReference: {title: "Test Dossiers", showEmpty: true}}.with_indifferent_access, data_transformation.options.with_indifferent_access)
  end

  test "#update should work (validation errors, successfully update value and confirmed)" do
    assert_equal 1, @data_transformation.data_transformation_items.size
    assert_equal 1, @data_transformation.data_transformation_items.invalid.unconfirmed.size
    data_transformation_item = @data_transformation.data_transformation_items.first

    patch api_data_transformation_path(@data_transformation), as: :json, params: {
      item_id: data_transformation_item.id,
      value: "Test"
    }
    assert_response :unprocessable_entity

    patch api_data_transformation_path(@data_transformation), as: :json, params: {
      item_id: data_transformation_item.id,
      value: "+49 341 1234567890"
    }
    assert_response :success
    assert_equal 1, @data_transformation.data_transformation_items.reload.valid.unconfirmed.size

    patch api_data_transformation_path(@data_transformation), as: :json, params: {
      item_id: data_transformation_item.id,
      confirmed: true
    }
    assert_response :success
    assert_equal 1, @data_transformation.data_transformation_items.reload.valid.confirmed.size
  end

  test "#confirm_all should work" do
    assert_equal 1, @data_transformation.data_transformation_items.size
    assert_equal 1, @data_transformation.data_transformation_items.invalid.unconfirmed.size

    assert_no_difference -> { @data_transformation.data_transformation_items.invalid.unconfirmed.size } do
      patch confirm_all_api_data_transformation_path(@data_transformation), as: :json
      assert_response :success
    end

    @data_transformation.data_transformation_items.first.update!(value: "+49 341 1234567890")
    assert_equal 1, @data_transformation.data_transformation_items.valid.unconfirmed.size

    patch confirm_all_api_data_transformation_path(@data_transformation), as: :json
    assert_response :success
    assert_equal 1, @data_transformation.data_transformation_items.valid.confirmed.size
  end

  test "#apply should work" do
    assert_equal 1, @data_transformation.data_transformation_items.size
    assert_equal 1, @data_transformation.data_transformation_items.invalid.unconfirmed.size

    patch apply_api_data_transformation_path(@data_transformation)
    assert_response :unprocessable_entity

    @data_transformation.data_transformation_items.first.update!(value: "+49 341 1234567890", confirmed: true)

    patch apply_api_data_transformation_path(@data_transformation)
    assert_response :success

    @dossier_item_definition.reload
    assert_equal @data_transformation.content_type, @dossier_item_definition.content_type
    assert_equal @data_transformation.options, @dossier_item_definition.options
    assert_equal @data_transformation.required, @dossier_item_definition.required
    assert_equal @data_transformation.unique, @dossier_item_definition.unique

    refute @dossier_item_definition.data_transformations.any?
  end
end
