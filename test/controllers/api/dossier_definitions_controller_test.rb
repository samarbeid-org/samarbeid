require "test_helper"

class Api::DossierDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "admin@example.org")

    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
  end

  test "#index should return all visible definitions for non-admin user" do
    login_as @user

    visible_definitions_for_user = DossierDefinition.includes(:groups).where(groups: {id: @user.groups})
    visible_workflow_definition_count = visible_definitions_for_user.count
    assert_operator DossierDefinition.count, :>=, visible_workflow_definition_count
    assert_operator visible_workflow_definition_count, :<=, 99

    get api_dossier_definitions_path(format: :json, params: {per_page: 99})
    assert_response :success

    assert_equal 1, @response.parsed_body["total_pages"]
    assert_equal visible_workflow_definition_count, @response.parsed_body["result"].count
  end

  test "#index should return all definitions for admin user" do
    login_as(@admin)

    visible_workflow_definition_count = DossierDefinition.count
    assert_operator visible_workflow_definition_count, :<=, 99

    get api_dossier_definitions_path(format: :json, params: {per_page: 99})
    assert_response :success

    assert_equal 1, @response.parsed_body["total_pages"]
    assert_equal visible_workflow_definition_count, @response.parsed_body["result"].count
  end

  test "#index should order prepend definitions without any groups and return correct count" do
    login_as(@admin)

    dd = DossierDefinition.order(name: :asc).last

    perform_enqueued_jobs do
      dd.groups = []
    end

    get api_dossier_definitions_path(format: :json, params: {per_page: 99, order: "name_asc"})
    assert_response :success

    assert_equal dd.id, @response.parsed_body["result"].first["id"]
    assert_equal 1, @response.parsed_body["hiddenElementsCount"]
  end

  test "should get list" do
    login_as @user

    get list_api_dossier_definitions_path(format: :json)
    assert_response :success
  end

  test "#list should support paging of results" do
    login_as @admin

    (1..10).each { |i| DossierDefinition.create!(name: "Test-Dossier-Definition #{i}") }

    result_count = DossierDefinition.count
    assert_operator result_count, :>, 10
    assert_operator result_count, :<, 20

    get list_api_dossier_definitions_path(format: :json, params: {page: 1, per_page: 10})
    assert_response :success
    assert_equal 10, @response.parsed_body.length

    get list_api_dossier_definitions_path(format: :json, params: {page: 2, per_page: 10})
    assert_response :success
    assert_equal result_count - 10, @response.parsed_body.length
  end

  test "#list should also return definitions without groups for admins depending on filter_by_groups_only parameter" do
    login_as(@admin)

    definitions = DossierDefinition.all.order(name: :asc)
    definition = definitions.first

    perform_enqueued_jobs do
      definition.groups = []
    end

    get list_api_dossier_definitions_path(format: :json, params: {page: 1, per_page: 99})
    assert_response :success
    assert_equal definitions.count, @response.parsed_body.count
    assert_equal definition.id, @response.parsed_body.first["id"]

    visible_definitions_for_user = DossierDefinition.includes(:groups).where(groups: {id: @admin.groups})
    get list_api_dossier_definitions_path(format: :json, params: {page: 1, per_page: 99, filter_by_groups_only: true})
    assert_response :success
    assert_equal visible_definitions_for_user.count, @response.parsed_body.count
  end

  test "should get show" do
    login_as @user

    get api_dossier_definition_path(@dossier_definition, format: :json)
    assert_response :success
  end

  test "should create new dossier definition" do
    login_as @user

    assert_difference -> { DossierDefinition.count } do
      post api_dossier_definitions_path, as: :json, params: {
        name: "A new name",
        description: "A new description",
        group_ids: Group.all.map(&:id)
      }
    end
    assert_response :success
  end

  test "should update" do
    login_as @user

    patch api_dossier_definition_path(@dossier_definition), as: :json, params: {
      name: "A new name",
      description: "Something different",
      group_ids: Group.pluck(:id),
      title_fields: @dossier_definition.title_fields |= [@dossier_definition.items.last.id],
      subtitle_fields: @dossier_definition.subtitle_fields - [@dossier_definition.subtitle_fields.last]
    }
    assert_response :success
  end

  test "should destroy dossier definition without instances" do
    login_as @user

    dossier_definition = DossierDefinition.create!(name: "Empty", groups: @user.groups)
    assert_difference -> { DossierDefinition.count }, -1 do
      delete api_dossier_definition_path(dossier_definition, format: :json)
    end
    assert_response :success
  end

  test "shouldn't destroy dossier definition with any instances" do
    login_as @user

    assert_not_empty @dossier_definition.dossiers

    assert_no_difference -> { DossierDefinition.count } do
      delete api_dossier_definition_path(@dossier_definition, format: :json)
    end
    assert_response :unprocessable_entity
  end
end
