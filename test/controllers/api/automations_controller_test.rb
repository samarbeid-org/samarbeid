require "test_helper"

class Api::AutomationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "dsb@example.org")
    login_as @user

    @automation = Automation.find_by!(title: "event-test-automation")
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")

    @daily_schedule = {rule: "daily", start: "2024-03-04", interval: 1, validations: {}}
    @weekly_schedule = {rule: "weekly", start: "2024-03-04", interval: 1, validations: {day: [1, 3]}}
  end

  test "should get index" do
    get api_automations_path(format: :json)
    assert_response :success

    assert_equal 1, @response.parsed_body["result"].count
  end

  test "should get show" do
    get api_automation_path(@automation, format: :json)
    assert_response :success
  end

  test "should create" do
    assert_difference -> { Automation.count } do
      post api_automations_path, as: :json, params: {
        title: "Daily Automation",
        workflow_definition_id: @workflow_definition.id,
        candidate_assignee_id: @user.id,
        schedule: @daily_schedule
      }
    end
    assert_response :success
  end

  test "should update" do
    patch api_automation_path(@automation), as: :json, params: {
      title: "Weekly Automation",
      workflow_definition_id: @workflow_definition.id,
      candidate_assignee_id: @user.id,
      schedule: @weekly_schedule,
      active: false
    }
    assert_response :success

    assert_equal "Weekly Automation", @response.parsed_body["title"]
    assert_equal @workflow_definition.id, @response.parsed_body["workflowDefinition"]["id"]
    assert_equal @user.id, @response.parsed_body["candidateAssignee"]["id"]
    assert_equal @weekly_schedule.with_indifferent_access, @response.parsed_body["schedule"].slice("start", "interval", "rule", "validations")
    refute @response.parsed_body["active"]
  end

  test "should destroy" do
    assert_difference -> { Automation.count } => -1 do
      delete api_automation_path(@automation, format: :json)
    end
    assert_response :success
  end

  test "should validate_schedule" do
    post validate_schedule_api_automations_path, as: :json, params: {
      schedule: @daily_schedule
    }
    assert_response :success
  end

  test "access should be restricted by workflow_definition.groups" do
    logout
    login_as User.find_by!(email: "srs_user@example.org")

    get api_automations_path(format: :json)
    assert_response :success
    assert_equal 0, @response.parsed_body["result"].count

    get api_automation_path(@automation, format: :json)
    assert_response :forbidden

    post api_automations_path, as: :json, params: {
      title: "Daily Automation",
      workflow_definition_id: @workflow_definition.id,
      candidate_assignee_id: @user.id,
      schedule: @daily_schedule
    }
    assert_response :forbidden

    patch api_automation_path(@automation), as: :json, params: {
      title: "Weekly Automation",
      workflow_definition_id: @workflow_definition.id,
      candidate_assignee_id: @user.id,
      schedule: @weekly_schedule,
      active: false
    }
    assert_response :forbidden

    delete api_automation_path(@automation, format: :json)
    assert_response :forbidden

    post validate_schedule_api_automations_path, as: :json, params: {
      schedule: @daily_schedule
    }
    assert_response :success
  end
end
