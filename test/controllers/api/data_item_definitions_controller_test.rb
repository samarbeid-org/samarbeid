require "test_helper"

class Api::DataItemDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "dsb@example.org")
    @workflow_definitions = WorkflowDefinition.includes(:groups).where(groups: {id: @user.groups})
    @workflow_definition = @workflow_definitions.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @task_definition = @workflow_definition.direct_task_definitions.find_by!(name: "Entscheidung Löschung")
    @task_item_definition = @task_definition.task_item_definitions.first
    login_as(@user)
  end

  test "should create data item definition and add it to the task_definitions items" do
    assert_difference -> { @task_definition.task_item_definitions.count } => 1,
      -> { @task_definition.workflow_definition.content_item_definitions.count } => 1 do
      post api_data_item_definitions_path(format: :json), params: {
        task_definition_id: @task_definition.id,
        content_item_definition: {label: "Testdatafield", content_type: "text"},
        task_item_definition: {required: true}
      }
    end
    assert_response :success

    task_item_definition = @task_definition.task_item_definitions.last
    content_item_definition = task_item_definition.content_item_definition
    assert_equal "Testdatafield", content_item_definition.label
    assert_equal ContentTypes::Text, content_item_definition.content_type
    assert_equal true, task_item_definition.required
  end

  test "should update data item definition with content_type 'text'" do
    content_item_definition = @workflow_definition.content_item_definitions.find_by!(content_type: ContentTypes::Text.to_s)
    refute_nil content_item_definition
    task_item_definition = content_item_definition.task_item_definitions.first
    refute_nil task_item_definition

    new_label_value = "updated label value"
    refute_equal new_label_value, content_item_definition.label
    refute_equal true, task_item_definition.info_box
    refute_equal true, task_item_definition.required

    patch api_data_item_definition_path(task_item_definition, format: :json), params: {
      content_item_definition: {label: new_label_value},
      task_item_definition: {info_box: true, required: true}
    }
    assert_response :success

    content_item_definition.reload
    task_item_definition.reload
    assert_equal new_label_value, content_item_definition.label
    assert_equal true, task_item_definition.info_box
    assert_equal true, task_item_definition.required
  end

  test "should update data item definition with content_type 'note'" do
    content_item_definition = @workflow_definition.content_item_definitions.find_by!(content_type: ContentTypes::Note.to_s)
    refute_nil content_item_definition
    task_item_definition = content_item_definition.task_item_definitions.first
    refute_nil task_item_definition

    new_note_value = "updated note text"
    refute_equal new_note_value, content_item_definition.options["text"]

    patch api_data_item_definition_path(task_item_definition, format: :json), params: {
      content_item_definition: {options: {text: new_note_value}}
    }
    assert_response :success

    content_item_definition.reload
    assert_equal new_note_value, content_item_definition.options["text"]
  end

  test "should destroy task_item_definition which is not alone in its content_item_definition" do
    content_item_definition = @workflow_definition.content_item_definitions.find { |cid| cid.task_item_definitions.count > 1 }
    refute_nil content_item_definition
    task_item_definition = content_item_definition.task_item_definitions.first
    refute_nil task_item_definition

    assert_difference -> { content_item_definition.task_item_definitions.count }, -1 do
      delete api_data_item_definition_path(task_item_definition)
    end
    assert_response :success

    assert_raises ActiveRecord::RecordNotFound do
      task_item_definition.reload
    end

    assert_nothing_raised do
      content_item_definition.reload
    end
  end

  test "should destroy task_item_definition which is alone in its content_item_definition" do
    content_item_definition = @workflow_definition.content_item_definitions.find { |cid| cid.task_item_definitions.count == 1 }
    refute_nil content_item_definition
    task_item_definition = content_item_definition.task_item_definitions.first
    refute_nil task_item_definition

    assert_difference -> { content_item_definition.task_item_definitions.count }, -1 do
      delete api_data_item_definition_path(task_item_definition)
    end
    assert_response :success

    assert_raises ActiveRecord::RecordNotFound do
      task_item_definition.reload
    end

    assert_raises ActiveRecord::RecordNotFound do
      content_item_definition.reload
    end
  end

  test "shouldn't destroy task_item_definition which is the precondition for a block" do
    content_item_definition = @workflow_definition.block_definitions.first.content_item_definition
    refute_nil content_item_definition
    assert_equal 1, content_item_definition.task_item_definitions.count
    task_item_definition = content_item_definition.task_item_definitions.first
    refute_nil task_item_definition

    assert_no_difference -> { content_item_definition.task_item_definitions.count } do
      delete api_data_item_definition_path(task_item_definition)
    end
    assert_response :unprocessable_entity
    assert_equal ["Dieses Feld wird in einem Block als Bedingung verwendet."], @response.parsed_body["base"]["full"]

    assert_nothing_raised do
      task_item_definition.reload
    end

    assert_nothing_raised do
      content_item_definition.reload
    end
  end

  test "task item should be movable down (index_new > index_old)" do
    task_definition = @workflow_definition.all_task_definitions.find { |td| td.task_item_definitions.count > 1 }
    refute_nil task_definition
    @task_item_definition_1 = task_definition.task_item_definitions.first
    refute_nil @task_item_definition_1
    @task_item_definition_2 = task_definition.task_item_definitions.second
    refute_nil @task_item_definition_2

    movee = @task_item_definition_1
    other = @task_item_definition_2

    assert_operator movee.position, :<, other.position
    assert_equal 0, task_definition.task_item_definitions.index(movee)
    assert_equal 1, task_definition.task_item_definitions.index(other)

    patch move_api_data_item_definition_path(movee, format: :json), params: {index: 1}
    assert_response :success

    task_definition.reload
    movee.reload
    other.reload
    assert_equal 0, task_definition.task_item_definitions.index(other)
    assert_equal 1, task_definition.task_item_definitions.index(movee)

    assert_operator movee.position, :>, other.position
  end

  test "task item should be movable up (index_new < index_old)" do
    task_definition = @workflow_definition.all_task_definitions.find { |td| td.task_item_definitions.count > 1 }
    refute_nil task_definition
    @task_item_definition_1 = task_definition.task_item_definitions.first
    refute_nil @task_item_definition_1
    @task_item_definition_2 = task_definition.task_item_definitions.second
    refute_nil @task_item_definition_2

    movee = @task_item_definition_2
    other = @task_item_definition_1

    assert_operator movee.position, :>, other.position
    assert_equal 1, task_definition.task_item_definitions.index(movee)
    assert_equal 0, task_definition.task_item_definitions.index(other)

    patch move_api_data_item_definition_path(movee, format: :json), params: {index: 0}
    assert_response :success

    task_definition.reload
    movee.reload
    other.reload
    assert_equal 1, task_definition.task_item_definitions.index(other)
    assert_equal 0, task_definition.task_item_definitions.index(movee)

    assert_operator movee.position, :<, other.position
  end

  test "should create, update, destroy or move data item definition of system_process_definition_single_task" do
    single_task_definition = WorkflowDefinition.system_process_definition_single_task.all_task_definitions.first
    task_item_definition = single_task_definition.task_item_definitions.first

    post api_data_item_definitions_path(format: :json), params: {
      task_definition_id: single_task_definition.id,
      content_item_definition: {label: "Testdatafield", content_type: "text"},
      task_item_definition: {required: true}
    }
    assert_response :unprocessable_entity

    patch api_data_item_definition_path(task_item_definition, format: :json), params: {
      content_item_definition: {label: "Updated label"},
      task_item_definition: {info_box: true, required: true}
    }
    assert_response :unprocessable_entity

    delete api_data_item_definition_path(task_item_definition)
    assert_response :unprocessable_entity

    patch move_api_data_item_definition_path(task_item_definition, format: :json), params: {index: 1}
    assert_response :unprocessable_entity
  end

  test "add_to_calendar should not be updatable on non date/datetime datafields" do
    patch api_data_item_definition_path(@task_item_definition, format: :json), params: {task_item_definition: {add_to_calendar: true}}
    assert_response :unprocessable_entity

    assert @response.parsed_body.to_s.match("Add to calendar Für diesen Datentyp nicht möglich!")
  end

  test "add_to_calendar should be updatable" do
    user = User.find_by(email: "admin@example.org")
    login_as(user)

    ci_def = ContentItemDefinition.where(content_type: ContentTypes::Date.type).first
    calendar_task_item_def = ci_def.task_item_definitions.first

    refute calendar_task_item_def.add_to_calendar

    patch api_data_item_definition_path(calendar_task_item_def, format: :json), params: {task_item_definition: {add_to_calendar: true}}
    assert_response :success
    calendar_task_item_def.reload
    assert calendar_task_item_def.add_to_calendar

    patch api_data_item_definition_path(calendar_task_item_def, format: :json), params: {task_item_definition: {add_to_calendar: false}}
    assert_response :success
    calendar_task_item_def.reload
    refute calendar_task_item_def.add_to_calendar
  end
end
