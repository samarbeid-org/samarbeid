require "test_helper"

class Api::MainMenuEntriesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @main_menu_entry = MainMenuEntry.find_by!(title: "DSB")

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "non admin users shouldn't access menu entries" do
    logout
    login_as User.find_by!(email: "srs_user@example.org")

    get api_main_menu_entries_path(format: :json)
    assert_response :forbidden

    assert_no_difference -> { MainMenuEntry.count } do
      post api_main_menu_entries_path, as: :json, params: {
        parent_id: nil,
        title: "Test"
      }
    end
    assert_response :forbidden

    patch api_main_menu_entry_path(@main_menu_entry), as: :json, params: {
      title: "Test"
    }
    assert_response :forbidden

    assert_no_difference -> { MainMenuEntry.count } do
      delete api_main_menu_entry_path(@main_menu_entry, format: :json)
    end
    assert_response :forbidden

    patch move_api_main_menu_entry_path(@main_menu_entry), as: :json, params: {
      index: MainMenuEntry.where(parent_id: nil).count - 1
    }
    assert_response :forbidden
  end

  test "all users should access index_filtered action" do
    logout
    login_as User.find_by!(email: "srs_user@example.org")

    get index_filtered_api_main_menu_entries_path(format: :json)
    assert_response :success

    assert @response.parsed_body["result"].is_a?(Array)
    assert_operator @response.parsed_body["result"].size, :>, 0

    refute @response.parsed_body["result"].find { |item| item["type"] == "submenu" && item["title"] == "DSB" }
    assert @response.parsed_body["result"].find { |item| item["type"] == "submenu" && item["title"] == "SRS" && item["childEntries"].size == 5 }
    assert @response.parsed_body["result"].find { |item| item["type"] == "dossier_definition" && item["linkedId"] == 1 }
  end

  test "admin users should get index_filtered for their groups (like normal users)" do
    get index_filtered_api_main_menu_entries_path(format: :json)
    assert_response :success

    assert @response.parsed_body["result"].is_a?(Array)
    assert_equal 2, @response.parsed_body["result"].size
    assert @response.parsed_body["result"].find { |item| item["type"] == "submenu" && item["title"] == "SRS" && item["childEntries"].size == 1 }
    assert @response.parsed_body["result"].find { |item| item["type"] == "dossier_definition" && item["linkedId"] == 1 }
  end

  test "should get index (unfiltered)" do
    get api_main_menu_entries_path(format: :json)
    assert_response :success

    assert @response.parsed_body["result"].is_a?(Array)
    assert_operator @response.parsed_body["result"].size, :>, 0
  end

  test "should create" do
    assert_difference -> { MainMenuEntry.count } do
      post api_main_menu_entries_path, as: :json, params: {
        parent_id: nil,
        title: "Test"
      }
    end
    assert_response :success

    assert @response.parsed_body["result"].is_a?(Array)
    assert_operator @response.parsed_body["result"].size, :>, 0
  end

  test "should update" do
    assert_changes -> { @main_menu_entry.reload.title }, from: "DSB", to: "Test" do
      patch api_main_menu_entry_path(@main_menu_entry), as: :json, params: {
        title: "Test"
      }
    end
    assert_response :success

    assert @response.parsed_body["result"].is_a?(Array)
    assert_operator @response.parsed_body["result"].size, :>, 0
  end

  test "should destroy" do
    assert_difference -> { MainMenuEntry.count }, -3 do
      delete api_main_menu_entry_path(@main_menu_entry, format: :json)
    end
    assert_response :success

    assert @response.parsed_body["result"].is_a?(Array)
    assert_operator @response.parsed_body["result"].size, :>, 0
  end

  test "should move" do
    assert_changes -> { @main_menu_entry.reload.position }, from: 1, to: 4 do
      patch move_api_main_menu_entry_path(@main_menu_entry), as: :json, params: {
        index: MainMenuEntry.where(parent_id: nil).count - 1
      }
    end
    assert_response :success
  end
end
