require "test_helper"

class Api::NotificationsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    login_as @user
  end

  test "should get index for new notifications" do
    get api_notifications_path(filter_category: "NEW", format: :json)
    assert_response :success
  end

  test "should get index for bookmarked notifications" do
    get api_notifications_path(filter_category: "BOOKMARKED", format: :json)
    assert_response :success
  end

  test "should get index for done notifications" do
    get api_notifications_path(filter_category: "DONE", format: :json)
    assert_response :success
  end

  test "should patch unread notification as done" do
    notification = @user.notifications.unread.first

    patch done_api_notification_path(notification, format: :json)
    assert_response :success
  end

  test "should patch a single unread notification as read" do
    assert_operator @user.notifications.unread.count, :>, 0

    notification_id = @user.notifications.unread.first.id
    assert_difference -> { @user.notifications.unread.count }, -1 do
      patch read_api_notifications_path(format: :json), params: {notification_ids: Array(notification_id)}
    end
    assert_response :success

    assert Notification.find(notification_id).read?
  end

  test "should patch many unread notifications as read" do
    assert_operator @user.notifications.unread.count, :>, 2

    notification_ids = @user.notifications.unread.limit(2).map(&:id)
    assert_difference -> { @user.notifications.unread.count }, -2 do
      patch read_api_notifications_path(format: :json), params: {notification_ids: notification_ids}
    end
    assert_response :success

    refute @user.notifications.unread.where(id: notification_ids).any?
  end

  test "should patch all unread notifications as read" do
    assert_operator @user.notifications.unread.count, :>, 0

    patch read_all_api_notifications_path(format: :json)
    assert_response :success

    refute @user.notifications.unread.any?
  end

  test "should patch a single undelivered notification as delivered" do
    assert_operator @user.notifications.undelivered.count, :>, 0

    notification_id = @user.notifications.undelivered.first.id
    assert_difference -> { @user.notifications.undelivered.count }, -1 do
      patch delivered_api_notifications_path(format: :json), params: {notification_ids: Array(notification_id)}
    end
    assert_response :success

    assert Notification.find(notification_id).delivered?
  end

  test "should patch many undelivered notifications as delivered" do
    assert_operator @user.notifications.undelivered.count, :>, 2

    notification_ids = @user.notifications.undelivered.limit(2).map(&:id)
    assert_difference -> { @user.notifications.undelivered.count }, -2 do
      patch delivered_api_notifications_path(format: :json), params: {notification_ids: notification_ids}
    end
    assert_response :success

    refute @user.notifications.undelivered.where(id: notification_ids).any?
  end
end
