require "test_helper"

class Api::EventsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as @current_user
  end

  test "should get events for workflow" do
    object = Workflow.find_by!(title: "MM-20")

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :success
  end

  test "should get events for task" do
    object = Workflow.find_by!(title: "MM-20").direct_tasks.first

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :success
  end

  test "shouldn't get events for an object which the user doesn't have access to" do
    object = Workflow.find_by!(title: "Prozess der überall vorkommt")

    get api_events_path(format: :json), params: {
      object_gid: object.to_global_id.to_s
    }
    assert_response :forbidden
  end

  test "should add comments to workflow" do
    object = Workflow.find_by!(title: "MM-20")

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to task" do
    object = Workflow.find_by!(title: "MM-20").direct_tasks.first

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to workflow_definition" do
    logout
    login_as User.find_by!(email: "team-admin@example.org")

    object = WorkflowDefinition.find_by!(name: "Veranstaltungsplanung")

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "should add comments to dossier" do
    object = Dossier.first

    assert_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :success
  end

  test "shouldn't be possible to add comments to an object which the user doesn't have access to" do
    object = Workflow.find_by!(title: "Prozess der überall vorkommt")

    assert_no_difference -> { object.comments.count } do
      post create_comment_api_events_path(format: :json), params: {
        object_gid: object.to_global_id.to_s,
        message: "Ein neuer Kommentar"
      }
    end
    assert_response :forbidden
  end

  test "should update comment created by user" do
    logout
    @current_user = User.find_by!(email: "srs_user@example.org")
    login_as(@current_user)

    event = Events::CommentedEvent.find_by!(subject: @current_user)

    assert_changes -> { event.comment.message } do
      assert_changes -> { event.comment.updated_at } do
        patch update_comment_api_event_path(event, format: :json), params: {
          message: "<p>Ein aktualisierter Kommentar für <mention m-id=\"1\" m-type=\"user\">@Admin User</mention> </p>"
        }
        event.comment.reload
      end
    end
    assert_response :success
  end

  test "shouldn't be possible to update comment of other user" do
    other_user = User.find_by!(email: "srs_user@example.org")
    event = Events::CommentedEvent.find_by!(subject: other_user)

    patch update_comment_api_event_path(event, format: :json), params: {
      message: "<p>Ein aktualisierter Kommentar für <mention m-id=\"1\" m-type=\"user\">@Admin User</mention> </p>"
    }
    assert_response :forbidden
  end

  test "should delete comment created by user" do
    logout
    @current_user = User.find_by!(email: "srs_user@example.org")
    login_as(@current_user)

    event = Events::CommentedEvent.find_by!(subject: @current_user)

    assert_difference -> { Comment.not_deleted.where(object: event.object).count }, -1 do
      delete delete_comment_api_event_path(event, format: :json)
    end
    assert_response :success
  end

  test "shouldn't be possible to delete comment of other user" do
    other_user = User.find_by!(email: "srs_user@example.org")
    event = Events::CommentedEvent.find_by!(subject: other_user)

    delete delete_comment_api_event_path(event, format: :json)
    assert_response :forbidden
  end

  test "should get obsolete-info for workflow" do
    event = Workflow.find_by!(title: "MM-20").events.first
    assert_equal "Events::AssignedEvent", event.type
    get obsolete_info_api_event_path(event)
    assert_response :success
  end

  test "should get updated obsolete-info for workflow" do
    skip("Temporarily disabled for #1277")
    workflow = Workflow.find_by!(title: "MM-20")
    event = workflow.events.first
    assert_equal "Events::AssignedEvent", event.type
    assert event.superseder_id.nil?

    get obsolete_info_api_event_path(event)
    assert_response :success
    assert_equal false, @response.parsed_body["obsolete"]

    patch update_assignee_api_workflow_path(workflow, format: :json), params: {assignee: nil}
    assert_response :success

    get obsolete_info_api_event_path(event)
    assert_response :success
    assert_equal true, @response.parsed_body["obsolete"]
    assert_equal workflow.events.where(type: "Events::UnassignedEvent").last.id, @response.parsed_body["last_superseder"]["id"]
  end

  test "shouldn't get obsolete-info for workflow when object is not readable" do
    event = Workflow.find_by!(title: "Prozess der überall vorkommt").events.first

    get obsolete_info_api_event_path(event)
    assert_response :forbidden
  end

  test "#time_tracking should return all comment events with time tracking data and have a full user object for the subject of the event" do
    task_with_time_tracking = Task.find(123)

    get time_tracking_api_events_path(format: :json), params: {
      object_gid: task_with_time_tracking.to_global_id.to_s
    }
    assert_response :success

    assert_equal task_with_time_tracking.time_tracking_spent_in_minutes, @response.parsed_body.map { |e| e.dig("payload", "time_tracking", "timeSpentInMinutes") }.reduce(:+)
    assert @response.parsed_body.reduce(true) { |res, e| res && e.key?("subject") && e["subject"].key?("id") && e["subject"].key?("fullname") }
    assert_equal response.parsed_body.sort_by { |e| ContentTypes::Date.cast(e.dig("payload", "time_tracking", "spentAt")) }, response.parsed_body, "time_tracking response should be sorted by spentAt date of time_tracking data"
  end
end
