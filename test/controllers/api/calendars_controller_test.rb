require "test_helper"

class Api::CalendarsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
  end

  test "should get calendar with correct token" do
    @current_user.create_token(:calendar_token)
    get api_calendar_url(token: @current_user.calendar_token)
    assert_response :success
  end

  test "should NOT get calendar with nil token" do
    assert_raises {
      get api_calendar_url(token: "")
    }
  end

  test "should reject if user is unconfirmed" do
    @current_user.create_token(:calendar_token)
    @current_user.update!(confirmed_at: nil)
    refute @current_user.confirmed?

    get api_calendar_url(token: @current_user.calendar_token)

    assert_response :forbidden
  end

  test "should reject if user is deactivated" do
    @current_user.create_token(:calendar_token)
    @current_user.deactivate
    assert @current_user.deactivated?

    get api_calendar_url(token: @current_user.calendar_token)

    assert_response :forbidden
  end

  test "should get create" do
    login_as(@current_user)
    refute @current_user.calendar_token

    post api_calendar_url

    assert_response :success
    assert @current_user.calendar_token
  end

  test "should get destroy" do
    @current_user.create_token(:calendar_token)
    login_as(@current_user)

    delete api_calendar_url

    assert_response :success
    refute @current_user.calendar_token
  end

  test "if token is invalid should reject get show" do
    get api_calendar_url(token: "blab lubb")
    assert_response :forbidden
  end

  test "if not logged in should reject get create" do
    post api_calendar_url
    assert_response :unauthorized
  end

  test "if not logged in should reject get destroy" do
    delete api_calendar_url
    assert_response :unauthorized
  end
end
