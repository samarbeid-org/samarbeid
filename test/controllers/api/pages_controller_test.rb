require "test_helper"

class Api::PagesControllerTest < ActionDispatch::IntegrationTest
  def setup
    @page = Page.find_by!(slug: "impressum")

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "non admin users shouldn't access menu entries" do
    logout

    get api_pages_path(format: :json)
    assert_response :unauthorized

    get list_api_pages_path(format: :json)
    assert_response :unauthorized

    assert_no_difference -> { Page.count } do
      post api_pages_path, as: :json, params: {}
    end
    assert_response :unauthorized

    patch api_page_path(@page), as: :json, params: {}
    assert_response :unauthorized

    assert_no_difference -> { Page.count }, -> { MenuEntry.count } do
      delete api_page_path(@page, format: :json)
    end
    assert_response :unauthorized
  end

  test "should get index" do
    get api_pages_path(format: :json)
    assert_response :success

    assert_equal Page.count, @response.parsed_body["result"].size
  end

  test "should get list" do
    get list_api_pages_path(format: :json)
    assert_response :success

    assert_equal Page.count, @response.parsed_body.size
  end

  test "#list should support paging of results" do
    (1..10).each { |i| Page.create!(title: "Testpage #{i}", slug: "Testpage #{i}") }

    result_count = Page.count
    assert_operator result_count, :>, 10
    assert_operator result_count, :<, 20

    get list_api_pages_path, as: :json, params: {page: 1, per_page: 10}
    assert_response :success
    assert_equal 10, @response.parsed_body.length

    get list_api_pages_path, as: :json, params: {page: 2, per_page: 10}
    assert_response :success
    assert_equal result_count - 10, @response.parsed_body.length
  end

  test "should get show" do
    get api_page_path(@page, format: :json)
    assert_response :success

    assert_equal @page.id, @response.parsed_body["id"]
    assert_equal @page.title, @response.parsed_body["title"]
    assert_equal @page.slug, @response.parsed_body["slug"]
    assert_equal @page.content, @response.parsed_body["content"]
  end

  test "should get show from slug (without authentication)" do
    logout
    get api_page_path(@page.slug, format: :json)
    assert_response :success

    assert_equal @page.id, @response.parsed_body["id"]
    assert_equal @page.title, @response.parsed_body["title"]
    assert_equal @page.slug, @response.parsed_body["slug"]
    assert_equal @page.content, @response.parsed_body["content"]
  end

  test "should create" do
    assert_difference -> { Page.count } do
      post api_pages_path, as: :json, params: {
        slug: "new-page",
        title: "A new page",
        content: "The content of the new page"
      }
    end
    assert_response :success

    assert_equal "new-page", @response.parsed_body["slug"]
    assert_equal "A new page", @response.parsed_body["title"]
    assert_equal "The content of the new page", @response.parsed_body["content"]
  end

  test "should update" do
    patch api_page_path(@page), as: :json, params: {
      slug: "updated-slug",
      title: "A updated title",
      content: "The updated content of the page"
    }
    assert_response :success

    assert_equal "updated-slug", @response.parsed_body["slug"]
    assert_equal "A updated title", @response.parsed_body["title"]
    assert_equal "The updated content of the page", @response.parsed_body["content"]
  end

  test "should destroy" do
    refute_empty @page.menu_entries

    assert_difference -> { Page.count } => -1, -> { MenuEntry.count } => -@page.menu_entries.count do
      delete api_page_path(@page, format: :json)
    end
    assert_response :success
  end
end
