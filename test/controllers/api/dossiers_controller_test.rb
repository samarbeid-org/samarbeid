require "test_helper"

class Api::DossiersControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "srs_user@example.org")
    login_as @current_user
    @dossier_definition = DossierDefinition.find_by(name: "Person")
    @dossier = @dossier_definition.dossiers.find(1) # Admin User
  end

  def get_dossier_item_definition_from_name!(name)
    @dossier_definition.items.find_by!(name: name)
  end

  test "should get index" do
    get api_dossiers_path(format: :json)
    assert_response :success
  end

  test "should get index with additional query params" do
    params = {
      page: 1,
      query: "james",
      definition_ids: [@dossier_definition.id],
      order: "created_at_desc"
    }
    get api_dossiers_path(format: :json), params: params
    assert_response :success
  end

  test "should get index with query on subtitle" do
    query_string = "willms"
    params = {
      page: 1,
      query: query_string,
      order: "created_at_desc"
    }
    get api_dossiers_path(format: :json), params: params
    assert_response :success

    assert_equal 1, @response.parsed_body["result"].length
    assert_match query_string, @response.parsed_body["result"][0]["subtitle"]
    refute_match query_string, @response.parsed_body["result"][0]["title"]
  end

  test "index of dossiers with data fields filter should work" do
    dossier_item = @dossier.dossier_items.includes(:dossier_item_definition).find_by!(value: "admin@example.org", dossier_item_definitions: {content_type: "string"})
    assert_operator @dossier_definition.dossiers.count, :>, 1

    params = {
      page: 1,
      dossier_definition_ids: [@dossier_definition.id],
      filter_category: "ALL",
      fields: [{id: dossier_item.dossier_item_definition.id, comp: "does_contain", values: "@example."}]
    }
    get api_dossiers_path(format: :json), params: params
    assert_response :success

    assert_equal 2, @response.parsed_body["result"].count
    assert_equal [2, 1], @response.parsed_body["result"].pluck("id")
  end

  test "index should export to excel" do
    get api_dossiers_path(format: :xlsx)
    assert_response :success

    assert_equal "application/octet-stream", response.header["Content-Type"] # File download
    assert_equal Dossier.accessible_by(@current_user.build_ability(true)).count, @controller.instance_variable_get(:@result).query.count # No pagination
  end

  test "should get list with query params" do
    get list_api_dossiers_path(format: :json), params: {
      definition_id: @dossier_definition.id,
      except: [@dossier.id],
      query: "user"
    }
    assert_response :success
  end

  test "#list should support paging of results" do
    result_count = @dossier_definition.dossiers.count
    assert_operator result_count, :>, 10

    last_page_count = result_count % 10
    per_page = result_count - last_page_count

    get list_api_dossiers_path(format: :json), params: {definition_id: @dossier_definition.id, page: 1, per_page: per_page}
    assert_response :success
    assert_equal per_page, @response.parsed_body.length

    get list_api_dossiers_path(format: :json), params: {definition_id: @dossier_definition.id, page: 2, per_page: per_page}
    assert_response :success
    assert_equal last_page_count, @response.parsed_body.length
  end

  test "should get list for external users authorized via share link (if shared task has dossier field of the same dossier definition)" do
    logout
    task_with_dossier_field = ContentItem.where(content_type: ContentTypes::Dossier.to_s).flat_map { |ci| ci.task_items.map { |ti| ti.task } }.find { |t| t.active? }
    refute_nil task_with_dossier_field
    share_link = ShareLink.create!(shareable: task_with_dossier_field)
    assert share_link.can?(:list, Dossier)

    params = {
      share_token: share_link.token,
      definition_id: share_link.listable_dossier_definition_ids.first
    }
    get list_api_dossiers_path(format: :json), params: params
    assert_response :success
    assert @response.parsed_body.any?
  end

  test "shouldn't get list for external users authorized via share link (if shared task hasn't dossier field of the same dossier definition)" do
    logout
    task_with_dossier_field = ContentItem.where(content_type: ContentTypes::Dossier.to_s).flat_map { |ci| ci.task_items.map { |ti| ti.task } }.find { |t| t.active? }
    refute_nil task_with_dossier_field
    share_link = ShareLink.create!(shareable: task_with_dossier_field)
    assert share_link.can?(:list, Dossier)

    non_authorized_dossier_definition = DossierDefinition.where.not(id: share_link.listable_dossier_definition_ids).first
    refute_nil non_authorized_dossier_definition

    params = {
      share_token: share_link.token,
      definition_id: non_authorized_dossier_definition&.id
    }
    get list_api_dossiers_path(format: :json), params: params
    assert_response :success
    refute @response.parsed_body.any?
  end

  test "should get dossier show" do
    get api_dossier_path(@dossier, format: :json)
    assert_response :success
  end

  test "should refuse to get show of a deleted dossier" do
    @dossier.destroy!
    get api_dossier_path(@dossier, format: :json)
    assert_response :not_found
  end

  test "should get new, a not persisted dossier" do
    get new_api_dossier_path(format: :json), params: {dossier_definition_id: @dossier_definition.id}
    assert_response :success
  end

  test "should create a new dossier" do
    data = {
      "E-Mail" => "test@test.org",
      "Name" => "Test Dossier Name"
    }

    assert_difference -> { Dossier.count } do
      post api_dossiers_path(format: :json), params: {
        dossier_definition_id: @dossier_definition.id,
        field_data: data.map { |k, v| [get_dossier_item_definition_from_name!(k).id, v] }.to_h
      }
    end
    assert_response :success
  end

  test "should not create a new dossier without all required fields" do
    assert_no_difference -> { Dossier.count } do
      post api_dossiers_path(format: :json), params: {
        dossier_definition_id: @dossier_definition.id,
        field_data: {get_dossier_item_definition_from_name!("E-Mail").id => "test@test.org"}
      }
    end
    assert_response :unprocessable_entity
  end

  test "should update dossier" do
    dossier_item_definition = get_dossier_item_definition_from_name!("Name")
    dossier_item = @dossier.dossier_items.find_by!(dossier_item_definition: dossier_item_definition)

    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_dossier_item_definition_from_name!("Name").id, value: "Updated Name", lock_version: dossier_item.lock_version}
    assert_response :success
  end

  test "should respond with error to invalid update" do
    dossier_item_definition = get_dossier_item_definition_from_name!("Name")
    dossier_item = @dossier.dossier_items.find_by!(dossier_item_definition: dossier_item_definition)

    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_dossier_item_definition_from_name!("Name").id, value: nil, lock_version: dossier_item.lock_version}
    assert_response :unprocessable_entity
  end

  test "should refuse update if dossier has been changed inbetween" do
    dossier_item_definition = get_dossier_item_definition_from_name!("Name")
    dossier_item = @dossier.dossier_items.find_by!(dossier_item_definition: dossier_item_definition)
    old_lock_version = dossier_item.lock_version
    dossier_item.touch

    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_dossier_item_definition_from_name!("Name").id, value: "Updated Name", lock_version: old_lock_version}
    assert_response :unprocessable_entity
  end

  test "should delete dossier" do
    delete api_dossier_path(@dossier, format: :json)
    assert_response :success
  end

  test "should get dossier show_task_references" do
    get show_task_references_api_dossier_path(@dossier, format: :json)
    assert_response :success
  end

  test "should refuse to get show_task_references of a deleted dossier" do
    @dossier.destroy!
    get show_task_references_api_dossier_path(@dossier, format: :json)
    assert_response :not_found
  end

  ## Access Denied Tests

  # All logged in Users may access #index and #list but get results according to search
  test "should refuse #index to not logged in users" do
    logout
    get api_dossiers_path(format: :json)
    assert_response :unauthorized
  end

  test "should refuse #list to not logged in users" do
    logout
    get list_api_dossiers_path(format: :json)
    assert_response :unauthorized
  end

  test "should refuse #show for non group user" do
    login_non_group_user
    get api_dossier_path(@dossier, format: :json)
    assert_response :forbidden
  end

  test "should refuse #new for non group user" do
    login_non_group_user
    get new_api_dossier_path(format: :json), params: {dossier_definition_id: @dossier_definition.id}
    assert_response :forbidden
  end

  test "should refuse #create for non group user" do
    login_non_group_user
    post api_dossiers_path(format: :json), params: {dossier_definition_id: @dossier_definition.id}
    assert_response :forbidden
  end

  test "should refuse #update for non group user" do
    login_non_group_user
    patch api_dossier_path(@dossier, format: :json), params: {dossier_field_definition_id: get_dossier_item_definition_from_name!("Name").id, value: "Updated Name"}
    assert_response :forbidden
  end

  test "should refuse #destroy for non group user" do
    login_non_group_user
    delete api_dossier_path(@dossier, format: :json)
    assert_response :forbidden
  end

  test "should refuse #show_task_references for non group user" do
    login_non_group_user
    get show_task_references_api_dossier_path(@dossier, format: :json)
    assert_response :forbidden
  end

  private

  def login_non_group_user
    logout
    login_as(User.find_by_email("no_access_user@example.org"))
  end
end
