require "test_helper"

class Api::DossierItemDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_item_definition = @dossier_definition.items.find_by!(name: "String")

    @team_admin = User.find_by!(email: "team-admin@example.org")
    login_as @team_admin
  end

  test "#create should create new dossier item definition" do
    assert_difference -> { DossierItemDefinition.count } do
      post api_dossier_item_definitions_path, as: :json, params: {
        dossier_definition_id: @dossier_definition.id,
        name: "New Field",
        content_type: ContentTypes::String.to_s,
        options: {}
      }
    end
    assert_response :success
  end

  test "#update should change a dossier item definition" do
    refute_equal "New Name", @dossier_item_definition.name
    refute @dossier_item_definition.recommended
    assert @dossier_item_definition.required
    assert @dossier_item_definition.unique

    patch api_dossier_item_definition_path(@dossier_item_definition), as: :json, params: {
      name: "New Name",
      recommended: true,
      required: false,
      unique: false
    }
    assert_response :success

    @dossier_item_definition.reload
    assert_equal "New Name", @dossier_item_definition.name
    assert @dossier_item_definition.recommended
    refute @dossier_item_definition.required
    refute @dossier_item_definition.unique
  end

  test "#update shouldn't change the content_type of a dossier item definition" do
    assert_equal ContentTypes::String.to_s, @dossier_item_definition.content_type.to_s

    assert_no_changes -> { @dossier_item_definition.content_type } do
      patch api_dossier_item_definition_path(@dossier_item_definition), as: :json, params: {
        content_type: ContentTypes::Integer.to_s
      }
      assert_response :success
      @dossier_item_definition.reload
    end
  end

  test "#update shouldn't activate required or unique of a dossier item definition" do
    item = @dossier_definition.items.find_by!(required: false, unique: false)

    patch api_dossier_item_definition_path(item), as: :json, params: {
      required: true,
      unique: true
    }
    assert_response :unprocessable_entity
  end

  test "#destroy should delete a dossier item definition" do
    assert_difference -> { DossierItemDefinition.count }, -1 do
      delete api_dossier_item_definition_path(@dossier_item_definition, format: :json)
    end
    assert_response :success
  end

  test "#move should reorder the items of a dossier definition by the attribute position" do
    refute_equal @dossier_definition.items.last, @dossier_item_definition

    patch move_api_dossier_item_definition_path(@dossier_item_definition), as: :json, params: {
      index: @dossier_definition.items.count
    }

    assert_response :success
    assert_equal @dossier_definition.items.last, @dossier_item_definition
  end
end
