require "test_helper"

class Api::UserSettingsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @admin = User.find_by!(email: "team-admin@example.org")
  end

  test "non admin shouldn't get other user settings" do
    login_as @user
    get api_user_setting_path(@admin, format: :json)
    assert_response :forbidden
  end

  test "non admin should get his own settings" do
    login_as @user
    get api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "admin should get other user settings" do
    login_as @admin
    get api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "user should be able to update it's own user data" do
    login_as @user
    avatar_file = fixture_file_upload("avatar.png", "image/png")
    patch api_user_settings_path, as: :multipart_form, params: {
      firstname: "testfirst",
      lastname: "testlast",
      avatar: avatar_file
    }
    assert_response :success

    assert_equal "testfirst", @response.parsed_body["firstname"]
    assert_equal "testlast", @response.parsed_body["lastname"]
    assert_not_nil @response.parsed_body["avatar"]["url"]
  end

  test "user should be able to update it's own email with correct email_confirmation" do
    old_email = @user.email
    new_email = "new@example.org"

    login_as @user

    assert_no_difference -> { ActionMailer::Base.deliveries.count } do
      patch update_email_api_user_setting_path(@user), as: :json, params: {
        email: new_email
      }
    end
    assert_response :unprocessable_entity

    assert_no_difference -> { ActionMailer::Base.deliveries.count } do
      patch update_email_api_user_setting_path(@user), as: :json, params: {
        email: new_email,
        email_confirmation: "different@example.org"
      }
    end
    assert_response :unprocessable_entity

    assert_difference -> { ActionMailer::Base.deliveries.count } do
      patch update_email_api_user_setting_path(@user), as: :json, params: {
        email: new_email,
        email_confirmation: new_email
      }
    end
    assert_response :success

    assert_equal old_email, @response.parsed_body["email"]
    assert_equal new_email, @response.parsed_body["unconfirmed_email"]
  end

  test "user should be able to update it's own user notification settings" do
    refute_equal "hourly", @user.noti_interval

    login_as @user
    patch update_notification_settings_api_user_settings_path, as: :json, params: {
      interval: "hourly"
    }
    assert_response :success

    assert_equal "hourly", @response.parsed_body["notifications"]["interval"]
  end

  test "non admin user shouldn't be able to update admin attribute" do
    login_as @user
    assert_no_changes -> { @user.reload.is_admin? } do
      patch update_admin_status_api_user_setting_path(@user), as: :json, params: {
        admin: true
      }
    end
    assert_response :forbidden
  end

  test "admin user should be able to update admin attribute of other user" do
    login_as @admin
    assert_changes -> { @user.reload.is_admin? } do
      patch update_admin_status_api_user_setting_path(@user), as: :json, params: {
        admin: true
      }
    end
    assert_response :success
  end

  test "admin user shouldn't be able to update admin attribute of his self" do
    login_as @admin
    assert_no_changes -> { @admin.reload.is_admin? } do
      patch update_admin_status_api_user_setting_path(@admin), as: :json, params: {
        admin: true
      }
    end
    assert_response :forbidden
  end

  test "user should be able to update password" do
    login_as @user
    patch update_password_api_user_settings_path, as: :json, params: {
      password: "test123",
      password_confirmation: "test123",
      current_password: "password"
    }
    assert_response :success

    # user is still signed in after password changed
    get api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "user shouldn't be able to update password without submitting current_password" do
    login_as @user
    patch update_password_api_user_settings_path, as: :json, params: {
      password: "test123",
      password_confirmation: "test123"
    }
    assert_response :unprocessable_entity
    assert @response.parsed_body.key?("current_password")
  end

  test "user shouldn't be able to update password without submitting correct password_confirmation" do
    login_as @user
    patch update_password_api_user_settings_path, as: :json, params: {
      password: "test123",
      password_confirmation: "test1234",
      current_password: "password"
    }
    assert_response :unprocessable_entity
    assert @response.parsed_body.key?("password_confirmation")
  end

  test "should get groups of a user" do
    login_as @user
    get groups_api_user_setting_path(@user, format: :json)
    assert_response :success
  end

  test "admin user should be able to update active status of other user" do
    login_as @admin

    assert_difference -> { Events::DeactivatedEvent.for_object(@user).count } do
      assert_changes -> { @user.reload.deactivated? }, from: false, to: true do
        patch update_active_status_api_user_setting_path(@user), as: :json, params: {
          active: false
        }
      end
    end
    assert_response :success

    assert_difference -> { Events::ActivatedEvent.for_object(@user).count } do
      assert_changes -> { @user.reload.deactivated? }, from: true, to: false do
        patch update_active_status_api_user_setting_path(@user), as: :json, params: {
          active: true
        }
      end
    end
    assert_response :success
  end

  test "non admin user shouldn't be able to update active status of other user" do
    @non_admin_user = User.find_by!(email: "dsb@example.org")
    refute_equal @user, @non_admin_user

    login_as @non_admin_user
    assert_no_difference -> { Events::DeactivatedEvent.for_object(@user).count } do
      assert_no_changes -> { @user.reload.deactivated? } do
        patch update_active_status_api_user_setting_path(@user), as: :json, params: {
          active: false
        }
      end
    end
    assert_response :forbidden
  end

  test "admin user shouldn't be able to update active status of itself" do
    login_as @admin
    assert_no_difference -> { Events::DeactivatedEvent.for_object(@admin).count } do
      assert_no_changes -> { @admin.reload.deactivated? } do
        patch update_active_status_api_user_setting_path(@admin), as: :json, params: {
          active: false
        }
      end
    end
    assert_response :forbidden
  end

  test "non admin shouldn't be able to retrigger confirmation email of unconfirmed user" do
    new_user = User.create(email: "i-receive-mail@example.org", firstname: "First", lastname: "Last")
    refute new_user.confirmed?

    login_as @user
    assert_no_difference -> { ActionMailer::Base.deliveries.count } do
      patch resend_confirmation_instructions_api_user_setting_path(new_user), as: :json
    end
    assert_response :forbidden
  end

  test "admin should be able to retrigger confirmation email of unconfirmed user" do
    new_user = User.create(email: "i-receive-mail@example.org", firstname: "First", lastname: "Last")
    refute new_user.confirmed?

    login_as @admin
    assert_difference -> { ActionMailer::Base.deliveries.count } do
      patch resend_confirmation_instructions_api_user_setting_path(new_user), as: :json
    end
    assert_response :success

    assert_account_confirmation_email(new_user.email)
  end

  test "retriggering of confirmation email of confirmed user should fail with error" do
    assert @user.confirmed?

    login_as @admin
    assert_no_difference -> { ActionMailer::Base.deliveries.count } do
      patch resend_confirmation_instructions_api_user_setting_path(@user), as: :json
    end
    assert_response :unprocessable_entity
  end
end
