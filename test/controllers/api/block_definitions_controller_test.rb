require "test_helper"

class Api::BlockDefinitionsControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "dsb@example.org")
    @workflow_definitions = WorkflowDefinition.includes(:groups).where(groups: {id: @user.groups})
    @workflow_definition = @workflow_definitions.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @block_definition = @workflow_definition.block_definitions.first
    login_as(@user)
  end

  test "should create block definition and add it to the end of an existing workflow definition" do
    block_count_before = @workflow_definition.block_definitions.count

    assert_difference -> { @workflow_definition.block_definitions.count } do
      post api_block_definitions_path(format: :json), params: {block_definition: {workflow_definition_id: @workflow_definition.id}}
      assert_response :success
    end

    assert_equal block_count_before + 1, @response.parsed_body["structure"].count { |elem| elem["type"] == "block_definition" }
    last_item = @response.parsed_body["structure"].last
    assert_equal "block_definition", last_item["type"]
    assert_equal "Neuer Block", last_item["title"]
  end

  test "should be updatable" do
    assert_not_nil @block_definition
    patch api_block_definition_path(@block_definition, format: :json), params: {block_definition: {title: "New title"}}
    assert_response :success
    assert_equal "New title", @block_definition.reload.title
  end

  test "should destroy a block definition" do
    @block_definition.direct_task_definitions.destroy_all

    assert_difference -> { @workflow_definition.block_definitions.count }, -1 do
      delete api_block_definition_path(@block_definition)
    end
    assert_response :success
  end

  # Moving tests

  test "block_definition should be movable to the beginning (index_new < index_old)" do
    movee = @workflow_definition.block_definitions.last
    assert_operator @workflow_definition.item_definitions.index(movee), :>, 0
    assert_not_nil movee
    patch move_api_block_definition_path(movee, format: :json), params: {index: 0}
    assert_response :success
    assert @workflow_definition.reload.item_definitions.first.id == movee.id
  end

  test "block_definition should be movable to the end (index_new > index_old)" do
    movee = @workflow_definition.block_definitions.first
    new_index = @workflow_definition.item_definitions.length - 1
    assert_operator @workflow_definition.item_definitions.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_block_definition_path(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert @workflow_definition.reload.item_definitions.last.id == movee.id
  end

  test "block_definition from end should be movable to the middle" do
    movee = @workflow_definition.block_definitions.last
    assert_operator @workflow_definition.item_definitions.index(movee), :!=, 1
    assert_not_nil movee
    patch move_api_block_definition_path(movee, format: :json), params: {index: 1}
    assert_response :success
    assert @workflow_definition.reload.item_definitions.second.id == movee.id
  end

  test "block_definition should be movable behind other block_definitions using big index (index_new > index_old)" do
    movee = @workflow_definition.block_definitions.first
    new_index = @workflow_definition.item_definitions.length + 99
    assert_operator @workflow_definition.item_definitions.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_block_definition_path(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert @workflow_definition.reload.item_definitions.last == movee
  end

  test "should create, update, destroy or move block definition of system_process_definition_single_task" do
    post api_block_definitions_path(format: :json), params: {block_definition: {workflow_definition_id: WorkflowDefinition.system_process_definition_single_task.id}}
    assert_response :unprocessable_entity

    workflow_definition = WorkflowDefinition.system_process_definition_single_task
    block_definition = workflow_definition.block_definitions.create!(title: "Neuer Block", workflow_definition: workflow_definition, position: 5)

    patch api_block_definition_path(block_definition, format: :json), params: {
      block_definition: {name: "Updated name"}
    }
    assert_response :unprocessable_entity

    delete api_block_definition_path(block_definition)
    assert_response :unprocessable_entity

    patch move_api_block_definition_path(block_definition, format: :json), params: {index: 1}
    assert_response :unprocessable_entity
  end
end
