require "test_helper"

class Api::TasksControllerTest < ActionDispatch::IntegrationTest
  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @user_dsb = User.find_by!(email: "dsb@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")
    @admin = User.find_by!(email: "admin@example.org")

    @srs_workflow = Workflow.find_by!(title: "Mein Geburtstag")
    @srs_task = @srs_workflow.direct_tasks.find { |t| t.name == "Veranstalter angeben" }
  end

  test "index of tasks should work without parameter and return correct json" do
    login_as(@user)

    get api_tasks_url(format: :json)
    assert_response :success

    assert @response.parsed_body.key?("filter_category_counts")
    assert_equal 5, @response.parsed_body["filter_category_counts"].count

    assert @response.parsed_body.key?("users")
    assert_equal 7, @response.parsed_body["users"].count

    assert @response.parsed_body.key?("workflow_definitions")
    assert_equal 6, @response.parsed_body["workflow_definitions"].count

    assert @response.parsed_body.key?("total_pages")
    assert_equal 1, @response.parsed_body["total_pages"]

    assert @response.parsed_body.key?("result")
    assert_equal 7, @response.parsed_body["result"].count
    assert @response.parsed_body["result"].first.key?("workflow")
    assert @response.parsed_body["result"].first.key?("tasks")
    assert_equal 2, @response.parsed_body["result"].first["tasks"].count
  end

  test "index of tasks for my marked tasks should work" do
    login_as(@user)

    get api_tasks_url(filter_category: "MARKED", format: :json)
    assert_response :success

    assert @response.parsed_body.key?("result")
    assert_equal 2, @response.parsed_body["result"].count
  end

  test "index of tasks for single tasks derived from the single task definition should work" do
    login_as(@user)

    get api_tasks_url(workflow_definition_ids: ["none"], format: :json)
    assert_response :success

    assert @response.parsed_body.key?("result")
    assert_equal 1, @response.parsed_body["result"].count
    assert_equal 2, @response.parsed_body["result"].first["tasks"].count
  end

  test "index of tasks with full filters should work" do
    login_as(@user)

    params = {
      page: 1,
      assignee_ids: [@user.id],
      workflow_definition_ids: [@srs_workflow.workflow_definition.id],
      filter_category: "ACTIVE",
      query: @srs_task.name,
      order: "created_at_desc"
    }

    get api_tasks_url(params.merge(format: :json))
    assert_response :success

    assert_equal 1, @response.parsed_body["result"]&.count
  end

  test "index of tasks with data fields filter should work" do
    login_as(@user)

    content_item = @srs_workflow.content_items.find_by!(content_type: "string", value: "Geburtstagsparty 2020")
    assert_operator @srs_workflow.workflow_definition.workflows.count, :>, 1

    params = {
      page: 1,
      workflow_definition_ids: [@srs_workflow.workflow_definition.id],
      filter_category: "ALL",
      fields: [{id: content_item.content_item_definition.id, comp: "does_contain", values: "party"}]
    }

    get api_tasks_url(params.merge(format: :json))
    assert_response :success

    assert_equal 1, @response.parsed_body["result"].count
    assert_equal @srs_workflow.id, @response.parsed_body["result"].first["workflow"]["id"]
  end

  test "fieldDefinitions of result of task-by-definition index action should be ordered correctly" do
    login_as(@user)

    workflow_definition = WorkflowDefinition.find_by!(name: "Bürgeranfrage")
    params = {
      workflow_definition_id: workflow_definition.id
    }

    get api_tasks_url(params.merge(format: :json))
    assert_response :success

    task_names_result = @response.parsed_body["fieldDefinitions"].map { |fd| fd["grouping"] }.uniq
    task_names_structure = workflow_definition.task_definitions_ordered_in_structure.map(&:name)
    assert_equal task_names_structure & task_names_result, task_names_result
  end

  test "index should export to excel" do
    login_as(@admin)

    params = {
      filter_category: "ALL"
    }

    get api_tasks_url(params.merge(format: :xlsx))
    assert_response :success

    assert_equal "application/octet-stream", response.header["Content-Type"] # File download
    assert_equal Task.accessible_by(@admin.build_ability(true)).count, @controller.instance_variable_get(:@result).query.count # No pagination
  end

  test "list of tasks should work" do
    login_as(@user)

    get list_api_tasks_url(format: :json)
    assert_response :success
  end

  test "list of tasks should work with filters" do
    login_as(@user)

    get list_api_tasks_url(format: :json), params: {except: [@srs_task.id], query: "srs"}
    assert_response :success
  end

  test "#list should support paging of results" do
    login_as @user

    result_count = Task.accessible_by(@user.ability, :read).count
    assert_operator result_count, :>, 10

    last_page_count = result_count % 10
    per_page = result_count - last_page_count

    get list_api_tasks_url, as: :json, params: {page: 1, per_page: per_page}
    assert_response :success
    assert_equal per_page, @response.parsed_body.length

    get list_api_tasks_url, as: :json, params: {page: 2, per_page: per_page}
    assert_response :success
    assert_equal last_page_count, @response.parsed_body.length
  end

  test "should show task" do
    login_as(@user)

    get api_task_url(@srs_task, format: :json)
    assert_response :success
  end

  test "should update_assignee" do
    login_as(@user)

    patch update_assignee_api_task_path(@srs_task, format: :json), params: {assignee: @user.id}
    assert_response :success
  end

  test "should allow removing assignee" do
    login_as(@user)

    patch update_assignee_api_task_path(@srs_task, format: :json), params: {assignee: nil}
    assert_response :success
  end

  test "should update_contributors" do
    login_as(@user)

    patch update_contributors_api_task_path(@srs_task, format: :json), params: {contributors: User.all.limit(3).pluck(:id)}
    assert_response :success
  end

  test "#update_contributors should work with users and groups and only add activated users" do
    login_as(@admin)

    group_srs = Group.find_by!(name: "srs")
    @srs_task.update!(contributor_ids: [])
    @user.deactivate

    assert_equal 0, @srs_task.contributors.count

    assert_difference -> { @srs_task.contributors.count }, group_srs.users.activated.count do
      patch update_contributors_api_task_path(@srs_task, format: :json), params: {
        contributors: [{type: "user", id: @user.id}, {type: "group", id: group_srs.id}]
      }
    end
    assert_response :success

    @user.activate

    assert_difference -> { @srs_task.contributors.count } do
      patch update_contributors_api_task_path(@srs_task, format: :json), params: {
        contributors: [{type: "group", id: group_srs.id}]
      }
    end
    assert_response :success
  end

  test "should allow removing all contributors" do
    login_as(@user)

    patch update_contributors_api_task_path(@srs_task, format: :json), params: {contributors: []}
    assert_response :success
  end

  test "update name of a task" do
    login_as(@user)

    patch api_task_url(@srs_task, format: :json), params: {task: {name: "updated name"}, lock_version: @srs_task.lock_version}
    assert_response :success
    assert @srs_task.reload.name == "updated name"
  end

  test "should refuse update if task title/name has been changed inbetween" do
    login_as(@user)

    old_lock_version = @srs_task.lock_version
    @srs_task.touch

    patch api_task_url(@srs_task, format: :json), params: {task: {name: "updated_name", description: "updated description"}, lock_version: old_lock_version}

    assert_response :conflict
  end

  test "update due date of a task" do
    login_as(@user)

    patch update_due_date_api_task_url(@srs_task, format: :json), params: {due_at: Date.today}
    assert_response :success
  end

  test "remove due date of a task" do
    login_as(@user)

    patch update_due_date_api_task_url(@srs_task, format: :json), params: {due_at: nil}
    assert_response :success
  end

  test "make a task snooze" do
    login_as(@user)

    assert @srs_task.active?
    patch snooze_api_task_url(@srs_task, format: :json), params: {snooze_until: 3.days.from_now}
    assert_response :success
  end

  test "mark a task" do
    login_as(@user)

    patch update_marked_api_task_url(@srs_task, format: :json), params: {marked: true}
    assert_response :success
  end

  test "start a task" do
    login_as(@user)

    workflow = create_workflow_for!(WorkflowDefinition.first)
    task = workflow.direct_tasks.created.first

    patch start_api_task_url(task, format: :json)
    assert_response :success
  end

  test "complete a task" do
    login_as(@user)

    workflow = create_workflow_for!(WorkflowDefinition.first)

    task = workflow.direct_tasks.active.first

    patch complete_api_task_url(task, format: :json)
    assert_response :success
  end

  # Access Forbidden Tests

  test "no_access_user shouldn't be able to assign a user to a task" do
    login_as(@no_access_user)

    patch update_assignee_api_task_url(@srs_task, format: :json), params: {assignee: @no_access_user.id}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update the contributors of a task" do
    login_as(@no_access_user)

    patch update_contributors_api_task_url(@srs_task, format: :json), params: {contributors: [@no_access_user.id]}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update description of a task" do
    login_as(@no_access_user)

    patch api_task_url(@srs_task, format: :json), params: {description: "updated description"}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to update due date of a task" do
    login_as(@no_access_user)

    patch update_due_date_api_task_url(@srs_task, format: :json), params: {due_at: Date.today}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to complete a task" do
    login_as(@no_access_user)

    patch complete_api_task_url(@srs_task, format: :json)
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to mark a task" do
    login_as(@no_access_user)

    patch update_marked_api_task_url(@srs_task, format: :json), params: {marked: true}
    assert_response :forbidden
  end

  test "no_access_user shouldn't be able to clone a task" do
    login_as(@no_access_user)

    patch clone_api_task_url(@srs_task, format: :json)
    assert_response :forbidden
  end

  # Moving tests

  test "task from end should be movable to beginning" do
    login_as(@user)

    movee = @srs_workflow.direct_tasks.last
    assert_operator @srs_workflow.items.index(movee), :>, 0
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 0}
    assert_response :success
    assert @srs_workflow.reload.items.first.id == movee.id
  end

  test "tasks from end should be movable to middle" do
    login_as(@user)

    movee = @srs_workflow.direct_tasks.last
    assert_operator @srs_workflow.items.index(movee), :!=, 1
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 1}
    assert_response :success
    assert @srs_workflow.reload.items.second.id == movee.id
  end

  test "tasks from beginning should be movable" do
    login_as(@user)

    movee = @srs_workflow.direct_tasks.first
    assert_operator @srs_workflow.items.index(movee), :!=, 2
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 2}
    assert_response :success
    assert @srs_workflow.reload.items.third.id == movee.id
  end

  test "tasks should be movable to end with higher index" do
    login_as(@user)

    movee = @srs_workflow.direct_tasks.first
    new_index = @srs_workflow.items.length + 99
    assert_operator @srs_workflow.items.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert @srs_workflow.reload.items.last == movee
  end

  test "task should be movable from workflow into block (beginning)" do
    login_as(@user_dsb)

    @workflow = Workflow.find_by!(title: "MM-20")
    @block = @workflow.blocks.first
    assert_operator @block.items.length, :>=, 2

    movee = @workflow.direct_tasks.first
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 0, target_id: @block.id, target_type: @block.class.name.underscore}
    assert_response :success
    refute @workflow.reload.direct_tasks.include?(movee)
    assert @block.reload.direct_tasks.first == movee
  end

  test "task should be movable from workflow into block (end)" do
    login_as(@user_dsb)

    @workflow = Workflow.find_by!(title: "MM-20")
    @block = @workflow.blocks.first
    assert_operator @block.items.length, :>=, 2

    movee = @workflow.direct_tasks.first
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: @block.items.length, target_id: @block.id, target_type: @block.class.name.underscore}
    assert_response :success
    refute @workflow.reload.direct_tasks.include?(movee)
    assert @block.reload.direct_tasks.last == movee
  end

  test "task should be movable from workflow into block (middle)" do
    login_as(@user_dsb)

    @workflow = Workflow.find_by!(title: "MM-20")
    @block = @workflow.blocks.first
    assert_operator @block.items.length, :>=, 2

    movee = @workflow.direct_tasks.first
    assert_not_nil movee
    patch move_api_task_url(movee, format: :json), params: {index: 1, target_id: @block.id, target_type: @block.class.name.underscore}
    assert_response :success
    refute @workflow.reload.direct_tasks.include?(movee)
    assert @block.reload.direct_tasks.second == movee
  end

  test "task should be movable from block into workflow (beginning)" do
    login_as(@user_dsb)

    @workflow = Workflow.find_by!(title: "MM-20")
    block = @workflow.blocks.first
    movee = block.direct_tasks.first
    assert_not_nil movee

    patch move_api_task_url(movee, format: :json), params: {index: 0, target_id: @workflow.id, target_type: @workflow.class.name.underscore}
    assert_response :success
    assert @workflow.reload.direct_tasks.first == movee
    refute block.reload.direct_tasks.include?(movee)
  end

  test "task should be movable from block to same block" do
    login_as(@user_dsb)

    workflow = Workflow.find_by!(title: "MM-20")
    block = workflow.blocks.first

    movee = block.direct_tasks.first
    refute workflow.direct_tasks.include?(movee)
    assert block.direct_tasks.include?(movee)
    assert block.direct_tasks.count > 1
    patch move_api_task_url(movee, format: :json), params: {index: 999, target_id: block.id, target_type: block.class.name.underscore}
    assert_response :success

    refute workflow.reload.direct_tasks.include?(movee)
    assert block.reload.direct_tasks.last == movee
    assert block.reload.items.last == movee
  end

  test "task should be movable from block to same block (without giving target)" do
    login_as(@user_dsb)

    workflow = Workflow.find_by!(title: "MM-20")
    block = workflow.blocks.first

    movee = block.direct_tasks.first
    refute workflow.direct_tasks.include?(movee)
    assert block.direct_tasks.include?(movee)
    assert block.direct_tasks.count > 1
    patch move_api_task_url(movee, format: :json), params: {index: 999}
    assert_response :success

    refute workflow.reload.direct_tasks.include?(movee)
    assert block.reload.direct_tasks.last == movee
  end

  test "task should be movable from block to other block" do
    login_as(@user_dsb)

    workflow = Workflow.find_by!(title: "MM-20")
    block1 = workflow.blocks.first
    block2 = workflow.blocks.last
    refute_equal block1, block2

    movee = block1.direct_tasks.first
    refute workflow.direct_tasks.include?(movee)
    assert block1.direct_tasks.include?(movee)
    refute block2.direct_tasks.include?(movee)

    patch move_api_task_url(movee, format: :json), params: {index: 1, target_id: block2.id, target_type: block2.class.name.underscore}
    assert_response :success

    assert block2.direct_tasks.include?(movee)
    refute block1.direct_tasks.include?(movee)
    refute workflow.reload.direct_tasks.include?(movee)
    assert block2.reload.direct_tasks.second == movee
  end

  test "should be possible to clone a task" do
    login_as(@user)

    assert_difference -> { Task.count } do
      patch clone_api_task_url(@srs_task, format: :json)
    end
    assert_response :success
  end

  test "cloning a single task should convert single task to normal process" do
    single_task_process_definition = WorkflowDefinition.system_process_definition_single_task
    single_task_process = Interactors::WorkflowInteractor.create(
      single_task_process_definition, {title: "Testaufgabe"}, {}, nil, @user
    )
    assert single_task_process.system_process_single_task?

    login_as(@user)
    assert_difference -> { single_task_process.direct_tasks.count }, 1 do
      patch clone_api_task_url(single_task_process.direct_tasks.first, format: :json)
    end
    assert_response :success

    refute single_task_process.reload.system_process_single_task?
  end

  test "should be possible to destroy tasks of a process but not the last one" do
    login_as(@user)

    task_count = @srs_workflow.all_tasks.count
    assert_operator task_count, :>, 1
    last_task = @srs_workflow.all_tasks.sample

    assert_difference -> { Task.count }, -(task_count - 1) do
      @srs_workflow.all_tasks.where.not(id: last_task.id).each do |task|
        delete api_task_url(task, format: :json)
      end
    end
    assert_response :success

    assert_equal 1, @srs_workflow.all_tasks.reload.count
    assert_no_difference -> { Task.count } do
      delete api_task_url(last_task, format: :json)
    end
    assert_response :unprocessable_entity
  end

  test "should be possible to share a task" do
    login_as(@user)

    assert_empty @srs_task.share_links.active
    assert_difference -> { @srs_task.share_links.active.count } do
      post create_share_api_task_url(@srs_task, format: :json), params: {name: "Testshare"}
    end
    assert_response :success
  end

  test "should be possible to complete sharing a task" do
    login_as(@user)

    Interactors::ShareTaskInteractor.share(@srs_task, "Testshare", @user)
    assert_equal 1, @srs_task.share_links.active.count

    assert_difference -> { @srs_task.share_links.active.count }, -1 do
      patch complete_share_api_task_url(@srs_task, format: :json)
    end
    assert_response :success
  end
end
