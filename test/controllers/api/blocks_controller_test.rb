require "test_helper"
require "aasm/minitest"

class Api::BlocksControllerTest < ActionDispatch::IntegrationTest
  def setup
    @current_user = User.find_by!(email: "dsb@example.org")
    login_as(@current_user)
    @workflow = Workflow.find_by!(title: "MM-20")
  end

  test "should be updatable" do
    block = @workflow.blocks.last
    assert_not_nil block
    patch api_block_url(block, format: :json), params: {block: {title: "New title"}}
    assert_response :success
    assert_equal "New title", block.reload.title
  end

  test "can edit parallel attribute if created" do
    block = @workflow.blocks.last
    assert_not_nil block
    assert_not block.parallel?
    assert_have_state block, :created
    patch api_block_url(block, format: :json), params: {block: {parallel: true}}
    assert_response :success
    assert block.reload.parallel?
  end

  test "cannot edit parallel attribute later" do
    @workflow = Workflow.find_by!(title: "event-test-workflow")
    block = @workflow.blocks.first
    assert_not block.parallel?
    patch api_block_url(block, format: :json), params: {block: {parallel: true}}
    assert_response :unprocessable_entity
    assert_not block.reload.parallel?
  end

  test "delete" do
    @workflow = Workflow.find_by!(title: "event-test-workflow")
    block = @workflow.blocks.first
    assert_not_nil block
    block.direct_tasks.destroy_all
    assert block.direct_tasks.empty?
    delete api_block_url(block, format: :json)
    assert_response :success
    refute Block.exists?(block.id)
  end

  test "cannot delete unless empty" do
    @workflow = Workflow.find_by!(title: "event-test-workflow")
    block = @workflow.blocks.first
    assert_not_nil block
    assert block.direct_tasks.any?
    delete api_block_url(block, format: :json)
    assert_response :unprocessable_entity
    refute block.destroyed?
  end

  test "block should be movable to the beginning (index_new < index_old)" do
    movee = @workflow.blocks.last
    assert_operator @workflow.items.index(movee), :>, 0
    assert_not_nil movee
    patch move_api_block_url(movee, format: :json), params: {index: 0}
    assert_response :success
    assert @workflow.reload.items.first.id == movee.id
  end

  test "block should be movable to the end (index_new > index_old)" do
    movee = @workflow.blocks.first
    new_index = @workflow.items.length - 1
    assert_operator @workflow.items.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_block_url(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert @workflow.reload.items.last.id == movee.id
  end

  test "block from end should be movable to the middle" do
    movee = @workflow.blocks.last
    assert_operator @workflow.items.index(movee), :!=, 1
    assert_not_nil movee
    patch move_api_block_url(movee, format: :json), params: {index: 1}
    assert_response :success
    assert @workflow.reload.items.second.id == movee.id
  end

  test "block should be movable behind other blocks using big index (index_new > index_old)" do
    movee = @workflow.blocks.first
    new_index = @workflow.items.length + 99
    assert_operator @workflow.items.index(movee), :<, new_index
    assert_not_nil movee
    patch move_api_block_url(movee, format: :json), params: {index: new_index}
    assert_response :success
    assert @workflow.reload.items.last == movee
  end
end
