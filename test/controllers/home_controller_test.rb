require "test_helper"

class HomeControllerTest < ActionDispatch::IntegrationTest
  setup do
    @current_user = User.find_by!(email: "srs_user@example.org")
  end

  test "should allow home#show to logged in user" do
    login_as(@current_user)
    get home_show_path
    assert_response :success
  end

  test "should allow home#my to logged in user" do
    login_as(@current_user)
    get "/my"
    assert_response :success
  end

  test "home#show should redirect to login if not logged in" do
    get home_show_path
    assert_redirected_to new_user_session_path
  end

  test "home#my should redirect to login if not logged in" do
    get "/my"
    assert_redirected_to new_user_session_path
  end

  test "home#share should work with valid share_token" do
    @share_link = ShareLink.create!(shareable: Task.first)

    get home_share_url(share_token: @share_link.token)
    assert_response :success

    completed_share_link = ShareLink.completed.first
    assert completed_share_link
    get home_share_url(share_token: completed_share_link.token)
    assert_response :success
  end

  test "home#share should redirect to login with invalid or empty share_token (if not logged-in)" do
    get home_share_url(share_token: "wrong_share_token")
    assert_redirected_to new_user_session_path

    get home_share_url(share_token: "")
    assert_redirected_to new_user_session_path
  end

  test "home#share should return forbidden with invalid share_token (if logged-in)" do
    login_as(@current_user)

    get home_share_url(share_token: "wrong_share_token")
    assert_response :forbidden
  end
end
