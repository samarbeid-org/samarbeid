require "test_helper"

class AddonToolsControllerTest < ActionDispatch::IntegrationTest
  test "swagger documentation for external API should work" do
    login_as(User.find_by_email!("dsb@example.org"))
    get "/samarbeid-api-documentation"
    assert_response :success
  end

  test "swagger documentation for external API should be only visible to authenticated users" do
    get "/samarbeid-api-documentation"
    assert_response :redirect
    assert_redirected_to new_user_session_path
  end
end
