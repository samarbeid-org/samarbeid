if ENV["CI"]
  require "./test/simplecov_env"
  SimpleCovEnv.start!
end

ENV["RAILS_ENV"] ||= "test"

require_relative "../config/environment"
require "rails/test_help"

require "capybara/rails"
require "capybara/minitest"

# Execute Tests in parallel on CI
# Requires a report file with timing metrics to be present. Regenerate (if new tests are added or some take longer) with:
# CI=true KNAPSACK_GENERATE_REPORT=true bundle exec rake test test:system
# to sort for nicer diff/comparison execute following on your rails console (sorts alphabetically and rounds to two digit)
# File.write("knapsack_minitest_report.json", JSON.pretty_generate(JSON.load(File.read("knapsack_minitest_report.json")).sort.to_h.transform_values{|x| x.round(2)}.as_json))
if ENV["CI"]
  require "knapsack"
  Knapsack.tracker.config({enable_time_offset_warning: false})
  knapsack_adapter = Knapsack::Adapters::MinitestAdapter.bind
  knapsack_adapter.set_test_helper_path(__FILE__)
end

class ActiveSupport::TestCase
  # Setup all fixtures in test/fixtures/*.yml for all tests in alphabetical order.
  fixtures :all

  # Parallelization on CI is configured via knapsack.
  parallelize(workers: :number_of_processors) unless ENV["CI"]

  # Add more helper methods to be used by all tests here...
  def assert_account_confirmation_email(new_user_email)
    last_email = ActionMailer::Base.deliveries.last
    assert_equal [new_user_email], last_email.to
    assert_equal "samarbeid | Anleitung zur Konto-Bestätigung", last_email.subject

    html = Capybara.string last_email.html_part.decoded
    html.assert_text "Willkommen #{new_user_email}!"
    html.assert_selector 'a[href^="https://samarbeid-tests.example.com/users/confirmation?confirmation_token="]'
  end

  def mock_environment(env: "test", partial_env_hash: {})
    old_env_mode = Rails.env
    old_env = ENV.to_hash

    Rails.env = env
    ENV.update(partial_env_hash)

    begin
      yield
    ensure
      Rails.env = old_env_mode
      ENV.replace(old_env)
    end
  end

  def create_workflow_for!(definition, field_data = nil)
    workflow = definition.build_workflow(field_data)
    workflow.title = "Test-Workflow"
    workflow.tap(&:save!)
  end
end

class ActionDispatch::IntegrationTest
  include Warden::Test::Helpers # allows login_as(@user)

  Warden.test_mode!

  teardown do
    Warden.test_reset!
  end
end

VCR.configure do |config|
  config.ignore_hosts "apache-tika", "selenium__standalone-chrome", Capybara.server_host if ENV["CI"] # used in CI for services

  config.ignore_request do |request|
    uri = URI(request.uri)
    uri.host =~ /\.?ingest\.sentry\.io$/
  end

  config.cassette_library_dir = File.expand_path("cassettes", __dir__)
  config.hook_into :webmock
  config.ignore_localhost = true
  config.default_cassette_options = {
    record: :none,
    allow_playback_repeats: true
  } # to update cassettes set record: new_episodes
end
