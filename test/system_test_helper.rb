def login_using_form_with(email, password = "password")
  assert has_text?("Anmelden")

  within("div.sign-in-page form") do
    fill_in("user[email]", with: email)
    fill_in("user[password]", with: password)
    click_button("Anmelden")
  end
end

def logout_using_menu
  find_button("user-menu").click
  assert has_text?("Abmelden")

  list_of_user_menu_items = find("#user-menu-list")
  logout_link = list_of_user_menu_items.find("a", text: "Abmelden")
  logout_link.click

  assert_current_path(new_user_session_path)
  assert has_text?("Anmelden")
end

def control_for(label)
  page.find(".v-input.custom-object-control, .v-input.custom-input-control, .custom-control", text: label)
end

def header_area
  page.find("header.v-app-bar")
end

def right_sidebar
  page.find(".v-navigation-drawer--right")
end

def left_sidebar
  page.find(".v-navigation-drawer", match: :first)
end

def middle_area
  page.find(".container", match: :first)
end

def click_link_in_app_menu(link_name, check_closed = true)
  navigation_drawer = find("aside.v-navigation-drawer", visible: :all)
  header_area.find("button.v-app-bar__nav-icon").click unless navigation_drawer[:class].split.include?("v-navigation-drawer--open")
  within(navigation_drawer) { click_link(link_name) }
  find("aside.v-navigation-drawer.v-navigation-drawer--close", visible: :hidden) if check_closed
end

def click_link_in_settings_menu(link_name)
  navigation_drawer = find("aside.v-navigation-drawer", visible: :all)
  header_area.find("button.v-app-bar__nav-icon").click unless navigation_drawer[:class].split.include?("v-navigation-drawer--open")
  within(navigation_drawer) do
    settings_group = find("div.v-list-group", text: "Einstellungen")
    settings_group.click unless settings_group[:class].split.include?("v-list-group--active")
    within(settings_group) { click_link(link_name) }
  end
  find("aside.v-navigation-drawer.v-navigation-drawer--close", visible: :hidden)
end

def click_link_in_admin_menu(link_name, check_closed = false)
  navigation_drawer = find("aside.v-navigation-drawer", visible: :all)
  header_area.find("button.v-app-bar__nav-icon").click unless navigation_drawer[:class].split.include?("v-navigation-drawer--open")
  within(navigation_drawer) do
    admin_group = find("div.v-list-group", text: "Admin")
    admin_group.click unless admin_group[:class].split.include?("v-list-group--active")
    within(admin_group) { click_link(link_name) }
  end
  find("aside.v-navigation-drawer.v-navigation-drawer--close", visible: :hidden) if check_closed
end

def use_app_search_with(query)
  within(header_area) do
    click_button("activate-search-btn")
    fill_in "fulltext-search", with: query + "\n"
  end
  middle_area.find("h1", text: "Suche").click   # move focus to close searchbar
end

# Probably supports only controls with a form field (input, textarea, ...) for the value
def has_infobox?(with_label:, with_value: nil)
  has_field_options = {
    disabled: true
  }
  has_field_options[:with] = with_value unless with_value.nil?

  find(".support-infobox .v-input", text: with_label).has_field?(**has_field_options)
end

def has_control?(with_label: nil, with_value: nil, is_readonly: false)
  selector = ".v-input" + (is_readonly ? ".v-input--is-disabled.control--is-readonly" : "")

  find_options = {}
  find_options[:text] = with_label unless with_label.nil?

  has_field_options = {
    disabled: is_readonly
  }
  has_field_options[:with] = with_value unless with_value.nil?

  find(selector, **find_options).has_field?(**has_field_options)
end

def find_control(with_label)
  find(".control", text: with_label)
end

def control_has_message?(control, type, **)
  message_types = {
    success: "Erfolgreich gespeichert",
    updated: "vor weniger als 1 Minute",
    error_required: "muss ausgefüllt werden",
    error_email: "muss eine E-Mail-Adresse sein"
  }

  control.has_css?("div.v-messages__message", text: message_types[type], **)
end

def fill_text_control_and_assert_response_message(with_label, value, message_type = :success, fill_options: {}, has_message_options: {})
  control = find_control(with_label)
  control.fill_in with_label, with: value, fill_options: fill_options
  send_keys(:tab)
  assert control_has_message?(control, message_type, **has_message_options), "Control with label '#{with_label}' doesn't have message_type '#{message_type}' after filled with value: #{value}"
end

def fill_richtext_control_and_assert_response_message(with_label, value, message_type = :success)
  control = find_control(with_label)
  editor = find_prosemirror_editor(control.find(:xpath, ".."))

  prosemirror_fill_in(editor, value)
  prosemirror_fill_in(editor, :tab)
  assert control_has_message?(control, message_type)
end

def open_vertical_dots_context_menu
  page.find("button .v-icon.mdi-dots-vertical").click
end

def find_prosemirror_editor(parent)
  parent.find(".tiptap-vuetify-editor .ProseMirror")
end

def prosemirror_fill_in(element, text)
  element.base.send_keys(text)
end

def fill_in_title(title)
  page.find(".v-card .v-input .v-text-field__slot .v-label", text: "Titel")
    .find(:xpath, "..")
    .find("input[type=text]")
    .fill_in with: title
end

def click_menu_list_item(text)
  page.find(".v-menu__content .v-list .v-list-item--link", text: text).click
end

def click_dialog_button(text)
  page.find(".v-dialog button", text: text).click
end

def click_list_item_link(text)
  page.find(".v-list .v-list-item--link", text: text).click
end

def click_card_action_button(text)
  find(".v-card .v-card__actions button", text: text).click
end

def click_tab(text)
  page.find(".v-tab", text: text).click
end

def open_create_workflow_dialog(with_definition_selection = true)
  click_on "Starten"
  page.find(".v-menu__content .v-list-item", text: with_definition_selection ? "AUS VORLAGE" : "EINZELAUFGABE").click
  page.find(".create-workflow-dialog", match: :first)
end

def open_add_main_menu_entry_dialog
  page.find(".page-list-header .v-btn.v-btn--icon .v-icon.mdi-plus").click
  page.find(".v-menu__content .v-list-item", text: "Menüeintrag hinzufügen").click
  page.find(".add-main-menu-entry-dialog", match: :first)
end

def has_item?(text, path = ".v-list-item")
  has_css?(path, text: text)
end

def select_objects_control_item(item_text, search_text:, button_text: "Ändern", close: false)
  click_on button_text
  fill_in "Suche", with: search_text unless search_text.nil?
  find(".v-list-item", text: item_text).click
  click_on "Ändern" if close
end

def select_date_control_day(day, button_text: "Ändern")
  click_on button_text
  within page.document.find(".v-menu__content .v-picker") do
    if day.nil?
      find("button", text: "LÖSCHEN").click
    else
      find("table td button", text: day).click
      find("button", text: "SPEICHERN").click
    end
  end
end

def set_time_budget(hours, minutes)
  button_text = has_css?("button", text: "BUDGET HINZUFÜGEN") ? "Budget hinzufügen" : "Budget ändern"
  click_on button_text

  within page.document.find(".v-dialog__content .v-card", text: button_text) do
    fill_in "Stunden", with: hours
    fill_in "Minuten", with: minutes
    click_on "OK"
  end
end

def freeze_backend_and_frontend_date(*, &block)
  Timecop.freeze(*) do
    page.execute_script "window.clock = FakeTimers.install({ now: #{Time.now.to_i * 1000}, shouldAdvanceTime: true, toFake: ['Date']});"
    block.call
    page.execute_script "window.clock.uninstall();"
  end
end
