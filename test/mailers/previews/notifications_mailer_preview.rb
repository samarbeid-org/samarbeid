# Preview all emails at http://localhost:3000/rails/mailers/notifications_mailer
class NotificationsMailerPreview < ActionMailer::Preview
  # Preview this email at http://localhost:3000/rails/mailers/notifications_mailer/recent
  # or e.g. http://localhost:3000/rails/mailers/notifications_mailer/recent?user=133235
  def recent
    user = User.find(params[:user] || 1)
    notifications = user.notifications.undelivered.order(created_at: :desc)
    NotificationsMailer.with(user: user, notifications: notifications).recent
  end

  # Preview this email at http://localhost:3000/rails/mailers/notifications_mailer/recent_all
  # or e.g. http://localhost:3000/rails/mailers/notifications_mailer/recent_all?user=133235
  def recent_all
    if params[:user]
      user = User.find(params[:user])
      notifications = user.notifications.undone.order(created_at: :desc)
    else
      user = User.first
      notifications = Notification.all
    end
    NotificationsMailer.with(user: user, notifications: notifications).recent
  end
end
