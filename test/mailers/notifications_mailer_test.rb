require "test_helper"

class NotificationsMailerTest < ActionMailer::TestCase
  test "recent mails for fixture user dsb@example.org" do
    dsb_user = User.find_by!(email: "dsb@example.org")
    exp_noti_count = 21 # current testing fixtures, update to whatever the testing data contains for dsb_user
    assert_equal exp_noti_count, dsb_user.unseen_and_unsent_notifications.count

    exp_noti_unread_count = 25 # current testing fixtures, update to whatever the testing data contains for dsb_user
    assert_equal exp_noti_unread_count, dsb_user.notifications.count

    assert_equal 12, dsb_user.notifications.map(&:event).map(&:type).uniq.count
    assert_equal 28, Event.descendants.count # if you're adding or removing event types, you will need to upgrade that
    assert_equal 16, Event.descendants.count - dsb_user.notifications.map(&:event).map(&:type).uniq.count # TODO:  See numbers above, looks like we have x event types more which we should create tests for

    mail = NotificationsMailer.with(user: dsb_user, notifications: dsb_user.unseen_and_unsent_notifications).recent

    assert_equal "samarbeid | #{exp_noti_count} neue Benachrichtigungen (#{exp_noti_unread_count} ungelesene insgesamt)", mail.subject
    assert_equal ["dsb@example.org"], mail.to
    assert_equal ["no-reply@example.org"], mail.from

    html = Capybara.string mail.html_part.decoded
    html.assert_text "Hallo DSB Nutzer,"
    html.assert_text "Du hast #{exp_noti_count} neue Benachrichtigungen in samarbeid."
  end

  test "each event type has a fixture" do
    event_types_to_test = Event.descendants.filter { _1.instance_methods.include?(:notification_receivers) }.map(&:name)
    all_fixture_types = Notification.all.map(&:event).map(&:class).map(&:name).uniq
    assert_equal [], event_types_to_test - all_fixture_types, "Some Events do not have fixtures and thus are not rendered in the recents mailer tests"
  end

  test "each event type in recent-notifications mails" do
    receiver = User.create(email: "i-receive-mail@example.org", firstname: "First", lastname: "Last")
    notifications = Notification.all

    mail = nil
    assert_nothing_raised { mail = NotificationsMailer.with(user: receiver, notifications: notifications).recent }

    assert_equal 0, mail.errors.length
    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    html = Capybara.string mail.html_part.decoded
    html.assert_text "Hallo First Last,"
    html.assert_text "Du hast #{notifications.length} neue Benachrichtigungen in samarbeid."
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/notifications"]'
    html.assert_selector "span", text: "Admin User"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/my/settings/notifications"]', text: "Einstellungen"
  end

  test "each event type in recent-notifications mails as user with few rights" do
    receiver = User.create(email: "i-receive-mail@example.org", firstname: "First", lastname: "Last")
    notifications = Notification.all

    mail = nil
    assert_nothing_raised { mail = NotificationsMailer.with(user: receiver, notifications: notifications).recent }

    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    html = Capybara.string mail.html_part.decoded
    # Just a random selection of notifications, update as you please...
    html.assert_text "Admin User schrieb einen Kommentar zu Aufgabe"
    html.assert_text "Hallo dies ist ein Testkommentar, welcher eine Benachrichtigung für den Admin User erzeugen sollte."
    html.assert_text "Aufgabe  #50  in Prozess  %10 wurde automatisch gestartet"
    html.assert_text "Admin User hat Prozess  %10 abgeschlossen"
    html.assert_text "Admin User hat das Fälligkeitsdatum von Aufgabe  #123  in Prozess  %18 auf 15.04.2021 geändert • 14.04.2021, 11:07 Uhr"
    html.assert_text "@Admin User was soll Ich hier tun?"

    html.assert_selector "span", text: "#50"
    html.assert_selector "span", text: "%10"
  end

  test "each event type in recent-notifications mails as all-seeing-admin" do
    receiver = User.find_by!(email: "admin@example.org")

    notifications = Notification.all

    mail = nil
    assert_nothing_raised { mail = NotificationsMailer.with(user: receiver, notifications: notifications).recent }

    assert_equal 0, mail.errors.length
    assert_match(/\Asamarbeid \| #{notifications.length} neue Benachrichtigungen/, mail.subject)

    html = Capybara.string mail.html_part.decoded
    # Just a random selection of notifications, update as you please...
    html.assert_text "Hallo Admin User"
    html.assert_text "Du hast #{notifications.length} neue Benachrichtigungen in samarbeid"
    html.assert_text "Admin User schrieb einen Kommentar zu Aufgabe"
    html.assert_text "Hallo dies ist ein Testkommentar, welcher eine Benachrichtigung für den Admin User erzeugen sollte."
    html.assert_text "Aufgabe  #50 • Patentansprüche formulieren  in Prozess  %10 (Patentanmeldung) • PA-1 wurde automatisch gestartet • 28.02.2020, 17:14 Uhr"
    html.assert_text "Admin User hat Prozess  %10 (Patentanmeldung) • PA-1 abgeschlossen • 28.02.2020, 17:15 Uhr"
    html.assert_text "Admin User hat das Fälligkeitsdatum von Aufgabe  #123 • Missbrauchshinweis aufnehmen  in Prozess  %18 (NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch) • event-test-workflow auf 15.04.2021 geändert • 14.04.2021, 11:07 Uhr"
    html.assert_text "@Admin User was soll Ich hier tun?"

    html.assert_selector "span", text: "Admin User"
    html.assert_selector 'a[href="https://samarbeid-tests.example.com/my/settings/notifications"]', text: "Einstellungen"

    # Just a random selection of notifications, update as you please...
    html.assert_selector "span", text: "#50 • Patentansprüche formulieren"
    html.assert_selector "span", text: "%11 (Patentanmeldung) • PA-3"
  end

  test "mails should be delivered as plain_text" do
    dsb_user = User.find_by!(email: "dsb@example.org")
    mail = NotificationsMailer.with(user: dsb_user, notifications: dsb_user.unseen_and_unsent_notifications).recent

    assert_equal 0, mail.errors.length

    # Execute below line to update fixture file
    # File.write("test/fixtures/files/plain-text-notification-mail.txt", mail.text_part.body.decoded)

    assert_equal File.read("test/fixtures/files/plain-text-notification-mail.txt"), mail.text_part.body.decoded
  end
end
