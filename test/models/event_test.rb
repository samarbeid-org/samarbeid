require "test_helper"

class EventTest < ActiveSupport::TestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
  end

  test "Event is not destroyed with user" do
    @user.events.destroy_all
    assert @user.events.none?

    event = Events::ChangedDueDateEvent.create!(subject: @user, object: Task.first, data: {new_due_date: Date.today})

    assert_equal 1, @user.reload.events.count

    @user.destroy
    assert_nil event.reload.subject
  end

  test "valid task changed due date event" do
    event = Events::ChangedDueDateEvent.new(subject: @user, object: Task.first, data: {new_due_date: Date.today})
    assert event.valid?
  end

  test "there may be events without real object which are valid" do
    task = Workflow.find(21).all_tasks.last
    Interactors::TaskInteractor.delete(task, @user)

    deleted_event = Event.last

    assert deleted_event.is_a?(Events::DeletedEvent)
    assert deleted_event.valid?
    refute deleted_event.object.persisted? # it is fake
    assert deleted_event.object_id # we can still read raw values
  end

  test "A fixture should exist for every event type" do
    automation = Automation.find_by!(title: "event-test-automation")
    workflow = Workflow.find_by!(title: "event-test-workflow")
    workflow_tasks = workflow.tasks_ordered_in_structure
    workflow_definition = WorkflowDefinition.find_by!(name: "Neu erstellte Vorlage - zum ausprobieren")
    dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    content_item = ContentItem.find(82)
    workflow_deleted = Workflow.new(id: 28)

    comment = Comment.first
    user = User.find_by!(email: "srs_user@example.org")

    # Note: We use only events from above specific workflow as those are also used in system tests
    events = Event.where(object: automation)
      .or(Event.where(object: workflow))
      .or(Event.where(object: workflow_deleted))
      .or(Event.where(object: workflow_tasks))
      .or(Event.where(object: comment))
      .or(Event.where(object: workflow_definition))
      .or(Event.where(object: dossier_definition))
      .or(Event.where(object: content_item))
      .or(Event.where(object: user))
      .or(Event.where(id: Events::FeatureReleasedEvent.first.id))

    fixture_events = events.map { |e| e.read_attribute(:type).demodulize.underscore }.uniq

    available_events = Dir.glob(Rails.root.join("app", "models", "events", "*.rb")).map { |path| File.basename(path, ".rb") }

    assert_empty available_events - fixture_events, "Could not find fixtures for events #{available_events - fixture_events}"
    assert_empty fixture_events - available_events, "Found fixtures for nonexistent events #{available_events - fixture_events}"
  end

  test "A localization should exist for every event type" do
    Events.classes.each do |event_class|
      I18n.translate!(event_class.model_name.i18n_key, scope: [event_class.i18n_scope, :models])
    end
  end

  test "Events should be valid and have a correct localized_object" do
    Event.all.each do |event|
      assert event.valid?, "Event #{event.id}: #{event.errors.full_messages}"

      lo = event.localized_object(true, @user, true, true)

      assert lo.key?(:action), "localized_object should have key :action"
      assert_kind_of String, lo[:action], "localized_object[:action] should be a String"
      refute_empty lo[:action], "localized_object[:action] shouldn't be empty"

      assert lo.key?(:avatar), "localized_object should have key :avatar"
      assert_kind_of Hash, lo[:avatar], "localized_object[:avatar] should be a Hash"
      assert lo[:avatar].key?(:icon) || lo[:avatar].key?(:label) || lo[:avatar].key?(:url), "localized_object[:avatar] should have have key :icon, :label or :url"

      assert lo.key?(:chunks), "localized_object should have key :chunks"
      assert_kind_of Array, lo[:chunks], "localized_object[:chunks] should be an Array"

      lo[:chunks].each do |chunk|
        assert chunk.key?(:text), "chunk should have key :text"
        assert_kind_of String, chunk[:text], "chunk[:text] should be a String"
        refute chunk[:text].starts_with?("translation missing:"), "chunk text should be translated: '#{chunk[:text]}'"
      end
    end
  end

  test "Events should have superseder support" do
    skip("Temporarily disabled for #1277")
    workflow = Workflow.find_by!(title: "PA-1")
    # this Workflow is first unassigned and later assigned
    unassigned_event = workflow.events.where(type: "Events::UnassignedEvent").first
    assigned_event = workflow.events.where(type: "Events::AssignedEvent").first
    assert_not_nil assigned_event
    assert_not_nil unassigned_event

    assert assigned_event.supersedee_candidates.first == unassigned_event

    # this chunk can be removed if we redo the fixtures and have superseders applied there
    assert_nil unassigned_event.superseder_id
    assigned_event.apply_supersedees
    unassigned_event.reload

    assert_not_nil unassigned_event.superseder_id
    assert_equal unassigned_event.superseder, assigned_event
    assert assigned_event.superseder_id.nil?
  end
end
