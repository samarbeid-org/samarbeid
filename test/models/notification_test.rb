require "test_helper"

class NotificationTest < ActiveSupport::TestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
  end

  test "The user who triggered the event should not be notified" do
    notifiable_events = Event.where.not(subject: nil).select { |e| e.respond_to?(:notification_receivers) }
    assert_not_empty notifiable_events

    notifiable_events.each do |event|
      assert_not_includes(event.notification_receivers, event.subject, "Event (#{event.class}, id: #{event.id}) shouldn't include subject user (id: #{event.subject&.id}) in notification_receivers") unless event.triggered_by.eql?(event.subject)
    end
  end

  test "Bookmark a notification should remove done state" do
    notification = @user.notifications.done.first
    notification.bookmark!

    assert notification.bookmarked?
    refute notification.done?
  end

  test "Old done notifications should be deleted after a certain time" do
    old_done_notifications_count = @user.notifications.old_done.count
    assert_operator old_done_notifications_count, :>, 0

    assert_difference -> { @user.notifications.old_done.count }, -old_done_notifications_count do
      Notification.cleanup
    end
  end

  test "Old undone read notifications should get marked after a certain time" do
    @user.notifications.undone.first.update!(read_at: 2.weeks.ago)
    old_read_undone_notifications_count = @user.notifications.undone.old_read.count
    assert_operator old_read_undone_notifications_count, :>, 0

    assert_difference -> { @user.notifications.undone.old_read.count }, -old_read_undone_notifications_count do
      Notification.cleanup
    end
  end
end
