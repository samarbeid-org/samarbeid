require "test_helper"

class MenuEntryTest < ActiveSupport::TestCase
  test "menu entries should have a associated page or an url but not both" do
    menu_entry = MenuEntry.first
    refute_nil menu_entry.page
    assert_nil menu_entry.url
    assert menu_entry.valid?

    menu_entry.url = "example.org"
    refute menu_entry.valid?
    assert menu_entry.errors.details[:page].any?
    assert menu_entry.errors.details[:url].any?

    menu_entry.page = nil
    assert menu_entry.valid?

    menu_entry.url = nil
    refute menu_entry.valid?
    assert menu_entry.errors.details[:page].any?
    assert menu_entry.errors.details[:url].any?
  end

  test "with_location_position scope adds the field location_position to the resulting items" do
    refute MenuEntry.first.respond_to?(:location_position)

    me = MenuEntry.with_location_position.first
    assert me.respond_to?(:location_position)
    assert_equal "#{me.location}_#{me.position}", me.location_position
  end
end
