require "test_helper"

class SearchTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "search should work for new objects" do
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Patentanmeldung"))
    perform_enqueued_jobs { workflow.update!(title: "SuchTestWorkflow") }

    assert workflow, Services::Search.new.fulltext("SuchTestWorkflow").first.searchable
  end

  test "Fulltext Search should work" do
    my_search = Services::Search.new

    assert_not_empty my_search.fulltext
  end

  test "titles should be weighted higher than other attributes" do
    skip
    # ambition = Ambition.find(2) # Blockchain basierte Nutella
    # workflow = Workflow.first
    # perform_enqueued_jobs { Workflow.first.ambitions << ambition }
    #
    # results = Services::Search.new.fulltext("Nutella", {per_page: 1000}).to_a
    #
    # assert results.include?(workflow.search_document) # because it's Ambition is Nutella
  end

  test "workflows should not be found if not in group" do
    private_workflow = Workflow.find(17) # Geburtstagsparty
    srs_user = private_workflow.assignee
    assert Services::Search.new(srs_user).fulltext("Geburtstag").include?(private_workflow.search_document)

    no_access_user = User.find_by(email: "no_access_user@example.org")
    refute Services::Search.new(no_access_user).fulltext("Geburtstag").include?(private_workflow.search_document)
  end

  test "tasks, workflows should be found via their comments" do
    query = "suchTestKommentare"
    results = Services::Search.new.fulltext(query)
    assert results.include?(Task.find(59).search_document)
    assert results.include?(Workflow.find(13).search_document)
  end

  test "tasks, workflows should be found by their identifier" do
    assert Services::Search.new.fulltext("#59").first(5).include?(Task.find(59).search_document)
    assert Services::Search.new.fulltext("%13").first(5).include?(Workflow.find(13).search_document)
  end

  test "search should work with umlaute and other special chars" do
    a_task = Task.first
    perform_enqueued_jobs { a_task.update!(title: "Dies ist ein Text mit Umläutendenerer und anschließendß mit 😉") }

    a_tasks_search_document = a_task.search_document
    assert_equal a_tasks_search_document, Services::Search.new.fulltext("Umläutendenerer").first
    assert_equal a_tasks_search_document, Services::Search.new.fulltext("anschließendß").first

    skip("Postgres fulltext search does not work with emojis (ignores those like blank space)")
    # Check rails db output for the following query:
    # SELECT * FROM ts_debug('german', 'Dies ist ein Text mit Umläutendenerer und anschließendß mit 😉.');
    assert_equal a_tasks_search_document, Services::Search.new.fulltext("😉").first
  end

  test "search should find workflows which reference dossiers" do
    person_referenced_in_workflows = Dossier.find(21) # Gräfin Tristan von der Bormann
    results = Services::Search.new.fulltext("Gräfin Tristan von der Bormann")
    assert_equal person_referenced_in_workflows.search_document, results.first
    assert results.include?(Workflow.find(7).search_document)
    assert results.include?(Workflow.find(16).search_document)
  end

  test "indexing strings with leading/trailing dots and other Elasticsearch limitations should not cause exceptions" do
    dossier_definition = DossierDefinition.find_by(name: "Person")
    dossier_item_definition = dossier_definition.items.find_by(name: "E-Mail")
    dossier_item = dossier_item_definition.dossier_items.first
    dossier_item.update!(value: "must-be-present-to-trigger-indexing@example.org")

    content_item = ContentItem.where(content_type: "string").first
    content_item.update!(value: "must be present to trigger indexing")

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update!(label: ".leading-dot") }
      perform_enqueued_jobs { dossier_item_definition.update!(name: ".leading-dot") }
    end

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update!(label: "a trailing-dot.") }
      perform_enqueued_jobs { dossier_item_definition.update!(name: "a trailing-dot.") }
    end

    assert_nothing_raised do
      perform_enqueued_jobs { content_item.update!(label: "foo..bar.baz") }
      perform_enqueued_jobs { dossier_item_definition.update!(name: "foo..bar.baz") }
    end
  end
end
