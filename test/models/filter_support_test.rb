require "test_helper"

class FilterSupportTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "filter_search_scope adds a scope to query by ILIKE of attributes and/or attributes of associated models" do
    refute TestTask.respond_to?(:with_filter_search)
    TestTask.filter_search_scope :name
    assert TestTask.respond_to?(:with_filter_search)

    assert_equal 2, TestTask.with_filter_search("test").count
    assert TestTask.with_filter_search("test").all? { |t| t.name.downcase.include?("test") }

    TestTask.filter_search_scope :name, associated_against: {workflow: :title}
    assert_equal 25, TestTask.with_filter_search("test").count
    assert TestTask.with_filter_search("test").all? { |t| t.name.downcase.include?("test") || t.workflow.title.downcase.include?("test") }
  end
end

class TestTask < ApplicationRecord
  include FilterSupport
  self.table_name = "tasks"
  belongs_to :workflow
end
