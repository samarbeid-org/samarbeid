require "test_helper"

class TaskItemTest < ActiveSupport::TestCase
  test "task_item may be rendered as data_field" do
    task_item = TaskItem.first
    task_item_as_datafield = task_item.as_datafield

    results = ApplicationController.render(
      template: "api/data_items/_data_item_task",
      formats: [:json],
      handlers: [:jbuilder],
      locals: {data_field: task_item_as_datafield, current_user: User.first}
    )

    assert JSON.parse(results).is_a?(Hash)
    assert_equal task_item.content_item.label, JSON.parse(results)["definition"]["name"]
  end

  test "task_items should show who updated them last" do
    content_item = ContentItem.first
    task_item = content_item.task_items.first

    Interactors::DataItemInteractor.update(task_item, nil, content_item.lock_version, User.first)

    assert task_item.last_data_change
    assert_equal User.first, task_item.last_data_change.subject
  end

  test "task_items may not be added to calenders for unsuitable content_item_types" do
    task = Task.first
    string_content = ContentItem.create!(content_type: ContentTypes::String.type, label: "something", workflow: task.workflow)
    task_item = TaskItem.create!(content_item: string_content, task: task, position: 100)

    task_item.add_to_calendar = true
    refute task_item.valid?

    task_item.add_to_calendar = false
    refute task_item.valid?

    task_item.add_to_calendar = nil
    assert task_item.valid?
  end

  test "task_items may be added to calenders for all suitable content_item_types" do
    task = Task.first
    [ContentTypes::Date, ContentTypes::Datetime].each do |calender_addable_content_types|
      content_item = ContentItem.create!(content_type: calender_addable_content_types.type, label: "something", workflow: task.workflow)
      task_item = TaskItem.create!(content_item: content_item, task: task, position: 100)

      task_item.add_to_calendar = true
      assert task_item.valid?, "For #{calender_addable_content_types}"

      task_item.add_to_calendar = false
      assert task_item.valid?, "For #{calender_addable_content_types}"
    end
  end
end
