require "test_helper"

class WebhookTest < ActiveSupport::TestCase
  setup do
    @task = Task.find(152) # Aufgabe mit allen Feldern
    @user = User.first
  end
  test "webhook should provide body for a task to submit" do
    webhook_body = Webhook.new(task: @task).body
    assert_equal 15, webhook_body[:fields].count
    assert_equal @task.id, webhook_body[:triggered_from_task][:id]

    assert_equal @user.email, Webhook.new(task: @task).body(@user)[:triggered_by]
  end

  test "executing a webhook should update response code and body" do
    webhook = Webhook.new(task: @task, url: "https://example.com", auth_token: "123")
    VCR.use_cassette("webhook") do
      webhook.perform(@user)
    end
    assert_equal 200, webhook.response_code
    assert_not_nil webhook.response_body
  end

  test "completing a task should trigger webhooks and notify task assignee of execution" do
    task = Workflow.active.first.all_tasks.active.first

    Webhook.create!(task: task, url: "https://example.com", auth_token: "123")

    VCR.use_cassette("webhook") do
      assert_difference -> { @user.notifications.count }, 1 do
        task.reload.complete!(@user)
      end
    end

    webhook = task.webhooks.first
    event = Events::SimpleActionEvent.where(object: webhook.task).last

    assert_equal 200, webhook.response_code
    assert_not_nil webhook.response_body
    assert_equal "abgeschlossen und dadurch Webhook ausgeführt: 'Response 200: Everything okay'", event.whats_going_on_translated
  end

  test "triggered webhook should only notify the task assignee if configured to do so" do
    task = Workflow.active.first.all_tasks.active.first
    Webhook.create!(task: task, url: "https://example.com", auth_token: "123", notify_after_execution: false)

    VCR.use_cassette("webhook") do
      assert_difference -> { @user.notifications.count }, 0 do
        task.reload.complete!(@user)
      end
    end
  end

  test "webhook as_task_item" do
    webhook = Webhook.create!(task: @task, url: "https://example.com", auth_token: "123")
    webhook.as_task_item
  end
end
