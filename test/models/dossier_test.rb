require "test_helper"

class DossierTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier = @dossier_definition.items.find_by(name: "String").dossier_items.find_by("value = :val", val: "Die ist ein Teststring".to_json).dossier
  end

  def get_dossier_item_definition_from_name!(name)
    @dossier_definition.items.find_by!(name: name)
  end

  test "it should be possible to create a dossier" do
    data = {
      "String" => "Test String",
      "Text" => "Test Text",
      "Integer" => 123456,
      "Boolean" => true,
      "Date" => "2021-03-07",
      "Richtext" => "<p>Test Richtext</p>"
    }
    field_data = data.map { |k, v| [get_dossier_item_definition_from_name!(k).id, v] }.to_h

    assert_difference -> { Dossier.count } do
      dossier = @dossier_definition.build_dossier(field_data)
      dossier.save!
    end
  end

  test "it should be possible to update the value of a dossier item" do
    dossier = Dossier.find(59)
    dossier_item = dossier.dossier_items.find_by(dossier_item_definition: 10)
    assert_changes -> { dossier_item.value } do
      assert_changes -> { dossier_item.updated_at } do
        assert_changes -> { dossier.updated_at } do
          dossier_item.update!(value: "neuer String")
        end
      end
    end
  end

  test "it should not be possible to create a dossier without definition" do
    dossier_new = Dossier.new(definition: nil)
    assert_not dossier_new.save
    assert dossier_new.errors.details[:definition].any?
  end

  test "it should not be possible to change the definition of a dossier" do
    dossier_definition_new = DossierDefinition.create!(name: "new", groups: Group.all)
    assert_not @dossier.update(definition: dossier_definition_new)
    assert @dossier.errors.details[:definition].any?
  end

  test "it should not be possible to save a dossier without a required field" do
    dossier_item = @dossier.dossier_items.find_by(dossier_item_definition: get_dossier_item_definition_from_name!("String"))
    refute dossier_item.update(value: nil)
    assert dossier_item.errors.details[:value].any?
  end

  test "it should not be possible to save a dossier with a already used value for a unique field" do
    dossier_item_definition = get_dossier_item_definition_from_name!("String")
    dossier_new = @dossier_definition.build_dossier({dossier_item_definition.id => @dossier.get_item_value(dossier_item_definition)})
    refute dossier_new.save
    assert dossier_new.dossier_items.first.errors.details[:value].any?
  end

  test "data_items method should return an array of DataItem objects with correct attributes" do
    data_items = @dossier.data_items
    assert_equal @dossier.definition.items.count, data_items.count
    assert_instance_of Array, data_items

    first_data_item = data_items.first
    assert_instance_of DataItem, first_data_item
    refute_nil first_data_item
    refute_nil first_data_item.item_definition_id
    dossier_item_definition = DossierItemDefinition.find(first_data_item.item_definition_id)
    dossier_item = @dossier.get_item(dossier_item_definition)
    assert_equal dossier_item_definition.name, first_data_item.name
    assert_equal dossier_item_definition.content_type, first_data_item.content_type
    assert_equal dossier_item_definition.options, first_data_item.options
    assert_equal dossier_item_definition.required, first_data_item.required
    assert_equal dossier_item_definition.recommended, first_data_item.recommended
    assert_equal dossier_item_definition.unique, first_data_item.unique
    assert_equal dossier_item.lock_version, first_data_item.lock_version
    assert_equal dossier_item.value, first_data_item.value
  end

  test "title should return a join string of all values of definition title_fields or a default value" do
    assert_equal "Die ist ein Teststring", @dossier.title
    perform_enqueued_jobs do
      @dossier.definition.update!(title_fields: [])
    end
    assert_equal "#{@dossier.id} • #{@dossier.definition.name}", @dossier.reload.title
  end

  test "subtitle should return a join string of all values of definition subtitle_fields or empty string" do
    assert_equal "dev.vuetifyjs.com • admin@example.org • Testtext, mit zweiter Zeile • Richtext mit bold, italic, Zitat • 99 • 99,99 • Ja • 01.03.2021 • 14:45 Uhr • 27.04.2021, 13:25 Uhr • Item 1, Item 3 • Admin User, Some Other User • cairo-multiline.pdf, table-screenshot.png, sample.docx", @dossier.subtitle
    perform_enqueued_jobs do
      @dossier.definition.update!(subtitle_fields: [])
    end
    assert_equal "", @dossier.reload.subtitle
  end

  test "#referenced_in_workflows should return correct workflows" do
    assert_equal [Workflow.find(16)], Dossier.find(24).referenced_in_workflows
  end

  test "Title and subtitle of a dossier should be updated from field values on create and update" do
    dossier, name_item, email_item, organisation_item = create_person_dossier("Create Name", "create@example.org")

    assert_equal "Create Name", dossier.title
    assert_equal "create@example.org", dossier.subtitle

    dossier.dossier_items.find_by!(dossier_id: dossier.id, dossier_item_definition_id: name_item.id).update!(value: "Update Name")
    dossier.dossier_items.find_by!(dossier_id: dossier.id, dossier_item_definition_id: email_item.id).update!(value: "update@example.org")
    dossier.dossier_items.find_by!(dossier_id: dossier.id, dossier_item_definition_id: organisation_item.id).update!(value: "Update Organisation")

    assert_equal "Update Name", dossier.title
    assert_equal "update@example.org • Update Organisation", dossier.subtitle
  end

  test "Changing title_fields and subtitle_fields of dossier definition should update title and subtitle" do
    dossier, _, email_item, _ = create_person_dossier("Create Name", "create@example.org", "Create Organisation")

    assert_equal "Create Name", dossier.title
    assert_equal "create@example.org • Create Organisation", dossier.subtitle

    perform_enqueued_jobs do
      dossier.definition.update!(title_fields: [], subtitle_fields: [email_item.id])
    end

    assert_equal "#{dossier.id} • #{dossier.definition.name}", dossier.reload.title
    assert_equal "create@example.org", dossier.subtitle

    perform_enqueued_jobs do
      dossier.definition.update!(subtitle_fields: [])
    end

    assert "", dossier.reload.subtitle
  end

  test "Updating the name of a dossier definition without title_fields should update dossier titles to fallback" do
    dossier, _ = create_person_dossier("Create Name", "create@example.org")

    perform_enqueued_jobs do
      dossier.definition.update!(title_fields: [])
    end
    assert_equal "#{dossier.id} • #{dossier.definition.name}", dossier.reload.title

    refute_equal "Test", dossier.definition.name
    perform_enqueued_jobs do
      dossier.definition.update!(name: "Test")
    end

    assert_equal "#{dossier.id} • Test", dossier.reload.title
  end

  test "Updating or deleting a referenced dossier should update title and subtitle of all dossiers where it is referenced" do
    referenced_dossier = Dossier.find(1)
    assert_equal "Admin User", referenced_dossier.title

    assert_equal "Die ist ein Teststring", @dossier.title

    dossier_item_definition = @dossier_definition.items.find_by!(content_type: ContentTypes::Dossier.to_s)
    new_title_fields = @dossier_definition.title_fields
    new_title_fields << dossier_item_definition.id

    perform_enqueued_jobs do
      @dossier_definition.update!(title_fields: new_title_fields)
    end
    assert_equal "Die ist ein Teststring • Admin User, Some Other User", @dossier.reload.title

    dossier_field = referenced_dossier.dossier_items.joins(:dossier_item_definition).find_by!(dossier_item_definition: {name: "Name"})
    perform_enqueued_jobs do
      dossier_field.update!(value: "Testname")
    end
    assert_equal "Die ist ein Teststring • Testname, Some Other User", @dossier.reload.title

    perform_enqueued_jobs do
      referenced_dossier.destroy!
    end
    assert_equal "Die ist ein Teststring • Some Other User", @dossier.reload.title
  end

  private

  def create_person_dossier(name, email, organisation = nil)
    dossier_definition = DossierDefinition.find_by!(name: "Person")
    name_item = dossier_definition.items.find_by!(name: "Name")
    email_item = dossier_definition.items.find_by!(name: "E-Mail")
    organisation_item = dossier_definition.items.find_by!(name: "Organisation")

    data_fields = {name_item.id => name, email_item.id => email}
    data_fields[organisation_item.id] = organisation if organisation.present?

    dossier = dossier_definition.build_dossier(data_fields)
    dossier.save!

    [dossier, name_item, email_item, organisation_item]
  end
end
