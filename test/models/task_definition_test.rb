require "test_helper"

class TaskTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    @user = User.find_by!(email: "dsb@example.org")
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
  end

  test "candidate_assignees should only be settable by one method at a time" do
    task_definition = @workflow_definition.direct_task_definitions.first

    task_definition.candidate_assignee = @user
    assert task_definition.valid?

    task_definition.candidate_assignee_from_workflow = true
    refute task_definition.valid?

    task_definition.candidate_assignee = nil
    assert task_definition.valid?
  end
end
