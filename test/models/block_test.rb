require "test_helper"

class BlockTest < ActiveSupport::TestCase
  def setup
    @workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch"))
  end

  test "title should have default and may be set to something specific" do
    block = Block.new
    assert block.title.present?

    block.title = "test-title"
    assert_equal "test-title", block.title
  end

  test "block with condition should be activated only when condition is confirmed" do
    block = @workflow.blocks.first
    condition_item = block.content_item
    task_for_condition_item = condition_item.task_items.first.task
    task_for_condition_item.previous_items.each { |task| start_and_complete(task) }

    assert task_for_condition_item.reload.active?
    assert block.created?
    condition_item.update(value: block.decision) # would trigger block
    task_for_condition_item.skip! # condition remains unconfirmed -> block not triggered

    assert block.reload.skipped?

    task_for_condition_item.reopen!
    start_and_complete(task_for_condition_item)

    assert block.reload.active?
  end

  test "subsequent blocks should only be activated one by one" do
    first_block, second_block = @workflow.blocks.sort_by(&:position)[0..1]
    # Ensure both blocks have same condition and condition is fullfilled
    second_block.update!(content_item: first_block.content_item, decision: first_block.decision)
    first_block.content_item.update(value: first_block.decision)

    # Complete all tasks previous of block
    first_block.previous_items.each { |task| start_and_complete(task) }

    assert first_block.reload.active?
    refute second_block.reload.active?

    # Complete first block
    first_block.direct_tasks.each { |task| start_and_complete(task) }

    assert first_block.reload.completed?
    assert second_block.reload.active?
  end

  test "block following skipped block should be activated right away" do
    first_block, second_block = @workflow.blocks.sort_by(&:position)[0..1]

    assert_equal first_block.content_item, second_block.content_item
    refute_equal first_block.decision, second_block.decision

    second_block.content_item.update(value: second_block.decision)

    # Complete all tasks previous of first block
    first_block.previous_items.each { |task| start_and_complete(task) }

    assert first_block.reload.skipped?
    assert second_block.reload.active?
  end

  test "block decision can also be a selection and can activate three different blocks" do
    workflow = Workflow.find_by(title: "Test Blöcke aktivieren mit Auswahlliste")

    left_block, middle_block, right_block = workflow.blocks

    task_with_condition_item = workflow.direct_tasks.first
    condition_item = task_with_condition_item.task_items.first.content_item

    assert_equal ContentTypes::Selection, condition_item.content_type

    [left_block, middle_block, right_block].each do |block|
      condition_item.reload.update(value: block.decision)
      task_with_condition_item.reopen! unless task_with_condition_item.open?
      start_and_complete(task_with_condition_item)

      assert block.reload.active?
      other_blocks = workflow.blocks - [block]
      other_blocks.each { |other_block| refute other_block.reload.active? }
    end
  end

  private

  def start_and_complete(task)
    task.reload
    task.enable! if task.may_enable?
    task.start! if task.may_start?
    task.complete!
  end
end
