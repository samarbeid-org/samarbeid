require "test_helper"

class SimpleActionEventTest < ActiveSupport::TestCase
  setup do
    @admin = User.find_by!(email: "admin@example.org")
    @current_user = User.find_by!(email: "team-admin@example.org")
    @user = User.find_by!(email: "dsb@example.org")
  end

  test "simple action event should track a action and localize it" do
    event = Events::SimpleActionEvent.create!(subject: @current_user, object: @user, whats_going_on: :promoted_to_admin)

    assert_equal :promoted_to_admin, event.data[:whats_going_on]
    assert_equal "die Admin Rolle zugewiesen", event.whats_going_on_translated
    assert_equal event.whats_going_on_translated, event.new_value
  end

  test "simple actions events have a default action if none is given" do
    event = Events::SimpleActionEvent.create!(object: @user)

    assert_equal "bearbeitet", event.whats_going_on_translated
  end

  test "simple action event should localize unknown actions in a human readable way" do
    event = Events::SimpleActionEvent.create!(object: @user, whats_going_on: :something_really_new)

    assert_equal "something really new", event.whats_going_on_translated
  end

  test "simple actions may localize with variables which are interpreted dynamically" do
    task = Task.last
    workflow = Workflow.find(7)
    event = Events::SimpleActionEvent.new(subject: @user, object: task,
      whats_going_on: {moved_task_to_other_process: {process: workflow}})

    assert_equal "aus Prozess '%7 (Bestimmung Externer Bedarfe) • EB-7' hierher verschoben", event.whats_going_on_translated

    workflow.update!(title: "I now have a different name")

    assert_equal "aus Prozess '%7 (Bestimmung Externer Bedarfe) • I now have a different name' hierher verschoben", event.whats_going_on_translated

    event.save!
    workflow.destroy!

    assert_equal "aus Prozess 'nicht mehr verfügbar' hierher verschoben", event.reload.whats_going_on_translated
  end

  test "simple actions may localize with strings provided on creation" do
    task = Task.first
    process = "I am a special string process"
    event = Events::SimpleActionEvent.new(subject: @user, object: task,
      whats_going_on: {moved_task_to_other_process: {process: process}})

    assert_equal "aus Prozess 'I am a special string process' hierher verschoben", event.whats_going_on_translated
  end

  test "simple actions which expect variables for localization still work if those are not present" do
    task = Task.first
    event = Events::SimpleActionEvent.new(subject: @user, object: task,
      whats_going_on: {moved_task_to_other_process: {mistyped_variable: "hey"}})

    assert_equal "aus Prozess '%{process}' hierher verschoben", event.whats_going_on_translated
  end

  test "simple actions may notify admin users but not the one who is subject" do
    assert_changes -> { @current_user.notifications.count } do
      assert_no_changes -> { @admin.notifications.count } do
        Events::SimpleActionEvent.create(subject: @admin, object: @user, notify: :admins)
      end
    end
  end

  test "simple actions may notify the subject for automated actions" do
    assert_changes -> { @admin.notifications.count } do
      Events::SimpleActionEvent.create(subject: @admin, object: @user, notify: :subject)
    end
  end

  test "simple actions may notify specific users (but not the one who is subject)" do
    assert_changes -> { @user.notifications.count } do
      assert_no_changes -> { @admin.notifications.count } do
        Events::SimpleActionEvent.create(subject: @admin, object: @user, notify: [@user, @admin])
      end
    end
  end

  test "simple actions by default notify nobody" do
    event = Events::SimpleActionEvent.create(object: @user)

    assert_empty event.notification_receivers
  end

  test "simple actions have a list of possible notification groups (users)" do
    possible_receivers = Events::SimpleActionEvent.new.send(:possible_notification_receivers)
    assert possible_receivers.keys.all? { |key| key.is_a?(Symbol) }
    assert possible_receivers.values.all? { |receivers| receivers.to_a.all? { |user| user.is_a?(User) } }
  end

  test "simple actions may set action and notification group at once" do
    event = Events::SimpleActionEvent.create(subject: @current_user, object: @user, whats_going_on: :demoted_to_user, notify: :admins)

    refute_empty event.notification_receivers
    assert_equal "die Admin Rolle entfernt", event.new_value
  end
end
