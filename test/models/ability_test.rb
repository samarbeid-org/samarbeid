require "test_helper"

class AbilityTest < ActiveSupport::TestCase
  def setup
    @personas = ["no_access_user", "member_of_group", "member_of_another_group", "contributor_but_not_member_of_group"].map { |persona_name| create_user(persona_name) }
    @a_new_group = Group.create!(name: "a fresh new group")
    @a_new_group.users << User.find_by_email!("member_of_group@example.com")
    @a_different_group = Group.create!(name: "a group which has no access to our objects")
    @a_different_group.users << User.find_by_email!("member_of_another_group@example.com")

    workflow_def_belonging_to_group = WorkflowDefinition.first
    workflow_def_belonging_to_group.groups << @a_new_group
    @workflow = create_workflow_for!(workflow_def_belonging_to_group)
    @task = @workflow.direct_tasks.first

    # This should actually never happen but just in case we include it in our tests
    @workflow.contributors << User.find_by_email!("contributor_but_not_member_of_group@example.com")
    @task.contributors << User.find_by_email!("contributor_but_not_member_of_group@example.com")

    # We used to have huge CSV Tables defining all kinds of rights. Now a few examples suffice to test our ACL
    # Currently (and it should stay this way) ACL for workflows and tasks are identical!
    @acl_rules = <<~ACL_DEFINITIONS
      ACTION,	NO_ACCESS_USER,	MEMBER_OF_GROUP,	MEMBER_OF_ANOTHER_GROUP,  CONTRIBUTOR_BUT_NOT_MEMBER_OF_GROUP
      create,	x,	            yes,	            x,                        x
      read,	  x,	            yes,	            x,                        x
      update,	x,	            yes,	            x,                        x
      delete,	x,	            yes,	            x,                        x
      manage,	x,	            yes,	            x,                        x
    ACL_DEFINITIONS
  end

  test "workflow acl" do
    test_all_acl_rules_for(@acl_rules, @personas, @workflow)
  end

  test "task acl" do
    test_all_acl_rules_for(@acl_rules, @personas, @task)
  end

  test "dossier acl" do
    dossier_def_belonging_to_group = DossierDefinition.first
    dossier_def_belonging_to_group.groups << @a_new_group
    @dossier = dossier_def_belonging_to_group.dossiers.first

    test_all_acl_rules_for(@acl_rules, @personas, @dossier)
  end

  private

  def create_user(name)
    User.create!(email: "#{name}@example.com", lastname: name, firstname: "first name", password: "password", confirmed_at: Time.current)
  end

  def test_all_acl_rules_for(acl_rules, personas, resource)
    parse_options = {
      headers: true, col_sep: ",",
      header_converters: ->(f) { f&.strip }, converters: ->(f) { f&.strip } # to cleanup our "nice" inline formatting
    }
    CSV.parse(acl_rules, **parse_options).each_with_index do |row, line_count|
      next unless row["ACTION"].present?
      personas.each do |persona|
        raise "Persona #{persona.lastname.upcase} missing!" unless row[persona.lastname.upcase]

        can_or_cannot = (row[persona.lastname.upcase] == "yes") ? :can? : :cannot?
        action = row["ACTION"].to_sym
        acl_rule_under_test = "ACL für #{persona.lastname} #{can_or_cannot} #{action} #{row["RESOURCE"]} nicht korrekt (definiert in: #{acl_rules}:#{line_count + 2})"
        assert persona.send(can_or_cannot, action, resource), acl_rule_under_test
      end
    end
  end
end
