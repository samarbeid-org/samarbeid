require "test_helper"

class WebhookDefinitionTest < ActiveSupport::TestCase
  test "webhook definition builds a webhook" do
    task_definition = Task.find(152).task_definition # Aufgabe mit allen Feldern
    WebhookDefinition.create!(task_definition: task_definition, url: "https://example.com", auth_token: "123")
    new_workflow = task_definition.workflow_definition.build_workflow
    new_workflow.save!

    new_workflow.all_tasks.find { |t| t.task_definition_id == task_definition.id }.tap do |new_task|
      assert new_task.webhooks.any?
      assert_equal "https://example.com", new_task.webhooks.first.url
      assert_equal "123", new_task.webhooks.first.auth_token
    end
  end
end
