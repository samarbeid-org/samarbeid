require "test_helper"

class TaskItemDefinitionTest < ActiveSupport::TestCase
  test "task_item_definitions may not be added to calenders for unsuitable content_item_types" do
    task_def = TaskDefinition.first
    string_content_def = ContentItemDefinition.create!(content_type: ContentTypes::String.type, label: "something", workflow_definition: task_def.workflow_definition)
    task_item_def = TaskItemDefinition.create!(content_item_definition: string_content_def, task_definition: task_def, position: 100, workflow_definition: task_def.workflow_definition)

    task_item_def.add_to_calendar = true
    refute task_item_def.valid?

    task_item_def.add_to_calendar = false
    refute task_item_def.valid?

    task_item_def.add_to_calendar = nil
    assert task_item_def.valid?
  end

  test "task_item_definitions may be added to calenders for all suitable content_item_types" do
    task_def = TaskDefinition.first
    [ContentTypes::Date, ContentTypes::Datetime].each do |calender_addable_content_types|
      content_item_def = ContentItemDefinition.create!(content_type: calender_addable_content_types.type, label: "something", workflow_definition: task_def.workflow_definition)
      task_item_def = TaskItemDefinition.create!(content_item_definition: content_item_def, task_definition: task_def, position: 100, workflow_definition: task_def.workflow_definition)

      task_item_def.add_to_calendar = true
      assert task_item_def.valid?, "For #{calender_addable_content_types}"

      task_item_def.add_to_calendar = false
      assert task_item_def.valid?, "For #{calender_addable_content_types}"
    end
  end
end
