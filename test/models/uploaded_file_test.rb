require "test_helper"

class UploadedFileTest < ActiveJob::TestCase
  test "pdf files should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")
    assert_equal "Hello World From James", uploaded_file.file_as_text
  end

  test "umlaute in pdfs should be convereted correctly" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/test-umlaute.pdf")
    assert_equal "Test Umläute!", uploaded_file.file_as_text
  end

  test "docx files should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/sample.docx")
    assert uploaded_file.file_as_text.match("The quick brown fox jumped over the lazy cat.")
  end

  test "image files should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/table-screenshot.png")
    assert_equal "Beschreibung Basis Projekt Kick-Off, Zwischenergebnisse und Endprasentation als jeweils " \
      "2 stiindiges Treffen Vor-Ort im ‘oder falls gewinscht in den Raumen des Tellnahme von mindestens zwei " \
      "Personen des Auftragnehmers. \\Vorbereitung, Durchfuhrung, Nachbereitung der Treffen, Abstimmung zwischen " \
      "den Treffen", uploaded_file.file_as_text
  end

  test "pdf files with normal text and text as image should be converted to text" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/normal-text-and-text-as-image.pdf")
    assert_equal "This is a simple text.", uploaded_file.file_as_text
    perform_enqueued_jobs
    assert_equal "This is a simple text. This is a text as an image", uploaded_file.reload.file_as_text
  end

  test "uploaded files should be found on dossier via search" do
    did = DossierItemDefinition.find_by!(content_type: ContentTypes::File.to_s)
    dossier = did.definition.dossiers.first

    refute_equal SearchDocument.where(searchable_type: "Dossier").fulltext("This is a simple text.").first.searchable, dossier
    refute_equal SearchDocument.where(searchable_type: "Dossier").fulltext("This is a simple text. This is a text as an image").first.searchable, dossier

    uploaded_file = create_uploaded_file_with("test/fixtures/files/normal-text-and-text-as-image.pdf")
    dossier_item = dossier.dossier_items.find_by(dossier_item_definition: did)
    refute_nil dossier_item
    dossier_item.update!(value: [uploaded_file.id])

    assert_equal "This is a simple text.", uploaded_file.file_as_text
    perform_enqueued_jobs only: ExtractTextFromFileJob
    assert_equal "This is a simple text. This is a text as an image", uploaded_file.reload.file_as_text
    perform_enqueued_jobs

    assert_equal SearchDocument.where(searchable_type: "Dossier").fulltext("This is a simple text. This is a text as an image").first.searchable, dossier
  end

  test "uploaded files should be found on workflows via search" do
    content_item = ContentItem.find(168)
    workflow = content_item.workflow

    refute_includes SearchDocument.where(searchable_type: "Workflow").fulltext("This is a simple text.").map, workflow.search_document
    refute_includes SearchDocument.where(searchable_type: "Workflow").fulltext("This is a simple text. This is a text as an image"), workflow.search_document

    uploaded_file = create_uploaded_file_with("test/fixtures/files/normal-text-and-text-as-image.pdf")
    content_item.update!(value: uploaded_file.id)

    assert_equal "This is a simple text.", uploaded_file.file_as_text
    perform_enqueued_jobs only: ExtractTextFromFileJob
    assert_equal "This is a simple text. This is a text as an image", uploaded_file.reload.file_as_text
    perform_enqueued_jobs

    assert_includes SearchDocument.where(searchable_type: "Workflow").fulltext("This is a simple text. This is a text as an image"), workflow.search_document
  end

  test "to_text should handle huge files gracefully and limit text length" do
    Tempfile.create do |huge_file|
      # Temp bugfix (use "x" instead of "X") for: https://bugs.launchpad.net/ubuntu/+source/file/+bug/2079979
      huge_file << "x" * 6_000_000
      uploaded_file = create_uploaded_file_with(huge_file.path)
      assert_equal "xxxx [length limited to 5 million chars]", uploaded_file.file_as_text.last(40)
    end
  end

  test "title should be extracted from filename" do
    filename = "cairo-multiline.pdf"
    uploaded_file = create_uploaded_file_with("test/fixtures/files/#{filename}")
    assert_equal filename, uploaded_file.title
  end

  test "title may be set to custom value" do
    uploaded_file = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")
    a_new_title = "this seems a nicer title!"
    uploaded_file.update(title: a_new_title)
    assert_equal a_new_title, uploaded_file.reload.title
  end

  test "file size should be limited" do
    Tempfile.create do |gigantic_file|
      gigantic_file << `head -c 104857600 /dev/urandom`
      uploaded_file = create_uploaded_file_with(gigantic_file.path)
      refute uploaded_file.valid?
    end
  end

  test "invalid_records should return correct orphaned uploaded files" do
    uploaded_file_dossier = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")
    dossier = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS").dossiers.first
    dossier_item_definition = DossierItemDefinition.find(30)
    dossier_item = dossier.dossier_items.find_by(dossier_item_definition: dossier_item_definition)
    refute_nil dossier_item
    dossier_item.update!(value: [uploaded_file_dossier.id])
    assert_equal uploaded_file_dossier, dossier.get_item_value(dossier_item_definition).first
    assert File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_dossier.file.key))

    uploaded_file_task = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")
    workflow = Workflow.find(26)
    content_item = workflow.content_items.find_by(content_type: ContentTypes::File.to_s)
    content_item.update!(value: [uploaded_file_task.id])
    assert_equal uploaded_file_task, content_item.reload.value.first
    assert File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_task.file.key))

    uploaded_file_task_definition = create_uploaded_file_with("test/fixtures/files/cairo-multiline.pdf")
    workflow_definition = WorkflowDefinition.find(10)
    content_item_definition = workflow_definition.content_item_definitions.find_by(content_type: ContentTypes::File.to_s)
    content_item_definition.update!(default_value: [uploaded_file_task_definition.id])
    assert_equal uploaded_file_task_definition, content_item_definition.reload.default_value.first
    assert File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_task_definition.file.key))

    perform_enqueued_jobs do
      UploadedFile.invalid_records.each(&:destroy)
    end

    assert UploadedFile.exists?(uploaded_file_dossier.id)
    assert File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_dossier.file.key))

    assert UploadedFile.exists?(uploaded_file_task.id)
    assert File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_task.file.key))

    assert UploadedFile.exists?(uploaded_file_task_definition.id)
    assert File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_task_definition.file.key))

    dossier.destroy
    assert dossier.destroyed?
    workflow.destroy
    assert workflow.destroyed?
    content_item_definition.destroy
    assert content_item_definition.destroyed?

    perform_enqueued_jobs do
      UploadedFile.invalid_records.each(&:destroy)
    end

    refute UploadedFile.exists?(uploaded_file_dossier.id)
    refute File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_dossier.file.key))

    refute UploadedFile.exists?(uploaded_file_task.id)
    refute File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_task.file.key))

    refute UploadedFile.exists?(uploaded_file_task_definition.id)
    refute File.exist?(ActiveStorage::Blob.service.path_for(uploaded_file_task_definition.file.key))
  end

  private

  def create_uploaded_file_with(file_path)
    uploaded_file = UploadedFile.create!
    File.open(file_path, "rb") do |pdf_test_file|
      uploaded_file.file.attach(io: pdf_test_file, filename: file_path.split("/").last)
    end
    uploaded_file
  end
end
