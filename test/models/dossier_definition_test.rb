require "test_helper"

class DossierDefinitionTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "Person")
  end

  test "it should be possible to create a dossier definition" do
    assert_difference -> { DossierDefinition.count } do
      DossierDefinition.create(name: "test", groups: Group.all)
    end
  end

  test "it should not be possible to save a dossier definition without name" do
    assert_not @dossier_definition.update(name: "")
    assert @dossier_definition.errors.details[:name].any?
  end

  test "it should not be possible to create a dossier definition with an empty name" do
    dossier_definition_new = DossierDefinition.create(name: "")
    assert dossier_definition_new.errors.details[:name].any?
    refute dossier_definition_new.valid?
  end

  test "dossier definition names should be unique" do
    dossier_definition_new = DossierDefinition.create(name: "Person")
    assert dossier_definition_new.errors.details[:name].any?
    refute dossier_definition_new.valid?
  end

  test "dossier definition should have a name for rails admin" do
    assert_not_empty @dossier_definition.name
  end

  test "it should not be possible to add a non existing field to title_fields or to set it to a value other than an array" do
    @dossier_definition.title_fields << "non_existing_field_id"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:title_fields].any?
    @dossier_definition.errors.clear

    @dossier_definition.title_fields = "email"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:title_fields].any?
  end

  test "it should not be possible to add a non existing field to subtitle_fields or to set it to a value other than an array" do
    @dossier_definition.subtitle_fields << "non_existing_field_id"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:subtitle_fields].any?
    @dossier_definition.errors.clear

    @dossier_definition.subtitle_fields = "email"
    assert_not @dossier_definition.save
    assert @dossier_definition.errors.details[:subtitle_fields].any?
  end

  test "it shouldn't be possible to delete a dossier definition which is uses in other dossier definitions or workflow definitions" do
    refute @dossier_definition.destroy
    assert_equal 10, @dossier_definition.errors[:base].size
  end

  test "build_dossier should create dossier_items for all defined dossier_item_definitions with default_value" do
    dossier_item_definition = @dossier_definition.items.find_by!(content_type: ContentTypes::String.to_s)
    dossier_item_definition.update!(default_value: "Testvalue")
    dossier = @dossier_definition.build_dossier
    assert_equal @dossier_definition.items.pluck(:id, :default_value).sort_by { |val| val[0] },
      dossier.dossier_items.map { |di| [di.dossier_item_definition_id, di.value] }.sort_by { |val| val[0] }
  end

  test "build_dossier with required_and_recommended_items_only should create only required and recommended dossier_items" do
    dossier = @dossier_definition.build_dossier(required_and_recommended_items_only: true)
    assert_equal @dossier_definition.items.required_or_recommended.pluck(:id).sort, dossier.dossier_items.map(&:dossier_item_definition_id).sort
  end
end
