require "test_helper"

class PageTest < ActiveSupport::TestCase
  test "slug shouln't be a number" do
    page = Page.first
    page.slug = 1
    refute page.valid?
    assert page.errors.details[:slug].any?
  end
end
