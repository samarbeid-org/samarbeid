require "test_helper"

class NewEngineTest < ActiveSupport::TestCase
  def setup
    @user = User.first
    @workflow = create_workflow_for!(WorkflowDefinition.find(9)) # Bürgeranfrage

    @decision_task = @workflow.items.find { |i| i.title == "Ersteinschätzung" }

    @yes_block = @workflow.blocks.find { |b| b.title == "Weiterleitung: Ja" }
    @no_block = @workflow.blocks.find { |b| b.title == "Weiterleitung: Nein" }

    @selection_block = @workflow.blocks.find { |b| b.title == "Kategorie B oder C" }
  end

  test "when condition is fulfilled block should be activated and following block remain untouched (created)" do
    refute @yes_block.active?
    refute @no_block.active?

    complete_decision_task_as(true)

    assert @yes_block.reload.active?
    assert @no_block.reload.created?
  end

  test "when condition is not fulfilled block should be skipped and following block activated if it has opposite condition" do
    refute @yes_block.active?
    refute @no_block.active?

    complete_decision_task_as(false)

    assert @yes_block.reload.skipped?
    assert @no_block.reload.active?
  end

  test "condition based on selection fields should activate block if fulfilled" do
    refute @selection_block.active?

    @workflow.content_items.find { |ci| ci.label == "Kategorie" }.update(value: 3)
    start_and_complete(@decision_task)

    assert @selection_block.reload.active?
  end

  test "condition based on selection fields should not activate block if not fulfilled" do
    refute @selection_block.active?

    @workflow.content_items.find { |ci| ci.label == "Kategorie" }.update(value: 1)
    start_and_complete(@decision_task)

    assert @selection_block.reload.skipped?
  end

  test "tasks in a parallel block should be enabled when block activated" do
    @yes_block.update(parallel: true)
    complete_decision_task_as(true)
    assert @yes_block.direct_tasks.all?(&:active?)
  end

  test "tasks in a linear block should be enabled one by one" do
    @yes_block.update(parallel: false)
    complete_decision_task_as(true)
    first_task, second_task = @yes_block.direct_tasks
    assert first_task.active?
    assert second_task.created?
    first_task.complete!

    assert second_task.reload.active?
  end

  test "block should be completed if all tasks closed" do
    complete_decision_task_as(true)
    assert @yes_block.reload.active?
    @yes_block.direct_tasks.each { |task| start_and_complete(task) }

    assert @yes_block.reload.completed?
  end

  test "block should be reopened if a task is reopened" do
    complete_decision_task_as(true)
    assert @yes_block.reload.active?
    @yes_block.direct_tasks.each { |task| start_and_complete(task) }

    assert @yes_block.reload.completed?

    @yes_block.direct_tasks.first.reopen!

    assert @yes_block.reload.active?
  end

  test "if block is first item, it's first task should be active?when workflow is activated" do
    definition = WorkflowDefinition.find(9)
    definition.direct_task_definitions.destroy_all # Remove all direct tasks
    definition.block_definitions.order(:position).first.update!(decision: nil, content_item_definition: nil) # Block has no longer a condition
    workflow = create_workflow_for!(definition)
    first_block = workflow.items.first

    assert first_block.active?

    assert first_block.direct_tasks.first.active?
  end

  test "workflow shouldn't be completable if any active task exists" do
    refute @workflow.completable?
    complete_decision_task_as(true)
    refute @workflow.reload.completable?
    @yes_block.direct_tasks.each { |task| start_and_complete(task) }
    assert @workflow.reload.completable?

    change_decision_task_to(false)
    refute @workflow.reload.completable?

    change_decision_task_to(true)
    refute @workflow.reload.completable?

    @no_block.reload.direct_tasks.active.each { |task| start_and_complete(task) }
    assert @workflow.reload.completable?

    @yes_block.direct_tasks.first.reopen!
    refute @workflow.reload.completable?
  end

  test "triggering tasks should be correct if tasks are started/skipped out of order" do
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Veranstaltungsplanung"))
    first_task, second_task, third_task, forth_task, fifth_task, _other_tasks = workflow.items

    assert first_task.active?
    assert second_task.created?

    # Start Task out-of-order
    forth_task.start!

    assert first_task.reload.active?
    assert second_task.reload.created?
    assert third_task.reload.created?
    assert forth_task.reload.active?
    assert fifth_task.reload.created?

    first_task.complete!
    assert first_task.reload.completed?
    assert second_task.reload.active?
    assert third_task.reload.created?
    assert forth_task.reload.active?
    assert fifth_task.reload.created?

    forth_task.complete!
    assert first_task.reload.completed?
    assert second_task.reload.active?
    assert third_task.reload.created?
    assert forth_task.reload.completed?
    assert fifth_task.reload.created?
  end

  test "we may print a workflows structure for debugging" do
    engine = Services::NewEngine.new(@workflow)

    assert engine.structure_to_s.present?

    expected_structure = <<~STRUCTURE
      Bürgeranfrage [active]
      - Ersteinschätzung [active]
      -- F: 
      -- F: E-Mail-Adresse Absender
      -- F: Text der E-Mail
      -- F: Weiterleitung an anderen Fachpolitiker*in?
      -- F: Testdatum
      -- F: Kategorie
      - B: Weiterleitung: Ja [created]
      -- Weiterleitung Fachpolitiker*in [created]
      --- F: 
      --- I: Text der E-Mail
      --- F: Notizen
      --- F: Fachpolitiker*in
      -- Kurzinformation an Bürger*in [created]
      --- F: 
      --- I: Text der E-Mail
      --- I: Vorher kein Label vorhanden 1
      - B: Weiterleitung: Nein [created]
      -- Vorbereitung Antwortschreiben [created]
      --- F: 
      --- I: E-Mail-Adresse Absender
      --- I: Text der E-Mail
      --- F: Antwortentwurf
      -- Versendung Antwortschreiben [created]
      --- F: 
      --- I: E-Mail-Adresse Absender
      --- I: Text der E-Mail
      --- I: Antwortentwurf
      --- F: Notizen zu Bürgerantwort
      - B: Kategorie B oder C [created]
      -- Aufnahme ins Wiki [created]
    STRUCTURE
    assert_equal expected_structure, engine.structure_to_s

    @decision_task.destroy!
    @yes_block.items.first.destroy!
    @no_block.items.each(&:destroy!)

    expected_structure = <<~STRUCTURE
      Bürgeranfrage [completed]
      - B: Weiterleitung: Ja [skipped]
      -- Kurzinformation an Bürger*in [created]
      --- F: 
      --- I: Text der E-Mail
      --- I: Vorher kein Label vorhanden 1
      - B: Weiterleitung: Nein [skipped]
      - B: Kategorie B oder C [skipped]
      -- Aufnahme ins Wiki [created]
    STRUCTURE
    assert_equal expected_structure, Services::NewEngine.new(@workflow.reload).structure_to_s
  end

  test "destroying all tasks should make a workflow completable" do
    @workflow.reload.all_tasks.each(&:destroy!)
    assert @workflow.reload.completable?
  end

  test "workflow items should only return non-destroyed tasks and blocks" do
    @workflow.reload.all_tasks.each(&:destroy!)
    @workflow.reload.blocks.each(&:destroy!)
    assert_empty @workflow.reload.items
  end

  test "deleting first task should make way for other task to be enabled" do
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Veranstaltungsplanung"))
    first_task, second_task, _other_tasks = workflow.items

    first_task.destroy!

    assert second_task.reload.active?
  end

  test "deleting any task should make way for other task to be enabled" do
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Veranstaltungsplanung"))
    first_task, second_task, third_task, _other_tasks = workflow.items

    second_task.destroy!
    first_task.complete!

    assert third_task.reload.active?
  end

  test "block should be completed if all tasks closed even when some are deleted" do
    complete_decision_task_as(true)
    assert @yes_block.reload.active?
    first_task, second_task = @yes_block.items
    first_task.destroy!
    start_and_complete second_task

    assert @yes_block.reload.completed?
  end

  test "task after empty block should be started automatically" do
    workflow = Workflow.find(29)
    task_before_empty_block, task_behind_empty_block = workflow.direct_tasks
    start_and_complete(task_before_empty_block)
    assert task_behind_empty_block.reload.active?
  end

  private

  def complete_decision_task_as(decision = true)
    @decision_task.task_items.find(&:required).content_item.update(value: decision)
    start_and_complete(@decision_task)
  end

  def change_decision_task_to(new_decision)
    @decision_task.reopen! if @decision_task.may_reopen?
    @decision_task.task_items.find(&:required).content_item.update(value: new_decision)
    @decision_task.complete!
  end

  def start_and_complete(task)
    task.start! if task.may_start?
    task.complete!
  end
end
