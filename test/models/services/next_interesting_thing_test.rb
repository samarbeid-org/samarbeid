require "test_helper"

class Services::NextInterestingThingTest < ActiveSupport::TestCase
  def setup
    @user = User.find_by!(email: "dsb-boss@example.org")
    @other_user = User.find_by!(email: "dsb@example.org")
    @workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch"))

    @first_task = @workflow.direct_tasks.first
    @last_task = @workflow.direct_tasks.last
  end

  test "should work for completable workflow" do
    @workflow.all_tasks.update_all(aasm_state: "completed")
    assert_nil Services::NextInterestingThing.new(@workflow.reload, @user).next_station
  end

  test "should work for all kind of workflows" do
    Workflow.find_each do |workflow|
      assert_nothing_raised do
        next_step = Services::NextInterestingThing.new(workflow, @user).next_station
        assert next_step.class.in?([Task, NilClass])
      end
    end
  end

  test "should show last task if more interesting" do
    (@workflow.direct_tasks - [@last_task]).each { |task| task.update!(assignee: @other_user) } # All other tasks have different Assignee
    @last_task.enable!
    @last_task.update!(assignee: @user)

    assert_equal @last_task, Services::NextInterestingThing.new(@workflow, @user).next_station

    @last_task.snooze!

    assert_equal @last_task, Services::NextInterestingThing.new(@workflow, @user).next_station
  end

  test "should show top task if all tasks have assignees" do
    @workflow.direct_tasks.each do |task|
      task.update!(assignee: @other_user)
      task.update!(aasm_state: "active")
    end
    assert_equal @first_task, Services::NextInterestingThing.new(@workflow, @user).next_station

    @workflow.direct_tasks.each do |task|
      task.update!(aasm_state: "snoozed")
    end
    assert_equal @first_task, Services::NextInterestingThing.new(@workflow, @user).next_station
  end

  test "should show first created task else" do
    @workflow.direct_tasks.each do |task|
      task.update!(assignee: @other_user)
      task.update!(aasm_state: "completed")
    end
    @last_task.reload.update!(aasm_state: "created")

    assert_equal @last_task, Services::NextInterestingThing.new(@workflow, @user).next_station
  end
end
