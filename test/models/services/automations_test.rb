require "test_helper"

class Services::AutomationsTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "execute_active_and_due_automations should only find active and due automations an execute them" do
    automation = Automation.find_by!(title: "event-test-automation")
    assert_equal "daily", automation.schedule["rule"]
    assert_equal 1, automation.schedule["interval"]

    Timecop.freeze(2024, 3, 4, 10) do
      automation.schedule["start"] = "2024-03-04"
      automation.active = true
      automation.save!

      assert_no_difference -> { Workflow.count } do
        Services::Automations.execute_active_and_due_automations
      end

      automation.active = false
      automation.save!
    end

    Timecop.freeze(2024, 3, 5, 10) do
      assert_no_difference -> { Workflow.count } do
        Services::Automations.execute_active_and_due_automations
      end
    end

    Timecop.freeze(2024, 3, 4, 10) do
      automation.active = true
      automation.save!
    end

    Timecop.freeze(2024, 3, 5, 10) do
      assert_difference -> { Workflow.count }, 1 do
        Services::Automations.execute_active_and_due_automations

        assert_equal Date.tomorrow, automation.reload.next_execution
      end
    end
  end
end
