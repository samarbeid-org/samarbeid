require "test_helper"

class Services::WorkflowsToTableTest < ActiveSupport::TestCase
  def setup
    @workflow_definition = WorkflowDefinition.find_by(name: "Prozessvorlage für automatisierte Tests")
    @workflows = @workflow_definition.workflows
    @workflow = Workflow.find_by(title: "Prozess mit Beispieldaten")
  end

  test "should always values for all definition columns even if fields are not present" do
    @workflow.content_items.destroy_all
    table_service = Services::WorkflowsToTable.new

    assert_equal table_service.definition_columns(@workflow.workflow_definition).count, table_service.values_for_definition_columns(@workflow).count
    assert table_service.values_for_definition_columns(@workflow).count >= @workflow.workflow_definition.content_item_definitions.count
  end

  test "should define column headers" do
    table_service = Services::WorkflowsToTable.new(@workflows)

    assert_equal table_service.base_columns.count, table_service.values_for_base_columns(@workflows.first).count
    assert_equal ["Prozess-ID", "Prozesstitel", "Status", "Verantwortlich", "Teilnehmend", "Erstellungsdatum", "Boolean", "Date", "Datum mit Zeit", "Decimal", "Dossiers"],
      (table_service.base_columns + table_service.definition_columns(@workflows.first.workflow_definition)).take(11)
  end

  test "block definitions should be included in the remaining stuff headers" do
    workflow_def_with_block = BlockDefinition.first.workflow_definition
    table_service = Services::WorkflowsToTable.new(workflow_def_with_block.workflows)

    assert_equal ["Datenfelder die nicht Teil der Vorlage sind", "Missbrauchshinweis aufnehmen", "Entscheidung Löschung",
      "Löschen: Ja: Anzeige löschen", "Löschen: Ja: Versendung Standard-Dankes-E-Mail",
      "Löschen: Nein: Versendung der E-Mail mit Entscheidung"],
      table_service.remaining_stuff_headers(workflow_def_with_block)
  end

  test "to_csv should return expected format" do
    table_service = Services::WorkflowsToTable.new(@workflows)

    expected_csv = <<~CSV
      Prozess-ID,Prozesstitel,Status,Verantwortlich,Teilnehmend,Erstellungsdatum,Boolean,Date,Datum mit Zeit,Decimal,Dossiers,Email,File,Integer,Richtext,Selection,String,Text,Time,Url,Datenfelder die nicht Teil der Vorlage sind,Aufgabe mit allen Feldern,Aufgabe mit allen Infoboxen
      https://samarbeid-tests.example.com/workflows/24,"""Prozess mit Beispieldaten""","""Aktiv""","""team-admin@example.org""","[""team-admin@example.org""]","""2021-12-13 18:34""",Nein,13.12.2021,"13.12.2021, 12:00 Uhr","99,99","Admin User, Some Other User",admin@example.org,"cairo-multiline.pdf, test-umlaute.pdf, tüv.pdf",99.000,"Richtext mit bold, italic, strike
      Bullet 1
      Bullet 2
      Zitat
      Todo 1
      Todo 2
      Jetzt ein Code-Block:
      def self.defaults_for_search_for_index {fields: [:title, :subtitle]}endUnd noch eine zweite Todo-Liste:
      Todo 3
      Todo 4
      Todo 5
      Verweise @1 %24 #153 *1
      ","Item 1, Item 3",Dies ist ein Teststring,"Testtext, mit zweiter Zeile",12:00 Uhr,dev.example.org,"""{\\""Phone Number\\""=>\\"" +49 170 123 4567 \\""}""","""Abgeschlossen""","""Aktiv"""
    CSV
    assert_equal expected_csv, table_service.to_csv_s.first
  end

  test "to_excel should return the expected excel file either written to a file or as a stream" do
    # Add custom task/datafields

    Interactors::TaskInteractor.add_data_field(@workflow.direct_tasks.last,
      {label: "eine kleine Notiz", content_type: "text"}, {}, User.first)
    @workflow.content_items.last.update(value: "Gemüse einkaufen")
    Interactors::TaskInteractor.add_data_field(@workflow.direct_tasks.last,
      {label: "Soviel Geld haben wir noch", content_type: "integer"}, {}, User.first)
    @workflow.content_items.last.update(value: 100)

    table_service = Services::WorkflowsToTable.new([@workflow.reload])
    tmp_file = Tempfile.new(["test-data", "xlsx"])

    assert table_service.to_excel.is_a?(String)

    assert_equal 0, tmp_file.length
    table_service.to_excel(tmp_file.path)
    assert tmp_file.length > 0

    # If this fails after changing fixtures simply uncomment following command, run test once and thus regenerate excel file
    # of course PLEASE do check if Excel file still looks like expected (i.e. open it :)
    # Services::WorkflowsToTable.new([@workflow]).to_excel("test/fixtures/files/workflow-excel-export.xlsx")
    assert_equal Roo::Spreadsheet.open("test/fixtures/files/workflow-excel-export.xlsx").to_matrix,
      Roo::Spreadsheet.open(tmp_file.path, extension: :xlsx).to_matrix
  end

  test "to_excel should create an excel file with many kinds of workflows" do
    table_service = Services::WorkflowsToTable.new(Workflow.all)

    assert_nothing_raised do
      table_service.to_excel
    end
  end

  test "frontend_link_for should produce links as needed" do
    table_service = Services::WorkflowsToTable.new(@workflows)
    assert_equal "https://samarbeid-tests.example.com/workflows/100",
      table_service.send(:frontend_link_for, "workflow", 100)
    assert_equal "https://samarbeid-tests.example.com/tasks/100",
      table_service.send(:frontend_link_for, "task", 100)
  end

  test "sheet names should be valid excel names" do
    table_service = Services::WorkflowsToTable.new
    assert_equal "______Nutzer_in-23.23..........",
      table_service.send(:valid_excel_sheet_name, "\\/}§'!Nutzer:in-23.23" + "." * 40)
  end

  test "sheet names must be unique even if valid excel names remove the uniqueness" do
    WorkflowDefinition.all.each_with_index { |wd, i| wd.update!(name: "this-name-is-only-unique-after-more-than-32-characters_#{i}") }
    table_service = Services::WorkflowsToTable.new(Workflow.all)
    assert_nothing_raised do
      table_service.to_excel
    end
  end
end
