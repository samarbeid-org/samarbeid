require "test_helper"

class Services::SnoozedTasksTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "find_unprocessed should only find those not yet processed" do
    task = Task.first
    Timecop.freeze do
      task.update!(start_at: 1.hour.ago)
      assert Task.with_start_at_in_past.include?(task)

      task.update!(start_at: Time.now)
      assert Task.with_start_at_in_past.include?(task)

      task.update!(start_at: 1.second.from_now)
      refute Task.with_start_at_in_past.include?(task)

      task.update!(start_at: nil)
      refute Task.with_start_at_in_past.include?(task)
    end
  end

  test "find_and_start should do everything necessary" do
    task = create_workflow_for!(WorkflowDefinition.first).direct_tasks.active.first
    task.snooze! && task.update(start_at: 1.hour.ago)

    assert_enqueued_with(job: ReindexJob, args: [task]) do
      assert_difference -> { Events::UnsnoozedEvent.for_object(task).count } do
        Services::SnoozedTasks.find_and_start
      end
    end
    assert task.reload.start_at.nil?
  end

  test "tasks leaving snoozed state should clean start_at" do
    task = Task.active.first

    task.snooze! && task.update!(start_at: 10.days.from_now)
    task.start!
    refute task.reload.start_at

    task.snooze! && task.update!(start_at: 10.days.from_now)
    task.skip!
    refute task.reload.start_at
  end

  test "unsnoozed event should notify assignee of task or assignee of workflow if former is nor present" do
    assignee_task = User.find_by!(email: "admin@example.org")
    assignee_workflow = User.find_by!(email: "team-admin@example.org")

    workflow_definition = WorkflowDefinition.first
    workflow_definition.groups = Group.all
    task = create_workflow_for!(workflow_definition).direct_tasks.active.first

    task.update!(assignee: assignee_task)
    task.workflow.update!(assignee: assignee_workflow)

    assert_equal assignee_task, task.assignee
    assert_equal assignee_workflow, task.workflow.assignee

    task.snooze! && task.update(start_at: 1.hour.ago)

    assert_no_difference -> { Notification.where(user: assignee_workflow).count } do
      assert_difference -> { Notification.where(user: assignee_task).count }, 1 do
        Services::SnoozedTasks.find_and_start
      end
    end
    assert task.reload.start_at.nil?

    task.update!(assignee: nil)
    assert_nil task.assignee
    assert_equal assignee_workflow, task.workflow.assignee

    task.snooze! && task.update(start_at: 1.hour.ago)

    assert_no_difference -> { Notification.where(user: assignee_task).count } do
      assert_difference -> { Notification.where(user: assignee_workflow).count }, 1 do
        Services::SnoozedTasks.find_and_start
      end
    end
    assert task.reload.start_at.nil?
  end
end
