require "test_helper"

class Services::HealthCheckTest < ActiveSupport::TestCase
  test "should run all checks without error" do
    expected_result = {"database" => true, "file_storage" => true}.to_json
    assert_equal expected_result, Services::HealthCheck.all
  end
end
