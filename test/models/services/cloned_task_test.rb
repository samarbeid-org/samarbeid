require "test_helper"

class Services::ClonedTaskTest < ActiveSupport::TestCase
  def setup
    @workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Bürgeranfrage"))
    @task_items_ordered_in_structure = @workflow.tasks_ordered_in_structure.map(&:task_items).flat_map { |tis| tis.sort_by(&:position) }
  end

  test "CloneTask should produce a new persisted task in same workflow" do
    task = @workflow.direct_tasks.first
    cloned_task = Services::ClonedTask.new(task).create

    assert cloned_task.persisted?
    assert_equal @workflow, cloned_task.workflow

    assert cloned_task.created?
    assert_equal task.task_items.count, cloned_task.task_items.count
    refute_equal task.task_definition, cloned_task.task_definition
  end

  test "cloned tasks should not be a identical copy" do
    task = @workflow.direct_tasks.first
    Comment.create!(message: "I won't be in the clone", author: User.first, object: task)
    task.update!(assignee: User.first, contributors: User.all, title: "will be copied")
    cloned_task = Services::ClonedTask.new(task).create

    assert_match task.title, cloned_task.title
    refute cloned_task.assignee
    assert_empty cloned_task.contributors
    assert_empty cloned_task.comments

    assert_not_empty task.events
    assert_empty cloned_task.events
  end

  test "it should be possible to clone manually added tasks" do
    manually_created_task = @workflow.direct_tasks.create!(name: "New Task", workflow: @workflow, position: 100)
    cloned_task = Services::ClonedTask.new(manually_created_task).create

    assert cloned_task.persisted?
    assert_equal @workflow, cloned_task.workflow
    refute cloned_task.task_definition
  end

  test "cloned task should be inside block if original one was from there" do
    task_in_block = @workflow.blocks.first.direct_tasks.first
    cloned_task = Services::ClonedTask.new(task_in_block).create

    assert cloned_task.workflow_or_block.is_a?(Block)
    assert task_in_block.workflow_or_block, cloned_task.workflow_or_block
  end

  test "task title should be appended with unique copy #" do
    task = @workflow.direct_tasks.create!(name: "New Task without any content_items. Auch mit weird title (klammern).!!", workflow: @workflow, position: 100)
    first_clone = Services::ClonedTask.new(task.reload).create
    second_clone = Services::ClonedTask.new(task.reload).create

    assert_equal "#{task.title} (1)", first_clone.title
    assert_equal "#{task.title} (2)", second_clone.title
  end

  test "cloned task should be positioned after original" do
    task = @workflow.direct_tasks.first
    cloned_task = Services::ClonedTask.new(task).create
    workflow_structure = @workflow.reload.tasks_ordered_in_structure
    assert_equal workflow_structure.index(task) + 1, workflow_structure.index(cloned_task)

    last_task_in_block = @workflow.blocks.first.items.last
    cloned_task_in_block = Services::ClonedTask.new(last_task_in_block).create
    workflow_structure = @workflow.reload.tasks_ordered_in_structure
    assert_equal workflow_structure.index(last_task_in_block) + 1, workflow_structure.index(cloned_task_in_block)
  end

  test "cloned tasks should have a unique position matching their count" do
    task = @workflow.direct_tasks.create!(name: "New Task without any content_items", workflow: @workflow, position: 100)
    first_clone = Services::ClonedTask.new(task.reload).create
    second_clone = Services::ClonedTask.new(task.reload).create

    refute_equal first_clone.position, second_clone.position
    assert task.position < first_clone.position && first_clone.position < second_clone.position

    assert @workflow.all_tasks.map(&:position).uniq.count, @workflow.all_tasks.count
  end

  test "task_items which are info_boxes should point to same content_items" do
    original_task_item = @task_items_ordered_in_structure.find { |ti| ti.info_box }
    task_with_info_box = original_task_item.task
    cloned_task = Services::ClonedTask.new(task_with_info_box).create
    cloned_task_item = cloned_task.task_items.find { |ti| ti.position.eql?(original_task_item.position) }

    assert_equal original_task_item.content_item, cloned_task_item.content_item
  end

  test "non infobox task_items should be copies" do
    original_task_item = @task_items_ordered_in_structure.find { |ti| !ti.info_box }
    task = original_task_item.task
    cloned_task = Services::ClonedTask.new(task).create

    refute_includes cloned_task.task_items.map(&:content_item).map(&:content_item_definition_id), original_task_item.content_item.content_item_definition_id
    refute_includes cloned_task.task_items.map(&:content_item), original_task_item.content_item
  end

  test "cloned task_items with ContentType other than Note should have empty value" do
    original_task_item = @task_items_ordered_in_structure.find { |ti| !ti.info_box && ti.content_item.content_type == ContentTypes::Text }
    original_task_item.content_item.update(value: "test")
    refute_empty original_task_item.content_item.reload.value

    task = original_task_item.task
    cloned_task = Services::ClonedTask.new(task).create

    cloned_task_item = cloned_task.task_items.find { |ti| ti.position == original_task_item.position }
    assert_nil cloned_task_item.content_item.value
  end

  test "cloned task_items should have empty value except for ContentTypes::Note where that value should be copied" do
    original_task_item_note = @task_items_ordered_in_structure.find { |ti| !ti.info_box && ti.content_item.content_type == ContentTypes::Note }
    refute_empty original_task_item_note.content_item.value

    task = original_task_item_note.task
    cloned_task = Services::ClonedTask.new(task).create

    cloned_task_item = cloned_task.task_items.find { |ti| ti.position == original_task_item_note.position }
    assert_equal original_task_item_note.content_item.value, cloned_task_item.content_item.value
  end
end
