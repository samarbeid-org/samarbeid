require "test_helper"

class Services::DueTasksTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "find_unprocessed should only find those not yet processed" do
    task_one, task_two, task_three = Task.open.workflow_open.limit(3)
    task_one.update!(due_at: 1.day.ago, due_processed_last: Date.current)
    task_two.update!(due_at: 1.day.ago, due_processed_last: 1.day.ago)
    task_three.update!(due_at: 1.day.ago, due_processed_last: nil)

    unprocessed = Services::DueTasks.find_unprocessed
    refute unprocessed.include?(task_one)
    assert unprocessed.include?(task_two)
    assert unprocessed.include?(task_three)

    Services::DueTasks.find_and_process

    unprocessed = Services::DueTasks.find_unprocessed
    refute unprocessed.include?(task_one)
    refute unprocessed.include?(task_two)
    refute unprocessed.include?(task_three)

    Timecop.travel(1.day.from_now) do
      unprocessed = Services::DueTasks.find_unprocessed
      assert unprocessed.include?(task_one)
      assert unprocessed.include?(task_two)
      assert unprocessed.include?(task_three)

      Services::DueTasks.find_and_process

      assert Services::DueTasks.find_unprocessed.empty?
    end
  end

  test "find_unprocessed should ignore snoozed tasks" do
    task_one, task_two, task_three = Task.active.workflow_open.limit(3)
    task_one.update!(due_at: 1.day.ago, due_processed_last: Date.current)
    task_two.update!(due_at: 1.day.ago, due_processed_last: 1.day.ago)
    task_three.update!(due_at: 1.day.ago, due_processed_last: nil)
    [task_one, task_two, task_three].each(&:snooze!)

    unprocessed = Services::DueTasks.find_unprocessed
    refute unprocessed.include?(task_one)
    refute unprocessed.include?(task_two)
    refute unprocessed.include?(task_three)

    [task_one, task_two, task_three].each(&:start!)

    unprocessed = Services::DueTasks.find_unprocessed
    refute unprocessed.include?(task_one)
    assert unprocessed.include?(task_two)
    assert unprocessed.include?(task_three)
  end

  test "find_and_process should do everything necessary" do
    task = Task.open.workflow_open.with_due_in_past.first
    task.update!(due_processed_last: nil)

    assert_enqueued_with(job: ReindexJob, args: [task]) do
      Services::DueTasks.find_and_process
    end
    assert task.reload.due_processed_last.present?
  end

  test "setting due_at date for tasks should reset processed state only if the task may still become due" do
    task = Task.open.workflow_open.with_due_in_past.first
    task.update!(due_processed_last: 3.days.ago)

    task.update!(due_at: 1.day.ago)
    assert task.reload.due_processed_last.present?

    task.update!(due_at: 1.day.from_now)
    refute task.reload.due_processed_last.present? # will become due tomorrow
  end
end
