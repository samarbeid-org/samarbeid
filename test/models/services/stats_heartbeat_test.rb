require "test_helper"

class StatsHeartbeatTest < ActiveSupport::TestCase
  test "StatsHeartbeat should report anonymized daily data on system usage" do
    heartbeats = []
    dates_to_gather_data_for = Event.group("DATE(created_at)").count.keys.sort.take(10)
    example_csv_for_gathered_data = "test/fixtures/files/example_stats_heartbeat.csv"

    heartbeats << Services::StatsHeartbeat.new.headers

    dates_to_gather_data_for.each do |date|
      heartbeats << Services::StatsHeartbeat.new(date).data.map(&:to_s)
    end

    # Uncomment line below to regenerate example csv if fixture data is changed
    # CSV.open(example_csv_for_gathered_data, "w") { |csv| heartbeats.map { |hb| csv << hb } }

    assert_equal CSV.read(example_csv_for_gathered_data), heartbeats
  end
end
