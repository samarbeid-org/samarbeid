require "test_helper"

class Services::WorkflowToDefinitionTest < ActiveSupport::TestCase
  def setup
    @workflow_definition = WorkflowDefinition.find_by!(name: "Bürgeranfrage")
    @workflow = create_workflow_for!(@workflow_definition)
  end

  test "it should update content item definitions" do
    content_item = @workflow.content_items.first
    content_item.update!(label: "NEW")

    note_content_item = @workflow.content_items.to_a.find { |ci| ci.content_type == ContentTypes::Note }
    note_content_item.update!(value: "NEW NOTE TEXT")

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    content_items_of_new_workflow = build_new_workflow.content_items
    assert_includes content_items_of_new_workflow.map(&:label), "NEW"
    assert_includes content_items_of_new_workflow.map(&:value), "NEW NOTE TEXT"
  end

  test "it should create new content item definitions" do
    @workflow.content_items << build_new_content_item
    @workflow.content_items << build_new_note_content_item

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    content_items_of_new_workflow = build_new_workflow.content_items
    assert_includes content_items_of_new_workflow.map(&:label), "NEW"
    assert_includes content_items_of_new_workflow.map(&:value), "NEW NOTE TEXT"
  end

  test "it should remove content item definitions if content items were removed" do
    removed_content_item = @workflow.content_items.last
    removed_content_item.task_items.each(&:destroy!) && removed_content_item.destroy!

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    refute_includes build_new_workflow.content_items.map(&:content_item_definition_id), removed_content_item.content_item_definition_id
  end

  test "it should update  block definitions" do
    block = @workflow.blocks.first
    block.update!(title: "NEW")
    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_equal "NEW", build_new_workflow.blocks.first.title
  end

  test "it should create block definitions if needed" do
    @workflow.blocks.create!(title: "NEW", position: @workflow.blocks.map(&:position).max + 10)

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert_equal "NEW", build_new_workflow.blocks.last.title
    assert @workflow.reload.workflow_definition.block_definitions.last.content_item_definition.nil?
  end

  test "it should remove block definitions if blocks were deleted" do
    removed_block = @workflow.blocks.last
    removed_block.destroy!

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    refute_includes build_new_workflow.blocks.map(&:title), removed_block.title
  end

  test "it should update task definitions and task_item_definitions" do
    task = @workflow.direct_tasks.first
    task.update!(title: "NEW")
    task_item = task.task_items.first
    task_item.content_item.update!(label: "NEW")
    task_item.update!(position: -1000)
    Services::WorkflowToDefinition.new(@workflow.reload).update!

    new_task = build_new_workflow.direct_tasks.first

    assert_equal "NEW", new_task.title
    assert_equal "NEW", new_task.task_items.first.content_item.label
    assert_equal(-1000, new_task.task_items.first.position)
  end

  test "it should create task definitions and task_item_definitions if needed" do
    new_task = @workflow.direct_tasks.create!(name: "NEW", position: -1000, workflow: @workflow)
    new_task.task_items.create!(position: 1, content_item: @workflow.content_items.first)
    new_task.task_items.create!(position: 2, content_item: build_new_content_item)

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    new_built_task = build_new_workflow.direct_tasks.first

    assert_equal "NEW", new_built_task.title
    assert_equal "NEW", new_built_task.task_items.last.content_item.label
    assert_equal new_task.task_items.map(&:content_item).map(&:label), new_built_task.task_items.map(&:content_item).map(&:label)
  end

  test "it should remove task definitions if task were deleted" do
    removed_task = @workflow.all_tasks.last
    Interactors::TaskInteractor.delete(removed_task, User.first)

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    assert removed_task.destroyed? # just in case validations stop deletion of task
    new_workflow = create_workflow_for!(@workflow_definition.reload)
    refute_includes new_workflow.all_tasks.map(&:title), removed_task.title
  end

  test "it should update definition from clones tasks" do
    cloned_task = Services::ClonedTask.new(@workflow.direct_tasks.first).create

    Services::WorkflowToDefinition.new(@workflow.reload).update!

    new_workflow = create_workflow_for!(@workflow_definition.reload)
    assert_includes new_workflow.direct_tasks.map(&:title), cloned_task.title
    assert_equal @workflow.all_tasks.count, new_workflow.all_tasks.count
    assert_equal @workflow.content_items.count, new_workflow.content_items.count
    assert_equal @workflow.all_tasks.flat_map(&:task_items).count, new_workflow.all_tasks.flat_map(&:task_items).count

    new_task_first, new_task_clone = new_workflow.direct_tasks.take(2)
    refute_equal new_task_first.task_items.map(&:content_item).map(&:content_item_definition_id), new_task_clone.task_items.map(&:content_item).map(&:content_item_definition_id)
  end

  test "it should update tasks moved into blocks" do
    first_task = @workflow.direct_tasks.first
    block = @workflow.blocks.first
    first_block_task = block.direct_tasks.first
    first_task.update!(position: -100, workflow_or_block: block)
    first_block_task.update!(position: -100, workflow_or_block: @workflow)

    Services::WorkflowToDefinition.new(@workflow.reload).update!
    new_workflow = create_workflow_for!(@workflow_definition.reload)

    refute_equal first_task.title, new_workflow.direct_tasks.first.title
    assert_equal first_task.title, new_workflow.blocks.first.direct_tasks.first.title
    assert_equal first_block_task.title, new_workflow.direct_tasks.first.title
  end

  test "updating (even with no changes) should increment version" do
    assert_difference -> { @workflow_definition.reload.version } do
      Services::WorkflowToDefinition.new(@workflow).update!
    end
  end

  test "it should cope with deleted tasks from deleted blocks" do
    admin = User.where(role: :super_admin).first!
    block = @workflow.blocks.first
    assert block.direct_tasks.any?
    block.direct_tasks.each { |task| Interactors::TaskInteractor.delete(task, admin) }
    block.destroy!

    assert_difference -> { @workflow_definition.reload.version } do
      Services::WorkflowToDefinition.new(@workflow.reload).update!
    end
  end

  test "#create should create new workflow definition with all other definitions derived/copied from instance data" do
    workflow = Workflow.find_by!(title: "PROCESS_WITH_TASKS_AND_BLOCKS_WITH_TASKS")

    params = {
      name: "New Workflow Definition",
      description: "A new workflow definition with the structure copied from a workflow instance",
      group_ids: [Group.system_group_all.id]
    }

    new_workflow_definition = nil
    assert_difference -> { TaskItemDefinition.count }, workflow.all_tasks.map(&:task_items).flatten.count do
      assert_difference -> { TaskDefinition.count }, workflow.all_tasks.count do
        assert_difference -> { BlockDefinition.count }, workflow.blocks.count do
          assert_difference -> { ContentItemDefinition.count }, workflow.content_items.count do
            assert_difference -> { WorkflowDefinition.count } do
              new_workflow_definition = Services::WorkflowToDefinition.new(workflow).create(params)
            end
          end
        end
      end
    end

    refute_nil new_workflow_definition
    refute new_workflow_definition.errors.any?
    assert_equal params[:name], new_workflow_definition.name
    assert_equal params[:description], new_workflow_definition.description
    assert_equal params[:group_ids], new_workflow_definition.groups.map(&:id)

    # check content_items
    workflow.content_items.each do |ci|
      assert_equal ci.label, ci.content_item_definition.label
      assert_equal ci.content_type.to_s, ci.content_item_definition.content_type.to_s
      assert_equal ci.options, ci.content_item_definition.options
      assert_equal new_workflow_definition, ci.content_item_definition.workflow_definition
    end

    # check blocks
    workflow.blocks.each do |b|
      assert_equal b.title, b.block_definition.title
      assert_equal b.position, b.block_definition.position
      assert_equal b.parallel, b.block_definition.parallel

      if b.decision.nil?
        assert_nil b.block_definition.decision
      else
        assert_equal b.decision, b.block_definition.decision
      end

      if b.content_item.nil?
        assert_nil b.block_definition.content_item_definition
      else
        assert_equal b.content_item.content_item_definition, b.block_definition.content_item_definition
      end
    end

    # check tasks
    workflow.all_tasks.each do |t|
      assert_equal t.name, t.task_definition.name
      assert_equal t.position, t.task_definition.position

      parent = t.workflow_or_block
      parent_definition = parent.is_a?(Workflow) ? parent.workflow_definition : parent.block_definition
      assert_equal parent_definition, t.task_definition.definition_workflow_or_block

      assert_equal new_workflow_definition, t.task_definition.workflow_definition
    end

    # check task_items
    workflow.all_tasks.map(&:task_items).flatten.each do |ti|
      assert_equal ti.position, ti.task_item_definition.position
      assert_equal ti.required.to_s, ti.task_item_definition.required.to_s
      assert_equal ti.info_box.to_s, ti.task_item_definition.info_box.to_s
      assert_equal ti.task.task_definition, ti.task_item_definition.task_definition
      assert_equal ti.task.workflow.workflow_definition, ti.task_item_definition.workflow_definition
      assert_equal ti.content_item.content_item_definition, ti.task_item_definition.content_item_definition
    end

    assert_equal new_workflow_definition, workflow.workflow_definition
  end

  private

  def build_new_workflow
    @workflow_definition.reload.build_workflow
  end

  def build_new_content_item
    new_content_item = ContentItemDefinition.first.build_content_item(@workflow)
    new_content_item.content_item_definition = nil
    new_content_item.label = "NEW"
    new_content_item
  end

  def build_new_note_content_item
    new_note_content_item = ContentItemDefinition.where(content_type: ContentTypes::Note.to_s).first.build_content_item(@workflow)
    new_note_content_item.content_item_definition = nil
    new_note_content_item.value = "NEW NOTE TEXT"
    new_note_content_item
  end
end
