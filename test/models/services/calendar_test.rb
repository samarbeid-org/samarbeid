require "test_helper"

class Services::CalendarTest < ActiveSupport::TestCase
  def setup
    @user = User.find_by(email: "admin@example.org")
    @calendar = Services::Calendar.new(@user)
  end

  test "calendar should select tasks relevant for user" do
    @calendar.relevant_due_tasks.each do |task|
      assert task.assignee == @user || task.contributors.include?(@user)
      assert task.due_at
    end
  end

  test "calendar should select data fields relevant for user" do
    Timecop.freeze(2023, 2, 17) do
      TaskItem.update_all(add_to_calendar: true)

      assert_not_empty @calendar.relevant_data_fields

      @calendar.relevant_data_fields.each do |task_item|
        assert task_item.task.assignee == @user || task_item.task.contributors.include?(@user)
      end
    end
  end

  test "calendar events should have summaries depending on task (type + state)" do
    # Active
    assert_equal "Folgeprojekte wählen aus P-Matching, Teammatching und anderen Bereichen • EB-7 (Bestimmung Externer Bedarfe)",
      @calendar.task_title_calendar_style(Task.find(26))

    # Completed
    assert_equal "[✓] Details zu Kooperationsidee sammeln • EB-7 (Bestimmung Externer Bedarfe)",
      @calendar.task_title_calendar_style(Task.find(22))

    # Skipped
    assert_equal "[✓] Versendung der E-Mail mit Entscheidung • event-test-workflow (NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch)",
      @calendar.task_title_calendar_style(Task.find(127))

    # Snoozed
    assert_equal "Anzeige löschen • event-test-workflow (NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch)",
      @calendar.task_title_calendar_style(Task.find(125))

    # Einzelne Aufgabe mit custom Prozess Namen
    assert_equal "Simple Task • PROCESS_WITH_TASKS_AND_BLOCKS_WITH_TASKS",
      @calendar.task_title_calendar_style(Task.find(158))

    # Einzelne Aufgabe
    workflow_without_definition = WorkflowDefinition.system_process_definition_single_task.build_workflow
    workflow_without_definition.save!
    assert_equal "Einzelaufgabe",
      @calendar.task_title_calendar_style(workflow_without_definition.all_tasks.first)
  end

  test "calendar should generate ical file for due tasks as expected" do
    TaskItem.update_all(add_to_calendar: nil) # only test due tasks here

    expected_calendar = <<~CALENDAR
      BEGIN:VCALENDAR
      VERSION:2.0
      PRODID:icalendar-ruby
      CALSCALE:GREGORIAN
      X-WR-CALNAME:samarbeid
      REFRESH-INTERVAL;VALUE=DURATION:P1H
      BEGIN:VEVENT
      DTSTAMP:20230216T230000Z
      UID:fc267f95-fe72-5b6f-a622-b26c6ac207a1
      DTSTART;VALUE=DATE:20230117
      DESCRIPTION:Link zur Aufgabe: https://samarbeid-tests.example.com/tasks/123
       \nVerantwortlich: Admin User\nStatus: Abgeschlossen
      SUMMARY:[✓] Outlook does not allow more than 255 chars for summary !!!!!!
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
       !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!...
      URL;VALUE=URI:https://samarbeid-tests.example.com/tasks/123
      END:VEVENT
      END:VCALENDAR
    CALENDAR

    # Also squish and replace newlines as they look different loaded from file vs. above
    Timecop.freeze(2023, 2, 17) do
      @calendar.relevant_due_tasks.first.update!(title: "Outlook does not allow more than 255 chars for summary " + "!" * 256)

      # Generate new calendar file by temporarily commenting out below line
      # File.write("admin-user-calendar-due-tasks.ics", @calendar.to_ical)
      assert_equal expected_calendar.gsub("\\n", " ").squish,
        @calendar.to_ical.gsub("\\n", " ").squish
    end
  end

  test "calendar should generate ical file for datafields as expected" do
    Task.update_all(due_at: nil) # only test datafield calender entries here

    expected_calendar = <<~CALENDAR
      BEGIN:VCALENDAR
      VERSION:2.0
      PRODID:icalendar-ruby
      CALSCALE:GREGORIAN
      X-WR-CALNAME:samarbeid
      REFRESH-INTERVAL;VALUE=DURATION:P1H
      BEGIN:VEVENT
      DTSTAMP:20230216T230000Z
      UID:e6e625be-eb14-5ba7-8b9b-9af87c6028bc
      DTSTART;VALUE=DATE:20230214
      DESCRIPTION:Link zur Aufgabe: https://samarbeid-tests.example.com/tasks/145
       \nVerantwortlich: DSB Boss\nStatus: Übersprungen\nDatenfeld: Startdatum d
       er Veranstaltung
      SUMMARY:[✓] Externe Veranstaltung erfassen • EV-22 (Teilnahme externe V
       eranstaltung) - Startdatum der Veranstaltung
      URL;VALUE=URI:https://samarbeid-tests.example.com/tasks/145
      END:VEVENT
      BEGIN:VEVENT
      DTSTAMP:20230216T230000Z
      UID:194f2066-05a9-547f-a97c-16bcef2142d1
      DTSTART:20230214T101100Z
      DTEND:20230214T104100Z
      DESCRIPTION:Link zur Aufgabe: https://samarbeid-tests.example.com/tasks/145
       \nVerantwortlich: DSB Boss\nStatus: Übersprungen\nDatenfeld: Start (mit U
       hrzeit)
      SUMMARY:[✓] Externe Veranstaltung erfassen • EV-22 (Teilnahme externe V
       eranstaltung) - Start (mit Uhrzeit)
      URL;VALUE=URI:https://samarbeid-tests.example.com/tasks/145
      END:VEVENT
      END:VCALENDAR
    CALENDAR

    # Also squish and replace newlines as they look different loaded from file vs. above
    Timecop.freeze(2023, 2, 17) do
      # Generate new calendar file by temporarily commenting out below line
      # File.write("admin-user-calendar-datafields.ics", @calendar.to_ical)
      assert_equal expected_calendar.gsub("\\n", " ").squish,
        @calendar.to_ical.gsub("\\n", " ").squish
    end
  end

  test "calender should only include events in relevant timeframe" do
    assert @calendar.valid_event_timerange.cover?(Time.now)
    refute @calendar.valid_event_timerange.cover?((1.year + 1.day).ago)
    assert @calendar.valid_event_timerange.cover?(200.years.from_now)

    Timecop.freeze(2023, 12, 20) do
      due_task = @calendar.relevant_due_tasks.first
      assert @calendar.relevant_due_tasks.include?(due_task)

      due_task.update(due_at: 13.months.ago)
      refute @calendar.relevant_due_tasks.include?(due_task)

      due_task.update(due_at: 2.years.from_now)
      assert @calendar.relevant_due_tasks.include?(due_task)

      calendar_task_item = @calendar.relevant_data_fields.first
      assert @calendar.relevant_data_fields.include?(calendar_task_item)

      calendar_task_item.content_item.update(value: 13.months.ago)
      refute @calendar.relevant_data_fields.include?(calendar_task_item)

      calendar_task_item.content_item.update(value: 2.years.from_now)
      assert @calendar.relevant_data_fields.include?(calendar_task_item)
    end
  end
end
