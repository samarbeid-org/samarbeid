require "test_helper"

class Services::EventDispatcherTest < ActiveSupport::TestCase
  setup do
    @user = User.find_by!(email: "admin@example.org")
    @task = Task.first
  end

  test "Creating an event triggers the creation of notifications for all receivers of this event" do
    @srs_user = User.find_by!(email: "srs_user@example.org")
    @task.update!(assignee: @user)

    event = Events::ChangedDueDateEvent.new(subject: @srs_user, object: @task, data: {new_due_date: Date.today})
    assert_not_empty event.notification_receivers

    assert_difference -> { Notification.count }, event.notification_receivers.count do
      event.save
    end
  end

  test "Notifications shouldn't created for users who doesn't have read access to the object of the event" do
    workflow = Workflow.find_by!(title: "Prozess mit Beispieldaten")
    assignee_user = User.find_by!(email: "srs_user@example.org")
    assignee_user.groups << workflow.workflow_definition.groups.first
    assert assignee_user.can?(:read, workflow)

    workflow.update!(assignee: assignee_user)
    assert_difference -> { assignee_user.notifications.count } do
      Interactors::WorkflowInteractor.update(workflow, {title: "Updated title 1"}, @user)
    end

    assignee_user.groups.delete(workflow.workflow_definition.groups.first)
    refute assignee_user.can?(:read, workflow)

    assert_no_difference -> { assignee_user.notifications.count } do
      Interactors::WorkflowInteractor.update(workflow, {title: "Updated title 2"}, @user)
    end
  end
end
