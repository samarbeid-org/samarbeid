require "test_helper"

class Services::MoveTaskTest < ActiveSupport::TestCase
  test "it should not be possible to move tasks with content_items which are used elsewhere" do
    task_with_content_items_used_elsewhere = Task.find(152)
    task_with_infoboxes = Task.find(153)

    refute Services::MoveTask.new(task_with_content_items_used_elsewhere).valid?
    refute Services::MoveTask.new(task_with_infoboxes).valid?

    assert_includes Services::MoveTask.new(task_with_content_items_used_elsewhere).errors, "Feld 'Email' wird noch in weiteren Aufgaben benutzt."
  end

  test "it should be possible to move a simple task" do
    movable_task = Task.find(154)
    assert Services::MoveTask.new(movable_task).valid?

    target_workflow = Workflow.where.not(id: movable_task.workflow.id).first
    assert_nothing_raised { Services::MoveTask.new(movable_task).move_to!(target_workflow) }
  end

  test "move a task with lot's of content_items and task_items" do
    task_with_infoboxes = Task.find(153)
    Interactors::TaskInteractor.delete(task_with_infoboxes, User.find_by(email: "admin@example.org"))

    task_with_content_items = Task.find(152)
    assert Services::MoveTask.new(task_with_content_items).valid?

    target_workflow = Workflow.where.not(id: task_with_content_items.workflow.id).first

    assert_nothing_raised { Services::MoveTask.new(task_with_content_items).move_to!(target_workflow) }

    assert_equal target_workflow, task_with_content_items.reload.workflow
  end
end
