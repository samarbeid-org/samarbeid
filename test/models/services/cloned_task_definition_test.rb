require "test_helper"

class Services::ClonedTaskDefinitionTest < ActiveSupport::TestCase
  def setup
    @workflow_definition = WorkflowDefinition.find_by!(name: "Bürgeranfrage")
    @task_item_definitions_ordered_in_structure = @workflow_definition.task_definitions_ordered_in_structure.map(&:task_item_definitions).flat_map { |tis| tis.sort_by(&:position) }
  end

  test "Cloning a task definition should produce a new persisted task definition in same workflow definition" do
    task_definition = @workflow_definition.direct_task_definitions.first
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition).create

    assert cloned_task_definition.persisted?
    assert_equal @workflow_definition, cloned_task_definition.workflow_definition

    assert_equal task_definition.task_item_definitions.count, cloned_task_definition.task_item_definitions.count
  end

  test "Cloning a task definition should copy certain things of the original" do
    task_definition = @workflow_definition.direct_task_definitions.first
    task_definition.update!(
      name: "will be copied",
      candidate_assignee: User.first,
      candidate_contributors: User.all,
      due_in_days: 10,
      deferral_in_days: 15
    )
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition).create

    assert_match task_definition.name, cloned_task_definition.name
    assert_equal task_definition.candidate_assignee, cloned_task_definition.candidate_assignee
    assert_equal task_definition.candidate_contributors, cloned_task_definition.candidate_contributors
    assert_equal task_definition.due_in_days, cloned_task_definition.due_in_days
    assert_equal task_definition.deferral_in_days, cloned_task_definition.deferral_in_days
  end

  test "cloned task definition should be inside block definition if original one was from there" do
    task_definition_in_block = @workflow_definition.block_definitions.first.item_definitions.first
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition_in_block).create

    assert cloned_task_definition.definition_workflow_or_block.is_a?(BlockDefinition)
    assert task_definition_in_block.definition_workflow_or_block, cloned_task_definition.definition_workflow_or_block
  end

  test "task definition name should be appended with unique copy #" do
    task_definition = @workflow_definition.direct_task_definitions.create!(
      name: "New Task definition without any content_items. Auch mit weird title (klammern).!!",
      workflow_definition: @workflow_definition,
      position: 100
    )
    first_clone = Services::ClonedTaskDefinition.new(task_definition.reload).create
    second_clone = Services::ClonedTaskDefinition.new(task_definition.reload).create

    assert_equal "#{task_definition.name} (1)", first_clone.name
    assert_equal "#{task_definition.name} (2)", second_clone.name
  end

  test "cloned task definition should be positioned after the original" do
    task_definition = @workflow_definition.direct_task_definitions.first
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition).create
    workflow_definition_structure = @workflow_definition.reload.item_definitions
    assert_equal workflow_definition_structure.index(task_definition) + 1, workflow_definition_structure.index(cloned_task_definition)

    # the new clone should be insert right after the original and the old clone
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition).create
    workflow_definition_structure = @workflow_definition.reload.item_definitions
    assert_equal workflow_definition_structure.index(task_definition) + 1, workflow_definition_structure.index(cloned_task_definition)

    last_task_definition_in_first_block_definition = @workflow_definition.block_definitions.first.item_definitions.last
    cloned_task_definition_in_block = Services::ClonedTaskDefinition.new(last_task_definition_in_first_block_definition).create
    block_definition_structure = @workflow_definition.reload.block_definitions.first.item_definitions
    assert_equal block_definition_structure.index(last_task_definition_in_first_block_definition) + 1, block_definition_structure.index(cloned_task_definition_in_block)
  end

  test "task_item_definitions which are info_boxes should point to same content_item_definitions" do
    original_task_item_definition = @task_item_definitions_ordered_in_structure.find { |tid| tid.info_box }
    task_definition_with_info_box = original_task_item_definition.task_definition
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition_with_info_box).create
    cloned_task_item_definition = cloned_task_definition.task_item_definitions.find { |tid| tid.position.eql?(original_task_item_definition.position) }

    assert_equal original_task_item_definition.content_item_definition, cloned_task_item_definition.content_item_definition
  end

  test "normal task_item_definitions should be copies" do
    original_task_item_definition = @task_item_definitions_ordered_in_structure.find { |tid| !tid.info_box && tid.content_item_definition.content_type == ContentTypes::Date }
    original_task_item_definition.update!(required: true, add_to_calendar: true)
    task_definition = original_task_item_definition.task_definition
    cloned_task_definition = Services::ClonedTaskDefinition.new(task_definition).create

    refute_includes cloned_task_definition.task_item_definitions.map(&:content_item_definition_id), original_task_item_definition.content_item_definition.id
    refute_includes cloned_task_definition.task_item_definitions.map(&:content_item_definition), original_task_item_definition.content_item_definition

    cloned_task_item_definition = cloned_task_definition.task_item_definitions.find { |tid| tid.position.eql?(original_task_item_definition.position) }
    assert_equal original_task_item_definition.required, cloned_task_item_definition.required
    assert_equal original_task_item_definition.add_to_calendar, cloned_task_item_definition.add_to_calendar
    assert_equal original_task_item_definition.content_item_definition.label, cloned_task_item_definition.content_item_definition.label
    assert_equal original_task_item_definition.content_item_definition.content_type, cloned_task_item_definition.content_item_definition.content_type
    assert_equal original_task_item_definition.content_item_definition.options, cloned_task_item_definition.content_item_definition.options
    assert_equal original_task_item_definition.content_item_definition.default_value, cloned_task_item_definition.content_item_definition.default_value
  end
end
