require "test_helper"

class Services::DossiersToTableTest < ActiveSupport::TestCase
  def setup
    @dossier = Dossier.find 59 # Test Daten Dossier
    @definition = @dossier.definition
    @dossiers = [@dossier]
  end

  test "to_csv should return expected format" do
    table_service = Services::DossiersToTable.new(@dossiers)

    expected_csv = <<~CSV
      Dossier-ID,Dossiertitel,Erstellungsdatum,String,Url,Datum mit Zeit,Email,Text,Richtext,Integer,Decimal,Boolean,Date,Time,Selection,Dossiers,File
      https://samarbeid-tests.example.com/dossiers/59,"""Die ist ein Teststring""","""2021-03-25 10:27""",Die ist ein Teststring,dev.vuetifyjs.com,"27.04.2021, 13:25 Uhr",admin@example.org,"Testtext, mit zweiter Zeile","Richtext mit bold, italic,
      Zitat
      ",99,"99,99",Ja,01.03.2021,14:45 Uhr,"Item 1, Item 3","Admin User, Some Other User","cairo-multiline.pdf, table-screenshot.png, sample.docx"
    CSV
    assert_equal expected_csv, table_service.to_csv_s.first
  end

  test "to_excel should return the expected excel file either written to a file or as a stream" do
    table_service = Services::DossiersToTable.new([@dossier])
    tmp_file = Tempfile.new(["test-data", "xlsx"])

    assert table_service.to_excel.is_a?(String)

    assert_equal 0, tmp_file.length
    table_service.to_excel(tmp_file.path)
    assert tmp_file.length > 0

    # If this fails after changing fixtures simply uncomment following command, run test once and thus regenerate excel file
    # of course PLEASE do check if Excel file still looks like expected (i.e. open it :)
    # Services::DossiersToTable.new([@dossier]).to_excel("test/fixtures/files/dossier-excel-export.xlsx")
    assert_equal Roo::Spreadsheet.open("test/fixtures/files/dossier-excel-export.xlsx").to_matrix,
      Roo::Spreadsheet.open(tmp_file.path, extension: :xlsx).to_matrix
  end

  test "to_excel should create an excel file with many kinds of workflows" do
    table_service = Services::DossiersToTable.new(Dossier.all)

    assert_nothing_raised do
      table_service.to_excel
    end
  end

  test "frontend_link_for should produce links as needed" do
    table_service = Services::DossiersToTable.new
    assert_equal "https://samarbeid-tests.example.com/dossiers/100",
      table_service.send(:frontend_link_for, "dossier", 100)
  end
end
