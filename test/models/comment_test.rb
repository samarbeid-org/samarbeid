require "test_helper"

class CommentTest < ActiveSupport::TestCase
  test "creating comments should work" do
    assert_nothing_raised do
      Comment.create!(message: "a message", author: User.first, object: Task.first)
    end
  end

  test "comments should allow attaching richtext images / RichtextImageAttachable concern" do
    ["avatar.png", "avatar.jpg", "avatar.gif"].each do |supported_image|
      comment = create_comment_with_richtext_image("test/fixtures/files/#{supported_image}")
      assert comment.valid?, supported_image
    end
    ["tüv.pdf", "sample.docx"].each do |unsupported_image|
      comment = create_comment_with_richtext_image("test/fixtures/files/#{unsupported_image}")
      refute comment.valid?, unsupported_image
    end
  end

  test "comments should validate the size of inline images / RichtextImageAttachable concern" do
    Tempfile.create(["image", ".jpeg"]) do |big_valid_image|
      big_valid_image << `head -c 10485760 /dev/urandom` # 10M
      comment = create_comment_with_richtext_image(big_valid_image.path)
      assert comment.valid?
    end

    Tempfile.create(["image", ".jpeg"]) do |too_big_image|
      too_big_image << `head -c 22020096 /dev/urandom` # 21M
      comment = create_comment_with_richtext_image(too_big_image.path)
      refute comment.valid?
    end
  end

  private

  def create_comment_with_richtext_image(file_path)
    comment = Comment.create!(message: "a message", author: User.first, object: Task.first)
    File.open(file_path, "rb") do |test_image|
      comment.richtext_images.attach(io: test_image, filename: file_path.split("/").last)
    end
    comment
  end
end
