require "test_helper"

class WorkflowDefinitionTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "destroying workflow definitions should work to allow admins to clean up system" do
    assert_nothing_raised do
      definition = WorkflowDefinition.find_by!(name: "Veranstaltungsplanung") # has workflows with documents and notifications attached
      definition.destroy!
    end
  end

  test "building a new workflow should copy all data and attached items" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    new_workflow = workflow_definition.build_workflow

    assert_equal workflow_definition.version, new_workflow.definition_version
    assert_equal workflow_definition.item_definitions.map(&:name), new_workflow.items.map(&:name)

    # Blocks
    first_block_definition = workflow_definition.block_definitions.min_by(&:position)
    first_block = new_workflow.blocks.min_by(&:position)

    assert_equal first_block_definition.direct_task_definitions.sort_by(&:position).map(&:name),
      first_block.direct_tasks.sort_by(&:position).map(&:name)
    assert_equal first_block_definition.content_item_definition.label, first_block.content_item.label
    assert_equal first_block_definition.decision, first_block.decision

    # Content_items
    first_task_definition = workflow_definition.direct_task_definitions.min_by(&:position)
    first_task = new_workflow.direct_tasks.min_by(&:position)

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] },
      first_task.task_items.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] }

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map(&:content_item_definition).map(&:label),
      first_task.task_items.sort_by(&:position).map(&:content_item).map(&:label)
  end

  test "building a single_task process should set correct system_identifier" do
    workflow_definition = WorkflowDefinition.system_process_definition_single_task
    new_workflow = workflow_definition.build_workflow

    assert new_workflow.system_process_single_task?
  end

  test "creating a new workflow should copy all attached items" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    new_workflow = create_workflow_for!(workflow_definition)
    new_workflow.reload
    assert_equal workflow_definition.item_definitions.map(&:name), new_workflow.items.map(&:name)

    # Blocks
    first_block_definition = workflow_definition.block_definitions.min_by(&:position)
    first_block = new_workflow.blocks.min_by(&:position)

    assert_equal first_block_definition.direct_task_definitions.sort_by(&:position).map(&:name),
      first_block.direct_tasks.sort_by(&:position).map(&:name)
    assert_equal first_block_definition.content_item_definition.label, first_block.content_item.label
    assert_equal first_block_definition.decision, first_block.decision

    # Content_items
    first_task_definition = workflow_definition.direct_task_definitions.min_by(&:position)
    first_task = new_workflow.direct_tasks.min_by(&:position)

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] },
      first_task.task_items.sort_by(&:position).map { |ti| [ti.position, ti.required, ti.info_box] }

    assert_equal first_task_definition.task_item_definitions.sort_by(&:position).map(&:content_item_definition).map(&:label),
      first_task.task_items.sort_by(&:position).map(&:content_item).map(&:label)
  end

  test "creating a new workflow should link all definitions" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    new_workflow = create_workflow_for!(workflow_definition)
    new_workflow.reload
    assert_equal workflow_definition, new_workflow.workflow_definition

    # Blocks
    first_block_definition = workflow_definition.block_definitions.min_by(&:position)
    first_block = new_workflow.blocks.min_by(&:position)

    assert_equal first_block_definition, first_block.block_definition

    # TaskItems
    first_task_item_definition = workflow_definition.direct_task_definitions.min_by(&:position).task_item_definitions.min_by(&:position)
    first_task_item = new_workflow.direct_tasks.min_by(&:position).task_items.min_by(&:position)

    assert_equal first_task_item_definition, first_task_item.task_item_definition

    first_content_item_definition = first_task_item_definition.content_item_definition
    first_content_item = first_task_item.content_item

    assert_equal first_content_item_definition, first_content_item.content_item_definition
  end

  test "creating a new workflow should not instantiate any deleted items" do
    workflow_definition = WorkflowDefinition.find(9) # Bürgeranfrage
    workflow_definition.content_item_definitions.each { |cid| cid.destroy! }
    workflow_definition.item_definitions.each { |id| id.destroy! }

    workflow = workflow_definition.reload.build_workflow

    assert_empty workflow.items
    assert_empty workflow.content_items
  end

  test "a empty workflow definition should be startable" do
    assert_nothing_raised do
      wd = WorkflowDefinition.create!(name: "Leere Prozessvorlage", groups: Group.all)
      create_workflow_for!(wd)
    end
  end

  test "system_process_definition_single_task should return a definition with a single task and 2 fields" do
    WorkflowDefinition.find_by(system_identifier: :system_process_definition_single_task).destroy

    definition_single_task = WorkflowDefinition.system_process_definition_single_task
    refute_nil definition_single_task
    assert definition_single_task.system_process_definition_single_task?
    assert_equal 1, definition_single_task.all_task_definitions.count

    task_definition = definition_single_task.all_task_definitions.first
    assert_equal 2, task_definition.task_item_definitions.count
    assert_equal ContentTypes::Richtext, task_definition.task_item_definitions.first.content_item_definition.content_type
    assert_equal ContentTypes::File, task_definition.task_item_definitions.second.content_item_definition.content_type
  end

  test "there should only be one definition with system_identifier equals system_process_definition_single_task" do
    WorkflowDefinition.find_by!(system_identifier: :system_process_definition_single_task)

    definition = WorkflowDefinition.create(
      name: "Test Definition",
      system_identifier: :system_process_definition_single_task,
      group_ids: Group.first.id
    )
    refute definition.valid?
    assert definition.errors.include?(:system_identifier)
  end

  test "deactivated process definition should have a correct display_name" do
    definition = WorkflowDefinition.activated.first

    refute definition.deactivated?
    refute definition.display_name.start_with?("[deaktiviert]")

    assert_difference -> { WorkflowDefinition.activated.count }, -1 do
      assert_difference -> { WorkflowDefinition.deactivated.count } do
        definition.deactivate
      end
    end

    assert definition.deactivated?
    assert definition.display_name.start_with?("[deaktiviert]")
  end

  test "process definition name should not contain deactivated prefix" do
    definition = WorkflowDefinition.first
    refute definition.update(name: "deaktivierte vorlage")
    refute definition.valid?
    assert definition.errors.include?(:name)
  end

  test "item_definitions of workflow_definitions and block_definitions should have uniq positions" do
    WorkflowDefinition.all.each do |wd|
      refute wd.item_definitions.map(&:position).length != wd.item_definitions.map(&:position).uniq.length, "#{wd.class} (id=#{wd.id}) has item_definitions with non uniq position values"
    end
    BlockDefinition.all.each do |bd|
      refute bd.item_definitions.map(&:position).length != bd.item_definitions.map(&:position).uniq.length, "#{bd.class} (id=#{bd.id}) has item_definitions with non uniq position values"
    end
  end
end
