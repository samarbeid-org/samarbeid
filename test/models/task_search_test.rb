require "test_helper"

class TaskSearchTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    @user = User.find_by!(email: "admin@example.org")
  end

  test "#filter_for_index should work if no instances are present" do
    TaskItem.delete_all
    Task.delete_all

    results = Task.filter_for_index(@user)
    assert_empty results.query
  end

  test "#filter_category_scopes should report correctly" do
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Erstgespräch Erfindung"))
    task = workflow.items.first
    assert_equal ["ACTIVE", "ALL"], filter_categories_for(task, @user)
    task.marked_by_users << @user
    assert_equal ["ACTIVE", "MARKED", "ALL"], filter_categories_for(task, @user)
    task.update(due_at: 3.days.ago)
    assert_equal ["ACTIVE", "DUE", "MARKED", "ALL"], filter_categories_for(task, @user)
    task.assignee = @user
    task.snooze!
    assert_equal ["DUE", "SNOOZED", "MARKED", "ALL"], filter_categories_for(task, @user)
    task.complete!
    assert_equal ["MARKED", "ALL"], filter_categories_for(task, @user)
    another_task = workflow.items.last
    another_task.update(assignee: @user) && another_task.start!
    another_task.skip!
    assert_equal ["ALL"], filter_categories_for(another_task, @user)
  end

  test "#filter_category_counts should report correctly" do
    results = Task.filter_for_index(@user)
    assert_equal({"ACTIVE" => 19, "DUE" => 3, "SNOOZED" => 1, "MARKED" => 4, "ALL" => 121}, results.filter_category_counts)
  end

  test "tasks should be found by their identifier and parts of title / subtitle" do
    task = Task.find(47) # Beschreibung erstellen - Patentanmeldung > PA-1
    queries = ["#47", "47"]
    queries.each do |query|
      assert_equal task, Task.search_for_list(@user, query).first, "Search by Task.search_for_list for '#{query}' did not find correct result"
    end
    assert Task.search_for_list(@user, "Patentanmeldung PA-1").include?(task)
  end

  test ":by_due_date should show most due tasks (oldest) first and then created_at newest first" do
    very_due_task, due_task, normal_task, very_old_task = Task.all.limit(4)
    very_due_task.update!(due_at: 1.year.ago)
    due_task.update!(due_at: 1.week.ago)
    normal_task.update!(due_at: nil, created_at: Time.now)
    very_old_task.update!(due_at: nil, created_at: 1.year.ago)

    tasks_by_due = Task.filter_for_index(@user, order_by: :by_due_date).query.to_a
    assert tasks_by_due.index(very_due_task) < tasks_by_due.index(due_task)
    assert tasks_by_due.index(due_task) < tasks_by_due.index(normal_task)
    assert tasks_by_due.index(normal_task) < tasks_by_due.index(very_old_task)
  end

  private

  def filter_categories_for(task, user)
    Task.filter_category_scopes(user).map { |category, scope_lambda| (scope_lambda.call(Task.where(id: task.id)).count > 0) ? category : nil }.compact
  end
end
