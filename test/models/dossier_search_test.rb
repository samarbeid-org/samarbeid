require "test_helper"

class DossierSearchTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @dossier = Dossier.find(1) # Admin User
    @dossier_2 = Dossier.find(2) # Some Other User
    @user = User.find_by!(email: "admin@example.org")
  end

  test "dossier filter_for_index should work" do
    results = Dossier.filter_for_index(@user)
    assert_not_empty results.query
  end

  test "#filter_for_index should work if no instances are present" do
    Dossier.delete_all

    results = Dossier.filter_for_index(@user)
    assert_empty results.query
  end

  test "dossier filter_for_index should respect order" do
    perform_enqueued_jobs do
      @dossier_2.update!(created_at: 200.years.from_now)
      @dossier.update!(created_at: 200.years.ago)
    end

    results = Dossier.filter_for_index(@user, order_by: :created_at_desc)
    assert_equal @dossier_2, results.query.to_a.first
    assert_equal @dossier, results.query.to_a.last
  end

  test "deleted dossier should not be included in search" do
    perform_enqueued_jobs do
      @dossier.destroy!
    end
    assert_empty Dossier.filter_for_index(@user, @dossier.title, apply_to: Dossier.where(definition_id: @dossier.definition_id)).query.to_a
  end

  test "searching for data fields should work" do
    results = SearchDocument.where(searchable_type: "Dossier").fulltext("James")
    assert_equal @dossier_2, results.first.searchable
  end

  test "indexing a dossier with all field types should basically work" do
    dossier_with_all_field_types = Dossier.find(59)
    assert_nothing_raised { dossier_with_all_field_types.as_search_document }

    search_for_file_content = SearchDocument.where(searchable_type: "Dossier").fulltext("The quick brown fox jumped over the lazy cat.")
    assert_equal dossier_with_all_field_types, search_for_file_content.first.searchable
  end

  test "dossiers should be found by their identifier and parts of title" do
    dossier = Dossier.find(1) # Admin User
    queries = ["1", "Admin", "admin@example.org"]
    queries.each do |query|
      assert_equal dossier, Dossier.search_for_list(@user, query).first, "Search by Dossier.search_for_list for '#{query}' did not find correct result"
    end
  end
end
