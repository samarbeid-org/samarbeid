require "test_helper"

class DossierItemDefinitionTest < ActiveSupport::TestCase
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_item_definition = @dossier_definition.items.find_by!(name: "String")
  end

  test "it should be possible to create a dossier item definition" do
    assert_difference -> { DossierItemDefinition.count } do
      DossierItemDefinition.create(definition: @dossier_definition, name: "Test", position: 99, content_type: ContentTypes::String.to_s)
    end
  end

  test "it should not be possible to save a dossier item definition without name" do
    assert_not @dossier_item_definition.update(name: "")
    assert @dossier_item_definition.errors.details[:name].any?
  end

  test "it should not be possible to save a dossier item definition without position" do
    assert_not @dossier_item_definition.update(position: nil)
    assert @dossier_item_definition.errors.details[:position].any?
  end

  test "dossier item definition names should be unique in the context of the dossier definition" do
    dossier_definition_new = DossierDefinition.create!(name: "test", groups: Group.all)

    dossier_item_definition_new = DossierItemDefinition.create(definition: @dossier_definition, name: "String", position: 99, content_type: ContentTypes::String.to_s)
    assert dossier_item_definition_new.errors.details[:name].any?

    dossier_item_definition_new.definition = dossier_definition_new
    assert_difference -> { DossierItemDefinition.count } do
      dossier_item_definition_new.save
    end
  end

  test "dossier item definition position should be unique in the context of the dossier definition" do
    dossier_definition_new = DossierDefinition.create!(name: "test", groups: Group.all)

    dossier_item_definition_new = DossierItemDefinition.create(definition: @dossier_definition, name: "Test", position: @dossier_item_definition.position, content_type: ContentTypes::Integer.to_s)
    assert dossier_item_definition_new.errors.details[:position].any?

    dossier_item_definition_new.definition = dossier_definition_new
    assert_difference -> { DossierItemDefinition.count } do
      dossier_item_definition_new.save
    end
  end

  test "it should not be possible to change the definition of a dossier item definition" do
    dossier_definition_new = DossierDefinition.create!(name: "test", groups: Group.all)

    assert_not @dossier_item_definition.update(definition: dossier_definition_new)
    assert @dossier_item_definition.errors.details[:definition].any?
  end

  test "items of a dossier definition are order by position" do
    last_position = @dossier_definition.items.max_by(&:position)&.position || 0
    dossier_item_definition_new = DossierItemDefinition.create!(definition: @dossier_definition, name: "Test", position: last_position + 1, content_type: ContentTypes::Integer.to_s)
    assert_equal dossier_item_definition_new, @dossier_definition.items.last

    @dossier_item_definition.update!(position: last_position + 2)
    assert_equal @dossier_item_definition, @dossier_definition.items.reload.last
  end

  test "dossier item definition should have a label for rails admin" do
    assert_not_empty @dossier_item_definition.rails_admin_label
  end
end
