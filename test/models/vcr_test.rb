require "test_helper"

class VcrTest < ActiveSupport::TestCase
  test "tests should NEVER be allowed to make external requests" do
    assert_raises VCR::Errors::UnhandledHTTPRequestError do
      response = RestClient.get("https://google.de")
      assert_equal 200, response.code
    end
  end
end
