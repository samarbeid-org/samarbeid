require "test_helper"

class AutomationTest < ActiveSupport::TestCase
  def setup
    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    @assignee = User.find_by!(email: "dsb-boss@example.org")

    @daily_schedule = {rule: "daily", start: "2024-03-04", interval: 1, validations: {}}
    @weekly_schedule = {rule: "weekly", start: "2024-03-04", interval: 1, validations: {day: [1, 3]}}
    @monthly_schedule = {rule: "monthly", start: "2024-03-04", interval: 1, validations: {day_of_month: "last"}}
    @yearly_schedule = {rule: "yearly", start: "2024-03-04", interval: 1, validations: {}}
  end

  test "it should be possible to create a valid automation" do
    assert_difference -> { Automation.count }, 4 do
      Automation.create!(
        title: "Daily Automation",
        workflow_definition: @workflow_definition,
        candidate_assignee: @assignee,
        schedule: @daily_schedule,
        active: true
      )

      Automation.create!(
        title: "Weekly Automation",
        workflow_definition: @workflow_definition,
        candidate_assignee: @assignee,
        schedule: @weekly_schedule,
        active: true
      )

      Automation.create!(
        title: "Monthly Automation",
        workflow_definition: @workflow_definition,
        candidate_assignee: @assignee,
        schedule: @monthly_schedule,
        active: true
      )

      Automation.create!(
        title: "Yearly Automation",
        workflow_definition: @workflow_definition,
        candidate_assignee: @assignee,
        schedule: @yearly_schedule,
        active: true
      )
    end
  end

  test "it shouldn't be possible to create an automation with an invalid schedule" do
    new_automation = Automation.new(
      title: "Daily Automation",
      workflow_definition: @workflow_definition,
      candidate_assignee: @assignee,
      schedule: {rule: "wrong_rule", start: "", interval: 0, validations: nil},
      active: true
    )

    assert_no_difference -> { Automation.count } do
      assert_not new_automation.save
    end

    assert new_automation.errors.details[:"schedule.rule"].any?
    assert new_automation.errors.details[:"schedule.start"].any?
    assert new_automation.errors.details[:"schedule.interval"].any?
    assert new_automation.errors.details[:"schedule.validations"].any?
  end

  test "schedule hash is cleaned up on save" do
    new_automation = Automation.create!(
      title: "Daily Automation",
      workflow_definition: @workflow_definition,
      candidate_assignee: @assignee,
      schedule: @daily_schedule.merge({additional_key: "test"}),
      active: true
    )

    refute_includes new_automation.schedule.keys, "additional_key"
  end

  test "next_execution is updated on save" do
    Timecop.freeze(2024, 3, 4, 10) do
      new_automation = Automation.create!(
        title: "Daily Automation",
        workflow_definition: @workflow_definition,
        candidate_assignee: @assignee,
        schedule: @daily_schedule.merge({start: "2024-03-07"}),
        active: true
      )

      assert_equal Date.parse("2024-03-07"), new_automation.next_execution
    end
  end

  test "validate_with_defaults set defaults for active and schedule" do
    new_automation = Automation.validate_with_defaults(
      title: "Daily Automation",
      workflow_definition: @workflow_definition
    )

    refute new_automation.errors.any?
    assert new_automation.active
    assert_equal({start: Date.today.to_s, interval: 1, rule: "daily", validations: {}}.with_indifferent_access, new_automation.schedule.with_indifferent_access)

    new_automation = Automation.validate_with_defaults(
      title: "Daily Automation",
      workflow_definition: @workflow_definition,
      schedule: @weekly_schedule,
      active: false
    )

    refute new_automation.errors.any?
    assert new_automation.active
    assert_equal(@weekly_schedule.with_indifferent_access, new_automation.schedule.with_indifferent_access)
  end

  test "ice_cube_schedule returns an object of IceCube::Schedule" do
    automation = Automation.find_by!(title: "event-test-automation")
    ice_cube_schedule = automation.ice_cube_schedule
    assert ice_cube_schedule.is_a?(IceCube::Schedule), "ice_cube_schedule result should be an IceCube::Schedule but is of type: #{ice_cube_schedule.class}"
  end

  test "get_next_execution returns Time" do
    automation = Automation.find_by!(title: "event-test-automation")

    Timecop.freeze(2024, 3, 4, Automation::EXECUTION_TIME.hour - 1) do
      next_execution_time = automation.get_next_execution
      assert next_execution_time.is_a?(Time), "get_next_execution result should be a Time but is of type: #{next_execution_time.class}"
    end

    Timecop.freeze(2024, 3, 4, Automation::EXECUTION_TIME.hour + 1) do
      next_execution_time = automation.get_next_execution
      assert next_execution_time.is_a?(Time), "get_next_execution result should be a Time but is of type: #{next_execution_time.class}"
    end
  end
end
