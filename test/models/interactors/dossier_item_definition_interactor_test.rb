require "test_helper"

class Interactors::DossierItemDefinitionInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_item_definition = @dossier_definition.items.find_by!(name: "Url")
    @user = User.first
  end

  test "#create should create a dossier item definition at the last position" do
    new_dossier_item_definition = nil
    last_position = @dossier_definition.items.max_by(&:position)&.position

    assert_difference -> { DossierItemDefinition.count } do
      assert_difference -> { Events::ChangedStructureEvent.count } do
        new_dossier_item_definition = Interactors::DossierItemDefinitionInteractor.create(
          @dossier_definition,
          {name: "test dossier definition name", content_type: "text"}, @user
        )
      end
    end

    assert_operator new_dossier_item_definition.position, :>, last_position
    assert_includes @dossier_definition.items, new_dossier_item_definition
  end

  test "#update name and recommended of dossier item definition should work" do
    assert_difference -> { Events::ChangedStructureEvent.count } do
      Interactors::DossierItemDefinitionInteractor.update(@dossier_item_definition, {
        name: "updated name",
        recommended: true
      }, @user)
    end

    assert_equal "updated name", @dossier_item_definition.name
    assert @dossier_item_definition.recommended
  end

  test "#update shouldn't activate required and unique state or set content_type or options of dossier item definition" do
    refute @dossier_item_definition.required
    refute @dossier_item_definition.unique

    assert_no_difference -> { Events::ChangedStructureEvent.count } do
      Interactors::DossierItemDefinitionInteractor.update(@dossier_item_definition, {
        content_type: ContentTypes::String.to_s,
        options: {bla: "blub"},
        required: true,
        unique: true
      }, @user)
    end

    assert @dossier_item_definition.errors.details[:content_type].any?
    assert @dossier_item_definition.errors.details[:options].any?
    assert @dossier_item_definition.errors.details[:required].any?
    assert @dossier_item_definition.errors.details[:unique].any?
  end

  test "#update should deactivate required and unique state of dossier item definition" do
    @dossier_item_definition.update!(required: true, unique: true)

    assert_difference -> { Events::ChangedStructureEvent.count } do
      Interactors::DossierItemDefinitionInteractor.update(@dossier_item_definition, {
        required: false,
        unique: false
      }, @user)
    end

    refute @dossier_item_definition.errors.any?
  end

  test "#destroy should delete a dossier item definition, remove item from title and subtile of dossier definition and delete dossier item" do
    @dossier_definition.title_fields |= [@dossier_item_definition.id]
    @dossier_definition.subtitle_fields |= [@dossier_item_definition.id]
    @dossier_definition.save!

    dossier = @dossier_definition.dossiers.first
    assert dossier.dossier_items.find_by(dossier_item_definition: @dossier_item_definition)

    assert_difference -> { DossierItemDefinition.count }, -1 do
      assert_difference -> { Events::ChangedStructureEvent.count } do
        Interactors::DossierItemDefinitionInteractor.destroy(@dossier_item_definition, @user)
      end
    end

    assert_empty @dossier_item_definition.errors
    refute_includes @dossier_definition.title_fields, @dossier_item_definition.id
    refute_includes @dossier_definition.subtitle_fields, @dossier_item_definition.id
    assert_nil dossier.reload.dossier_items.find_by(dossier_item_definition: @dossier_item_definition)
  end

  test "#move should reorder the items of a dossier definition by the attribute position" do
    # move from index 1 to index 2
    item_ids_order_old = DossierItemDefinition.where(definition: @dossier_definition).order(:position).map(&:id)
    items_count = item_ids_order_old.length
    assert_operator items_count, :>=, 4
    assert_equal @dossier_item_definition.id, item_ids_order_old[1]
    Interactors::DossierItemDefinitionInteractor.move(@dossier_item_definition, 2, @user)
    item_ids_order_act = @dossier_definition.items.reload.map(&:id)
    item_ids_order_exp = item_ids_order_old.dup
    item_ids_order_exp[1], item_ids_order_exp[2] = item_ids_order_exp[2], item_ids_order_exp[1]
    assert_equal item_ids_order_exp, item_ids_order_act

    # move from index 2 to first position
    item_ids_order_old = item_ids_order_act
    Interactors::DossierItemDefinitionInteractor.move(@dossier_item_definition, 0, @user)
    item_ids_order_act = @dossier_definition.items.reload.map(&:id)
    item_ids_order_exp = item_ids_order_old.dup
    item_ids_order_exp[0], item_ids_order_exp[1], item_ids_order_exp[2] = item_ids_order_exp[2], item_ids_order_exp[0], item_ids_order_exp[1]
    assert_equal item_ids_order_exp, item_ids_order_act

    # move from index 0 to last position
    item_ids_order_old = item_ids_order_act
    Interactors::DossierItemDefinitionInteractor.move(@dossier_item_definition, items_count, @user)
    item_ids_order_act = @dossier_definition.items.reload.map(&:id)
    item_ids_order_exp = item_ids_order_old.dup
    first_id = item_ids_order_exp.shift
    item_ids_order_exp.push(first_id)
    assert_equal item_ids_order_exp, item_ids_order_act
  end
end
