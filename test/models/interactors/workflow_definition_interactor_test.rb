require "test_helper"

class Interactors::WorkflowDefinitionInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")
    @workflow_definition = WorkflowDefinition.find_by!(name: "Bürgeranfrage")
  end

  test "creating definitions should also create an event" do
    assert_difference -> { WorkflowDefinition.count } do
      assert_difference -> { Events::CreatedEvent.count } do
        Interactors::WorkflowDefinitionInteractor.create({name: "A new name", groups: Group.all}, @user)
      end
    end
  end

  test "creating definitions should also create a first task" do
    assert_difference -> { WorkflowDefinition.count } do
      assert_difference -> { TaskDefinition.count } do
        Interactors::WorkflowDefinitionInteractor.create({name: "A new name", groups: Group.all}, @user)
      end
    end
  end

  test "#create_from_instance should create definition with event and copy structure from an instance" do
    workflow = Workflow.find_by!(title: "PROCESS_WITH_TASKS_AND_BLOCKS_WITH_TASKS")

    assert_difference -> { TaskItemDefinition.count }, workflow.all_tasks.map(&:task_items).flatten.count do
      assert_difference -> { TaskDefinition.count }, workflow.all_tasks.count do
        assert_difference -> { BlockDefinition.count }, workflow.blocks.count do
          assert_difference -> { ContentItemDefinition.count }, workflow.content_items.count do
            assert_difference -> { WorkflowDefinition.count } do
              assert_difference -> { Events::CreatedEvent.count } do
                Interactors::WorkflowDefinitionInteractor.create_from_instance(workflow, {name: "A new name", groups: Group.all}, @user)
              end
            end
          end
        end
      end
    end
  end

  test "updating definitions should work" do
    new_groups = Group.pluck(:id)
    assert_difference -> { Events::ChangedTitleEvent.for_object(@workflow_definition).count } do
      Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {name: "A new name", description: "Something different",
                        group_ids: new_groups}, @user)
    end
    @workflow_definition.reload

    assert_equal "A new name", @workflow_definition.name
    assert_equal "Something different", @workflow_definition.description
    assert_equal Group.all.sort, @workflow_definition.groups.sort
  end

  test "updating start_item_definition_ids should work" do
    new_item_defs = @workflow_definition.content_item_definitions.map(&:id)
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {start_item_definition_ids: new_item_defs}, @user)
    assert_equal new_item_defs, @workflow_definition.start_item_definitions.map(&:id)
    assert_equal 0, @workflow_definition.content_item_definitions.where.not(start_item_position: nil).where.not(id: new_item_defs).count

    new_item_defs = new_item_defs.first(3).rotate
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {start_item_definition_ids: new_item_defs}, @user)
    assert_equal new_item_defs, @workflow_definition.start_item_definitions.map(&:id)
    assert_equal 0, @workflow_definition.content_item_definitions.where.not(start_item_position: nil).where.not(id: new_item_defs).count
  end

  test "updating title_item_definition_ids should work" do
    new_item_defs = @workflow_definition.content_item_definitions.map(&:id)
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {title_item_definition_ids: new_item_defs}, @user)
    assert_equal new_item_defs, @workflow_definition.title_item_definitions.map(&:id)
    assert_equal 0, @workflow_definition.content_item_definitions.where.not(title_position: nil).where.not(id: new_item_defs).count

    new_item_defs = new_item_defs.first(3).rotate
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {title_item_definition_ids: new_item_defs}, @user)
    assert_equal new_item_defs, @workflow_definition.title_item_definitions.map(&:id)
    assert_equal 0, @workflow_definition.content_item_definitions.where.not(title_position: nil).where.not(id: new_item_defs).count
  end

  test "destroying definitions should work only if no instances exist" do
    workflow_definition = WorkflowDefinition.create!(name: "Empty", groups: Group.all)
    assert_difference -> { WorkflowDefinition.count }, -1 do
      assert_difference -> { Events::DeletedEvent.count } do
        Interactors::WorkflowDefinitionInteractor.destroy(workflow_definition, @user)
      end
    end

    create_workflow_for!(@workflow_definition)
    assert_no_difference -> { WorkflowDefinition.count } do
      assert_no_difference -> { Events::DeletedEvent.count } do
        Interactors::WorkflowDefinitionInteractor.destroy(@workflow_definition, @user)
      end
    end
    refute_empty @workflow_definition.errors
  end

  test "updating structure should create appropriate event" do
    workflow = create_workflow_for!(@workflow_definition)
    assert_difference -> { Events::ChangedStructureEventByInstance.for_object(@workflow_definition).count } do
      Interactors::WorkflowDefinitionInteractor.update_structure(workflow, @user)
    end
    changed_structure_event = Events::ChangedStructureEventByInstance.for_object(@workflow_definition).order(:created_at).last
    assert_equal @workflow_definition.reload.version, changed_structure_event.new_workflow_definition_version
    assert_equal workflow, changed_structure_event.workflow_used_for_update
  end

  test "activating/deactivating definition should work" do
    assert_difference -> { Events::DeactivatedEvent.for_object(@workflow_definition).count } do
      assert_changes -> { @workflow_definition.deactivated? }, from: false, to: true do
        Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {active: false}, @user)
      end
    end

    assert_difference -> { Events::ActivatedEvent.for_object(@workflow_definition).count } do
      assert_changes -> { @workflow_definition.deactivated? }, from: true, to: false do
        Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, {active: true}, @user)
      end
    end
  end
end
