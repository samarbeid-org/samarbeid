require "test_helper"

class Interactors::ShareTaskInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @admin = User.find_by!(email: "admin@example.org")
    @user = User.find_by!(email: "srs_user@example.org")

    @users_task = Task.accessible_by(@user.ability).active.first
    @users_task.update!(assignee: @user)
  end

  test "#share should create share_link and notify task assignee" do
    assert_difference -> { @user.notifications.reload.count } do
      assert Interactors::ShareTaskInteractor.share(@users_task, "Somebody external", @admin)
    end

    share_link = ShareLink.find_by(shareable: @users_task)
    assert_not_nil share_link
  end

  test "#share should not notify task assignee if shared by himself" do
    assert_no_difference -> { @user.notifications.reload.count } do
      assert Interactors::ShareTaskInteractor.share(@users_task, "Somebody external", @user)
    end
  end

  test "#complete should complete share_link and notify task assignee" do
    share_link = ShareLink.create!(shareable: @users_task, name: "Somebody external")

    assert_difference -> { @user.notifications.reload.count } do
      assert Interactors::ShareTaskInteractor.complete(share_link)
    end

    assert share_link.reload.completed?
  end
end
