require "test_helper"

class Interactors::WorkflowInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")
    @other_user = User.find_by!(email: "srs_user@example.org")
    @workflow_definition = WorkflowDefinition.first

    @running_workflow = create_workflow_for!(@workflow_definition)
  end

  test "#create should create workflow instance with assignee and data_fields, create events and reindex all tasks" do
    workflow = nil

    data_fields = @workflow_definition.content_item_definitions.filter do |cid|
      cid.content_type.to_s == ContentTypes::Text.to_s
    end.map do |cid|
      [cid.id.to_s, "Initialer Wert für Content Item mit ContentItemDefinition ID=#{cid.id}"]
    end.to_h
    assert data_fields.is_a?(Hash)
    assert_operator data_fields.size, :>, 0

    assert_difference -> { Events::CreatedEvent.count }, @workflow_definition.all_task_definitions.count do
      assert_difference -> { Events::StartedEvent.for_object(workflow).count } do
        assert_difference -> { Workflow.count } do
          assert_enqueued_with(job: ReindexJob) do
            workflow = Interactors::WorkflowInteractor.create(
              @workflow_definition, {title: "Test-Workflow", assignee_id: @other_user.id}, data_fields, nil, @user
            )
          end
        end
      end
    end

    assert_equal @other_user, workflow.assignee
    refute workflow.all_tasks.map(&:assignee).any?

    data_fields.each do |key, value|
      ci = workflow.content_items.find_by!(content_item_definition: key.to_i)
      assert_equal value, ci.value, "DataField für ContentItem mit DefinitionId=#{key} hat falschen Wert."
    end
  end

  test "#create should set task assignee like workflow if there is only one task" do
    @workflow_definition.all_task_definitions.all[1..].each(&:destroy!)
    workflow = Interactors::WorkflowInteractor.create(
      @workflow_definition.reload, {title: "Test-Workflow", assignee_id: @user.id}, nil, nil, @other_user
    )

    assert_equal @user, workflow.all_tasks.first.assignee
  end

  test "#create with predecessor_workflow should create new workflow and set predecessor_workflow and create events" do
    predecessor_workflow = Workflow.first
    workflow = nil

    assert_difference -> { Events::CreatedEvent.count }, @workflow_definition.all_task_definitions.count do
      assert_difference -> { Events::StartedEvent.for_object(workflow).count } do
        assert_difference -> { Events::AddedSuccessorWorkflowEvent.for_object(predecessor_workflow).count } do
          workflow = Interactors::WorkflowInteractor.create(
            @workflow_definition, {title: "Test-Workflow"}, nil, predecessor_workflow, @user
          )
        end
      end
    end

    assert_equal predecessor_workflow, workflow.predecessor_workflow
    assert predecessor_workflow.successor_workflows.include?(workflow)
  end

  test "#create should return an error for definitions without any task" do
    empty_workflow_definition = WorkflowDefinition.create!(name: "Empty definition", groups: Group.all)
    workflow = nil

    assert_no_difference -> { Workflow.count } do
      workflow = Interactors::WorkflowInteractor.create(
        empty_workflow_definition, {title: "Test-Workflow"}, nil, nil, @user
      )
    end

    assert workflow.errors.any?
  end

  test "#create should set title and/or assignee of single_task when creating system_process_definition_single_task" do
    single_task_workflow_definition = WorkflowDefinition.system_process_definition_single_task

    workflow = nil
    assert_difference -> { Workflow.count } do
      workflow = Interactors::WorkflowInteractor.create(
        single_task_workflow_definition, {title: "Test-Task", assignee_id: @other_user.id}, nil, nil, @user
      )
    end

    assert_nil workflow.title_from_db
    assert_nil workflow.assignee

    single_task = workflow.direct_tasks.first
    assert_equal "Test-Task", single_task.name
    assert_equal @other_user, single_task.assignee
  end

  test "#delete should destroy workflow and do everything necessary" do
    @running_workflow.reindex_as_search_document(mode: :sync)
    @running_workflow.reload

    assert_difference -> { Events::DeletedEvent.count } do
      Interactors::WorkflowInteractor.delete(@running_workflow, @user)
    end

    assert @running_workflow.destroyed?
    assert @running_workflow.search_document.destroyed?
  end

  test "#cancel should skip all tasks which need to skip to complete workflow" do
    workflow = Workflow.find_by!(title: "PROCESS_WITH_TASKS_AND_BLOCKS_WITH_TASKS")

    assert workflow.direct_tasks.created.any?
    assert workflow.direct_tasks.active.any?

    condition_not_valid_block = workflow.blocks.first
    refute condition_not_valid_block.condition_valid?
    assert condition_not_valid_block.direct_tasks.created.any?
    assert condition_not_valid_block.direct_tasks.active.any?

    condition_valid_or_no_condition_set_block = workflow.blocks.second
    assert condition_valid_or_no_condition_set_block.condition_valid?
    assert condition_valid_or_no_condition_set_block.direct_tasks.all? { |t| t.created? }

    assert_difference -> { Events::SkippedEvent.count }, 5 do
      assert_enqueued_with(job: ReindexJob) do
        Interactors::WorkflowInteractor.cancel(workflow, @user)
      end
    end

    assert workflow.reload.direct_tasks.all? { |t| t.skipped? }
    assert condition_not_valid_block.reload.direct_tasks.all? { |t| t.closed_or_created? }
    assert condition_valid_or_no_condition_set_block.reload.direct_tasks.all? { |t| t.skipped? }
    assert workflow.completed?
  end

  test "#update_assignee with a user value should set the assignee of the workflow, create an event and reindex all tasks" do
    # assign
    assert_difference -> { Events::AssignedEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: ReindexJob) do
        Interactors::WorkflowInteractor.update_assignee(@running_workflow, @user, @user)
      end
    end

    assert_equal @user, @running_workflow.assignee
  end

  test "#update_assignee with a nil value should remove the assignee of the workflow, create an event and reindex all tasks" do
    @running_workflow.update!(assignee: @user)

    assert_difference -> { Events::UnassignedEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: ReindexJob) do
        Interactors::WorkflowInteractor.update_assignee(@running_workflow, nil, @user)
      end
    end

    assert_nil @running_workflow.assignee
  end

  test "#update_contributors should add and remove users as contributors of the workflow and if necessary, create an event and reindex all tasks" do
    removed_contributor = User.find_by!(email: "srs_user@example.org")
    @running_workflow.contributors << removed_contributor

    assert_difference -> { Events::AddedContributorEvent.for_object(@running_workflow).count } do
      assert_difference -> { Events::RemovedContributorEvent.for_object(@running_workflow).count } do
        assert_enqueued_with(job: ReindexJob) do
          Interactors::WorkflowInteractor.update_contributors(@running_workflow, User.where(id: @user.id), @user)
        end
      end
    end

    assert @running_workflow.contributors.include?(@user)
    assert_not @running_workflow.contributors.include?(removed_contributor)
  end

  test "#update_contributors should remove current_user from the contributors of the workflow and create an event" do
    @running_workflow.contributors << @user
    assert @running_workflow.contributors.include?(@user)

    assert_difference -> { Events::RemovedContributorEvent.for_object(@running_workflow).count } do
      Interactors::WorkflowInteractor.update_contributors(@running_workflow, @running_workflow.contributors - [@user], @user)
    end

    assert_not @running_workflow.contributors.include?(@user)
  end

  test "#update should change the title of the workflow, create an event and reindex all tasks" do
    update_params = {title: "test title"}

    assert_difference -> { Events::ChangedTitleEvent.for_object(@running_workflow).count } do
      assert_enqueued_with(job: ReindexJob) do
        Interactors::WorkflowInteractor.update(@running_workflow, update_params, @user)
      end
    end

    assert_equal update_params[:title], @running_workflow.title
  end

  test "#update the description of the workflow with a mentioning of a user should work" do
    other_user = User.find_by!(email: "srs_user@example.org")

    params = ActionController::Parameters.new({
      workflow: {
        description: "<p>Changed Description <mention m-id=\"#{other_user.id}\" m-type=\"user\"></mention></p>"
      }
    })

    assert_difference -> { Events::ChangedSummaryEvent.for_object(@running_workflow).count } do
      Interactors::WorkflowInteractor.update(@running_workflow, params.require(:workflow).permit(:description), @user)
    end

    assert_equal params[:workflow][:description], @running_workflow.description
    assert Events::ChangedSummaryEvent.for_object(@running_workflow).last.mentioned_users.include?(other_user)
  end

  test "#update shouldn't change the title if definition has auto_generate_title == true" do
    @running_workflow.workflow_definition.update(auto_generate_title: true)

    update_params = {title: "test title"}
    old_title = @running_workflow.title_from_db
    refute_empty update_params[:title], old_title

    Interactors::WorkflowInteractor.update(@running_workflow, update_params, @user)

    assert_equal old_title, @running_workflow.title_from_db
  end

  test "#add_task should add task at end of process and set given parameters" do
    new_task_params = {name: "New Task", assignee_id: @other_user.id}
    new_task = Interactors::WorkflowInteractor.add_task(@running_workflow, new_task_params, @user)

    assert new_task.valid?
    assert_equal new_task_params[:name], new_task.title
    assert_equal @other_user, new_task.assignee
  end

  test "#add_task should set process assignee to current user if it was a single-task-process before" do
    single_task_workflow = Interactors::WorkflowInteractor.create(
      WorkflowDefinition.system_process_definition_single_task,
      {title: "Test-Task", assignee_id: @other_user.id}, nil, nil, @user
    )
    assert_nil single_task_workflow.assignee

    second_task = Interactors::WorkflowInteractor.add_task(single_task_workflow, {name: "New Task", assignee_id: @other_user.id}, @user)

    assert_equal @other_user, second_task.assignee
    assert_equal @user, single_task_workflow.assignee
  end

  test "#add_task or #add_block should remove system_identifier if process was single_task" do
    # add_task
    workflow_single_task = WorkflowDefinition.system_process_definition_single_task.build_workflow
    workflow_single_task.save!
    assert workflow_single_task.system_process_single_task?
    assert workflow_single_task.workflow_definition.system_process_definition_single_task?

    Interactors::WorkflowInteractor.add_task(workflow_single_task, {name: "New Task"}, @user)
    refute workflow_single_task.system_process_single_task?
    assert workflow_single_task.workflow_definition.system_process_definition_single_task?

    # add_block
    workflow_single_task = WorkflowDefinition.system_process_definition_single_task.build_workflow
    workflow_single_task.save!
    assert workflow_single_task.system_process_single_task?
    assert workflow_single_task.workflow_definition.system_process_definition_single_task?

    Interactors::WorkflowInteractor.add_block(workflow_single_task, {title: "New Block"})
    refute workflow_single_task.system_process_single_task?
    assert workflow_single_task.workflow_definition.system_process_definition_single_task?
  end

  test "#add_task when adding a new task to a completed process this process is reactivated" do
    completed_workflow = Workflow.completed.first
    new_task_params = {name: "New Task", description: "New Description", assignee_id: @other_user.id}
    new_task = Interactors::WorkflowInteractor.add_task(completed_workflow, new_task_params, @user)

    assert new_task.valid?
    assert new_task.active?
    assert completed_workflow.active?
  end
end
