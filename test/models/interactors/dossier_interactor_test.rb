require "test_helper"

class Interactors::DossierInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "admin@example.org")
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier = @dossier_definition.dossiers.first
  end

  test "#new should instantiate new, not persisted dossier" do
    dossier = Interactors::DossierInteractor.new(@dossier_definition)
    assert_equal @dossier_definition, dossier.definition
    refute dossier.persisted?

    assert_equal @dossier_definition.items.required_or_recommended.size, dossier.dossier_items.size
    assert_equal @dossier_definition.items.required_or_recommended.pluck(:id).sort, dossier.dossier_items.map(&:dossier_item_definition_id).sort
  end

  test "#create should create dossier from definition with values" do
    assert_equal 1, @dossier_definition.items.required_or_recommended.count
    field = @dossier_definition.items.required_or_recommended.first
    assert_equal ContentTypes::String, field.content_type

    dossier = nil

    assert_difference -> { Events::CreatedEvent.for_object(dossier).count } do
      assert_difference -> { Dossier.count } do
        dossier = Interactors::DossierInteractor.create(@dossier_definition, {field.id => "Test data"}, @user)
      end
    end
  end

  test "#update should change value of a data item when lock_version equals current version" do
    data_items = @dossier.data_items
    refute_empty data_items
    data_item = data_items.find { |di| di.content_type == ContentTypes::String }
    refute_nil data_item
    assert_nil data_item.last_updated
    assert_nil data_item.last_updated_by

    ts_before = Time.current
    assert_changes -> { @dossier.reload.data_items.find { |di| di.content_type == ContentTypes::String }.value } do
      assert_changes -> { Events::ChangedDataEvent.count } do
        Interactors::DossierInteractor.update(@dossier, data_item.item_definition_id, "Test data", data_item.lock_version, @user)
      end
    end

    data_item = @dossier.get_item(DossierItemDefinition.find(data_item.id)).to_data_item # reload
    assert_equal @user, data_item.last_updated_by
    assert data_item.last_updated.between?(ts_before, Time.current)
  end

  test "#update should create error when object is stale (lock_version doesn't equal current version)" do
    data_items = @dossier.data_items
    refute_empty data_items
    data_item = data_items.find { |di| di.content_type == ContentTypes::String }
    refute_nil data_item

    dossier_item = @dossier.get_item(DossierItemDefinition.find(data_item.item_definition_id))
    refute_nil dossier_item

    dossier_item.touch # changes lock_version

    assert_no_changes -> { @dossier.reload.data_items.find { |di| di.content_type == ContentTypes::String }.value } do
      @dossier = Interactors::DossierInteractor.update(@dossier, data_item.item_definition_id, "Test data", data_item.lock_version, @user)
    end

    assert @dossier.errors.any?
  end

  test "#delete should delete a dossier" do
    assert_difference -> { Events::DeletedEvent.count } do
      Interactors::DossierInteractor.delete(@dossier, @user)
    end
    assert @dossier.destroyed?
  end
end
