require "test_helper"

class Interactors::DataItemInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @admin = User.find_by!(email: "admin@example.org")
    @user = User.find_by!(email: "srs_user@example.org")

    @running_workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Veranstaltungsplanung"))
  end

  test "#lock should lock content item" do
    content_item = @running_workflow.content_items.where(content_type: ContentTypes::Text.to_s).first
    task_item = content_item.task_items.first
    refute_nil content_item
    refute_nil task_item
    refute content_item.locked?

    assert_difference -> { content_item.lock_version } do
      assert Interactors::DataItemInteractor.lock(task_item, content_item.lock_version, @user)
    end

    content_item.reload
    assert content_item.locked?
    assert_equal @user, content_item.locked_by
  end

  test "#unlock should unlock content item" do
    content_item = @running_workflow.content_items.where(content_type: ContentTypes::Text.to_s).first
    task_item = content_item.task_items.first
    refute_nil content_item
    refute_nil task_item
    refute content_item.locked?

    content_item.update!(locked_at: Time.now, locked_by: @user)
    assert content_item.locked?
    assert_equal @user, content_item.locked_by

    assert Interactors::DataItemInteractor.unlock(task_item, @user)

    content_item.reload
    refute content_item.locked?
    assert_nil content_item.locked_by
  end

  test "#update on task_item should update the content_item, add contributor and NOT start task if necessary" do
    new_text = "let's fill in something new"
    created_task = @running_workflow.direct_tasks.created.last
    task_item = created_task.task_items.first
    content_item = task_item.content_item

    assert_nil task_item.as_datafield[:last_updated]
    assert_nil task_item.as_datafield[:last_updated_by]

    ts_before = Time.current
    assert_difference -> { Events::ChangedDataEvent.for_object(content_item).count } do
      assert_no_difference -> { Events::StartedEvent.for_object(created_task).count } do
        Interactors::DataItemInteractor.update(task_item, new_text, content_item.lock_version, @user)
      end
    end

    assert_equal new_text, content_item.reload.value
    assert created_task.contributors.include?(@user)
    refute created_task.active?
    assert_equal @user, created_task.assignee

    assert_equal @user, task_item.as_datafield[:last_updated_by]
    assert task_item.as_datafield[:last_updated].between?(ts_before, Time.current)
  end

  test "#update on task_item should work for share_link users" do
    new_text = "let's fill in something new"
    content_item = @running_workflow.content_items.where(content_type: ContentTypes::Text.to_s).first
    task_item = content_item.task_items.first
    share_link = ShareLink.create!(shareable: task_item.task)

    assert_difference -> { Events::ChangedDataEvent.for_object(task_item.content_item).count } do
      assert_no_difference -> { Event.for_object(task_item.task).count } do
        Interactors::DataItemInteractor.update(task_item, new_text, content_item.lock_version, share_link)
      end
    end

    assert_equal new_text, content_item.reload.value
  end
end
