require "test_helper"

class Interactors::DossierDefinitionInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @team_admin_user = User.find_by!(email: "team-admin@example.org")

    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
  end

  test "#create should create a dossier definition" do
    assert_difference -> { DossierDefinition.count } do
      assert_difference -> { Events::CreatedEvent.count } do
        Interactors::DossierDefinitionInteractor.create(
          {name: "test name", description: "test description", group_ids: Group.all.map(&:id)},
          @team_admin_user
        )
      end
    end
  end

  test "#update should update title of dossier definition and create event" do
    assert_difference -> { Events::ChangedTitleEvent.for_object(@dossier_definition).count } do
      Interactors::DossierDefinitionInteractor.update(@dossier_definition, {name: "updated name"}, @team_admin_user)
    end

    assert_equal "updated name", @dossier_definition.name
  end

  test "#destroy shouldn't delete a dossier definition and return an error if the definition has any instances" do
    assert @dossier_definition.dossiers.any?

    assert_no_difference -> { DossierDefinition.count } do
      assert_no_difference -> { Events::DeletedEvent.count } do
        Interactors::DossierDefinitionInteractor.destroy(@dossier_definition, @team_admin_user)
      end
    end

    assert_not_empty @dossier_definition.errors
  end

  test "#destroy should delete a dossier definition if the definition hasn't any instances" do
    @dossier_definition.dossiers.destroy_all
    refute @dossier_definition.dossiers.any?

    assert_difference -> { DossierDefinition.count }, -1 do
      assert_difference -> { Events::DeletedEvent.count } do
        Interactors::DossierDefinitionInteractor.destroy(@dossier_definition, @team_admin_user)
      end
    end

    assert_empty @dossier_definition.errors
  end
end
