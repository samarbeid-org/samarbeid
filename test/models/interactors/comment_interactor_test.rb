require "test_helper"
require "minitest/mock"

class Interactors::CommentInteractorTest < ActiveSupport::TestCase
  def setup
    @current_user = User.find_by!(email: "admin@example.org")
    @any_task = Task.first
  end

  test "#create should create comment and event and add to contributors if necessary" do
    @any_task.update!(contributors: [])

    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { Comment.count } do
        Interactors::CommentInteractor.create("My own comment.", nil, nil, @any_task, @current_user)
      end
    end

    assert @any_task.contributors.include?(@current_user)
  end

  test "#create should add mentioned users if present" do
    any_user = User.first
    Interactors::CommentInteractor.create("mention a user <mention m-id=\"#{any_user.id}\" m-type=\"user\">#{any_user.mention_label}</mention>", nil, nil, @any_task, @current_user)
    created_event = Events::CommentedEvent.for_object(@any_task).last
    assert_equal [any_user], created_event.mentioned_users
  end

  test "#create should add notifications for all previous commenters" do
    dossier = Dossier.find(8)
    user_one, user_two = User.first(2)

    # We assume a model without contributors and assignee
    refute dossier.respond_to?(:contributors)
    refute dossier.respond_to?(:assingee)

    Interactors::CommentInteractor.create("My own comment.", nil, nil, dossier, user_one)
    assert_difference -> { Notification.where(user: user_one).count }, 1 do
      Interactors::CommentInteractor.create("My own comment.", nil, nil, dossier, user_two)
    end
  end

  test "it should be possible to create a comment for a workflow, task, workflow_definition and dossier" do
    object = Task.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", nil, nil, object, @current_user)
      end
    end

    object = Workflow.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", nil, nil, object, @current_user)
      end
    end

    object = WorkflowDefinition.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", nil, nil, object, @current_user)
      end
    end

    object = Dossier.first
    assert_difference -> { Events::CommentedEvent.count } do
      assert_difference -> { object.comments.count } do
        Interactors::CommentInteractor.create("My own comment.", nil, nil, object, @current_user)
      end
    end
  end

  test "update should update comment and event and notify only new mentioned users" do
    event = Events::CommentedEvent.find(9)
    assert_equal @current_user, event.subject

    new_mentioned_user = User.find_by!(email: "dsb-boss@example.org")
    group = Group.find_by!(name: "srs")
    group.users << new_mentioned_user
    assert new_mentioned_user.can?(:read, event.object)
    refute_includes event.mentioned_users, new_mentioned_user

    message = "mention a user <mention m-id=\"#{new_mentioned_user.id}\" m-type=\"user\">#{new_mentioned_user.mention_label}</mention>"

    assert_not_equal [new_mentioned_user], event.mentioned_users

    assert_difference -> { Notification.count }, 1 do
      assert_changes -> { event.comment.message } do
        assert_changes -> { event.comment.updated_at } do
          Interactors::CommentInteractor.update(event, message, nil, nil)
        end
      end
    end

    assert_equal [new_mentioned_user], event.reload.mentioned_users
  end

  test "delete should delete comment message with mentioned_users and mark comment as deleted without notifying anybody" do
    event = Events::CommentedEvent.find_by!(subject: @current_user)

    assert_no_difference -> { Notification.count } do
      assert_difference -> { Comment.not_deleted.count }, -1 do
        Interactors::CommentInteractor.delete(event)
      end
    end

    assert_empty event.mentioned_users
    assert event.comment.deleted?
    assert_nil event.comment.message
  end

  test "create (on task) with time tracking data should increase time_tracking_spent_in_minutes of task" do
    time_tracking_task = Task.find(123)

    assert_difference -> { Events::CommentedEvent.for_object(time_tracking_task).with_time_tracking.count }, 1 do
      assert_difference -> { time_tracking_task.time_tracking_spent_in_minutes }, 60 do
        Interactors::CommentInteractor.create("Add some time tracking", "2023-09-12", 60, time_tracking_task, @current_user)
      end
    end
  end

  test "update (on task) with time tracking data should update time_tracking_spent_in_minutes of task" do
    time_tracking_task = Task.find(123)
    time_tracking_event = Events::CommentedEvent.for_object(time_tracking_task).with_time_tracking.first

    assert_no_difference -> { Events::CommentedEvent.for_object(time_tracking_task).with_time_tracking.count } do
      assert_difference -> { time_tracking_task.time_tracking_spent_in_minutes }, 60 do
        Interactors::CommentInteractor.update(time_tracking_event, "Update some time tracking", "2023-09-12", time_tracking_event.time_spent_in_minutes + 60)
      end
    end
  end

  test "delete comment (on task) with time tracking data should reduce time_tracking_spent_in_minutes of task" do
    time_tracking_task = Task.find(123)
    time_tracking_event = Events::CommentedEvent.for_object(time_tracking_task).with_time_tracking.first
    time_spent_in_minutes = time_tracking_event.time_spent_in_minutes

    assert_difference -> { Events::CommentedEvent.for_object(time_tracking_task).with_time_tracking.count }, -1 do
      assert_difference -> { time_tracking_task.time_tracking_spent_in_minutes }, -time_spent_in_minutes do
        Interactors::CommentInteractor.delete(time_tracking_event)
      end
    end
  end

  test "#create should call persist_inline_images of Services::RichtextImageStorage" do
    new_value = "test"

    mocked_persist_method = Minitest::Mock.new
    mocked_persist_method.expect :call, new_value, [new_value, ActiveStorage::Attached::Many]

    Services::RichtextImageStorage.stub :persist_inline_images, mocked_persist_method do
      Interactors::CommentInteractor.create(new_value, nil, nil, @any_task, @current_user)
    end

    mocked_persist_method.verify
  end

  test "#update should call purge_unused_images and persist_inline_images of Services::RichtextImageStorage" do
    event = Events::CommentedEvent.find(9)

    new_value = "test"

    mocked_purge_method = Minitest::Mock.new
    mocked_purge_method.expect :call, nil, [new_value, ActiveStorage::Attached::Many]

    mocked_persist_method = Minitest::Mock.new
    mocked_persist_method.expect :call, new_value, [new_value, ActiveStorage::Attached::Many]

    Services::RichtextImageStorage.stub :purge_unused_images, mocked_purge_method do
      Services::RichtextImageStorage.stub :persist_inline_images, mocked_persist_method do
        Interactors::CommentInteractor.update(event, new_value, nil, nil)
      end
    end

    mocked_purge_method.verify
    mocked_persist_method.verify
  end

  test "#delete should call purge_unused_images of Services::RichtextImageStorage" do
    event = Events::CommentedEvent.find(9)

    mocked_purge_method = Minitest::Mock.new
    mocked_purge_method.expect :call, nil, [nil, ActiveStorage::Attached::Many]

    Services::RichtextImageStorage.stub :purge_unused_images, mocked_purge_method do
      Interactors::CommentInteractor.delete(event)
    end

    mocked_purge_method.verify
  end
end
