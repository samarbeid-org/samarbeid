require "test_helper"

class Interactors::TaskInteractorTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @admin = User.find_by!(email: "admin@example.org")
    @user = User.find_by!(email: "srs_user@example.org")

    @running_workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Veranstaltungsplanung"))
    @task = @running_workflow.direct_tasks.active.first
  end

  test "#update_assignee with a user value should set the assignee of the task, add contributor if necessary and create an event" do
    assert_difference -> { Events::AssignedEvent.for_object(@task).count } do
      Interactors::TaskInteractor.update_assignee(@task, @user, @user)
    end

    assert_equal @user, @task.assignee
    assert @task.contributors.include?(@user)
  end

  test "#update_assignee with a nil value should clear the assignee of the task, add contributor if necessary and create an event" do
    @task.update!(assignee: @user)

    assert_difference -> { Events::UnassignedEvent.for_object(@task).count } do
      Interactors::TaskInteractor.update_assignee(@task, nil, @user)
    end

    assert_nil @task.assignee
    assert @task.contributors.include?(@user)
  end

  test "#update_contributors should add and remove users as contributors of the task if necessary and create an event" do
    @task.contributors << @admin

    assert_difference -> { Events::AddedContributorEvent.for_object(@task).count } do
      assert_difference -> { Events::RemovedContributorEvent.for_object(@task).count } do
        assert_difference -> { @user.notifications.count } do # Notify manually added contributor
          Interactors::TaskInteractor.update_contributors(@task, User.where(id: @user.id), @admin)
        end
      end
    end

    assert @task.contributors.include?(@user)
    assert_not @task.contributors.include?(@admin)
  end

  test "#update_contributors should remove current_user from the contributors of the task and create an event" do
    @task.contributors << @user
    assert @task.contributors.include?(@user)

    assert_difference -> { Events::RemovedContributorEvent.for_object(@task).count } do
      Interactors::TaskInteractor.update_contributors(@task, @task.contributors - [@user], @user)
    end

    assert_not @task.contributors.include?(@user)
  end

  test "#update_due_at should change the due_at attribute of the task, add contributor if necessary and create an event" do
    new_due_date = Date.today

    assert_difference -> { Events::ChangedDueDateEvent.for_object(@task).count } do
      Interactors::TaskInteractor.update_due_at(@task, new_due_date.to_s, @user)
    end

    assert_equal new_due_date, @task.due_at
    assert @task.contributors.include?(@user)
  end

  test "#update_time_tracking_budget should change time budget of task, create event and add contributor if necessary" do
    assert_difference -> { Events::ChangedTimeTrackingBudgetEvent.for_object(@task).count } do
      assert_difference -> { @task.time_tracking_budget_in_minutes }, 60 do
        Interactors::TaskInteractor.update_time_tracking_budget(@task, @task.time_tracking_budget_in_minutes + 60, @user)
      end
    end

    assert @task.contributors.include?(@user)
  end

  test "#complete should complete the task and create an event" do
    assert_difference -> { Events::CompletedEvent.for_object(@task).count } do
      Interactors::TaskInteractor.complete(@task, @user)
    end

    assert @task.reload.completed?
  end

  test "#start should start the task and create an event (and only one)" do
    created_task = @running_workflow.direct_tasks.created.first
    assert_difference -> { Events::StartedEvent.for_object(created_task).count } do
      Interactors::TaskInteractor.start(created_task, @user)
    end
    assert_equal @user, Events::StartedEvent.for_object(created_task).first.subject
    assert created_task.reload.active?
  end

  test "task#snooze should work as expected" do
    Interactors::TaskInteractor.snooze(@task, 3.days.from_now.to_s, @user)
    assert @task.reload.snoozed?
    refute @task.start_at.nil?
    assert @task.errors.none?

    Interactors::TaskInteractor.snooze(@task, 2.days.from_now.to_s, @user)
    assert @task.reload.snoozed?
    refute @task.start_at.nil?
    assert @task.errors.none?

    Interactors::TaskInteractor.snooze(@task, 5.days.from_now.to_s, @user)
    assert @task.reload.snoozed?
    refute @task.start_at.nil?
    assert @task.errors.none?

    # Unsnooze by deleting start_at (only possible if previously set)
    Interactors::TaskInteractor.snooze(@task, nil, @user)
    assert @task.reload.active?
    assert @task.start_at.nil?
    assert @task.errors.none?

    # Setting no start_at is not allowed in general
    Interactors::TaskInteractor.snooze(@task, nil, @user)
    refute @task.reload.snoozed?
    assert @task.start_at.nil?
    refute @task.errors.none?

    # Only allow setting to future
    Interactors::TaskInteractor.snooze(@task, 3.days.ago.to_s, @user)
    refute @task.reload.snoozed?
    assert @task.start_at.nil?
    refute @task.errors.none?
  end

  test "#mark should mark my task" do
    Interactors::TaskInteractor.update_marked(@task, true, @user)
    assert @task.reload.marked?(@user)
    assert_nothing_raised do
      Interactors::TaskInteractor.update_marked(@task, true, @user)
    end
    Interactors::TaskInteractor.update_marked(@task, false, @user)
    refute @task.reload.marked?(@user)
  end

  test "#update name" do
    old_name = @task.name
    params = ActionController::Parameters.new({
      task: {
        name: "New task name"
      }
    })

    assert_difference -> { Events::ChangedTitleEvent.for_object(@task).count } do
      Interactors::TaskInteractor.update(@task, params.require(:task).permit(:name), @task.lock_version, @user)
    end

    assert_equal params[:task][:name], @task.name
    assert_equal params[:task][:name], Events::ChangedTitleEvent.for_object(@task).last.new_title
    assert_equal old_name, Events::ChangedTitleEvent.for_object(@task).last.old_title
  end

  test "#delete should delete task and do everything necessary" do
    assert_enqueued_with(job: ReindexJob) do
      assert_difference -> { Events::DeletedEvent.for_object(@task).count } do
        Interactors::TaskInteractor.delete(@task, @user)
      end
    end
    assert @task.destroyed?
    assert_empty @task.errors
    assert @task.task_items.all?(&:destroyed?)
    assert @task.task_items.map(&:content_item).all?(&:destroyed?)
  end

  test "#delete should not allow deleting tasks with fields used in blocks" do
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch"))
    task = workflow.items.find { |i| i.title == "Entscheidung Löschung" }
    Interactors::TaskInteractor.delete(task, @user)

    refute task.destroyed?
    assert_not_empty task.errors
  end

  test "#delete should not allow deleting the last task of a process" do
    workflow = Workflow.find(26)
    assert_equal 1, workflow.all_tasks.count

    task = workflow.all_tasks.first
    Interactors::TaskInteractor.delete(task, @user)

    refute task.destroyed?
    assert_not_empty task.errors
  end

  test "#delete should also delete content_items if they are the only ones left" do
    workflow = Workflow.find(21)
    assert workflow.content_items.any?

    # Add task without content_items and delete others
    last_position = workflow.items.max_by(&:position)&.position || 0
    empty_task = workflow.direct_tasks.create(name: "Empty Task", workflow: workflow, position: last_position + 10)

    workflow.all_tasks.where.not(id: empty_task.id).each do |task|
      Interactors::TaskInteractor.delete(task.reload, @user)
      assert task.destroyed?
      assert_empty task.errors
    end

    refute workflow.content_items.any?
  end

  test "#add_data_field should work as expected" do
    workflow = Workflow.find_by!(title: "srs-workflow")
    task = workflow.direct_tasks.first
    content_item = {label: "Testdatafield", content_type: "text"}

    Interactors::TaskInteractor.add_data_field(task, content_item, {}, @user)

    assert task.task_items.last.content_item.label == "Testdatafield"
    assert task.task_items.last.content_item.content_type == ContentTypes::Text
    refute task.task_items.last.required
    refute task.task_items.last.info_box
  end

  test "#move_to_other_process should work as expected" do
    assert_no_difference -> { Events::SimpleActionEvent.for_object(@task).count } do
      task = Interactors::TaskInteractor.move_to_other_process(@task, nil, @user)
      refute task.errors.any?
    end

    assert_difference -> { Events::SimpleActionEvent.for_object(@task).count } do
      Interactors::TaskInteractor.move_to_other_process(@task, Workflow.accessible_by(@user.ability, :manage).where.not(id: @task.workflow.id).first, @user)
    end

    workflow_not_accessible_by_user = Workflow.where.not(id: Workflow.accessible_by(@user.ability, :read).pluck(:id)).first
    assert_raises(CanCan::AccessDenied) do
      Interactors::TaskInteractor.move_to_other_process(@task, workflow_not_accessible_by_user, @user)
    end
  end
end
