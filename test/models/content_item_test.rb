require "test_helper"
require "minitest/mock"

class ContentItemTest < ActiveSupport::TestCase
  def setup
    workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch"))

    @new_task = workflow.items.first
    @new_content_item = @new_task.task_items.map(&:content_item).find { |ci| ci.content_type == ContentTypes::Email }
  end

  test "content item should be undefined if value is changed" do
    content_item = ContentItem.where(content_type: ContentTypes::String.to_s).confirmed.first
    content_item.value = "#{content_item.value} let's change it a littel."
    assert content_item.undefined?

    content_item.save!
    assert content_item.reload.undefined?
  end

  test "content items should by default be undefined" do
    assert @new_content_item.undefined?
  end

  test "content items may be confirmed" do
    @new_content_item.confirm!
    assert @new_content_item.reload.confirmed?
  end

  test "empty content items should be confirmed when task completes" do
    start_and_complete(@new_task)

    assert @new_content_item.reload.confirmed?
  end

  test "content items with value should be confirmed when task completes" do
    @new_content_item.value = "some-email@example.com"
    @new_content_item.save!

    assert @new_content_item.undefined?
    start_and_complete(@new_task)

    assert @new_content_item.reload.confirmed?
  end

  test "a content_items confirmed_at is set/unset" do
    Timecop.freeze do
      @new_content_item.confirm!
      assert_equal Time.now, @new_content_item.confirmed_at
    end

    @new_content_item.change!
    refute @new_content_item.confirmed_at
  end

  test "a confirmed content_item validates values correctly confirmed or not" do
    @new_content_item.value = nil
    assert @new_content_item.valid?

    @new_content_item.value = ""
    assert @new_content_item.valid?

    @new_content_item.value = "not a email"

    refute @new_content_item.valid?
    @new_content_item.value = "a-email@example.org"
    assert @new_content_item.valid?

    @new_content_item.confirm!

    @new_content_item.value = "not a email"
    refute @new_content_item.valid?

    @new_content_item.value = "another-email@example.org"
    assert @new_content_item.valid?
  end

  test "setting the value of a content_item should call cast, prepare and serialize methods of the content_type" do
    content_type = @new_content_item.content_type
    new_value = "test"
    casted_value = content_type.cast(new_value)
    prepared_value = content_type.prepare(casted_value, @new_content_item)
    serialized_value = content_type.serialize(prepared_value)

    mocked_cast_method = Minitest::Mock.new
    mocked_cast_method.expect :call, casted_value, [new_value]

    mocked_prepare_method = Minitest::Mock.new
    mocked_prepare_method.expect :call, prepared_value, [casted_value, @new_content_item]

    mocked_serialize_method = Minitest::Mock.new
    mocked_serialize_method.expect :call, serialized_value, [prepared_value]

    @new_content_item.content_type.stub :serialize, mocked_serialize_method do
      @new_content_item.content_type.stub :prepare, mocked_prepare_method do
        @new_content_item.content_type.stub :cast, mocked_cast_method do
          @new_content_item.value = new_value
        end
      end
    end
    mocked_serialize_method.verify
    mocked_prepare_method.verify
    mocked_cast_method.verify
  end

  test "lock_with! should lock content_item or raise error" do
    user1 = User.find_by!(email: "dsb@example.org")
    user2 = User.find_by!(email: "admin@example.org")
    refute @new_content_item.locked?

    # initial lock
    assert @new_content_item.lock_with!(@new_content_item.lock_version, user1)
    assert @new_content_item.locked?
    assert_equal user1, @new_content_item.locked_by

    # extend lock
    last_locked_at = @new_content_item.locked_at
    assert @new_content_item.lock_with!(@new_content_item.lock_version, user1)
    assert @new_content_item.locked?
    assert_equal user1, @new_content_item.locked_by
    assert_operator last_locked_at, :<, @new_content_item.locked_at

    # attempt to lock content item with obsolete lock_version
    assert_operator 0, :<, @new_content_item.lock_version
    assert_raise ActiveRecord::StaleObjectError do
      refute @new_content_item.lock_with!(0, user1)
    end

    # attempt to lock already locked content item by other user
    assert_raise LockSupport::LockException do
      refute @new_content_item.lock_with!(@new_content_item.lock_version, user2)
    end
  end

  test "unlock_for! should unlock content_item or raise error" do
    user1 = User.find_by!(email: "dsb@example.org")
    user2 = User.find_by!(email: "admin@example.org")
    refute @new_content_item.locked?
    assert @new_content_item.lock_with!(@new_content_item.lock_version, user1)
    assert @new_content_item.locked?
    assert_equal user1, @new_content_item.locked_by

    # unlock by correct user
    assert @new_content_item.unlock_for!(user1)
    refute @new_content_item.locked?

    # unlock already unlocked content item should work
    assert @new_content_item.unlock_for!(user1)
    refute @new_content_item.locked?

    # attempt to unlock content item locked by other user
    assert @new_content_item.lock_with!(@new_content_item.lock_version, user1)
    assert @new_content_item.reload.locked?
    assert_raise LockSupport::LockException do
      refute @new_content_item.unlock_for!(user2)
    end
  end

  test "a locked content item should release lock after timeout" do
    user = User.find_by!(email: "dsb@example.org")

    Timecop.freeze do
      @new_content_item.lock_with!(@new_content_item.lock_version, user)
      assert @new_content_item.locked?

      Timecop.travel(ContentItem::LOCK_TIMEOUT_IN_SECONDS.seconds)
      refute @new_content_item.locked?
    end
  end

  test "content items may be locked by different records than users" do
    assert @new_content_item.lock_with!(@new_content_item.lock_version, Automation.first!)
  end

  private

  def start_and_complete(task)
    task.start! if task.may_start?
    task.complete!
  end
end
