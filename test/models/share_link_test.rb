require "test_helper"

class ShareLinkTest < ActiveSupport::TestCase
  setup do
    @task = Task.first
  end
  test "share link should generate token on create and should be findable by it" do
    share_link = ShareLink.create!(shareable: @task)
    assert_not_nil share_link.token

    assert_equal share_link.id, ShareLink.find_and_authenticate_by_token(:token, share_link.token).id
  end

  test "share link tokens should be unique" do
    share_link = ShareLink.create!(shareable: @task)
    second_share_link = ShareLink.create!(shareable: Task.where.not(id: @task.id).first)
    assert_raises(ActiveRecord::RecordNotUnique) do
      second_share_link.update!(token: share_link.token)
    end
  end

  test "there may only exist one active share link for a shareable" do
    share_link = ShareLink.create!(shareable: @task)
    assert_raises(ActiveRecord::RecordInvalid) do
      ShareLink.create!(shareable: @task)
    end
    share_link.complete!
    assert_nothing_raised do
      ShareLink.create!(shareable: @task)
    end
  end

  test "share links can only be created for specific classes" do
    assert_raises(ActiveRecord::RecordInvalid) do
      ShareLink.create!(shareable: Workflow.first)
    end
  end

  test "share link should allow access to itself and to a shared task" do
    share_link = ShareLink.create!(shareable: @task)
    assert share_link.can?(:show, share_link)
    assert share_link.can?(:complete, share_link)
    assert share_link.can?(:read, @task)
    assert share_link.can?(:read, @task.task_items.first)
    assert share_link.can?(:lock, @task.task_items.first)
    assert share_link.can?(:unlock, @task.task_items.first)
    assert share_link.can?(:update, @task.task_items.first)
  end

  test "share link should not allow access to shareable after it has been completed or deactivated" do
    share_link = ShareLink.create!(shareable: @task)
    share_link.complete!
    assert share_link.can?(:show, share_link)
    assert_not share_link.can?(:read, @task)
    assert_not share_link.can?(:read, @task.task_items.first)
  end
end
