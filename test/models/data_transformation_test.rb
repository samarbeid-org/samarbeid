require "test_helper"

class DataTransformationTest < ActiveSupport::TestCase
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_item_definition = @dossier_definition.items.find_by!(name: "String")
  end

  test "DataTransformation items should get destroyed when source element gets updated" do
    dossier_item = @dossier_item_definition.dossier_items.first
    assert dossier_item.data_transformation_items.any?

    dossier_item.update!(value: "New value")

    dossier_item.data_transformation_items.reload
    refute dossier_item.data_transformation_items.any?
  end
end
