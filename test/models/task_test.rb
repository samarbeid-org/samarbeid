require "test_helper"

class TaskTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  setup do
    @user = User.find_by!(email: "dsb@example.org")
    @srs_user = User.find_by!(email: "srs_user@example.org")
    @no_access_user = User.find_by!(email: "no_access_user@example.org")

    @workflow_definition = WorkflowDefinition.find_by!(name: "NUR FÜR AUTOMATISIERTE TESTS: DSB Meldung Missbrauch")
    assert @user.can?(:build_instance, @workflow_definition)
  end

  test "User assigned task is not destroyed with user" do
    @user.assigned_tasks.clear
    assert @user.assigned_tasks.none?

    workflow = create_workflow_for!(@workflow_definition)
    task = workflow.direct_tasks.first
    task.update(assignee: @user)

    assert_equal 1, @user.reload.assigned_tasks.count

    @user.destroy
    assert_nil task.reload.assignee
  end

  test "custom scopes should work" do
    assert_nothing_raised do
      Task.current_and_assigned_to(User.first).first
      Task.with_due_in_past.first
    end
  end

  test "current_and_assigned_to scope should work as expected" do
    new_workflow = create_workflow_for!(@workflow_definition)
    active_task = new_workflow.direct_tasks.active.first
    created_task = new_workflow.direct_tasks.created.first

    assert_difference -> { Task.current_and_assigned_to(@user).count } do
      active_task.update!(assignee: @user)
    end

    assert_difference -> { Task.current_and_assigned_to(@user).count } do
      created_task.update!(assignee: @user)
      created_task.start!
    end

    assert_difference -> { Task.current_and_assigned_to(@user).count }, -1 do
      created_task.destroy!
    end

    assert_difference -> { Task.current_and_assigned_to(@no_access_user).count }, 0 do
      task_for_which_user_has_no_access = create_workflow_for!(@workflow_definition).direct_tasks.active.first
      task_for_which_user_has_no_access.update!(assignee: @user)
    end
  end

  test "task should have due_at set automatically if specified" do
    test_time = Time.new(2021)
    Timecop.freeze(test_time) do
      workflow = create_workflow_for!(@workflow_definition)

      task_due_in_6 = workflow.items.first
      task_due_in_24 = workflow.items.second

      assert task_due_in_6.active?
      assert task_due_in_6.due?
      # Currently we only track due_at with daily accuracy. If this changes
      # this assertion should be changed
      Timecop.freeze(test_time - 1.day) { refute task_due_in_6.due? }

      refute task_due_in_24.due_at
      task_due_in_24.assignee = @user
      task_due_in_24.enable!
      assert task_due_in_24.active?
      refute task_due_in_24.due?
      Timecop.freeze(test_time + 2.days) { assert task_due_in_24.due? }
    end
  end

  test "task should have assignee set automatically if candidate_assignee specified" do
    wd = WorkflowDefinition.find(1)
    workflow = create_workflow_for!(wd)
    candidate_assignee = wd.groups.flat_map(&:users).first
    task = workflow.direct_tasks.created.first
    task.task_definition.update!(candidate_assignee: candidate_assignee)

    assert task.assignee.blank?
    assert task.contributors.empty?
    task.enable!
    assert_equal candidate_assignee, task.reload.assignee
    assert_includes task.reload.contributors, candidate_assignee
  end

  test "task should have assignee set to workflow assignee if specified" do
    wd = WorkflowDefinition.find(1)

    workflow = create_workflow_for!(wd)
    workflow_assignee = wd.groups.flat_map(&:users).first
    workflow.update!(assignee: workflow_assignee)
    task = workflow.direct_tasks.created.first
    task.task_definition.update!(candidate_assignee_from_workflow: workflow_assignee)

    assert task.assignee.blank?
    task.enable!
    assert_equal workflow_assignee, task.reload.assignee
  end

  test "triggering state machine should work as expected" do
    workflow = create_workflow_for!(@workflow_definition)
    task = workflow.items.first

    assert task.is_a?(Task) # just in case
    assert task.active?

    task.snooze!
    assert task.snoozed?
    task.start!

    task.complete!
    assert task.completed?
    task.reopen!
    assert task.active?
    task.skip!
    assert task.skipped?
    task.reopen!
    assert task.active?

    # Workflows automatically align state depending on their tasks
    workflow.direct_tasks.each { |task| task.complete! if task.reload.may_complete? }
    assert workflow.reload.completed?
    block_task = workflow.blocks.first.direct_tasks.first
    assert block_task.may_start?
    refute block_task.may_complete?
    refute block_task.may_skip?
    block_task.start!
    assert workflow.reload.active?
    assert block_task.reload.may_complete?
    assert block_task.may_skip?

    # ActiveRecord basic destroy
    task.reload.destroy!
    assert task.destroyed?
  end

  test "new tasks should have their definitions name" do
    workflow = create_workflow_for!(@workflow_definition)
    workflow.all_tasks.each do |task|
      assert_equal task.task_definition.name, task.name
    end
  end

  test "changing a task state with parameter ':skip_engine' should prevent running the state engine for changing state of other workflow items" do
    workflow = create_workflow_for!(@workflow_definition)
    item_expectation = [[Task, :active], [Task, :created], [Block, :created], [Block, :created]]
    assert_equal item_expectation.count, workflow.items.count

    workflow.items.each_with_index do |item, index|
      assert item.is_a?(item_expectation[index].first), "workflow item of type #{item.class.name} at index #{index} is should be of type #{item_expectation[index].first}"
      assert item.send(:"#{item_expectation[index].second}?"), "workflow item at index #{index} is should have state #{item_expectation[index].second}"
    end

    first_task = workflow.direct_tasks.first
    assert first_task.may_skip?
    first_task.skip!(:skip_engine)

    item_expectation[0][1] = :skipped
    workflow.items.each_with_index do |item, index|
      assert item.is_a?(item_expectation[index].first), "workflow item of type #{item.class.name} at index #{index} is should be of type #{item_expectation[index].first}"
      assert item.send(:"#{item_expectation[index].second}?"), "workflow item at index #{index} is should have state #{item_expectation[index].second}"
    end
  end

  test "a non manual started task should automatically get deferred if definition has set deferral_in_days" do
    assert @workflow_definition.item_definitions.take(2).all? { |item| item.is_a?(TaskDefinition) }
    @workflow_definition.item_definitions.first.update!(deferral_in_days: 1)
    @workflow_definition.item_definitions.second.update!(deferral_in_days: 1)

    workflow = nil
    assert_difference -> { Events::SnoozedEvent.count } do
      workflow = create_workflow_for!(@workflow_definition)
    end

    first_task = workflow.items.first
    second_task = workflow.items.second
    assert first_task.snoozed?
    assert first_task.may_complete?

    assert_difference -> { Events::SnoozedEvent.for_object(second_task).count } do
      first_task.complete!
    end

    assert second_task.reload.snoozed?
  end

  test "a manual started task shouldn't get deferred" do
    assert @workflow_definition.item_definitions.take(2).all? { |item| item.is_a?(TaskDefinition) }
    @workflow_definition.item_definitions.second.update!(deferral_in_days: 1)

    workflow = nil
    assert_no_difference -> { Events::SnoozedEvent.count } do
      workflow = create_workflow_for!(@workflow_definition)
    end

    first_task = workflow.items.first
    assert first_task.active?

    second_task = workflow.items.second.reload
    assert second_task.may_start?

    assert_no_difference -> { Events::SnoozedEvent.for_object(second_task).count } do
      assert second_task.start!
    end

    refute second_task.reload.snoozed?
  end

  test "a manual started task should get it's assignee from candidate assignee" do
    workflow = create_workflow_for!(@workflow_definition)
    second_task = workflow.items.second.reload
    workflow.update!(assignee: @user)
    second_task.task_definition.update!(candidate_assignee_from_workflow: true)
    second_task.reload

    assert second_task.may_start?

    second_task.start!(triggered_by: @srs_user)

    assert_equal @user, second_task.reload.assignee
  end

  test "after automatically activation (unsnooze) of a task it shouldn't get deferred if definition has set deferral_in_days" do
    assert @workflow_definition.item_definitions.take(1).all? { |item| item.is_a?(TaskDefinition) }
    @workflow_definition.item_definitions.first.update!(deferral_in_days: 1)

    workflow = create_workflow_for!(@workflow_definition)
    first_task = workflow.items.first
    assert first_task.snoozed?

    first_task.update!(start_at: Time.now - 1.hours)
    Services::SnoozedTasks.find_and_start
    assert first_task.reload.active?
  end

  test "task may have todo counts" do
    task_with_todos = Task.find(152)
    assert_equal [2, 5], task_with_todos.todo_counts

    task_without_todos = Task.find(153)
    assert_nil task_without_todos.todo_counts
  end
end
