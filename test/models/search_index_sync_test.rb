require "test_helper"

class SearchIndexSyncTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  def setup
    @user = User.find_by!(email: "srs_user@example.org")
    @a_new_workflow = create_workflow_for!(WorkflowDefinition.find_by!(name: "Patentanmeldung"))
    @a_new_task = @a_new_workflow.all_tasks.active.first
    @new_tasks_content_item = @a_new_task.task_items.first.content_item
    @a_workflow_definition = WorkflowDefinition.first
    @a_dossier_definition = DossierDefinition.first
  end

  test "workflow should be reindexed when visibility or search  relevant changes occur" do
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) { @a_new_workflow.update(title: "Another new title") }
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) { @a_new_workflow.update(description: "Eine neue Beschreibung") }
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) { Comment.create!(message: "a new comment", author: @user, object: @a_new_workflow) }

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) { @new_tasks_content_item.update(value: "change the value") }
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) { @new_tasks_content_item.destroy }

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow.all_tasks.last]) do
        @a_new_workflow.update(assignee: @user)
      end
    end

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow.all_tasks.last]) do
        @a_new_workflow.update(assignee: nil)
      end
    end

    @a_new_workflow.reload.destroy!

    refute SearchDocument.exists?(searchable: @a_new_workflow)
    refute SearchDocument.exists?(searchable: @a_new_task)
  end

  test "workflow << methods should trigger reindex" do
    # Contributor may have visibility to workflow and task -> ACL update
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow.all_tasks.last]) do
        @a_new_workflow.contributors << @user
      end
    end
  end

  test "workflow = methods should trigger reindex" do
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow.all_tasks.last]) do
        @a_new_workflow.update(contributors: [@user])
      end
    end
  end

  test "tasks should be reindexed when visibility or search relevant changes occur" do
    assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) { @a_new_task.update(title: "Ein neue Titel") }
    assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) { Comment.create!(message: "a new comment", author: @user, object: @a_new_task) }
    assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) { @a_new_task.marked_by_users << @user }
    assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) { @a_new_task.marked_by_users = [] }
  end

  test "tasks AND workflow should be reindexed when visibility relevant changes occur concerning both" do
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) do
        @a_new_task.contributors << @user
      end
    end

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) do
        @a_new_task.contributors = []
      end
    end

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) do
        @a_new_task.update(assignee: @user)
      end
    end

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) do
        @a_new_task.update(assignee: nil)
      end
    end

    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) do
        @a_new_task.update(due_at: Time.now)
      end
    end
  end

  test "task AND workflow should be reindex on taks state change" do
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do # If a user is allowed to see a single task this affects workflow ACL
      assert_enqueued_with(job: ReindexJob, args: [@a_new_task]) do
        @a_new_task.skip!
      end
    end
  end

  test "dossier definitions should trigger reindex of all their dossiers on changes reindex self" do
    assert_enqueued_with(job: ReindexSearchablesJob, args: [@a_dossier_definition, :dossiers]) do
      refute_empty @a_dossier_definition.title_fields
      @a_dossier_definition.update!(title_fields: [])
    end
  end

  test "workflows should be reindexed if the definition has a name change" do
    assert_enqueued_with(job: ReindexSearchablesJob, args: [@a_workflow_definition, :workflows]) do
      @a_workflow_definition.update(name: "A new name")
    end
  end

  test "workflows should be reindexed if their content_items change" do
    content_item = @a_new_workflow.content_items.first
    assert_enqueued_with(job: ReindexJob, args: [@a_new_workflow]) do
      content_item.update(value: "hey - new value which is indexed on workflow")
    end
  end
end
