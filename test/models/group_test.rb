require "test_helper"

class GroupTest < ActiveSupport::TestCase
  test "groups may have several workflow definitions associated but each only once" do
    group = Group.find_by!(name: "tomsy")
    assert_nothing_raised do
      group.workflow_definitions = WorkflowDefinition.all.to_a
    end

    assert_raises ActiveRecord::RecordNotUnique do
      group.workflow_definitions << WorkflowDefinition.first
    end
  end

  test "groups may have several dossier definitions associated but each only once" do
    group = Group.find_by!(name: "tomsy")
    assert_nothing_raised do
      group.dossier_definitions = DossierDefinition.all.to_a
    end

    assert_raises ActiveRecord::RecordNotUnique do
      group.dossier_definitions << DossierDefinition.first
    end
  end

  test "system_group_all should return group all with all users assigned" do
    Group.find_by(system_identifier: :system_group_all).destroy

    group_all = Group.system_group_all
    refute_nil group_all
    assert group_all.system_group_all?
    refute group_all.users.map(&:id).difference(User.all.map(&:id)).any?
  end

  test "there should only be one group with system_identifier equals system_group_all" do
    Group.find_by!(system_identifier: :system_group_all)

    group = Group.create(name: "Test Group", system_identifier: :system_group_all)
    refute group.valid?
    assert group.errors.include?(:system_identifier)
  end
end
