require "test_helper"
require "minitest/mock"

class ContentTypeTest < ActiveSupport::TestCase
  test "content type 'email' should cast, serialized and deserialize input values correctly" do
    # should remove not printable characters while casting input value
    test_content_type(ContentTypes::Email, "irgendwer@​example.org", "irgendwer@example.org")

    # emails with charset from [-a-z0-9ßàÁâãóôþüúðæåïçèõöÿýòäœêëìíøùîûñé]
    test_content_type(ContentTypes::Email, "test+user@test-ßàÁâãóôþüúðæåïçèõöÿýòäœêëìíøùîûñé-domain.org")

    # valid traditional email addresses
    test_content_type(ContentTypes::Email, "Abc@example.com") # (English, ASCII)
    test_content_type(ContentTypes::Email, "Abc.123@example.com") # (English, ASCII)
    test_content_type(ContentTypes::Email, "user+mailbox/department=shipping@example.com") # (English, ASCII)
    test_content_type(ContentTypes::Email, "!#$%&'*+-/=?^_`.{|}~@example.com") # (English, ASCII)
    test_content_type(ContentTypes::Email, '"Abc@def"@example.com') # (English, ASCII)
    test_content_type(ContentTypes::Email, '"Fred\ Bloggs"@example.com') # (English, ASCII)
    test_content_type(ContentTypes::Email, '"Joe.\\Blow"@example.com') # (English, ASCII)

    test_content_type(ContentTypes::Email, "用户@例子.广告") # (Chinese, Unicode)
    test_content_type(ContentTypes::Email, "अजय@डाटा.भारत") # (Hindi, Unicode)
    test_content_type(ContentTypes::Email, "квіточка@пошта.укр") # (Ukrainian, Unicode)
    test_content_type(ContentTypes::Email, "χρήστης@παράδειγμα.ελ") # (Greek, Unicode)
    test_content_type(ContentTypes::Email, "Dörte@Sörensen.example.com") # (German, Unicode)
    test_content_type(ContentTypes::Email, "коля@пример.рф") # (Russian, Unicode)
    test_content_type(ContentTypes::Email, "support@ツッ.com") # (Katakana, Unicode)
  end

  test "content type 'decimal' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Decimal, 99.99, "99.99")
    test_content_type(ContentTypes::Decimal, "99.99")
    test_content_type(ContentTypes::Decimal, BigDecimal("99.99"), "99.99")
    test_content_type(ContentTypes::Decimal, 99, "99.0")
  end

  test "content type 'integer' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Integer, 99)
    test_content_type(ContentTypes::Integer, "99", 99)
  end

  test "content type 'boolean' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Boolean, true)
    test_content_type(ContentTypes::Boolean, false)
    test_content_type(ContentTypes::Boolean, "true", true)
    test_content_type(ContentTypes::Boolean, "false", false)
  end

  test "content type 'date' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Date, "2021-03-01")
    test_content_type(ContentTypes::Date, Date.parse("2021-03-01"), "2021-03-01")
  end

  test "content type 'time' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Time, "14:45")
    test_content_type(ContentTypes::Time, Time.parse("14:45"), "14:45")
  end

  test "content type 'datetime' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Datetime, "2021-04-27T13:25:00")
  end

  test "content type 'dossier' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::Dossier, Dossier.find(1), ["gid://samarbeid/Dossier/1"])
    test_content_type(ContentTypes::Dossier, Dossier.order(:id).find(1, 2), ["gid://samarbeid/Dossier/1", "gid://samarbeid/Dossier/2"])
  end

  test "content type 'file' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::File, UploadedFile.find(4), ["gid://samarbeid/UploadedFile/4"])
    test_content_type(ContentTypes::File, UploadedFile.order(:id).find(4, 7), ["gid://samarbeid/UploadedFile/4", "gid://samarbeid/UploadedFile/7"])
  end

  test "content type 'phone_number' should cast, serialized and deserialize input values correctly" do
    test_content_type(ContentTypes::PhoneNumber, "+49 30 901820")
    test_content_type(ContentTypes::PhoneNumber, "030 901820")
    test_content_type(ContentTypes::PhoneNumber, "+49 1522 3433333")
    test_content_type(ContentTypes::PhoneNumber, "0049 1522 3433333")
    test_content_type(ContentTypes::PhoneNumber, "01522 3433333")
  end

  test "content type 'richtext' should extract number of todos and completed todos correctly" do
    content_item = ContentItem.find(158)
    todos_done, todos_total = ContentTypes::Richtext.extract_done_and_total_todos(content_item.value)
    assert_equal 2, todos_done
    assert_equal 5, todos_total
  end

  test "#update type option of content type dossier should remove inverseReference definition" do
    dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    dossier_item_definition_dossiers = dossier_definition.items.find_by!(name: "Dossiers")

    dossier_item_definition_dossiers.dossier_items.each { |di| di.destroy! }
    dossier_item_definition_dossiers.reload

    assert_equal ContentTypes::Dossier, dossier_item_definition_dossiers.content_type
    assert_equal 1, dossier_item_definition_dossiers.options.with_indifferent_access[:type]
    assert dossier_item_definition_dossiers.options.with_indifferent_access.has_key?("inverseReference")

    new_options = dossier_item_definition_dossiers.options
    new_options["type"] = 2
    dossier_item_definition_dossiers.update!(options: new_options)

    dossier_item_definition_dossiers.reload
    assert_equal 2, dossier_item_definition_dossiers.options.with_indifferent_access[:type]
    refute dossier_item_definition_dossiers.options.with_indifferent_access.has_key?("inverseReference")
  end

  test "prepare of content_type richtext should call Services::RichtextImageStorage methods to purge and persist images" do
    dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    dossier_item_definition_richtext = dossier_definition.items.find_by!(name: "Richtext")
    dossier_item_richtext = dossier_item_definition_richtext.dossier_items.first

    content_type = dossier_item_definition_richtext.content_type
    assert_equal ContentTypes::Richtext.to_s, content_type.to_s

    value = "test"

    mocked_purge_method = Minitest::Mock.new
    mocked_purge_method.expect :call, nil, [value, dossier_item_richtext.richtext_images]

    mocked_persist_method = Minitest::Mock.new
    mocked_persist_method.expect :call, value, [value, dossier_item_richtext.richtext_images]

    Services::RichtextImageStorage.stub :purge_unused_images, mocked_purge_method do
      Services::RichtextImageStorage.stub :persist_inline_images, mocked_persist_method do
        content_type.prepare(value, dossier_item_richtext)
      end
    end

    mocked_purge_method.verify
    mocked_persist_method.verify
  end

  test "localized_string of content_type richtext should return correct text representation" do
    assert_equal "One & Two\n", ContentTypes::Richtext.localized_string("<p>One & <b>Two</b></p>")
  end

  private

  def test_content_type(content_type, value_in, value_stored_in_db = nil, options = {})
    value_stored_in_db = value_in if value_stored_in_db.nil?

    value_casted = content_type.cast(value_in)
    value_serialized = content_type.serialize(value_casted)
    value_deserialized = content_type.deserialize(value_serialized)

    assert_equal value_stored_in_db, value_serialized, "Wrong serialized value of content type '#{content_type}' #{caller_locations(1..1)&.map { |loc| "#{loc.path}:#{loc.lineno}" }}"
    assert_equal value_casted, value_deserialized, "Wrong deserialized value of content type '#{content_type}' #{caller_locations(1..1)&.map { |loc| "#{loc.path}:#{loc.lineno}" }}"

    unless content_type.data_empty?(value_deserialized)
      validation_result = content_type.validate_data(value_deserialized, options)
      assert_equal true, validation_result, "#{value_deserialized} #{validation_result}"
    end
  end
end
