require "test_helper"

class GoodJobScheduleTest < ActiveSupport::TestCase
  include ActiveJob::TestHelper

  test "good job schedule helper methods make schedule easier to read but still creates functional schedules" do
    schedule = SamarbeidGoodJobSchedule.new {}
    job_key, job_hash = schedule.create_job_hash("every 10 minutes", "MyJobClass", :some_arg)

    assert_equal "MyJobClass-some_arg", job_key
    assert job_hash[:cron].match?(/\d{1,2} \*\/10 \* \* \* \*/) # random seconds, every 10 minutes
    assert_equal "MyJobClass", job_hash[:class]
    assert_equal [:some_arg], job_hash[:args]
  end

  test "good job schedule helper methods prevent duplicate job keys" do
    assert_raises(RuntimeError) do
      SamarbeidGoodJobSchedule.new { schedule_job "every 10 minutes", "DeliverRecentNotificationsMailsJob", :immediately }
    end
  end

  test "all scheduled jobs are valid and may be exectued" do
    scheduled_jobs = Rails.application.config.good_job.cron
    assert scheduled_jobs.any?

    scheduled_jobs.each do |job_key, job_hash|
      VCR.insert_cassette("rss_feed") if job_key.match?(/ImportRssFeedJob/)

      assert_nothing_raised do
        job_hash[:class].constantize.perform_now(*job_hash[:args])
      end

      VCR.eject_cassette if job_key.match?(/ImportRssFeedJob/)
    end
  end

  test "task to send hearbeat stats are only enqued if enabled via environment setting" do
    scheduled_jobs = Rails.application.config.good_job.cron
    stats_heartbeat_jobs = scheduled_jobs.select { |job_key, job_hash| job_hash[:class].match "SendStatsHeartbeatJob" }

    assert stats_heartbeat_jobs.empty?

    mock_environment(partial_env_hash: {"SEND_STATS_HEARTBEAT_TO" => "1"}) do
      # Cleanout the good_job schedule and reload it
      Rails.application.config.good_job.cron = {}
      require "#{Rails.root}/config/initializers/good_job_schedule.rb"

      scheduled_jobs = Rails.application.config.good_job.cron
      stats_heartbeat_jobs = scheduled_jobs.select { |job_key, job_hash| job_hash[:class].match "SendStatsHeartbeatJob" }

      assert stats_heartbeat_jobs.present?
    end
  end
end
