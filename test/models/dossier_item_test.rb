require "test_helper"
require "minitest/mock"

class DossierItemTest < ActiveSupport::TestCase
  def setup
    @dossier_definition = DossierDefinition.find_by!(name: "DOSSIER FÜR AUTOMATISIERTE TESTS")
    @dossier_item_definition = @dossier_definition.items.find_by!(name: "String")
    @dossier_item = @dossier_item_definition.dossier_items.first
  end

  test "setting the value of a dossier_item should call cast, prepare and serialize methods of the content_type" do
    content_type = @dossier_item_definition.content_type
    new_value = "test"
    casted_value = content_type.cast(new_value)
    prepared_value = content_type.prepare(casted_value, @dossier_item)
    serialized_value = content_type.serialize(prepared_value)

    mocked_cast_method = Minitest::Mock.new
    mocked_cast_method.expect :call, casted_value, [new_value]

    mocked_prepare_method = Minitest::Mock.new
    mocked_prepare_method.expect :call, prepared_value, [casted_value, @dossier_item]

    mocked_serialize_method = Minitest::Mock.new
    mocked_serialize_method.expect :call, serialized_value, [prepared_value]

    @dossier_item_definition.content_type.stub :serialize, mocked_serialize_method do
      @dossier_item_definition.content_type.stub :prepare, mocked_prepare_method do
        @dossier_item_definition.content_type.stub :cast, mocked_cast_method do
          @dossier_item.value = new_value
        end
      end
    end
    mocked_serialize_method.verify
    mocked_prepare_method.verify
    mocked_cast_method.verify
  end
end
