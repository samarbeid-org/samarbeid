require "test_helper"

class UserTest < ActiveSupport::TestCase
  test "a users mention label should consist of an @ and his name" do
    any_user = User.first
    internal_reference_for_mentioning = any_user.mention_label
    assert internal_reference_for_mentioning.match(any_user.name)
    assert "@", internal_reference_for_mentioning.first
  end

  test "reorder_by_name_with_user_first scope should work" do
    user = User.order(:id).last

    assert_equal user, User.reorder_by_name_with_user_first(user).first
    refute_equal user, User.all.order(:id).first
    assert_equal user, User.all.order(:id).reorder_by_name_with_user_first(user).first
  end

  test "except_users scope should work" do
    assert_equal User.count, User.except_users([]).count

    user_to_exclude = User.first
    refute User.except_users([user_to_exclude.id]).all.include?(user_to_exclude)

    assert_equal 0, User.except_users(User.pluck(:id)).count
  end

  test "filtered_by_groups scope should work" do
    assert_nothing_raised { User.filter_by_groups([]).all }

    group = Group.first
    user = group.users.first
    user.update!(groups: Group.all)

    assert_equal group.users.sort, User.filter_by_groups([group.id]).sort

    assert User.all.any? { |u| u.groups.count > 1 }
    assert_equal Group.all.flat_map { |g| g.users }.map(&:id).uniq.sort, User.filter_by_groups(Group.all.pluck(:id)).pluck(:id).sort

    assert_equal [User.find_by!(email: "admin@example.org").id], User.filter_by_groups(Group.all.pluck(:id), true).search_for_list_with_pg_search("admin@example.org").pluck(:id)
  end

  test "admins should report correctly" do
    team_admins = User.where(role: :team_admin).all
    super_admins = User.where(role: :super_admin).all

    assert team_admins.first.is_admin?
    assert super_admins.first.is_admin?

    assert_equal (team_admins | super_admins).sort, User.admins.sort
  end

  test "unconfirmed or deactivated users should have prefix in fullname attribute" do
    user = User.find_by!(email: "srs_user@example.org")
    assert user.confirmed?
    assert user.activated?
    refute user.fullname.include?("unbestätigt")
    refute user.fullname.include?("deaktiviert")

    user.update(confirmed_at: nil)
    refute user.confirmed?
    assert user.activated?
    assert user.fullname.include?("unbestätigt")
    refute user.fullname.include?("deaktiviert")

    user.update(confirmed_at: Time.current)
    user.deactivate
    assert user.confirmed?
    assert user.deactivated?
    refute user.fullname.include?("unbestätigt")
    assert user.fullname.include?("deaktiviert")

    user.update(confirmed_at: nil)
    refute user.confirmed?
    assert user.deactivated?
    assert user.fullname.include?("unbestätigt")
    assert user.fullname.include?("deaktiviert")
  end

  test "devise method 'set_reset_password_token' should return the plaintext token" do
    subject = User.first

    potential_token = subject.send(:set_reset_password_token)
    potential_token_digest = Devise.token_generator.digest(subject, :reset_password_token, potential_token)
    actual_token_digest = subject.reset_password_token
    assert_equal potential_token_digest, actual_token_digest
  end
end
