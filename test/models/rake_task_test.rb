require "test_helper"

class RakeTaskTest < ActiveSupport::TestCase
  def setup
    Samarbeid::Application.load_tasks
  end

  custom_rake_tasks_to_test = [
    # "db:definitions:dump", TODO: Creates fixtures dir and dumped files - maybe cleanup in test env?
    # "db:fixtures:dump", TODO: Creates dumped file - maybe cleanup in test env?
    "db:anonymize",
    "db:create_super_admin"
  ]

  custom_rake_tasks_to_test.each do |task_to_test|
    test task_to_test do
      assert_nothing_raised { Rake::Task[task_to_test].invoke }
    end
  end
end
