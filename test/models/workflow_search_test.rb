require "test_helper"

class WorkflowSearchTest < ActiveSupport::TestCase
  setup do
    @user = User.find_by_email!("srs_user@example.org")
    @workflow = Workflow.accessible_by(@user.ability).first
  end

  test "#filter_for_index should work" do
    results = Workflow.filter_for_index(@user)
    assert_not_empty results.query
  end

  test "#filter_for_index should work if no instances are present" do
    Workflow.destroy_all

    results = Workflow.filter_for_index(@user)
    assert_empty results.query
  end

  test "#filter_category_scopes should report correctly" do
    workflow = Workflow.first
    assert_equal ["ACTIVE", "WITH_DUE_TASKS", "ALL"], filter_categories_for(workflow, @user)
    workflow.direct_tasks.each { |task| task.complete! if task.reload.may_complete? }
    assert_equal ["ALL"], filter_categories_for(workflow, @user)
  end

  test "#filter_category_counts should report correctly" do
    results = Workflow.filter_for_index(@user)
    assert_equal({"ACTIVE" => 9, "WITH_DUE_TASKS" => 2, "ALL" => 12}, results.filter_category_counts)
  end

  test "#filter_for_index with aggregates and additional_query_params should work" do
    result = Workflow.filter_for_index(@user, @workflow.title, order_by: :created_at_desc, filter_category: "ALL")
    assert_equal [@workflow], result.query.to_a
  end

  test "workflows should be found by their identifier and parts of title" do
    workflow = Workflow.find(13) # srs-worfklow
    queries = ["13", "srs"]
    queries.each do |query|
      assert_equal workflow, Workflow.search_for_list(@user, query).first, "Search by Workflow.search_for_list for '#{query}' did not find correct result"
    end
    assert Workflow.search_for_list(@user, "Patentanmeldung").include?(workflow)
  end

  private

  def filter_categories_for(workflow, user)
    Workflow.filter_category_scopes(user).map { |category, scope_lambda| (scope_lambda.call(Workflow.where(id: workflow.id)).count > 0) ? category : nil }.compact
  end
end
