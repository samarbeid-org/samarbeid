require "test_helper"

class ContentTypeFilterSupportTest < ActiveSupport::TestCase
  def setup
    @user = User.find_by!(email: "admin@example.org")
    @workflow_definition = WorkflowDefinition.find_by!(name: "DSB Premiumanzeige")
    @dossier_definition = DossierDefinition.find_by!(name: "Person")
  end

  test "Filter task and dossiers by fields of content_type 'String', 'Text', 'Richtext', 'Url', 'Email' and 'PhoneNumber' should work" do
    types_with_values = [
      {type: ContentTypes::String, values: ["FirstTestValue", "SecondTestValue"], tests: ["firsttestvalue", "test", "second"]},
      {type: ContentTypes::Text, values: ["FirstTestValue", "SecondTestValue"], tests: ["firsttestvalue", "test", "second"]},
      {type: ContentTypes::Richtext, values: ["FirstTestValue", "SecondTestValue"], tests: ["firsttestvalue", "test", "second"]},
      {type: ContentTypes::Url, values: ["First.Test.Value.org", "Second.Test.Value.org"], tests: ["first.test.value.org", "test", "second"]},
      {type: ContentTypes::Email, values: ["First@Test.Value.org", "Second@Test.Value.org"], tests: ["first@test.value.org", "test", "second"]},
      {type: ContentTypes::PhoneNumber, values: ["+49 000 001", "+49 000 002"], tests: ["+49 000 001", "49", "002"]}
    ]

    types_with_values.each do |type_with_values|
      content_type = type_with_values[:type]

      content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, content_type)
      workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, type_with_values[:values] + [nil])

      dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, content_type)
      dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, type_with_values[:values] + [nil], @user)

      assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
      assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

      assert_filter(workflow_3_id, content_item_definition, :is_empty)
      assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

      assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
      assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

      assert_filter(workflow_1_id, content_item_definition, :is_equal, type_with_values[:tests].first)
      assert_filter(dossier_1_id, dossier_item_definition, :is_equal, type_with_values[:tests].first)

      assert_filter(workflow_2_id, content_item_definition, :is_not_equal, type_with_values[:tests].first)
      assert_filter(dossier_2_id, dossier_item_definition, :is_not_equal, type_with_values[:tests].first)

      assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :does_contain, type_with_values[:tests].second)
      assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :does_contain, type_with_values[:tests].second)

      assert_filter(workflow_1_id, content_item_definition, :does_not_contain, type_with_values[:tests].third)
      assert_filter(dossier_1_id, dossier_item_definition, :does_not_contain, type_with_values[:tests].third)
    end
  end

  test "Filter task and dossiers by fields of content_type 'Date' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Date)
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, ["2024-12-18", "2024-12-31", nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Date)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, ["2024-12-18", "2024-12-31", nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :between, ["2024-12-18", "2024-12-31"])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :between, ["2024-12-18", "2024-12-31"])

    assert_filter(workflow_2_id, content_item_definition, :between, ["2024-12-31", nil])
    assert_filter(dossier_2_id, dossier_item_definition, :between, ["2024-12-31", nil])

    assert_filter(workflow_1_id, content_item_definition, :between, [nil, "2024-12-18"])
    assert_filter(dossier_1_id, dossier_item_definition, :between, [nil, "2024-12-18"])
  end

  test "Filter task and dossiers by fields of content_type 'Time' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Time)
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, ["12:00", "13:00", nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Time)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, ["12:00", "13:00", nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :between, ["12:00", "13:00"])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :between, ["12:00", "13:00"])

    assert_filter(workflow_2_id, content_item_definition, :between, ["13:00", nil])
    assert_filter(dossier_2_id, dossier_item_definition, :between, ["13:00", nil])

    assert_filter(workflow_1_id, content_item_definition, :between, [nil, "12:00"])
    assert_filter(dossier_1_id, dossier_item_definition, :between, [nil, "12:00"])
  end

  test "Filter task and dossiers by fields of content_type 'Datetime' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Datetime)
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, ["2024-12-18T12:00", "2024-12-31T13:00", nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Datetime)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, ["2024-12-18T12:00", "2024-12-31T13:00", nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :between, ["2024-12-18T12:00", "2024-12-31T13:00"])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :between, ["2024-12-18T12:00", "2024-12-31T13:00"])

    assert_filter(workflow_2_id, content_item_definition, :between, ["2024-12-31T13:00", nil])
    assert_filter(dossier_2_id, dossier_item_definition, :between, ["2024-12-31T13:00", nil])

    assert_filter(workflow_1_id, content_item_definition, :between, [nil, "2024-12-18T12:00"])
    assert_filter(dossier_1_id, dossier_item_definition, :between, [nil, "2024-12-18T12:00"])
  end

  test "Filter task and dossiers by fields of content_type 'Integer' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Integer)
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, ["-5", "5", nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Integer)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, ["-5", "5", nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :between, ["-5", "5"])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :between, ["-5", "5"])

    assert_filter(workflow_2_id, content_item_definition, :between, ["5", nil])
    assert_filter(dossier_2_id, dossier_item_definition, :between, ["5", nil])

    assert_filter(workflow_1_id, content_item_definition, :between, [nil, "-5"])
    assert_filter(dossier_1_id, dossier_item_definition, :between, [nil, "-5"])
  end

  test "Filter task and dossiers by fields of content_type 'Decimal' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Decimal)
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, ["-5.5", "5.5", nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Decimal)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, ["-5.5", "5.5", nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :between, ["-5.5", "5.5"])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :between, ["-5.5", "5.5"])

    assert_filter(workflow_2_id, content_item_definition, :between, ["5.5", nil])
    assert_filter(dossier_2_id, dossier_item_definition, :between, ["5.5", nil])

    assert_filter(workflow_1_id, content_item_definition, :between, [nil, "-5.5"])
    assert_filter(dossier_1_id, dossier_item_definition, :between, [nil, "-5.5"])
  end

  test "Filter task and dossiers by fields of content_type 'Boolean' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Boolean)
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, ["true", "false", nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Boolean)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, ["true", "false", nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter(workflow_1_id, content_item_definition, :is_equal, "true")
    assert_filter(dossier_1_id, dossier_item_definition, :is_equal, "true")

    assert_filter(workflow_2_id, content_item_definition, :is_not_equal, "true")
    assert_filter(dossier_2_id, dossier_item_definition, :is_not_equal, "true")

    assert_filter(workflow_2_id, content_item_definition, :is_equal, "false")
    assert_filter(dossier_2_id, dossier_item_definition, :is_equal, "false")

    assert_filter(workflow_1_id, content_item_definition, :is_not_equal, "false")
    assert_filter(dossier_1_id, dossier_item_definition, :is_not_equal, "false")
  end

  test "Filter task and dossiers by fields of content_type 'Selection' should work" do
    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Selection, {
      items: [{text: "value 1", value: 1}, {text: "value 2", value: 2}, {text: "value 3", value: 3}, {text: "value 5", value: 5}],
      multiple: true
    })
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, [[1, 2, 3], [2, 5], nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Selection, {
      items: [{text: "value 1", value: 1}, {text: "value 2", value: 2}, {text: "value 3", value: 3}, {text: "value 5", value: 5}],
      multiple: true
    })
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, [[1, 2, 3], [2, 5], nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter(workflow_1_id, content_item_definition, :is_equal, ["3", "2", "1"])
    assert_filter(dossier_1_id, dossier_item_definition, :is_equal, ["3", "2", "1"])

    assert_filter(workflow_2_id, content_item_definition, :is_not_equal, ["3", "2", "1"])
    assert_filter(dossier_2_id, dossier_item_definition, :is_not_equal, ["3", "2", "1"])

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :does_contain_any, ["2", "4"])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :does_contain_any, ["2", "4"])

    assert_filter(workflow_1_id, content_item_definition, :does_contain_any, ["1"])
    assert_filter(dossier_1_id, dossier_item_definition, :does_contain_any, ["1"])

    assert_filter(workflow_2_id, content_item_definition, :does_not_contain_any, ["1", "3"])
    assert_filter(dossier_2_id, dossier_item_definition, :does_not_contain_any, ["1", "3"])

    assert_filter([], content_item_definition, :does_not_contain_any, ["1", "5"])
    assert_filter([], dossier_item_definition, :does_not_contain_any, ["1", "5"])

    assert_filter(workflow_1_id, content_item_definition, :does_contain_all, ["2", "3"])
    assert_filter(dossier_1_id, dossier_item_definition, :does_contain_all, ["2", "3"])
  end

  test "Filter task and dossiers by fields of content_type 'Dossier' should work" do
    dossier_definition = DossierDefinition.find_by!(name: "Person")
    dossier_value_1 = dossier_definition.dossiers.first
    dossier_value_2 = dossier_definition.dossiers.second
    dossier_value_3 = dossier_definition.dossiers.third
    dossier_value_4 = dossier_definition.dossiers.last

    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Dossier, {
      type: dossier_definition.id,
      multiple: true
    })
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, [[dossier_value_1, dossier_value_2], [dossier_value_2, dossier_value_3], nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Dossier, {
      type: dossier_definition.id,
      multiple: true
    })
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, [[dossier_value_1, dossier_value_2], [dossier_value_2, dossier_value_3], nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter(workflow_1_id, content_item_definition, :is_equal, [dossier_value_2.id.to_s, dossier_value_1.id.to_s])
    assert_filter(dossier_1_id, dossier_item_definition, :is_equal, [dossier_value_2.id.to_s, dossier_value_1.id.to_s])

    assert_filter(workflow_2_id, content_item_definition, :is_not_equal, [dossier_value_2.id.to_s, dossier_value_1.id.to_s])
    assert_filter(dossier_2_id, dossier_item_definition, :is_not_equal, [dossier_value_2.id.to_s, dossier_value_1.id.to_s])

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :does_contain_any, [dossier_value_2.id.to_s, dossier_value_4.id.to_s])
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :does_contain_any, [dossier_value_2.id.to_s, dossier_value_4.id.to_s])

    assert_filter(workflow_1_id, content_item_definition, :does_contain_any, [dossier_value_1.id.to_s])
    assert_filter(dossier_1_id, dossier_item_definition, :does_contain_any, [dossier_value_1.id.to_s])

    assert_filter(workflow_2_id, content_item_definition, :does_not_contain_any, [dossier_value_1.id.to_s])
    assert_filter(dossier_2_id, dossier_item_definition, :does_not_contain_any, [dossier_value_1.id.to_s])

    assert_filter([], content_item_definition, :does_not_contain_any, [dossier_value_1.id.to_s, dossier_value_3.id.to_s])
    assert_filter([], dossier_item_definition, :does_not_contain_any, [dossier_value_1.id.to_s, dossier_value_3.id.to_s])

    assert_filter(workflow_1_id, content_item_definition, :does_contain_all, [dossier_value_1.id.to_s, dossier_value_2.id.to_s])
    assert_filter(dossier_1_id, dossier_item_definition, :does_contain_all, [dossier_value_1.id.to_s, dossier_value_2.id.to_s])
  end

  test "Filter task and dossiers by fields of content_type 'File' should work" do
    UploadedFile.update_all(title: nil)

    uploaded_file_value_1 = UploadedFile.first
    uploaded_file_value_1.update!(title: "Test 1 Uploaded File")
    uploaded_file_value_2 = UploadedFile.second
    uploaded_file_value_2.update!(title: "Test 2 Uploaded File")
    uploaded_file_value_3 = UploadedFile.third
    uploaded_file_value_3.update!(title: "Last File")

    content_item_definition = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::File, {multiple: true})
    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition, [[uploaded_file_value_1, uploaded_file_value_2], [uploaded_file_value_2, uploaded_file_value_3], nil])

    dossier_item_definition = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::File, {multiple: true})
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition, [[uploaded_file_value_1, uploaded_file_value_2], [uploaded_file_value_2, uploaded_file_value_3], nil], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition)

    assert_filter(workflow_3_id, content_item_definition, :is_empty)
    assert_filter(dossier_3_id, dossier_item_definition, :is_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :is_not_empty)
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :is_not_empty)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition, :does_contain, "UpLoaded")
    assert_filter([dossier_1_id, dossier_2_id], dossier_item_definition, :does_contain, "UpLoaded")

    assert_filter(workflow_1_id, content_item_definition, :does_contain, "1 UpLoaded")
    assert_filter(dossier_1_id, dossier_item_definition, :does_contain, "1 UpLoaded")

    assert_filter(workflow_2_id, content_item_definition, :does_not_contain, "1 UpLoaded")
    assert_filter(dossier_2_id, dossier_item_definition, :does_not_contain, "1 UpLoaded")

    assert_filter([], content_item_definition, :does_not_contain, "UpLoaded")
    assert_filter([], dossier_item_definition, :does_not_contain, "UpLoaded")
  end

  test "Filter task and dossiers by multiple fields should work" do
    content_item_definition_string = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::String)
    content_item_definition_decimal = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Decimal)
    content_item_definition_integer = create_workflow_definition_data_field_definitions(@workflow_definition, ContentTypes::Integer,
      first_task_definition: @workflow_definition.direct_task_definitions.third, second_task_definition: @workflow_definition.direct_task_definitions.fourth)

    workflow_1_id, workflow_2_id, workflow_3_id = create_workflows_with_values(content_item_definition_string, ["test1", "test2", "bla"])
    update_value_of_content_items(content_item_definition_decimal, [workflow_1_id, workflow_2_id, workflow_3_id], ["-5.5", "5.5", "9.9"])
    update_value_of_content_items(content_item_definition_integer, [workflow_1_id, workflow_2_id, workflow_3_id], ["-5", "5", "9"])

    dossier_item_definition_string = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::String)
    dossier_item_definition_decimal = create_dossier_field_definition(@user, @dossier_definition, ContentTypes::Decimal)
    dossier_1_id, dossier_2_id, dossier_3_id = create_dossiers_with_values(dossier_item_definition_string, ["test1", "test2", "bla"], @user)
    update_value_of_dossier_items(dossier_item_definition_decimal, [dossier_1_id, dossier_2_id, dossier_3_id], ["-5.5", "5.5", "9.9"], @user)

    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition_string)
    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition_decimal)
    assert_items([workflow_1_id, workflow_2_id, workflow_3_id], content_item_definition_integer)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition_string)
    assert_items([dossier_1_id, dossier_2_id, dossier_3_id], dossier_item_definition_decimal)

    assert_filter([workflow_1_id, workflow_2_id], content_item_definition_string, :does_contain, "test")
    assert_filter([workflow_2_id, workflow_3_id], content_item_definition_decimal, :between, ["5.0", "10.0"])
    assert_filter([workflow_2_id, workflow_3_id], content_item_definition_decimal, :between, ["5", "10"])

    filtered_records = Task.with_item_value_filter(content_item_definition_string, :does_contain, "test", filter_index: 0)
      .with_item_value_filter(content_item_definition_decimal, :between, ["5.0", "10.0"], filter_index: 1)
    assert_filtered_records(workflow_2_id, filtered_records, Task)

    filtered_records = Task.with_item_value_filter(content_item_definition_string, :does_contain, "test", filter_index: 0)
      .with_item_value_filter(content_item_definition_integer, :between, ["5", "10"], filter_index: 1)
    assert_filtered_records(workflow_2_id, filtered_records, Task)

    filtered_records = Dossier.with_item_value_filter(dossier_item_definition_string, :does_contain, "test", filter_index: 0)
      .with_item_value_filter(dossier_item_definition_decimal, :between, ["5.0", "10.0"], filter_index: 1)
    assert_filtered_records(dossier_2_id, filtered_records, Dossier)
  end

  private

  def create_workflows_with_values(content_item_definition, values)
    values.map do |value|
      create_workflow_for!(content_item_definition.workflow_definition, {content_item_definition.id => value}).id
    end
  end

  def update_value_of_content_items(content_item_definition, workflow_ids, values)
    workflow_ids.each_with_index do |workflow_id, index|
      content_item = Workflow.find(workflow_id).content_items.find_by!(content_item_definition_id: content_item_definition.id)
      content_item.update!(value: values[index])
    end
  end

  def update_value_of_dossier_items(dossier_item_definition, dossier_ids, values, user)
    dossier_ids.each_with_index do |dossier_id, index|
      Interactors::DossierInteractor.update(Dossier.find(dossier_id), dossier_item_definition.id, values[index], 0, user)
    end
  end

  def create_workflow_definition_data_field_definitions(workflow_definition, content_type, options = {}, first_task_definition: nil, second_task_definition: nil)
    first_task_definition = workflow_definition.direct_task_definitions.first if first_task_definition.nil?
    second_task_definition = workflow_definition.direct_task_definitions.second if second_task_definition.nil?

    task_item_definition = Interactors::WorkflowDefinitionInteractor.add_data_item_definition(
      first_task_definition,
      {
        content_type: content_type.to_s,
        label: "Testfield",
        options: options
      }, {
        info_box: false,
        required: false
      }
    )

    Interactors::WorkflowDefinitionInteractor.add_data_item_definition(
      second_task_definition,
      {
        id: task_item_definition.content_item_definition.id
      }, {
        info_box: false,
        required: false
      }
    )

    task_item_definition.content_item_definition
  end

  def task_ids_of(content_item_definition, workflow)
    tasks = content_item_definition.task_item_definitions.flat_map(&:task_items).map(&:task).select do |task|
      workflow == task.workflow
    end

    tasks.map(&:id).sort
  end

  def create_dossier_field_definition(user, dossier_definition, content_type, options = {})
    Interactors::DossierItemDefinitionInteractor.create(
      dossier_definition, {
        name: "testfield_#{content_type}",
        content_type: content_type.to_s,
        options: options
      },
      user
    )
  end

  def create_dossiers_with_values(dossier_item_definition, values, user)
    values.each_with_index.map do |value, index|
      dossier = dossier_item_definition.definition.dossiers.order(:id).limit(1).offset(index).first
      Interactors::DossierInteractor.update(dossier, dossier_item_definition.id, value, 0, user)
      dossier.id
    end
  end

  def assert_items(exp_ids, item_definition)
    model = item_definition.is_a?(ContentItemDefinition) ? Task : Dossier
    filtered_records = model.with_item_definition(item_definition)

    filtered_record_ids = if item_definition.is_a?(ContentItemDefinition)
      filtered_records.pluck(:workflow_id)
    else
      filtered_records.pluck(:id)
    end

    assert_equal Array(exp_ids).sort, filtered_record_ids.uniq.sort
  end

  def assert_filter(exp_ids, item_definition, comp_op, *options)
    model = item_definition.is_a?(ContentItemDefinition) ? Task : Dossier
    filtered_records = model.with_item_value_filter(item_definition, comp_op, *options)

    filtered_record_ids = if item_definition.is_a?(ContentItemDefinition)
      filtered_records.pluck(:workflow_id)
    else
      filtered_records.pluck(:id)
    end

    assert_equal Array(exp_ids).sort, filtered_record_ids.uniq.sort, filter_error_msg(model, comp_op, item_definition.content_type)
  end

  def assert_filtered_records(exp_ids, filtered_records, model)
    filtered_record_ids = if model == Task
      filtered_records.pluck(:workflow_id)
    else
      filtered_records.pluck(:id)
    end

    assert_equal Array(exp_ids).sort, filtered_record_ids.uniq.sort, "#{model}: filter multiple fields doesn't work as expected (sql: #{filtered_records.to_sql})"
  end

  def with_item_definition_error_msg(model, content_type)
    "#{model}: 'with_item_definition' for content_type '#{content_type}' doesn't work as expected"
  end

  def filter_error_msg(model, comp_op, content_type)
    "#{model}: filter_by(#{comp_op}) for content_type '#{content_type}' doesn't work as expected"
  end
end
