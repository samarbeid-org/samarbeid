import babelParser from '@babel/eslint-parser'
import pluginVue from 'eslint-plugin-vue'

import { FlatCompat } from '@eslint/eslintrc'
import path from 'path'
import { fileURLToPath } from 'url'

import { readFile } from 'node:fs/promises'

const __filename = fileURLToPath(import.meta.url)
const __dirname = path.dirname(__filename)

const compat = new FlatCompat({
  baseDirectory: __dirname
})

const fileUrl = new URL('node_modules/eslint-plugin-vue/lib/utils/inline-non-void-elements.json', import.meta.url)
const INLINE_ELEMENTS = JSON.parse(await readFile(fileUrl, 'utf8'))

export default [
  ...compat.extends('standard'),
  ...pluginVue.configs['flat/vue2-recommended'],
  {
    files: ['**/*.vue', '**/*.js', '**/*.cjs'],
    languageOptions: {
      parserOptions: {
        parser: babelParser
      }
    },
    rules: {
      'vue/multiline-html-element-content-newline': ['warn', {
        ignores: ['pre', 'textarea', 'router-link'].concat(INLINE_ELEMENTS),
        ignoreWhenEmpty: true,
        allowEmptyLines: false
      }],

      'vue/multi-word-component-names': ['error', {
        ignores: ['Avatar', 'Breadcrumbs', 'Mention', 'Messages', 'Snackbars']
      }]
    }
  }
]
