import Vue from 'vue'

export default function globalIdFor (model, id) {
  return `gid://${Vue.$config.globalIdApp}/${model}/${id}`
}
