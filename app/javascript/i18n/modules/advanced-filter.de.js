export default {
  dialog: {
    title: 'Erweiterte Filter',
    buttons: {
      ok: 'Ok',
      reset: 'Alle zurücksetzen'
    }
  }
}
