import ListPage from '../list-page'
import GroupListContent from './list-content.vue'

export default {
  name: 'GroupListPage',
  mixins: [ListPage],
  computed: {
    pageTitleParts () {
      return ['Gruppen']
    },
    pageContentComponents () {
      return GroupListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.groups.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Gruppenname',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Name – aufsteigend', value: 'name_asc' },
              { text: 'Name – absteigend', value: 'name_desc' },
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' }
            ],
            default: 'name_asc',
            cols: 4
          }]
      ]
    }
  }
}
