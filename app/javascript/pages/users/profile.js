import Page from '../page'
import ProfileContent from './profile-content.vue'

export default {
  name: 'UserProfilePage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.fullname] : []), 'Nutzer:in Profil']
    },
    pageContentComponents () {
      return ProfileContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.users.show(this.id)
    }
  }
}
