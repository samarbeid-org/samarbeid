import ListPage from '../list-page'
import ListContent from './list-content.vue'

export default {
  name: 'FooterMenuEntryListPage',
  mixins: [ListPage],
  data () {
    return {
      filter: {
        page: 1,
        queryText: null,
        order: 'location_position'
      }
    }
  },
  computed: {
    pageTitleParts () {
      return ['Fußzeilenmenüeinträge']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.menuEntries.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Titel des Menüeintrags',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Reihenfolge', value: 'location_position' },
              { text: 'Titel – aufsteigend', value: 'title_asc' },
              { text: 'Titel – absteigend', value: 'title_desc' },
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' }
            ],
            default: 'location_position',
            cols: 4
          }]
      ]
    }
  }
}
