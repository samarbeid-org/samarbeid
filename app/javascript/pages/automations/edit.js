import Page from '../page'
import EditContent from './edit-content.vue'

export default {
  name: 'AutomationEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.title] : []), 'Automatisierungen']
    },
    pageContentComponents () {
      return EditContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.automations.show(this.id)
    }
  }
}
