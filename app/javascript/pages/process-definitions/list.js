import ListPage from '../list-page'
import ListContent from './list-content.vue'

export default {
  name: 'ProcessDefinitionListPage',
  mixins: [ListPage],
  computed: {
    pageTitleParts () {
      return ['Prozessvorlagen']
    },
    pageContentComponents () {
      return ListContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.workflowDefinitions.index()
    },
    initPageRequestParams () {
      return {
        page: this.pages.current,
        query: this.filters.values.query,
        order: this.filters.values.order,
        state: this.filters.values.state
      }
    },
    filterFields () {
      return [
        [
          {
            name: 'query',
            type: 'text',
            label: 'Prozessvorlagentitel',
            default: ''
          },
          {
            name: 'order',
            type: 'single-select',
            label: 'Sortierung',
            items: [
              { text: 'Name – aufsteigend', value: 'name_asc' },
              { text: 'Name – absteigend', value: 'name_desc' },
              { text: 'Erstellungsdatum – neueste zuerst', value: 'created_at_desc' },
              { text: 'Erstellungsdatum – älteste zuerst', value: 'created_at_asc' }
            ],
            default: 'name_asc',
            cols: 4
          }],
        [
          {
            name: 'state',
            type: 'single-select',
            label: 'Status',
            items: [
              { text: 'Aktiviert', value: 'activated' },
              { text: 'Deaktiviert', value: 'deactivated' },
              { text: 'Alle', value: 'all' }
            ],
            default: 'activated'
          }
        ]
      ]
    }
  }
}
