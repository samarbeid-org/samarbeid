import Page from '../page'
import StaticPageEditContent from './edit-content.vue'

export default {
  name: 'StaticPageEditPage',
  mixins: [Page],
  props: {
    id: {
      type: Number,
      required: true
    }
  },
  computed: {
    pageTitleParts () {
      return [...(this.value ? [this.value.title] : []), 'Statische Seiten']
    },
    pageContentComponents () {
      return StaticPageEditContent
    }
  },
  methods: {
    initPageRequestUrl () {
      return this.$apiEndpoints.pages.show(this.id)
    }
  }
}
