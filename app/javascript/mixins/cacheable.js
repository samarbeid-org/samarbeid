import cloneDeep from 'lodash/cloneDeep'
import isEqual from 'lodash/isEqual'

export default {
  data () {
    return {
      cacheValue: null
    }
  },
  methods: {
    createCacheValue () {
      this.cacheValue = cloneDeep(this.value)
    }
  },
  computed: {
    cachedValueChanged () {
      return !isEqual(this.value, this.cacheValue)
    }
  },
  watch: {
    value: {
      handler () {
        this.createCacheValue()
      },
      deep: true,
      immediate: true
    }
  }
}
