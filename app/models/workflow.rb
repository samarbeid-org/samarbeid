class Workflow < ApplicationRecord
  include ReferenceSupport
  include SearchForList
  include FilterSupport::WorkflowFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: {id: "A", title: "B"},
    associated_against: {
      workflow_definition: {name: "C"}
    },
    using: {
      tsearch: {prefix: true}
    }

  # This association should not trigger any action if record is destroyed as associations direct_tasks and blocks will destroy all tasks
  has_many :all_tasks, -> { order(:position) }, class_name: "Task", inverse_of: :workflow

  has_many :direct_tasks, -> { order(:position) }, class_name: "Task", as: :workflow_or_block, inverse_of: :workflow_or_block, dependent: :destroy
  has_many :blocks, -> { order(:position) }, dependent: :destroy
  has_many :content_items, dependent: :destroy
  def items
    (direct_tasks + blocks).sort_by(&:position)
  end

  def tasks_ordered_in_structure
    items.flat_map { |item| item.is_a?(Block) ? item.items : item }
  end

  belongs_to :workflow_definition

  has_many :title_content_items, -> {
    joins(:content_item_definition)
      .where.not(content_item_definitions: {title_position: nil})
      .order("content_item_definitions.title_position")
  }, class_name: "ContentItem"

  belongs_to :assignee, class_name: "User", optional: true

  has_many :contributions, as: :contributable, dependent: :destroy
  has_many :contributors, -> { order "contributions.created_at" }, through: :contributions, inverse_of: false, source: :user, after_add: :reindex_with_tasks, after_remove: :reindex_with_tasks

  belongs_to :predecessor_workflow, class_name: "Workflow", optional: true
  has_many :successor_workflows, class_name: "Workflow", foreign_key: "predecessor_workflow_id", inverse_of: :predecessor_workflow, dependent: :nullify

  has_many :comments, as: :object, dependent: :destroy

  enum system_identifier: [:system_process_single_task].map { |entry| [entry.to_sym, entry.to_s] }.to_h

  scope :not_system_process, -> { where(system_identifier: nil) }
  scope :not_system_process_single_task, -> { where.not(system_identifier: :system_process_single_task).or(not_system_process) }

  after_save :reindex_with_tasks
  after_commit :on_process_updated, on: :update

  after_create_commit :update_workflow_title_on_create

  include AddToFulltextSearch
  def as_search_document
    {identifier: identifier, title: title, subtitle: workflow_definition&.name,
     content: [description, comments_as_text, data_fields_as_text].join("<br>-------------------------<br>")}
  end

  def comments_as_text
    comments.map { |c| "#{c.author.fullname}: #{c.message}" }.join("<br>")
  end

  def data_fields_as_text
    content_items.with_value.map(&:as_search_data).reduce({}, :merge).map { |k, v| "#{k}: #{v}" }.join("<br>")
  end

  delegate :name, to: :workflow_definition

  attr_accessor :triggered_by

  include AASM
  after_create do
    items.first.enable!(triggered_by) if items.first&.may_enable?
  end
  aasm whiny_persistence: true do
    state :active, initial: true
    state :completed

    after_all_events do |triggered_by|
      update!(state_updated_at: Time.now)
      Services::NewEngine.new(self).run(triggered_by)
    end

    event :complete do
      transitions from: :active, to: :completed,
        after: -> { update!(completed_at: Time.now) }
    end
    event :reopen do
      transitions from: :completed, to: :active,
        after: -> { update!(completed_at: nil) }
    end
  end

  # Workaround for quick deploy of task refactoring
  def align_state
    return unless persisted?
    if completable?
      update_columns(aasm_state: "completed", completed_at: Time.now) if read_attribute(:aasm_state) == "active"
    elsif read_attribute(:aasm_state) == "completed"
      update_columns(aasm_state: "active", completed_at: nil)
    end
  end

  def completable?
    direct_tasks.all?(&:closed?) &&
      blocks.select(&:condition_valid?).all? { |block| block.items.all?(&:closed?) } &&
      blocks.select { |block| !block.condition_valid? }.all? { |block| block.items.all?(&:closed_or_created?) }
  end

  def due_tasks_count
    all_tasks.count(&:due?)
  end

  def contributors=(new_contributors)
    super
    reindex_with_tasks
  end

  def title_from_db
    read_attribute(:title)
  end

  def title_from_fields
    content_item_values_to_text(workflow_definition&.auto_generate_title? ? title_content_items : [], [String(id), String(workflow_definition&.name)])
  end

  def title
    # if title in db is empty or nil return default value
    return title_from_fields if title_from_db.blank?

    title_from_db
  end

  def sync_title_from_fields
    update(title: workflow_definition&.auto_generate_title? ? title_from_fields : nil)
  end

  def title_tooltip
    return unless workflow_definition&.auto_generate_title?

    tooltip = title_content_items.filter { |content_item| !content_item.localized_string(content_item.value).empty? }
      .map { |content_item| "#{content_item.label}: #{content_item.localized_string(content_item.value)}" }
      .join("\n")

    tooltip.empty? ? nil : tooltip
  end

  def identifier
    "%#{id}"
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    if workflow_definition&.system_process_definition_single_task?
      identifier + (noaccess ? "" : " • #{title}")
    else
      identifier + (noaccess ? "" : " (#{workflow_definition&.name}) • #{title}")
    end
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  def start_items
    workflow_definition.start_item_definitions.map do |sid|
      ci = content_items.find { |ci| ci.content_item_definition.id == sid.id }
      TaskItem.new(required: false, info_box: false, content_item: ci)
    end
  end

  attr_accessor :skip_indexing
  def without_reindexing
    @skip_indexing = true
    result = yield
    @skip_indexing = false
    result
  end

  def reindex_with_tasks(_params = nil)
    return if @skip_indexing
    return unless persisted?
    reindex_as_search_document
    all_tasks.map(&:reindex_as_search_document)
  end

  def content_item_values_to_text(content_items, default_values)
    elements = content_items.map { |content_item| content_item.localized_string(content_item.value) }.reject(&:empty?)
    elements = default_values if elements.none?

    elements.reject(&:empty?).join(" • ").squish
  end

  def on_process_updated
    MonitorChannel.broadcast_object_updated(self)
    MonitorProcessStructureChannel.broadcast_process_structure_updated(self)
  end

  def update_workflow_title_on_create
    return unless workflow_definition&.auto_generate_title?

    sync_title_from_fields
  end
end
