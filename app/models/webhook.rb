class Webhook < ApplicationRecord
  belongs_to :task
  belongs_to :webhook_definition, optional: true

  def perform(triggered_by = nil)
    uri = URI(url)
    response = Net::HTTP.post(uri, body(triggered_by).to_json, {
      "Content-Type" => "application/json",
      "Authorization" => "Bearer #{auth_token}"
    })
    update(response_code: response.code.to_i, response_body: response.body)
  end

  def body(triggered_by = nil)
    {
      triggered_by: triggered_by&.email,
      triggered_from_task: triggered_from_task,
      fields: fields_to_submit
    }
  end

  # Temporary workaround to display webhooks in task
  def as_task_item
    note = "Webhook #{url}"
    note << " - #{result_to_s}" if response_code
    ci = ContentItem.new(content_type: ContentTypes::Note.type, value: note, workflow: task.workflow)
    TaskItem.new(task: task, position: -id, content_item: ci)
  end

  def result_to_s
    "Response #{response_code}: #{response_body.to_s.truncate(140)}"
  end

  private

  def triggered_from_task
    {
      id: task.id,
      name: task.name,
      due_at: task.due_at,
      assignee: task.assignee&.email,
      contributors: task.contributors.map(&:email),
      task_definition: {
        id: task.task_definition.id
      },
      workflow: {
        id: task.workflow.id,
        name: task.workflow.name,
        assignee: task.workflow.assignee&.email
      },
      workflow_definition: {
        id: task.workflow.workflow_definition&.id,
        name: task.workflow.workflow_definition&.name
      }
    }
  end

  def fields_to_submit
    task.task_items.map(&:content_item).uniq.reject { |ci| ci.content_type.to_s == "note" }.map do |content_item|
      {
        id: content_item.id,
        definition_id: content_item.content_item_definition&.id,
        label: content_item.label,
        value: content_item.value,
        localized_value: content_item.localized_value
      }
    end
  end
end
