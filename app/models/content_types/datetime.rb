class ContentTypes::Datetime < ContentTypes::Value
  class << self
    def type
      :datetime
    end

    def cast(value)
      return nil unless value.present?

      DateTime.parse(value)
    end

    def serialize(value)
      value&.strftime("%FT%T")
    end

    def deserialize(value)
      value
    end

    def localized_string(value, options = nil)
      I18n.l(cast(value), format: :default)
    end

    def calendar_addable?
      true
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      from = cast(values&.first)
      to = cast(values&.second)

      case comp_op
      when :between
        if from.present? && to.present?
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::timestamp between :from and :to", from: serialize(from), to: serialize(to)]))
        elsif from.present?
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::timestamp >= :from", from: serialize(from)]))
        else
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::timestamp <= :to", to: serialize(to)]))
        end
      else
        super
      end
    end

    def supported_comparators
      [:between] + super
    end
  end
end
