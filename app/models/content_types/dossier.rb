class ContentTypes::Dossier < ContentTypes::Value
  class << self
    def type
      :dossier
    end

    def cast(value)
      Dossier.where(id: value) unless value.nil?
    end

    def serialize(value)
      value&.map(&:to_global_id)&.map(&:to_s)
    end

    def deserialize(value)
      GlobalID::Locator.locate_many(value, ignore_missing: true) unless value.nil?
    end

    def localized_string(value, options = nil)
      value.map(&:title).join(", ")
    end

    def data_empty?(value)
      super || value.length == 0
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      val = cast(values)

      case comp_op
      when :is_empty
        super.or(relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value = '[]'::jsonb"])))
      when :is_not_empty
        filter_by(relation, relation_alias, :is_empty, values).invert_where
      when :is_equal
        relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value @> :val AND #{relation_alias}.value <@ :val", val: serialize(val).to_json]))
      when :is_not_equal
        filter_by(relation, relation_alias, :is_equal, values).invert_where
      when :does_contain_any
        relation.where(relation.sanitize_sql_for_conditions(["array_to_json(translate(#{relation_alias}.value::text, '[]', '{}')::text[])::jsonb ?| array[:val]", val: serialize(val)]))
      when :does_not_contain_any
        filter_by(relation, relation_alias, :does_contain_any, values).invert_where
      when :does_contain_all
        relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value @> :val", val: serialize(val).to_json]))
      else
        super
      end
    end

    def supported_comparators
      [:does_contain_any, :does_not_contain_any, :does_contain_all, :is_equal, :is_not_equal] + super
    end

    def options_schema
      {
        type: "object",
        required: ["type"],
        properties: {
          type: {
            type: "integer",
            format: "dossier-definition",
            enum: DossierDefinition.pluck(:id)
          },
          multiple: {
            type: "boolean"
          }
        },
        additionalProperties: true
      }
    end

    def validate_options(options, value, referenced = false)
      result_super = super
      result = []

      result << {base: result_super} unless result_super === true

      if value
        if options["multiple"] == false && value.count > 1
          result << {multiple: I18n.t("content_type.#{type}.errors.current_value_does_not_match_option_multiple")}
        end

        if value.any? { |dossier| dossier.definition.id != options["type"] }
          result << {type: I18n.t("content_type.#{type}.errors.current_value_does_not_match_option_type")}
        end
      end

      result.empty? ? true : result
    end

    def on_options_changed(old_value, new_value)
      result_super = super
      result_super.delete("inverseReference") if old_value["type"] != result_super["type"]

      result_super
    end

    def inverseReference(field)
      inverse_reference_definition = field.options["inverseReference"] || {}
      title_from_options = inverse_reference_definition["title"]
      {
        title: title_from_options.blank? ? "#{field.definition.name.truncate(20)} • #{field.name}" : title_from_options,
        definition: {
          title: title_from_options || "",
          showEmpty: !!inverse_reference_definition["showEmpty"]
        }
      }
    end
  end
end
