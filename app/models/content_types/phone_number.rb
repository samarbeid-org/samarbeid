class ContentTypes::PhoneNumber < ContentTypes::String
  class << self
    def type
      :phone_number
    end

    def cast(value)
      super(value&.strip)
    end

    def transform(from_value, from_content_type, from_options, to_options)
      return from_value if self == from_content_type

      case from_content_type.to_s
      when ContentTypes::Text.to_s
        from_value&.gsub(/\R+/, "\\")&.delete "-/"
      else
        ContentTypes::String.transform(from_value, from_content_type, from_options, to_options)&.delete "-/"
      end
    end

    def validate_data(value, options)
      if value.empty? || /\A(\+[1-9])?[0-9\s]*\z/i.match?(value)
        true
      else
        "muss eine Telefonnummer sein"
      end
    end
  end
end
