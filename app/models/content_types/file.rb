class ContentTypes::File < ContentTypes::Value
  class << self
    def type
      :file
    end

    def cast(value)
      UploadedFile.where(id: value) unless value.nil?
    end

    def serialize(value)
      value&.map(&:to_global_id)&.map(&:to_s)
    end

    def deserialize(value)
      GlobalID::Locator.locate_many(value) unless value.nil?
    end

    def localized_string(value, options = nil)
      value.map(&:title).join(", ")
    end

    def data_empty?(value)
      super || value.length == 0
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      val = values

      case comp_op
      when :is_empty
        super.or(relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value = '[]'::jsonb"])))
      when :is_not_empty
        filter_by(relation, relation_alias, :is_empty, values).invert_where
      when :does_contain
        files = UploadedFile.joins(:file_blob).where(UploadedFile.arel_table[:title].matches("%#{val}%")).or(
          UploadedFile.joins(:file_blob).where(ActiveStorage::Blob.arel_table[:filename].matches("%#{val}%"))
        )
        relation.where(relation.sanitize_sql_for_conditions(["array_to_json(translate(#{relation_alias}.value::text, '[]', '{}')::text[])::jsonb ?| array[:val]", val: serialize(files)]))
      when :does_not_contain
        filter_by(relation, relation_alias, :does_contain, values).invert_where
      else
        super
      end
    end

    def supported_comparators
      [:does_contain, :does_not_contain] + super
    end

    def options_schema
      {
        type: "object",
        properties: {
          multiple: {
            type: "boolean"
          }
        },
        additionalProperties: false
      }
    end

    def validate_options(options, value, referenced = false)
      result_super = super
      result = []
      array_value = Array(value)

      result << {base: result_super} unless result_super === true

      if options["multiple"] == false && array_value.count > 1
        result << {multiple: I18n.t("content_type.#{type}.errors.current_value_does_not_match_option_multiple")}
      end

      result.empty? ? true : result
    end
  end
end
