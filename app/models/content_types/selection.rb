class ContentTypes::Selection < ContentTypes::Value
  class << self
    def type
      :selection
    end

    def cast(value)
      Array(value).map { |val| val.is_a?(Integer) ? val : Integer(val, exception: false) }.compact
    end

    def transform(from_value, from_content_type, from_options, to_options)
      if (self == from_content_type) && !from_value.nil?
        to_option_values = to_options["items"].map { |val| val["value"] }
        if to_options["multiple"] == false
          return (from_value.length == 1 && to_option_values.include?(from_value.first)) ? from_value : nil
        else
          return from_value & to_option_values
        end
      end

      nil
    end

    def localized_string(value, options)
      array_value = Array(value)
      options["items"].select { |item| array_value.include? item["value"] }.map { |item| item["text"] }.join(", ")
    end

    def data_empty?(value)
      super || value.blank?
    end

    def validate_data(value, options)
      result = []
      array_value = Array(value)

      if options["multiple"] == false && array_value.count > 1
        result << "Die Auswahl darf nur einen Wert enthalten."
      end

      if array_value.difference(options["items"].map { |i| i["value"] }).any?
        result << "Die Auswahl enthält nicht definierte Listeneinträge. Eventuell wurden diese gelöscht."
      end

      result.any? ? result.join(" ") : true
    end

    def compare(val1, val2)
      ((val1 || []) & (val2 || [])).any? # val1 includes any element from val2
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      val = cast(values)

      case comp_op
      when :is_empty
        super.or(relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value = '[]'::jsonb"])))
      when :is_not_empty
        filter_by(relation, relation_alias, :is_empty, values).invert_where
      when :is_equal
        relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value @> :val AND #{relation_alias}.value <@ :val", val: serialize(val).to_json]))
      when :is_not_equal
        filter_by(relation, relation_alias, :is_equal, values).invert_where.merge(filter_by(relation, relation_alias, :is_not_empty, values))
      when :does_contain_any
        relation.where(relation.sanitize_sql_for_conditions(["array_to_json(translate(#{relation_alias}.value::text, '[]', '{}')::text[])::jsonb ?| array[:val]", val: serialize(val.map(&:to_s))]))
      when :does_not_contain_any
        filter_by(relation, relation_alias, :does_contain_any, values).invert_where.merge(filter_by(relation, relation_alias, :is_not_empty, values))
      when :does_contain_all
        relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value @> :val", val: serialize(val).to_json]))
      else
        super
      end
    end

    def supported_comparators
      [:does_contain_any, :does_not_contain_any, :does_contain_all, :is_equal, :is_not_equal] + super
    end

    def options_schema
      {
        type: "object",
        required: ["items"],
        properties: {
          items: {
            type: "array",
            minItems: 1,
            items: {
              type: "object",
              required: %w[value text],
              properties: {
                value: {
                  type: "integer",
                  format: "auto-increment"
                },
                text: {
                  type: "string"
                }
              }
            }
          },
          multiple: {
            type: "boolean"
          }
        },
        additionalProperties: false
      }
    end

    def validate_options(options, value, referenced = false)
      result_super = super
      result = []
      array_value = Array(value)

      result << {base: result_super} unless result_super === true

      if !referenced && options["multiple"] == false && array_value.count > 1
        result << {multiple: I18n.t("content_type.#{type}.errors.current_value_does_not_match_option_multiple")}
      end

      item_texts = options["items"].pluck("text")
      if item_texts != item_texts.uniq
        result << {items: I18n.t("content_type.#{type}.errors.option_items_not_unique")}
      elsif array_value.difference(options["items"].map { |i| i["value"] }).any?
        result << {items: I18n.t("content_type.#{type}.errors.current_value_does_not_match_option_items")}
      end

      result.empty? ? true : result
    end
  end
end
