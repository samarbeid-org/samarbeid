class ContentTypes::Richtext < ContentTypes::String
  class << self
    def type
      :richtext
    end

    def prepare(value, container)
      unless value.nil?
        value, _ = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(value))

        Services::RichtextImageStorage.purge_unused_images(value, container.richtext_images)
        value = Services::RichtextImageStorage.persist_inline_images(value, container.richtext_images)
      end

      super
    end

    def localized_string(value, options = nil)
      with_labels = Services::Mentioning.add_mention_labels(value)
      # Note: This is a quick-n-easy HTML to TXT conversion which allows to read/understand.
      # If required we could use an external library to cover all possible markup in a standard way
      # (for instance Markdown).
      readable_text = with_labels.gsub("</p>", "</p>\n").gsub("<br />", "<br />\n")
      Nokogiri::HTML.parse(readable_text).text
    end

    def data_empty?(value)
      super || Nokogiri::HTML5.fragment(value).text.blank?
    end

    def extract_done_and_total_todos(value)
      html_fragment = Nokogiri::HTML(value)

      total_todos = html_fragment.css('li[data-type="todo_item"]').count
      done_todos = html_fragment.css('li[data-type="todo_item"][data-done="true"]').count

      [done_todos, total_todos]
    end
  end
end
