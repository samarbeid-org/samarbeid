class ContentTypes::Date < ContentTypes::Value
  class << self
    def type
      :date
    end

    def cast(value)
      return nil unless value.present?

      ActiveModel::Type::Date.new.cast(value)
    end

    def localized_string(value, options = nil)
      I18n.l(value)
    end

    def calendar_addable?
      true
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      from = cast(values&.first)
      to = cast(values&.second)

      case comp_op
      when :between
        if from.present? && to.present?
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::date between :from and :to", from: serialize(from), to: serialize(to)]))
        elsif from.present?
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::date >= :from", from: serialize(from)]))
        else
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::date <= :to", to: serialize(to)]))
        end
      else
        super
      end
    end

    def supported_comparators
      [:between] + super
    end
  end
end
