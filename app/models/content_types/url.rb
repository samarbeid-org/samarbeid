class ContentTypes::Url < ContentTypes::String
  class << self
    def type
      :url
    end

    def cast(value)
      super(value&.strip)
    end

    def validate_data(value, options)
      if value.empty? || /\A\S*\z/i.match?(value)
        true
      else
        "muss eine Url sein"
      end
    end
  end
end
