class ContentTypes::Decimal < ContentTypes::Value
  class << self
    def type
      :decimal
    end

    def cast(value)
      ActiveModel::Type::Decimal.new.cast(value.to_s) unless value.nil?
    end

    def localized_string(value, options)
      result = ActiveSupport::NumberHelper.number_to_rounded(value, precision: options["decimalLength"])
      result = options["prefix"] + " " + result if options["prefix"].present?
      result = result + " " + options["suffix"] if options["suffix"].present?
      result
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      from = cast(values&.first)
      to = cast(values&.second)

      case comp_op
      when :between
        if from.present? && to.present?
          relation.where(relation.sanitize_sql_for_conditions(["(#{relation_alias}.value #>> '{}')::numeric between :from and :to", from: serialize(from), to: serialize(to)]))
        elsif from.present?
          relation.where(relation.sanitize_sql_for_conditions(["(#{relation_alias}.value #>> '{}')::numeric >= :from", from: serialize(from)]))
        else
          relation.where(relation.sanitize_sql_for_conditions(["(#{relation_alias}.value #>> '{}')::numeric <= :to", to: serialize(to)]))
        end
      else
        super
      end
    end

    def supported_comparators
      [:between] + super
    end

    def options_schema
      {
        type: "object",
        properties: {
          decimalLength: {
            type: "integer",
            default: 2
          },
          prefix: {
            type: "string"
          },
          suffix: {
            type: "string"
          }
        },
        additionalProperties: false
      }
    end
  end
end
