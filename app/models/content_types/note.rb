class ContentTypes::Note < ContentTypes::Richtext
  class << self
    def type
      :note
    end
  end
end
