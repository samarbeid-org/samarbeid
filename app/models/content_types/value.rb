class ContentTypes::Value
  class << self
    def type
      raise "Not implemented"
    end

    def to_s
      type.to_s
    end

    def label
      I18n.t "content_type.#{type}.label"
    end

    def deserialize(value)
      cast(value) unless value.nil?
    end

    def cast(value)
      value unless value.nil?
    end

    def prepare(value, container)
      value
    end

    def transform(from_value, from_content_type, from_options, to_options)
      return from_value if self == from_content_type

      nil
    end

    def serialize(value)
      value&.as_json
    end

    def localized_string(value, options = nil)
      value.to_s
    end

    def data_empty?(value)
      value.nil?
    end

    def compare(val1, val2)
      val1 == val2
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      case comp_op
      when :is_empty
        relation.where(relation_alias => {value: nil})
      when :is_not_empty
        filter_by(relation, relation_alias, :is_empty, values).invert_where
      else
        raise "Not implemented: filter_by(#{comp_op})"
      end
    end

    def supported_comparators
      [:is_empty, :is_not_empty]
    end

    def validate_data(value, options)
      true
    end

    def options_schema
      nil
    end

    def calendar_addable?
      false
    end

    # works with default attribute on property in options_schema
    def set_options_defaults(options)
      return options unless options_schema.present?

      set_option_default_value(options_schema, options)
    end

    def validate_options(options, value, referenced = false)
      return true unless options_schema

      result = JSON::Validator.fully_validate(options_schema, options)
      result.empty? ? true : result
    end

    def on_options_changed(old_value, new_value)
      new_value
    end

    def options_definition
      return nil unless options_schema

      result = options_schema.except(:additionalProperties)
      reorganize_options_properties!(result, "content_type.#{type}.options")

      result
    end

    private

    def reorganize_options_properties!(options_def, i18n_key)
      options_def.except!(:type)

      required_options = options_def[:required] || []
      options_def.except!(:required)

      if options_def.has_key?(:properties)
        options_def[:properties].each do |property_type, property_def|
          reorganize_options_property!(property_def, required_options.include?(property_type.to_s), "#{i18n_key}.#{property_type}")
        end

        properties = options_def[:properties]
        options_def.except!(:properties)
        options_def.merge!(properties)
      end
    end

    def reorganize_options_property!(property_def, required, i18n_key)
      if property_def.has_key?(:format)
        property_def[:type] = property_def[:format]
        property_def.except!(:format)
      end
      property_def.except!(:enum)
      property_def[:required] = required

      property_def[:title] = I18n.t("#{i18n_key}.label") if I18n.exists?("#{i18n_key}.label")

      if property_def[:type] === "array" && property_def.has_key?(:items)
        reorganize_options_properties!(property_def[:items], "#{i18n_key}.items")
      end
    end

    def set_option_default_value(property_def, property_value)
      case property_def[:type]
      when "object"
        res = {}
        property_def[:properties].each do |property_key, property_val|
          res[property_key.to_s] = set_option_default_value(property_val, property_value[property_key.to_s])
        end
        res
      when "array"
        property_value.map { |val| set_option_default_value(property_def[:items], val) }
      else
        if property_def.has_key?(:default) && (property_value.nil? || (property_def[:type] === "string" && property_value.blank?))
          property_def[:default]
        else
          property_value
        end
      end
    end
  end
end
