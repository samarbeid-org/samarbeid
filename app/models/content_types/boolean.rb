class ContentTypes::Boolean < ContentTypes::Value
  class << self
    def type
      :boolean
    end

    def cast(value)
      ActiveModel::Type::Boolean.new.cast(value) unless value.nil?
    end

    def localized_string(value, options = nil)
      (value === true) ? "Ja" : "Nein"
    end

    def validate_data(value, options)
      if value.to_s.in?(["true", "false"])
        true
      else
        "muss 'true' oder 'false' sein"
      end
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      val = cast(values)

      case comp_op
      when :is_equal
        relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::boolean = :val", val: serialize(val)]))
      when :is_not_equal
        filter_by(relation, relation_alias, :is_equal, values).invert_where
      else
        super
      end
    end

    def supported_comparators
      [:is_equal, :is_not_equal] + super
    end
  end
end
