class ContentTypes::Time < ContentTypes::Value
  class << self
    def type
      :time
    end

    def cast(value)
      return nil unless value.present?

      val = ActiveModel::Type::Time.new.cast(value)&.change(year: 2000, month: 1, day: 1, offset: 0)
      def val.as_json(options = nil)
        self&.strftime("%H:%M")
      end

      val
    end

    def localized_string(value, options = nil)
      I18n.l(value, format: :short)
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      from = cast(values&.first)
      to = cast(values&.second)

      case comp_op
      when :between
        if from.present? && to.present?
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::time between :from and :to", from: serialize(from), to: serialize(to)]))
        elsif from.present?
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::time >= :from", from: serialize(from)]))
        else
          relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text::time <= :to", to: serialize(to)]))
        end
      else
        super
      end
    end

    def supported_comparators
      [:between] + super
    end
  end
end
