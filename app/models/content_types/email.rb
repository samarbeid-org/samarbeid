class ContentTypes::Email < ContentTypes::String
  class << self
    def type
      :email
    end

    def cast(value)
      super(value&.strip&.gsub(/[\u200B-\u200D\uFEFF]/, ""))
    end

    def validate_data(value, _options = nil)
      if value.empty? || /\A((.*)<)?(?:[^@<\\"\s]+|".*\S+.*")@((?:[-\p{Number}\p{Letter}\p{Mark}]+\.)+[\p{Number}\p{Letter}\p{Mark}]{2,})(?(1)>|)\z/i.match?(value)
        true
      else
        "muss eine E-Mail-Adresse sein"
      end
    end
  end
end
