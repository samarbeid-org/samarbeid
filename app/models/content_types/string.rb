class ContentTypes::String < ContentTypes::Value
  class << self
    def type
      :string
    end

    def cast(value)
      ActiveModel::Type::String.new.cast(value) unless value.nil?
    end

    def transform(from_value, from_content_type, from_options, to_options)
      return from_value if self == from_content_type

      from_content_type.localized_string(from_value, from_options)
    end

    def data_empty?(value)
      super || value.blank?
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      val = cast(values)

      case comp_op
      when :is_empty
        super.or(relation.where(relation.sanitize_sql_for_conditions(["#{relation_alias}.value::text = :val", val: serialize("")])))
      when :is_not_empty
        filter_by(relation, relation_alias, :is_empty, values).invert_where
      when :is_equal
        relation.where(relation.sanitize_sql_for_conditions(["lower(#{relation_alias}.value::text)::jsonb ? :val", val: serialize(val.downcase)]))
      when :is_not_equal
        filter_by(relation, relation_alias, :is_equal, values).invert_where
      when :does_contain
        relation.where(relation.sanitize_sql_for_conditions(["lower(#{relation_alias}.value::text) LIKE :val", val: "%#{serialize(val.downcase)}%"]))
      when :does_not_contain
        filter_by(relation, relation_alias, :does_contain, values).invert_where
      else
        super
      end
    end

    def supported_comparators
      [:does_contain, :does_not_contain, :is_equal, :is_not_equal] + super
    end
  end
end
