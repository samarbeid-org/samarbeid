class ContentTypes::Integer < ContentTypes::Value
  class << self
    def type
      :integer
    end

    def cast(value)
      ActiveModel::Type::Integer.new.cast(value) unless value.nil?
    end

    def localized_string(value, options = nil)
      ActiveSupport::NumberHelper.number_to_delimited(value)
    end

    def filter_by(relation, relation_alias, comp_op, values = nil)
      from = cast(values&.first)
      to = cast(values&.second)

      case comp_op
      when :between
        if from.present? && to.present?
          relation.where(relation.sanitize_sql_for_conditions(["(#{relation_alias}.value #>> '{}')::int between :from and :to", from: serialize(from), to: serialize(to)]))
        elsif from.present?
          relation.where(relation.sanitize_sql_for_conditions(["(#{relation_alias}.value #>> '{}')::int >= :from", from: serialize(from)]))
        else
          relation.where(relation.sanitize_sql_for_conditions(["(#{relation_alias}.value #>> '{}')::int <= :to", to: serialize(to)]))
        end
      else
        super
      end
    end

    def supported_comparators
      [:between] + super
    end
  end
end
