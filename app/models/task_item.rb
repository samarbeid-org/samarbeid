class TaskItem < ApplicationRecord
  belongs_to :task
  belongs_to :content_item
  belongs_to :task_item_definition, optional: true

  validates :position, presence: true
  validate :add_to_calendar_possible?

  def as_datafield
    {
      definition: OpenStruct.new(
        id: content_item.id || content_item&.content_item_definition&.id,
        name: show_task_reference ? content_item.label_with_task : content_item.label,
        content_type: content_item.content_type,
        required: required,
        options: content_item.options,
        infobox: info_box,
        task_item_id: id,
        task_item_count: content_item.task_items.count,
        block_count: content_item.blocks.count,
        add_to_calendar: add_to_calendar
      ),
      lock_version: content_item.lock_version,
      locked_by: content_item.locked? ? content_item.locked_by : nil,
      value: content_item.value,
      state: content_item.aasm_state,
      confirmed_at: content_item.confirmed_at,
      last_updated: last_data_change&.created_at,
      last_updated_by: last_data_change&.subject
    }
  end

  def last_data_change
    Event.where(object: content_item).order(:created_at).last
  end

  def show_task_reference
    !content_item.first_task_item(true).nil? && content_item.first_task_item(true) != self
  end

  private

  def add_to_calendar_possible?
    return if add_to_calendar.nil?
    errors.add(:add_to_calendar, "Für diesen Datentyp nicht möglich!") unless content_item.content_type.calendar_addable?
  end
end
