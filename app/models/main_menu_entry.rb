class MainMenuEntry < ApplicationRecord
  include ReferenceSupport

  belongs_to :parent, -> { where(linked_object: nil) }, class_name: "MainMenuEntry", optional: true
  has_many :children, class_name: "MainMenuEntry", foreign_key: "parent_id", dependent: :destroy
  belongs_to :linked_object, polymorphic: true, optional: true, inverse_of: :main_menu_entries

  validates :position, presence: true
  validates :position, uniqueness: {scope: :parent}

  validate :validate_type
  validate :validate_parent

  scope :readable_by, ->(user) {
    where(linked_object: WorkflowDefinition.accessible_by(user.ability, :build_instance))
      .or(where(linked_object: DossierDefinition.accessible_by(user.ability, :build_instance)))
      .or(where(linked_object: nil))
  }

  scope :ordered_by_position, -> { order(:position, :title, :id) }

  def rails_admin_label
    is_submenu? ? title : linked_object.rails_admin_label
  end

  def is_submenu?
    linked_object.nil?
  end

  def self.entries_for(user, show_empty_submenus = false, parent = nil)
    entries = where(parent: parent).accessible_by(user.ability, :show).ordered_by_position.map do |entry|
      {
        id: entry.id,
        title: entry.linked_object&.name || entry.title,
        position: entry.position,
        type: entry.linked_object&.model_name&.param_key || :submenu,
        **(entry.linked_object ? {linked_id: entry.linked_object.id} : {child_entries: entries_for(user, show_empty_submenus, entry)})
      }
    end

    show_empty_submenus ? entries : entries.select { |entry| !entry.key?(:child_entries) || entry[:child_entries].count > 0 }
  end

  def reference_label(noaccess = false)
    if is_submenu?
      "Untermenü '#{title}'"
    else
      "Hauptmenüeintrag '#{linked_object.reference_label(noaccess)}'"
    end
  end

  private

  def validate_type
    errors.add(:base, "Specify a title or a linked object, not both") unless title.nil? ^ linked_object.nil?

    errors.add(:parent, "A submenu entry must have no parent") if is_submenu? && parent.present?
  end

  def validate_parent
    return unless parent == self

    errors.add(:parent, "A menu item can't be it's own parent")
  end
end
