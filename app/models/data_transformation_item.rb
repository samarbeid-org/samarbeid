class DataTransformationItem < ApplicationRecord
  belongs_to :data_transformation, inverse_of: :data_transformation_items
  belongs_to :dossier, inverse_of: :data_transformation_items

  validate :validate_value

  scope :valid, -> { where(error_messages: nil) }
  scope :invalid, -> { where.not(error_messages: nil) }
  scope :confirmed, -> { where(confirmed: true) }
  scope :unconfirmed, -> { where(confirmed: false) }

  def value
    data_transformation.content_type.deserialize(read_attribute(:value))
  end

  def value=(new_value)
    # TODO: To copy richtext with images we need to use prepare here and also copy active_storage has_many association
    casted_new_value = data_transformation.content_type.serialize(data_transformation.content_type.cast(new_value))
    return if casted_new_value == value

    write_attribute(:value, casted_new_value)
  end

  def to_data_item
    DataItem.new(id, data_transformation.dossier_item_definition.name, value, data_transformation.content_type, data_transformation.options, data_transformation.required)
  end

  private

  def validate_value
    messages = []

    if data_transformation.required? && data_transformation.content_type.data_empty?(value)
      messages << "muss ausgefüllt werden"
    end

    if data_transformation.unique? && !data_transformation.content_type.data_empty?(value) &&
        self.class.where.not(id: id).where(data_transformation: data_transformation).where("value @> ?", read_attribute(:value).to_json).exists?
      messages << "ist bereits vergeben"
    end

    unless data_transformation.content_type.data_empty?(value)
      validation_result = data_transformation.content_type.validate_data(value, data_transformation.options)
      messages << validation_result if validation_result != true
    end

    self.confirmed = false if messages.any?

    if new_record?
      self.error_messages = messages.any? ? messages.join(". ") : nil
    else
      self.error_messages = nil
      messages.each { |msg| errors.add(:value, msg) }
    end
  end
end
