class ShareLink < ApplicationRecord
  include Devise::Models::Timeoutable   # needed because we use ShareLink as ActionCable identifier which needs to be able to timeout
  include TokenAuthenticatable
  before_create do
    regenerate_token(:token)
  end

  after_commit :on_share_link_updated, on: :update

  belongs_to :shareable, polymorphic: true
  validates :shareable, presence: true
  validates :shareable_id, uniqueness: {scope: [:shareable_type, :aasm_state]}, if: :active?
  validates :shareable_type, inclusion: {in: %w[Task]}

  include AASM

  aasm whiny_persistence: true do
    state :active, initial: true
    state :completed

    event :complete do
      transitions from: :active, to: :completed
    end
  end

  delegate :can?, :cannot?, to: :ability
  def ability
    Ability.new(self)
  end

  #############################
  # Quack like a user

  def fullname
    "[externer user] #{name}"
  end

  # used to display simple action events
  def reference_label(noaccess = false)
    name.blank? ? "unbenannt" : name
  end

  mock_user_methods = {email: nil, unconfirmed_email: false, confirmed?: true, deactivated?: true, deactivated_at: false}

  # Needed for full layout rendering (_current_user.json)
  #
  # mock_user_methods = mock_user_methods.merge({is_admin?: false, super_admin?: false})

  # def mention_label(noaccess = false)
  #   fullname + "mention"
  # end

  mock_user_methods.each do |method_name, return_value|
    define_method(method_name) { return_value }
  end

  def listable_dossier_definition_ids
    return [] unless shareable.is_a?(Task)
    shareable.task_items.select { |ti| ti.content_item.content_type == ContentTypes::Dossier }.map { |ti| ti.content_item.options["type"] }.uniq
  end

  def groups
    Group.all
  end

  private

  def on_share_link_updated
    MonitorChannel.broadcast_object_updated(shareable)
  end
end
