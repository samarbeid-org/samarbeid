class User < ApplicationRecord
  include ReferenceSupport
  include NotificationSupport
  include DeactivationSupport
  include TokenAuthenticatable
  include SearchForList
  include FilterSupport::UserFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: [:firstname, :lastname, :email],
    using: {
      tsearch: {prefix: true}
    }

  has_one_attached :avatar
  attr_accessor :remove_avatar

  after_save { avatar.purge if remove_avatar == "1" }
  after_commit :init_new_user, on: :create

  has_many :user_groups, dependent: :destroy
  has_many :groups, through: :user_groups, inverse_of: :users, dependent: :destroy

  has_many :assigned_workflows, class_name: "Workflow", foreign_key: "assignee_id", inverse_of: :assignee, dependent: :nullify
  has_many :assigned_tasks, class_name: "Task", foreign_key: "assignee_id", inverse_of: :assignee, dependent: :nullify
  has_many :candidate_tasks_definitions, class_name: "TaskDefinition", foreign_key: "candidate_assignee_id", inverse_of: :candidate_assignee, dependent: :nullify
  has_and_belongs_to_many :marked_tasks, class_name: "Task", association_foreign_key: :workflow_task_id, join_table: :users_workflow_tasks_marked

  has_many :contributions, dependent: :destroy
  has_many :contributed_workflows, -> { order "contributions.created_at" }, through: :contributions, source: :contributable, source_type: "Workflow", inverse_of: :contributors
  has_many :contributed_tasks, -> { order "contributions.created_at" }, through: :contributions, source: :contributable, source_type: "Task", inverse_of: :contributors

  has_many :events, foreign_key: "subject_id", inverse_of: :subject, dependent: :nullify
  has_many :notifications, dependent: :destroy

  has_many :comments, foreign_key: "author_id", inverse_of: :author, dependent: :nullify

  validates :firstname, :lastname, presence: true
  validates_no_deactivated_prefix :firstname, :lastname
  validates_no_prefix(I18n.t("state_support.states.unconfirmed"), :firstname, :lastname)

  attribute :validate_email_with_confirmation, :boolean
  attr_accessor :email_confirmation
  attr_reader :act_as_normal_user
  validates :email, confirmation: true, if: :validate_email_with_confirmation?
  validates :email_confirmation, presence: true, if: :validate_email_with_confirmation?

  scope :with_fullname, -> { select(arel.projections, Arel.sql("concat_ws(' ', firstname, lastname) As fullname")) }
  scope :reorder_by_name_with_user_first, ->(user) { reorder(Arel.sql("(users.id = #{user.id}) desc")).order(:firstname, :lastname, :id) }
  scope :filter_by_groups, ->(group_ids, with_pg_search_rank = false) {
    grouping_attributes = ["users.id"]
    grouping_attributes << sanitize_sql_array(%W[%s #{PgSearch::Configuration.alias("users")}.rank]) if with_pg_search_rank
    joins(:user_groups).where(user_groups: {group_id: group_ids}).group(grouping_attributes.join(", "))
  }
  scope :except_users, ->(ids_to_exclude) { where.not("users.id": ids_to_exclude) }
  scope :confirmed, -> { where.not(confirmed_at: nil) }
  scope :unconfirmed, -> { where(confirmed_at: nil) }
  scope :admins, -> { where(role: [:team_admin, :super_admin]) }
  scope :not_admins, -> { where(User.admins.arel.constraints.reduce(:and).not) }

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, # basic authentication support for hashed password in users table
    :timeoutable, # timouts session after configure period. Without this user is NEVER signed out by default (even if "remember me" is NOT checked)
    :rememberable, # makes session cookie expire after 14 days if "remember me" is checked on login.
    :validatable, #  creates all needed validations for a user email and password
    :recoverable,
    :confirmable

  delegate :can?, :cannot?, to: :ability

  enum role: {user: 0, team_admin: 1, super_admin: 2}

  attr_writer :act_as_normal_user

  def build_ability(act_as_normal_user)
    Ability.new(self, act_as_normal_user)
  end

  def ability
    build_ability(@act_as_normal_user)
  end

  def is_admin?
    super_admin? || team_admin?
  end

  def name
    "#{firstname} #{lastname}"
  end

  def unconfirmed_prefix
    (!confirmed?) ? I18n.t("state_support.states.unconfirmed") : nil
  end

  def prefixes
    [unconfirmed_prefix, deactivated_prefix]
  end

  def fullname
    prepend_prefixes(name, prefixes)
  end

  def identifier
    "@#{id}"
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    fullname.presence || identifier
  end

  def mention_label(noaccess = false)
    noaccess ? identifier : prepend_prefixes("@#{name}", prefixes)
  end

  def avatar_label
    "#{firstname[0]}#{lastname[0]}"
  end

  def avatar_url(size = nil)
    size ||= 36
    return nil unless avatar.attached?
    Rails.application.routes.url_helpers.rails_representation_url(avatar.variant(resize_to_fill: [size, size]), only_path: true)
  end

  def active_for_authentication?
    super && activated?
  end

  def self.send_reset_password_instructions(attributes = {})
    recoverable = find_or_initialize_with_errors(reset_password_keys, attributes, :not_found)
    if recoverable.persisted?
      if recoverable.activated?
        recoverable.send_reset_password_instructions
      else
        recoverable.errors.add(:base, I18n.t("devise.errors.messages.deactivated"))
      end
    end
    recoverable
  end

  def after_confirmation
    Events::ConfirmedEvent.create!(object: self)
  end

  def info
    OpenStruct.new({
      count_undone_notifications: notifications.unread.count,
      has_unread_notifications: notifications.unread.exists?,
      count_open_user_assigned_tasks: Task.current_and_assigned_to(self).count,
      has_due_open_user_assigned_tasks: Task.current_and_assigned_to(self).with_due_in_past.exists?
    })
  end

  def token_auth_disabled?
    deactivated? || !confirmed?
  end

  protected

  def password_required?
    confirmed? ? super : false
  end

  private

  def init_new_user
    Group.system_group_all.users << self unless groups.include?(Group.system_group_all)
  end
end
