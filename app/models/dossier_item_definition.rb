class DossierItemDefinition < ApplicationRecord
  include ContentTypeSupport
  include RichtextImageAttachable

  belongs_to :definition, class_name: "DossierDefinition", inverse_of: :items
  has_many :dossier_items, inverse_of: :dossier_item_definition, dependent: :destroy
  has_many :data_transformations, inverse_of: :dossier_item_definition, dependent: :destroy

  validates :name, presence: true, unless: -> { content_type.to_s === ContentTypes::Note.to_s }
  validates :position, :content_type, presence: true
  validates :name, uniqueness: {scope: :definition_id, case_sensitive: false}, allow_blank: true
  validates :position, uniqueness: {scope: :definition_id}
  validate :validate_unchangeable_items, on: :update
  validate :validate_default_value

  scope :required, -> { where(required: true) }
  scope :recommended, -> { where(recommended: true) }
  scope :required_or_recommended, -> { required.or(recommended) }
  scope :no_notes, -> { where.not(content_type: ContentTypes::Note.to_s) }

  def default_value
    content_type.deserialize(read_attribute(:default_value))
  end

  def default_value=(new_value)
    casted_new_value = content_type.serialize(content_type.prepare(content_type.cast(new_value), self))
    return if casted_new_value == default_value

    write_attribute(:default_value, casted_new_value)
  end

  def get_content_type_values
    dossier_items.map { |di| di.value }
  end

  def computed_label
    if content_type.to_s === ContentTypes::Note.to_s
      note_label = ActionView::Base.full_sanitizer.sanitize(default_value)&.truncate(50)
      note_label = "\"#{note_label}\"" if !note_label.blank?
      return note_label
    end

    label
  end

  # for rails admin
  def rails_admin_label
    "(##{id}) #{name}"
  end

  def label
    name
  end

  private

  def validate_unchangeable_items
    errors.add(:definition, "kann nicht geändert werden") if definition_id_changed?
  end

  def validate_default_value
    return if default_value.nil?
    validation_result = content_type.validate_data(default_value, options)
    errors.add(:default_value, validation_result) if validation_result != true
  end
end
