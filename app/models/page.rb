class Page < ApplicationRecord
  include SearchForList
  include FilterSupport::PageFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: {title: "A", slug: "B"},
    using: {
      tsearch: {prefix: true}
    }

  has_many :menu_entries, dependent: :destroy

  before_validation :set_slug
  validates :slug, :title, presence: true
  validates :slug, uniqueness: true
  validates :slug, format: {with: /\A.*[^0-9]+.*\z/i, message: "darf keine Zahl sein"}

  # for rails admin
  def name
    slug
  end

  private

  def set_slug
    self.slug = slug&.parameterize
  end
end
