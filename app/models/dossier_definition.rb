class DossierDefinition < ApplicationRecord
  include ReferenceSupport
  include ContentTypeAttributeSupport
  include SearchForList
  include FilterSupport::DossierDefinitionFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: [:name],
    using: {
      tsearch: {prefix: true}
    }

  has_many :items, -> { order(:position) }, class_name: "DossierItemDefinition", foreign_key: "definition_id", dependent: :destroy, inverse_of: :definition
  has_many :dossiers, foreign_key: "definition_id", dependent: :destroy, inverse_of: :definition

  has_and_belongs_to_many :groups, after_add: :after_groups_changed, after_remove: :after_groups_changed

  has_many :comments, as: :object, dependent: :destroy
  has_many :main_menu_entries, -> { order(:position) }, as: :linked_object, dependent: :destroy, inverse_of: :linked_object

  scope :hidden, -> { where.missing(:groups) }

  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validate :validate_title_items
  validate :validate_subtitle_items

  after_save :reindex_associated_dossiers
  before_destroy :check_for_usage_before_destroy, prepend: true
  after_commit :main_menu_auto_add, on: :create
  after_commit :main_menu_update, on: [:update, :destroy]
  after_commit :update_dossier_title_and_subtitle, on: [:update]

  # for rails admin
  accepts_nested_attributes_for :items, allow_destroy: true

  content_type_attribute :description, ContentTypes::Richtext

  # for rails admin
  def rails_admin_label
    "(##{id}) #{name}"
  end

  def reference_label(_noaccess = false)
    name.presence || id
  end

  def referenced_in_dossier_item_definitions_for(user)
    DossierItemDefinition.where(content_type: ContentTypes::Dossier.to_s)
      .where("options @> ?", {type: id}.to_json)
      .accessible_by(user.ability, :show).sort_by do |item|
      item.content_type.inverseReference(item)[:title]
    end
  end

  def check_for_usage_before_destroy
    content_item_definitions = ContentItemDefinition.where(content_type: "dossier").where("options @> ?", {type: id}.to_json)
    dossier_item_definitions = DossierItemDefinition.where(content_type: "dossier").where("options @> ?", {type: id}.to_json)

    if content_item_definitions.any? || dossier_item_definitions.any?
      errors.add :base, "Kann nicht gelöscht werden"

      if content_item_definitions.any?
        content_item_definitions.to_a.group_by(&:workflow_definition_id).each do |workflow_definition_id, ci_definitions|
          workflow_definition = WorkflowDefinition.find_by(id: workflow_definition_id)
          if workflow_definition
            fields = ci_definitions.map { |cid| "'#{cid.label}'" }.join(", ")
            errors.add :base, "wird in den Feldern #{fields} der Prozessvorlage '#{workflow_definition.name}' verwendet"
          end
        end

        if dossier_item_definitions.any?
          dossier_item_definitions.to_a.group_by(&:definition_id).each do |dossier_definition_id, item_definitions|
            dossier_definition = DossierDefinition.find_by(id: dossier_definition_id)
            if dossier_definition
              fields = item_definitions.map { |did| "'#{did.name}'" }.join(", ")
              errors.add :base, "wird in den Feldern #{fields} der Dossiervorlage '#{dossier_definition.name}' verwendet"
            end
          end
        end

        throw(:abort)
      end
    end
  end

  def title_item_definitions
    DossierItemDefinition.find(title_fields)
  end

  def subtitle_item_definitions
    DossierItemDefinition.find(subtitle_fields)
  end

  def build_dossier(data_items = nil, required_and_recommended_items_only: false)
    data_items = {} unless data_items&.is_a?(Hash)

    dossier = Dossier.new(definition: self)

    item_definitions = required_and_recommended_items_only ? items.required_or_recommended : items
    item_defs = item_definitions.map { |did| [did.id.to_s, did.default_value] }.to_h
    item_defs.merge(data_items.transform_keys { |key| key.to_s }).each do |dossier_item_def_id, value|
      item_definition = DossierItemDefinition.find(dossier_item_def_id)
      unless item_definition.content_type == ContentTypes::Note
        dossier.dossier_items << DossierItem.new(dossier: dossier, dossier_item_definition: item_definition, value: value)
      end
    end

    dossier
  end

  private

  def validate_title_items
    if title_fields.is_a?(Array)
      title_fields.each do |field_id|
        unless items.where(id: field_id).exists?
          errors.add(:title_fields, "referenziert das Feld mit der ID (#{field_id}), welches in dieser Dossier Definition nicht existert")
        end
      end
    else
      errors.add(:title_fields, "muss ein Array von Feldern sein")
    end
  end

  def validate_subtitle_items
    if subtitle_fields.is_a?(Array)
      subtitle_fields.each do |field_id|
        unless items.where(id: field_id).exists?
          errors.add(:subtitle_fields, "referenziert das Feld mit der ID (#{field_id}), welches in dieser Dossier Definition nicht existert")
        end
      end
    else
      errors.add(:subtitle_fields, "muss ein Array von Feldern sein")
    end
  end

  def reindex_associated_dossiers
    ReindexSearchablesJob.perform_later(self, :dossiers)
  end

  def after_groups_changed(_params)
    MenuChannel.broadcast_update
  end

  def main_menu_auto_add
    Interactors::MainMenuEntryInteractor.create_many(nil, [self], nil) unless Setting.main_menu_auto_add_deactivated
  end

  def main_menu_update
    MenuChannel.broadcast_update if destroyed? || name_previously_changed?
  end

  def update_dossier_title_and_subtitle
    title_config_changed = title_fields_previously_changed? || (title_fields.empty? && (id_previously_changed? || name_previously_changed?))
    subtitle_config_changed = subtitle_fields_previously_changed?
    return unless title_config_changed || subtitle_config_changed

    SyncTitlesJob.perform_later(self, :dossiers)
  end
end
