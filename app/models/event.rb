class Event < ApplicationRecord
  # Temporarily disabled SupersedeSupport for #1277
  def obsolete?
    false
  end
  # include SupersedeSupport

  attr_accessor :triggered_by

  after_commit :dispatch_event_on_create, on: :create
  after_commit :dispatch_event_on_update, on: :update

  belongs_to :object, polymorphic: true
  belongs_to :subject, class_name: "User", optional: true
  has_many :notifications, dependent: :destroy

  serialize :data, Services::ActiveRecordToGlobalIdSerializer

  validate :validate_data

  scope :for_object, ->(object) { where(object: object).or(where(object: Comment.where(object: object))).order(created_at: :desc) }

  def new_value
    nil
  end

  def old_value
    nil
  end

  def mentioned_users
    []
  end

  def payload(current_user)
    nil
  end

  def self.invalid_records
    all.find_each.reject do |event|
      event.valid? || begin
        event.data
      rescue
        Sentry.capture_exception(error)
        nil
      end
    end
  end

  def localized_object(referenced, current_user, relative_date, indicate_obsolete)
    {
      id: id,
      action: action,
      avatar: avatar(referenced),
      subject: {
        id: subject&.id
      },
      chunks: localized_message_chunks(referenced, current_user, relative_date, indicate_obsolete),
      payload: payload(current_user),
      target: target(current_user)
    }
  end

  def target(current_user)
    object.reference_object(current_user).merge({event: id, hash: "#timeline-item-#{action}-#{id}"})
  end

  private

  def localized_message_chunks(referenced, current_user, relative_date, indicate_obsolete)
    path_subkey = referenced ? "referenced" : "normal"

    path_prefix = "event.#{subject ? "with_subject" : "without_subject"}.#{action}"
    path = I18n.exists?("#{path_prefix}.#{path_subkey}") ? "#{path_prefix}.#{path_subkey}" : path_prefix

    new_value_part = i18n_t_or_default("#{path_prefix}.new_value_part") if new_value.present?
    old_value_part = i18n_t_or_default("#{path_prefix}.old_value_part") if old_value.present?
    delete_value_part = i18n_t_or_default("#{path_prefix}.delete_value_part") if new_value.blank?

    mentioned_part = nil
    not_mentioned_part = nil

    if mentioned_users.present? && mentioned_users.include?(current_user)
      mentioned_part = i18n_t_or_default("#{path_prefix}.mentioned_part")
    else
      not_mentioned_part = i18n_t_or_default("#{path_prefix}.not_mentioned_part")
    end

    text = I18n.t(
      path,
      old_value_part: old_value_part,
      new_value_part: new_value_part,
      delete_value_part: delete_value_part,
      mentioned_part: mentioned_part,
      not_mentioned_part: not_mentioned_part,
      subject: "%{subject}",
      obj: "%{obj}",
      new_value: "%{new_value}",
      old_value: "%{old_value}"
    )

    result = []
    text.strip.split(/(%\{\w+\})/) do |sub|
      unless sub.empty?
        case sub
        when "%{subject}"
          result += chunks_for(subject, "subject", true, current_user)
        when "%{obj}"
          result += chunks_for(object, "object", referenced, current_user)
        when "%{old_value}"
          result += chunks_for(translate_value(old_value), "value", true, current_user)
        when "%{new_value}"
          result += chunks_for(translate_value(new_value), "value", true, current_user)
        else
          result << {type: "text", text: sub}
        end
      end
    end

    result << {type: "text", text: " • "}

    result << if relative_date
      {
        type: "text",
        text: ApplicationController.helpers.time_ago_in_words(at, scope: "datetime.distance_in_words.ago"),
        tooltip: I18n.l(at)
      }
    else
      {type: "text", text: I18n.l(at)}
    end

    if changed_at
      result << {type: "text", text: " • bearbeitet "}

      result << if relative_date
        {
          type: "text",
          text: ApplicationController.helpers.time_ago_in_words(changed_at, scope: "datetime.distance_in_words.ago"),
          tooltip: I18n.l(created_at)
        }
      else
        {type: "text", text: I18n.l(changed_at)}
      end
    end

    result << {type: "text", text: " • " + i18n_t_or_default("event.outdated")} if indicate_obsolete && obsolete?

    result
  end

  def chunks_for(value, position, referenced, current_user)
    result = []

    case value
    when Workflow
      result << {type: "text", text: I18n.t("event_object.workflow")}
      if referenced
        result << {type: "text", text: " "}
        result << value.reference_object(current_user).merge(position: position)
      end
    when WorkflowDefinition
      result << {type: "text", text: I18n.t("event_object.workflow_definition")}
      if referenced
        result << {type: "text", text: " "}
        result << value.reference_object(current_user).merge(position: position)
      end
    when Task
      result << {type: "text", text: I18n.t("event_object.task")}
      if referenced
        result << {type: "text", text: " "}
        result << value.reference_object(current_user).merge(position: position)
        result << {type: "text", text: " "}
        result << {type: "text", text: I18n.t("event_object.of_workflow")}
        result << {type: "text", text: " "}
        result << value.workflow.reference_object(current_user).merge(position: position) if value.workflow
      end
    when User
      result << {type: "text", text: I18n.t("event_object.user")} unless position === "subject"
      result << {type: "text", text: " "} if position != "subject" && referenced
      result << value.reference_object(current_user).merge(position: position) if referenced
    when ShareLink
      result << {type: "text", text: value.fullname}
    when Dossier
      result << {type: "text", text: I18n.t("event_object.dossier")}
      if referenced
        result << {type: "text", text: " "}
        result << value.reference_object(current_user).merge(position: position)
      end
    when DossierDefinition
      result << {type: "text", text: I18n.t("event_object.dossier_definition")}
      if referenced
        result << {type: "text", text: " "}
        result << value.reference_object(current_user).merge(position: position)
      end
    when Automation
      result << {type: "text", text: I18n.t("event_object.automation")}
      if referenced
        result << {type: "text", text: " "}
        result << value.reference_object(current_user).merge(position: position)
      end
    else
      result << if value.respond_to?(:reference_object)
        value.reference_object(current_user).merge(position: position)
      else
        {type: "text-value", text: value}
      end
    end

    result
  end

  def i18n_t_or_default(key, default: nil)
    I18n.exists?(key) ? I18n.t(key) : default
  end

  def at
    created_at
  end

  def action
    self.class.name.demodulize.sub(/Event\Z/, "").underscore
  end

  def avatar(referenced)
    # Those icons need to be generated as pngs for E-Mails as well. See lib/tasks/emails.rake.
    # It's expecting lines in the format {icon: "mdi-foo"}. If the format is changed or an additional style is used, the rake tasks need to be adapted.
    case action
    when "created"
      {icon: "mdi-plus"}
    when "readied"
      {icon: "mdi-flag-outline"}
    when "started"
      {icon: "mdi-play-outline"}
    when "completed"
      {icon: "mdi-check"}
    when "skipped"
      {icon: "mdi-debug-step-over"}
    when "deleted"
      {icon: "mdi-trash-can-outline"}
    when "reopened"
      {icon: "mdi-undo"}
    when "assigned", "unassigned"
      {icon: "mdi-account-outline"}
    when "added_contributor", "removed_contributor"
      {icon: "mdi-account-multiple-outline"}
    when "added_successor_workflow"
      {icon: "mdi-location-exit"}
    when "added_workflow", "removed_workflow", "completed_workflow"
      {icon: "mdi-bookmark-plus-outline"}
    when "changed_due_date"
      {icon: "mdi-calendar-blank-outline"}
    when "changed_title", "changed_summary"
      {icon: "mdi-playlist-edit"}
    when "changed_visibility"
      {icon: "mdi-lock-open-outline"}
    when "feature_released"
      {icon: "mdi-new-box"}
    when "changed_data"
      {icon: "mdi-database-edit-outline"}
    when "changed_time_tracking_budget"
      {icon: "mdi-timer-outline"}
    else
      {icon: "mdi-information-outline"}
    end
  end

  def translate_value(value)
    value
  end

  def changed_at
    nil
  end

  def dispatch_event_on_create
    Services::EventDispatcher.process!(self)
    MonitorChannel.broadcast_events_updated(object) unless is_a?(Events::CommentedEvent) || is_a?(Events::ChangedDataEvent)
  end

  def dispatch_event_on_update
    if respond_to?(:notification_receivers_added?) && notification_receivers_added?
      Services::EventDispatcher.process!(self)
    end
  end

  def validate_data
    errors.add(:data, "should be a hash") unless data.nil? || data.is_a?(Hash)
  end

  def validate_object_is_one_of(*klasses)
    return if klasses.any? { |klass| object.is_a?(klass) }

    klass_part = klasses.to_sentence(two_words_connector: " or ", last_word_connector: " or ")
    errors.add(:object, "should be an '#{klass_part}'")
  end
end
