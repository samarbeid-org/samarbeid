class TaskItemDefinition < ApplicationRecord
  belongs_to :task_definition
  belongs_to :content_item_definition
  has_many :task_items, dependent: :nullify
  belongs_to :workflow_definition

  validates :position, presence: true
  validate :all_belong_to_same_workflow_definition
  validate :add_to_calendar_possible?

  def rails_admin_label
    "#{task_definition&.rails_admin_label} > #{content_item_definition&.label}"
  end

  # optionally takes the list of the workflow's content items, so they're not queried multiple times during workflow creation
  def build_task_item(task, content_items: task.workflow.content_items)
    # find content item which was created before during workflow building.
    content_item = content_items.find { |ci| ci.content_item_definition_id == content_item_definition_id }
    TaskItem.new(task: task, position: position, info_box: info_box, required: required, add_to_calendar: add_to_calendar,
      content_item: content_item, task_item_definition: self)
  end

  def human_model_name
    "#{content_item_definition.human_model_name} der #{task_definition.human_model_name}"
  end

  private

  def add_to_calendar_possible?
    return if add_to_calendar.nil?
    errors.add(:add_to_calendar, "Für diesen Datentyp nicht möglich!") unless content_item_definition.content_type.calendar_addable?
  end

  def all_belong_to_same_workflow_definition
    return unless task_definition && content_item_definition

    unless [task_definition.workflow_definition_id, content_item_definition.workflow_definition_id].all? { |ref_id| ref_id.eql?(workflow_definition_id) }
      errors.add(:base, "Alles muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
  end
end
