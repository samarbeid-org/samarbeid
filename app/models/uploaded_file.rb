class UploadedFile < ApplicationRecord
  has_one_attached :file
  validates :file, size: {less_than: 100.megabytes, message: "Datei muss kleiner als 100 Mb sein!"}

  before_save do
    if attachment_changes["file"]
      file_content, content_type = get_content_with_type
      self.file_as_text = extract_text(file_content, content_type, false)
    end
  end

  after_save do
    if attachment_changes["file"]
      _, content_type = get_content_with_type
      if content_type == "application/pdf"
        ExtractTextFromFileJob.perform_later(self)
      end
    end
  end

  def title
    return read_attribute(:title) unless read_attribute(:title).blank?
    file.filename.to_s
  end

  def as_search_data(prefix: nil)
    {title: title,
     file_name: filename_for_search,
     file_content: to_text}.transform_keys { |key| "#{prefix}#{UploadedFile.human_attribute_name(key)}" }
  end

  def self.invalid_records
    all.find_each.reject do |uploaded_file|
      ContentItem.where(content_type: ContentTypes::File.to_s).select { |ci| ci.value&.include?(uploaded_file) }.any? ||
        ContentItemDefinition.where(content_type: ContentTypes::File.to_s).select { |cid| cid.default_value&.include?(uploaded_file) }.any? ||
        DossierItemDefinition.where(content_type: ContentTypes::File.to_s).select { |did|
          did.dossier_items.select { |di| di.value&.include?(uploaded_file) }.any?
        }.any?
    end
  end

  def update_extracted_text_and_reindex_owner_object
    file_content, content_type = get_content_with_type
    update!(file_as_text: extract_text(file_content, content_type, true))

    ContentItem.where(content_type: ContentTypes::File.to_s).select { |ci| ci.value&.include?(self) }.map(&:workflow).uniq.each(&:reindex_as_search_document)

    dossiers = []
    DossierItemDefinition.where(content_type: ContentTypes::File.to_s).each { |dfd|
      dfd.dossier_items.each { |di| dossiers << di.dossier if di.value&.include?(self) }
    }
    dossiers.uniq.each(&:reindex_as_search_document)
  end

  private

  def to_text
    file_as_text
  end

  def get_content_with_type
    begin
      content_type = file.blob.content_type
      file_content = file.download
    rescue
      return unless attachment_changes["file"]
      # This is a bad hack. Because ActiveStorage enqueues a job to actually process the attached file we can not access the blob/file for a long time
      # One way around for now (there exist no callbacks - see for example https://github.com/rails/rails/issues/35044)
      # Best seems to be to use a different gem for uploading files
      attachable = attachment_changes["file"].attachable

      tmp_file_attachment = attachable.is_a?(Hash) ? attachable[:io] : attachable.to_io
      return unless tmp_file_attachment
      file_content = File.read tmp_file_attachment
      content_type = IO.popen(
        ["file", "--brief", "--mime-type", tmp_file_attachment.path],
        in: :close, err: :close
      ) { |io| io.read.chomp }
    end

    [file_content, content_type]
  end

  def extract_text(file_content, content_type, extract_pdf_inline_images)
    return unless file_content && content_type

    rest_client_response = RestClient::Request.execute(method: :put, url: Tika::Configuration.new.url,
      headers: {"Content-Type": content_type, Accept: "text/html", "X-Tika-PDFextractInlineImages": extract_pdf_inline_images},
      timeout: extract_pdf_inline_images ? 300 : 5,
      payload: file_content)
    file_as_utf_text = Nokogiri::HTML.parse(rest_client_response.force_encoding("UTF-8").to_s).text # TODO: We should use 'Nokogiri::HTML5.parse' but this introduces problems with non printable special characters
    file_as_utf_text.gsub(/[[:space:]]/, " ").squish.truncate(5_000_000, omission: " [length limited to 5 million chars]")
  rescue RestClient::Exception => exception
    Sentry.capture_exception(exception)
  end

  def filename_for_search
    filename = file.blob.filename.to_s
    searchable_filename = filename.gsub(/[.\-_]/, " ")
    [filename, searchable_filename].join("\n")
  end
end
