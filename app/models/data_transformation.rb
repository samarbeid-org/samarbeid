class DataTransformation < ApplicationRecord
  include ContentTypeSupport

  belongs_to :dossier_item_definition, inverse_of: :data_transformations
  has_many :data_transformation_items, inverse_of: :data_transformation, dependent: :destroy

  after_create :create_missing_items

  def get_content_type_values
    values = data_transformation_items.map { |di| di.value }
    values.empty? ? [nil] : values
  end

  def create_missing_items
    dossiers = dossier_item_definition.definition.dossiers.where.not(id: data_transformation_items.select(:dossier_id))
    dossiers = dossiers.with_item_of_definition(dossier_item_definition.id) unless required

    dossiers.find_each do |dossier|
      source_value = dossier.get_item_value(dossier_item_definition)

      if required || !source_value.nil?
        data_transformation_items.create(
          data_transformation: self,
          dossier: dossier,
          value: content_type.transform(source_value, dossier_item_definition.content_type, dossier_item_definition.options, options),
          confirmed: false
        )
      end
    end
  end

  def apply
    create_missing_items
    errors.add(:base, "kann nicht ausgeführt werden solang es fehlerhafte Elemente gibt") && return if data_transformation_items.invalid.exists?

    ActiveRecord::Base.transaction do
      dossier_item_definition.content_type = content_type.to_s
      dossier_item_definition.options = options
      dossier_item_definition.required = required
      dossier_item_definition.unique = unique

      data_transformation_items.each do |item|
        dossier_item = item.dossier.dossier_items.find_or_initialize_by(dossier_item_definition: dossier_item_definition)
        dossier_item.dossier_item_definition = dossier_item_definition
        dossier_item.value = item.value
        dossier_item.save!
      end

      dossier_item_definition.save!

      destroy!
    end
  end
end
