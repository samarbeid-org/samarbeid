class MenuEntry < ApplicationRecord
  include FilterSupport::MenuEntryFilterSupport

  belongs_to :page, optional: true

  enum location: [:footer_menu].map { |entry| [entry.to_sym, entry.to_s] }.to_h

  validates :location, :position, :title, presence: true
  validates :position, uniqueness: {scope: :location}
  validate :validate_presence_of_page_or_url

  scope :with_location_position, -> { select(arel.projections, Arel.sql("concat_ws('_', location, position) As location_position")) }

  private

  def validate_presence_of_page_or_url
    if page.present? && url.present?
      errors.add(:page, "darf nicht angegeben werden, wennn eine #{self.class.human_attribute_name(:url)} angegeben wurde")
      errors.add(:url, "darf nicht angegeben werden, wennn eine #{self.class.human_attribute_name(:page)} angegeben wurde")
    elsif page.blank? && url.blank?
      errors.add(:page, "muss angegeben werden, wennn keine #{self.class.human_attribute_name(:url)} angegeben wurde")
      errors.add(:url, "muss angegeben werden, wennn keine #{self.class.human_attribute_name(:page)} angegeben wurde")
    end
  end
end
