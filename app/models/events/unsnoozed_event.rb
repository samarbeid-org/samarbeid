class Events::UnsnoozedEvent < Event
  validate :validate_event_type_values

  def notification_receivers
    [object.assignee || object.workflow.assignee].compact
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task)
  end
end
