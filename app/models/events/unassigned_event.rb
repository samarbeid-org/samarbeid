class Events::UnassignedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def old_assignee
    data&.[](:old_assignee)
  end

  alias_method :old_value, :old_assignee

  def notification_receivers
    receivers = [object.assignee] | [old_assignee]
    receivers |= [object.workflow.assignee] if object.is_a?(Task) && object.assignee.blank?

    receivers.compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
    errors.add(:data, "value for key :old_assignee should be a user") unless old_assignee.is_a?(User)
  end
end
