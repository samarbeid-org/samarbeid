class Events::FeatureReleasedEvent < Event
  validate :validate_event_type_values

  def notification_receivers
    User.all
  end

  def news
    object
  end

  def payload(_current_user)
    {message: "<p><strong>#{news.title}</strong></p><p>#{news.description}</p>"}
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(News)
  end
end
