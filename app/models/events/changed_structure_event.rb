class Events::ChangedStructureEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  private

  def validate_event_type_values
    validate_object_is_one_of(WorkflowDefinition, DossierDefinition)
  end
end
