class Events::ChangedStructureEventByInstance < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def workflow_used_for_update
    data&.[](:workflow_used_for_update)
  end
  alias_method :old_value, :workflow_used_for_update

  def new_workflow_definition_version
    data&.[](:new_workflow_definition_version)
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(WorkflowDefinition)
    errors.add(:data, "value for key :workflow_used_for_update should be workflow") unless workflow_used_for_update.is_a?(Workflow)
    errors.add(:data, "value for key :new_workflow_definition_version should be an Integer") unless new_workflow_definition_version.is_a?(Integer)
  end
end
