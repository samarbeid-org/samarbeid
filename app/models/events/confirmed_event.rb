class Events::ConfirmedEvent < Event
  validate :validate_event_type_values

  def notification_receivers
    User.admins
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(User)
  end
end
