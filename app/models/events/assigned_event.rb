class Events::AssignedEvent < Event
  validate :validate_event_type_values

  def new_assignee
    data&.[](:new_assignee)
  end

  def old_assignee
    data&.[](:old_assignee)
  end

  alias_method :new_value, :new_assignee
  alias_method :old_value, :old_assignee

  def notification_receivers
    [object&.assignee, old_assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
    errors.add(:data, "value for key :new_assignee should be a user") unless new_assignee.is_a?(User)
    errors.add(:data, "value for key :old_assignee should be a user") unless old_assignee.nil? || old_assignee.is_a?(User)
  end
end
