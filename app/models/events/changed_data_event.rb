class Events::ChangedDataEvent < Event
  validate :validate_event_type_values
  validate :subject_or_external_user

  def subject
    super || data&.[](:share_link)
  end

  def get_parent_element
    data&.[](:parent_element)
  end

  def parent_element
    return get_parent_element if get_parent_element.present?

    # When setting process creation we omit parent_element and deviate it from content_item.
    # Also: Legacy data changes on content_items without recording the task
    return get_object.workflow if get_object.respond_to?(:workflow)
    return get_object.dossier if get_object.respond_to?(:dossier)

    get_object
  end

  def field_title
    get_object.label if get_object.is_a?(ContentItem)
  end
  alias_method :old_value, :field_title

  def new_value
    true if get_object.is_a?(Dossier)
  end

  alias_method :get_object, :object
  def object
    parent_element
  end

  private

  def subject_or_external_user
    errors.add(:subject, "should be a User or data[:share_link] should be present") unless subject.is_a?(User) || data&.[](:share_link).is_a?(ShareLink)
  end

  def validate_event_type_values
    errors.add(:object, "should be an #{ContentItem}, #{DossierItem} or #{Dossier}") unless get_object.is_a?(ContentItem) || get_object.is_a?(DossierItem) || get_object.is_a?(Dossier)
    validate_object_is_one_of(Task, Workflow, Dossier)
    errors.add(:data, "value for key :parent_element should be a #{Task} or a #{Dossier}") unless get_parent_element.nil? || get_parent_element.is_a?(Task) || get_parent_element.is_a?(Dossier)
  end
end
