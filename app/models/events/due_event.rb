class Events::DueEvent < Event
  validate :validate_event_type_values

  def notification_receivers
    return [object.assignee] if object.assignee.present?
    return [object.workflow.assignee] if object.workflow.assignee.present?

    object.contributors
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task)
  end
end
