class Events::CommentedEvent < Event
  scope :with_time_tracking, -> { filter { |event| (event.time_spent_in_minutes || 0) > 0 } }

  validate :validate_event_type_values

  validates :subject, presence: true

  def mentioned_users
    data&.[](:mentioned_users) || []
  end

  def time_spent_in_minutes
    data&.[](:time_spent_in_minutes)
  end

  def time_spent_at
    data&.[](:time_spent_at)
  end

  def mentioned_users_previously_was
    data_previously_was&.[](:mentioned_users) || []
  end

  def previous_commenters
    object.comments.map(&:author).uniq
  end

  def notification_receivers
    receivers = []
    receivers |= [object.assignee] if object.respond_to?(:assignee)
    receivers |= object.contributors if object.respond_to?(:contributors)
    receivers |= previous_commenters if object.respond_to?(:comments)

    receivers = receivers.compact.uniq - [subject] # remove subject here and keep it if mentioned explicitely
    (receivers | mentioned_users).compact.uniq
  end

  def notification_receivers_added?
    !(mentioned_users - mentioned_users_previously_was).empty?
  end

  alias_method :set_object, :object=
  def object=(value)
    raise StandardError, "#{self.class} doesn't allow to set object directly. Use comment setter instead."
  end

  alias_method :get_object, :object
  def object
    comment&.object
  end

  def comment=(value)
    set_object(value)
  end

  def comment
    get_object
  end

  def payload(current_user)
    unless comment.deleted?
      result = {message: Services::Mentioning.add_mention_labels(comment.message, current_user)}
      unless time_spent_in_minutes.nil?
        result[:time_tracking] = {spentAt: ContentTypes::Date.serialize(time_spent_at), timeSpentInMinutes: time_spent_in_minutes}
      end
      result
    end
  end

  private

  def validate_event_type_values
    errors.add(:comment, "should be an #{Comment}") unless comment.is_a?(Comment)
    validate_object_is_one_of(Workflow, Task, WorkflowDefinition, Dossier, DossierDefinition)
    errors.add(:data, "value for key :mentioned_users should be an array of users") unless mentioned_users.all? { |u| u.is_a?(User) }

    unless time_spent_in_minutes.nil?
      validate_object_is_one_of(Task)
      errors.add(:data, "value for key :time_spent_in_minutes should be an integer") unless time_spent_in_minutes.is_a?(Integer)
      errors.add(:data, "value for key :time_spent_at should be an date") unless time_spent_at.is_a?(Date)
    end
  end

  def at
    comment.deleted? ? comment.deleted_at : super
  end

  def action
    return "comment_deleted" if comment.deleted?
    super
  end

  def avatar(referenced)
    if referenced || comment.deleted?
      {
        icon: "mdi-comment-text-outline"
      }
    else
      {
        label: subject.avatar_label,
        url: subject.avatar_url
      }
    end
  end

  def changed_at
    comment.updated_at if (comment.updated_at > comment.created_at) && !comment.deleted?
  end
end
