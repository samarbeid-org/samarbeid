class Events::AddedContributorEvent < Event
  validate :validate_event_type_values

  def added_contributor
    data&.[](:added_contributor)
  end

  alias_method :new_value, :added_contributor

  def notification_receivers
    return [] if subject.nil?

    ([object.assignee] | [added_contributor]).compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
    errors.add(:data, "value for key :added_contributor should be a user") unless added_contributor.is_a?(User)
  end
end
