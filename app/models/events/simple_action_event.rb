# Use this event to track anything for which we do not want to invest the time necessary to create individual events
# Use as following:
#     Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :promoted_to_admin, notify: :admins)
# will generate something similar to
#     "current_user promoted to admin @user"
# You may add a custom translation for "whats_going_on" in simple_action_events.xx.yml if needed (otherwise it defaults to above)
# You may notify admins, the subject or specific users
# See more examples in test/models/simple_action_event_test.rb
class Events::SimpleActionEvent < Event
  def notify=(whom)
    if whom.is_a?(Array)
      set_specific_users_to_notify(whom)
    else
      set_generic_whom_to_notify(whom)
    end
  end

  def whats_going_on=(something)
    self.data ||= {}
    if something.is_a?(Symbol)
      data[:whats_going_on] = something
    elsif something.is_a?(Hash) && something.keys.size == 1
      data[:whats_going_on] = something.keys.first
      data[:whats_going_on_variables] = something.values.first
    else
      raise "use a symbol or a hash with one key"
    end
  end

  def whats_going_on_translated
    what = data&.[](:whats_going_on)
    what = :default if what.blank?
    variables = data&.[](:whats_going_on_variables) || {}

    variables = variables.transform_values do |thingy|
      if thingy.respond_to?(:reference_label)
        thingy.reference_label
      elsif thingy.nil?
        "nicht mehr verfügbar"
      else
        thingy.to_s
      end
    end
    variables[:default] = what.to_s.humanize(capitalize: false)
    begin
      I18n.translate("simple_action.whats_going_on." + what.to_s, **variables)
    rescue I18n::MissingInterpolationArgument
      I18n.translate("simple_action.whats_going_on." + what.to_s, **variables.slice(:default))
    end
  end
  alias_method :new_value, :whats_going_on_translated

  def notification_receivers
    whom = data&.[](:whom_to_notify)
    return [] unless whom
    return [] unless possible_notification_receivers.has_key?(whom)
    if whom == :specific_users
      (User.where(id: data&.[](:specific_users_ids)) - [subject]).compact
    else
      possible_notification_receivers[whom]
    end
  end

  private

  def possible_notification_receivers
    {admins: User.admins - [subject],
     all: User.all - [subject],
     subject: [subject].compact,
     specific_users: []}
  end

  def set_generic_whom_to_notify(whom)
    raise "Use a valid notifier key such as: #{possible_notification_receivers.keys}" unless possible_notification_receivers.has_key?(whom)
    self.data ||= {}
    data[:whom_to_notify] = whom
  end

  def set_specific_users_to_notify(whom)
    raise "Use an array of users" unless whom.all? { |u| u.is_a?(User) }
    self.data ||= {}
    data[:whom_to_notify] = :specific_users
    data[:specific_users_ids] = whom.map(&:id)
  end
end
