class Events::StartedEvent < Event
  validate :validate_event_type_values

  def via=(val)
    raise "use a automation" unless val.is_a?(Automation)
    self.data ||= {}
    data[:via] = val
  end

  def via
    data&.[](:via)
  end
  alias_method :new_value, :via

  def automatic
    via.nil? ? I18n.t("via.automatic") : nil
  end
  alias_method :old_value, :automatic

  def notification_receivers
    return [] if triggered_by_automation_or_system?
    receivers = [object.assignee].compact
    receivers - [subject]
  end

  private

  def triggered_by_automation_or_system?
    subject.nil?
  end

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
  end
end
