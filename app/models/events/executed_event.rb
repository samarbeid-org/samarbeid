class Events::ExecutedEvent < Event
  validate :validate_event_type_values

  def process=(val)
    raise "use a automation" unless val.is_a?(Workflow)
    self.data ||= {}
    data[:process] = val
  end

  def process
    data&.[](:process)
  end

  alias_method :new_value, :process

  private

  def validate_event_type_values
    validate_object_is_one_of(Automation)
  end
end
