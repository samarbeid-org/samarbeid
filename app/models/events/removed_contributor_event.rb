class Events::RemovedContributorEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def removed_contributor
    data&.[](:removed_contributor)
  end

  alias_method :old_value, :removed_contributor

  def notification_receivers
    ([object.assignee].compact | [removed_contributor]) - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
    errors.add(:data, "value for key :removed_contributor should be a user") unless removed_contributor.is_a?(User)
  end
end
