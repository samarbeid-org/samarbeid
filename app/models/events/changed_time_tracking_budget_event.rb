class Events::ChangedTimeTrackingBudgetEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def new_budget_in_minutes
    data&.[](:new_budget_in_minutes)
  end

  def old_budget_in_minutes
    data&.[](:old_budget_in_minutes)
  end

  alias_method :new_value, :new_budget_in_minutes
  alias_method :old_value, :old_budget_in_minutes

  def notification_receivers
    [object.assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task)
    errors.add(:data, "value for key :new_budget_in_minutes should be a integer") unless new_budget_in_minutes.nil? || new_budget_in_minutes.is_a?(Integer)
    errors.add(:data, "value for key :old_budget_in_minutes should be a integer") unless old_budget_in_minutes.nil? || old_budget_in_minutes.is_a?(Integer)
  end

  def translate_value(value)
    Services::TimeTracking.localized_time(value)
  end
end
