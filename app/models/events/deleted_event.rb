class Events::DeletedEvent < Event
  validate :validate_event_type_values, on: :create
  validates :subject, presence: true

  def notification_receivers
    return [] unless object.class.in?([Workflow, Task])

    receivers = (object&.contributors | [object&.assignee])
    receivers |= [object.workflow.assignee] if object.is_a?(Task)
    receivers |= object.all_tasks.map(&:assignee) if object.is_a?(Workflow)

    receivers.compact - [subject]
  end

  def object
    super || object_type.constantize.new(id: object_id) # return fake object if deleted
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task, Dossier, DossierDefinition, WorkflowDefinition, Group, MainMenuEntry)
  end
end
