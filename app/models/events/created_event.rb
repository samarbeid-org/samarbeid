class Events::CreatedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true, unless: -> { object.is_a?(Task) || object.is_a?(MainMenuEntry) }

  def notification_receivers
    receivers = []

    if object.is_a?(Task) && subject.present? # Manually created task
      receivers = [object.workflow.assignee].compact - [subject]
    end

    receivers
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task, WorkflowDefinition, Dossier, DossierDefinition, User, Group, Automation, MainMenuEntry)
  end
end
