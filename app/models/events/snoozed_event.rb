class Events::SnoozedEvent < Event
  validate :validate_event_type_values

  def snoozed_until
    data&.[](:snoozed_until)
  end

  alias_method :new_value, :snoozed_until

  def notification_receivers
    [object.assignee].compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Task)
    errors.add(:data, "value for key :snoozed_until should be a Date") unless snoozed_until.is_a?(Date)
  end

  def translate_value(value)
    I18n.l(value)
  end
end
