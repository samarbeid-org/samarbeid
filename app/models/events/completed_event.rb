class Events::CompletedEvent < Event
  validate :validate_event_type_values

  validates :subject, presence: true

  def notification_receivers
    receivers = [object.assignee]
    receivers |= [object.workflow.assignee] if object.is_a?(Task)

    receivers.compact - [subject]
  end

  private

  def validate_event_type_values
    validate_object_is_one_of(Workflow, Task)
  end
end
