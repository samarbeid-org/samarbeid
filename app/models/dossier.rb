class Dossier < ApplicationRecord
  include ReferenceSupport
  include SearchForList
  include FilterSupport::DossierFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: {id: "A", title: "B", subtitle: "C"},
    associated_against: {
      definition: {name: "D"}
    },
    using: {
      tsearch: {prefix: true}
    }

  belongs_to :definition, class_name: "DossierDefinition", inverse_of: :dossiers
  has_many :dossier_items, inverse_of: :dossier, dependent: :destroy
  has_many :comments, as: :object, dependent: :destroy
  has_many :data_transformation_items, inverse_of: :dossier, dependent: :destroy

  validates :definition_id, presence: true
  validate :validate_required_items, on: :create
  validate :validate_unchangeable_attributes, on: :update

  scope :with_item_of_definition, ->(dossier_item_definition_id) { includes(:dossier_items).where(dossier_items: {dossier_item_definition_id: dossier_item_definition_id}) }

  scope :with_item_definition, ->(dossier_item_definition, as: nil) {
    dossier_table = Dossier.arel_table
    dossier_items_table = Arel::Table.new(:dossier_items, as: as)

    joins(
      dossier_table.create_join(
        dossier_items_table,
        dossier_table.create_on(dossier_table[:id].eq(dossier_items_table[:dossier_id]))
      )
    ).where(dossier_items_table[:dossier_item_definition_id].eq(dossier_item_definition.id))
  }

  scope :with_item_value_filter, ->(dossier_item_definition, comp_op, *options, filter_index: nil) {
    relation_alias = "dossier_items_#{filter_index}"

    with_item_definition(dossier_item_definition, as: relation_alias)
      .merge(dossier_item_definition.content_type.filter_by(DossierItem.all, relation_alias, comp_op, *options))
  }

  after_save :reindex_workflows
  after_commit :update_references, on: [:destroy]

  include AddToFulltextSearch
  def as_search_document
    {identifier: identifier, title: title, subtitle: subtitle, content: data_fields_as_text}
  end

  def data_fields_as_text
    dossier_items.with_value.map(&:as_search_data).reduce({}, :merge).map { |k, v| "#{k}: #{v}" }.join("  ///  ")
  end

  def get_item(dossier_item_definition)
    dossier_items.find_or_initialize_by(dossier_item_definition: dossier_item_definition)
  end

  def get_item_value(dossier_item_definition)
    get_item(dossier_item_definition).value
  end

  def data_items
    definition.items.map do |item_def|
      get_item(item_def).to_data_item
    end
  end

  def title_from_fields
    item_values_to_text(definition.title_item_definitions, [String(id), definition.name]) if definition
  end

  def subtitle_from_fields
    item_values_to_text(definition.subtitle_item_definitions, []) if definition
  end

  def sync_title_from_fields
    self.title = title_from_fields
    self.subtitle = subtitle_from_fields
    save
    update_references
  end

  def identifier
    "*#{id}"
  end

  def reference_label(noaccess = false)
    identifier + (noaccess ? "" : " (#{definition&.name}) • #{title}")
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  def referenced_in_content_items
    ContentItem.where(content_type: ContentTypes::Dossier.to_s).select { |ci| ci.value&.include?(self) }
  end

  def referenced_in_workflows
    Workflow.where(id: referenced_in_content_items.map(&:workflow_id).uniq)
  end

  def referenced_in_tasks
    Task.where(id: referenced_in_content_items.map { |ci| (ci.first_task_item(true) || ci.first_task_item(false))&.task_id }.uniq)
  end

  def reindex_with_references
    reindex_as_search_document
    reindex_workflows
  end

  def used_in_dossier_items
    DossierItem.with_content_type(ContentTypes::Dossier).with_value_contains(Array(to_global_id))
  end

  def used_in_content_items
    ContentItem.with_content_type(ContentTypes::Dossier).with_value_contains(Array(to_global_id))
  end

  private

  def validate_required_items
    if definition
      existing_dossier_item_definition_ids = dossier_items.map(&:dossier_item_definition_id)
      required_dossier_item_definition_ids = definition.items.required.pluck(:id)

      unless (required_dossier_item_definition_ids - existing_dossier_item_definition_ids).empty?
        errors.add(:dossier_items, "Alle erforderlichen Felder müssen ausgefüllt werden")
      end
    end
  end

  def validate_unchangeable_attributes
    errors.add(:definition, "kann nicht geändert werden") if definition_id_changed?
  end

  def item_values_to_text(dossier_item_definitions, default_values)
    elements = dossier_item_definitions.map { |item_def| item_def.localized_string(get_item_value(item_def)) }.reject(&:empty?)
    elements = default_values if elements.none?

    elements.reject(&:empty?).join(" • ").squish
  end

  def reindex_workflows
    ReindexSearchablesJob.perform_later(self, :referenced_in_workflows)
  end

  def update_references
    SyncTitlesOfDossierReferencesJob.perform_later(id)
  end
end
