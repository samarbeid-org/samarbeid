class Notification < ApplicationRecord
  include NotificationLifecycleSupport
  include FilterSupport::NotificationFilterSupport

  after_commit :broadcast_user_info_changes, on: [:create, :update]
  after_commit :broadcast_notification, on: [:create]

  belongs_to :user
  belongs_to :event

  scope :inbox, -> { undone }

  scope :since, ->(t) { where(created_at: t..) }
  scope :before, ->(t) { where(created_at: ..t) }

  validates :user, :event, presence: true
  validates :event, uniqueness: {scope: :user}

  def type
    event.class.name.demodulize.underscore.sub(/_event\Z/, "")
  end

  # for marking as archived
  scope :old_read, -> { read.where(read_at: ..2.weeks.ago) }
  def old_read?
    read? && read_at < 2.weeks.ago
  end

  # for deleting
  scope :old_done, -> { done.where(done_at: ..2.weeks.ago) }
  def old_done?
    done? && done_at < 2.weeks.ago
  end

  def seen?
    delivered? || read? || done? || bookmarked?
  end

  def self.cleanup
    unbookmarked.old_done.destroy_all
    unbookmarked.undone.old_read.update(done_at: Time.current)
  end

  private

  def broadcast_user_info_changes
    UserChannel.broadcast_info(user)
  end

  def broadcast_notification
    # TODO: send email for this notification if user selected send mail immediately
    NotificationChannel.broadcast_info(user, self)
  end
end
