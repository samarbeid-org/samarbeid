class DossierItem < ApplicationRecord
  include RichtextImageAttachable

  belongs_to :dossier, inverse_of: :dossier_items, touch: true
  has_one :definition, through: :dossier
  belongs_to :dossier_item_definition, inverse_of: :dossier_items
  has_many :data_transformation_items, through: :dossier

  scope :reference_to, ->(dossier) { where("value ?| array[:dossier_gids]", dossier_gids: [dossier.to_global_id.to_s]) }
  scope :no_notes, -> { joins(:dossier_item_definition).where.not(dossier_item_definitions: {content_type: ContentTypes::Note.to_s}) }
  scope :with_value, -> { where.not(value: nil) }
  scope :with_content_type, ->(content_type) { joins(:dossier_item_definition).where(dossier_item_definitions: {content_type: content_type.to_s}) }
  scope :with_value_contains, ->(val) { where("value @> ?", val.to_json) }

  validate :validate_unchangeable_attributes, on: :update
  validate :validate_value, unless: -> { dossier_item_definition.nil? }

  after_update :reindex_dossier
  after_destroy :reindex_dossier
  after_update :destroy_data_transformation_items
  after_destroy :destroy_data_transformation_items

  after_commit :update_dossier_title_and_subtitle, on: [:create, :update]

  delegate :label, :name, :content_type, :content_type_before_type_cast, :localized_string, to: :dossier_item_definition

  def value
    return dossier_item_definition.default_value if dossier_item_definition.content_type == ContentTypes::Note

    dossier_item_definition.content_type.deserialize(read_attribute(:value))
  end

  def value=(new_value)
    casted_new_value = dossier_item_definition.content_type.serialize(dossier_item_definition.content_type.prepare(dossier_item_definition.content_type.cast(new_value), self))
    return if casted_new_value == read_attribute(:value)

    write_attribute(:value, casted_new_value)
  end

  def to_data_item
    data_item = DataItem.new(dossier_item_definition.id, dossier_item_definition.name, value, dossier_item_definition.content_type, dossier_item_definition.options, dossier_item_definition.required)
    data_item.recommended = dossier_item_definition.recommended
    data_item.unique = dossier_item_definition.unique
    data_item.lock_version = lock_version
    data_item.last_updated = last_value_change_event&.created_at
    data_item.last_updated_by = last_value_change_event&.subject

    data_item
  end

  def update_dossier_title_and_subtitle
    title_field_changed = definition.title_fields.include?(dossier_item_definition_id)
    subtitle_field_changed = definition.subtitle_fields.include?(dossier_item_definition_id)
    dossier.sync_title_from_fields if title_field_changed || subtitle_field_changed
  end

  def as_search_data
    case content_type.name
    when ContentTypes::File.name
      value.each_with_index.map { |doc, index| doc.as_search_data(prefix: "#{name} #{index + 1} - ") }.reduce({}, :merge)
    else
      {name => localized_string(value)}
    end
  end

  private

  def last_value_change_event
    Event.where(object: self).order(:created_at).last
  end

  def validate_unchangeable_attributes
    errors.add(:dossier, "kann nicht geändert werden") if dossier_id_changed?
    errors.add(:dossier_item_definition, "kann nicht geändert werden") if dossier_item_definition_id_changed?
  end

  def validate_value
    if dossier_item_definition.required? && dossier_item_definition.content_type.data_empty?(value)
      errors.add(:value, "muss ausgefüllt werden")
    end

    if dossier_item_definition.unique? && !dossier_item_definition.content_type.data_empty?(value) &&
        DossierItem.where.not(id: id).where(dossier_item_definition: dossier_item_definition).where("value @> ?", read_attribute(:value).to_json).exists?
      errors.add(:value, "ist bereits vergeben")
    end

    unless dossier_item_definition.content_type.data_empty?(value)
      validation_result = dossier_item_definition.content_type.validate_data(value, dossier_item_definition.options)
      errors.add(:value, validation_result) if validation_result != true
    end
  end

  def reindex_dossier
    dossier.reindex_with_references
  end

  def destroy_data_transformation_items
    data_transformation_items.each(&:destroy!)
  end
end
