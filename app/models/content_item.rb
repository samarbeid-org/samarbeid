class ContentItem < ApplicationRecord
  include ContentTypeSupport
  include ReferenceSupport
  include LockSupport
  include RichtextImageAttachable

  belongs_to :workflow
  has_one :workflow_definition, through: :workflow
  has_many :task_items, dependent: :destroy
  belongs_to :content_item_definition, optional: true
  has_many :blocks, dependent: :nullify

  scope :no_notes, -> { where.not(content_type: ContentTypes::Note.to_s) }
  scope :with_value, -> { where.not(value: nil) }
  scope :with_content_type, ->(content_type) { where(content_type: content_type.to_s) }
  scope :with_value_contains, ->(val) { where("value @> ?", val.to_json) }

  validates :label, presence: true, unless: -> { content_type.to_s === ContentTypes::Note.to_s }
  validate :validate_value

  after_update :reindex_workflow
  after_destroy :reindex_workflow

  after_commit :update_workflow_title

  include AASM

  aasm whiny_persistence: true do
    state :undefined, initial: true
    state :confirmed

    event :confirm do
      transitions from: :undefined, to: :confirmed, after: -> { update(confirmed_at: Time.now) }
    end

    event :change do
      transitions from: :confirmed, to: :undefined, after: -> { update(confirmed_at: nil) }
    end
  end

  def value
    content_type.deserialize(read_attribute(:value))
  end

  def localized_value
    localized_string(value)
  end

  def value=(new_value)
    casted_new_value = content_type.serialize(content_type.prepare(content_type.cast(new_value), self))
    return if casted_new_value == value

    write_attribute(:value, casted_new_value)
    change if may_change?
  end

  def get_content_type_values
    [value]
  end

  def get_content_type_referenced_values
    blocks.map(&:decision)
  end

  def as_search_data
    case content_type.name
    when ContentTypes::File.name
      value.each_with_index.map { |doc, index| doc.as_search_data(prefix: "#{label} #{index + 1} - ") }.reduce({}, :merge)
    else
      {label => " #{localized_value} "}
    end
  end

  def reference_label(_noaccess = false)
    ""
  end

  def first_task_item(exclude_infoboxes)
    tasks_ordered = workflow.tasks_ordered_in_structure

    result = task_items.includes(:task)
    result = result.reject(&:info_box) if exclude_infoboxes
    result.min_by { |ti| [tasks_ordered.find_index(ti.task), ti.position] }
  end

  def label_with_task
    first_task = task_items.reject(&:info_box).map(&:task).min_by(&:position)
    [label.to_s, "- aus Aufgabe #{first_task&.name}"].join(" ")
  end

  def value_empty?
    content_type.data_empty?(value)
  end

  def update_workflow_title(force = false)
    return unless force || value_previously_changed? || destroyed?
    return unless workflow_definition&.auto_generate_title?
    return if content_item_definition&.title_position.nil?

    workflow.reload.sync_title_from_fields
  end

  private

  def validate_value
    return if value_empty?
    validation_result = content_type.validate_data(value, options)
    errors.add(:value, validation_result) if validation_result != true
  end

  def reindex_workflow
    workflow.reindex_with_tasks
  end
end
