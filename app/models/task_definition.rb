class TaskDefinition < ApplicationRecord
  belongs_to :workflow_definition
  has_many :tasks, dependent: :nullify
  has_many :task_item_definitions, -> { order(:position) }, dependent: :destroy

  belongs_to :definition_workflow_or_block, polymorphic: true, inverse_of: :direct_task_definitions

  belongs_to :candidate_assignee, class_name: "User", optional: true

  has_many :contributions, as: :contributable, dependent: :destroy
  has_many :candidate_contributors, -> { order "contributions.created_at" }, through: :contributions, inverse_of: false, source: :user

  has_many :webhook_definitions, dependent: :destroy

  validates :name, presence: true
  validates :workflow_definition, presence: true
  validates :position, presence: true
  validate :all_belong_to_same_workflow_definition
  validate :candidate_assignee_settings
  validates :deferral_in_days, numericality: {greater_than: 0}, allow_nil: true

  def rails_admin_label
    "#{workflow_definition&.rails_admin_label} > #{id}-#{name&.parameterize}"
  end

  def build_task(workflow_or_block)
    new_task = Task.new(name: name, position: position, task_definition: self)
    new_task.workflow_or_block = workflow_or_block

    new_task.workflow = workflow_or_block if workflow_or_block.is_a?(Workflow)
    new_task.workflow = workflow_or_block.workflow if workflow_or_block.is_a?(Block)

    workflow_content_items = new_task.workflow.content_items
    task_item_definitions.each { |def_task_item| new_task.task_items << def_task_item.build_task_item(new_task, content_items: workflow_content_items) }
    webhook_definitions.each { |def_webhook| new_task.webhooks << def_webhook.build_webhook(new_task) }
    new_task
  end

  def human_model_name
    "#{model_name.human} (#{name}) der #{workflow_definition.human_model_name}"
  end

  private

  def all_belong_to_same_workflow_definition
    return unless definition_workflow_or_block # Is mandatory with existing validation but may be nil

    ref_workflow_id = definition_workflow_or_block.is_a?(WorkflowDefinition) ? definition_workflow_or_block.id : definition_workflow_or_block.workflow_definition.id
    errors.add(:definition_workflow_or_block, "Muss identisch zu workflow_definition '#{workflow_definition&.rails_admin_label}' sein!") unless ref_workflow_id.eql?(workflow_definition_id)

    unless task_item_definitions.all? { |tid| tid.workflow_definition_id.eql?(workflow_definition_id) }
      errors.add(:task_item_definitions, "Muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
  end

  def candidate_assignee_settings
    if candidate_assignee.present? && candidate_assignee_from_workflow
      errors.add(:base, "Entweder Automatischen Verantwortlichen setzen ODER automatisch Prozessverantwortlichen setzen")
    end
  end
end
