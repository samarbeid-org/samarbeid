module Events
  def self.classes
    res = Events.constants.map do |c|
      const_val = Events.const_get(c)
      const_val.is_a?(Class) ? const_val : nil
    end
    res.compact
  end
end
