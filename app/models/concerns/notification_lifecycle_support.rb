require "active_support/concern"

module NotificationLifecycleSupport
  extend ActiveSupport::Concern

  included do
    scope :delivered, -> { where.not(delivered_at: nil) }
    scope :undelivered, -> { where(delivered_at: nil) }

    def delivered?
      !delivered_at.nil?
    end

    def delivered!
      self.delivered_at = Time.current
      save!
    end

    scope :read, -> { where.not(read_at: nil) }
    scope :unread, -> { where(read_at: nil) }

    def read?
      !read_at.nil?
    end

    def read!
      self.read_at = Time.current
      save!
    end

    scope :bookmarked, -> { where.not(bookmarked_at: nil) }
    scope :unbookmarked, -> { where(bookmarked_at: nil) }

    def bookmarked?
      !bookmarked_at.nil?
    end

    def bookmark!
      self.bookmarked_at = Time.current
      save!
    end

    def unbookmark!
      self.bookmarked_at = nil
      save!
    end

    scope :done, -> { where.not(done_at: nil) }
    scope :undone, -> { where(done_at: nil) }

    def done?
      !done_at.nil?
    end

    def done!
      self.done_at = Time.current
      save!
    end

    def undone!
      self.done_at = nil
      save!
    end

    before_update :ensure_lifecycle_changes
    def ensure_lifecycle_changes
      unless read?
        if bookmarked_at_changed? || done_at_changed?
          self.read_at = [Time.current, bookmarked_at, done_at].compact.min
        end
      end
      unless delivered?
        if read_at_changed? || bookmarked_at_changed? || done_at_changed?
          self.delivered_at = [Time.current, read_at, bookmarked_at, done_at].compact.min
        end
      end
      if bookmarked_at_changed? && bookmarked?
        self.done_at = nil
      end
      if bookmarked_at_changed? && !bookmarked?
        if event.obsolete? || old_read?
          self.done_at = Time.current
        end
      end
      if done_at_changed? && bookmarked? && done?
        self.bookmarked_at = nil
      end
    end
  end

  class_methods do
    def read_all
      unread.update_all(read_at: Time.current)
    end

    def deliver_all
      undelivered.update_all(delivered_at: Time.current)
    end
  end
end
