require "active_support/concern"

module LockSupport
  extend ActiveSupport::Concern

  class LockException < StandardError; end

  included do
    belongs_to :locked_by, polymorphic: true, optional: true

    const_set(:LOCK_TIMEOUT_IN_SECONDS, 30)

    def locked?
      return false if locked_at.nil?

      self.class::LOCK_TIMEOUT_IN_SECONDS.seconds.ago < locked_at
    end

    def lock_with!(lock_version, locker)
      raise LockException if locked? && locked_by != locker

      with_lock do
        self.lock_version = lock_version
        update!(locked_at: Time.now, locked_by: locker)
      end
    end

    def unlock_for!(locker)
      return true unless locked?
      raise LockException if locked_by != locker

      with_lock do
        update!(locked_at: nil, locked_by: nil)
      end
    end
  end
end
