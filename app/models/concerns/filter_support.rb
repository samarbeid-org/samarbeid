require "active_support/concern"

module FilterSupport
  extend ActiveSupport::Concern

  class_methods do
    # filter_search_scope [:title, :subtitle]
    # filter_search_scope :name, associated_against: { workflow: :title, workflow_definition: [:name, :description] }
    def filter_search_scope(against, associated_against: {})
      scope :with_filter_search, ->(query) {
        fields_with_parent = Array(against).map { |field| {parent: nil, field: field} } +
          Hash(associated_against).flat_map { |association, fields| Array(fields).map { |field| {parent: association, field: field} } }

        self_with_joins = self
        fields_with_parent.map { |fwp| fwp[:parent] }.compact.uniq.each do |parent|
          self_with_joins = self_with_joins.joins(parent)
        end

        res = self_with_joins
        fields_with_parent.each_with_index do |fwp, index|
          arel_tbl = fwp[:parent].present? ? reflect_on_association(fwp[:parent]).klass.arel_table : arel_table

          res = if index.zero?
            res.where(arel_tbl[fwp[:field]].matches("%#{query}%"))
          else
            res.or(self_with_joins.where(arel_tbl[fwp[:field]].matches("%#{query}%")))
          end
        end

        res
      }
    end

    def standard_order_scopes
      {
        created_at_desc: ->(m) { m.order(created_at: :desc) },
        created_at_asc: ->(m) { m.order(created_at: :asc) }
      }
    end

    def filter_for_index(current_user, query = nil, filter_category: nil, order_by: nil, page: nil, per_page: nil, act_as_normal_user: false, apply_to: all, for_export: false)
      result = OpenStruct.new

      current_user.act_as_normal_user = true if act_as_normal_user

      sql_query = apply_to
      sql_query = sql_query.accessible_by(current_user.ability, :show)

      sql_query = sql_query.with_filter_search(query) if query.present?

      unless for_export
        if sql_query.model.respond_to?(:filter_category_scopes)
          result.filter_category_counts = sql_query.model.filter_category_scopes(current_user).map { |category, scope_lambda| [category, scope_lambda.call(sql_query).uniq.count] }.to_h
        else
          result.total_count = sql_query.uniq.count
        end
      end

      sql_query = filter_category_scopes(current_user)[filter_category].call(sql_query) if filter_category.present?

      if order_by.present? && sql_query.model.respond_to?(:order_scopes)
        sql_query = sql_query.order_scopes[order_by.to_sym].call(sql_query)
      end

      if !for_export && (page.present? || per_page.present?)
        sql_query = sql_query.page(page || 1).per(per_page || 10)
        result.total_pages = sql_query.total_pages
      end

      current_user.act_as_normal_user = nil if act_as_normal_user

      result.query = sql_query
      result
    end
  end
end
