require "active_support/concern"

module ReferenceSupport
  extend ActiveSupport::Concern

  included do
    def reference_label(noaccess = false)
      raise NotImplementedException, "#{self.class} has not implemented method '#{__method__}'"
    end

    def object_type
      self.class.name.demodulize.underscore
    end

    def reference_object(current_user)
      noaccess = current_user.cannot?(:show, self)
      {
        type: "reference",
        object_type: object_type,
        object_id: id,
        noaccess: noaccess,
        deleted: destroyed? || !persisted?,
        text: reference_label(noaccess)
      }
    end
  end
end
