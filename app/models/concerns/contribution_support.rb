require "active_support/concern"

module ContributionSupport
  extend ActiveSupport::Concern

  included do
    private_class_method :add_contributor_if_needed
  end

  class_methods do
    def add_contributor_if_needed(contributable, contributor)
      return if contributable.is_a?(Workflow)
      return unless contributor

      unless contributable.contributors.include?(contributor)
        contributable.contributors << contributor
        Events::AddedContributorEvent.create!(object: contributable, data: {added_contributor: contributor})
      end
    end
  end
end
