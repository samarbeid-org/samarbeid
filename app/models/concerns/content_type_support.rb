require "active_support/concern"

module ContentTypeSupport
  extend ActiveSupport::Concern

  included do
    enum content_type: ContentType.enum_values

    validates_presence_of :content_type
    validate :validate_options, if: :will_save_change_to_options?

    before_validation :cleanup_options, if: :options_changed?
    before_update :on_options_changed, if: :options_changed?

    def content_type_class_for(content_type)
      result = ContentType.types_hash[content_type.to_sym]
      raise "Unsupported content_type '#{content_type}'" if result.nil?
      result
    end

    def content_type_class
      content_type_class_for content_type_before_type_cast
    end

    alias_method :content_type, :content_type_class

    def localized_string(value)
      return "" if value.nil?

      content_type.localized_string(value, content_type.set_options_defaults(options))
    end

    def get_content_type_values
      raise NotImplementedException, "#{self.class} has not implemented method '#{__method__}'"
    end

    def get_content_type_referenced_values
      []
    end

    private

    def validate_options
      if options.is_a?(Hash)
        (get_content_type_values.map { |v| [v, false] } + get_content_type_referenced_values.map { |v| [v, true] }).each do |content_type_value, referenced|
          validation_result = content_type.validate_options(options, content_type_value, referenced)
          unless validation_result === true
            validation_result.each do |error_message|
              errors.add(:options, error_message)
            end
            break
          end
        end
      else
        errors.add(:options, "muss ein Hash sein")
      end
    end

    def on_options_changed
      self.options = content_type.on_options_changed(options_was, options)
    end

    def cleanup_options
      self.options = options.delete_if { |k, v| v.nil? } if options.is_a?(Hash)
    end
  end

  class_methods do
  end
end
