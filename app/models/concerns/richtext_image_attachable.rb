require "active_support/concern"

module RichtextImageAttachable
  extend ActiveSupport::Concern

  included do
    has_many_attached :richtext_images
    validates :richtext_images, size: {less_than: 20.megabytes, message: "Bild muss kleiner als 20 Mb sein!"},
      content_type: ["image/png", "image/jpeg", "image/gif"]
  end
end
