require "active_support/concern"

module SearchForList
  extend ActiveSupport::Concern

  include PgSearch::Model

  class_methods do
    def search_for_list(current_user, query = nil, limited_to_ids: nil, excluded_ids: nil, order: nil, page: nil, per_page: nil, act_as_normal_user: false, apply_to: all)
      current_user.act_as_normal_user = true if act_as_normal_user

      sql_query = apply_to
      sql_query = sql_query.accessible_by(current_user.ability, :show)
      sql_query = sql_query.where(id: limited_to_ids) if limited_to_ids.present?
      sql_query = sql_query.where.not(id: excluded_ids) if excluded_ids.present?
      sql_query = sql_query.search_for_list_with_pg_search(query) if query.present?
      sql_query = sql_query.order(order) if order.present?
      sql_query = sql_query.page(page || 1).per(per_page || 10) if page.present? || per_page.present?

      current_user.act_as_normal_user = nil if act_as_normal_user

      sql_query
    end
  end
end
