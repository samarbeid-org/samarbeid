require "active_support/concern"

module DeactivationSupport
  extend ActiveSupport::Concern

  included do
    scope :activated, -> { where(deactivated_at: nil) }
    scope :deactivated, -> { where.not(deactivated_at: nil) }

    def activated?
      !deactivated_at
    end

    def deactivated?
      !activated?
    end

    def deactivate
      update_attribute(:deactivated_at, Time.current) unless deactivated_at
    end

    def activate
      update_attribute(:deactivated_at, nil) if deactivated_at
    end

    def deactivated_prefix
      deactivated? ? I18n.t("state_support.states.deactivated") : nil
    end

    def prepend_prefixes(val, prefixes)
      prefix = Array(prefixes).compact.join("/")

      if prefix.present?
        return "[#{prefix}] #{val}"
      end

      val
    end
  end

  class_methods do
    def validates_no_prefix(prefix, *attributes)
      validates(*attributes, format: {with: /\A(?!.*#{Regexp.quote(prefix)}).*\z/i, message: I18n.t("state_support.validation_error_msg", prefix: prefix)})
    end

    def validates_no_deactivated_prefix(*attributes)
      validates_no_prefix(I18n.t("state_support.states.deactivated"), *attributes)
    end
  end
end
