require "active_support/concern"

module NotificationSupport
  extend ActiveSupport::Concern

  included do
    enum noti_interval: {immediately: 1, hourly: 1.hour.to_i, daily: 1.day.to_i, weekly: 1.week.to_i, never: 0}

    scope :with_unsent_notifications, -> { joins(:notifications).where("notifications.delivered_at": nil).group(:id) }
    scope :for_sending_notifications_every, ->(interval) { confirmed.activated.where(noti_interval: interval).with_unsent_notifications }

    def unseen_and_unsent_notifications
      notifications.undelivered.order(created_at: :desc)
    end
  end

  class_methods do
    def notification_select_options
      noti_intervals.keys.map { |interval| [interval, notification_interval_translation(interval)] }
    end

    def notification_interval_translation(interval)
      I18n.t("notification.intervals.#{interval}")
    end
  end
end
