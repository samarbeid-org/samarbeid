require "active_support/concern"

##
# This concern is exclusively used for events. It contains scopes, hooks, relations and methods which are added to Event and its subclasses.
#
# Superseding is the feature that specific events become obsolete when a new event is created.
# E.g. if a task is completed, the previous event about the task being started becomes obsolete.
# E.g. if an items is renamed multiple times, all rename-events but the last become obsolete.
# Superseding goes hand in hand with setting notifications as archived.
#
# There are three responsibilities in this concern which play closely together:
# 1. finding events which should be marked as superseded (and thus obsolete)
# 2. make explicit relations between events and their superseded events and allow traversing them
# 3. mark specific notifications as archived when the event they are related to is superseded (and it is ensured that the user will not see them anymore)
#
# The finding algorithm looks at all events, however the explicit relations are only created for direct successors. It is possible to traverse the relations to find all superseded events.
#
# Responsibility 1: Finding
#
# The finding algorithm is implemented in the scope supersedee_candidates which is called by the method with the same name.
# It is triggered by an after_commit hook on creation of a new event.
#
# The finding algorithm consists of (currently) three separate scopes which are unioned together:
# 1. the "general" scope which is used for most event types and primarily observes conceptual relations of event types, e.g. the task states or pairs like adding/removing, assigning/unassigning, etc.
# 2. the "children" scope which is used for events to be obsoleted in different objects, e.g. task-events to become obsolete when a workflow is completed
# 3. the "comments" scope which is used for comment events which have comment objects and only indireclty reference tasks, workflows, etc.
# The latter two share a similar structure and depending supersede rules defined in the future, it might make sense to merge them.
#
# Responsibility 2: Relations
#
# There are two relations: belongs_to :superseder and has_many :supersedees. Those are augmented by the method superseders which goes multiple steps
#
# Responsibility 3: Archiving Notifications
#
# Initially superseding an event archived all (unbookmarked) notifications. However later we discovered some exceptions:
# * when the receiver of a notification did not get a notification for the follow-up event, the notification should not be archived
# * the subject of the new event does not get a notification but should still get their own notifications archived
# * for comments, the notifications about *all* previous CommentedEvents for the same subject should be archived, not only those of the superseded event. Note that this is explicitly only for the subject, not for the other notification receivers.
#   This last point is implemented in apply_supersedees and not in mark_notifications_as_done because it traverses all, even though their events were already superseded before.
#
module SupersedeSupport
  extend ActiveSupport::Concern

  included do
    after_commit :apply_supersedees, on: :create
    def apply_supersedees
      supersedee_candidates.unsuperseded.update(superseder_id: id)
      comment_event_ids = Event.supersede_events_of_comments(self).pluck(:id)
      Notification.where(user: subject, event_id: comment_event_ids).unbookmarked.undone.update(done_at: Time.now)
    end

    after_commit :mark_notifications_as_done, on: :update, if: :saved_change_to_superseder_id?
    def mark_notifications_as_done
      return unless obsolete?
      # initially, we marked all notifications for an obsolete event as done (unless they were bookmarked), but now we only want to mark those as done where the receiver actually received the superseder event (or triggered it)
      receivers_of_superseder_notifications = [superseder.subject_id]
      receivers_of_superseder_notifications |= superseder.notification_receivers.pluck(:id) if superseder.respond_to?(:notification_receivers) && object_type != "Comment"
      notifications_to_archive = notifications.undone.unbookmarked.where(user_id: receivers_of_superseder_notifications)
      notifications_to_archive.update(done_at: Time.now)
    end

    belongs_to :superseder, class_name: "Event", optional: true
    def obsolete?
      superseder_id.present?
    end

    has_many :supersedees, class_name: "Event", foreign_key: :superseder, inverse_of: :superseder

    scope :before_event, ->(event) { where(id: ...event.id) }
    scope :same_object_as, ->(event) { where(object_id: event.object_id, object_type: event.object_type) }
    scope :unsuperseded, -> { where(superseder_id: nil) }

    scope :supersede_constrain_by_value, lambda { |event|
      case event.class.name
      when "Events::AddedContributorEvent"
        where(Arel.sql("data like '%:removed_contributor:%\"/User/#{event.added_contributor.id}\"%'")).or(where(Arel.sql("data like '%:added_contributor:%\"/User/#{event.added_contributor.id}\"%'")))
      when "Events::RemovedContributorEvent"
        where(Arel.sql("data like '%:removed_contributor:%\"/User/#{event.removed_contributor.id}\"%'")).or(where(Arel.sql("data like '%:added_contributor:%\"/User/#{event.removed_contributor.id}\"%'")))
      else
        # by default no restrictions on value. Relevant only for events with two objects like AddedWorkflow
        all
      end
    }

    scope :supersede_candidates_by_type, lambda { |event|
      # Either the when returns a scope directly or it returns a list of classes which is then later used to build the scope.
      # Good ideas how to handle the special case of negation or none better are welcome.

      types = case event.class.name
      when "Events::CreatedEvent", "Events::ReopenedEvent", "Events::StartedEvent"
        ["Events::CompletedEvent", "Events::CreatedEvent", "Events::DeletedEvent", "Events::ReopenedEvent", "Events::SkippedEvent", "Events::StartedEvent"]
      when "Events::CompletedEvent", "Events::SkippedEvent"
        ["Events::CompletedEvent", "Events::CreatedEvent", "Events::DeletedEvent", "Events::ReopenedEvent", "Events::SkippedEvent", "Events::StartedEvent", "Events::ChangedDueDateEvent", "Events::ChangedSummaryEvent", "Events::ChangedTitleEvent", "Events::DueEvent", "Events::AssignedEvent", "Events::AddedContributorEvent", "Events::RemovedContributorEvent", "Events::SnoozedEvent", "Events::UnsnoozedEvent"]
      when "Events::DeletedEvent"
        return where.not(type: ["Events::CommentedEvent"])
      when "Events::AssignedEvent", "Events::UnassignedEvent"
        ["Events::AssignedEvent", "Events::UnassignedEvent"]
      when "Events::AddedContributorEvent", "Events::RemovedContributorEvent"
        ["Events::AddedContributorEvent", "Events::RemovedContributorEvent"]
      when "Events::ChangedDueDateEvent"
        ["Events::ChangedDueDateEvent", "Events::DueEvent"]
      when "Events::DueEvent"
        ["Events::DueEvent"]
      when "Events::ChangedSummaryEvent"
        ["Events::ChangedSummaryEvent"]
      when "Events::ChangedTitleEvent"
        ["Events::ChangedTitleEvent"]
      when "Events::ChangedStructureEventByInstance"
        ["Events::ChangedStructureEventByInstance"]
      when "Events::ChangedStructureEvent"
        ["Events::ChangedStructureEvent"]
      when "Events::SnoozedEvent"
        ["Events::SnoozedEvent", "Events::UnsnoozedEvent", "Events::DueEvent"]
      when "Events::UnsnoozedEvent"
        ["Events::SnoozedEvent", "Events::UnsnoozedEvent"]
      when "Events::ActivatedEvent"
        ["Events::ActivatedEvent", "Events::DeactivatedEvent"]
      when "Events::DeactivatedEvent"
        ["Events::DeactivatedEvent", "Events::ActivatedEvent"]
      else
        return none # by default no event types are superseded, not even same type
      end
      where(type: types)
    }

    scope :supersede_events_general, lambda { |event|
      Event
        .supersede_candidates_by_type(event) # It's important that this check is done first because it depends on the specific sub class
        .before_event(event)
        .same_object_as(event)
        .supersede_constrain_by_value(event)
    }

    scope :supersede_events_of_children, lambda { |event|
      case [event.object_type, event.class.name]
      when ["Workflow", "Events::CompletedEvent"]
        task_ids = event.object.all_tasks.pluck(:id)
        where(object_type: "Task", object_id: task_ids).before_event(event).where(type: ["Events::CompletedEvent", "Events::SkippedEvent", "Events::DeletedEvent"])
      else
        none
      end
    }

    scope :supersede_events_of_comments, lambda { |event|
      case [event.object_type, event.class.name]
      when ["Comment", "Events::CommentedEvent"]
        comment_ids = event.object.comments.pluck(:id)
        where(object_type: "Comment", object_id: comment_ids).before_event(event)
      else
        none
      end
    }

    include SetOperations
    scope :supersedee_candidates, lambda { |event|
      Event.union_scope(
        Event.supersede_events_general(event),
        Event.supersede_events_of_children(event),
        Event.supersede_events_of_comments(event)
      )
    }

    def supersedee_candidates
      self.class.supersedee_candidates(self)
    end

    def superseders
      list = []
      sup = superseder

      # This does potentially a lot of queries
      while sup
        list.push(sup)
        sup = sup.superseder
      end

      list
    end

    def last_superseder
      superseders.last
    end
  end
end
