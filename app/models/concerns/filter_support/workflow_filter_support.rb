require "active_support/concern"

module FilterSupport::WorkflowFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope :title
  end

  class_methods do
    def filter_category_scopes(user)
      {
        "ACTIVE" => ->(m) { m.active },
        "WITH_DUE_TASKS" => ->(m) { m.joins(:all_tasks).active.merge(Task.open.with_due_in_past) },
        "ALL" => ->(m) { m.all }
      }
    end

    def order_scopes
      standard_order_scopes.merge({
        title_desc: ->(m) { m.order(title: :desc) },
        title_asc: ->(m) { m.order(title: :asc) }
      })
    end
  end
end
