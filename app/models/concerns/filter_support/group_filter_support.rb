require "active_support/concern"

module FilterSupport::GroupFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope :name
  end

  class_methods do
    def self.order_scopes
      standard_order_scopes.merge({
        name_desc: ->(m) { m.order(name: :desc) },
        name_asc: ->(m) { m.order(name: :asc) }
      })
    end
  end
end
