require "active_support/concern"

module FilterSupport::MenuEntryFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope :title
  end

  class_methods do
    def order_scopes
      standard_order_scopes.merge({
        title_desc: ->(m) { m.order(title: :desc) },
        title_asc: ->(m) { m.order(title: :asc) },
        location_position: ->(m) { m.order(location_position: :asc) }
      })
    end
  end
end
