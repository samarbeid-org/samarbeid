require "active_support/concern"

module FilterSupport::DossierFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope [:title, :subtitle]
  end

  class_methods do
    def order_scopes
      standard_order_scopes.merge({
        updated_at_desc: ->(m) { m.order(updated_at: :desc) },
        title_desc: ->(m) { m.order(title: :desc) },
        title_asc: ->(m) { m.order(title: :asc) }
      })
    end
  end
end
