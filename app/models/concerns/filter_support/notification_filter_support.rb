require "active_support/concern"

module FilterSupport::NotificationFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  class_methods do
    def filter_category_scopes(user)
      {
        "DONE" => ->(m) { m.done },
        "NEW" => ->(m) { m.inbox },
        "BOOKMARKED" => ->(m) { m.bookmarked }
      }
    end

    def order_scopes
      standard_order_scopes
    end
  end
end
