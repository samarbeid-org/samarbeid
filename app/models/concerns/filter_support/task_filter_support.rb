require "active_support/concern"

module FilterSupport::TaskFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope :name,
      associated_against: {
        workflow: :title
      }
  end

  class_methods do
    def filter_category_scopes(user)
      {
        "ACTIVE" => ->(m) { m.joins(:workflow).active.merge(Workflow.active) },
        "DUE" => ->(m) { m.joins(:workflow).open.with_due_in_past.merge(Workflow.active) },
        "SNOOZED" => ->(m) { m.joins(:workflow).snoozed.merge(Workflow.active) },
        "MARKED" => ->(m) { m.joins(:marked_by_users).where(marked_by_users: {id: user.id}) },
        "ALL" => ->(m) { m.all }
      }
    end

    def order_scopes
      standard_order_scopes.merge({
        title_desc: ->(m) { m.order(title: :desc) },
        title_asc: ->(m) { m.order(title: :asc) },
        by_due_date: ->(m) { m.order(due_at: :asc) }
      })
    end
  end
end
