require "active_support/concern"

module FilterSupport::AutomationFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope :title
  end

  class_methods do
    def order_scopes
      standard_order_scopes.merge({
        title_desc: ->(m) { m.order(title: :desc) },
        title_asc: ->(m) { m.order(title: :asc) }
      })
    end
  end
end
