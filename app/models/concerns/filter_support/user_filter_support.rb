require "active_support/concern"

module FilterSupport::UserFilterSupport
  extend ActiveSupport::Concern
  include FilterSupport

  included do
    filter_search_scope [:firstname, :lastname, :email]
  end

  class_methods do
    def filter_category_scopes(user)
      {
        "ADMIN" => ->(m) { m.admins },
        "NO_ADMIN" => ->(m) { m.not_admins },
        "ALL" => ->(m) { m.all }
      }
    end

    def order_scopes
      standard_order_scopes.merge({
        name_desc: ->(m) { m.order(fullname: :desc) },
        name_asc: ->(m) { m.order(fullname: :asc) }
      })
    end
  end
end
