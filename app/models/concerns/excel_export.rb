require "active_support/concern"

module ExcelExport
  extend ActiveSupport::Concern

  included do
  end

  # Excel file with one page per Definition
  def to_excel(to_file = nil)
    @package = Axlsx::Package.new

    @definitions_and_items.each do |definition, instances|
      add_sheet_to_workbook(definition, instances, @package.workbook)
    end

    if to_file
      @package.serialize(to_file)
    else
      string_io = @package.to_stream
      string_io.string # Return plain string to use in send_data
    end
  end

  def add_sheet_to_workbook(definition, instances, workbook)
    sheet = workbook.add_worksheet(name: valid_excel_sheet_name(definition.name))
    headers = base_columns + definition_columns(definition) + remaining_stuff_headers(definition)
    sheet.add_row headers

    instances.each do |instance|
      data_row = values_for_base_columns(instance)
      data_row += values_for_definition_columns(instance)
      data_row += remaining_stuff(instance)
      excel_row = sheet.add_row
      data_row.each do |value|
        add_to_excel_row(value, excel_row)
      end
    end

    sheet.auto_filter = "A1:#{sheet.cells.last.r}"
  end

  def add_to_excel_row(thingy, row)
    row.add_cell(nil) and return unless thingy
    case thingy.class.name
    when Workflow.name
      cell = row.add_cell thingy.identifier
      row.worksheet.add_hyperlink location: frontend_link_for("workflow", thingy.id), ref: cell
    when Dossier.name
      cell = row.add_cell thingy.identifier
      row.worksheet.add_hyperlink location: frontend_link_for("dossier", thingy.id), ref: cell
    when ContentItem.name
      case thingy.content_type.name
      when ContentTypes::File.name
        cell = row.add_cell thingy.localized_value
        task = thingy.task_items&.first&.task
        row.worksheet.add_hyperlink location: frontend_link_for("task", task.id), ref: cell if task
      when ContentTypes::Url.name
        cell = row.add_cell thingy.localized_value
        row.worksheet.add_hyperlink location: thingy.localized_value, ref: cell
      when ContentTypes::String.name
        row.add_cell thingy.value, type: :string
      when ContentTypes::Text.name
        row.add_cell thingy.value, type: :text
      when ContentTypes::Integer.name
        row.add_cell thingy.value, type: :integer
      when ContentTypes::Decimal.name
        row.add_cell thingy.value, type: :float
      else
        row.add_cell thingy.localized_value, type: :string
      end
    when String.name
      row.add_cell thingy, type: :string
    when Array.name
      if thingy.empty?
        row.add_cell nil
      else
        row.add_cell thingy.join(", "), type: :string
      end
    else
      row.add_cell thingy.inspect, type: :string
    end
  end

  # Array of CSV Strings - one entry per Definition
  def to_csv_s
    @definitions_and_items.map do |definition, instances|
      CSV.generate do |csv|
        csv << base_columns + definition_columns(definition) + remaining_stuff_headers(definition)
        instances.each do |instance|
          row = values_for_base_columns(instance) + values_for_definition_columns(instance) + remaining_stuff(instance).map(&:to_s)
          csv_values = row.map { |thingy| value_for_table(thingy) }
          csv << csv_values
        end
      end
    end
  end

  def value_for_table(thingy)
    return nil unless thingy
    return frontend_link_for("workflow", thingy.id) if thingy.is_a?(Workflow)
    return frontend_link_for("dossier", thingy.id) if thingy.is_a?(Dossier)
    return thingy.localized_value if thingy.respond_to?(:localized_value)
    thingy.inspect
  end

  private

  def remaining_stuff_headers(definition)
    []
  end

  def remaining_stuff(thingy)
    []
  end

  def valid_excel_sheet_name(name)
    only_valid_chars = name.gsub(/[^\w\-.]/, "_")
    valid_name = only_valid_chars.truncate(31)

    count = 1
    until sheet_name_unique?(valid_name)
      valid_name = only_valid_chars.truncate(31, omission: "_#{count}")
      count += 1
    end

    valid_name
  end

  def frontend_link_for(object_type, object_id)
    Object.new.extend(EmailHelper).object_reference_url({object_type: object_type, object_id: object_id})
  end

  def sheet_name_unique?(name)
    return true unless @package
    @package.workbook.worksheets.none? { |sheet| sheet.name == name }
  end
end
