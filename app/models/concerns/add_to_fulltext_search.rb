require "active_support/concern"

module AddToFulltextSearch
  extend ActiveSupport::Concern

  included do
    has_one :search_document, as: :searchable, dependent: :destroy

    after_commit :reindex_as_search_document, on: [:create, :update]

    def search_document
      super || SearchDocument.new(searchable: self)
    end

    def reindex_as_search_document(mode: :async)
      if mode == :sync
        search_document.update!(as_search_document)
      else
        ReindexJob.perform_later(self)
      end
    end

    def as_search_document
      raise "implement in including class"
    end
  end
end
