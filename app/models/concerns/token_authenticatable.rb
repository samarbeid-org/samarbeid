require "active_support/concern"

module TokenAuthenticatable
  extend ActiveSupport::Concern

  module ClassMethods
    # This is vulnerable to timing-attacks. Some day we might switch to
    # find_by user_id + secure compare for encrypted token
    # but before that we should secure brute-forcing standard login (rack-attack et al)
    def find_and_authenticate_by_token(column, api_token = nil)
      return nil unless api_token.present?

      authenticatable = where(column => api_token).first

      return nil if authenticatable.respond_to?(:token_auth_disabled?) && authenticatable.token_auth_disabled?

      authenticatable
    end
  end

  def create_token(column)
    regenerate_token(column)
    save
  end

  def delete_token(column)
    send(:"#{column}=", nil)
    save
  end

  def regenerate_token(column)
    send(:"#{column}=", generate_token(column))
  end

  private

  def generate_token(column)
    loop do
      token = Devise.friendly_token(24)
      break token unless self.class.unscoped.where(column => token).first
    end
  end
end
