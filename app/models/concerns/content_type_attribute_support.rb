require "active_support/concern"

module ContentTypeAttributeSupport
  extend ActiveSupport::Concern
  include RichtextImageAttachable

  class_methods do
    def content_type_attribute(name, content_type)
      define_method(:"#{name}=") do |new_value|
        casted_new_value = content_type.serialize(content_type.prepare(content_type.cast(new_value), self))
        return if casted_new_value == read_attribute(name)

        write_attribute(name, casted_new_value)
      end

      define_method(name) do
        content_type.deserialize(read_attribute(name))
      end
    end
  end
end
