class Comment < ApplicationRecord
  include RichtextImageAttachable

  belongs_to :author, class_name: "User"
  belongs_to :object, polymorphic: true

  validates_presence_of :message, if: proc { |comment| !comment.deleted? }

  after_save :reindex_object

  scope :not_deleted, -> { where(deleted_at: nil) }

  def deleted?
    deleted_at.present?
  end

  private

  def reindex_object
    object.reindex_as_search_document if object.respond_to?(:reindex_as_search_document)
  end
end
