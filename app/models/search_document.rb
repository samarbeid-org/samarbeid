class SearchDocument < ApplicationRecord
  belongs_to :searchable, polymorphic: true

  validates :identifier, presence: true, uniqueness: true # presence for now but not enforced in DB as it may be dropped in the future
  validates :title, presence: true

  scope :visible_to, ->(user) do
    where(searchable: Task.accessible_by(user.ability))
      .or(where(searchable: Workflow.accessible_by(user.ability)))
      .or(where(searchable: Dossier.accessible_by(user.ability)))
  end

  include PgSearch::Model

  pg_search_scope :fulltext,
    against: [
      [:identifier, "A"],
      [:title, "A"],
      [:subtitle, "B"],
      [:content, "C"]
    ],
    using: {
      tsearch: {
        highlight: {
          StartSel: "<mark>",
          StopSel: "</mark>",
          MaxWords: 50,
          MaxFragments: 3,
          FragmentDelimiter: "<br>...<br>"
        },
        any_word: true,
        dictionary: "german"
      }
    }

  def self.reindex_all
    [Task, Workflow, Dossier].each do |klass|
      klass.find_each do |record|
        record.reindex_as_search_document(mode: :sync)
      end
    end
  end
end
