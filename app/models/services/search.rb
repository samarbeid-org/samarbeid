class Services::Search
  attr_accessor :user
  def initialize(user = nil)
    @user = user
  end

  def fulltext(query = nil, additional_options = {})
    results = SearchDocument.all
    results = results.fulltext(query) if query
    results = results.visible_to(user) if user
    results = results.with_pg_search_highlight if query # Highlight search results
    results.page(additional_options.dig(:page) || 1).per(additional_options.dig(:per_page) || 10) # kaminari pagination
  end
end
