class Services::WorkflowToDefinition
  attr_accessor :workflow
  def initialize(workflow)
    raise "Must be initialized with a workflow instead of: #{workflow.inspect}" unless workflow.is_a?(Workflow)
    @workflow = workflow
  end

  def create(params)
    new_workflow_definition = nil

    ActiveRecord::Base.transaction do
      new_workflow_definition = WorkflowDefinition.create!(params)
      @workflow.update!(workflow_definition: new_workflow_definition)
      update_or_create_from_content_items!(true)
      update_or_create_from_blocks!(true)
      update_or_create_from_tasks!(true)
      update_or_create_from_task_items!(true)
    end
    new_workflow_definition
  rescue ActiveRecord::RecordInvalid => invalid
    if new_workflow_definition.nil?
      new_workflow_definition = invalid.record
    else
      new_workflow_definition.errors.add(:base, "Fehler beim Übertragen der Struktur des Prozesses in die neue Vorlage.")
      invalid.record&.errors&.full_messages&.each do |msg|
        new_workflow_definition.errors.add(:base, msg)
      end
    end
    new_workflow_definition
  end

  def update!
    ActiveRecord::Base.transaction do
      update_or_create_from_content_items!
      update_or_create_from_blocks!
      update_or_create_from_tasks!
      update_or_create_from_task_items!
      @workflow.workflow_definition.increment!(:version)
    end
  end

  def update
    update!
  rescue ActiveRecord::RecordInvalid => invalid
    case invalid.record
    when ContentItemDefinition, BlockDefinition, TaskDefinition, TaskItemDefinition, WorkflowDefinition
      error = "#{invalid.record.human_model_name} hat folgende Fehler: #{invalid.record.errors.full_messages.join(". ")}"
      @workflow.workflow_definition.errors.add(:base, error)
    else
      raise invalid
    end
  end

  def update_or_create_from_content_items!(cpy_to_new_workflow_def = false)
    ci_definitions_not_to_delete = []
    @workflow.content_items.each do |ci|
      if !cpy_to_new_workflow_def && ci.content_item_definition
        ci.content_item_definition.update!(label: ci.label, options: ci.options, workflow_definition: @workflow.workflow_definition)
        ci.content_item_definition.update!(default_value: ci.value) if ci.content_type == ContentTypes::Note
        ci_definitions_not_to_delete << ci.content_item_definition
      else
        new_definition = ContentItemDefinition.create!(label: ci.label, content_type: ci.content_type.to_s, options: ci.options,
          workflow_definition: @workflow.workflow_definition)
        new_definition.update!(default_value: ci.value) if ci.content_type == ContentTypes::Note
        ci.update!(content_item_definition: new_definition)
        ci_definitions_not_to_delete << new_definition
      end
    end

    unless cpy_to_new_workflow_def
      content_item_defs_to_delete = @workflow.workflow_definition.content_item_definitions.reject { |definition| definition.in?(ci_definitions_not_to_delete) }
      content_item_defs_to_delete.each(&:destroy!)
    end
  end

  def update_or_create_from_blocks!(cpy_to_new_workflow_def = false)
    block_defs_not_to_delete = []
    @workflow.blocks.each do |block|
      new_content_item_definition = block.content_item&.reload&.content_item_definition

      if !cpy_to_new_workflow_def && block.block_definition
        block.block_definition.update!(title: block.title, position: block.position, parallel: block.parallel,
          decision: block.decision, content_item_definition: new_content_item_definition)
        block_defs_not_to_delete << block.block_definition
      else
        new_block_definition = BlockDefinition.create!(title: block.title, position: block.position, parallel: block.parallel,
          decision: block.decision, content_item_definition: new_content_item_definition,
          workflow_definition: @workflow.workflow_definition)
        block.update!(block_definition: new_block_definition)
        block_defs_not_to_delete << new_block_definition
      end
    end

    unless cpy_to_new_workflow_def
      block_defs_to_delete = @workflow.workflow_definition.block_definitions.reject { |definition| definition.in?(block_defs_not_to_delete) }
      block_defs_to_delete.each { |definition| definition.direct_task_definitions.each { |task_def| task_def.update!(definition_workflow_or_block: @workflow.workflow_definition) } }
      block_defs_to_delete.each(&:destroy!)
    end
  end

  def update_or_create_from_tasks!(cpy_to_new_workflow_def = false)
    task_defs_not_to_delete = []
    @workflow.all_tasks.each do |task|
      if !cpy_to_new_workflow_def && task.task_definition
        task.task_definition.update!(name: task.name, position: task.position,
          definition_workflow_or_block: definition_for(task.workflow_or_block))
        task_defs_not_to_delete << task.task_definition
      else
        new_task_definition = TaskDefinition.create!(name: task.name, position: task.position,
          definition_workflow_or_block: definition_for(task.workflow_or_block), workflow_definition: @workflow.workflow_definition)
        task.update!(task_definition: new_task_definition)
        task_defs_not_to_delete << new_task_definition
      end
    end

    unless cpy_to_new_workflow_def
      task_defs_to_delete = @workflow.workflow_definition.all_task_definitions.reject { |definition| definition.in?(task_defs_not_to_delete) }
      task_defs_to_delete.each(&:destroy!)
    end
  end

  def update_or_create_from_task_items!(cpy_to_new_workflow_def = false)
    task_item_defs_not_to_delete = @workflow.all_tasks.flat_map do |task|
      update_or_create_task_items_for!(task, cpy_to_new_workflow_def)
    end

    unless cpy_to_new_workflow_def
      task_item_defs_to_delete = @workflow.workflow_definition.all_task_definitions.flat_map(&:task_item_definitions).reject { |definition| definition.in?(task_item_defs_not_to_delete) }
      task_item_defs_to_delete.each(&:destroy)
    end
  end

  def update_or_create_task_items_for!(task, cpy_to_new_workflow_def = false)
    task_item_defs_not_to_delete = []
    task.task_items.each do |task_item|
      if !cpy_to_new_workflow_def && task_item.task_item_definition
        task_item.task_item_definition.update!(position: task_item.position, required: task_item.required, info_box: task_item.info_box, add_to_calendar: task_item.add_to_calendar)
        task_item_defs_not_to_delete << task_item.task_item_definition
      else
        content_item_definition = @workflow.workflow_definition.content_item_definitions.reload.find do |content_item_definition|
          task_item.content_item.content_item_definition == content_item_definition
        end
        new_task_item_definition = TaskItemDefinition.create!(position: task_item.position, required: task_item.required,
          info_box: task_item.info_box, add_to_calendar: task_item.add_to_calendar,
          task_definition: task.task_definition, workflow_definition: @workflow.workflow_definition,
          content_item_definition: content_item_definition)
        task_item.update!(task_item_definition: new_task_item_definition)
        task_item_defs_not_to_delete << new_task_item_definition
      end
    end
    task_item_defs_not_to_delete
  end

  def definition_for(instance)
    return instance.workflow_definition if instance.is_a?(Workflow)
    return instance.block_definition if instance.is_a?(Block)
    instance.task_definition if instance.is_a?(Task)
  end
end
