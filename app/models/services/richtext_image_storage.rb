require "mini_magick"
class Services::RichtextImageStorage
  REGEX_BLOBS = %r{\A/rails/active_storage/blobs/redirect/(.*)/.*\z}
  REGEX_REPRESENTATIONS = %r{\A/rails/active_storage/representations/redirect/(.*)/.*/.*\z}

  def self.persist_inline_images(html_with_inline_images, has_many_attached_association)
    attach_reused_images(html_with_inline_images, has_many_attached_association)

    iterate_html_elements(html_with_inline_images, "img[src^='data:image/']") do |element|
      uploaded_file = base64_image_src_to_file(element["src"])
      if uploaded_file
        image_blob = ActiveStorage::Blob.create_and_upload!(io: uploaded_file, filename: uploaded_file.original_filename)

        has_many_attached_association.attach(image_blob)

        has_many_attached_association.record.valid?
        if has_many_attached_association.record.errors[has_many_attached_association.name].any? # in case the image size exceeds limits given in RichtextImageAttachable concern
          image_blob.purge
          element["src"] = ""
          next
        end

        element["src"] = if MiniMagick::Image.read(image_blob.download).identify { |b| b.format("%n") } == "1"
          Rails.application.routes.url_helpers.rails_representation_url(image_blob.variant(resize_to_limit: [1000, nil]), only_path: true) # normal image
        else
          Rails.application.routes.url_helpers.rails_blob_path(image_blob, only_path: true) # animated gif
        end

        element["data-high-res-src"] = Rails.application.routes.url_helpers.rails_blob_path(image_blob, only_path: true)

        # makes very big files for animated gifs
        # Rails.application.routes.url_helpers.rails_representation_url(image_blob.variant(loader: {page: nil}, coalesce: true, resize_to_limit: [1000, nil]), only_path: true) # normal image
      end
    end
  end

  def self.purge_unused_images(html_with_image_tags, has_many_attached_association)
    used_blob_ids = []

    iterate_active_storage_img_elements(html_with_image_tags) do |element|
      signed_blob_id = signed_blob_id_from_url(element["src"])
      next unless signed_blob_id

      blob = ActiveStorage::Blob.find_signed(signed_blob_id)
      used_blob_ids << blob.id unless blob.nil?
    end

    has_many_attached_association.reject { |a| a.blob_id.in?(used_blob_ids) || !a.persisted? }.each(&:purge_later)
  end

  def self.transform_image_urls(html_with_image_tags, &block)
    iterate_active_storage_img_elements(html_with_image_tags) do |element|
      element["src"] = block.call(element["src"])
    end
  end

  def self.attach_reused_images(html_with_image_tags, has_many_attached_association)
    used_blobs = []

    iterate_active_storage_img_elements(html_with_image_tags) do |element|
      signed_blob_id = signed_blob_id_from_url(element["src"])
      next unless signed_blob_id

      blob = ActiveStorage::Blob.find_signed(signed_blob_id)
      used_blobs << blob unless blob.nil?
    end

    blobs_to_attach = used_blobs.difference(has_many_attached_association.map { |assoc| assoc.blob })
    blobs_to_attach.each { |blob| has_many_attached_association.attach(blob) } if blobs_to_attach.any?
  end

  def self.base64_image_src_to_file(data)
    return unless data.is_a? String

    regex = %r{\Adata:image/(\S*);base64,(.*)\z}
    type, image = data.scan(regex)[0]
    return unless type && image

    filename = "#{SecureRandom.hex}.#{type}"
    tempfile = Tempfile.new(filename)
    tempfile.binmode
    tempfile.write(Base64.decode64(image))

    uf = ActionDispatch::Http::UploadedFile.new(
      tempfile: tempfile,
      filename: filename,
      original_filename: filename
    )
    uf.rewind
    uf
  end

  def self.iterate_active_storage_img_elements(html, &)
    iterate_html_elements(html, "img[src^='/rails/active_storage/blobs/redirect/']", "img[src^='/rails/active_storage/representations/redirect/']", &)
  end

  def self.iterate_html_elements(html, *css_selectors, &)
    html_fragment = Nokogiri::HTML5.fragment(html)
    html_fragment.css(*css_selectors).each(&)

    html_fragment.to_html
  end

  def self.signed_blob_id_from_url(url)
    url.scan(REGEX_BLOBS)[0]&.[](0) || url.scan(REGEX_REPRESENTATIONS)[0]&.[](0)
  end

  private_class_method [:attach_reused_images, :base64_image_src_to_file, :iterate_html_elements, :iterate_active_storage_img_elements]
end
