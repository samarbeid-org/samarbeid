class Services::HealthCheck < ApplicationRecord
  # Only return overall status - do not leak more information (versions etc.)
  def self.all
    {
      database: ActiveRecord::Base.connection.present?,
      file_storage: File.writable?(Rails.root.join("storage"))
    }.to_json
  end
end
