class Services::Automations < ApplicationRecord
  def self.execute_active_and_due_automations
    Automation.active_and_due_today.find_each do |automation|
      automation.execute
      automation.save! # save to update next_execution date
    end
  end
end
