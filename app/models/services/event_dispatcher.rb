class Services::EventDispatcher
  def self.process!(event)
    if event.respond_to?(:notification_receivers)
      (event.notification_receivers - [event.triggered_by]).uniq.each do |receiver|
        unless Notification.exists?(user: receiver, event: event) || receiver.cannot?(:read, event.object)
          Notification.create!(user: receiver, event: event)
        end
      end
    end
  end
end
