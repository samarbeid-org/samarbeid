class Services::NextInterestingThing
  attr_accessor :workflow, :user
  def initialize(workflow, user)
    @workflow = workflow
    @user = user
  end

  def next_station
    # wenn ein Prozess durch das Abschließen einer Aufgabe abgeschlosse wird, dann sollte es keine Weiterleitung auf irgendeine andere Aufgabe geben
    return if workflow.completed?

    # die Liste aller Aufgaben wird von oben nach unten durchgegangen und die Aufgabe wird "nächste interessante Station" wenn:
    # aktiv und Nutzer ist Verantwortlich
    # zurückgestellt und Nutzer ist Verantwortlich
    # aktiv und hat keinen Verantwortlichen
    # zurückgestellt und hat keinen Verantwortlichen
    workflow.tasks_ordered_in_structure.each do |task|
      return task if task.aasm_state.in?(["active", "snoozed"]) && task.assignee.in?([user, nil])
    end

    # wenn es keine dieser Aufgaben gibt, wird die oberste aktive oder zurückgestellte Aufgabe die einen anderen Verantwortlichen hat
    workflow.tasks_ordered_in_structure.each do |task|
      return task if task.aasm_state.in?(["active", "snoozed"])
    end

    # wenn es auch eine solche Aufgabe nicht gibt, default oberste erstellte Aufgabe
    tasks_with_state("created").first
  end

  def tasks_with_state(state)
    workflow.tasks_ordered_in_structure.select do |task|
      task.send(:"#{state}?")
    end
  end
end
