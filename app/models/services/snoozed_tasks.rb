class Services::SnoozedTasks < ApplicationRecord
  def self.find_and_start
    find_unprocessed.find_each do |due_snoozed_task|
      if due_snoozed_task.start!
        Events::UnsnoozedEvent.create!(object: due_snoozed_task)
        due_snoozed_task.update!(start_at: nil)
      end
    end
  end

  def self.find_unprocessed
    Task.snoozed.workflow_open.with_start_at_in_past
  end
end
