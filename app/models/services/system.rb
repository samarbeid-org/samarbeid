class Services::System
  def self.revision
    revision_file = Rails.root.join("REVISION")
    File.exist?(revision_file) ? File.read(revision_file).squish : nil
  end

  def self.revision_text
    revision_human_info_file = Rails.root.join("REVISION_TEXT")
    File.exist?(revision_human_info_file) ? File.read(revision_human_info_file) : nil
  end
end
