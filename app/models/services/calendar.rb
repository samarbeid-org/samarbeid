class Services::Calendar
  def initialize(user)
    @user = user
  end

  def relevant_due_tasks
    # Assignee is always also set as contributor)
    @user.contributed_tasks.accessible_by(@user.ability).where.not(due_at: nil).where(due_at: valid_event_timerange)
  end

  def relevant_data_fields
    TaskItem.where(add_to_calendar: true, task: @user.contributed_tasks.accessible_by(@user.ability))
      .reject { |task_item| !task_item.content_item.content_type.calendar_addable? || task_item.content_item.value_empty? }
      .select { |task_item| valid_event_timerange.cover?(task_item.content_item.value) }
  end

  def to_ical
    cal = Icalendar::Calendar.new
    cal.append_custom_property("X-WR-CALNAME", "samarbeid")
    cal.append_custom_property("REFRESH-INTERVAL;VALUE=DURATION", "P1H")

    relevant_due_tasks.find_each do |due_task|
      cal.add_event(new_due_task_event(due_task))
    end

    relevant_data_fields.each do |task_item|
      cal.add_event(new_calendar_item_event(task_item))
    end

    cal.events.each { |event| event.summary = event.summary.truncate(200) } # 255 chars is outlook event title limit but emoji / unicode seem to be longer thus we play it safe

    cal.to_ical
  end

  def new_due_task_event(task)
    event = Icalendar::Event.new
    event.dtstart = Icalendar::Values::Date.new(task.due_at)
    event.summary = task_title_calendar_style(task)
    event.description = event_description_task(task)
    event.url = task_url(task)
    event.uid = uuid_for(task)
    event
  end

  def new_calendar_item_event(task_item)
    content_item = task_item.content_item
    task = task_item.task
    event = Icalendar::Event.new

    case content_item.content_type.type
    when ContentTypes::Date.type
      event.dtstart = Icalendar::Values::Date.new(content_item.value)
    when ContentTypes::Datetime.type
      start = Time.zone.parse(content_item.value).utc
      event.dtstart = Icalendar::Values::DateTime.new(start, "tzid" => "UTC")
      event.dtend = Icalendar::Values::DateTime.new(start + 30.minutes, "tzid" => "UTC")
    else
      event.dtstart = Icalendar::Values::Date.new(Date.current)
    end

    event.summary = task_title_calendar_style(task) + " - #{content_item.label}"
    event.description = event_description_task(task) + "\nDatenfeld: #{content_item.label}"
    event.url = task_url(task)
    event.uid = uuid_for(task_item)
    event
  end

  def task_title_calendar_style(task)
    workflow = task.workflow
    workflow_def = workflow.workflow_definition
    summary = ""

    summary << "[#{I18n.t("task_state_symbol." + task.aasm_state)}] " if task.aasm_state.in?(["completed", "skipped"])
    summary << task.title.to_s
    if workflow_def.system_process_definition_single_task?
      summary << " • #{workflow.title_from_db}" if workflow.title_from_db.present?
    else
      summary << " • #{workflow.title} (#{workflow_def.name})"
    end
    summary
  end

  def event_description_task(task)
    "Link zur Aufgabe: #{task_url(task)}\nVerantwortlich: #{task.assignee&.name}\nStatus: #{I18n.t("task_state." + task.aasm_state)}"
  end

  def task_url(task)
    Rails.application.config.action_mailer.default_url_options[:host] + "/tasks/#{task.to_param}"
  end

  def uuid_for(object)
    Digest::UUID.uuid_v5(Digest::UUID::DNS_NAMESPACE, object.to_global_id.to_s)
  end

  def valid_event_timerange
    (12.months.ago..)
  end
end
