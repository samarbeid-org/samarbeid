class Services::ClonedTask
  attr_accessor :original_task
  def initialize(task)
    @original_task = task
  end

  def create
    cloned_task = build_clone
    move_all_existing_tasks_one_down
    cloned_task.save
    cloned_task
  end

  def build_clone
    # Maybe some day it is more elegant to do this by building a definition from given task and to build a task from
    # that definition (needed for modified stuff + tasks without definition)

    cloned_task = Task.new(name: "#{@original_task.name} (#{next_free_name_counter})", position: next_position,
      workflow_or_block: @original_task.workflow_or_block)

    cloned_task.workflow = @original_task.workflow_or_block if @original_task.workflow_or_block.is_a?(Workflow)
    cloned_task.workflow = @original_task.workflow_or_block.workflow if @original_task.workflow_or_block.is_a?(Block)

    cloned_task.update(due_at: nil, assignee: nil, contributors: [])

    cloned_task.task_items = build_task_item_clones

    cloned_task
  end

  def build_task_item_clones
    @original_task.task_items.map do |o_ti|
      new_content_item = o_ti.info_box ? o_ti.content_item : build_content_item_clone(o_ti.content_item)

      TaskItem.new(position: o_ti.position, info_box: o_ti.info_box, required: o_ti.required, add_to_calendar: o_ti.add_to_calendar,
        content_item: new_content_item)
    end.to_a
  end

  def build_content_item_clone(content_item)
    new_value = (content_item.content_type == ContentTypes::Note) ? content_item.value : nil

    ContentItem.new(label: content_item.label,
      content_type: content_item.content_type.to_s, options: content_item.options, value: new_value,
      workflow: @original_task.workflow)
  end

  def all_clones_and_original_task
    @original_task.workflow.all_tasks.select { |task| task.name.include?(@original_task.name) }
  end

  def next_free_name_counter
    all_clones_and_original_task.count
  end

  def next_position
    all_clones_and_original_task.map(&:position).max.to_i + 1
  end

  def move_all_existing_tasks_one_down
    items_behind_original = @original_task.workflow_or_block.items.select { |item| item.position >= next_position }
    items_behind_original.each { |item| item.increment(:position) }
  end
end
