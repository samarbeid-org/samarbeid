class Services::WorkflowsToTable
  include ExcelExport

  def initialize(workflows = [])
    @definitions_and_items = workflows.group_by(&:workflow_definition).sort_by { |wd, _| wd.name }
  end

  def base_columns
    ["Prozess-ID", "Prozesstitel", "Status", "Verantwortlich", "Teilnehmend", "Erstellungsdatum"]
  end

  def definition_columns(workflow_definition)
    content_item_definitions_sorted(workflow_definition).map do |cid|
      if content_item_definitions_sorted(workflow_definition).count { |other| other.label == cid.label } > 1
        cid.label_with_task
      else
        cid.label
      end
    end
  end

  def values_for_base_columns(workflow)
    [workflow, workflow.title, I18n.t("workflow_state." + workflow.aasm_state), workflow.assignee&.email, workflow.contributors.map(&:email),
      workflow.created_at.strftime("%Y-%m-%d %H:%M")]
  end

  def values_for_definition_columns(workflow)
    values_for_content_item_definitions_in(workflow)
  end

  def remaining_stuff_headers(definition)
    ["Datenfelder die nicht Teil der Vorlage sind"] + definition.task_definitions_ordered_in_structure.map do |task_def|
      header = ""
      header << "#{task_def.definition_workflow_or_block.title}: " if task_def.definition_workflow_or_block.is_a?(BlockDefinition)
      header << task_def.name
      header
    end
  end

  def remaining_stuff(workflow)
    [data_fields_without_definition(workflow)] + task_states(workflow)
  end

  def task_states(workflow)
    definition_tasks = workflow.workflow_definition.task_definitions_ordered_in_structure
    definition_tasks.map do |task_definition|
      task = workflow.all_tasks.find_by(task_definition: task_definition)
      task ? I18n.t("task_state." + task&.aasm_state) : ""
    end
  end

  def data_fields_without_definition(workflow)
    non_standard_content_items = workflow.content_items.no_notes.order(:label)
      .reject { |ci| content_item_definitions_sorted(workflow.workflow_definition).include?(ci.content_item_definition) }
    non_standard_content_items.reject { |ci| ci.value_empty? }.map(&:as_search_data).join("\n")
  end

  def values_for_content_item_definitions_in(workflow)
    content_item_definitions_sorted(workflow.workflow_definition).map do |definition|
      workflow.content_items.find { |ci| ci.content_item_definition.eql?(definition) }
    end
  end

  def content_item_definitions_sorted(workflow_definition)
    workflow_definition.content_item_definitions.where.not(content_type: ContentTypes::Note.to_s).order(:label)
  end
end
