class Services::MoveTask
  attr_accessor :task, :origin_workflow
  def initialize(task)
    @task = task
    @origin_workflow = @task.workflow
  end

  def valid?
    errors.none?
  end

  def errors
    content_items_used_elsewhere = @task.task_items.map(&:content_item).select do |content_item|
      content_item.task_items.any? { |task_item| !task_item.task.eql?(@task) }
    end
    content_items_used_elsewhere.map { |ci| "Feld '#{ci.label}' wird noch in weiteren Aufgaben benutzt." }
  end

  def move_to!(target_workflow)
    @target_workflow = target_workflow

    raise "Not valid" unless valid?

    migrate_content_items
    migrate_task_items
    migrate_task
    transition_states_and_reindex
    delete_origin_workflow_if_empty
  end

  def migrate_content_items
    @task.task_items.map(&:content_item).each do |content_item|
      content_item.update!(workflow: @target_workflow, content_item_definition: nil)
    end
  end

  def migrate_task_items
    @task.task_items.each do |task_item|
      task_item.update!(task_item_definition: nil)
    end
  end

  def migrate_task
    @task.update!(workflow: @target_workflow, workflow_or_block: @target_workflow,
      task_definition: nil,
      position: @target_workflow.items.map(&:position).max.to_i + 1)
  end

  def transition_states_and_reindex
    [@target_workflow, @origin_workflow].each do |workflow|
      Services::NewEngine.new(workflow).run
      workflow.reindex_with_tasks
    end
  end

  def delete_origin_workflow_if_empty
    @origin_workflow.destroy if @origin_workflow.reload.all_tasks.empty?
  end
end
