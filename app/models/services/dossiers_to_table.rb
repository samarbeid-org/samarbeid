class Services::DossiersToTable
  include ExcelExport

  def initialize(dossiers = [])
    @definitions_and_items = dossiers.group_by(&:definition).sort_by { |d, _| d.name }
  end

  def base_columns
    ["Dossier-ID", "Dossiertitel", "Erstellungsdatum"]
  end

  def definition_columns(dossier_definition)
    item_definitions_sorted(dossier_definition).map(&:name)
  end

  def values_for_base_columns(dossier)
    [dossier, dossier.title, dossier.created_at.strftime("%Y-%m-%d %H:%M")]
  end

  def values_for_definition_columns(workflow)
    values_for_content_item_definitions_in(workflow)
  end

  def values_for_content_item_definitions_in(dossier)
    item_definitions_sorted(dossier.definition).map do |item_def|
      ContentItem.new(label: item_def.name,
        content_type: item_def.content_type.to_s.to_sym, options: item_def.options,
        value: dossier.get_item_value(item_def))
    end
  end

  def item_definitions_sorted(definition)
    definition.items.no_notes.sort_by(&:position)
  end
end
