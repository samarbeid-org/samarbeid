class Services::ClonedTaskDefinition
  attr_accessor :original_task_definition
  def initialize(task_definition)
    @original_task_definition = task_definition
  end

  def create
    move_all_existing_task_definitions_one_down # we need to move them down before cloning!
    cloned_task_definition = build_clone
    cloned_task_definition.save
    cloned_task_definition
  end

  def build_clone
    cloned_task_definition = TaskDefinition.new(
      name: "#{@original_task_definition.name} (#{next_free_name_counter})",
      candidate_assignee: @original_task_definition.candidate_assignee,
      candidate_contributors: @original_task_definition.candidate_contributors,
      due_in_days: @original_task_definition.due_in_days,
      deferral_in_days: @original_task_definition.deferral_in_days,
      workflow_definition: @original_task_definition.workflow_definition,
      definition_workflow_or_block: @original_task_definition.definition_workflow_or_block,
      position: next_position
    )

    cloned_task_definition.task_item_definitions = build_task_item_definition_clones

    cloned_task_definition
  end

  def build_task_item_definition_clones
    @original_task_definition.task_item_definitions.map do |o_ti_def|
      if o_ti_def.info_box
        TaskItemDefinition.new(position: o_ti_def.position, info_box: o_ti_def.info_box, required: o_ti_def.required, add_to_calendar: o_ti_def.add_to_calendar,
          workflow_definition: o_ti_def.workflow_definition, content_item_definition: o_ti_def.content_item_definition)
      else
        TaskItemDefinition.new(position: o_ti_def.position, info_box: o_ti_def.info_box, required: o_ti_def.required, add_to_calendar: o_ti_def.add_to_calendar,
          workflow_definition: o_ti_def.workflow_definition, content_item_definition: build_content_item_definition_clone(o_ti_def.content_item_definition))
      end
    end.to_a
  end

  def build_content_item_definition_clone(content_item_definition)
    ContentItemDefinition.new(
      label: content_item_definition.label,
      content_type: content_item_definition.content_type.to_s,
      options: content_item_definition.options,
      default_value: content_item_definition.default_value,
      workflow_definition: @original_task_definition.workflow_definition
    )
  end

  def all_clones_and_original_task
    @original_task_definition.definition_workflow_or_block.direct_task_definitions
      .select { |task_definition| task_definition.name.include?(@original_task_definition.name) }
  end

  def next_free_name_counter
    all_clones_and_original_task.count
  end

  def next_position
    @original_task_definition.position + 1
  end

  def move_all_existing_task_definitions_one_down
    @original_task_definition.definition_workflow_or_block.direct_task_definitions.where("position >= ?", next_position).update_all("position = position + 1")
  end
end
