class Services::DueTasks < ApplicationRecord
  def self.find_and_process
    find_unprocessed.find_each do |due_task|
      Events::DueEvent.create!(object: due_task)
      due_task.update!(due_processed_last: Date.current)
    end
  end

  def self.find_unprocessed
    Task.created_or_active.workflow_open.with_due_in_past.where("due_processed_last < ?", Date.current)
      .or(Task.created_or_active.workflow_open.with_due_in_past.where(due_processed_last: nil))
  end
end
