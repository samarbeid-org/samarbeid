class Services::NewEngine
  attr_accessor :workflow
  def initialize(workflow)
    @workflow = workflow
    @workflow.reload if @workflow.persisted?
  end

  def run(triggered_by = nil)
    auto_transition_blocks

    current_item = next_item_to_enable
    return unless current_item&.previous_items

    if current_item.previous_items.all?(&:closed?)
      current_item.enable!(triggered_by)

      run(triggered_by) if current_item.is_a?(Block) && current_item.reload.skipped? # we want the next block to be enabled automatically
    end
  ensure
    workflow.align_state
  end

  def auto_transition_blocks
    workflow.blocks.each do |block|
      block.complete! if block.may_complete?
      block.check_condition! if block.may_check_condition?
    end
  end

  def next_item_to_enable
    all_items = workflow.items.flat_map { |item| item.is_a?(Block) ? [item] + item.items : item }
    all_items.find { |item| item.reload.may_enable? }
  end

  def structure_to_s
    output = ""
    output << "#{workflow.name} [#{workflow.aasm_state}]\n"
    workflow.items.each do |item|
      output << "- " + item.name + " [#{item.aasm_state}]\n"
      if item.is_a?(Task)
        item.task_items.sort_by(&:position).each do |task_item|
          output << "-- " + (task_item.info_box ? "I: " : "F: ") + (task_item.content_item.label || "") + "\n"
        end
      end
      if item.is_a?(Block)
        item.items.each do |task|
          output << "-- " + task.name + " [#{task.aasm_state}]\n"
          task.task_items.sort_by(&:position).each do |task_item|
            output << "--- " + (task_item.info_box ? "I: " : "F: ") + (task_item.content_item.label || "") + "\n"
          end
        end
      end
    end
    output
  end
end
