class Services::TimeTracking
  def self.localized_time(time_in_minutes)
    hours = time_in_minutes / 60
    minutes = time_in_minutes % 60
    times = []
    times.push(I18n.t("time_tracking.time.hours", count: hours)) if hours > 0
    times.push(I18n.t("time_tracking.time.minutes", count: minutes)) if minutes > 0

    times.join(" ")
  end

  def self.message(time_tracking)
    "<p>--<br>#{I18n.t("time_tracking.message", time_spent: localized_time(time_tracking[:timeSpentInMinutes]), spent_at: I18n.l(ContentTypes::Date.cast(time_tracking[:spentAt])))}</p>"
  end
end
