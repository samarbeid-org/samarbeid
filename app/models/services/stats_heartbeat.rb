class Services::StatsHeartbeat
  def initialize(date = 1.day.ago.to_date)
    raise "date must be a date" unless date.is_a?(Date)
    @host = ENV.fetch("HOST", "default")
    @date = date
    @timeframe = @date.beginning_of_day..@date.end_of_day
  end

  def headers
    ["host", "date", "daily_active_users", "daily_files_total_size_in_byte"] + event_types
  end

  def data
    [@host, @date.to_fs(:db), daily_active_users[@date].to_i, daily_files_total_size] + event_types.map { |et| events_per_class[et].to_i }
  end

  def as_hash
    headers.zip(data).to_h
  end

  def enabled?
    send_to.present?
  end

  def send_to
    ENV["SEND_STATS_HEARTBEAT_TO"]
  end

  private

  def event_types
    if Rails.env.development?
      Rails.application.eager_load!
    end # otherwise only used classes appear
    Event.subclasses.map(&:name).sort
  end

  def events_per_class
    @epc ||= Event.where(created_at: @timeframe).group(:type).count
  end

  def daily_active_users
    @dau ||= Event.where(created_at: @timeframe).where.not(subject_id: nil).group("DATE(created_at)").distinct.count(:subject_id)
  end

  def daily_files_total_size
    @fts ||= ActiveStorage::Blob.where(created_at: @timeframe).sum(:byte_size)
  end
end
