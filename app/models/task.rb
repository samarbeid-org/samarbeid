class Task < ApplicationRecord
  include ReferenceSupport
  include SearchForList
  include FilterSupport::TaskFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: {id: "A", name: "B"},
    associated_against: {
      workflow: {title: "C"},
      workflow_definition: {name: "D"}
    },
    using: {
      tsearch: {prefix: true}
    }

  scope :with_item_definition, ->(content_item_definition, as_content_items: nil) {
    task_table = Task.arel_table
    workflows_table = Arel::Table.new(:workflows)
    content_items_table = Arel::Table.new(:content_items, as: as_content_items)

    joins(
      task_table.create_join(
        workflows_table,
        task_table.create_on(task_table[:workflow_id].eq(workflows_table[:id]))
      ),
      workflows_table.create_join(
        content_items_table,
        task_table.create_on(workflows_table[:id].eq(content_items_table[:workflow_id]))
      )
    ).where(content_items_table[:content_item_definition_id].eq(content_item_definition.id))
  }

  scope :with_item_value_filter, ->(content_item_definition, comp_op, *options, filter_index: nil) {
    content_items_alias = "content_items_#{filter_index}"

    with_item_definition(content_item_definition, as_content_items: content_items_alias)
      .merge(content_item_definition.content_type.filter_by(ContentItem.all, content_items_alias, comp_op, *options))
  }

  after_save :reindex_self_and_workflow
  after_destroy -> { workflow.reindex_as_search_document unless destroyed_by_association }
  after_destroy -> { Services::NewEngine.new(workflow).run unless destroyed_by_association }
  after_commit :on_task_updated, on: :update

  belongs_to :workflow
  has_one :workflow_definition, through: :workflow
  belongs_to :task_definition, optional: true

  belongs_to :workflow_or_block, polymorphic: true, inverse_of: :direct_tasks

  has_many :task_items, -> { order(:position) }, inverse_of: :task, dependent: :destroy

  alias_attribute :title, :name

  belongs_to :assignee, class_name: "User", optional: true
  delegate :candidate_assignee, to: :task_definition, allow_nil: true

  has_many :contributions, as: :contributable, dependent: :destroy
  has_many :contributors, -> { order "contributions.created_at" }, through: :contributions, inverse_of: false, source: :user, after_add: :on_contributors_changed, after_remove: :on_contributors_changed
  delegate :candidate_contributors, to: :task_definition, allow_nil: true

  has_and_belongs_to_many :marked_by_users, class_name: "User", foreign_key: :workflow_task_id, join_table: :users_workflow_tasks_marked, after_add: :reindex_self, after_remove: :reindex_self

  has_many :comments, as: :object, dependent: :destroy

  has_many :share_links, -> { order(updated_at: :desc) }, as: :shareable, dependent: :destroy

  has_many :webhooks, dependent: :destroy

  include AddToFulltextSearch
  def as_search_document
    {
      identifier: identifier, title: title, subtitle: "#{workflow.name} > #{workflow.title}",
      content: comments_as_text
    }
  end

  def comments_as_text
    comments.map { |c| "#{c.author.fullname}: #{c.message}" }.join("<br>")
  end

  validates :name, :workflow, presence: true

  include AASM

  aasm whiny_persistence: true do
    state :created, initial: true, after_exit: ->(triggered_by = nil) { set_defaults(triggered_by) }
    state :active
    state :snoozed, after_exit: -> { update!(start_at: nil) }
    state :completed
    state :skipped

    after_all_events do |triggered_by, param|
      update!(state_updated_at: Time.now)
      Services::NewEngine.new(workflow).run(triggered_by) unless param == :skip_engine
    end

    event :enable do
      transitions from: :created, to: :active, if: -> { workflow.active? && workflow_or_block.active? }
      after do |triggered_by|
        Events::StartedEvent.create!(object: self, triggered_by: triggered_by)

        if task_definition&.deferral_in_days
          snooze!
          update!(start_at: Time.now + task_definition.deferral_in_days.days)
          Events::SnoozedEvent.create!(object: self, data: {snoozed_until: start_at&.to_date})
        end
      end
    end

    event :start do
      transitions from: [:created, :snoozed], to: :active
    end

    event :snooze do
      transitions from: :active, to: :snoozed, if: -> { workflow.active? }
    end

    event :complete do
      transitions from: [:snoozed, :active], to: :completed, if: -> { workflow.active? }
      after do |triggered_by|
        update!(completed_at: Time.now)
        confirm_content_items
        trigger_webhooks(triggered_by)
      end
    end

    event :reopen do
      transitions from: [:completed, :skipped], to: :active,
        after: -> { maybe_reopen_block }
    end

    event :skip do
      transitions from: [:created, :snoozed, :active], to: :skipped, if: -> { workflow.active? }
    end
  end

  def open?
    created? || active? || snoozed?
  end

  def closed?
    !open?
  end

  def closed_or_created?
    closed? || created?
  end

  def set_defaults(triggered_by)
    update!(due_at: Time.now + task_definition.due_in_days.days) if task_definition&.due_in_days
    set_assignee_from_candidates(triggered_by)
    set_candidate_contributors(triggered_by)
  end

  def set_assignee_from_candidates(triggered_by)
    return if assignee.present?

    new_assignee = get_best_candidate_assignee

    if new_assignee
      Interactors::TaskInteractor.update_assignee(self, new_assignee, nil, triggered_by)
    end
  end

  def get_best_candidate_assignee
    return candidate_assignee if candidate_assignee.present? && candidate_assignee.can?(:read, self)
    return workflow.assignee if task_definition&.candidate_assignee_from_workflow && workflow.assignee.present? && workflow.assignee.can?(:read, self)
    nil
  end

  def set_candidate_contributors(triggered_by)
    new_contributors = candidate_contributors&.select { |user| user.can?(:read, self) }

    if new_contributors&.any?
      Interactors::TaskInteractor.update_contributors(self, contributors | new_contributors, nil, triggered_by)
    end
  end

  def maybe_reopen_block
    if workflow_or_block.is_a?(Block) && workflow_or_block.completed?
      workflow_or_block.reopen!
    end
  end

  def confirm_content_items
    task_items.map(&:content_item).uniq.each do |content_item|
      content_item.confirm! if content_item.may_confirm?
    end
  end

  def trigger_webhooks(triggered_by)
    webhooks.each do |webhook|
      webhook.perform(triggered_by)
      webhook_executed_event = Events::SimpleActionEvent.new(subject: triggered_by, object: webhook.task,
        whats_going_on: {webhook_executed: {result: webhook.result_to_s}})
      webhook_executed_event.notify = :subject if webhook.notify_after_execution
      webhook_executed_event.save!
    end
  end

  singleton_class.undef_method :open # hide Kernel.open (which we do not need), avoiding a warning when defining scope :open
  scope :open, -> { created.or(active).or(snoozed) }
  scope :created_or_active, -> { created.or(active) }
  scope :closed, -> { completed.or(skipped) }
  scope :workflow_open, -> { joins(:workflow).where(workflows: {aasm_state: :active}) }
  scope :current_and_assigned_to, ->(user) { active.accessible_by(user.ability).workflow_open.where(assignee: user) }
  scope :with_due_in_past, -> { where("due_at <= ?", Date.today) }
  scope :with_start_at_in_past, -> { where("start_at <= ?", Time.now) }

  def completable?
    required_content_items_without_data.none?
  end

  def required_content_items_without_data
    required_content_items = task_items.select(&:required).map(&:content_item).uniq
    required_content_items.select do |content_item|
      content_item.content_type.data_empty?(content_item.value)
    end
  end

  def previous_items
    own_index = workflow_or_block.items.index(self)
    return [] if own_index == 0
    workflow_or_block.items[0..(own_index - 1)]
  end

  def events
    Event.where(object: self).order(:created_at)
  end

  def marked?(user)
    marked_by_users.exists?(user.id)
  end

  def due?
    open? && due_at.present? && due_at <= Date.today
  end

  def due_at=(new_due_date)
    super
    if due_processed_last.present? && !due?
      update(due_processed_last: nil)
    end
  end

  def contributors=(new_contributors)
    super
    reindex_self_and_workflow
  end

  def marked_by_users=(new_users)
    super
    reindex_self
  end

  def assignee=(new_assignee)
    super
    reindex_self_and_workflow
  end

  def identifier
    "##{id}"
  end

  def todo_counts
    content_items_of_type_richtext = task_items.reject(&:info_box).map(&:content_item).select { |ci| ci.content_type == ContentTypes::Richtext }.uniq
    todo_counts = content_items_of_type_richtext.map { |ci| ci.content_type.extract_done_and_total_todos(ci.value) }.select { |e| e[1] > 0 }
    todo_counts.transpose.map(&:sum) if todo_counts.any?
  end

  # ReferenceSupport
  def reference_label(noaccess = false)
    identifier + (noaccess ? "" : " • #{title}")
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  def time_tracking_spent_in_minutes
    Events::CommentedEvent.for_object(self).reduce(0) { |sum, e| sum + (e.time_spent_in_minutes || 0) }
  end

  def reindex_self(_params = nil)
    return if workflow.skip_indexing
    return unless persisted?
    reindex_as_search_document
  end

  def reindex_self_and_workflow
    return if workflow.skip_indexing
    return unless persisted?
    reindex_as_search_document
    workflow.reindex_as_search_document
  end

  def on_contributors_changed(_params = nil)
    reindex_self_and_workflow
    MonitorChannel.broadcast_object_updated(self)
  end

  def on_task_updated
    MonitorChannel.broadcast_object_updated(self)
    MonitorProcessStructureChannel.broadcast_process_structure_updated(self)
  end
end
