class Setting < ApplicationRecord
  def self.setting_accessor(attr)
    singleton_class.define_method(attr) do
      get(attr)
    end

    singleton_class.define_method(:"#{attr}=") do |value|
      set(attr, value)
    end
  end

  serialize :value

  setting_accessor :main_menu_auto_add_deactivated

  def self.get(key)
    find_by(key: key)&.value
  end

  def self.set(key, value)
    setting = find_by(key: key)

    if value.nil?
      setting&.destroy
    else
      setting ||= new(key: key)
      setting.value = value
      setting.save
    end
  end
end
