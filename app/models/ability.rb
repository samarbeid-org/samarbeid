class Ability
  include CanCan::Ability

  def initialize(user, act_as_normal_user = false)
    return unless user

    #################################
    # Access via ShareLink
    if user.is_a?(ShareLink)
      can :ping, User, id: user.id
      can [:show, :complete], ShareLink, id: user.id
      return unless user.active?
      can :read, user.shareable
      if user.shareable.is_a?(Task)
        can [:read, :lock, :unlock, :update], TaskItem, task: user.shareable
        can [:list, :show], Dossier, definition_id: user.listable_dossier_definition_ids
        can :create, UploadedFile
        can :manage_uploads, user.shareable
      end
    end

    #################################
    # Normal User
    return unless user.is_a?(User)

    can [:show, :list, :index], User
    can [:read, :show_settings, :groups, :update, :update_email, :update_password, :update_notification_settings,
      :info, :ping, :manage_tokens], User, id: user.id

    can :manage, Notification, user: user

    can :read, Services::Search

    can :read, Group

    can :create, UploadedFile

    can :manage, DossierDefinition, groups: {id: user.group_ids}
    can :create, DossierDefinition
    can :manage, DossierItemDefinition, definition: {groups: {id: user.group_ids}}
    can :create, DossierItemDefinition
    can :manage, Dossier, definition: {groups: {id: user.group_ids}}

    can :manage, Task, workflow: {workflow_definition: {groups: {id: user.group_ids}}}
    can :manage, TaskItem, task: {workflow: {workflow_definition: {groups: {id: user.group_ids}}}}
    can :manage, Block, workflow: {workflow_definition: {groups: {id: user.group_ids}}}

    can [:manage, :read, :new, :create, :update, :update_assignee, :update_contributors, :update_private_status], Workflow, workflow_definition: {groups: {id: user.group_ids}}
    can :manage, ContentItem, workflow: {workflow_definition: {groups: {id: user.group_ids}}}

    can [:manage, :build_instance], WorkflowDefinition, groups: {id: user.group_ids}
    can :create, WorkflowDefinition
    can :manage, TaskDefinition, workflow_definition: {groups: {id: user.group_ids}}
    can :manage, BlockDefinition, workflow_definition: {groups: {id: user.group_ids}}
    can :manage, TaskItemDefinition, workflow_definition: {groups: {id: user.group_ids}}

    can :manage, Automation, workflow_definition: {groups: {id: user.group_ids}}

    can [:update, :destroy], Comment, author_id: user.id
    can :read, News

    can :manage, :api

    can :show, MainMenuEntry, linked_object: WorkflowDefinition.includes(:groups).where(groups: {id: user.group_ids})
    can :show, MainMenuEntry, linked_object: DossierDefinition.includes(:groups).where(groups: {id: user.group_ids})
    can :show, MainMenuEntry, linked_object: nil
    can :index_filtered, MainMenuEntry

    #################################
    # Admins
    return unless user.is_admin? && !act_as_normal_user

    can :manage, WorkflowDefinition
    can :manage, TaskDefinition
    can :manage, BlockDefinition
    can :manage, TaskItemDefinition
    can :manage, DossierDefinition
    can :manage, DossierItemDefinition

    can :manage, Workflow
    can :manage, Task
    can :manage, TaskItem
    can :manage, Dossier

    can :manage, User
    cannot [:update_admin_status, :update_active_status], User, id: user.id

    can :manage, Group

    can :manage, MenuEntry
    can :manage, Page

    can :manage, MainMenuEntry

    #################################
    # Super Admins
    return unless user.super_admin?

    can :manage, :all # grant access to do anything on all models
    can :access, :rails_admin # grant access to rails_admin
  end
end
