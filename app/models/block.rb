class Block < ApplicationRecord
  belongs_to :workflow
  belongs_to :content_item, optional: true
  belongs_to :block_definition, optional: true
  has_many :direct_tasks, -> { order(:position) }, class_name: "Task", as: :workflow_or_block, dependent: :destroy

  validates_presence_of :position
  validate :validate_decision

  def items
    direct_tasks
  end

  include AASM

  aasm whiny_persistence: true do
    state :created, initial: true
    state :active, after_enter: ->(triggered_by = nil) { enable_one_or_all_tasks(triggered_by) }
    state :skipped, :completed

    after_all_events do
      workflow.align_state
    end

    event :enable do
      transitions from: :created, to: :active, if: -> { condition_valid? }
      transitions from: :created, to: :skipped, unless: -> { condition_valid? }
    end

    event :check_condition do
      transitions from: :skipped, to: :active, if: -> { condition_valid? }
      transitions from: :active, to: :skipped, unless: -> { condition_valid? }
    end

    event :complete do
      transitions from: :active, to: :completed, if: -> { items.all?(&:closed?) }
    end

    event :reopen do
      transitions from: :completed, to: :active, if: -> { condition_valid? }
      transitions from: :completed, to: :skipped, unless: -> { condition_valid? }
    end
  end

  def open?
    created? || active?
  end

  def closed?
    !open?
  end

  def previous_items
    own_index = workflow.reload.items.index(self)
    return [] if own_index == 0
    workflow.items[0..(own_index - 1)]
  end

  def name
    "B: #{title}"
  end

  def title
    return "Block (#{content_item&.label}:#{localized_decision})" if read_attribute(:title).blank?
    read_attribute(:title)
  end

  def decision_set?
    content_item.present? && !decision_empty?
  end

  def condition_set?
    decision_set? && !content_item.content_type.data_empty?(content_item.value)
  end

  def condition_valid?
    return false if direct_tasks.empty?
    return true unless decision_set?
    return false unless content_item.confirmed?

    content_item.content_type.compare(content_item.value, decision)
  end

  def condition_valid_and_not_confirmed?
    return true unless decision_set?

    content_item.content_type.compare(content_item.value, decision)
  end

  def enable_one_or_all_tasks(triggered_by)
    items.each(&:reload)
    if parallel
      items.each { |task| task.enable!(triggered_by) if task.may_enable? }
    else
      items.find(&:may_enable?)&.enable!(triggered_by)
    end
  end

  def decision
    content_item&.content_type&.deserialize(read_attribute(:decision))
  end

  def decision=(new_decision_value)
    casted_new_decision_value = content_item&.content_type&.serialize(content_item&.content_type&.prepare(content_item&.content_type&.cast(new_decision_value), self))
    return if casted_new_decision_value == decision

    write_attribute(:decision, casted_new_decision_value)
  end

  def assign_attributes(new_attributes)
    assign_first = new_attributes.extract!(:content_item_id, :content_item)
    super(assign_first) unless assign_first.empty?
    super
  end

  private

  def decision_empty?
    content_item.content_type.data_empty?(decision)
  end

  def localized_decision
    content_item&.content_type&.localized_string(decision) || ""
  end

  def validate_decision
    return unless content_item.present?

    errors.add(:decision, :blank) && return if decision_empty?

    decision_options = content_item.options.clone
    decision_options["multiple"] = true if decision_options.has_key?("multiple")
    validation_result = content_item.content_type.validate_data(decision, decision_options)
    errors.add(:decision, validation_result) if validation_result != true
  end
end
