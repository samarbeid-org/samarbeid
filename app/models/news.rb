class News < ApplicationRecord
  include ReferenceSupport

  validates :title, :url, presence: true

  def reference_label(noaccess = false)
    title.presence || id
  end

  def reference_object(current_user)
    {
      type: "link",
      url: url,
      text: reference_label(current_user.cannot?(:show, self))
    }
  end
end
