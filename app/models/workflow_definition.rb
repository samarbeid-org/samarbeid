class WorkflowDefinition < ApplicationRecord
  include ReferenceSupport
  include DeactivationSupport
  include ContentTypeAttributeSupport
  include SearchForList
  include FilterSupport::WorkflowDefinitionFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: [:name],
    using: {
      tsearch: {prefix: true}
    }

  has_many :workflows, dependent: :destroy
  has_many :comments, as: :object, dependent: :destroy
  has_many :automations, foreign_key: "workflow_definition_id", dependent: :destroy, inverse_of: :workflow_definition
  has_many :main_menu_entries, -> { order(:position) }, as: :linked_object, dependent: :destroy, inverse_of: :linked_object

  has_many :all_task_definitions, -> { order(:position) }, class_name: "TaskDefinition", inverse_of: :workflow_definition, dependent: :destroy
  has_many :direct_task_definitions, -> { order(:position) }, class_name: "TaskDefinition", as: :definition_workflow_or_block
  has_many :block_definitions, -> { order(:position) }, dependent: :destroy
  has_many :content_item_definitions, dependent: :destroy
  has_many :title_item_definitions, -> { where.not(title_position: nil).order(:title_position) }, class_name: "ContentItemDefinition"
  has_many :task_item_definitions, dependent: :destroy

  has_and_belongs_to_many :groups, after_add: :after_groups_changed, after_remove: :after_groups_changed

  scope :system_process_definition_single_task_scope, -> { where(system_identifier: :system_process_definition_single_task) }
  scope :not_system_process_definition, -> { where(system_identifier: nil) }
  scope :not_system_process_definition_single_task, -> { where.not(system_identifier: :system_process_definition_single_task).or(not_system_process_definition) }
  scope :hidden, -> { activated.where.missing(:groups) }

  content_type_attribute :description, ContentTypes::Richtext

  def item_definitions
    (direct_task_definitions + block_definitions).sort_by(&:position)
  end

  def task_definitions_ordered_in_structure
    item_definitions.flat_map { |item_definition| item_definition.is_a?(BlockDefinition) ? item_definition.item_definitions : item_definition }
  end

  enum system_identifier: [:system_process_definition_single_task].map { |entry| [entry.to_sym, entry.to_s] }.to_h, _scopes: false

  validates :name, presence: true, uniqueness: true
  validates_no_deactivated_prefix :name
  validates :system_identifier, uniqueness: true, allow_nil: true, if: :system_process_definition_single_task?

  after_update :reindex_workflows
  after_commit :main_menu_auto_add, on: :create
  after_commit :main_menu_update, on: [:update, :destroy]

  after_commit :update_workflow_title

  def display_name
    prepend_prefixes(name, [deactivated_prefix])
  end

  def rails_admin_label
    "#{id}-#{name&.parameterize&.truncate(25)}"
  end

  def build_workflow(field_data = nil)
    field_data = {} unless field_data.is_a?(Hash)
    field_data.transform_keys!(&:to_s)

    new_workflow = Workflow.new(workflow_definition: self, description: description, definition_version: version)
    new_workflow.content_items = content_item_definitions.map do |content_item_definition|
      options = field_data.key?(content_item_definition.id.to_s) ? {value: field_data[content_item_definition.id.to_s]} : {}
      content_item_definition.build_content_item(new_workflow, **options)
    end
    new_workflow.direct_tasks = direct_task_definitions.map { |def_task| def_task.build_task(new_workflow) }
    new_workflow.blocks = block_definitions.map { |def_block| def_block.build_block(new_workflow) }
    new_workflow.system_identifier = Workflow.system_identifiers[:system_process_single_task] if system_process_definition_single_task?
    new_workflow
  end

  def structure_as_text
    Services::NewEngine.new(build_workflow).structure_to_s
  end

  def reference_label(_noaccess = false)
    display_name.presence || id
  end

  def start_item_definitions
    content_item_definitions.where.not(start_item_position: nil).order(:start_item_position)
  end

  def update_start_item_definitions(cid_ids)
    update_content_item_definition_position_attribute(start_item_definitions.map(&:id), cid_ids, :start_item_position)
  end

  def update_title_item_definitions(cid_ids)
    update_content_item_definition_position_attribute(title_item_definitions.map(&:id), cid_ids, :title_position)
    reindex_workflows
    # FIXME: seeing this while working on !1741, I think we should also reindex tasks here.
  end

  def self.system_process_definition_single_task
    process_definition = find_by(system_identifier: :system_process_definition_single_task)
    return process_definition unless process_definition.nil?

    process_definition = WorkflowDefinition.new(
      system_identifier: :system_process_definition_single_task,
      name: I18n.t("system_process_definition_single_task.name"),
      description: I18n.t("system_process_definition_single_task.description"),
      group_ids: Group.system_group_all.id
    )

    task_definition = process_definition.all_task_definitions.build(
      name: I18n.t("system_process_definition_single_task.task.name"),
      position: 10,
      definition_workflow_or_block: process_definition
    )

    content_item_description = process_definition.content_item_definitions.build(
      label: I18n.t("system_process_definition_single_task.task.items.description"),
      content_type: ContentTypes::Richtext.to_s
    )

    content_item_attachments = process_definition.content_item_definitions.build(
      label: I18n.t("system_process_definition_single_task.task.items.attachments"),
      content_type: ContentTypes::File.to_s,
      options: {multiple: true}
    )

    process_definition.task_item_definitions.build([
      {
        position: 10,
        content_item_definition: content_item_description,
        task_definition: task_definition,
        workflow_definition: process_definition
      },
      {
        position: 20,
        content_item_definition: content_item_attachments,
        task_definition: task_definition,
        workflow_definition: process_definition
      }
    ])

    process_definition.save
    process_definition
  end

  def human_model_name
    "#{model_name.human} (#{name})"
  end

  def field_definitions
    res = content_item_definitions.no_notes.map do |cid|
      OpenStruct.new(content_item_definition: cid, task_item_definition: cid.first_task_item_definition(false))
    end

    res.sort_by do |item|
      [
        item.task_item_definition.task_definition.definition_workflow_or_block.is_a?(BlockDefinition) ? item.task_item_definition.task_definition.definition_workflow_or_block.position : item.task_item_definition.task_definition.position,
        item.task_item_definition.task_definition.definition_workflow_or_block.is_a?(BlockDefinition) ? item.task_item_definition.task_definition.position : 0,
        item.task_item_definition.position
      ]
    end.map do |field_def|
      OpenStruct.new(
        id: field_def.content_item_definition.id,
        label: field_def.content_item_definition.label,
        content_type: field_def.content_item_definition.content_type,
        options: field_def.content_item_definition.options,
        grouping: field_def.task_item_definition.task_definition.name
      )
    end
  end

  private

  def after_groups_changed(_params)
    MenuChannel.broadcast_update
  end

  def reindex_workflows
    ReindexSearchablesJob.perform_later(self, :workflows)
  end

  def update_content_item_definition_position_attribute(current_cid_ids, new_cid_ids, position_attribute)
    if current_cid_ids != new_cid_ids
      new_cid_ids.each_with_index do |cid_id, index|
        if current_cid_ids[index] != cid_id
          cid = content_item_definitions.find_by(id: cid_id)
          if cid.nil?
            errors.add(:content_item_definition, "ContentItemDefinition mit ID '#{cid_id}' existiert nicht")
            return nil
          end
          unless cid.update(position_attribute => index)
            errors.add(:content_item_definition, "#{cid.errors.full_messages.join(". ")}.")
            return nil
          end
        end
      end
      content_item_definitions.where.not(id: new_cid_ids)
        .where.not(position_attribute => nil)
        .update(position_attribute => nil)
      title_item_definitions.reset if position_attribute == :title_position
    end
  end

  def main_menu_auto_add
    Interactors::MainMenuEntryInteractor.create_many(nil, [self], nil) unless Setting.main_menu_auto_add_deactivated
  end

  def main_menu_update
    MenuChannel.broadcast_update if destroyed? || name_previously_changed?
  end

  def update_workflow_title
    return unless auto_generate_title_previously_changed?

    SyncTitlesJob.perform_later(self, :workflows)
  end
end
