class BlockDefinition < ApplicationRecord
  has_many :blocks, dependent: :nullify
  belongs_to :workflow_definition
  belongs_to :content_item_definition, optional: true
  has_many :direct_task_definitions, -> { order(:position) }, class_name: "TaskDefinition", as: :definition_workflow_or_block, dependent: :destroy

  def item_definitions
    direct_task_definitions
  end

  validates_presence_of :position, :title
  validate :validate_decision
  validate :all_belong_to_same_workflow_definition

  def name
    "B: #{title}"
  end

  def rails_admin_label
    "#{workflow_definition&.rails_admin_label} > #{id}-#{name&.parameterize}"
  end

  def build_block(new_workflow)
    new_block = Block.new(title: title, position: position, parallel: parallel, block_definition: self)
    new_block.workflow = new_workflow
    new_block.direct_tasks = direct_task_definitions.map { |def_task_in_block| def_task_in_block.build_task(new_block) }

    if content_item_definition
      new_block.content_item = new_workflow.content_items.find { |ci| ci.label == content_item_definition.label }
      new_block.decision = decision
    end
    new_block
  end

  def decision
    content_item_definition&.content_type&.deserialize(read_attribute(:decision))
  end

  def decision=(new_decision_value)
    casted_new_decision_value = content_item_definition&.content_type&.serialize(content_item_definition&.content_type&.prepare(content_item_definition&.content_type&.cast(new_decision_value), self))
    return if casted_new_decision_value == decision

    write_attribute(:decision, casted_new_decision_value)
  end

  def assign_attributes(new_attributes)
    assign_first = new_attributes.extract!(:content_item_definition_id, :content_item_definition)
    super(assign_first) unless assign_first.empty?
    super
  end

  def human_model_name
    "#{model_name.human} (#{title}) der #{workflow_definition.human_model_name}"
  end

  private

  def decision_empty?
    content_item_definition.content_type.data_empty?(decision)
  end

  def validate_decision
    return unless content_item_definition.present?

    errors.add(:decision, :blank) && return if decision_empty?

    decision_options = content_item_definition.options.clone
    decision_options["multiple"] = true if decision_options.has_key?("multiple")
    validation_result = content_item_definition.content_type.validate_data(decision, decision_options)
    errors.add(:decision, validation_result) if validation_result != true
  end

  def all_belong_to_same_workflow_definition
    if content_item_definition && !content_item_definition.workflow_definition.id.eql?(workflow_definition_id)
      errors.add(:content_item_definition, "Muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
    unless direct_task_definitions.all? { |td| td.workflow_definition_id.eql?(workflow_definition_id) }
      errors.add(:direct_task_definitions, "Muss zur selben workflow_definition '#{workflow_definition&.rails_admin_label}' gehören!")
    end
  end
end
