class ContentItemDefinition < ApplicationRecord
  include ContentTypeSupport
  include RichtextImageAttachable

  belongs_to :workflow_definition
  has_many :task_item_definitions, dependent: :destroy
  has_many :content_items, dependent: :nullify
  has_many :block_definitions, dependent: :nullify

  scope :no_notes, -> { where.not(content_type: ContentTypes::Note.to_s) }

  validates :label, presence: true, unless: -> { content_type.to_s === ContentTypes::Note.to_s }
  validate :validate_default_value

  after_commit :update_workflow_title

  # def filter_by_content_type_value(comp_op, *options)
  #   content_type.filter_by(ContentItem.all, comp_op, *options)
  # end

  def default_value
    content_type.deserialize(read_attribute(:default_value))
  end

  def default_value=(new_value)
    casted_new_value = content_type.serialize(content_type.prepare(content_type.cast(new_value), self))
    return if casted_new_value == default_value

    write_attribute(:default_value, casted_new_value)
  end

  def get_content_type_values
    [default_value]
  end

  def get_content_type_referenced_values
    block_definitions.map(&:decision)
  end

  def computed_label
    if content_type.to_s === ContentTypes::Note.to_s
      note_label = ActionView::Base.full_sanitizer.sanitize(default_value)&.truncate(50)
      note_label = "\"#{note_label}\"" if !note_label.blank?
      return note_label
    end

    label
  end

  def rails_admin_label
    "#{workflow_definition&.rails_admin_label} > #{id}-#{label&.parameterize}"
  end

  def label_with_task
    first_task_definition = task_item_definitions.reject(&:info_box).map(&:task_definition).min_by(&:position)
    "#{label} - aus Aufgabe #{first_task_definition&.name}"
  end

  def build_content_item(new_workflow, **build_options)
    ContentItem.new(label: label, content_type: content_type.to_s, options: options,
      value: build_options.key?(:value) ? build_options[:value] : default_value,
      workflow: new_workflow, content_item_definition: self)
  end

  def human_model_name
    "#{model_name.human} (#{label}) der #{workflow_definition.human_model_name}"
  end

  def first_task_item_definition(exclude_infoboxes)
    task_definitions_ordered = workflow_definition.task_definitions_ordered_in_structure

    result = task_item_definitions.includes(:task_definition)
    result = result.reject(&:info_box) if exclude_infoboxes
    result.min_by { |tid| [task_definitions_ordered.find_index(tid.task_definition), tid.position] }
  end

  private

  def validate_default_value
    return if default_value.nil?
    validation_result = content_type.validate_data(default_value, options)
    errors.add(:default_value, validation_result) if validation_result != true
  end

  def update_workflow_title
    return unless title_position_previously_changed? || title_position.present?
    return unless workflow_definition&.auto_generate_title?

    SyncTitlesJob.perform_later(workflow_definition, :workflows)
  end
end
