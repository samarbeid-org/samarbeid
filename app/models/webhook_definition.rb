class WebhookDefinition < ApplicationRecord
  belongs_to :task_definition
  has_many :webhooks, dependent: :nullify

  def build_webhook(new_task)
    Webhook.new(url: url, auth_token: auth_token,
      task: new_task, webhook_definition: self,
      wait_for_response: wait_for_response,
      notify_after_execution: notify_after_execution)
  end
end
