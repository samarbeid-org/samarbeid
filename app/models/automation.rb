class Automation < ApplicationRecord
  include ReferenceSupport
  include FilterSupport::AutomationFilterSupport

  belongs_to :workflow_definition, class_name: "WorkflowDefinition", inverse_of: :automations
  belongs_to :candidate_assignee, class_name: "User", optional: true

  validates :title, presence: true, uniqueness: true
  validates :active, exclusion: [nil]
  validate :validate_schedule

  before_save :cleanup_schedule_hash
  before_save :update_next_execution_date

  scope :activated, -> { where(active: true) }
  scope :deactivated, -> { where(active: false) }
  scope :active_and_due_today, -> { activated.where(next_execution: ..Date.today) }

  EXECUTION_TIME = Time.local(1970, 1, 1, 6) # '06:00 am' (sync with schedule.rb runner time)

  RULES = {
    IceCube::DailyRule.to_s => "daily",
    IceCube::WeeklyRule.to_s => "weekly",
    IceCube::MonthlyRule.to_s => "monthly",
    IceCube::YearlyRule.to_s => "yearly"
  }

  def ice_cube_schedule
    start_date = Date.parse(schedule["start"])
    validations = case schedule["rule"]
    when "weekly"
      {day: schedule["validations"]&.[]("day") || [start_date.wday]}
    when "monthly"
      day_of_month = schedule["validations"]&.[]("day_of_month") || "start"
      {day_of_month: (day_of_month == "start") ? [start_date.day] : [-1]}
    when "yearly"
      {
        day_of_month: [start_date.day],
        month_of_year: [start_date.month]
      }
    else
      {}
    end

    IceCube::Schedule.from_hash({
      start_time: schedule["start"],
      rrules: [
        {
          validations: validations,
          rule_type: RULES.key(schedule["rule"]),
          interval: schedule["interval"],
          week_start: 1
        }
      ]
    })
  end

  def get_next_execution
    execution_time_today = Time.now.beginning_of_day.change(hour: EXECUTION_TIME.hour, minute: EXECUTION_TIME.min)
    ((Time.now < execution_time_today) && ice_cube_schedule.occurs_on?(Date.today)) ? execution_time_today : ice_cube_schedule.next_occurrence&.beginning_of_day&.change(hour: EXECUTION_TIME.hour, minute: EXECUTION_TIME.min)
  end

  def self.validate_with_defaults(attributes)
    res = new(attributes)
    res.active = true
    res.schedule = {
      start: Date.today.to_s,
      interval: 1,
      rule: "daily",
      validations: {}
    }.merge(res.schedule || {})

    res.validate_schedule
    res
  end

  def execute
    workflow = Interactors::WorkflowInteractor.create(
      workflow_definition,
      {assignee_id: candidate_assignee&.id},
      nil,
      nil,
      nil,
      self
    )

    Events::ExecutedEvent.create!(object: self, process: workflow) unless workflow.errors.any?
  end

  def validate_schedule
    errors.add(:schedule, "must be a hash") unless schedule.is_a?(Hash)

    begin
      Date.parse(schedule["start"])
    rescue
      errors.add("schedule.start", "must be a valid date")
    end

    errors.add("schedule.interval", "must be an Integer between 1 and 1000") unless schedule["interval"].is_a?(Integer) && schedule["interval"].between?(1, 1000)
    errors.add("schedule.rule", "must be a valid rule") unless RULES.value?(schedule["rule"])
    errors.add("schedule.validations", "must be a hash") unless schedule["validations"].is_a?(Hash)

    if errors.none?
      begin
        ice_cube_schedule
      rescue => error
        errors.add(:schedule, error.message)
      end
    end
  end

  def reference_label(noaccess = nil)
    title
  end

  private

  def cleanup_schedule_hash
    self.schedule = schedule&.slice("start", "interval", "rule", "validations")
  end

  def update_next_execution_date
    self.next_execution = get_next_execution.to_date
  end
end
