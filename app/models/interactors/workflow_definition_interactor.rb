class Interactors::WorkflowDefinitionInteractor
  def self.create(params, user)
    params[:group_ids] = user.groups.map(&:id) unless user.is_admin?
    new_workflow_definition = WorkflowDefinition.create(params)
    if new_workflow_definition.valid?
      Events::CreatedEvent.create!(subject: user, object: new_workflow_definition)
      add_default_task_definition(new_workflow_definition)
    end
    new_workflow_definition
  end

  def self.add_default_task_definition(workflow_definition)
    workflow_definition.direct_task_definitions.create!(name: "Neue Aufgabe", workflow_definition: workflow_definition, position: 0)
  end

  def self.update(workflow_definition, params, user)
    if params.key?(:group_ids)
      if user.is_admin?
        workflow_definition.group_ids = params[:group_ids]
        return workflow_definition if workflow_definition.errors.any?
      end
      params.extract!(:group_ids)
    end

    if params.key?(:active)
      return if is_readonly?(workflow_definition)
      if ActiveRecord::Type::Boolean.new.cast(params[:active])
        workflow_definition.activate
        Events::ActivatedEvent.create!(subject: user, object: workflow_definition) if workflow_definition.valid?
      else
        workflow_definition.deactivate
        Events::DeactivatedEvent.create!(subject: user, object: workflow_definition) if workflow_definition.valid?
      end
      return workflow_definition if workflow_definition.errors.any?

      params.extract!(:active)
    end

    if params.key?(:start_item_definition_ids)
      return if is_readonly?(workflow_definition)
      workflow_definition.update_start_item_definitions(params[:start_item_definition_ids])
      return workflow_definition if workflow_definition.errors.any?

      params.extract!(:start_item_definition_ids)
    end

    if params.key?(:title_item_definition_ids)
      return if is_readonly?(workflow_definition)
      workflow_definition.update_title_item_definitions(params[:title_item_definition_ids])
      return workflow_definition if workflow_definition.errors.any?

      params.extract!(:title_item_definition_ids)
    end

    unless params.blank?
      return if is_readonly?(workflow_definition)
      if workflow_definition.update(params)
        if workflow_definition.name_previously_changed?
          Events::ChangedTitleEvent.create!(
            subject: user,
            object: workflow_definition,
            data: {new_title: workflow_definition.name, old_title: workflow_definition.name_previously_was}
          )
        end
      end
    end
    workflow_definition
  end

  def self.create_from_instance(workflow_instance, params, user)
    params[:group_ids] = user.groups.map(&:id) unless user.is_admin?

    new_workflow_definition = Services::WorkflowToDefinition.new(workflow_instance).create(params)
    return new_workflow_definition if new_workflow_definition.errors.any?

    Events::CreatedEvent.create!(subject: user, object: new_workflow_definition)

    new_workflow_definition
  end

  def self.update_structure(workflow_instance, user)
    return if is_readonly?(workflow_instance.workflow_definition)

    Services::WorkflowToDefinition.new(workflow_instance).update
    workflow_definition = workflow_instance.workflow_definition

    if workflow_definition.errors.none?
      Events::ChangedStructureEventByInstance.create!(subject: user, object: workflow_definition,
        data: {workflow_used_for_update: workflow_instance, new_workflow_definition_version: workflow_definition.version})
    end
  end

  def self.destroy(workflow_definition, user)
    if workflow_definition.system_process_definition_single_task?
      workflow_definition.errors.add(:base, I18n.t("system_process_definition_single_task.errors.destroy"))
      return workflow_definition
    end

    if workflow_definition.workflows.any?
      workflow_definition.errors.add(:base, "Die Vorlage kann erst gelöscht werden, wenn alle dazugehörigen Prozesse gelöscht wurden.")
    else
      workflow_definition.destroy!
      Events::DeletedEvent.create!(object: workflow_definition, subject: user)
    end
  end

  def self.add_task_definition(workflow_definition)
    last_position = workflow_definition.item_definitions.max_by(&:position)&.position || 0
    task_definition = workflow_definition.direct_task_definitions.new(name: "Neue Aufgabe",
      workflow_definition: workflow_definition, position: last_position + 1)

    task_definition.save unless is_readonly?(workflow_definition, task_definition)
    task_definition
  end

  def self.update_task_definition(task_definition, params)
    return if is_readonly?(task_definition.workflow_definition, task_definition)

    task_definition.update(params)
  end

  def self.destroy_task_definition(task_definition)
    return if is_readonly?(task_definition.workflow_definition, task_definition)

    if task_definition.task_item_definitions.map(&:content_item_definition).any? { |ci| ci.block_definitions.any? }
      task_definition.errors.add(:base, "Dieses Datenfeld wird noch als Bedingung in mindestens einem Block genutzt. Es kann daher nicht gelöscht werden.") && return
    end

    if task_definition.errors.none?
      if task_definition.destroy
        task_definition.workflow_definition.content_item_definitions.select { |ci| ci.task_item_definitions.none? }.each(&:destroy!)
      end
    end
  end

  def self.move_task_definition(task_definition, params)
    return if is_readonly?(task_definition.workflow_definition, task_definition)

    target_type = params[:target_type]
    target_id = params[:target_id]

    case target_type
    when "process_definition"
      target = WorkflowDefinition.find_by(id: target_id)
    when "block_definition"
      target = BlockDefinition.find_by(id: target_id)
    end
    target ||= task_definition.definition_workflow_or_block
    unless target == task_definition.workflow_definition || target.workflow_definition == task_definition.workflow_definition
      task_definition.errors.add(:base, "Cannot move task to different process")
      return
    end
    index = params.require(:index).to_i

    position = target.item_definitions[index]&.position || (target.item_definitions.max_by(&:position)&.position || 0)

    if target.item_definitions.index(task_definition).nil? # Move to new target
      if index < target.item_definitions.length # If not move to end of target items
        position -= 1
      end
    elsif index < target.item_definitions.index(task_definition)
      position -= 1 # Move to lower index
    end

    target.direct_task_definitions.where("position > ?", position).update_all("position = position + 1")
    target.block_definitions.where("position > ?", position).update_all("position = position + 1") if target.respond_to?(:block_definitions)

    task_definition.update(position: position + 1, definition_workflow_or_block: target)
    # TODO: reindex source workflow_definition and target workflow_definition (and task_definition?)
  end

  def self.add_block_definition(workflow_definition)
    last_position = workflow_definition.item_definitions.max_by(&:position)&.position || 0
    block_definition = workflow_definition.block_definitions.new(title: "Neuer Block",
      workflow_definition: workflow_definition, position: last_position + 1)

    block_definition.save unless is_readonly?(workflow_definition, block_definition)
    block_definition
  end

  def self.update_block_definition(block_definition, params)
    return if is_readonly?(block_definition.workflow_definition, block_definition)

    block_definition.update(params)
  end

  def self.destroy_block_definition(block_definition)
    return if is_readonly?(block_definition.workflow_definition, block_definition)

    if block_definition.direct_task_definitions.any?
      block_definition.errors.add(:base, "Blöcke mit Aufgaben können nicht gelöscht werden. Bitte erst die Aufgaben verschieben oder löschen.")
    end

    if block_definition.errors.none?
      block_definition.destroy
    end
  end

  def self.move_block_definition(block_definition, params)
    return if is_readonly?(block_definition.workflow_definition, block_definition)

    target = block_definition.workflow_definition
    index = params.require(:index).to_i

    position = target.item_definitions[index]&.position || (target.item_definitions.max_by(&:position)&.position || 0)
    position -= 1 if index < target.item_definitions.index(block_definition)

    target.direct_task_definitions.where("position > ?", position).update_all("position = position + 1")
    target.block_definitions.where("position > ?", position).update_all("position = position + 1") if target.respond_to?(:block_definitions)

    block_definition.update(position: position + 1)
  end

  def self.add_data_item_definition(task_definition, content_item_definition_params, task_item_definition_params)
    workflow_definition = task_definition.workflow_definition

    content_item_definition = if content_item_definition_params[:id]
      workflow_definition.content_item_definitions.find(content_item_definition_params[:id])
    else
      workflow_definition.content_item_definitions.new(content_item_definition_params)
    end

    last_position = task_definition.task_item_definitions.maximum(:position) || 0
    task_item_definition = task_definition.task_item_definitions.new(
      task_item_definition_params.merge(
        workflow_definition: workflow_definition,
        content_item_definition: content_item_definition,
        position: last_position + 10
      )
    )

    task_item_definition.save if !is_readonly?(workflow_definition, task_item_definition) && content_item_definition.valid?
    task_item_definition
  end

  def self.update_data_item_definition(task_item_definition, content_item_params, task_item_params)
    return if is_readonly?(task_item_definition.workflow_definition, task_item_definition)

    if content_item_params
      task_item_definition.content_item_definition.update(content_item_params)
    end

    if task_item_params
      task_item_definition.update(task_item_params)
    end
  end

  def self.destroy_data_item_definition(task_item_definition)
    return if is_readonly?(task_item_definition.workflow_definition, task_item_definition)

    # If this is the last task_item_definition for the content_item_definition, so should delete it
    if task_item_definition.content_item_definition.task_item_definitions.count == 1
      # If the content_item_definition is a precondition for a block we should not delete anything
      if task_item_definition.content_item_definition.block_definitions.any?
        task_item_definition.errors.add(:base, "Dieses Feld wird in einem Block als Bedingung verwendet.")
        return
      end
      task_item_definition.content_item_definition.destroy
    end
    task_item_definition.destroy
  end

  def self.move_data_item_definition(task_item_definition, index)
    return if is_readonly?(task_item_definition.workflow_definition, task_item_definition)

    target = task_item_definition.task_definition

    position = target.task_item_definitions[index]&.position || (target.task_item_definitions.max_by(&:position)&.position || 0)
    position -= 1 if index < target.task_item_definitions.index(task_item_definition)

    target.task_item_definitions.where("position > ?", position).update_all("position = position + 1")
    task_item_definition.update(position: position + 1)
  end

  private_class_method def self.is_readonly?(workflow_definition, model_for_object = nil)
    model_for_object = workflow_definition if model_for_object.nil?

    if workflow_definition.system_process_definition_single_task?
      model_for_object.errors.add(:base, I18n.t("system_process_definition_single_task.errors.update"))
      return true
    end

    false
  end
end
