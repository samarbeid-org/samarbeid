class Interactors::DossierItemDefinitionInteractor
  def self.create(dossier_definition, params, user)
    last_position = dossier_definition.items.maximum(:position) || 0
    new_item = dossier_definition.items.create(params.merge(position: last_position + 1))
    if new_item
      Events::ChangedStructureEvent.create!(subject: user, object: dossier_definition)
    end
    new_item
  end

  def self.update(dossier_item_definition, params, user)
    dossier_item_definition.errors.add(:content_type, "kann nur durch eine Datentransformation aktiviert werden") if params.has_key?(:content_type)
    dossier_item_definition.errors.add(:options, "kann nur durch eine Datentransformation aktiviert werden") if params.has_key?(:options)
    dossier_item_definition.errors.add(:required, "kann nur durch eine Datentransformation aktiviert werden") if params.has_key?(:required) && params[:required]
    dossier_item_definition.errors.add(:unique, "kann nur durch eine Datentransformation aktiviert werden") if params.has_key?(:unique) && params[:unique]
    return if dossier_item_definition.errors.any?

    if dossier_item_definition.update(params)
      Events::ChangedStructureEvent.create!(subject: user, object: dossier_item_definition.definition)
    end
  end

  def self.destroy(dossier_item_definition, user)
    if dossier_item_definition.destroy
      dossier_item_definition.definition.title_fields -= [dossier_item_definition.id]
      dossier_item_definition.definition.subtitle_fields -= [dossier_item_definition.id]
      dossier_item_definition.definition.save

      Events::ChangedStructureEvent.create!(subject: user, object: dossier_item_definition.definition)
    end

    dossier_item_definition
  end

  def self.move(dossier_item_definition, index, user)
    target = dossier_item_definition.definition

    position = target.items[index]&.position || (target.items.max_by(&:position)&.position || 0)
    position -= 1 if index < target.items.index(dossier_item_definition)

    target.items.where("position > ?", position).update_all("position = position + 1")
    if dossier_item_definition.update(position: position + 1)
      Events::ChangedStructureEvent.create!(subject: user, object: dossier_item_definition.definition)
    end
  end
end
