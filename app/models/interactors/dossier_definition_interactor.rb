class Interactors::DossierDefinitionInteractor
  def self.create(params, user)
    new_dossier_definition = DossierDefinition.new(params)
    new_dossier_definition.groups = user.groups unless user.is_admin?
    if new_dossier_definition.save
      Events::CreatedEvent.create!(subject: user, object: new_dossier_definition)
    end
    new_dossier_definition
  end

  def self.update(dossier_definition, params, user)
    if params.key?(:group_ids)
      if user.is_admin?
        dossier_definition.group_ids = params[:group_ids]
        return dossier_definition if dossier_definition.errors.any?
      end
      params.extract!(:group_ids)
    end

    unless params.blank?
      if dossier_definition.update(params)
        if dossier_definition.name_previously_changed?
          Events::ChangedTitleEvent.create!(
            subject: user,
            object: dossier_definition,
            data: {new_title: dossier_definition.name, old_title: dossier_definition.name_previously_was}
          )
        end
      end
    end

    dossier_definition
  end

  def self.destroy(dossier_definition, user)
    if dossier_definition.dossiers.any?
      dossier_definition.errors.add(:base, "Die Vorlage kann erst gelöscht werden, wenn alle dazugehörigen Dossiers gelöscht wurden.")
    elsif dossier_definition.destroy
      Events::DeletedEvent.create!(object: dossier_definition, subject: user)
    end
  end
end
