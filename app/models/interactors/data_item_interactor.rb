class Interactors::DataItemInteractor
  include ContributionSupport

  def self.update(task_item, new_value, content_item_lock_version, user)
    task = task_item.task
    content_item = task_item.content_item
    content_item.lock_version = content_item_lock_version

    if content_item.update(value: new_value)
      if user.is_a?(ShareLink)
        Events::ChangedDataEvent.create!(object: content_item, data: {parent_element: task, share_link: user})
      elsif user.is_a?(User)
        Events::ChangedDataEvent.create!(object: content_item, subject: user, data: {parent_element: task})
        add_contributor_if_needed(task, user)
        Interactors::TaskInteractor.update_assignee(task, user, user) if task.assignee.nil?
      end
      unlock(task_item, user)
    end
  end

  def self.lock(task_item, lock_version, user)
    content_item = task_item.content_item
    content_item.lock_with!(lock_version, user)
    MonitorChannel.broadcast_content_item_locked(content_item)
  end

  def self.unlock(task_item, user)
    content_item = task_item.content_item
    content_item.unlock_for!(user)
    MonitorChannel.broadcast_content_item_unlocked(content_item)
  end
end
