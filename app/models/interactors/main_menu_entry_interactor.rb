class Interactors::MainMenuEntryInteractor
  def self.create_many(parent_id, objects_to_create_entries_for, user)
    last_position = last_position(parent_id)

    definitions_to_add_to_menue = objects_to_create_entries_for.map do |lo|
      case lo["type"]
      when "workflow_definition"
        WorkflowDefinition.find(lo["id"])
      when "dossier_definition"
        DossierDefinition.find(lo["id"])
      end
    end

    new_menue_entries = definitions_to_add_to_menue.map do |definition_to_add|
      last_position += 1
      new_entry = MainMenuEntry.new(parent_id: parent_id, position: last_position, linked_object: definition_to_add)
      if new_entry.save
        Events::CreatedEvent.create!(subject: user, object: new_entry)
      end
      new_entry
    end

    errors = new_menue_entries.find { |entry| entry.errors.any? }&.errors

    unless errors&.any?
      Setting.main_menu_auto_add_deactivated = true
      MenuChannel.broadcast_update
    end

    errors
  end

  def self.create_sub_menu(parent_id, title, user)
    sub_menu_name = MainMenuEntry.new(parent_id: parent_id, title: title, position: last_position(parent_id) + 1)

    if sub_menu_name.save
      Events::CreatedEvent.create!(subject: user, object: sub_menu_name)
      Setting.main_menu_auto_add_deactivated = true
      MenuChannel.broadcast_update
    end

    sub_menu_name.errors
  end

  def self.update(main_menu_entry, title, user)
    if main_menu_entry.update(title: title)
      Events::ChangedTitleEvent.create!(subject: user, object: main_menu_entry, data: {new_title: title, old_title: main_menu_entry.title_previously_was})
      MenuChannel.broadcast_update
      Setting.main_menu_auto_add_deactivated = true
    end

    main_menu_entry
  end

  def self.destroy(main_menu_entry, user)
    if main_menu_entry.destroy
      Events::DeletedEvent.create!(subject: user, object: main_menu_entry)
      MenuChannel.broadcast_update
      Setting.main_menu_auto_add_deactivated = true
    end

    main_menu_entry
  end

  def self.move(main_menu_entry, parent_id, index, user)
    items = MainMenuEntry.where(parent_id: parent_id).ordered_by_position

    position = items[index]&.position || (items&.max_by(&:position)&.position || 0)
    if main_menu_entry.parent_id != parent_id
      if index < items.length
        position -= 1
      end
    elsif index < items.index(main_menu_entry)
      position -= 1
    end

    begin
      ActiveRecord::Base.transaction do
        items.where.not(position: ..position).update_all("position = position + 1") # where("position > ?", position)
        main_menu_entry.update!(position: position + 1, parent_id: parent_id)
      end
    rescue ActiveRecord::ActiveRecordError => invalid
      if main_menu_entry.errors.none?
        main_menu_entry.errors.add(:base, "An error occurred while moving the menu entry")
        invalid.record&.errors&.full_messages&.each do |msg|
          main_menu_entry.errors.add(:base, msg)
        end
      end
    end

    unless main_menu_entry.errors.any?
      Events::SimpleActionEvent.create!(subject: user, object: main_menu_entry, whats_going_on: :main_menu_entry_moved)
      MenuChannel.broadcast_update
      Setting.main_menu_auto_add_deactivated = true
    end

    main_menu_entry
  end

  private_class_method def self.last_position(parent_id)
    MainMenuEntry.where(parent_id: parent_id).maximum(:position) || 0
  end
end
