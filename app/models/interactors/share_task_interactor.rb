class Interactors::ShareTaskInteractor
  include ContributionSupport

  def self.share(task, name, user)
    share_link = ShareLink.create!(shareable: task, name: name)
    Events::SimpleActionEvent.create(subject: user, object: share_link.shareable,
      whats_going_on: {share_created: {share_link: share_link}}, notify: [share_link.shareable.assignee].compact)
  end

  def self.complete(share_link, user = nil)
    if user.nil? && share_link.shareable.is_a?(Task) && share_link.shareable.required_content_items_without_data.any?
      share_link.errors.add(:base, "Zuarbeit kann erst abgeschlossen werden, wenn alle erforderlichen Datenfelder (#{share_link.shareable.required_content_items_without_data.map { |ci| "\"#{ci.label}\"" }.to_sentence}) ausgefüllt sind.") && return
    end

    if share_link.errors.none?
      share_link.complete!

      if user.nil?
        Events::SimpleActionEvent.create(object: share_link.shareable,
          whats_going_on: {share_completed: {share_link: share_link}}, notify: [share_link.shareable.assignee].compact)
      else
        Events::SimpleActionEvent.create(subject: user, object: share_link.shareable,
          whats_going_on: {share_completed: {share_link: share_link}}, notify: [share_link.shareable.assignee].compact)
      end
    end
  end
end
