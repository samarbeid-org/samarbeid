class Interactors::CommentInteractor
  include ContributionSupport

  def self.create(html_message, time_spent_at, time_spent_in_minutes, object, user)
    cleaned_html_message, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(html_message))
    user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    mentioned_users = User.where(id: user_mention_ids)

    comment = Comment.new(message: cleaned_html_message, author: user, object: object)
    comment.message = Services::RichtextImageStorage.persist_inline_images(cleaned_html_message, comment.richtext_images)

    if comment.save
      data = {mentioned_users: mentioned_users}
      data[:time_spent_at] = ContentTypes::Date.cast(time_spent_at) unless time_spent_at.nil?
      data[:time_spent_in_minutes] = time_spent_in_minutes unless time_spent_in_minutes.nil?
      Events::CommentedEvent.create!(subject: user, comment: comment, data: data)

      if object.respond_to?(:contributors)
        add_contributor_if_needed(object, user)
      end

      MonitorChannel.broadcast_events_updated(comment.object)
      MonitorProcessStructureChannel.broadcast_process_structure_updated(comment.object) if comment.object.is_a?(Workflow) || comment.object.is_a?(Task)
    end

    comment
  end

  def self.update(event, html_message, time_spent_at, time_spent_in_minutes)
    cleaned_html_message, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(html_message))
    user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    mentioned_users = User.where(id: user_mention_ids)

    comment = event.comment

    comment.message = cleaned_html_message
    comment.message = Services::RichtextImageStorage.persist_inline_images(comment.message, comment.richtext_images)

    if comment.save
      Services::RichtextImageStorage.purge_unused_images(comment.message, comment.richtext_images)

      data = {mentioned_users: mentioned_users}
      data[:time_spent_at] = ContentTypes::Date.cast(time_spent_at) unless time_spent_at.nil?
      data[:time_spent_in_minutes] = time_spent_in_minutes unless time_spent_in_minutes.nil?
      event.update!(data: data)

      MonitorChannel.broadcast_events_updated(comment.object)
    end

    comment
  end

  def self.delete(event)
    comment = event.comment

    if comment.update(message: nil, deleted_at: Time.now)
      Services::RichtextImageStorage.purge_unused_images(comment.message, comment.richtext_images)
      event.update!(data: {mentioned_users: nil, time_spent_at: nil, time_spent_in_minutes: nil})

      MonitorChannel.broadcast_events_updated(comment.object)
      MonitorProcessStructureChannel.broadcast_process_structure_updated(comment.object)
    end

    comment
  end
end
