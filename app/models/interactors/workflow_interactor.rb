class Interactors::WorkflowInteractor
  include ContributionSupport

  def self.new(workflow_definition, user)
    workflow = workflow_definition.build_workflow
    workflow.assignee = user
    workflow
  end

  def self.create(workflow_definition, params, field_data, predecessor_workflow, user, via = nil)
    workflow = workflow_definition.build_workflow(field_data)

    if workflow_definition.deactivated?
      workflow.errors.add(:base, "Aus einer deaktivierten Vorlage kann kein Prozess erstellt werden.")
      return workflow
    end

    if workflow_definition.system_process_definition_single_task?
      unless params.key?(:title)
        workflow.errors.add(:title, "Der Titel der Aufgabe darf nicht leer sein.")
        return workflow
      end

      single_task = workflow.direct_tasks.first
      single_task.name = params[:title]
      single_task.assignee_id = params[:assignee_id] if params.key?(:assignee_id)
    else
      params.extract!(:title) if workflow.workflow_definition.auto_generate_title?
      workflow.attributes = params
    end

    unless workflow.direct_tasks.any? || workflow.blocks.reduce(false) { |res, block| res || block.direct_tasks.any? }
      workflow.errors.add(:base, "Aus der Vorlage kann kein Prozess erstellt werden, da diese keine Aufgaben enthält.")
      return workflow
    end

    workflow.triggered_by = user

    if workflow.without_reindexing { workflow.save }
      if via == :api
        Events::SimpleActionEvent.create!(subject: user, object: workflow, whats_going_on: :process_created_via_api, notify: :subject)
      elsif via.is_a?(Automation)
        Events::StartedEvent.create!(object: workflow, via: via)
      else
        Events::StartedEvent.create!(subject: user, object: workflow)
      end

      field_data&.each do |key, _|
        content_item = workflow.content_items.find { |ci| ci.content_item_definition.id.to_s == key.to_s }
        Events::ChangedDataEvent.create!(object: content_item, subject: user)
      end

      if workflow_definition.system_process_definition_single_task?
        single_task = workflow.direct_tasks.first
        unless single_task.assignee.nil?
          Events::AssignedEvent.create!(subject: user, object: single_task, data: {new_assignee: single_task.assignee, old_assignee: nil})
          add_contributor_if_needed(single_task, single_task.assignee)
        end
        add_contributor_if_needed(single_task, user)
      else
        unless workflow.assignee.nil?
          Events::AssignedEvent.create!(subject: user, object: workflow, data: {new_assignee: workflow.assignee, old_assignee: nil})
          add_contributor_if_needed(workflow, workflow.assignee)
        end
        add_contributor_if_needed(workflow, user)
      end

      workflow.all_tasks.each { |task| Events::CreatedEvent.create!(object: task) }

      if predecessor_workflow
        workflow.update(predecessor_workflow: predecessor_workflow)
        Events::AddedSuccessorWorkflowEvent.create!(subject: user, object: predecessor_workflow, data: {successor_workflow: workflow})
      end

      if workflow.all_tasks.count == 1 && !workflow_definition.system_process_definition_single_task?
        workflow.all_tasks.each do |task|
          Interactors::TaskInteractor.update_assignee(task, workflow.assignee, user) unless task.assignee.present?
        end
      end

      workflow.reindex_with_tasks
    end

    workflow
  end

  def self.cancel(workflow, user)
    return if workflow.completed?

    workflow.tasks_ordered_in_structure.each do |task|
      if task.workflow_or_block.is_a?(Block)
        next unless (task.workflow_or_block.condition_valid? && !task.closed?) || (!task.workflow_or_block.condition_valid? && !task.closed_or_created?) # rubocop:todo Style/UnlessLogicalOperators
      end

      if task.may_skip?
        task.skip!(user, :skip_engine)
        Events::SkippedEvent.create!(object: task, subject: user)
      end
    end

    Events::SkippedEvent.create!(object: workflow, subject: user)

    workflow.align_state
  end

  def self.delete(workflow, user)
    if workflow.destroy
      Events::DeletedEvent.create!(object: workflow, subject: user)
    end
  end

  def self.update_assignee(workflow, assignee, user)
    old_assignee = workflow.assignee

    if old_assignee != assignee
      if workflow.update(assignee: assignee)
        if assignee.nil?
          Events::UnassignedEvent.create!(subject: user, object: workflow, data: {old_assignee: old_assignee})
        else
          Events::AssignedEvent.create!(subject: user, object: workflow, data: {new_assignee: assignee, old_assignee: old_assignee})
          add_contributor_if_needed(workflow, assignee)
        end

        add_contributor_if_needed(workflow, user)
      end
    end
  end

  def self.update_contributors(workflow, contributors, user)
    to_add = contributors - workflow.contributors
    to_remove = workflow.contributors - contributors

    if to_add.any? || to_remove.any?
      workflow.contributors = contributors

      if workflow.valid?
        to_add.each do |contributor|
          Events::AddedContributorEvent.create!(subject: user, object: workflow, data: {added_contributor: contributor})
        end

        to_remove.each do |contributor|
          Events::RemovedContributorEvent.create!(subject: user, object: workflow, data: {removed_contributor: contributor})
        end

        add_contributor_if_needed(workflow, user) unless to_remove.include?(user)
      end
    end
  end

  def self.update(workflow, params, user)
    old_title = workflow.title
    old_description = workflow.description

    params.extract!(:title) if workflow.workflow_definition.auto_generate_title?

    workflow.title = params[:title] if params.key?(:title)
    if params.key?(:description) && params[:description].present?
      workflow.description, mentions = Services::Mentioning.extract_mentions(Services::EditorSanitizer.sanitize(params[:description]))
      user_mention_ids = Services::Mentioning.get_user_mentions(mentions)
    end

    if workflow.changed? && workflow.save
      if workflow.saved_change_to_title?
        Events::ChangedTitleEvent.create!(subject: user, object: workflow, data: {new_title: workflow.title, old_title: old_title})
      end

      if workflow.saved_change_to_description?
        old_user_mention_ids = Services::Mentioning.get_user_mentions(Services::Mentioning.get_mentions(old_description))
        user_mention_ids -= old_user_mention_ids
        mentioned_users = User.where(id: user_mention_ids)

        Events::ChangedSummaryEvent.create!(subject: user, object: workflow, data: {new_summary: workflow.description, old_summary: old_description, mentioned_users: mentioned_users})
        mentioned_users.each { |mu| add_contributor_if_needed(workflow, mu) }
      end

      add_contributor_if_needed(workflow, user)
    end
  end

  def self.add_task(workflow, add_task_params, user)
    last_position = workflow.items.max_by(&:position)&.position || 0
    task = workflow.direct_tasks.create(name: add_task_params[:name], workflow: workflow, position: last_position + 10)

    if task.valid?
      Events::CreatedEvent.create!(subject: user, object: task)
      default_content_items = [
        {
          label: I18n.t("system_process_definition_single_task.task.items.description"),
          content_type: :richtext
        },
        {
          label: I18n.t("system_process_definition_single_task.task.items.attachments"),
          content_type: :file,
          options: {multiple: true}
        }
      ]
      default_content_items.each_with_index do |content_item_attributes, count|
        content_item = task.workflow.content_items.create!(content_item_attributes)
        task.task_items.create!(content_item: content_item, position: count + 10)
      end

      assignee = User.find_by(id: add_task_params[:assignee_id])
      Interactors::TaskInteractor.update_assignee(task, assignee, user) if assignee

      workflow.align_state if workflow.completed?

      Interactors::TaskInteractor.start(task, user) # manually created tasks are automatically started
      task.reload # XXX: #709 required for aasm, otherwise state_updated_at would be nil -> error in browser console

      if workflow.system_process_single_task?
        update_assignee(workflow, user, user)
        workflow.update!(system_identifier: nil)
      end

      MonitorProcessStructureChannel.broadcast_process_structure_updated(workflow)
    end

    task
  end

  def self.add_block(workflow, add_block_params)
    last_position = workflow.items.max_by(&:position)&.position || 0
    block = workflow.blocks.create(add_block_params.merge({position: last_position + 10}))

    if block.valid?
      workflow.update!(system_identifier: nil) if workflow.system_process_single_task?

      MonitorProcessStructureChannel.broadcast_process_structure_updated(workflow)
    end

    block
  end
end
