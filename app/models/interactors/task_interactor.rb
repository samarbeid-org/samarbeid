class Interactors::TaskInteractor
  include ContributionSupport

  def self.update(task, params, lock_version, user)
    task.lock_version = lock_version
    old_name = task.name
    if params.key?(:name)
      task.name = params[:name]
    end

    if task.changed? && task.save
      if task.saved_change_to_name?
        Events::ChangedTitleEvent.create!(subject: user, object: task, data: {new_title: task.name, old_title: old_name})
      end

      add_contributor_if_needed(task, user)
    end
  end

  def self.update_assignee(task, assignee, user, triggered_by = user)
    old_assignee = task.assignee
    if old_assignee != assignee
      if task.update(assignee: assignee)
        if assignee.nil?
          Events::UnassignedEvent.create!(subject: user, object: task, data: {old_assignee: old_assignee}, triggered_by: triggered_by)
        else
          Events::AssignedEvent.create!(subject: user, object: task, data: {new_assignee: assignee, old_assignee: old_assignee}, triggered_by: triggered_by)
          add_contributor_if_needed(task, assignee)
        end

        add_contributor_if_needed(task, user)
      end
    end
  end

  def self.update_contributors(task, contributors, user, triggered_by = user)
    to_add = contributors - task.contributors
    to_remove = task.contributors - contributors

    if to_add.any? || to_remove.any?
      task.contributors = contributors

      if task.valid?
        to_add.each do |contributor|
          Events::AddedContributorEvent.create!(subject: user, object: task, data: {added_contributor: contributor}, triggered_by: triggered_by)
        end

        to_remove.each do |contributor|
          Events::RemovedContributorEvent.create!(subject: user, object: task, data: {removed_contributor: contributor}, triggered_by: triggered_by)
        end

        add_contributor_if_needed(task, user) unless to_remove.include?(user)
      end
    end
  end

  def self.update_marked(task, marked, user)
    if marked === !task.marked?(user)
      if marked
        task.marked_by_users << user
      else
        task.marked_by_users.delete(user)
      end
    end
  end

  def self.update_due_at(task, due_at, user)
    due_at = nil if due_at&.empty?
    old_due_at = task.due_at

    if old_due_at != due_at
      if task.update(due_at: due_at)
        Events::ChangedDueDateEvent.create!(subject: user, object: task, data: {new_due_date: due_at&.to_date, old_due_date: old_due_at})

        add_contributor_if_needed(task, user)
      end
    end
  end

  def self.update_time_tracking_budget(task, budget_in_minutes, user)
    old_budget_in_minutes = task.time_tracking_budget_in_minutes

    if old_budget_in_minutes != budget_in_minutes
      if task.update(time_tracking_budget_in_minutes: budget_in_minutes)
        Events::ChangedTimeTrackingBudgetEvent.create!(subject: user, object: task, data: {
          new_budget_in_minutes: (budget_in_minutes == 0) ? nil : budget_in_minutes,
          old_budget_in_minutes: (old_budget_in_minutes == 0) ? nil : old_budget_in_minutes
        })

        add_contributor_if_needed(task, user)
      end
    end
  end

  def self.start(task, user)
    task.errors.add(:base, "Während Du diese Aufgabe bearbeitet hast, hat ein:e andere:r Nutzer:in den Status der Aufgabe geändert. Daher kannst Du die Aufgabe jetzt nicht starten. Bitte lade die Seite neu.") && return unless task.may_start?
    update_assignee(task, user, user) if task.assignee.nil? && task.get_best_candidate_assignee.nil?

    if task.start!
      Events::StartedEvent.create!(subject: user, object: task)
      add_contributor_if_needed(task, user)
    end
  end

  def self.snooze(task, start_at, user)
    # Unsnooze
    if start_at.blank? && task.may_start?
      start(task, user)
      return
    end

    task.errors.add(:base, "Ein Zurückstelldatum muss angegeben werden") && return unless start_at.present?
    task.errors.add(:base, "Das Zurückstelldatum muss in der Zukunft liegen") && return if start_at.present? && Date.parse(start_at) <= Date.current
    task.errors.add(:base, "Nur aktive Aufgaben können zurückgestellt werden") && return unless task.snoozed? || task.may_snooze?

    if task.snoozed? || task.snooze!
      task.update!(start_at: start_at)
      add_contributor_if_needed(task, user)
      Events::SnoozedEvent.create!(subject: user, object: task, data: {snoozed_until: task.start_at&.to_date})
    end
  end

  def self.complete(task, user)
    task.errors.add(:base, "Aufgabe kann erst abgeschlossen werden, wenn alle erforderlichen Datenfelder (#{task.required_content_items_without_data.map { |ci| "\"#{ci.label}\"" }.to_sentence}) ausgefüllt sind.") && return unless task.completable?
    task.errors.add(:base, "Die Aufgabe kann nicht abgeschlossen werden, da andere Nutzer:innen gerade Datenfelder der Aufgabe bearbeiten.") && return unless task.task_items.map(&:content_item).uniq.none?(&:locked?)
    task.errors.add(:base, "Während Du diese Aufgabe bearbeitet hast, hat ein:e andere:r Nutzer:in den Status der Aufgabe geändert. Daher kannst Du die Aufgabe jetzt nicht abschließen. Bitte lade die Seite neu.") && return unless task.may_complete?
    update_assignee(task, user, user) if task.assignee.nil?
    if task.complete!(user)
      task.task_items.map(&:content_item).uniq.each { |ci| MonitorChannel.broadcast_content_item_updated(ci) }
      Events::CompletedEvent.create!(subject: user, object: task)
      add_contributor_if_needed(task, user)
    end
  end

  def self.reopen(task, user)
    task.errors.add(:base, "Während Du diese Aufgabe bearbeitet hast, hat ein:e andere:r Nutzer:in den Status der Aufgabe geändert. Daher kannst Du die Aufgabe jetzt nicht wiedereröffnen. Bitte lade die Seite neu.") && return unless task.may_reopen?
    update_assignee(task, user, user) if task.assignee.nil?
    task.reopen!
    Events::ReopenedEvent.create!(object: task, subject: user)
  end

  def self.skip(task, user)
    task.errors.add(:base, "Die Aufgabe kann nicht übersprungen werden, da andere Nutzer:innen gerade Datenfelder der Aufgabe bearbeiten.") && return unless task.task_items.map(&:content_item).uniq.none?(&:locked?)
    task.errors.add(:base, "Während Du diese Aufgabe bearbeitet hast, hat ein:e andere:r Nutzer:in den Status der Aufgabe geändert. Daher kannst Du die Aufgabe jetzt nicht überspringen. Bitte lade die Seite neu.") && return unless task.may_skip?
    task.skip!
    Events::SkippedEvent.create!(object: task, subject: user)
  end

  def self.delete(task, user)
    if task.workflow.all_tasks.count == 1
      task.errors.add(:base, "Die letzte Aufgabe eines Prozesses kann nicht gelöscht werden.") && return
    end

    if task.task_items.map(&:content_item).any? { |ci| ci.blocks.any? }
      task.errors.add(:base, "Diese Aufgabe wird noch als Bedingung in mindestens einem Block genutzt. Sie kann daher nicht gelöscht werden.") && return
    end

    if task.destroy
      task.workflow.content_items.select { |ci| ci.task_items.none? }.each(&:destroy!)
      Events::DeletedEvent.create!(object: task, subject: user, triggered_by: user)
      Services::NewEngine.new(task.workflow).run(user)

      MonitorProcessStructureChannel.broadcast_process_structure_updated(task)
    end
  end

  def self.add_data_field(task, content_item_params, task_item_params = {}, user)
    content_item = task.workflow.content_items.find(content_item_params[:id]) if content_item_params[:id].present?
    content_item ||= task.workflow.content_items.create(content_item_params)

    if content_item.valid?
      last_position = task.task_items.maximum(:position) || 0
      @task_item = task.task_items.create(task_item_params.merge(content_item: content_item, position: last_position + 10))
    end

    if @task_item&.valid?
      Events::SimpleActionEvent.create!(subject: user, object: task, whats_going_on: :added_datafield_to_task)
      add_contributor_if_needed(task, user)
    end

    content_item
  end

  # call with empty target_process to test if task is moveable
  def self.move_to_other_process(task, target_process, user)
    move_service = Services::MoveTask.new(task)
    if move_service.valid?
      unless target_process.nil?
        raise CanCan::AccessDenied unless user.can?(:manage, target_process)

        move_service.move_to!(target_process)

        event = Events::SimpleActionEvent.new(subject: user, object: task,
          notify: [task.assignee, target_process.assignee, move_service.origin_workflow.assignee].compact)
        event.whats_going_on = if move_service.origin_workflow.system_process_single_task?
          :moved_single_task_to_other_process
        elsif move_service.origin_workflow.destroyed?
          {moved_last_task_to_other_process: {process: move_service.origin_workflow}}
        else
          {moved_task_to_other_process: {process: move_service.origin_workflow}}
        end
        event.save!
      end
    else
      task.errors.add(:base, move_service.errors.join(", "))
    end

    task
  end
end
