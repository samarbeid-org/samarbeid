class Interactors::DossierInteractor
  def self.new(dossier_definition)
    dossier_definition.build_dossier(required_and_recommended_items_only: true)
  end

  def self.create(dossier_definition, field_data, user)
    dossier = dossier_definition.build_dossier(field_data)
    if dossier.save
      Events::CreatedEvent.create!(subject: user, object: dossier)
    else
      # TODO: Remove field errors and handle dossier_item value errors instead in frontend
      dossier.dossier_items.each do |dossier_item|
        dossier_item.errors.each { |error| dossier.errors.add("field_#{dossier_item.dossier_item_definition.id}", error.message) }
      end
    end

    dossier
  end

  def self.update(dossier, dossier_item_definition_id, value, lock_version, user)
    dossier_item = dossier.dossier_items.find_or_initialize_by(dossier_item_definition_id: dossier_item_definition_id)
    dossier_item.lock_version = lock_version if dossier_item.persisted?

    begin
      dossier_item.value = value
      if dossier_item.save
        Events::ChangedDataEvent.create!(object: dossier_item, subject: user)
      else
        # TODO: Remove field errors and handle dossier_item value errors instead in frontend
        dossier_item.errors.each { |error| dossier.errors.add("field_#{dossier_item.dossier_item_definition.id}", error.message) }
      end
    rescue ActiveRecord::StaleObjectError
      dossier.errors.add(:base, I18n.t("errors.stale_object"))
    end

    dossier
  end

  def self.delete(dossier, user)
    if dossier.destroy
      Events::DeletedEvent.create!(object: dossier, subject: user)
    end
  end
end
