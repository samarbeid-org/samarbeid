class Group < ApplicationRecord
  include ReferenceSupport
  include SearchForList
  include FilterSupport::GroupFilterSupport

  pg_search_scope :search_for_list_with_pg_search, against: [:name],
    using: {
      tsearch: {prefix: true}
    }

  has_many :user_groups, dependent: :destroy
  has_many :users, through: :user_groups, inverse_of: :groups, dependent: :destroy

  has_and_belongs_to_many :workflow_definitions
  has_and_belongs_to_many :dossier_definitions

  enum system_identifier: [:system_group_all].map { |entry| [entry.to_sym, entry.to_s] }.to_h

  validates :name, presence: true, uniqueness: true
  validates :system_identifier, uniqueness: true, allow_nil: true, if: :system_group_all?

  def reference_label(noaccess = nil)
    "@#{noaccess ? id : name}"
  end

  def mention_label(noaccess = false)
    reference_label(noaccess)
  end

  def self.system_group_all
    create_with(name: I18n.t("system_group_all.name"), description: I18n.t("system_group_all.description"), user_ids: User.all.map(&:id))
      .find_or_create_by!(system_identifier: :system_group_all)
  end
end
