class DataItem
  attr_reader :item_definition_id, :name, :content_type, :options, :value, :required
  attr_accessor :recommended, :unique, :lock_version, :last_updated, :last_updated_by

  def initialize(item_definition_id, name, value, content_type, options, required)
    @item_definition_id = item_definition_id
    @name = name
    @value = value
    @content_type = content_type
    @options = options
    @required = required
  end

  def id
    @item_definition_id
  end
end
