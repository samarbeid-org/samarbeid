class Api::MenuEntriesController < ApiController
  load_and_authorize_resource

  def index
    base_query = MenuEntry.with_location_position
    @result = MenuEntry.filter_for_index(current_user, params[:query], order_by: params[:order] || :location_position, page: params[:page], per_page: params[:per_page], apply_to: base_query)
  end

  def show
  end

  def create
    @menu_entry = MenuEntry.create(menu_entry_params)

    api_response_with(view: :show, errors: @menu_entry.errors)
  end

  def update
    @menu_entry.update(menu_entry_params)

    api_response_with(view: :show, errors: @menu_entry.errors)
  end

  def destroy
    @menu_entry.destroy

    api_response_with(status: :ok, errors: @menu_entry.errors)
  end

  def locations
    @locations = MenuEntry.locations.keys
  end

  private

  def menu_entry_params
    params.require(:menu_entry).permit(:title, :location, :position, :page_id, :url, :new_window)
  end
end
