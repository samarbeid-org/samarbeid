class Api::TasksController < ApiController
  load_and_authorize_resource

  def index
    assignee_ids = Array(params[:assignee_ids]) || []
    contributor_ids = Array(params[:contributor_ids]) || []
    workflow_assignee_ids = Array(params[:workflow_assignee_ids]) || []

    workflow_definition_id = params[:workflow_definition_id]
    @workflow_definition = (workflow_definition_id == "none") ? WorkflowDefinition.system_process_definition_single_task : WorkflowDefinition.find(workflow_definition_id) if workflow_definition_id

    workflow_definition_ids = workflow_definition_id ? [workflow_definition_id] : (Array(params[:workflow_definition_ids]) || [])
    filter_single_tasks = workflow_definition_ids.include?("singletask")
    filter_without_user_defined_template = workflow_definition_ids.include?("none")
    filter_with_user_defined_template = workflow_definition_ids.include?("any")
    workflow_definition_ids = workflow_definition_ids.reject { |value| value === "singletask" || value === "none" || value === "any" }.map(&:to_i)

    task_definition_ids = @workflow_definition ? (Array(params[:task_definition_ids]) || []) : []

    fields = params[:fields] || []

    @filter_category = params[:filter_category] || "ACTIVE"

    is_export = params[:format].eql?("xlsx")

    @users = User.reorder_by_name_with_user_first(current_user)

    unless @workflow_definition
      @workflow_definitions = WorkflowDefinition.activated.not_system_process_definition_single_task.accessible_by(current_user.build_ability(true))
      @workflow_definitions = @workflow_definitions.or(WorkflowDefinition.where(id: workflow_definition_ids)) if current_user.is_admin?
      @workflow_definitions = @workflow_definitions.order(name: :asc).to_a
    end

    base_query = Task.all
    base_query = base_query.where(assignee_id: convert_null_to_nil(assignee_ids)) if assignee_ids.any?
    base_query = base_query.joins(:contributors).where(contributors: {id: convert_null_to_nil(contributor_ids)}) if contributor_ids.any?
    base_query = base_query.joins(:workflow).where(workflows: {assignee_id: convert_null_to_nil(workflow_assignee_ids)}) if workflow_assignee_ids.any?
    base_query = base_query.where(task_definition_id: task_definition_ids) if task_definition_ids.any?

    if fields.any?
      fields.each_with_index do |field, index|
        content_item_definition = ContentItemDefinition.find(field[:id])
        if content_item_definition
          base_query = base_query.with_item_value_filter(content_item_definition, field[:comp].to_sym, field[:values], filter_index: index)
        end
      end
    end

    if filter_single_tasks || filter_without_user_defined_template || filter_with_user_defined_template || workflow_definition_ids.any?
      base_query = base_query.scoping do
        or_queries = Task.none
        or_queries = or_queries.or(Task.joins(workflow: :workflow_definition).merge(Workflow.system_process_single_task)) if filter_single_tasks
        or_queries = or_queries.or(Task.joins(workflow: :workflow_definition).merge(Workflow.not_system_process_single_task).merge(WorkflowDefinition.system_process_definition_single_task_scope)) if filter_without_user_defined_template
        or_queries = or_queries.or(Task.joins(workflow: :workflow_definition).merge(Workflow.not_system_process_single_task).merge(WorkflowDefinition.not_system_process_definition_single_task)) if filter_with_user_defined_template
        or_queries = or_queries.or(Task.joins(workflow: :workflow_definition).merge(WorkflowDefinition.where(id: workflow_definition_ids))) if workflow_definition_ids.any?
        or_queries
      end
    end

    act_as_normal_user = workflow_definition_ids.empty? || !current_user.is_admin?
    @result = Task.filter_for_index(current_user, params[:query], filter_category: @filter_category,
      order_by: params[:order] || :created_at_desc, page: is_export ? nil : params[:page], per_page: params[:per_page] || 15,
      act_as_normal_user: act_as_normal_user, apply_to: base_query, for_export: is_export)

    if is_export
      send_data Services::WorkflowsToTable.new(@result.query.map(&:workflow).uniq).to_excel, filename: "tasks-#{DateTime.now.strftime("%Y-%m-%d--%H-%M-%S")}.xlsx"
    end
  end

  def list
    excluded_ids = params[:except].to_a
    limited_to_ids = params[:ids].to_a

    @tasks = Task.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {id: :desc}, page: params[:page], per_page: params[:per_page]
    )
  end

  def show
  end

  def update
    begin
      Interactors::TaskInteractor.update(@task, task_params, params.require(:lock_version), current_user)
    rescue ActiveRecord::StaleObjectError
      api_response_with(status: :conflict) and return
    end

    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_assignee
    Interactors::TaskInteractor.update_assignee(@task, User.find_by(id: params.fetch(:assignee)), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_contributors
    user_ids = params.fetch(:contributors).filter { |c| c.is_a?(Numeric) } |
      params.fetch(:contributors).filter { |c| c.is_a?(ActionController::Parameters) && c[:type] == "user" && c.has_key?(:id) }.map { |c| c[:id] }

    group_ids = params.fetch(:contributors).filter { |c| c.is_a?(ActionController::Parameters) && c[:type] == "group" && c.has_key?(:id) }.map { |c| c[:id] }

    Interactors::TaskInteractor.update_contributors(
      @task,
      User.includes(:groups).activated.where(groups: {id: group_ids}).or(User.activated.where(id: user_ids)).distinct,
      current_user
    )
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_marked
    Interactors::TaskInteractor.update_marked(@task, params.fetch(:marked), current_user)
    api_response_with(status: :ok, errors: @task.errors)
  end

  def update_due_date
    Interactors::TaskInteractor.update_due_at(@task, params.fetch(:due_at), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def update_time_tracking_budget
    Interactors::TaskInteractor.update_time_tracking_budget(@task, params.fetch(:budget_in_minutes), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def snooze
    Interactors::TaskInteractor.snooze(@task, params.fetch(:snooze_until), current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def start
    Interactors::TaskInteractor.start(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def complete
    Interactors::TaskInteractor.complete(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def skip
    Interactors::TaskInteractor.skip(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def reopen
    Interactors::TaskInteractor.reopen(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def move
    target_type = params[:target_type]
    target_id = params[:target_id]
    case target_type
    when "workflow"
      target = Workflow.find_by(id: target_id)
    when "block"
      target = Block.find_by(id: target_id)
    end
    target ||= @task.workflow_or_block
    unless target == @task.workflow || target.workflow == @task.workflow
      api_response_with(status: :unprocessable_entity) # TODO: add error message: "Cannot move task to different workflow"
      return
    end
    index = params.require(:index).to_i

    position = target.items[index]&.position || (target.items.max_by(&:position)&.position || 0)

    if target.items.index(@task).nil? # Move to new target
      if index < target.items.length # If not move to end of target items
        position -= 1
      end
    elsif index < target.items.index(@task)
      position -= 1 # Move to lower index
    end

    target.direct_tasks.where("position > ?", position).update_all("position = position + 1")
    target.blocks.where("position > ?", position).update_all("position = position + 1") if target.respond_to?(:blocks)

    @task.update(position: position + 1, workflow_or_block: target)
    # TODO: reindex source workflow and target workflow (and task?)

    @workflow = @task.workflow
    MonitorProcessStructureChannel.broadcast_process_structure_updated(@task) if @task.errors.none?
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def clone
    cloned_task = Services::ClonedTask.new(@task).create

    unless cloned_task.errors.any?
      Events::CreatedEvent.create!(subject: current_user, object: cloned_task)
      @task.workflow.update!(system_identifier: nil) if @task.workflow.system_process_single_task?
    end

    @workflow = @task.workflow
    MonitorProcessStructureChannel.broadcast_process_structure_updated(@task) if cloned_task.errors.none?
    api_response_with(view: "api/workflows/show", errors: cloned_task.errors)
  end

  def destroy
    Interactors::TaskInteractor.delete(@task, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def test_move_to_process
    Interactors::TaskInteractor.move_to_other_process(@task, nil, current_user)

    if @task.errors.none?
      api_response_with(object: {origin_workflow_deletion: @task.workflow.all_tasks.count == 1 && !@task.workflow.system_process_single_task?})
    else
      api_response_with(errors: @task.errors)
    end
  end

  def move_to_process
    @workflow = Workflow.find(params.require(:workflow_id))
    Interactors::TaskInteractor.move_to_other_process(@task, @workflow, current_user)

    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def create_share
    Interactors::ShareTaskInteractor.share(@task, params[:name], current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def complete_share
    share_link = @task.share_links.active.first
    Interactors::ShareTaskInteractor.complete(share_link, current_user)
    @workflow = @task.workflow
    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  private

  def task_params
    params.require(:task).permit(:name)
  end
end
