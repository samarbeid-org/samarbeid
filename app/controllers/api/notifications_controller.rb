class Api::NotificationsController < ApiController
  load_and_authorize_resource

  def index
    load_notifications_and_counts
  end

  def delivered
    notifications = current_user.notifications.undelivered.where(id: params.require(:notification_ids))
    notifications.deliver_all
    api_response_with(status: :ok)
  end

  def read
    notifications = current_user.notifications.unread.where(id: params.require(:notification_ids))
    notifications.read_all
    api_response_with(status: :ok)
  end

  def read_all
    notifications = current_user.notifications.unread
    notifications.read_all
    api_response_with(status: :ok)
  end

  def done
    @notification.done!
    load_notifications_and_counts unless @notification.errors.any?
    api_response_with(view: :index, errors: @notification.errors)
  end

  def bookmark
    @notification.bookmark!
    load_notifications_and_counts unless @notification.errors.any?
    api_response_with(view: :index, errors: @notification.errors)
  end

  def unbookmark
    @notification.unbookmark!
    load_notifications_and_counts(params[:filter_category] || "BOOKMARKED") unless @notification.errors.any?
    api_response_with(view: :index, errors: @notification.errors)
  end

  private

  def load_notifications_and_counts(filter_category = params[:filter_category] || "NEW")
    @result = Notification.filter_for_index(current_user, order_by: :created_at_desc, page: params[:page], per_page: 25,
      filter_category: filter_category, act_as_normal_user: true)
  end
end
