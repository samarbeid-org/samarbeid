class Api::EventsController < ApiController
  before_action :load_and_authorize_object, only: [:index, :time_tracking, :create_comment]
  skip_authorization_check only: :journal

  def index
    @events = Event.for_object(@object)
  end

  def time_tracking
    @events = Events::CommentedEvent.for_object(@object).with_time_tracking.sort_by(&:time_spent_at)
  end

  def journal
    page = params[:page]&.to_i || 1
    per_page = 25
    limit = 2_000
    types = params[:types]
    subject_ids = params[:subject_ids]

    @events = []
    events = Event.all
    events = events.where(type: types) if types.present?
    events = events.where(subject_id: convert_null_to_nil(subject_ids)) if subject_ids.present?
    events.includes(:object).find_each(order: :desc) do |event|
      break if @events.count >= page * per_page
      break if @events.count >= limit

      @events << event if current_user.can?(:read, event.object)
    end

    @events = @events[(page - 1) * per_page, per_page] || []

    @event_types = Events.classes.sort_by { |el| el.model_name.human }
    @users = User.reorder_by_name_with_user_first(current_user)
    max_total_pages = (events.count / per_page) + 1
    @total_pages = [max_total_pages, (limit / per_page)].min
  end

  def create_comment
    comment = Interactors::CommentInteractor.create(
      params.require(:message),
      params.fetch(:time_spent_at, nil),
      params.fetch(:time_spent_in_minutes, nil),
      @object,
      current_user
    )
    api_response_with(status: :ok, errors: comment.errors)
  end

  def update_comment
    event = Event.find(params[:id])
    authorize!(:update, event.comment)

    comment = Interactors::CommentInteractor.update(
      event,
      params.require(:message),
      params.fetch(:time_spent_at, nil),
      params.fetch(:time_spent_in_minutes, nil)
    )
    api_response_with(status: :ok, errors: comment.errors)
  end

  def delete_comment
    event = Event.find(params[:id])
    authorize!(:destroy, event.comment)

    comment = Interactors::CommentInteractor.delete(event)
    api_response_with(status: :ok, errors: comment.errors)
  end

  def obsolete_info
    @event = Event.find(params[:id])
    authorize!(:read, @event.object)
    @event.notifications.find_by(user: current_user)&.read!
  end

  private

  def load_and_authorize_object
    @object = GlobalID::Locator.locate params.require(:object_gid)
    authorize!(:read, @object)
  end
end
