class Api::DashboardController < ApiController
  skip_authorization_check

  def processes
    offset = params[:offset]&.to_i || 0
    limit = params[:limit]&.to_i

    current_user.act_as_normal_user = true
    full_result = WorkflowDefinition.activated.accessible_by(current_user.ability, :build_instance).sort_by do |wd|
      [
        -(wd.system_process_definition_single_task? ? 1 : 0),
        -current_user.assigned_tasks.active.includes(:workflow).where(workflow: {workflow_definition: wd}).count,
        -current_user.assigned_tasks.includes(:workflow).where(workflow: {workflow_definition: wd}).count,
        -wd.workflows.count,
        wd.name
      ]
    end

    @total_count = full_result.count
    @result = full_result.slice(offset, limit || @total_count - offset)
  end

  def dossier_definitions
    offset = params[:offset]&.to_i || 0
    limit = params[:limit]&.to_i

    current_user.act_as_normal_user = true
    full_result = DossierDefinition.accessible_by(current_user.ability, :build_instance).sort_by do |dd|
      [
        -Events::CreatedEvent.where(subject: current_user, object: dd.dossiers).count,
        -Comment.where(author: current_user, object: dd.dossiers).count,
        -dd.dossiers.count,
        dd.name
      ]
    end

    @total_count = full_result.count
    @result = full_result.slice(offset, limit || @total_count - offset)
  end

  def everything_in_view
    current_user.act_as_normal_user = true

    @unassigned_tasks_count_of_my_active_workflows = Task.accessible_by(current_user.ability).includes(:workflow)
      .active.where(assignee: nil, workflow: {assignee: current_user, aasm_state: :active}).count

    @unassigned_active_processes_count = Workflow.accessible_by(current_user.ability)
      .active.not_system_process_single_task.where(assignee: nil).count

    # @unassigned_active_workflows_count = Workflow.accessible_by(current_user.ability).active.where(assignee: nil).count

    if current_user.is_admin?
      @hidden_workflow_definition_count = WorkflowDefinition.hidden.count
      @hidden_dossier_definition_count = DossierDefinition.hidden.count
      @unconfigured_main_menu = !Setting.main_menu_auto_add_deactivated
    end
  end

  def my_last_changes
    limit = params[:limit]&.to_i || 5

    @events = []
    Event.where(subject: current_user).find_each(order: :desc) do |event|
      break if @events.count >= limit

      deduplicate_on = [:object_id, :object_type, :class]
      @events << event unless @events.any? { |existing| existing.slice(*deduplicate_on).eql?(event.slice(*deduplicate_on)) }
    end
  end
end
