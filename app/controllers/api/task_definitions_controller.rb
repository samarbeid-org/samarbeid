class Api::TaskDefinitionsController < ApiController
  load_and_authorize_resource

  def create
    @workflow_definition = @task_definition.workflow_definition
    @task_definition = Interactors::WorkflowDefinitionInteractor.add_task_definition(@workflow_definition)
    api_response_with(view: "api/workflow_definitions/show", errors: @task_definition.errors)
  end

  def update
    Interactors::WorkflowDefinitionInteractor.update_task_definition(@task_definition, update_params)
    @workflow_definition = @task_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: @task_definition.errors)
  end

  def destroy
    Interactors::WorkflowDefinitionInteractor.destroy_task_definition(@task_definition)
    @workflow_definition = @task_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: @task_definition.errors)
  end

  def move
    Interactors::WorkflowDefinitionInteractor.move_task_definition(@task_definition, params)
    @workflow_definition = @task_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: @task_definition.errors)
  end

  def clone
    cloned_task_definition = Services::ClonedTaskDefinition.new(@task_definition).create

    @workflow_definition = @task_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: cloned_task_definition.errors)
  end

  def update_candidate_contributors
    user_ids = params.fetch(:candidate_contributors).filter { |c| c.is_a?(Numeric) } |
      params.fetch(:candidate_contributors).filter { |c| c.is_a?(ActionController::Parameters) && c[:type] == "user" && c.has_key?(:id) }.map { |c| c[:id] }

    group_ids = params.fetch(:candidate_contributors).filter { |c| c.is_a?(ActionController::Parameters) && c[:type] == "group" && c.has_key?(:id) }.map { |c| c[:id] }

    Interactors::WorkflowDefinitionInteractor.update_task_definition(@task_definition, {
      candidate_contributor_ids: User.includes(:groups).activated.where(groups: {id: group_ids}).or(User.activated.where(id: user_ids)).distinct.pluck(:id)
    })

    @workflow_definition = @task_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: @task_definition.errors)
  end

  private

  wrap_parameters TaskDefinition, include: TaskDefinition.attribute_names + [:candidate_contributor_ids]

  def create_params
    params.require(:task_definition).permit(:workflow_definition_id)
  end

  def update_params
    params.require(:task_definition).permit(:name, :candidate_assignee_id, :candidate_assignee_from_workflow, :due_in_days, :deferral_in_days, candidate_contributor_ids: [])
  end
end
