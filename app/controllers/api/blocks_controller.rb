class Api::BlocksController < ApiController
  load_and_authorize_resource

  def update
    @workflow = @block.workflow

    update_params = params.require(:block).permit(:title, :parallel, :content_item_id)
    update_params = update_params.merge({decision: params.require(:block).fetch(:decision)}) if params.require(:block).has_key?(:decision)

    if update_params[:parallel] != @block.parallel && !@block.created?
      @block.errors.add(:parallel, "kann nur direkt nach Erstellen des Blocks geändert werden")
      api_response_with(view: "api/workflows/show", errors: @block.errors)
      return
    end

    @block.update(update_params)
    MonitorProcessStructureChannel.broadcast_process_structure_updated(@block) if @block.errors.none?
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end

  def destroy
    if @block.items.any?
      @block.errors.add(:base, "Blöcke mit Aufgaben können nicht gelöscht werden. Bitte erst die Aufgaben verschieben oder löschen.")
      api_response_with(view: "api/workflows/show", errors: @block.errors)
      return
    end

    @block.destroy!

    @workflow = @block.workflow
    MonitorProcessStructureChannel.broadcast_process_structure_updated(@block) if @block.errors.none?
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end

  def move
    target = @block.workflow
    index = params.require(:index).to_i

    position = target.items[index]&.position || (target.items.max_by(&:position)&.position || 0)
    position -= 1 if index < target.items.index(@block)

    target.direct_tasks.where("position > ?", position).update_all("position = position + 1")
    target.blocks.where("position > ?", position).update_all("position = position + 1") if target.respond_to?(:blocks)

    @block.update(position: position + 1)

    @workflow = @block.workflow.reload
    MonitorProcessStructureChannel.broadcast_process_structure_updated(@block) if @block.errors.none?
    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end
end
