class Api::UsersAndGroupsController < ApiController
  skip_authorization_check

  def list
    query = params[:query]

    excluded_user_ids = params[:except].to_a.filter { |i| i.is_a?(ActionController::Parameters) && i[:type] == "user" && i.has_key?(:id) }.map { |i| i[:id] }
    excluded_group_ids = params[:except].to_a.filter { |i| i.is_a?(ActionController::Parameters) && i[:type] == "group" && i.has_key?(:id) }.map { |i| i[:id] }

    limited_to_user_ids = params[:ids].to_a.filter { |i| i.is_a?(ActionController::Parameters) && i[:type] == "user" && i.has_key?(:id) }.map { |i| i[:id] }
    limited_to_group_ids = params[:ids].to_a.filter { |i| i.is_a?(ActionController::Parameters) && i[:type] == "group" && i.has_key?(:id) }.map { |i| i[:id] }

    group_ids = params[:group_ids].to_a

    users_base_sql_query = User.activated
    users_base_sql_query = users_base_sql_query.filter_by_groups(group_ids, query.present?) if group_ids.any?
    users = User.search_for_list(current_user, query, limited_to_ids: limited_to_user_ids, excluded_ids: excluded_user_ids, apply_to: users_base_sql_query)

    groups_base_sql_query = Group.all
    groups_base_sql_query = groups_base_sql_query.where(id: group_ids) if group_ids.any?
    groups = Group.search_for_list(current_user, query, limited_to_ids: limited_to_group_ids, excluded_ids: excluded_group_ids, apply_to: groups_base_sql_query)

    @users_and_groups = (users | groups).sort_by do |user_or_group|
      [
        (user_or_group == current_user) ? 0 : 1,
        user_or_group.name
      ]
    end
    @users_and_groups = Kaminari.paginate_array(@users_and_groups).page(params[:page]).per(params[:per_page] || 10)
  end
end
