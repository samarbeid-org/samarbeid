class Api::SearchController < ApiController
  def fulltext
    authorize!(:read, Services::Search)
    page = params[:page]&.to_i || 1
    @query = params[:query]
    @search_result = Services::Search.new(current_user).fulltext(@query, {page: page})
  end
end
