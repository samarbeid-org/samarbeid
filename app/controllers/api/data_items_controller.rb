class Api::DataItemsController < ApiController
  include ::ShareLinkAuth

  skip_before_action :authenticate_user!, only: [:index, :show, :update, :lock, :unlock]
  before_action :authenticate_user_or_share_token!, only: [:index, :show, :update, :lock, :unlock]

  load_and_authorize_resource :task_item, id_param: :id

  def index
    task = Task.find(params.require(:task_id))
    authorize!(:read, task)
    @task_items = task.task_items.order(:position)

    # Temporary workaround to display webhooks in task
    @task_items = (task.webhooks.map(&:as_task_item) + @task_items) if task.webhooks.any?
  end

  def create
    task = Task.find(params.require(:task_id))
    authorize!(:read, task)

    content_item = Interactors::TaskInteractor.add_data_field(task, create_params_content_item, create_params_task_item, current_user)

    @task_items = task.task_items.order(:position)
    api_response_with(view: "index", errors: content_item.errors)
  end

  def show
  end

  def update
    if params.key?(:value)
      # update value
      begin
        Interactors::DataItemInteractor.update(@task_item, params.fetch(:value), params.require(:lock_version), current_user)
      rescue ActiveRecord::StaleObjectError
        @task_item.errors.add(:base, t("errors.stale_object"))
      end
    else
      # update definition
      raise CanCan::AccessDenied unless can?(:update_definition, @task_item)
      @task_item.update(update_params_task_item) if params[:task_item].present?
      @task_item.content_item.update(update_params_content_item) if params[:content_item].present?
    end

    @task_item.errors.merge!(@task_item.content_item.errors) if @task_item.content_item.errors.any?

    @task = @task_item.task.reload
    @workflow = @task.workflow
    api_response_with(view: "update", errors: @task_item.errors)
  end

  def destroy
    # If this is the last task_item for the content_item, so should delete it
    if @task_item.content_item.task_items.count == 1
      # If the content_item is a precondition for a block we should not delete anything
      if @task_item.content_item.blocks.any?
        @task_item.errors.add(:base, "Dieses Feld wird in einem Block als Bedingung verwendet.")
        api_response_with(errors: @task_item.errors)
        return
      end
      @task_item.content_item.destroy
    end
    @task_item.destroy

    @task_items = @task_item.task.task_items.order(:position)
    api_response_with(view: "index", errors: @task_item.errors)
  end

  def move
    target = @task_item.task
    index = params.require(:index).to_i

    position = target.task_items[index]&.position || (target.task_items.max_by(&:position)&.position || 0)
    position -= 1 if index < target.task_items.index(@task_item)

    target.task_items.where("position > ?", position).update_all("position = position + 1")
    @task_item.update(position: position + 1)

    @task_items = @task_item.task.task_items.order(:position)
    api_response_with(view: "index", errors: @task_item.errors)
  end

  def lock
    Interactors::DataItemInteractor.lock(@task_item, params.require(:lock_version), current_user)

    api_response_with(object: @task_item.content_item.lock_version, errors: @task_item.errors)
  end

  def unlock
    Interactors::DataItemInteractor.unlock(@task_item, current_user)

    api_response_with(object: @task_item.content_item.lock_version, errors: @task_item.errors)
  end

  private

  def create_params_content_item
    content_item_params = params.require(:content_item)

    res = content_item_params.permit(:id, :label, :content_type, options: {})
    res = res.merge(value: content_item_params.fetch(:value)) if content_item_params.key?(:value)
    res
  end

  def create_params_task_item
    params.require(:task_item).permit(:required, :info_box, :add_to_calendar)
  end

  def update_params_content_item
    content_item_params = params.require(:content_item)

    res = content_item_params.permit(:label, options: {})
    res = res.merge(value: content_item_params.fetch(:value)) if content_item_params.key?(:value)
    res
  end

  def update_params_task_item
    params.require(:task_item).permit(:required, :info_box, :add_to_calendar)
  end
end
