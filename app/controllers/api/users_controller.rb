class Api::UsersController < ApiController
  include ::ShareLinkAuth

  skip_before_action :authenticate_user!, only: [:ping]
  before_action :authenticate_user_or_share_token!, only: [:ping]

  load_and_authorize_resource

  def index
    filter_category = params[:filter_category] ||= "ALL"
    activation_state = params[:activation_state] ||= "activated"
    confirmation_state = params[:confirmation_state] ||= "all"

    base_query = User.with_fullname
    base_query = base_query.activated if activation_state == "activated"
    base_query = base_query.deactivated if activation_state == "deactivated"
    base_query = base_query.confirmed if confirmation_state == "confirmed"
    base_query = base_query.unconfirmed if confirmation_state == "unconfirmed"
    @result = User.filter_for_index(current_user, params[:query], filter_category: filter_category,
      order_by: params[:order] || :name_asc, page: params[:page], per_page: params[:per_page], apply_to: base_query)
  end

  def list
    excluded_ids = params[:except].to_a.filter { |i|
      begin
        true if Integer(i)
      rescue
        false
      end
    } | params[:except].to_a.filter { |i| i.is_a?(ActionController::Parameters) && i[:type] == "user" && i.has_key?(:id) }.map { |i| i[:id] }

    limited_to_ids = params[:ids].to_a.filter { |i|
      begin
        true if Integer(i)
      rescue
        false
      end
    } | params[:ids].to_a.filter { |i| i.is_a?(ActionController::Parameters) && i[:type] == "user" && i.has_key?(:id) }.map { |i| i[:id] }

    group_ids = params[:group_ids].to_a
    query = params[:query]

    base_sql_query = User.activated
    base_sql_query = base_sql_query.filter_by_groups(group_ids, query.present?) if group_ids.any?
    base_sql_query = base_sql_query.reorder_by_name_with_user_first(current_user)
    @users = User.search_for_list(
      current_user, query, limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      page: params[:page], per_page: params[:per_page], apply_to: base_sql_query
    )
  end

  def show
  end

  def create
    @user = User.create(create_params)

    Events::CreatedEvent.create!(subject: current_user, object: @user) if @user.valid?

    api_response_with(view: :show, errors: @user.errors)
  end

  def info
    @user_info = current_user.info
  end

  def ping
    data = {form_authenticity_token: form_authenticity_token}
    data[:revision] = Services::System.revision if Rails.env.in?(%w[production canary])

    api_response_with object: data, status: :ok
  end

  private

  wrap_parameters User, include: User.attribute_names + [:group_ids]

  def create_params
    params.require(:user).permit(:email, :firstname, :lastname, group_ids: [])
  end
end
