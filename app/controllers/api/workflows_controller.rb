class Api::WorkflowsController < ApiController
  load_and_authorize_resource except: [:show, :new, :create]

  def index
    assignee_ids = Array(params[:assignee_ids]) || []
    contributor_ids = Array(params[:contributor_ids]) || []
    workflow_definition_ids = Array(params[:workflow_definition_ids]) || []
    filter_category = params[:filter_category] || "ACTIVE"

    is_export = params[:format].eql?("xlsx")

    @users = User.reorder_by_name_with_user_first(current_user)
    @workflow_definitions = WorkflowDefinition.activated.not_system_process_definition_single_task.accessible_by(current_user.build_ability(true))

    base_query = Workflow.all
    base_query = base_query.where(assignee_id: convert_null_to_nil(assignee_ids)) if assignee_ids.any?
    base_query = base_query.joins(:contributors).where(contributors: {id: convert_null_to_nil(contributor_ids)}) if contributor_ids.any?
    base_query = base_query.where(workflow_definition_id: convert_null_to(workflow_definition_ids, WorkflowDefinition.system_process_definition_single_task.id)) if workflow_definition_ids.any?

    @result = Workflow.filter_for_index(current_user, params[:query], filter_category: filter_category,
      order_by: params[:order] || :created_at_desc, page: is_export ? nil : params[:page], per_page: params[:per_page],
      act_as_normal_user: true, apply_to: base_query, for_export: is_export)

    if is_export
      send_data Services::WorkflowsToTable.new(@result.query).to_excel
    end
  end

  def list
    excluded_ids = params[:except].to_a
    limited_to_ids = params[:ids].to_a

    base_sql_query = Workflow.not_system_process_single_task
    @workflows = Workflow.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {id: :desc}, page: params[:page], per_page: params[:per_page], apply_to: base_sql_query
    )
  end

  def new
    workflow_definition = WorkflowDefinition.find(params.require(:workflow_definition_id))
    @workflow = Interactors::WorkflowInteractor.new(workflow_definition, current_user)
    @task_items = @workflow.start_items
    authorize!(:new, @workflow)
    api_response_with(view: :new, errors: @workflow.errors)
  end

  def create
    workflow_definition = WorkflowDefinition.find(params.require(:workflow_definition_id))
    authorize!(:create, workflow_definition.workflows.new)

    predecessor_workflow = Workflow.find_by(id: params[:predecessor_workflow_id])

    @workflow = Interactors::WorkflowInteractor.create(
      workflow_definition,
      workflow_create_params,
      params[:field_data]&.to_unsafe_hash,
      predecessor_workflow,
      current_user
    )
    api_response_with(view: :create, errors: @workflow.errors)
  end

  def show
    if params[:from_task_id].nil?
      @workflow = Workflow.find(params[:id])
      @task = @workflow.all_tasks.find_by(id: params[:with_task]) unless params[:with_task].nil?
    else
      @task = Task.find(params[:id])
      @workflow = @task.workflow
    end

    authorize!(:show, @workflow)
  end

  def update
    Interactors::WorkflowInteractor.update(@workflow, workflow_update_params, current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def update_assignee
    Interactors::WorkflowInteractor.update_assignee(@workflow, User.find_by(id: params.fetch(:assignee)), current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def update_contributors
    Interactors::WorkflowInteractor.update_contributors(@workflow, User.where(id: params.fetch(:contributors)), current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def cancel
    Interactors::WorkflowInteractor.cancel(@workflow, current_user)
    api_response_with(view: :show, errors: @workflow.errors)
  end

  def destroy
    Interactors::WorkflowInteractor.delete(@workflow, current_user)

    api_response_with(status: :ok, errors: @workflow.errors)
  end

  def add_task
    @task = Interactors::WorkflowInteractor.add_task(@workflow, add_task_params, current_user)

    api_response_with(view: "api/workflows/show", errors: @task.errors)
  end

  def add_block
    add_block_params = params.require(:block).permit(:title, :parallel, :content_item_id)
    add_block_params = add_block_params.merge({decision: params.require(:block).fetch(:decision)}) if params.require(:block).has_key?(:decision)
    @block = Interactors::WorkflowInteractor.add_block(@workflow, add_block_params)

    api_response_with(view: "api/workflows/show", errors: @block.errors)
  end

  def content_items
    @content_items = @workflow.content_items.where.not(content_type: ContentTypes::Note.to_s)
    @content_items = @content_items.where(content_type: params[:types].select { |type| ContentType.types_hash.key?(type.to_sym) }) if params[:types]&.is_a?(Array)
  end

  def update_definition
    Interactors::WorkflowDefinitionInteractor.update_structure(@workflow, current_user)

    api_response_with(status: :ok, errors: @workflow.workflow_definition.errors)
  end

  private

  def workflow_create_params
    return {} unless params[:workflow].present?

    params.require(:workflow).permit(:title, :assignee_id)
  end

  def workflow_update_params
    params.require(:workflow).permit(:title, :description)
  end

  def add_task_params
    params.require(:task).permit(:name, :assignee_id)
  end
end
