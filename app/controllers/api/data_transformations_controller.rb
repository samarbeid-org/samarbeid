class Api::DataTransformationsController < ApiController
  before_action :load_and_authorize_object, only: [:index]

  def index
    @data_transformations = DataTransformation.where(dossier_item_definition: @object.items)
  end

  def show
    @data_transformation = DataTransformation.find(params[:id])
    authorize!(:manage, @data_transformation.dossier_item_definition)

    type = params[:type] || "READY"

    @data_transformation.create_missing_items

    load_item_counts

    @data_transformation_items = @data_transformation.data_transformation_items.order(value: :asc)

    @data_transformation_items = case type
    when "INVALID"
      @data_transformation_items.invalid
    when "UNCONFIRMED"
      @data_transformation_items.valid.unconfirmed
    else # READY
      @data_transformation_items.confirmed
    end
  end

  def create
    dossier_item_definition = DossierItemDefinition.find(params.require(:dossier_item_definition_id))
    authorize!(:manage, dossier_item_definition)

    new_data_transformation = {
      dossier_item_definition: dossier_item_definition,
      required: dossier_item_definition.required,
      unique: dossier_item_definition.unique,
      content_type: dossier_item_definition.content_type.to_s
    }.merge(create_params)

    new_data_transformation[:options] = (new_data_transformation[:content_type] != dossier_item_definition.content_type.to_s) ? {} : dossier_item_definition.options
    new_data_transformation[:options] = new_data_transformation[:options].merge(create_params[:options]) if create_params.has_key?(:options)

    @data_transformation = DataTransformation.create(new_data_transformation)

    api_response_with(view: :create, errors: @data_transformation.errors)
  end

  def update
    @data_transformation = DataTransformation.find(params[:id])
    authorize!(:manage, @data_transformation.dossier_item_definition)

    @data_transformation_item = @data_transformation.data_transformation_items.find(params.require(:item_id))
    @data_transformation_item.value = params[:value] if params.has_key?(:value)
    @data_transformation_item.confirmed = params[:confirmed] if params.has_key?(:confirmed)
    @data_transformation_item.save

    load_item_counts unless @data_transformation_item.errors.any?

    api_response_with(view: "api/data_transformations/update", errors: @data_transformation_item.errors)
  end

  def confirm_all
    @data_transformation = DataTransformation.find(params[:id])
    authorize!(:manage, @data_transformation.dossier_item_definition)

    @data_transformation.data_transformation_items.valid.unconfirmed.find_each do |dti|
      dti.update(confirmed: true)
    end

    api_response_with(status: :ok)
  end

  def destroy
    @data_transformation = DataTransformation.find(params[:id])
    authorize!(:manage, @data_transformation.dossier_item_definition)

    @data_transformation.destroy!

    api_response_with(status: :ok, errors: @data_transformation.errors)
  end

  def apply
    @data_transformation = DataTransformation.find(params[:id])
    authorize!(:manage, @data_transformation.dossier_item_definition)

    @data_transformation.apply
    api_response_with(status: :ok, errors: @data_transformation.errors)
  end

  private

  def create_params
    params.require(:data_transformation).permit(:content_type, :required, :unique, options: {})
  end

  def load_item_counts
    @data_transformation_item_counts = {
      INVALID: @data_transformation.data_transformation_items.invalid.count,
      UNCONFIRMED: @data_transformation.data_transformation_items.valid.unconfirmed.count,
      READY: @data_transformation.data_transformation_items.confirmed.count
    }
  end

  def load_and_authorize_object
    @object = GlobalID::Locator.locate params.require(:object_gid)
    authorize!(:manage, @object)
  end
end
