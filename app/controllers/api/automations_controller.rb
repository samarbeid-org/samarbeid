class Api::AutomationsController < ApiController
  load_and_authorize_resource

  def index
    state = params[:state] ||= "active"

    base_query = Automation.all
    base_query = base_query.activated if state == "activate"
    base_query = base_query.deactivated if state == "inactive"
    @result = Automation.filter_for_index(current_user, params[:query], order_by: params[:order] || :title_asc, page: params[:page], per_page: params[:per_page], apply_to: base_query)
  end

  def show
  end

  def create
    @automation.active = true
    @automation.save

    Events::CreatedEvent.create!(subject: current_user, object: @automation) unless @automation.errors.any?

    api_response_with(view: :show, errors: @automation.errors)
  end

  def validate_schedule
    @automation = Automation.validate_with_defaults(params.require(:automation).permit(schedule: {}))
    @automation.candidate_assignee = current_user
    api_response_with(view: :show, errors: @automation.errors)
  end

  def update
    @automation.update(update_params)
    api_response_with(view: :show, errors: @automation.errors)
  end

  def destroy
    @automation.destroy
    api_response_with(status: :ok, errors: @automation.errors)
  end

  private

  def create_params
    params.require(:automation).permit(:title, :workflow_definition_id, :candidate_assignee_id, schedule: {})
  end

  def update_params
    params.require(:automation).permit(:title, :active, :workflow_definition_id, :candidate_assignee_id, schedule: {})
  end
end
