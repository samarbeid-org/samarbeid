class Api::UserSettingsController < ApiController
  load_and_authorize_resource :user, parent: false, class: "User"
  skip_authorize_resource only: [:show]

  def show
    authorize! :show_settings, @user
  end

  def update_admin_status
    if ActiveRecord::Type::Boolean.new.cast(params.require(:admin))
      if @user.update(role: :team_admin)
        Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :promoted_to_admin, notify: :admins)
      end
    elsif @user.update(role: :user)
      Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :demoted_to_user, notify: :admins)
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_active_status
    if ActiveRecord::Type::Boolean.new.cast(params.require(:active))
      if @user.activate
        Events::ActivatedEvent.create!(subject: current_user, object: @user)
      end
    elsif @user.deactivate
      Events::DeactivatedEvent.create!(subject: current_user, object: @user)
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def update
    @user = current_user
    if @user.update(update_params)
      Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :user_settings_update)
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_email
    @user.validate_email_with_confirmation = true
    if @user.update(update_email_params)
      Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :user_settings_update_email)
    end
    api_response_with(view: :show, errors: @user.errors)
  end

  def update_password
    @user = current_user
    if @user.update_with_password(update_password_params)
      Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :user_settings_update_password)
      bypass_sign_in @user
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def update_notification_settings
    @user = current_user
    if @user.update_columns(noti_interval: params.require(:interval))
      Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :user_settings_update_notification_settings)
    end

    api_response_with(view: :show, errors: @user.errors)
  end

  def resend_confirmation_instructions
    if @user.resend_confirmation_instructions
      Events::SimpleActionEvent.create(subject: current_user, object: @user, whats_going_on: :user_settings_resend_confirmation_instructions) if @user.valid?
    end

    api_response_with(errors: @user.errors)
  end

  def groups
    @groups = @user.groups.order(:name)

    api_response_with(view: "api/groups/list")
  end

  private

  wrap_parameters User, include: User.attribute_names + [:password, :password_confirmation, :current_password, :avatar, :remove_avatar, :email_confirmation], format: [:json, :multipart_form]

  def update_params
    params.require(:user).permit(:firstname, :lastname, :avatar, :remove_avatar)
  end

  def update_email_params
    params.require(:user).permit(:email, :email_confirmation)
  end

  def update_password_params
    params.require(:user).permit(:password, :password_confirmation, :current_password)
  end
end
