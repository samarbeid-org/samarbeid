class Api::ShareController < ApiController
  include ::ShareLinkAuth

  skip_before_action :authenticate_user!, only: [:show, :complete]
  before_action :authenticate_user_or_share_token!, only: [:show, :complete]

  def show
    authorize! :show, @share_link_user
  end

  def complete
    authorize! :complete, @share_link_user
    Interactors::ShareTaskInteractor.complete(@share_link_user)
    api_response_with(view: :show, errors: @share_link_user.errors)
  end
end
