class Api::ContentTypesController < ApiController
  skip_authorization_check

  def index
    @content_types = ContentType.types_array
  end

  def show
    @content_type = ContentType.types_array.find { |ct| ct.type.to_s == params[:id] }
  end
end
