class Api::BlockDefinitionsController < ApiController
  load_and_authorize_resource

  def create
    @workflow_definition = @block_definition.workflow_definition
    @block_definition = Interactors::WorkflowDefinitionInteractor.add_block_definition(@workflow_definition)
    api_response_with(view: "api/workflow_definitions/show", errors: @block_definition.errors)
  end

  def update
    update_params = params.require(:block_definition).permit(:title, :parallel, :content_item_definition_id)
    update_params = update_params.merge({decision: params.require(:block_definition).fetch(:decision)}) if params.require(:block_definition).has_key?(:decision)

    Interactors::WorkflowDefinitionInteractor.update_block_definition(@block_definition, update_params)
    @workflow_definition = @block_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: @block_definition.errors)
  end

  def destroy
    Interactors::WorkflowDefinitionInteractor.destroy_block_definition(@block_definition)
    @workflow_definition = @block_definition.workflow_definition
    api_response_with(view: "api/workflow_definitions/show", errors: @block_definition.errors)
  end

  def move
    Interactors::WorkflowDefinitionInteractor.move_block_definition(@block_definition, params)
    @workflow_definition = @block_definition.workflow_definition.reload
    api_response_with(view: "api/workflow_definitions/show", errors: @block_definition.errors)
  end

  private

  def create_params
    params.require(:block_definition).permit(:workflow_definition_id)
  end
end
