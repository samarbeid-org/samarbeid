class Api::WorkflowDefinitionsController < ApiController
  load_and_authorize_resource

  def index
    state = params[:state] ||= "activated"

    base_query = WorkflowDefinition.left_joins(:groups).group(:id).order(Arel.sql("COUNT(groups.id) > 0 OR deactivated_at IS NOT NULL"))
    base_query = base_query.activated if state == "activated"
    base_query = base_query.deactivated if state == "deactivated"
    @result = WorkflowDefinition.filter_for_index(current_user, params[:query],
      order_by: params[:order] || :name_asc, page: params[:page], per_page: params[:per_page], apply_to: base_query)
  end

  def list
    excluded_ids = params[:except].to_a.map { |elem| (elem == "single_task") ? WorkflowDefinition.system_process_definition_single_task.id : elem }
    excluded_ids |= WorkflowDefinition.deactivated.map(&:id)
    limited_to_ids = params[:ids].to_a

    filter_by_groups_only = ActiveRecord::Type::Boolean.new.cast(params[:filter_by_groups_only] || false)

    @workflow_definitions = WorkflowDefinition.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {name: :asc}, page: params[:page], per_page: params[:per_page], act_as_normal_user: filter_by_groups_only
    )
  end

  def show
  end

  def show_system_process_definition_single_task
    @workflow_definition = WorkflowDefinition.system_process_definition_single_task
    authorize!(:show, @workflow_definition)

    api_response_with(view: :show)
  end

  def create
    if params[:workflow_id].nil?
      @workflow_definition = Interactors::WorkflowDefinitionInteractor.create(create_params, current_user)
    else
      workflow = Workflow.find(params[:workflow_id])
      authorize!(:read, workflow)
      @workflow_definition = Interactors::WorkflowDefinitionInteractor.create_from_instance(workflow, create_params, current_user)
    end
    api_response_with(view: :show, errors: @workflow_definition.errors)
  end

  def update
    Interactors::WorkflowDefinitionInteractor.update(@workflow_definition, update_params, current_user)
    api_response_with(view: :show, errors: @workflow_definition.errors)
  end

  def destroy
    Interactors::WorkflowDefinitionInteractor.destroy(@workflow_definition, current_user)
    api_response_with(status: :ok, errors: @workflow_definition.errors)
  end

  private

  wrap_parameters WorkflowDefinition, include: WorkflowDefinition.attribute_names + [:active, :group_ids, :start_item_definition_ids, :title_item_definition_ids]

  def create_params
    params.require(:workflow_definition).permit(:name, :description, group_ids: [])
  end

  def update_params
    params.permit(:name, :description, :auto_generate_title, :active, group_ids: [], start_item_definition_ids: [], title_item_definition_ids: [])
  end
end
