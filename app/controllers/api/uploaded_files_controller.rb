class Api::UploadedFilesController < ApiController
  include ::ShareLinkAuth

  skip_before_action :authenticate_user!, only: [:create, :update]
  before_action :authenticate_user_or_share_token!, only: [:create, :update]

  before_action :set_uploaded_file, except: :create
  before_action :check_acl, except: :create

  wrap_parameters UploadedFile, include: [:file, :title], format: [:json, :multipart_form]

  # POST /api/uploaded_files or /api/uploaded_files.json
  def create
    authorize!(:create, UploadedFile)
    create_params = params.require(:uploaded_file).permit(:file)
    @uploaded_file = UploadedFile.create(create_params)
    api_response_with(view: :show, errors: @uploaded_file.errors)
  end

  # PATCH/PUT /api/uploaded_files/1 or /api/uploaded_files/1.json
  def update
    new_title = params.require(:uploaded_file).require(:title)
    @uploaded_file.update(title: new_title)

    api_response_with(view: :show, errors: @uploaded_file.errors)
  end

  private

  def set_uploaded_file
    @uploaded_file = UploadedFile.find(params[:id])
  end

  def check_acl
    @acl_object = GlobalID::Locator.locate params[:acl_object_global_id]
    raise CanCan::AccessDenied unless @acl_object.is_a?(Dossier) || @acl_object.is_a?(Task)
    authorize!(:manage_uploads, @acl_object)

    case @acl_object.class.name
    when Task.name
      raise CanCan::AccessDenied unless @acl_object.task_items.map(&:content_item).select { |ci| ci.content_type.to_s === ContentTypes::File.to_s && ci.value&.include?(@uploaded_file) }.any?
    when Dossier.name
      # TODO: Not yet implemented
    end
  end
end
