class Api::DossiersController < ApiController
  include ::ShareLinkAuth

  skip_before_action :authenticate_user!, only: [:list]
  before_action :authenticate_user_or_share_token!, only: [:list]

  load_and_authorize_resource except: [:new, :create]

  def index
    @dossier_definition = DossierDefinition.find(params[:definition_id]) if params[:definition_id]
    definition_ids = @dossier_definition ? [@dossier_definition.id] : (Array(params[:definition_ids]) || [])
    fields = params[:fields] || []

    is_export = params[:format].eql?("xlsx")

    unless @dossier_definition
      @dossier_definitions = DossierDefinition.accessible_by(current_user.build_ability(true))
      @dossier_definitions = @dossier_definitions.or(DossierDefinition.where(id: definition_ids)) if current_user.is_admin?
      @dossier_definitions = @dossier_definitions.order(name: :asc).to_a
    end

    base_query = Dossier.all
    base_query = base_query.where(definition_id: definition_ids) if definition_ids.any?

    if fields.any?
      fields.each_with_index do |field, index|
        dossier_item_definition = DossierItemDefinition.find(field[:id])
        if dossier_item_definition
          base_query = base_query.with_item_value_filter(dossier_item_definition, field[:comp].to_sym, field[:values], filter_index: index)
        end
      end
    end

    act_as_normal_user = definition_ids.empty? || !current_user.is_admin?
    @result = Dossier.filter_for_index(current_user, params[:query],
      order_by: params[:order] || :updated_at_desc, page: is_export ? nil : params[:page], per_page: params[:per_page],
      act_as_normal_user: act_as_normal_user, apply_to: base_query, for_export: is_export)

    if is_export
      send_data Services::DossiersToTable.new(@result.query).to_excel, filename: "dossiers-#{DateTime.now.strftime("%Y-%m-%d--%H-%M-%S")}.xlsx"
    end
  end

  def list
    definition_ids = params[:definition_id]
    excluded_ids = params[:except].to_a
    limited_to_ids = params[:ids].to_a

    base_sql_query = Dossier.all
    base_sql_query = base_sql_query.where(definition_id: definition_ids) if definition_ids.present?
    @dossiers = Dossier.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {title: :asc}, page: params[:page], per_page: params[:per_page], apply_to: base_sql_query
    )
  end

  def new
    dossier_definition = DossierDefinition.find(params.require(:dossier_definition_id))
    @dossier = Interactors::DossierInteractor.new(dossier_definition)
    authorize!(:new, @dossier)
  end

  def create
    dossier_definition = DossierDefinition.find(params.require(:dossier_definition_id))
    authorize!(:create, Dossier.new(definition: dossier_definition))

    @dossier = Interactors::DossierInteractor.create(dossier_definition, params[:field_data].to_unsafe_hash, current_user)
    api_response_with(view: :create, errors: @dossier.errors)
  end

  def show
    set_reference_count
  end

  def update
    @dossier = Interactors::DossierInteractor.update(@dossier, params.require(:dossier_field_definition_id),
      params.fetch(:value), params[:lock_version], current_user)
    set_reference_count unless @dossier.errors.any?
    api_response_with(view: :show, errors: @dossier.errors)
  end

  def destroy
    Interactors::DossierInteractor.delete(@dossier, current_user)
    api_response_with(status: :ok, errors: @dossier.errors)
  end

  def show_task_references
    @tasks = @dossier.referenced_in_tasks.accessible_by(current_user.ability).includes(:workflow)
  end

  def show_dossier_field_references
    dossier_item_def = DossierItemDefinition.find(params.require(:dossier_field_definition_id))
    authorize!(:show, dossier_item_def)

    @dossiers = Dossier.where(id: dossier_item_def.dossier_items.reference_to(@dossier).select(:dossier_id).distinct.pluck(:dossier_id))
    api_response_with(view: :list)
  end

  private

  def set_reference_count
    @referenced_in_tasks_count = @dossier.referenced_in_tasks.accessible_by(current_user.ability).count
  end
end
