class Api::DossierDefinitionsController < ApiController
  load_and_authorize_resource

  def index
    base_query = DossierDefinition.left_joins(:groups).group(:id).order(Arel.sql("COUNT(groups.id) > 0"))
    @result = DossierDefinition.filter_for_index(current_user, params[:query], order_by: params[:order] || :name_asc, page: params[:page], per_page: params[:per_page], apply_to: base_query)
  end

  def list
    excluded_ids = params[:except].to_a
    limited_to_ids = params[:ids].to_a

    filter_by_groups_only = ActiveRecord::Type::Boolean.new.cast(params[:filter_by_groups_only] || false)

    @dossier_definitions = DossierDefinition.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {name: :asc}, page: params[:page], per_page: params[:per_page], act_as_normal_user: filter_by_groups_only
    )
  end

  def show
  end

  def create
    @dossier_definition = Interactors::DossierDefinitionInteractor.create(create_params, current_user)
    api_response_with(view: :show, errors: @dossier_definition.errors)
  end

  def update
    Interactors::DossierDefinitionInteractor.update(@dossier_definition, update_params, current_user)
    api_response_with(view: :show, errors: @dossier_definition.errors)
  end

  def destroy
    Interactors::DossierDefinitionInteractor.destroy(@dossier_definition, current_user)
    api_response_with(status: :ok, errors: @dossier_definition.errors)
  end

  def show_references
    @fields = @dossier_definition.referenced_in_dossier_item_definitions_for(current_user)
  end

  def update_reference
    dossier_item_definition = DossierItemDefinition.find(params.require(:dossier_field_definition_id))

    dossier_item_definition.update(options: (dossier_item_definition.options || {}).merge({inverseReference: params.required(:reference_definition)}))

    dossier_definition = DossierDefinition.find(dossier_item_definition.options["type"])
    @fields = dossier_definition.referenced_in_dossier_item_definitions_for(current_user)
    api_response_with(view: "api/dossier_definitions/show_references", errors: dossier_item_definition.errors)
  end

  private

  wrap_parameters DossierDefinition, include: DossierDefinition.attribute_names + [:group_ids]

  def create_params
    params.require(:dossier_definition).permit(:name, :description, group_ids: [])
  end

  def update_params
    params.require(:dossier_definition).permit(:name, :description, group_ids: [], title_fields: [], subtitle_fields: [])
  end
end
