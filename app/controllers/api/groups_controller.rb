class Api::GroupsController < ApiController
  load_and_authorize_resource

  def index
    @result = Group.filter_for_index(current_user, params[:query], order_by: params[:order] || :name_asc, page: params[:page], per_page: params[:per_page])
  end

  def list
    excluded_ids = params[:except].to_a
    limited_to_ids = params[:ids].to_a

    @groups = Group.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {name: :asc}, page: params[:page], per_page: params[:per_page]
    )
  end

  def show
  end

  def create
    @group = Group.create(group_params)
    Events::CreatedEvent.create!(subject: current_user, object: @group) if @group.errors.none?

    api_response_with(view: :show, errors: @group.errors)
  end

  def update
    @group.errors.add(:base, I18n.t("system_group_all.errors.update")) if @group.system_group_all?

    if @group.errors.none?
      if @group.update(group_params)
        group_params.keys.each do |changed_attribute|
          Events::SimpleActionEvent.create(subject: current_user, object: @group, whats_going_on: :"groups_update_#{changed_attribute}")
        end
      end
    end

    api_response_with(view: :show, errors: @group.errors)
  end

  def destroy
    @group.errors.add(:base, I18n.t("system_group_all.errors.destroy")) if @group.system_group_all?

    if @group.errors.none?
      if @group.destroy
        Events::DeletedEvent.create!(subject: current_user, object: @group)
      end
    end

    api_response_with(status: :ok, errors: @group.errors)
  end

  private

  wrap_parameters Group, include: Group.attribute_names + [:user_ids, :workflow_definition_ids, :dossier_definition_ids]
  def group_params
    params.require(:group).permit(:name, :description, user_ids: [], workflow_definition_ids: [], dossier_definition_ids: [])
  end
end
