class Api::MainMenuEntriesController < ApiController
  load_and_authorize_resource

  def index
    @result = MainMenuEntry.entries_for(current_user, true)
  end

  def index_filtered
    current_user.act_as_normal_user = true
    @result = MainMenuEntry.entries_for(current_user, false)
    render :index
  end

  def create
    errors = if params[:title].present?
      Interactors::MainMenuEntryInteractor.create_sub_menu(params[:parent_id], params[:title], current_user)
    else
      Interactors::MainMenuEntryInteractor.create_many(params[:parent_id], params.fetch(:linked_objects, []), current_user)
    end

    @result = MainMenuEntry.entries_for(current_user, true)

    api_response_with(view: :index, errors: errors)
  end

  def update
    Interactors::MainMenuEntryInteractor.update(@main_menu_entry, params.require(:title), current_user)

    @result = MainMenuEntry.entries_for(current_user, true)

    api_response_with(view: :index, errors: @main_menu_entry.errors)
  end

  def destroy
    Interactors::MainMenuEntryInteractor.destroy(@main_menu_entry, current_user)

    @result = MainMenuEntry.entries_for(current_user, true)

    api_response_with(view: :index, errors: @main_menu_entry.errors)
  end

  def move
    Interactors::MainMenuEntryInteractor.move(
      @main_menu_entry,
      params.fetch(:parent_id, nil)&.to_i,
      params.require(:index).to_i,
      current_user
    )

    api_response_with(status: :ok, errors: @main_menu_entry.errors)
  end

  private

  def main_menu_entry_params
    params.require(:main_menu_entry).permit(:parent_id, :title, linked_objects: [:id, :type])
  end
end
