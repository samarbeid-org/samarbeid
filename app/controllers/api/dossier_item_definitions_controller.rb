class Api::DossierItemDefinitionsController < ApiController
  load_and_authorize_resource

  def create
    @dossier_definition = DossierDefinition.find(params.require(:dossier_definition_id))
    authorize!(:manage, @dossier_definition)
    dossier_item_definition = Interactors::DossierItemDefinitionInteractor.create(@dossier_definition, create_params, current_user)

    api_response_with(view: "api/dossier_definitions/show", errors: dossier_item_definition.errors)
  end

  def update
    Interactors::DossierItemDefinitionInteractor.update(@dossier_item_definition, update_params, current_user)

    @dossier_definition = @dossier_item_definition.definition
    api_response_with(view: "api/dossier_definitions/show", errors: @dossier_item_definition.errors)
  end

  def destroy
    Interactors::DossierItemDefinitionInteractor.destroy(@dossier_item_definition, current_user)

    @dossier_definition = @dossier_item_definition.definition.reload
    api_response_with(view: "api/dossier_definitions/show", errors: @dossier_item_definition.errors)
  end

  def move
    Interactors::DossierItemDefinitionInteractor.move(@dossier_item_definition, params.require(:index).to_i, current_user)

    @dossier_definition = @dossier_item_definition.definition.reload
    api_response_with(view: "api/dossier_definitions/show", errors: @dossier_item_definition.errors)
  end

  private

  def create_params
    res = params.permit(:name, :required, :recommended, :unique, :content_type, options: {})
    res = res.merge(default_value: params.fetch(:default_value)) if params.key?(:default_value)
    res
  end

  def update_params
    res = params.permit(:name, :recommended, :required, :unique)
    res = res.merge(default_value: params.fetch(:default_value)) if params.key?(:default_value)
    res
  end
end
