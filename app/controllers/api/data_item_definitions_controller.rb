class Api::DataItemDefinitionsController < ApiController
  load_and_authorize_resource :task_item_definition, id_param: :id

  def create
    task_definition = TaskDefinition.find(params.require(:task_definition_id))

    task_item_definition = Interactors::WorkflowDefinitionInteractor.add_data_item_definition(
      task_definition,
      create_params_content_item_definition,
      create_params_task_item_definition
    )
    content_item_definition = task_item_definition.content_item_definition

    @task_item_definitions = task_definition.task_item_definitions.order(:position)
    @content_item_definitions = task_definition.workflow_definition.content_item_definitions

    errors = content_item_definition.errors.any? ? content_item_definition.errors : task_item_definition.errors
    api_response_with(view: "api/data_item_definitions/show", errors: errors)
  end

  def update
    Interactors::WorkflowDefinitionInteractor.update_data_item_definition(@task_item_definition, update_params_content_item_definition, update_params_task_item_definition)

    @task_item_definitions = @task_item_definition.task_definition.task_item_definitions.order(:position)
    @content_item_definitions = @task_item_definition.workflow_definition.content_item_definitions

    content_item_definition = @task_item_definition.content_item_definition
    errors = content_item_definition.errors.any? ? content_item_definition.errors : @task_item_definition.errors
    api_response_with(view: "api/data_item_definitions/show", errors: errors)
  end

  def destroy
    Interactors::WorkflowDefinitionInteractor.destroy_data_item_definition(@task_item_definition)

    @task_item_definitions = @task_item_definition.task_definition.task_item_definitions.order(:position)
    @content_item_definitions = @task_item_definition.workflow_definition.content_item_definitions

    api_response_with(view: "api/data_item_definitions/show", errors: @task_item_definition.errors)
  end

  def move
    Interactors::WorkflowDefinitionInteractor.move_data_item_definition(@task_item_definition, params.require(:index).to_i)

    @task_item_definitions = @task_item_definition.task_definition.task_item_definitions.order(:position)
    @content_item_definitions = @task_item_definition.workflow_definition.content_item_definitions

    api_response_with(view: "api/data_item_definitions/show", errors: @task_item_definition.errors)
  end

  private

  def create_params_content_item_definition
    content_item_definition_params = params.require(:content_item_definition)

    res = content_item_definition_params.permit(:id, :label, :content_type, options: {})
    res = res.merge(default_value: content_item_definition_params.fetch(:default_value)) if content_item_definition_params.key?(:default_value)
    res
  end

  def create_params_task_item_definition
    params.require(:task_item_definition).permit(:required, :info_box, :add_to_calendar)
  end

  def update_params_content_item_definition
    return unless params[:content_item_definition].present?

    content_item_definition_params = params.require(:content_item_definition)
    res = content_item_definition_params.permit(:label, options: {})
    res = res.merge(default_value: content_item_definition_params.fetch(:default_value)) if content_item_definition_params.key?(:default_value)
    res
  end

  def update_params_task_item_definition
    return unless params[:task_item_definition].present?

    params.require(:task_item_definition).permit(:required, :info_box, :add_to_calendar)
  end
end
