class Api::PagesController < ApiController
  before_action :authenticate_user!, except: :show
  skip_authorization_check only: :show
  load_and_authorize_resource except: :show

  def index
    @result = Page.filter_for_index(current_user, params[:query], order_by: params[:order] || :title_asc, page: params[:page], per_page: params[:per_page])
  end

  def list
    excluded_ids = params[:except].to_a
    limited_to_ids = params[:ids].to_a

    @pages = Page.search_for_list(
      current_user, params[:query], limited_to_ids: limited_to_ids, excluded_ids: excluded_ids,
      order: {title: :asc}, page: params[:page], per_page: params[:per_page]
    )
  end

  def show
    @page = Page.find_by(id: params[:id]) || Page.find_by(slug: params[:id])
  end

  def create
    @page = Page.create(page_params)

    api_response_with(view: :show, errors: @page.errors)
  end

  def update
    @page.update(page_params)

    api_response_with(view: :show, errors: @page.errors)
  end

  def destroy
    @page.destroy

    api_response_with(status: :ok, errors: @page.errors)
  end

  private

  def page_params
    params.require(:page).permit(:slug, :title, :content)
  end
end
