class Api::CalendarsController < ApiController
  skip_authorization_check only: :show
  skip_before_action :authenticate_user!, only: :show

  def show
    calendar_token = params.require(:token)
    user = User.find_and_authenticate_by_token(:calendar_token, calendar_token)
    raise CanCan::AccessDenied unless user

    render body: Services::Calendar.new(user).to_ical, mime_type: Mime::Type.lookup("text/calendar")
  end

  def create
    authorize!(:manage_tokens, current_user)
    if current_user.create_token(:calendar_token)
      Events::SimpleActionEvent.create(subject: current_user, object: current_user, whats_going_on: :calendar_create)
    end

    @token = current_user.calendar_token
    api_response_with(view: :create, errors: current_user.errors)
  end

  def destroy
    authorize!(:manage_tokens, current_user)
    if current_user.delete_token(:calendar_token)
      Events::SimpleActionEvent.create(subject: current_user, object: current_user, whats_going_on: :calendar_destroy)
    end

    @token = current_user.calendar_token
    api_response_with(view: :create, errors: current_user.errors)
  end
end
