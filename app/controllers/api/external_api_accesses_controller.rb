class Api::ExternalApiAccessesController < ApiController
  def create
    authorize!(:manage_tokens, current_user)
    if current_user.create_token(:api_token)
      Events::SimpleActionEvent.create(subject: current_user, object: current_user, whats_going_on: :api_token_create)
    end

    api_response_with(object: {api_token: current_user.api_token}, errors: current_user.errors)
  end

  def destroy
    authorize!(:manage_tokens, current_user)
    if current_user.delete_token(:api_token)
      Events::SimpleActionEvent.create(subject: current_user, object: current_user, whats_going_on: :api_token_destroy)
    end

    api_response_with(object: {api_token: current_user.api_token}, errors: current_user.errors)
  end
end
