module ShareLinkAuth
  def authenticate_user_or_share_token!
    if params.key?(:share_token)
      authenticate_with_share_token(params.require(:share_token))
      raise CanCan::AccessDenied unless @share_link_user
    else
      authenticate_user!
    end
  end

  def authenticate_with_share_token(share_token)
    @share_link_user = ShareLink.find_and_authenticate_by_token(:token, share_token)
    define_singleton_method(:current_user) { @share_link_user }
  end
end
