module ApiExceptionHandler
  extend ActiveSupport::Concern

  included do
    rescue_from ActiveRecord::RecordNotFound do |exception|
      api_response_with(object: exception.message, status: :not_found)
      Sentry.capture_exception(exception)
    end
  end
end
