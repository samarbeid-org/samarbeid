module ApiResponse
  def api_response_with(view: nil, object: nil, status: :ok, errors: nil)
    if errors&.any?
      error_messages = errors.group_by_attribute.map { |k, v| [k, {short: v.map(&:message), full: v.map(&:full_message)}] }.to_h
      render json: error_messages, status: :unprocessable_entity
    elsif view
      render view
    elsif object
      render json: object, status: status
    else
      head status
    end
  end
end
