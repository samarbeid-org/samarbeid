class ApiController < ApplicationController
  include ApiResponse
  include ApiExceptionHandler
  check_authorization # Ensure all API Controller Actions are protected by authorization

  # Add Exceptions for Healthcheck Endpoint
  skip_authorization_check only: :health_check
  skip_before_action :authenticate_user!, only: :health_check
  newrelic_ignore only: :health_check

  def health_check
    api_response_with object: Services::HealthCheck.all
  end

  private

  def convert_null_to(array_of_values, new_value)
    array_of_values.map do |value|
      value.eql?("null") ? new_value : value
    end
  end

  def convert_null_to_nil(array_of_values)
    convert_null_to(array_of_values, nil)
  end

  def remove_none_and_any(array_of_values)
    array_of_values.reject { |value| value === "none" || value === "any" }
  end

  def includes_none?(array_of_values)
    array_of_values.include?("none")
  end

  def includes_any?(array_of_values)
    array_of_values.include?("any")
  end
end
