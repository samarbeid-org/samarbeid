class HomeController < ApplicationController
  include ::ShareLinkAuth

  skip_before_action :authenticate_user!, only: [:unauthenticated, :share]

  def show
  end

  def my
    redirect_to("/users/#{current_user.id}/#{params[:path]}")
  end

  def unauthenticated
    render :show
  end

  def share
    authenticate_with_share_token(params.fetch(:share_token, nil))
    authenticate_user! unless @share_link_user
    authorize! :show, @share_link_user

    render :show
  end
end
