class Users::ConfirmationsController < Devise::ConfirmationsController
  def show
    confirmation_token = params[:confirmation_token]
    unless confirmation_token.blank?
      confirmable = resource_class.find_first_by_auth_conditions(confirmation_token: confirmation_token)
      if confirmable.confirmed? && !confirmable.pending_reconfirmation?
        if confirmable.encrypted_password.blank?
          redirect_to after_confirmation_path_for(resource_name, confirmable) and return
        else
          set_flash_message!(:notice, :already_confirmed_with_password)
          redirect_to new_session_path(confirmable) and return
        end
      end
    end
    super
  end

  protected

  def after_confirmation_path_for(resource_name, resource)
    set_flash_message!(:notice, :confirmed_without_password)
    token = resource.send(:set_reset_password_token)
    edit_password_path(resource, reset_password_token: token)
  end
end
