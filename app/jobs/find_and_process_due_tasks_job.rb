class FindAndProcessDueTasksJob < ApplicationJob
  def perform
    Services::DueTasks.find_and_process
  end
end
