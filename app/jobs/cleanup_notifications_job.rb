class CleanupNotificationsJob < ApplicationJob
  def perform
    Notification.cleanup
  end
end
