class SendStatsHeartbeatJob < ApplicationJob
  queue_as :default

  def perform
    stats_heartbeat = Services::StatsHeartbeat.new
    return false unless stats_heartbeat.enabled?

    transfer_to_collector(stats_heartbeat)
  end

  private

  def transfer_to_collector(stats_heartbeat)
    uri = URI(stats_heartbeat.send_to)
    http = Net::HTTP.new(uri.host, uri.port)
    http.use_ssl = (uri.scheme == "https")

    request = Net::HTTP::Post.new(uri)

    if uri.user && uri.password
      request.basic_auth(uri.user, uri.password)
    end

    request.body = {metrics: stats_heartbeat.as_hash}.to_json
    request.content_type = "application/json"

    response = http.request(request)

    unless response.code == "200"
      raise UnexpectedResponseException.new(response.code)
    end
  end
end
