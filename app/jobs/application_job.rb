class ApplicationJob < ActiveJob::Base
  # These settings should match those for our MailerJobs (see initializers/good_job.rb)
  unless Rails.env.test? || Rails.env.development?
    retry_on StandardError, wait: :exponentially_longer, attempts: 10 do |_job, exception|
      # after last retry
      Sentry.capture_exception(exception)
    end
  end

  # report any exception inside a job
  around_perform do |_job, block|
    block.call
  rescue => e
    Sentry.capture_exception(e)
    raise
  end
end
