class ReindexSearchablesJob < ApplicationJob
  include GoodJob::ActiveJobExtensions::Concurrency

  queue_as :reindex

  good_job_control_concurrency_with(
    enqueue_limit: 1,
    key: -> { "reindex-searchables-job-#{arguments.first}-#{arguments.second}" }
  )

  def perform(object, method)
    searchables = object.send(method)
    searchables.find_each do |searchable|
      searchable.reindex_as_search_document(mode: :sync)
    end
  end
end
