class MigrationJob::ImportContactUpdateCsvJob < ApplicationJob
  queue_as :default

  def perform(blob)
    csv = CSV.parse(blob.download, headers: true, encoding: "utf-8")

    user_creating_dossiers = User.find_by!(email: "admin@claudia-maicher.de")
    contact_dossier_def = DossierDefinition.find_by!(name: "Kontakt")
    email_field_def = contact_dossier_def.items.find_by!(name: "Email")

    orga_dossier_def = DossierDefinition.find_by!(name: "Organisation")
    orga_field_def = orga_dossier_def.items.find_by!(name: "Name")

    invalid_rows = []

    csv.each_with_index do |row, index|
      row_number = index + 2
      puts "Importing row #{row_number}"

      row = row.to_h.transform_values { |v| v&.squish }

      data = {}

      data["Zeile in Excel"] = row_number

      ["Name", "Vorname", "Titel", "Amt/Position", "PLZ",
        "DSGVO", "Telefonnummer", "Adresse", "Ort", "Anmerkung"].each do |simple_field|
        data[simple_field.delete(".")] = row[simple_field]
      end

      data["Newsletter"] = (row["Newsletter"] == "1")

      data["Anmerkung"] ||= ""
      data["Anmerkung"] << " [Aus Spalte AD] #{row["Spalte AD"]}" unless row["Spalte AD"].blank?

      if ContentTypes::Email.validate_data(row["Email"].to_s) == true
        data["Email"] = row["Email"]
      else
        data["Anmerkung"] << " [Aus Spalte Email (keine gültige Email)] #{row["Email"]}"
      end

      if data["Email"].present?
        if Dossier.where(definition: contact_dossier_def).joins(:dossier_items).where("dossier_items.dossier_item_definition": email_field_def).where("dossier_items.value": data["Email"]).any?
          invalid_rows << row_number
          next
        end
      end

      anrede_def_field = contact_dossier_def.items.find_by!(name: "Anrede")
      data["Anrede"] = anrede_def_field.options["items"].find { |item| item["text"] == row["Anrede"] }.dig("value") unless row["Anrede"].blank?

      data["Themen"] = []
      themen_field_def = contact_dossier_def.items.find_by!(name: "Themen")
      row["Themen"].to_s.split(", ").each do |row_card_value|
        row_card_value = "Lpz" if row_card_value == "Leipzig"
        select_value = themen_field_def.options["items"].find { |item| item["text"] == row_card_value }&.dig("value")
        raise "No select value found for #{row_card_value} in CSV row #{row_number}" unless select_value
        data["Themen"] << select_value
      end

      data["Neujahreskarte"] = []
      neujahrskarte_field_def = contact_dossier_def.items.find_by!(name: "Neujahreskarte")
      row["Neujahreskarte"].to_s.split(", ").each do |row_card_value|
        select_value = neujahrskarte_field_def.options["items"].find { |item| item["text"] == row_card_value }.dig("value")
        raise "No select value found for #{row_card_value} in CSV row #{row_number}" unless select_value
        data["Neujahreskarte"] << select_value
      end

      if row["Organisation"].present?
        dossier = Dossier.where(definition: orga_dossier_def).joins(:dossier_items).where("dossier_items.dossier_item_definition": orga_field_def).where("dossier_items.value": row["Organisation"]).first
        if dossier.nil?
          field_data = {orga_field_def.id => row["Organisation"]}
          dossier = Interactors::DossierInteractor.create(orga_dossier_def, field_data, user_creating_dossiers)
          raise "DossierInvalid: #{dossier.errors.messages}" unless dossier.valid?
          Events::SimpleActionEvent.create(object: dossier, whats_going_on: :created_via_import) if dossier.valid?
        end
        data["Organisation"] = [dossier]
      end
      field_data = data.transform_keys { |field_name|
        begin
          contact_dossier_def.items.find_by!(name: field_name).id
        rescue
          raise "FIELD_NAME: #{field_name}"
        end
      }
      dossier = Interactors::DossierInteractor.create(contact_dossier_def, field_data, user_creating_dossiers)
      raise "DossierInvalid: #{dossier.errors.messages} _____ DATA: #{data} _____ ROW: #{row}" unless dossier.valid?
      Events::SimpleActionEvent.create(object: dossier, whats_going_on: :created_via_import) if dossier.valid?
    end

    puts "INVALID ROWS: #{invalid_rows}"
  end
end
