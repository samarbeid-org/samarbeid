class SyncTitlesOfDossierReferencesJob < ApplicationJob
  include GoodJob::ActiveJobExtensions::Concurrency

  queue_as :mass_update

  good_job_control_concurrency_with(
    enqueue_limit: 1,
    key: -> { "sync-titles-of-dossier-references-job-#{arguments.first}" }
  )

  def perform(dossier_id)
    dossier = Dossier.find_or_initialize_by(id: dossier_id)

    dossier.used_in_dossier_items.find_each do |dossier_item|
      dossier_item.update_dossier_title_and_subtitle
    end

    dossier.used_in_content_items.find_each do |content_item|
      content_item.update_workflow_title(true)
    end
  end
end
