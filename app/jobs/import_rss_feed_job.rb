class ImportRssFeedJob < ApplicationJob
  require "rss"

  def perform
    url = "https://www.samarbeid.org/c/feature-releases/feed/"
    URI.open(url) do |rss| # standard:disable Security/Open
      feed = RSS::Parser.parse(rss)
      feed.items.reverse_each do |item|
        news = News.find_or_initialize_by(url: item.link)
        new_record = news.new_record?
        pub_at = item.pubDate.to_time
        if new_record || news.updated_at < pub_at
          news.title = item.title
          news.description = item.description
          news.updated_at = pub_at
          news.created_at = pub_at if new_record
          news.save!

          Events::FeatureReleasedEvent.create!(object: news) if new_record
        end
      end
    end
  end
end
