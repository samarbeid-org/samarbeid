class FindAndStartSnoozedTasksJob < ApplicationJob
  def perform
    Services::SnoozedTasks.find_and_start
  end
end
