class DeliverRecentNotificationsMailsJob < ApplicationJob
  def perform(interval)
    User.for_sending_notifications_every(interval).each do |user|
      notifications = user.unseen_and_unsent_notifications
      next if notifications.empty?

      NotificationsMailer.with(user: user, notifications: notifications.to_a).recent.deliver_later

      notifications.where(delivered_at: nil).update_all(delivered_at: Time.zone.now)
    end
  end
end
