class SyncTitlesJob < ApplicationJob
  include GoodJob::ActiveJobExtensions::Concurrency

  queue_as :mass_update

  good_job_control_concurrency_with(
    enqueue_limit: 1,
    key: -> { "sync-titles-job-#{arguments.first}-#{arguments.second}" }
  )

  def perform(object, instances_method)
    instances = object.send(instances_method)
    instances.find_each do |instance|
      instance.sync_title_from_fields
    end
  end
end
