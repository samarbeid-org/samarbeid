class ExtractTextFromFileJob < ApplicationJob
  def perform(uploaded_file)
    uploaded_file.update_extracted_text_and_reindex_owner_object
  end
end
