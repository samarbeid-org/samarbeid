class ReindexJob < ApplicationJob
  queue_as :reindex

  def perform(searchable)
    searchable.reindex_as_search_document(mode: :sync)
  end
end
