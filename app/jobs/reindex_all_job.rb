class ReindexAllJob < ApplicationJob
  queue_as :reindex

  def perform
    SearchDocument.reindex_all
  end
end
