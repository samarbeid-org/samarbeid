class CleanupInvalidDataJob < ApplicationJob
  def perform(cleanup_all: false)
    # Uploaded Files are never directly deleted but instead "unassociated" from the file field.
    # Thus they currently MUST be cleaned up manually.
    UploadedFile.invalid_records.select { |uf| uf.created_at < 2.days.ago }.each(&:destroy)

    return unless cleanup_all
    CleanupInvalidDataJob.active_storage_blob_invalid_records.each(&:purge_later)
    Event.invalid_records.each(&:destroy)
  end

  def self.invalid_data_stats
    {
      invalid_uploaded_files: UploadedFile.invalid_records.count,
      invalid_active_storage_blobs: active_storage_blob_invalid_records.count,
      invalid_events: Event.invalid_records.count
    }
  end

  def self.active_storage_blob_invalid_records
    ActiveStorage::Blob.unattached.all
  end
end
