class RunAutomationsJob < ApplicationJob
  def perform
    Services::Automations.execute_active_and_due_automations
  end
end
