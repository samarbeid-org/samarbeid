module Samarbeid
  class Api < Grape::API
    prefix "api"
    version "v0"
    format :json

    use GrapeLogging::Middleware::RequestLogger,
      instrumentation_key: "grape_key",
      include: [GrapeLogging::Loggers::Response.new,
        GrapeLogging::Loggers::FilterParameters.new]

    helpers do
      def current_user
        return nil unless auth_token
        authenticated_user = User.find_and_authenticate_by_token(:api_token, auth_token)
        authenticated_user if authenticated_user&.can?(:read, :api)
      end

      def authenticate!
        error!("400 Bad Request - no token passed as api_token query param. You might have to first enable API access for your user!", 400) unless auth_token
        error!("401 Unauthorized - pass your token as api_token query param.", 401) unless current_user
      end

      def auth_token
        auth_token = params[:api_token].to_s
        return nil if auth_token.blank?
        auth_token
      end
    end

    mount Samarbeid::DossierDefinition
    mount Samarbeid::Dossier
    mount Samarbeid::ProcessDefinition
    mount Samarbeid::Process
    mount Samarbeid::Export

    add_swagger_documentation hide_documentation_path: true,
      info: {
        title: "samarbeid - API",
        doc_version: "0.0.2",
        description: "Simple RESTful API endpoints are available. For authentication, include your user-specific api_token as a query parameter." \
            "Examples of the API's functionality can be found in our [test suite](https://gitlab.com/samarbeid-org/samarbeid/-/tree/main/test/api)." \
            "This API adheres to the OpenAPI (Swagger) Specification. The API schema can be downloaded at: #{GrapeSwaggerRails.options.url}"
      },
      security_definitions: {
        api_key: {
          type: "apiKey",
          name: "api_token",
          in: "query"
        }
      }
  end
end
