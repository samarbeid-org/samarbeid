module Samarbeid
  class ProcessDefinition < Grape::API
    resource :process_definitions do
      desc "Returns all available process definitions",
        entity: Samarbeid::Entities::ProcessDefinition,
        detail: "Returns all activated process definitions for which processes may be started by current user"

      get do
        authenticate!
        process_definitions = WorkflowDefinition.activated.accessible_by(current_user.ability, :build_instance)
        present process_definitions, with: Samarbeid::Entities::ProcessDefinition
      end
    end
  end
end
