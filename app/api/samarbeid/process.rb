module Samarbeid
  class Process < Grape::API
    resource :process do
      desc "Allows creating processes",
        entity: Samarbeid::Entities::Process,
        detail: "Create a process along with field data. Please note that the 'fields' option is not supported in this UI. Instead, you can use a CLI tool or manually construct the URL."
      params do
        requires :process_definition_id, type: Integer
        optional :name, type: String
        # This does not work although it would be very nice to use!
        # optional :fields, type: Array[Samarbeid::Entities::Field]
        optional :fields, type: Array(JSON) do
          optional :definition_id
          optional :value
        end
      end
      post do
        authenticate!

        workflow_definition = WorkflowDefinition.activated.accessible_by(current_user.ability, :build_instance).find_by(id: params[:process_definition_id])
        error!("400 Bad Request - no valid process_definition_id given", 400) unless workflow_definition

        fields = params[:fields].to_h do |field_p|
          # TODO: Hack to allow select fields, replace by proper solution
          # Interactor expects select options as integer, via GRAPE API we only get Strings
          value = field_p["value"].is_a?(Array) ? field_p["value"].map(&:to_i) : field_p["value"]
          [field_p["definition_id"], value]
        end
        process = Interactors::WorkflowInteractor.create(
          workflow_definition, {title: params[:name], assignee: current_user},
          fields,
          nil,
          current_user,
          :api
        )

        error!("400 Bad Request - #{process.errors.full_messages}, 400") if process.errors.any?

        present process, with: Samarbeid::Entities::Process
      end

      desc "Search for processes",
        entity: Samarbeid::Entities::Process,
        detail: "Search processes matching provided parameters. Please note that the 'fields' option is not supported in this UI. Instead, you can use a CLI tool or manually construct the URL."
      params do
        requires :process_definition_id, type: Integer
        optional :name, type: String
        # This does not work although it would be very nice to use!
        # optional :fields, type: Array[Samarbeid::Entities::Field]
        optional :fields, type: Array(JSON) do
          optional :definition_id
          optional :value
        end
      end
      get do
        authenticate!

        workflow_definition = WorkflowDefinition.activated.accessible_by(current_user.ability, :build_instance).find_by(id: params[:process_definition_id])
        error!("400 Bad Request - no valid process_definition_id given", 400) unless workflow_definition

        processes = Workflow.where(workflow_definition_id: params[:process_definition_id]).accessible_by(current_user.ability)
        processes = processes.where(title: params[:name]) if params[:name].present?

        query_fields = params[:fields]

        if query_fields

          query_fields.map { |qf| qf[:definition_id] }.each do |field_def_id|
            error!("400 Bad Request - invalid field definition_id given: #{field_def_id}", 400) unless workflow_definition.content_item_definitions.find_by(id: field_def_id)
          end

          processes = processes.select do |process|
            query_fields.all? do |q_field|
              field = process.content_items.find_by(content_item_definition_id: q_field[:definition_id])
              field && (field.value == q_field[:value])
            end
          end
        end
        present processes, with: Samarbeid::Entities::Process
      end
    end
  end
end
