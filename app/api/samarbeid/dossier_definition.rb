module Samarbeid
  class DossierDefinition < Grape::API
    resource :dossier_definitions do
      desc "Returns all available dossier definitions",
        entity: Samarbeid::Entities::DossierDefinition,
        detail: "Returns all dossier definitions for which dossiers may be created by current user"

      get do
        authenticate!
        dossier_definitions = ::DossierDefinition.accessible_by(current_user.ability, :build_instance).all
        present dossier_definitions, with: Samarbeid::Entities::DossierDefinition
      end
    end
  end
end
