module Samarbeid
  class Export < Grape::API
    content_type :xlsx, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
    format :xlsx

    resource :export do
      desc "Export dossiers as Excel file (xlsx)"
      params do
        requires :dossier_definition_id, type: Integer
      end
      get "dossiers" do
        authenticate!

        dossier_definition = ::DossierDefinition.accessible_by(current_user.ability, :build_instance).find_by(id: params[:dossier_definition_id])
        error!("400 Bad Request - no valid dossier_definition_id given", 400) unless dossier_definition

        header["Content-Disposition"] = "attachment; filename=dossiers.xlsx"
        Services::DossiersToTable.new(dossier_definition.dossiers).to_excel
      end

      desc "Export processes as Excel file (xlsx)"
      params do
        requires :process_definition_id, type: Integer
      end
      get "processes" do
        authenticate!

        workflow_definition = ::WorkflowDefinition.activated.accessible_by(current_user.ability, :build_instance).find_by(id: params[:process_definition_id])
        error!("400 Bad Request - no valid process_definition_id given", 400) unless workflow_definition

        header["Content-Disposition"] = "attachment; filename=processes.xlsx"
        Services::WorkflowsToTable.new(workflow_definition.workflows).to_excel
      end
    end
  end
end
