module Samarbeid::Entities
  class DossierDefinition < Grape::Entity
    expose :id, as: :definition_id, documentation: {type: "Integer", required: true,
                                                    desc: "Unique identifier for dossier definition"}
    expose :name, documentation: {type: "String", required: true, desc: "Name of dossier definition"}
    expose :field_definitions, using: FieldDefinition

    private

    def field_definitions
      object.items.no_notes
    end
  end
end
