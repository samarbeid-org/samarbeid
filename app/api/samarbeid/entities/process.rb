module Samarbeid::Entities
  class Process < Grape::Entity
    expose :id, documentation: {type: "Integer"}
    expose :title, as: :name, documentation: {type: "String"}
    expose :fields, using: Samarbeid::Entities::Field

    private

    def fields
      object.content_items.no_notes
    end
  end
end
