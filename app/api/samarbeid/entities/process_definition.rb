module Samarbeid::Entities
  class ProcessDefinition < Grape::Entity
    expose :id, as: :definition_id, documentation: {type: "Integer", required: true,
                                                    desc: "Unique identifier for process definition"}
    expose :name, documentation: {type: "String", required: true, desc: "Name of process definition"}
    expose :field_definitions, using: FieldDefinition

    private

    def field_definitions
      object.content_item_definitions.no_notes
    end
  end
end
