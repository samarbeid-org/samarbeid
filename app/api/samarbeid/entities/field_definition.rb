module Samarbeid::Entities
  class FieldDefinition < Grape::Entity
    expose :id, as: :definition_id, documentation: {type: "Integer", required: true,
                                                    desc: "Unique identifier for item definition"}
    expose :label, documentation: {type: "String", required: true, desc: "Name of item definition"}
    expose :content_type_before_type_cast, as: :type, documentation: {type: "String", required: true,
                                                                      desc: "type of content for this field"}
    expose :required, documentation: {type: "Boolean"}, if: lambda { |field_def, _options| field_def.respond_to?(:required) }
    expose :options, documentation: {type: "Hash", desc: "Options for this field"}
    expose :default_value

    def required
      object.try(:required)
    end
  end
end
