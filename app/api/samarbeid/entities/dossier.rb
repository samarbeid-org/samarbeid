module Samarbeid::Entities
  class Dossier < Grape::Entity
    expose :id, documentation: {type: "Integer"}
    expose :title, as: :name, documentation: {type: "String"}
    expose :fields, using: Samarbeid::Entities::Field

    private

    def fields
      object.dossier_items.no_notes
    end
  end
end
