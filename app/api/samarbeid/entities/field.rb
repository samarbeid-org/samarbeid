module Samarbeid::Entities
  class Field < Grape::Entity
    expose :id, documentation: {type: "Integer"}
    expose :resolve_definition_id, as: :definition_id, documentation: {type: "Integer"}
    expose :label, documentation: {type: "String"}
    expose :content_type_before_type_cast, as: :type, documentation: {type: "String"}
    expose :value, documentation: {type: "String"}

    def resolve_definition_id
      case object.class.name
      when "DossierItem"
        object.dossier_item_definition_id
      when "ContentItem"
        object.content_item_definition_id
      end
    end
  end
end
