module Samarbeid
  class Dossier < Grape::API
    resource :dossier do
      desc "Allows creating dossiers",
        entity: Samarbeid::Entities::Dossier,
        detail: "Create a dossier along with field data. Please note that the 'fields' option is not supported in this UI. Instead, you can use a CLI tool or manually construct the URL."
      params do
        requires :dossier_definition_id, type: Integer
        # This does not work although it would be very nice to use!
        # optional :fields, type: Array[Samarbeid::Entities::Field]
        optional :fields, type: Array(JSON) do
          optional :definition_id
          optional :value
        end
      end
      post do
        authenticate!

        dossier_definition = ::DossierDefinition.accessible_by(current_user.ability, :build_instance).find_by(id: params[:dossier_definition_id])
        error!("400 Bad Request - no valid dossier_definition_id given", 400) unless dossier_definition

        fields = params[:fields].to_h do |field_p|
          # TODO: Hack to allow select fields, replace by proper solution
          # Interactor expects select options as integer, via GRAPE API we only get Strings
          value = field_p["value"].is_a?(Array) ? field_p["value"].map(&:to_i) : field_p["value"]
          [field_p["definition_id"], value]
        end

        dossier = Interactors::DossierInteractor.create(dossier_definition, fields, current_user)

        error!("400 Bad Request - #{dossier.errors.full_messages}", 400) if dossier.errors.any?

        Events::SimpleActionEvent.create(subject: current_user, object: dossier, whats_going_on: :dossier_created_via_api, notify: :subject)

        present dossier, with: Samarbeid::Entities::Dossier
      end

      desc "Allows updating a single field of a dossiers",
        entity: Samarbeid::Entities::Dossier,
        detail: "Update a dossier."
      params do
        requires :id, type: Integer
        requires :field_definition_id
        requires :value
      end
      put do
        authenticate!
        dossier = ::Dossier.find_by(id: params[:id])
        error!("400 Bad Request - no valid dossier id given", 400) unless dossier

        field = dossier.dossier_items.find_or_initialize_by(dossier_item_definition_id: params[:field_definition_id])
        error!("400 Bad Request - no valid field id given", 400) unless field

        value = params["value"].is_a?(Array) ? params["value"].map(&:to_i) : params["value"]

        dossier = Interactors::DossierInteractor.update(dossier, field.dossier_item_definition_id, value, field.lock_version, current_user)

        error!("400 Bad Request - #{dossier.errors.full_messages}", 400) if dossier.errors.any?

        present dossier, with: Samarbeid::Entities::Dossier
      end

      desc "Search for dossiers",
        entity: Samarbeid::Entities::Dossier,
        detail: "Search dossiers matching provided parameters. Please note that the 'fields' option is not supported in this UI. Instead, you can use a CLI tool or manually construct the URL."
      params do
        requires :dossier_definition_id, type: Integer
        optional :name, type: String
        optional :fields, type: Array(JSON) do
          optional :definition_id
          optional :value
        end
      end
      get do
        authenticate!

        dossier_definition = ::DossierDefinition.accessible_by(current_user.ability, :build_instance).find_by(id: params[:dossier_definition_id])
        error!("400 Bad Request - no valid dossier_definition_id given", 400) unless dossier_definition

        dossiers = ::Dossier.where(definition_id: params[:dossier_definition_id]).accessible_by(current_user.ability)

        dossiers = dossiers.find_each.select { |dossier| dossier.title == params[:name] } if params[:name].present?

        query_fields = params[:fields]

        if query_fields

          query_fields.map { |qf| qf[:definition_id] }.each do |field_def_id|
            error!("400 Bad Request - invalid field definition_id given: #{field_def_id}", 400) unless dossier_definition.items.find_by(id: field_def_id)
          end

          dossiers = dossiers.select do |dossier|
            query_fields.all? do |q_field|
              field = dossier.dossier_items.find_by(dossier_item_definition_id: q_field[:definition_id])
              field && (field.value == q_field[:value])
            end
          end
        end
        present dossiers, with: Samarbeid::Entities::Dossier
      end
    end
  end
end
