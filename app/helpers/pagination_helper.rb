module PaginationHelper
  def total_page_count_for(paginated_result)
    return nil unless paginated_result.total_count.present? && paginated_result.per_page.present?

    (paginated_result.total_count + paginated_result.per_page - 1) / paginated_result.per_page
  end
end
