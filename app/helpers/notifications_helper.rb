module NotificationsHelper
  def truncateNotificationMessage(localized_object, truncate_payload_max_length)
    unless localized_object.dig(:payload, :message).nil? || truncate_payload_max_length == false
      # we need to add utf-8 encoding to correctly special characters like (üöä ...)
      localized_object[:payload][:message] = Truncato.truncate("<meta charset=\"UTF-8\">" + localized_object[:payload][:message], max_length: truncate_payload_max_length, filtered_tags: ["meta"])
    end

    localized_object
  end
end
