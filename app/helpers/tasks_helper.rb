module TasksHelper
  def tasks_visible_in_sidebar(workflow)
    workflow.items.flat_map { |item| item.is_a?(Block) ? item.items : item }
  end

  def sort_tasks_by_position_in_sidebar(tasks)
    tasks.sort_by { |t| [t.workflow_or_block.try(:position), t.position].compact }
  end
end
