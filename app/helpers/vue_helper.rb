module VueHelper
  def flash_as_messages(flash)
    flash.map { |type, text| {type: type, text: text} }
  end

  def resource_errors_as_messages(resource)
    resource.errors.full_messages.map { |error| {type: "error", text: error} }
  end
end
