module EmailHelper
  def attach_email_image(path)
    attachments.inline[path] = File.read(Rails.root.join("app/assets/images/#{path}")) unless attachments[path]
    attachments[path]
  end

  def object_reference_url(ref)
    return ref[:url] if ref[:url].present?

    raise "cannot generate url for #{ref.inspect}" unless ref[:object_type].present? && ref[:object_id].present?

    # Replace workflow_definition here as a fix because we renamed it to process_definition in frontend
    type_path = ((ref[:object_type] == "workflow_definition") ? "process_definition" : ref[:object_type]).pluralize.dasherize
    query = "?event=#{ref[:event]}" if ref[:event].present?
    hash = ref[:hash] if ref[:hash]
    absolute_url_for_mails("#{type_path}/#{ref[:object_id]}#{query}#{hash}")
  end

  def notifications_url
    absolute_url_for_mails("notifications")
  end

  def my_notification_settings_url
    absolute_url_for_mails("my/settings/notifications")
  end

  def absolute_url_for_mails(from_path)
    default_mailer_url = Rails.application.config.action_mailer.default_url_options[:host]
    URI.join(default_mailer_url, from_path).normalize.to_s
  end
end
