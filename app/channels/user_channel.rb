class UserChannel < ApplicationCable::Channel
  def subscribed
    stream_for current_user
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def self.broadcast_info(user)
    UserChannel.broadcast_to(user, task_info_json(user))
  end

  def self.task_info_json(user)
    Jbuilder.new { |json|
      ApplicationController.render(
        "api/users/info",
        assigns: {
          user_info: user.info
        },
        locals: {json: json}
      )
    }.attributes!
  end
  private_class_method :task_info_json
end
