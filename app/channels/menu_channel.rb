class MenuChannel < ApplicationCable::Channel
  def subscribed
    stream_for MainMenuEntry
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def self.broadcast_update
    MenuChannel.broadcast_to(MainMenuEntry, {action: :menu_updated})
  end
end
