class MonitorChannel < ApplicationCable::Channel
  def subscribed
    object = GlobalID::Locator.locate params[:object_gid]
    if current_user.ability.can?(:read, object)
      stream_for object
    else
      reject
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def self.broadcast_content_item_locked(content_item)
    content_item&.task_items&.each do |task_item|
      broadcast_to(task_item.task, {action: :data_item_locked, id: task_item.id})
    end
  end

  def self.broadcast_content_item_unlocked(content_item)
    content_item&.task_items&.each do |task_item|
      broadcast_to(task_item.task, {action: :data_item_unlocked, id: task_item.id})
    end
  end

  def self.broadcast_content_item_updated(content_item)
    content_item&.task_items&.each do |task_item|
      broadcast_to(task_item.task, {action: :data_item_updated, id: task_item.id})
    end
  end

  def self.broadcast_events_updated(object)
    broadcast_to(object, {action: :events_updated}) if object.persisted?
  end

  def self.broadcast_object_updated(object)
    broadcast_to(object, {action: :updated})
  end
end
