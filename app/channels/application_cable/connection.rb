module ApplicationCable
  class Connection < ActionCable::Connection::Base
    include ::ShareLinkAuth

    identified_by :current_user

    def connect
      authenticate_with_share_token(request.params[:share_token]) if request.params.key?(:share_token)
      self.current_user = find_verified_user unless @share_link_user
    end

    private

    # this checks whether a user is authenticated with devise
    def find_verified_user
      if (verified_user = env["warden"].user)
        verified_user
      else
        reject_unauthorized_connection
      end
    rescue UncaughtThrowError
      reject_unauthorized_connection
    end
  end
end
