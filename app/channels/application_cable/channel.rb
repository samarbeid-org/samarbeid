module ApplicationCable
  class Channel < ActionCable::Channel::Base
    # A little fix to close actioncable channel after connection exceeds devise timeout interval to force client
    # to reconnect, which checks if session is still valid or user session has expired
    periodically every: 5.seconds do
      if current_user.timedout?(connection.statistics[:started_at])
        connection.close # closes connection with reconnect=true
      end
    end
  end
end
