class MonitorProcessStructureChannel < ApplicationCable::Channel
  def subscribed
    object = GlobalID::Locator.locate params[:object_gid]
    object = object.workflow if object.is_a?(Task)

    if object.is_a?(Workflow) && current_user.ability.can?(:read, object)
      stream_for object
    else
      reject
    end
  end

  def unsubscribed
    # Any cleanup needed when channel is unsubscribed
  end

  def self.broadcast_process_structure_updated(object)
    object = object.workflow unless object.is_a?(Workflow)
    broadcast_to(object, {action: :process_structure_updated})
  end
end
