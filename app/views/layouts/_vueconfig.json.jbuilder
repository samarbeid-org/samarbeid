json.current_user do
  json.partial! "/api/users/current_user", user: user.is_a?(User) ? user : nil
end

if user.is_a?(ShareLink)
  json.share_token user.token
end

json.menus do
  MenuEntry.locations.keys.each do |location|
    json.set! location do
      json.array! MenuEntry.where(location: location).order(position: :asc) do |entry|
        json.title entry.title
        if entry.page
          json.slug entry.page.slug
        else
          json.url entry.url
        end
        json.new_window entry.new_window
      end
    end
  end
end

json.global_id_app GlobalID.app

json.flags do
  if Rails.env.in?(%w[staging canary])
    json.show_extended_version_info true
    json.revision_human Services::System.revision_text if Services::System.revision_text
  else
    json.show_extended_version_info false
  end
  json.enable_experimental_features ENV["ENABLE_EXPERIMENTAL_FEATURES"].present?

  if user
    json.system_process_definition_single_task_access user.can?(:build_instance, WorkflowDefinition.system_process_definition_single_task)
  end
end

if Rails.env.in?(%w[production canary])
  json.sentry_dsn SENTRY_DSN
  json.host ENV.fetch("HOST", "default")
  json.release Services::System.revision if Services::System.revision
end
