json.result do
  json.partial! "/api/main_menu_entries/main_menu_entry", collection: @result, as: :main_menu_entry
end
