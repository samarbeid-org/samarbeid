json.id main_menu_entry[:id]
json.title main_menu_entry[:title]
json.type main_menu_entry[:type]
json.linkedId main_menu_entry[:linked_id] if main_menu_entry.has_key?(:linked_id)
json.childEntries main_menu_entry[:child_entries], partial: "/api/main_menu_entries/main_menu_entry", as: :main_menu_entry if main_menu_entry.has_key?(:child_entries)
