json.array! @tasks do |task|
  json.partial! "/api/tasks/task_minimal", task: task
  json.workflow do
    json.partial! "/api/workflows/workflow_minimal", workflow: task.workflow
  end
end
