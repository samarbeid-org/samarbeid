if task.nil?
  json.null!
else
  json.type "task"
  json.variant "minimal"
  json.id task.id
  json.gid task.to_global_id.to_s
  json.definitionId task.task_definition&.id

  json.name task.name

  json.state task.aasm_state
  json.stateUpdatedAt task.state_updated_at

  json.dueAt task.due_at
  json.snoozeUntil task.start_at&.to_date
  json.marked task.marked?(current_user)

  json.assignee do
    json.partial! "api/users/user", user: task.assignee
  end

  json.commentsCount task.comments.count { |c| !c.deleted? }
  json.referenceLabel task.reference_label
  json.mentionLabel task.mention_label

  todo_counts = task.todo_counts
  unless todo_counts.nil?
    json.todoCounts do
      json.total todo_counts[1]
      json.done todo_counts[0]
    end
  end

  json.activeShare task.share_links.active.exists?
end
