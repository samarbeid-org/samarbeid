json.filter_category_counts @result.filter_category_counts

json.users do
  json.partial! "/api/users/user", collection: @users, as: :user
end

if @workflow_definition
  json.workflow_definition do
    json.partial! "/api/workflow_definitions/workflow_definition", workflow_definition: @workflow_definition
  end

  unless @workflow_definition.system_process_definition_single_task?
    json.task_definitions @workflow_definition.task_definitions_ordered_in_structure.map { |td| {value: td.id, text: td.name} }
  end

  json.partial! "/api/field_filters/field_filters", field_definitions: @workflow_definition.field_definitions
else
  json.workflow_definitions @workflow_definitions.map { |pd| {value: pd.id, text: pd.name} }
end

json.total_pages @result.total_pages
json.result do
  json.array! @result.query.group_by(&:workflow).each do |workflow, tasks|
    json.workflow do
      json.type "workflow"
      json.id workflow.id
      json.title workflow.title
      json.titleTooltip workflow.title_tooltip
      json.systemIdentifier workflow.system_identifier
      json.state workflow.aasm_state
      json.definition do
        json.partial! "/api/workflow_definitions/workflow_definition", workflow_definition: workflow.workflow_definition
      end
      json.visible_tasks tasks_visible_in_sidebar(workflow) do |sub_task|
        json.id sub_task.id
        json.state sub_task.aasm_state
      end
    end
    json.tasks do
      json.partial! "/api/tasks/task_minimal", collection: sort_tasks_by_position_in_sidebar(tasks), as: :task
    end
  end
end
