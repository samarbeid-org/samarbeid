if task.nil?
  json.null!
else
  json.partial!("api/tasks/task_minimal", task: task)

  json.variant "full"

  json.lock_version task.lock_version
  json.dataReadonly !task.open? || !task.workflow.active?
  json.contributors task.contributors, partial: "api/users/user", as: :user

  json.timeTracking do
    json.timeSpentInMinutes task.time_tracking_spent_in_minutes
    json.timeBudgetInMinutes task.time_tracking_budget_in_minutes
  end

  json.shares do
    json.partial! "api/share/share", collection: task.share_links, as: :share
  end

  json.availableActions do
    json.array!({
      start: task.may_start?,
      complete: task.may_complete?,
      reopen: task.may_reopen?,
      skip: task.may_skip?
    }.filter_map { |k, v| k if v })
  end

end
