if task.nil?
  json.null!
else
  json.id task.id
  json.gid task.to_global_id.to_s
  json.name task.name

  json.dataReadonly !task.open? || !task.workflow.active?
  json.lock_version task.lock_version
end
