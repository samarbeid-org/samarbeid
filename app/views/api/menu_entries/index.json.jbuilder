json.total_pages @result.total_pages
json.result do
  json.partial! "/api/menu_entries/menu_entry", collection: @result.query, as: :menu_entry
end
