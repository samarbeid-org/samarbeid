json.id menu_entry.id
json.title menu_entry.title
json.location do
  json.value menu_entry.location
  json.text MenuEntry.human_enum_name(:location, menu_entry.location)
end
json.position menu_entry.position
json.page do
  if menu_entry.page.present?
    json.partial! "/api/pages/page", page: menu_entry.page
  else
    json.null!
  end
end
json.url menu_entry.url
json.new_window menu_entry.new_window
json.updatedAt menu_entry.updated_at
