@locations.each do |location|
  json.set! location, MenuEntry.human_enum_name(:location, location)
end
