lo = event.localized_object(local_assigns.fetch(:referenced, false), current_user, true, false)
lo = truncateNotificationMessage(lo, local_assigns.fetch(:truncate_payload_max_length, false))
json.merge! lo

json.subject do
  json.partial! "api/users/user", user: event.subject
end
