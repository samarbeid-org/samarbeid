json.event_types @event_types.map { |el| {value: el.name, text: el.model_name.human} }
json.users do
  json.partial! "/api/users/user", collection: @users, as: :user
end

json.result do
  json.partial! "/api/events/event", collection: @events, as: :event, referenced: true
end

json.total_pages @total_pages
