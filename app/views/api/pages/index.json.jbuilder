json.total_pages @result.total_pages
json.result do
  json.partial! "/api/pages/page", collection: @result.query, as: :page
end
