json.id content_item_definition.id
json.label content_item_definition.computed_label
json.type do
  json.name content_item_definition.content_type.to_s
  json.label content_item_definition.content_type.label
end
json.partial! "/api/content_types/options", type: content_item_definition.content_type, options: content_item_definition.options

json.partial! "/api/data_items/data_item_value", content_type: content_item_definition.content_type, value: content_item_definition.default_value, key: :default_value

json.taskItemDefinitionCount content_item_definition.task_item_definitions.count
json.blockDefinitionCount content_item_definition.block_definitions.count
