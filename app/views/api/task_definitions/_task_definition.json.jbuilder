if task_definition.nil?
  json.null!
else
  json.id task_definition.id
  json.updatedAt task_definition.updated_at
  json.name task_definition.name
  json.dueInDays task_definition.due_in_days
  json.deferralInDays task_definition.deferral_in_days
  json.candidateAssignee do
    json.partial! "api/users/user", user: task_definition.candidate_assignee
  end
  json.candidateAssigneeFromProcess task_definition.candidate_assignee_from_workflow

  json.candidateContributors task_definition.candidate_contributors, partial: "api/users/user", as: :user

  json.taskItemDefinitions do
    json.partial! "/api/task_item_definitions/task_item_definition",
      collection: task_definition.task_item_definitions, as: :task_item_definition
  end
end
