json.total_pages @result.total_pages
json.result do
  json.partial! "/api/groups/group", collection: @result.query, as: :group
end
