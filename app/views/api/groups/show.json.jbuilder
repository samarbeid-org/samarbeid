json.partial! "/api/groups/group_minimal", group: @group

json.updatedAt @group.updated_at
json.description @group.description
json.systemIdentifier @group.system_identifier

json.users @group.users, partial: "api/users/user", as: :user
json.workflowDefinitions @group.workflow_definitions, partial: "api/workflow_definitions/workflow_definition", as: :workflow_definition
json.dossierDefinitions @group.dossier_definitions, partial: "api/dossier_definitions/dossier_definition", as: :dossier_definition
json.groupMember @group.users.include?(current_user)
