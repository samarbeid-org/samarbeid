if group.nil?
  json.null!
else
  json.partial! "api/groups/group_minimal", group: group

  json.userCount group.users.count
  json.workflowDefinitionCount group.workflow_definitions.count
  json.dossierDefinitionCount group.dossier_definitions.count
  json.groupMember group.users.include?(current_user)
end
