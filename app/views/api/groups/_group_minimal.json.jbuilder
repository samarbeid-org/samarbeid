if group.nil?
  json.null!
else
  json.type "group"
  json.id group.id
  json.name group.name
end
