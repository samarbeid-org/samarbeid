json.dataFields do
  json.partial! "/api/data_items/data_item_dossier", collection: @dossier.dossier_items.map(&:to_data_item), as: :data_item
end
