if dossier.nil?
  json.null!
else
  json.id dossier.id

  json.definition do
    json.name dossier.definition.name
  end

  json.title dossier.title
  json.subtitle dossier.subtitle

  json.referenceLabel dossier.reference_label
  json.mentionLabel dossier.mention_label
end
