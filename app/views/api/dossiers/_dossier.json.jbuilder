if dossier.nil?
  json.null!
else
  json.id dossier.id
  json.gid dossier.to_global_id.to_s
  json.title dossier.title
  json.subtitle dossier.subtitle

  json.createdAt dossier.created_at

  json.groups dossier.definition.groups, partial: "api/groups/group_minimal", as: :group

  json.dataFields do
    json.partial! "/api/data_items/data_item_dossier", collection: dossier.data_items, as: :data_item
  end

  json.definition do
    json.id dossier.definition.id
    json.name dossier.definition.name
  end

  json.taskReferenceCount @referenced_in_tasks_count

  json.dossierFieldReferences dossier.definition.referenced_in_dossier_item_definitions_for(current_user) do |field|
    inverse_reference = field.content_type.inverseReference(field)
    json.id field.id
    json.title inverse_reference[:title]
    json.count field.dossier_items.reference_to(dossier).select(:dossier_id).distinct.count
    json.showEmpty inverse_reference[:definition][:showEmpty]
  end

  json.referenceLabel dossier.reference_label
end
