json.array! @tasks.group_by(&:workflow) do |workflow, tasks|
  json.workflow do
    json.partial! "/api/workflows/workflow_minimal", workflow: workflow
  end

  json.tasks do
    json.array! tasks do |task|
      json.partial! "/api/tasks/task_minimal", task: task

      json.dossierFields task.task_items.map(&:content_item).select { |ci| ci.content_type.name == ContentTypes::Dossier.name } do |content_item|
        dossiers = content_item.value.to_a.compact

        if dossiers.any?
          json.id content_item.id
          json.label content_item.label

          json.value do
            json.partial! "api/dossiers/dossier_list_item", collection: dossiers, as: :dossier
          end
        end
      end
    end
  end

  # json.linkTaskId task.id

  # json.dossierFields task.workflow.content_items.select { |ci| ci.content_type.name == ContentTypes::Dossier.name } do |content_item|
  #   dossiers = content_item.value.to_a.compact
  #
  #   if dossiers.any?
  #     json.id content_item.id
  #     json.label content_item.label
  #
  #     json.value do
  #       json.partial! "api/dossiers/dossier_list_item", collection: dossiers, as: :dossier
  #     end
  #   end
  # end
end
