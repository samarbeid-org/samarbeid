if @dossier_definition
  json.dossier_definition do
    json.partial! "/api/dossier_definitions/dossier_definition", dossier_definition: @dossier_definition
  end

  json.partial! "/api/field_filters/field_filters", field_definitions: @dossier_definition.items
else
  json.dossier_definitions @dossier_definitions.map { |pd| {value: pd.id, text: pd.name} }
end

json.totalCount @result.total_count
json.total_pages @result.total_pages
json.result do
  json.partial! "/api/dossiers/dossier_list_item", collection: @result.query, as: :dossier
end
