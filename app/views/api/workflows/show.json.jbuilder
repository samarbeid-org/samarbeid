json.partial! "/api/workflows/workflow", workflow: Workflow.includes(:direct_tasks, :blocks, :content_items, :workflow_definition, :assignee, all_tasks: [:task_items]).find(@workflow.id)
