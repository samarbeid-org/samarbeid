json.filter_category_counts @result.filter_category_counts
json.users do
  json.partial! "/api/users/user", collection: @users, as: :user
end
json.workflow_definitions @workflow_definitions.map { |pd| {value: pd.id, text: pd.name} }

json.total_pages @result.total_pages
json.result do
  json.partial! "/api/workflows/workflow_minimal", collection: @result.query, as: :workflow
end
