json.array! @content_items do |content_item|
  json.id content_item.id
  json.label content_item.label_with_task
  json.type do
    json.name content_item.content_type.type
    json.label content_item.content_type.label
  end
  json.partial! "/api/content_types/options", type: content_item.content_type, options: content_item.content_type.set_options_defaults(content_item.options)
end
