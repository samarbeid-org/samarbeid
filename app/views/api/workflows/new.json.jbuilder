json.title @workflow.title_from_db
json.autoGenerateTitle @workflow.workflow_definition.auto_generate_title

json.assignee do
  json.partial! "api/users/user", user: @workflow.assignee
end
json.groups @workflow.workflow_definition.groups, partial: "api/groups/group_minimal", as: :group

json.dataFields @task_items.map(&:as_datafield), partial: "/api/data_items/data_item_task", as: :data_field
