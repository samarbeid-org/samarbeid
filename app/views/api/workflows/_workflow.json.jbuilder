json.type "workflow"
json.id workflow.id
json.gid workflow.to_global_id.to_s
json.title workflow.title
json.titleTooltip workflow.title_tooltip
json.titleFromDB workflow.title_from_db
# json.description add_mention_labels(workflow.description, current_user)
json.state workflow.aasm_state
json.stateUpdatedAt workflow.state_updated_at
json.systemIdentifier workflow.system_identifier

json.finished workflow.completable?
json.availableActions do
  json.array!({
    complete: workflow.may_complete?,
    reopen: workflow.may_reopen?,
    design: true
  }.filter_map { |k, v| k if v })
end

json.commentsCount workflow.comments.count

json.definition do
  json.partial! "/api/workflow_definitions/workflow_definition", workflow_definition: workflow.workflow_definition
end

json.assignee do
  json.partial! "api/users/user", user: workflow.assignee
end

json.contributors workflow.contributors, partial: "api/users/user", as: :user

json.groups workflow.workflow_definition.groups, partial: "api/groups/group_minimal", as: :group

json.structure workflow.items do |item|
  case item
  when Task
    if defined?(@task) && @task && (@task.id == item.id)
      json.partial!("api/tasks/task", task: item)
    else
      json.partial!("api/tasks/task_minimal", task: item)
    end
  when Block
    json.type "block"
    json.id item.id

    json.active item.active? || item.completed?
    json.important item.items.any? { |item| item.active? || item.completed? }

    if item.decision_set?
      json.condition do
        json.set item.condition_set?
        json.valid item.condition_valid_and_not_confirmed?
        json.confirmed item.content_item.confirmed?
      end
    end

    json.title item.title
    json.tasks item.items do |block_task|
      if defined?(@task) && (@task.id == block_task.id)
        json.partial!("api/tasks/task", task: block_task)
      else
        json.partial!("api/tasks/task_minimal", task: block_task)
      end
    end

    json.parallel item.parallel
    json.decision item.content_item&.content_type&.deserialize(item.decision)
    json.contentItemId item.content_item_id
  end
end

json.nextTaskId Services::NextInterestingThing.new(workflow, current_user).next_station&.id

json.successorWorkflows workflow.successor_workflows.order(created_at: :desc), partial: "api/workflows/workflow_minimal", as: :workflow
json.predecessorWorkflow do
  json.partial! "api/workflows/workflow_minimal", workflow: workflow.predecessor_workflow
end

json.referenceLabel workflow.reference_label

json.visible_tasks tasks_visible_in_sidebar(workflow) do |sub_task|
  json.id sub_task.id
  json.state sub_task.aasm_state
end
