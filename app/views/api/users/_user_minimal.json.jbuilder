if user.nil?
  json.null!
else
  json.type "user"
  json.id user.id
  json.fullname user.fullname

  json.avatar do
    json.partial! "api/users/avatar", user: user, size: local_assigns[:avatar_size], show_deactivated: local_assigns[:show_deactivated]
  end
end
