if user.nil?
  json.null!
else
  json.partial! "api/users/user_minimal", user: user, avatar_size: local_assigns[:avatar_size], show_deactivated: local_assigns[:show_deactivated]

  json.email user.email
  json.unconfirmed_email user.unconfirmed_email if user.unconfirmed_email.present?
  json.confirmed user.confirmed?
  json.deactivatedAt user.deactivated_at
end
