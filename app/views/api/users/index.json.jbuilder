json.filter_category_counts @result.filter_category_counts
json.total_pages @result.total_pages
json.result do
  json.partial! "/api/users/user", collection: @result.query, as: :user
end
