json.contentItemDefinitions do
  @content_item_definitions.each do |content_item_definition|
    json.set! content_item_definition.id do
      json.partial! "/api/content_item_definitions/content_item_definition", content_item_definition: content_item_definition
    end
  end
end

json.taskItemDefinitions do
  json.partial! "/api/task_item_definitions/task_item_definition", collection: @task_item_definitions, as: :task_item_definition
end
