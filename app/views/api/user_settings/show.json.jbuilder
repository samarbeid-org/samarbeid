json.partial! "/api/users/user", user: @user, avatar_size: 100, show_deactivated: true

json.firstname @user.firstname
json.lastname @user.lastname
json.isAdmin @user.is_admin?
json.updatedAt @user.updated_at
json.groupCount @user.groups.count

json.notifications do
  json.interval @user.noti_interval
  json.items User.notification_select_options.map do |value, text|
    json.value value
    json.text text
  end
end

json.partial! "/api/calendars/calendar", token: @user.calendar_token
json.api_token @user.api_token
