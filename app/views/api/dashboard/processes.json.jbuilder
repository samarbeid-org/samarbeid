json.total_count @total_count
json.result @result do |workflow_definition|
  json.id workflow_definition.id
  json.systemIdentifier workflow_definition.system_identifier
  json.name workflow_definition.name
  json.description ActionView::Base.full_sanitizer.sanitize(workflow_definition.description)&.truncate(100)

  assignees_tasks = Task.includes(:workflow).active.where(workflow: {workflow_definition: workflow_definition})
    .where.not(assignee: nil).group_by(&:assignee)
    .sort_by { |assignee, tasks| [assignee.eql?(current_user) ? 0 : 1, -tasks.count] }

  json.assignees_with_count assignees_tasks do |assignee, tasks|
    json.user do
      json.partial! "/api/users/user", user: assignee
    end
    json.count tasks.count
  end
end
