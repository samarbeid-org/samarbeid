json.array! @events do |event|
  json.object do
    object = event.object
    json.type object.object_type
    json.id object.id
    json.title object.reference_label
  end
  json.change_name event.model_name.human
  json.last_updated_at event.created_at
end
