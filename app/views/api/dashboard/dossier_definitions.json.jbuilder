json.total_count @total_count
json.result @result do |result|
  json.id result.id
  json.name result.name
  json.description ActionView::Base.full_sanitizer.sanitize(result.description)&.truncate(100)
  json.instance_count result.dossiers.count
end
