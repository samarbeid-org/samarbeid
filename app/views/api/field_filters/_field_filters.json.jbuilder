json.fieldDefinitions do
  json.array! field_definitions do |field_definition|
    json.id field_definition.id
    json.label field_definition.label
    json.contentType field_definition.content_type.to_s
    json.partial! "/api/content_types/options", type: field_definition.content_type, options: field_definition.content_type.set_options_defaults(field_definition.options)

    json.grouping field_definition.grouping if field_definition.respond_to?(:grouping)
  end
end

json.comparators do
  json.contentTypes do
    ContentType.types_array.each do |content_type|
      json.set! content_type.to_s, content_type.supported_comparators
    end
  end
  json.translations do
    ContentType.types_array.flat_map { |ct| ct.supported_comparators }.uniq.each do |comparator|
      json.set! comparator, I18n.t("content_type_comparators.#{comparator}")
    end
  end
end
