json.total_pages @result.total_pages
json.result do
  json.array! @result.query.each do |dossier_definition|
    json.partial! "/api/dossier_definitions/dossier_definition", dossier_definition: dossier_definition
    json.instanceCount dossier_definition.dossiers.count
    json.groupCount dossier_definition.groups.count
  end
end
json.hiddenElementsCount DossierDefinition.hidden.count
