json.id dossier_definition.id
json.name dossier_definition.name
json.description dossier_definition.description
json.descriptionText strip_tags(dossier_definition.description)
