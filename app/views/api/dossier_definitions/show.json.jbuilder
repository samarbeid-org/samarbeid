json.partial! "/api/dossier_definitions/dossier_definition", dossier_definition: @dossier_definition

json.gid @dossier_definition.to_global_id.to_s
json.updatedAt @dossier_definition.updated_at

json.groups do
  json.partial! "/api/groups/group", collection: @dossier_definition.groups.order(:name), as: :group
end

json.titleFields @dossier_definition.title_fields
json.subtitleFields @dossier_definition.subtitle_fields

json.fields @dossier_definition.items do |field|
  json.id field.id
  json.name field.computed_label
  json.position field.position
  json.type do
    json.name field.content_type.to_s
    json.label field.content_type.label
    json.partial! "/api/content_types/options", type: field.content_type, options: field.options
  end
  json.required field.required
  json.recommended field.recommended
  json.unique field.unique
  json.partial! "/api/data_items/data_item_value", content_type: field.content_type, value: field.default_value, key: :default_value
end

json.dataTransformations do
  json.partial! "/api/data_transformations/data_transformation",
    collection: DataTransformation.includes(:dossier_item_definition).where(dossier_item_definition: {definition_id: @dossier_definition.id}),
    as: :data_transformation
end

json.instanceCount @dossier_definition.dossiers.count
json.referenceCount @dossier_definition.referenced_in_dossier_item_definitions_for(current_user).count
json.dataTransformationCount DataTransformation.includes(:dossier_item_definition).where(dossier_item_definition: {definition_id: @dossier_definition.id}).count
