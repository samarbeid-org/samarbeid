json.array! @fields do |field|
  inverse_reference = field.content_type.inverseReference(field)
  json.id field.id
  json.title inverse_reference[:title]
  json.source do
    json.definition field.definition.name
    json.field field.name
  end
  json.referenceDefinition do
    json.title inverse_reference[:definition][:title]
    json.showEmpty inverse_reference[:definition][:showEmpty]
  end
end
