json.filter_category_counts @result.filter_category_counts
json.total_pages @result.total_pages
json.result do
  json.partial! "/api/notifications/notification", collection: @result.query, as: :notification
end

# TODO: This should be part of the _current_user partial and the cached current_user in the frontend should get updated when changing user data
json.userNotificationInterval User.notification_interval_translation(current_user.noti_interval)
