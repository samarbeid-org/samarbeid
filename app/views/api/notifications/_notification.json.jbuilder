json.id notification.id
lo = notification.event.localized_object(true, current_user, true, true)
lo = truncateNotificationMessage(lo, truncate_payload_max_length) if defined?(truncate_payload_max_length) && truncate_payload_max_length.present?
json.event lo
json.bookmarked_at notification.bookmarked_at
json.delivered_at notification.delivered_at
json.read_at notification.read_at
