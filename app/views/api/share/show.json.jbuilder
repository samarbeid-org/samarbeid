if @share_link_user.active?
  json.task @share_link_user.shareable, partial: "/api/tasks/task_share", as: :task
end

json.requester do
  json.partial! "api/users/user_minimal", user: @share_link_user.shareable.assignee, avatar_size: 60
end
json.completed @share_link_user.completed?
