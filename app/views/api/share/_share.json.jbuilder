if share.nil?
  json.null!
else
  json.state share.aasm_state
  json.name share.name
  json.updatedAt share.updated_at

  if share.active?
    json.url home_share_url(share_token: share.token)
  end
end
