json.array! @users_and_groups do |user_or_group|
  case user_or_group
  when User
    json.partial! "/api/users/user", user: user_or_group
    json.mentionLabel user_or_group.mention_label
  when Group
    json.partial! "/api/groups/group", group: user_or_group
    json.mentionLabel user_or_group.mention_label
  end
end
