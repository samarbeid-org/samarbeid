if automation.nil?
  json.null!
else
  json.id automation.id
  json.title automation.title
  json.active automation.active?

  json.schedule do
    json.merge! automation.schedule
    json.description automation.ice_cube_schedule&.to_s
    json.nextExecution automation.get_next_execution
  end

  json.workflowDefinition do
    json.partial! "api/workflow_definitions/workflow_definition", workflow_definition: automation.workflow_definition
  end

  json.candidateAssignee do
    json.partial! "api/users/user", user: automation.candidate_assignee
  end

  json.updatedAt automation.updated_at
end
