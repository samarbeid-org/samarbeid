json.total_pages @result.total_pages
json.result do
  json.partial! "/api/automations/automation", collection: @result.query, as: :automation
end
