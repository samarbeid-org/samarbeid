json.id data_transformation_item.id
json.error_messages data_transformation_item.error_messages
json.confirmed data_transformation_item.confirmed

json.target_item do
  data_item = data_transformation_item.to_data_item
  json.partial! "/api/data_items/data_item_value", content_type: data_item.content_type, value: data_item.value
end

json.source_item do
  data_item = data_transformation_item.dossier.get_item(data_transformation_item.data_transformation.dossier_item_definition).to_data_item
  json.partial! "/api/data_items/data_item_value", content_type: data_item.content_type, value: data_item.value
end

json.parent do
  json.id data_transformation_item.dossier.id
  json.gid data_transformation_item.dossier.to_global_id.to_s
  json.title data_transformation_item.dossier&.title
  json.subtitle data_transformation_item.dossier&.subtitle
end
