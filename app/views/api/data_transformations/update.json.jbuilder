json.type_counts @data_transformation_item_counts
json.item do
  json.partial! "/api/data_transformations/data_transformation_item", data_transformation_item: @data_transformation_item
end
