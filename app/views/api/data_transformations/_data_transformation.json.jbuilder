json.id data_transformation.id

json.field do
  json.id data_transformation.dossier_item_definition.id
  json.name data_transformation.dossier_item_definition.name
end

json.source_definition do
  json.type do
    json.name data_transformation.dossier_item_definition.content_type.to_s
    json.label data_transformation.dossier_item_definition.content_type.label
    json.partial! "/api/content_types/options", type: data_transformation.dossier_item_definition.content_type, options: data_transformation.dossier_item_definition.options
  end
  json.required data_transformation.dossier_item_definition.required
  json.unique data_transformation.dossier_item_definition.unique
end

json.target_definition do
  json.type do
    json.name data_transformation.content_type.to_s
    json.label data_transformation.content_type.label
    json.partial! "/api/content_types/options", type: data_transformation.content_type, options: data_transformation.options
  end
  json.required data_transformation.required
  json.unique data_transformation.unique
end
