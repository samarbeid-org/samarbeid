json.partial! "/api/data_transformations/data_transformation", data_transformation: @data_transformation
json.type_counts @data_transformation_item_counts
json.items do
  json.partial! "/api/data_transformations/data_transformation_item", collection: @data_transformation_items, as: :data_transformation_item
end
