json.partial! "/api/workflow_definitions/workflow_definition", workflow_definition: @workflow_definition

json.gid @workflow_definition.to_global_id.to_s
json.updatedAt @workflow_definition.updated_at

json.groups do
  json.partial! "/api/groups/group", collection: @workflow_definition.groups.order(:name), as: :group
end

json.instanceCount @workflow_definition.workflows.count

json.contentItemDefinitions do
  json.merge!({})
  @workflow_definition.content_item_definitions.each do |content_item_definition|
    json.set! content_item_definition.id do
      json.partial! "/api/content_item_definitions/content_item_definition", content_item_definition: content_item_definition
    end
  end
end

json.startItemDefinitionIds @workflow_definition.start_item_definitions.map(&:id)
json.titleItemDefinitionIds @workflow_definition.title_item_definitions.map(&:id)

json.structure @workflow_definition.item_definitions do |item|
  case item
  when TaskDefinition
    json.type "task_definition"
    json.partial!("api/task_definitions/task_definition", task_definition: item)
  when BlockDefinition
    json.type "block_definition"
    json.id item.id
    json.title item.title
    json.parallel item.parallel
    json.updatedAt item.updated_at

    json.condition do
      json.itemId item.content_item_definition&.id
      json.decision item.content_item_definition&.content_type&.deserialize(item.decision)
    end

    json.items item.direct_task_definitions do |block_task_definition|
      json.type "task_definition"
      json.partial!("api/task_definitions/task_definition", task_definition: block_task_definition)
    end
  end
end
