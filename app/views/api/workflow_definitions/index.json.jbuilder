json.total_pages @result.total_pages
json.result do
  json.array! @result.query.each do |workflow_definition|
    json.partial! "/api/workflow_definitions/workflow_definition", workflow_definition: workflow_definition
    json.instanceCount workflow_definition.workflows.count
    json.groupCount workflow_definition.groups.count
  end
end
json.hiddenElementsCount WorkflowDefinition.hidden.count
