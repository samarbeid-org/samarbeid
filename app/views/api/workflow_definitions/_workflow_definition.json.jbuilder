if workflow_definition.nil?
  json.null!
else
  json.id workflow_definition.id
  json.systemIdentifier workflow_definition.system_identifier
  json.name workflow_definition.display_name
  json.nameFromDB workflow_definition.name
  json.description workflow_definition.description
  json.descriptionText strip_tags(workflow_definition.description)
  json.deactivatedAt workflow_definition.deactivated_at
  json.autoGenerateTitle workflow_definition.auto_generate_title
end
