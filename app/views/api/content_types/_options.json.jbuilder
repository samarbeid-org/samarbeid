if options.nil?
  json.options do
    json.null!
  end
else
  case type.name
  when ContentTypes::Dossier.name
    json.options do
      options.each do |key, value|
        case key
        when "type"
          json.set! key do
            json.partial! "/api/dossier_definitions/dossier_definition", dossier_definition: DossierDefinition.find(value)
          end
        else
          json.set! key, value
        end
      end
    end
  else
    json.options options
  end
end
