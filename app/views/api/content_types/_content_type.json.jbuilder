if content_type.nil?
  json.null!
else
  json.type content_type.type
  json.label content_type.label
  json.options_definition content_type.options_definition
  json.calendar_addable content_type.calendar_addable?
end
