json.queryTerm @query

json.total_pages @search_result.total_pages

json.results do
  json.array! @search_result do |document|
    result = document.searchable

    case result.class.name
    when Task.name
      json.type "task"
      json.object do
        json.partial! "/api/tasks/task", task: result
        json.workflow do
          json.partial! "/api/workflows/workflow_minimal", workflow: result.workflow
        end
      end
    when Workflow.name
      json.type "workflow"
      json.object do
        json.partial! "/api/workflows/workflow_minimal", workflow: result
      end
    when Dossier.name
      json.type "dossier"
      json.object do
        json.partial! "/api/dossiers/dossier_list_item", dossier: result
      end
    else
      json.error "Unknown result object with ruby-class #{result.class.name}!"
    end
    json.highlights do
      json.array! [["treffer", document.pg_search_highlight]].map do |highlight_key, highlight_fragments|
        json.key highlight_key
        json.fragments [highlight_fragments].map { |fragment| sanitize(fragment, tags: ["mark", "br"]) }
      end
    end
  end
end
