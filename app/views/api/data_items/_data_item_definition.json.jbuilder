json.id definition.id
json.name definition.name
json.type definition.content_type.to_s
json.partial! "/api/content_types/options", type: definition.content_type, options: definition.content_type.set_options_defaults(definition.options)
json.required definition.required
