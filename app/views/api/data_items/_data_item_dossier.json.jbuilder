json.definition do
  json.partial! "/api/data_items/data_item_definition", definition: data_item

  json.recommended data_item.recommended unless data_item.recommended.nil?
  json.unique data_item.unique unless data_item.unique.nil?
end

json.partial! "/api/data_items/data_item_value", content_type: data_item.content_type, value: data_item.value
json.lock_version data_item.lock_version unless data_item.lock_version.nil?

if current_user&.can?(:list, User)
  json.lastUpdated do
    json.at data_item.last_updated
    json.by do
      json.partial! "/api/users/user", user: data_item.last_updated_by
    end
  end
end
