key = local_assigns.fetch :key, :value
if value.nil?
  json.set! key do
    json.null!
  end
else
  case content_type.name
  when ContentTypes::Dossier.name
    json.set! key do
      json.partial! "api/dossiers/dossier_list_item", collection: value, as: :dossier
    end
  when ContentTypes::File.name
    json.set! key do
      json.partial! "api/uploaded_files/uploaded_file", collection: value, as: :uploaded_file
    end
  when ContentTypes::Richtext.name, ContentTypes::Note.name
    json.set! key, add_mention_labels(value, current_user)
  else
    json.set! key, value
  end
end
