json.definition do
  json.partial! "/api/data_items/data_item_definition", definition: data_field[:definition]

  json.infobox data_field[:definition].infobox
  json.task_item_id data_field[:definition].task_item_id
  json.task_item_count data_field[:definition].task_item_count
  json.block_count data_field[:definition].block_count

  if data_field[:definition].content_type.calendar_addable?
    json.options do
      json.add_to_calendar data_field[:definition].add_to_calendar
    end
  end
end

json.partial! "/api/data_items/data_item_value", content_type: data_field[:definition].content_type, value: data_field[:value]
json.lock_version data_field[:lock_version]
json.locked data_field[:locked_by].present?

if current_user&.can?(:list, User)
  json.confirmedAt data_field[:confirmed_at]
  json.lastUpdated do
    json.at data_field[:last_updated]
    json.by do
      json.partial! "/api/users/user", user: data_field[:last_updated_by]
    end
  end

  json.lockedBy do
    json.partial! "/api/users/user", user: data_field[:locked_by]
  end
end
