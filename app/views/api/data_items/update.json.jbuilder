json.workflow do
  json.partial! "/api/workflows/workflow", workflow: @workflow if current_user.can?(:read, @workflow)
end

json.dataField do
  json.partial! "/api/data_items/data_item_task", data_field: @task_item.as_datafield
end
