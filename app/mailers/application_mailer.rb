class ApplicationMailer < ActionMailer::Base
  include ActionView::Helpers::TextHelper
  helper EmailHelper

  after_action do
    mail.subject.prepend("samarbeid | ")
  end

  # default from: "no-reply@samarbeid.org" # configured in environments/production.rb via ENV
  layout "mailer"

  protected

  # Adopted from: https://www.netskin.com/posts/20230716-autogenerate-plain-email-version
  # Creates a very simple plain text version of html mail. Does NOT care for conserving links or other styling elements
  # but only retains the pure information and some layout (newlines)
  def render_text_for(template_name)
    html = render_to_string(template_name, formats: :html)

    preprocessed = html
      .gsub("<hr>", "\n--- --- --- --- --- --- --- --- --- --- --- ---\n")
      .gsub(/<br\\?>/, " ") # line breaks
      .gsub(/<li[^>]*>/, "- ") # lists
      .gsub(/<\/t[hd]>\s*<t[hd][^>]*>/, " | ") # table cells (tr and table tags are removed later)

    text = ActionController::Base.helpers.strip_tags(preprocessed)
      .split("\n").map(&:strip).join("\n") # fix indentation
      .gsub("\n\n\n", "\n") # remove extensive new lines
      .strip

    render(plain: text)
  end
end
