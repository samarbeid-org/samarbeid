class NotificationsMailer < ApplicationMailer
  def recent
    @user = params[:user]
    @notifications = params[:notifications]
    @total_count = @user.notifications.unread.count

    and_more_text = " (#{@total_count} ungelesene insgesamt)" if @notifications.length < @total_count
    subject_main = if @notifications.length == 1
      @notifications.first.event.localized_object(true, @user, false, true)[:chunks].map do |c|
        c[:text]
      end.join("")
    else
      "#{@notifications.count} neue Benachrichtigungen"
    end
    subject = "#{subject_main}#{and_more_text}".truncate(200, omission: "…", separator: /\s/)

    mail(to: @user.email, subject: subject) do |format|
      format.html
      format.text { render_text_for(:recent) }
    end
  end
end
