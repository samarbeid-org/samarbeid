import { defineConfig } from "vite";
import ViteRails from "vite-plugin-rails";
import vuePlugin from '@vitejs/plugin-vue2'

import path from "path";
import fs from "fs";

const sourceCodeDir = "app/javascript";
const items = fs.readdirSync(sourceCodeDir);
const directories = items.filter((item) =>
  fs.lstatSync(path.join(sourceCodeDir, item)).isDirectory(),
);
const aliasesFromJavascriptRoot: { [key: string]: string } = {};
directories.forEach((directory) => {
  aliasesFromJavascriptRoot[directory] = path.resolve(
    __dirname,
    sourceCodeDir,
    directory,
  );
});

export default defineConfig({
  plugins: [
    ViteRails(),
    vuePlugin(),
  ],
  resolve: {
    alias: {
      ...aliasesFromJavascriptRoot,
      assets: path.resolve(__dirname, 'app/assets'),
      vue: 'vue/dist/vue.esm.js'
    },
  },
});
