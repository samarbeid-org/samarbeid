namespace :db do
  desc "Anonymize majority of database content to allow debugging by external parties. WARNING DELETES MOST DB CONTENT!"
  task anonymize: :environment do
    raise "This DELETES most DB content. Only allowed for development environment" unless Rails.env.in?(["development", "test"])
    unless Rails.env.eql?("test")
      puts "WARNING: DELETES ALL DB CONTENT!!! Do you really want to continue (press 'Y')?"
      exit unless $stdin.gets.squish.downcase == "y"
    end

    puts "Anonymizing all content items" unless Rails.env.eql?("test")
    ContentItem.update_all(value: nil)

    puts "Anonymizing all workflow titles" unless Rails.env.eql?("test")
    Workflow.update_all(title: nil)

    puts "Anonymizing all dossier items" unless Rails.env.eql?("test")
    DossierItem.update_all(value: nil)

    puts "Anonymizing all comments" unless Rails.env.eql?("test")
    Comment.update_all(message: nil)

    puts "(Re)Creating admin@example.org super-user with default password [change-me]" unless Rails.env.eql?("test")
    Rake::Task["db:create_super_admin"].invoke("admin@example.org", "change-me")
  end

  desc "Create or update super_admin user account"
  task :create_super_admin, [:email, :password] => :environment do |t, args|
    email = args[:email] || "admin@example.org"
    password = args[:password] || SecureRandom.alphanumeric(12)

    admin = User.create_with(
      password: password, confirmed_at: Time.current, noti_interval: "never",
      firstname: "Samarbeid Team", lastname: "Deaktivieren falls kein Support nötig"
    )
      .find_or_create_by(email: email)
    admin.update!(role: :super_admin, groups: Group.all, deactivated_at: nil)
    admin.update!(password: password, password_confirmation: password)
    unless Rails.env.test?
      puts "=" * 80
      puts "Created/updated super_admin user #{admin.email} with password: #{password}"
      puts "=" * 80
    end
  end
end
