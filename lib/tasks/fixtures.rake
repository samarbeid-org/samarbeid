namespace :db do
  namespace :fixtures do
    desc 'Create YAML test fixtures from data in an existing database.
    Defaults to development database.  Set RAILS_ENV to override.'
    task dump: :environment do
      skip_tables = ["schema_migrations", "ar_internal_metadata", "good_jobs", "good_job_processes", "good_job_batches", "good_job_executions", "good_job_settings"]
      puts "Dumping all tables to fixture files. Skipping tables: #{skip_tables.join(", ")}" unless Rails.env.eql?("test")
      Dir.mkdir(Rails.root.join("test/fixtures_dump")) unless File.exist?(Rails.root.join("test/fixtures_dump"))

      ActiveRecord::Base.establish_connection(Rails.env.to_sym)
      (ActiveRecord::Base.connection.tables - skip_tables).each do |table_name|
        # Gather table information
        columns_and_types = columns_and_types(table_name)
        column_names = column_names(columns_and_types)

        # Make order of output more deterministic
        table_query_with_order = "SELECT * FROM %s"
        if column_names.include?("id")
          table_query_with_order << " ORDER BY id ASC"
        elsif column_names.select { |column_name| column_name.match(/.*_id\Z/) }.present?
          other_id_columns = column_names.select { |column_name| column_name.match(/.*_id\Z/) }.sort
          other_id_sql = other_id_columns.map { |cn| "#{cn} ASC" }
          table_query_with_order << " ORDER BY #{other_id_sql.join(", ")}"
        else
          table_query_with_order << " ORDER BY #{column_names.min} ASC"
        end

        # Write table data to yml fixture file
        File.open(Rails.root.join("test/fixtures_dump/#{table_name}.yml"), "w") do |file|
          data = ActiveRecord::Base.connection.execute(table_query_with_order % table_name).to_a
          json_columns = columns_and_types.select { |ct| ct["data_type"] == "jsonb" }.map { |ct| ct["column_name"] }

          i = "000"
          hash_with_keys = data.each_with_object({}) { |record, hash|
            json_columns.each { |column_name| record[column_name] = JSON.parse(record[column_name]) unless record[column_name].nil? }
            hash["#{table_name}_#{i.succ!}"] = record.to_h
          }
          file.write hash_with_keys.to_yaml
        end
      end
    end
  end
end

def columns_and_types(table_name)
  case database_adapter
  when :sqlite
    columns_and_types_query = "PRAGMA table_info('#{table_name}')"
  when :postgres
    columns_and_types_query = "SELECT COLUMN_NAME, DATA_TYPE FROM information_schema.columns WHERE TABLE_NAME = '#{table_name}'"
  end

  ActiveRecord::Base.connection.execute(columns_and_types_query)
end

def column_names(columns_and_types)
  case database_adapter
  when :sqlite
    columns_and_types.map { |ct| ct["name"] }
  when :postgres
    columns_and_types.map { |ct| ct["column_name"] }
  end
end

def database_adapter
  case ActiveRecord::Base.connection.class.name
  when "ActiveRecord::ConnectionAdapters::SQLite3Adapter"
    :sqlite
  when "ActiveRecord::ConnectionAdapters::PostgreSQLAdapter"
    :postgres
  else
    raise "Unsupported Database"
  end
end
