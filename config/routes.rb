Rails.application.routes.draw do
  devise_for :users, skip: [:sessions, :passwords, :registrations], controllers: {confirmations: "users/confirmations"}
  devise_scope :user do
    get "users/sign-in", to: "devise/sessions#new", as: :new_user_session
    post "users/sign-in", to: "devise/sessions#create", as: :user_session
    get "users/sign-out", to: "devise/sessions#destroy", as: :destroy_user_session

    get "users/lost-password", to: "devise/passwords#new", as: :new_user_password
    post "users/lost-password", to: "devise/passwords#create", as: :user_password

    match "users/change-password", to: "devise/passwords#update", via: [:put, :patch]
    get "users/change-password", to: "devise/passwords#edit", as: :edit_user_password
  end

  mount Samarbeid::Api => "/"
  authenticated :user do
    root to: "home#show", as: :authenticated_root

    mount GrapeSwaggerRails::Engine => "/samarbeid-api-documentation"
  end
  root to: redirect("/users/sign-in")

  mount RailsAdmin::Engine => "/admin", :as => "rails_admin"
  authenticate :user, ->(user) { user.super_admin? } do
    mount GoodJob::Engine => "/good_job"
  end

  get "home/show"

  get "/health_check", controller: :api, action: :health_check

  get "/samarbeid-calendar", controller: :"api/calendars", action: :show

  get "/share/:share_token", to: "home#share", as: :home_share

  namespace :api, defaults: {format: :json} do
    resources :workflow_definitions do
      collection do
        get :list
        get :show_system_process_definition_single_task
      end
    end

    resources :task_definitions, only: [:create, :update, :destroy] do
      member do
        patch :move
        patch :clone
        patch :update_candidate_contributors
      end
    end

    resources :block_definitions, only: [:create, :update, :destroy] do
      member do
        patch :move
      end
    end

    resources :data_item_definitions, only: [:create, :update, :destroy] do
      member do
        patch :move
      end
    end

    resources :workflows, only: [:index, :show, :update, :new, :create, :destroy] do
      collection do
        get :list
      end
      member do
        patch :update_assignee
        patch :update_contributors
        patch :cancel
        post :add_task
        post :add_block
        get :content_items
        patch :update_definition
      end
    end

    resources :tasks, only: [:index, :show, :update, :destroy] do
      collection do
        get :list
      end
      member do
        patch :update_assignee
        patch :update_contributors
        patch :update_marked
        patch :update_due_date
        patch :update_time_tracking_budget
        patch :snooze
        patch :start
        patch :complete
        patch :skip
        patch :reopen
        patch :move
        patch :clone
        get :test_move_to_process
        patch :move_to_process
        post :create_share
        patch :complete_share
      end
    end

    resources :blocks, only: [:update, :destroy] do
      member do
        patch :move
      end
    end

    resources :data_items, only: [:index, :create, :show, :update, :destroy] do
      member do
        patch :move
        patch :lock
        patch :unlock
      end
    end

    resources :content_types, only: [:index, :show]

    resources :events, only: [:index] do
      collection do
        get :time_tracking
        get :journal
        post :create_comment
      end
      member do
        get :obsolete_info
        patch :update_comment
        delete :delete_comment
      end
    end

    resources :users, only: [:index, :show, :create] do
      collection do
        get :list
        get :info
        get :ping
      end
    end

    resources :user_settings, only: [:show] do
      collection do
        patch "update"
        patch :update_password
        patch :update_notification_settings
      end
      member do
        patch :update_email
        patch :update_admin_status
        patch :update_active_status
        patch :resend_confirmation_instructions
        get :groups
      end
    end

    resource :calendar, only: [:show, :create, :destroy]
    resource :external_api_access, only: [:create, :destroy]

    resources :groups, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :list
      end
    end

    resources :users_and_groups, only: [] do
      collection do
        get :list
      end
    end

    resources :notifications, only: [:index] do
      collection do
        patch :read
        patch :read_all
        patch :delivered
      end
      member do
        patch :bookmark
        patch :unbookmark
        patch :done
      end
    end

    get "search/fulltext"

    resources :dossiers, only: [:index, :show, :new, :create, :update, :destroy] do
      collection do
        get :list
      end
      member do
        get :show_task_references
        get :show_dossier_field_references
      end
    end

    resources :dossier_definitions, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :list
        patch :update_reference
      end
      member do
        get :show_references
      end
    end

    resources :dossier_item_definitions, only: [:create, :update, :destroy] do
      member do
        patch :move
      end
    end

    resources :automations, only: [:index, :show, :create, :update, :destroy] do
      collection do
        post :validate_schedule
      end
    end

    resources :data_transformations, only: [:index, :show, :create, :update, :destroy] do
      member do
        patch :confirm_all
        patch :apply
      end
    end

    resources :uploaded_files, only: [:create, :update]

    resources :pages, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :list
      end
    end

    resources :menu_entries, only: [:index, :show, :create, :update, :destroy] do
      collection do
        get :locations
      end
    end

    resources :main_menu_entries, only: [:index, :create, :update, :destroy] do
      collection do
        get :index_filtered
      end
      member do
        patch :move
      end
    end

    resources :share, param: :share_token, only: [:show] do
      member do
        patch :complete
      end
    end

    get "dashboard/processes"
    get "dashboard/dossier_definitions"
    get "dashboard/everything_in_view"
    get "dashboard/my_last_changes"
  end

  get "p/*path", to: "home#unauthenticated", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end

  get "my/*path", to: "home#my", constraints: ->(request) do
    !request.xhr? && request.format.html?
  end

  get "*path", to: "home#show", constraints: ->(request) do
    !request.xhr? && request.format.html? && !request.fullpath.start_with?("/api")
  end
end
