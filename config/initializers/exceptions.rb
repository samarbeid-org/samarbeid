class NotImplementedException < StandardError; end

class UnexpectedResponseException < StandardError
  def initialize(code)
    super("Unexpected response code #{code}")
  end
end
