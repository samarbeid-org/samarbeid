if Rails.env.development?
  api_files = Dir[Rails.root.join("app", "api", "**", "*.rb")]
  api_reloader = ActiveSupport::FileUpdateChecker.new(api_files) do
    Rails.application.reloader.reload!
  end

  ActiveSupport::Reloader.to_complete do
    api_reloader.execute_if_updated
  end
end
