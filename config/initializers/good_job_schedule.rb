# Configure GoodJob to run cron jobs
Rails.application.configure do
  # Enable cron enqueuing in all good_job processes (we only use one)
  config.good_job.enable_cron = true
end

################################################
# Accepts natural expression for scheduling jobs. Also adds random delay and ensures schedules are unique.
# Example: schedule_job "every 10 minutes", "MyJobClass", :some_arg
# This delay will be fixed for each job but different between jobs and samarbeid instances
class SamarbeidGoodJobSchedule
  def initialize(&block)
    instance_eval(&block) if block
  end

  def convert_to_cron_with_random_delay(every)
    cron = Fugit.parse_cronish(every)
    raise "Can not parse expression: #{every}" if cron.nil?

    "#{rand(0..59)} #{cron.original}"
  end

  def create_job_hash(every, job_klass, *args)
    unique_job_key = [job_klass, *args].compact.join("-")

    [
      unique_job_key,
      {
        cron: convert_to_cron_with_random_delay(every),
        class: job_klass,
        args: args
      }
    ]
  end

  def schedule_job(every, job_klass, *)
    unique_job_key, job_hash = create_job_hash(every, job_klass, *)
    raise "Duplicate job key: #{unique_job_key}" if Rails.application.config.good_job.cron.key?(unique_job_key)

    Rails.application.config.good_job.cron[unique_job_key] = job_hash
  end
end

################################################
# Create actual GoodJob cron schedule
################################################
SamarbeidGoodJobSchedule.new do
  schedule_job "every 2 minutes", "DeliverRecentNotificationsMailsJob", :immediately

  schedule_job "every 1 hour", "DeliverRecentNotificationsMailsJob", :hourly

  schedule_job "every day at 6:05 am", "DeliverRecentNotificationsMailsJob", :daily

  schedule_job "every monday at 6:10 am", "DeliverRecentNotificationsMailsJob", :weekly

  schedule_job "every 3 hours", "FindAndProcessDueTasksJob"

  schedule_job "every 3 hours", "FindAndStartSnoozedTasksJob"

  schedule_job "every day at 0:15 am", "CleanupInvalidDataJob"

  schedule_job "every day at 0:15 am", "CleanupNotificationsJob"

  schedule_job "every day at 1:00 am", "SendStatsHeartbeatJob" if ENV["SEND_STATS_HEARTBEAT_TO"].present?

  # schedule nothing "every day at 4:30 am" as we redeploy our servers via Gitlab CI schedule. May cause cronjobs to be skipped!

  schedule_job "every day at 5:50 am", "ImportRssFeedJob"

  schedule_job "every day at 6:00 am", "RunAutomationsJob"
end
