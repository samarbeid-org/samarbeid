class ExternalServices
  def start_docker_containers_if_needed
    return if Rails.env.in?(["production", "canary", "staging"]) # Docker Host is already setup and container running
    return if ENV["CI"] # Gitlab CI starts services (aka docker container) based on .gitlab-ci.yml
    return if ENV["IN_DOCKER"] # Running in Docker (manually defined)

    return if all_required_external_services_up?

    raise "PLEASE INSTALL docker" unless Kernel.system("which", "docker")

    docker_compose_command = if Kernel.system("docker", "compose", "version")
      "docker compose"
    elsif Kernel.system("which", "docker-compose")
      "docker-compose"
    else
      raise "PLEASE INSTALL docker-compose"
    end

    puts "Docker container(s) not running. Starting docker containers..."

    Kernel.system(*docker_compose_command.split(" "), "up", "-d", "--remove-orphans")
  end

  def all_required_external_services_up?
    require_relative "tika"
    ::Tika::Service.up?
  end
end

# Boot docker container VERY early (i.e. even before fully loading rails app) as these are mere shell scripts (i.e. no ruby class loading issues)
ExternalServices.new.start_docker_containers_if_needed
