Mjml.setup do |config|
  # Use :haml as a template language
  config.template_language = :haml

  # Ignore errors silently
  config.raise_render_exception = true

  # Optimize the size of your emails
  # config.beautify = false
  # config.minify = true
  config.beautify = Rails.env.development?
  config.minify = !Rails.env.development?

  # Render MJML templates with errors
  config.validation_level = "strict"
end
