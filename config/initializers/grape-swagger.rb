GrapeSwaggerRails.options.url = "/api/v0/swagger_doc.json"
GrapeSwaggerRails.options.before_action do
  raise CanCan::AccessDenied unless current_user

  GrapeSwaggerRails.options.app_url = request.protocol + request.host_with_port

  GrapeSwaggerRails.options.api_key_name = "api_token"
  GrapeSwaggerRails.options.api_key_type = "query"
  GrapeSwaggerRails.options.api_key_default_value = current_user.api_token
end

GrapeSwaggerRails.options.app_name = "samarbeid - API documentation"

GrapeSwaggerRails.options.doc_expansion = "list"
GrapeSwaggerRails.options.hide_url_input = true
