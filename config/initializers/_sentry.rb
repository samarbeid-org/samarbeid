SENTRY_CSP_URI = ENV["SENTRY_CSP_URI"]
SENTRY_DSN = ENV["SENTRY_DSN"]

# Migrated from Raven, see https://github.com/getsentry/sentry-ruby/blob/master/MIGRATION.md
Sentry.init do |config|
  config.dsn = SENTRY_DSN
  config.enabled_environments = ["production", "canary"]
  if ENV["CI"] && ENV["CI_NODE_TOTAL"] # when running tests on Gitlab CI we want to gather data on flaky tests in sentry (different DSN set)
    config.enabled_environments = ["test"]
  end
  config.breadcrumbs_logger = [:active_support_logger, :http_logger]

  # Set tracesSampleRate to 1.0 to capture 100%
  # of transactions for performance monitoring.
  # We recommend adjusting this value in production
  config.traces_sample_rate = nil # Turn-Off tracing as it's not really useful in free sentry.io plan

  config.excluded_exceptions << "ActionController::RoutingError" # We still want to capture 404 in case we link to wrong IDs but Routing Errors are mostly noise caused by bots (sitemap.txt) or script kiddies (wp-login.ph)
end
Sentry.set_tags(host: ENV.fetch("HOST", "default"))

ActiveSupport::Reloader.to_prepare do
  Sentry.configuration.release = Services::System.revision if Services::System.revision
end
