RailsAdmin.config do |config|
  # Setup to use sprockets for now. We could also use webpacker but using the install
  # script this adds a lot of dependencies to our production frontend UI package (which is not needed for normal users)
  config.asset_source = :sprockets

  ## == Cancan Access Control ==
  config.authorize_with :cancancan
  config.current_user_method(&:current_user)
  config.parent_controller = "ApplicationController"

  ## == Configure excluded models
  #
  # hide all event subclasses in rails admin
  Rails.root.join("app", "models", "events").entries.each do |filepath|
    config.excluded_models << "Events::#{filepath.basename(".rb").to_s.camelize}"
  end
  # Exclude more models which are not really useful for users
  config.excluded_models << "ActiveStorage::Blob"
  config.excluded_models << "ActiveStorage::Attachment"
  config.excluded_models << "ActiveStorage::VariantRecord"
  config.excluded_models << "ContentTypes::Value"
  config.excluded_models << "GoodJob::ActiveJobJob"

  # Allow rails admin specific labels
  config.label_methods = [:rails_admin_label, :name, :title]

  config.actions do
    dashboard # mandatory
    index # mandatory
    new
    export
    bulk_delete
    show
    edit
    delete
    show_in_app

    ## With an audit adapter, you can add:
    history_index
    history_show
  end

  # Hide image column to prevent auto creation of variants for all variants shown through rails admin.
  # In case we re-include ActiveStorage models we could add this config block
  # config.model "ActiveStorage::VariantRecord" do
  #   configure :image do
  #     visible false
  #   end
  # end

  config.model "User" do
    configure :user_groups do
      visible false
    end

    create do
      configure :groups do
        visible false
      end
    end

    edit do
    end
  end

  config.model "Group" do
    configure :user_groups do
      visible false
    end

    create do
      configure :users do
        visible false
      end
    end
  end

  config.model "Event" do
    configure :data, :text
  end

  config.model "DossierDefinition" do
    create do
      configure :fields do
        visible false
      end
    end
  end

  config.model "DossierItemDefinition" do
    configure :definition do
      read_only do
        !bindings[:object].new_record?
      end
    end
  end

  ["WorkflowDefinition", "BlockDefinition", "TaskDefinition", "ContentItemDefinition", "TaskItemDefinition", "DossierDefinition", "DossierItemDefinition"].each do |model|
    config.model model do
      navigation_label "Vorlagen"
    end
  end

  def scope_to(column, object)
    proc { |scope| scope.where(column => object.id) if object.present? }
  end

  config.model "WorkflowDefinition" do
    base do
      exclude_fields :workflows
      configure :all_task_definitions do
        read_only true
      end
      configure :task_item_definitions do
        read_only true
      end
      configure :groups do
        read_only true
        help "Sollte über das normale UI gesetzt werden"
      end
      [:direct_task_definitions, :block_definitions, :content_item_definitions].each do |habtm_association|
        configure habtm_association do
          associated_collection_scope { scope_to(:workflow_definition_id, bindings[:object]) }
        end
      end
    end
    edit do
      # Show printed out full structure
      field :structure_as_text, :text do
        read_only true
        pretty_value do
          value.gsub("\n", "<br>").html_safe
        end
      end
    end

    list do
      # Make loading list nice and fast
      exclude_fields :all_task_definitions, :task_item_definitions, :direct_task_definitions, :block_definitions, :content_item_definitions
    end
  end

  def make_read_only_after_create(attribute)
    configure attribute do
      read_only do
        !bindings[:object].new_record?
      end
      help "Kann nach Erstellen nicht mehr verändert werden."
    end
  end

  config.model "ContentItemDefinition" do
    base do
      exclude_fields :content_items

      make_read_only_after_create(:workflow_definition)

      configure :task_item_definitions do
        read_only true
        help "Muss auf Seite der TaskDefinition gesetzt werden."
      end

      configure :block_definition do
        help "Wird in dieser BlockDefinition zur Entscheidung genutzt. Muss auf Seite der BlockDefinition gesetzt werden."
      end

      configure :default_value do
        help "Alle neuen Instanzen enthalten diesen Wert. ACHTUNG: Ist intern ein JSONB Feld und nicht alle content_types validieren ihre Werte tatsächlich."
      end
    end
  end

  config.model "TaskDefinition" do
    exclude_fields :tasks
    make_read_only_after_create(:workflow_definition)
    configure :task_item_definitions do
      associated_collection_scope { scope_to(:workflow_definition_id, bindings[:object]&.workflow_definition) }
    end
  end

  config.model "TaskItemDefinition" do
    exclude_fields :task_items
    make_read_only_after_create(:workflow_definition)
  end

  config.model "BlockDefinition" do
    configure :direct_task_definitions do
      associated_collection_scope { scope_to(:workflow_definition_id, bindings[:object]&.workflow_definition) }
    end
    configure :content_item_definition do
      associated_collection_scope { scope_to(:workflow_definition_id, bindings[:object]&.workflow_definition) }
      help "Wenn der Value des Content Items der Decision entspricht wird der Block aktiviert (wenn an der Reihe)."
    end
    configure :decision do
      help "Wenn dies dem Value des content_item_definition entspricht  wird der Block aktiviert (wenn an der Reihe)."
    end
  end

  config.model "DossierDefinition" do
    exclude_fields :dossiers
  end

  config.model "MainMenuEntry" do
    edit do
      configure :parent do
        associated_collection_scope do
          resource_scope = bindings[:object].class.reflect_on_association(:parent).source_reflection.scope

          proc do |scope|
            resource_scope ? scope.merge(resource_scope) : scope
          end
        end
      end
    end
  end

  config.model "ShareLink" do
    configure :events do
      visible false
    end
    edit do
      # configure :shareable do
      #   visible true
      # end
      configure :token do
        visible false
      end
      configure :aasm_state do
        visible false
      end
    end
  end
end

# Taken from: https://github.com/sferik/rails_admin/issues/2502
# Don't blow up RailsAdmin on JSON searches or invalid JSON formats
class RailsAdmin::Config::Fields::Types::Json
  register_instance_option :formatted_value do
    if value.is_a?(Hash) || value.is_a?(Array)
      JSON.pretty_generate(value)
    else
      value
    end
  end

  def parse_value(value)
    value.present? ? JSON.parse(value) : nil
  rescue JSON::ParserError
    value
  end
end
