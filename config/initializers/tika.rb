module Tika
  class Configuration
    def initialize
      @url = ENV["TIKA_URL"] || "http://localhost:9998"
    end

    def url(path = "/tika")
      URI.join(@url, path).to_s
    end
  end

  class Service
    def self.up?
      response = RestClient.get(Configuration.new.url, timeout: 1)
      response.body.include?("Tika Server")
    rescue
      false
    end
  end
end
